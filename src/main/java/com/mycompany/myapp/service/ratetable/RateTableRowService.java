package com.mycompany.myapp.service.ratetable;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.domain.ratetable.RateTableRow;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.ratetable.DefaultFormula;
import com.mycompany.myapp.dto.ratetable.RateTableColumnDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratetable.RateTableRowDTO;
import com.mycompany.myapp.mapper.ratetable.RateTableRowMapper;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.formula.FormulaRepository;
import com.mycompany.myapp.repository.normalizer.NormValueRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRowsRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.formula.FormulaService;
import com.mycompany.myapp.utils.OCSUtils;
import org.apache.poi.ss.formula.functions.Rate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
public class RateTableRowService extends AbstractCloneMappingService implements RateTableMappingService {

    @Autowired
    private RateTableRowsRepository rateTableRowsRepository;

    @Autowired
    private FormulaService formulaService;

    @Autowired
    private RateTableRowMapper rateTableRowMapper;

    @Autowired
    private NormValueRepository normValueRepository;

    @Autowired
    private RateTableService rateTableService;

    @Autowired
    private FormulaRepository formulaRepository;

    @Autowired
    RateTableColumnService rateTableColumnService;

    public void deleteRow(RateTableRow rateTableRow) {
        Integer domainId = OCSUtils.getDomain();
        rateTableRowsRepository.deleteByIdAndDomainId(rateTableRow.getId(), domainId);
        formulaRepository.deleteByIdAndDomainId(rateTableRow.getFormulaId(), domainId);
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = rateTableRowsRepository;
        this.childService = formulaService;
    }

    @Override
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
        List<RateTableRow> tableRows = rateTableRowsRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);
        List<CloneDTO> functions = new ArrayList<>();
        for (Long id : newParentIds) {
            Long oldRateTableId = oldNewIdParentMap.get(id).getId();
            List<Long> formulaIds = tableRows.stream().filter(item -> Objects.equals(item.getRateTableId(), oldRateTableId))
                .map(RateTableRow::getFormulaId).collect(Collectors.toList());
            formulaIds.forEach(formulaId -> {
                CloneDTO cloneDTO = new CloneDTO();
                cloneDTO.setId(formulaId);
                cloneDTO.setParentId(id);
                functions.add(cloneDTO);
            });
        }
        super.cloneMappings(newParentIds, functions, oldNewIdParentMap, parentNewChildrenOldIdMap);
    }

    public void getAllRowOfRateTable(RateTableDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        List<RateTableRowDTO> rowDTOs = rateTableRowMapper.toDto(rateTableRowsRepository.findByRateTableIdAndDomainId(dto.getId(), domainId));
        List<RateTableColumnDTO> columnDTOs = rateTableColumnService.getListColumns(dto.getId());
        List<Long> listValueIds = new ArrayList<>();

        List<Long> formulaIds = rowDTOs.stream().map(RateTableRowDTO::getFormulaId).collect(Collectors.toList());
        List<DefaultFormula> defaultFormulas = rateTableService.getDefaultFormulas(formulaIds, dto.getId());
        Map<Long, DefaultFormula> longDefaultFormulaMap = defaultFormulas.stream().collect(Collectors.toMap(DefaultFormula::getId, defaultFormula -> defaultFormula));

        for (RateTableRowDTO row : rowDTOs) {
            listValueIds = getValueIds(row.getCombinedValue());
            if (row.getFormulaId() != null) {
                DefaultFormula defaultFormula = longDefaultFormulaMap.get(row.getFormulaId());
                if (defaultFormula != null) {
                    row.setFormulaString(defaultFormula.getDefaultFormula());
                }
            }
            row.setColumns(columnDTOs);
            if (Objects.equals(dto.getDefaultCombinedValue(), row.getCombinedValue())) {
                row.setDefault(true);
            }
        }

        List<Long> normalizerIds = columnDTOs.stream().map(RateTableColumnDTO::getNormalizerId).collect(Collectors.toList());

        List<NormalizerValue> normalizerValues = normValueRepository.findByValueIdInAndNormalizerIdInAndDomainId(listValueIds, normalizerIds, domainId);

        Map<Long, NormalizerValue> longNormalizerValueMap = normalizerValues.stream().collect(Collectors.toMap(NormalizerValue::getId, normalizerValue -> normalizerValue));
        for (RateTableColumnDTO rateTableColumnDTO : columnDTOs) {
            NormalizerValue value = longNormalizerValueMap.get(rateTableColumnDTO.getNormalizerId());
            if (value != null) {
                rateTableColumnDTO.setValueId(value.getValueId());
                rateTableColumnDTO.setValueName(value.getName());
                rateTableColumnDTO.setColorText(value.getColorText());
                rateTableColumnDTO.setColorBG(value.getColorBG());
            }
        }

        dto.setRows(rowDTOs);
        dto.setColumns(columnDTOs);
    }

    @Override
    public void checkParent(List<Tree> dependencies, List<Long> childId) {
        // ignore check parent and child from formula
        return;
    }

    @Override
    public void checkChild(List<Tree> dependencies, List<Long> parentId) {
        return;
    }

    private List<Long> getValueIds(String combinedValue) {
        String[] valueIdStrings = combinedValue.split("\\/");
        List<Long> list = new ArrayList<>();
        for (String item : valueIdStrings) {
            list.add(Long.parseLong(item));
        }
        return list;
    }

    @Override
    protected String getResource() {
        return null;
    }

    @Override
    public Integer countChildByParentId(Long parentId) {
        // do not count formula
        return 0;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }

    @Override
    public Long getChildId(OCSCloneableMap mappingParent, List<Long> childrenIds, Map<Long, CloneDTO> oldNewChildMap, Long newParentId) {
        Long childId = mappingParent.getChildId();
        // if this child is create new

        Long finalChildId = childId;
        Optional<Long> newChildId = oldNewChildMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue().getId(), finalChildId)
                && Objects.equals(newParentId, entry.getValue().getParentId()))
            .map(Map.Entry::getKey)
            .findAny();

        if (newChildId.isPresent()) {
            return newChildId.get();
        }
        return childId;
    }

}
