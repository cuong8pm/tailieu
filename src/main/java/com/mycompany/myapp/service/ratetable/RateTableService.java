package com.mycompany.myapp.service.ratetable;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.mycompany.myapp.domain.normalizer.PreFunction;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.dto.normailizer.validator.FilterDTO;
import com.mycompany.myapp.dto.normailizer.validator.NormFilterValidator;
import com.mycompany.myapp.dto.normailizer.validator.NormInputValidator;
import com.mycompany.myapp.dto.normailizer.validator.PreFunctionDTO;
import com.mycompany.myapp.dto.ratetable.DefaultFormula;
import com.mycompany.myapp.repository.normalizer.PreFunctionRepository;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.NestedObject;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.StatisticItem;
import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.FormulaType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.formula.Formula;
import com.mycompany.myapp.domain.formula.FormulaErrorCode;
import com.mycompany.myapp.domain.formula.FormulaUnitConvert;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.domain.ratetable.RateTableColumn;
import com.mycompany.myapp.domain.ratetable.RateTableRow;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.StatisticItemDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.UnitTypeDTO;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.dto.ratetable.RateTableColumnDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratetable.RateTableRowDTO;
import com.mycompany.myapp.mapper.StatisticItemMapper;
import com.mycompany.myapp.mapper.formula.FormulaMapper;
import com.mycompany.myapp.mapper.normalizer.NormalizerMapper;
import com.mycompany.myapp.mapper.ratetable.RateTableColumnMapper;
import com.mycompany.myapp.mapper.ratetable.RateTableMapper;
import com.mycompany.myapp.mapper.ratetable.RateTableRowMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.StatisticItemRepository;
import com.mycompany.myapp.repository.UnitTypeRepository;
import com.mycompany.myapp.repository.actiontype.ActionTypeRepository;
import com.mycompany.myapp.repository.block.BlockRateTableMapRepository;
import com.mycompany.myapp.repository.characteristic.CharacteristicRepository;
import com.mycompany.myapp.repository.formula.FormulaErrorCodeRepository;
import com.mycompany.myapp.repository.formula.FormulaRepository;
import com.mycompany.myapp.repository.formula.FormulaUnitConvertRepository;
import com.mycompany.myapp.repository.normalizer.NestedObjectRepository;
import com.mycompany.myapp.repository.normalizer.NormValueRepository;
import com.mycompany.myapp.repository.normalizer.NormalizerRepository;
import com.mycompany.myapp.repository.ratetable.RateTableColumnsRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRowsRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRateTableMapRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.UnitTypeService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.clone.RateTableParentMappingService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service("rateTableService")
@Transactional
public class RateTableService extends AbstractCloneService {

    public static final String FORMULA_TYPE = "formula_type";
    public static final String INPUT_B = "input_B";
    public static final String INPUT_A = "input_A";
    public static final String A = "A";
    public static final String B = "B";
    public static final String PER = "per";

    @Autowired
    private RateTableRepository rateTableRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FormulaMapper formulaMapper;

    @Autowired
    private RateTableMapper rateTableMapper;

    @Autowired
    private RateTableColumnsRepository rateTableColumnsRepository;

    @Autowired
    private RateTableColumnMapper rateTableColumnMapper;

    @Autowired
    private RateTableRowMapper rateTableRowsMapper;

    @Autowired
    private RateTableRowsRepository rateTableRowsRepository;

    @Autowired
    private RatingFilterRateTableMapRepository ratingFilterRateTableMapRepository;

    @Autowired
    private BlockRateTableMapRepository blockRateTableMapRepository;

    @Autowired
    private FormulaRepository formulaRepository;

    @Autowired
    private CharacteristicRepository characteristicRepository;

    @Autowired
    private FormulaUnitConvertRepository formulaUnitConvertRepository;

    @Autowired
    private FormulaErrorCodeRepository formulaErrorCodeRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private List<RateTableMappingService> rateTableMappingServices;

    @Autowired
    private ActionTypeRepository actionTypeRepository;

    @Autowired
    private UnitTypeRepository unitTypeRepository;

    @Autowired
    private StatisticItemRepository statisticItemRepository;

    @Autowired
    private StatisticItemMapper statisticItemMapper;

    @Autowired
    private UnitTypeService unitTypeService;

    @Autowired
    private NestedObjectRepository nestedObjectRepository;

    @Autowired
    private NormalizerRepository normalizerRepository;

    @Autowired
    private RateTableColumnService rateTableColumnService;

    @Autowired
    private NormValueRepository normValueRepository;

    @Autowired
    private List<RateTableParentMappingService> rateTableParentMappingService;

    @Autowired
    private RateTableRowService rateTableRowService;

    @Autowired
    private RateTableRowMapper rateTableRowMapper;

    @Autowired
    private NormalizerMapper normalizerMapper;

    @Autowired
    protected PreFunctionRepository preFunctionRepository;

    private static final String CHARACTERISTIC = "characteristic";

    private static final String REFER_TABLE = "refer_table";

    private static final String OBJECT_NAME ="formula";

    protected static final String BEGIN_FILTER = "\\{";
    protected static final String END_FILTER = "}";
    protected static final String PATH_SPLITTER = "\\.";
    protected static final String BEGIN_FUNCTION = "\\(";
    protected static final String END_FUNCTION = ")";
    protected static final String FILTER_FUNCTION_SPLITTER = ";";
    protected static final String FILTER_SPLITTER = "&";
    protected static final String FILTER_KEY_VALUE = "=";
    protected static final String PRE_FUNCTION_SPLITTER = ":";
    protected static final String PARAMETER_SPLITTER = ",";

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = this.rateTableRepository;
        this.parentService = this.categoryService;
        this.ocsCloneableRepository = rateTableRepository;
        this.cloneMappingServices = rateTableMappingServices;
        this.parentDependMappingServices = rateTableParentMappingService;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return RateTable.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.RATE_TABLE;
    }

    @Override
    protected String getNameCategory() {
        return "Rate Tables";
    }

    public RateTableDTO getRateTableDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        RateTable rateTable = rateTableRepository.findByIdAndDomainId(id, domainId);
        if (rateTable == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.Normalizer.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(rateTable.getParentId(), domainId);
        RateTableDTO rateTableDTO = rateTableMapper.toDto(rateTable);
        if (category != null) {
            rateTableDTO.setCategoryId(category.getId());
            rateTableDTO.setCategoryName(category.getName());
            rateTableDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        if (rateTableDTO.getDefaultFormulaId() != null) {
            rateTableDTO.setDefaultFormulaString(getDefaultFormula(rateTableDTO.getDefaultFormulaId(), id));
        }
        UnitType unitType = unitTypeRepository.findByIdAndDomainId(rateTableDTO.getUnitTypeId(), domainId);
        if (unitType != null) {
            rateTableDTO.setUnitType(unitType.getName());
            rateTableDTO.setBaseRate(unitType.getBaseRate());
        }
        rateTableRowService.getAllRowOfRateTable(rateTableDTO);
        return rateTableDTO;
    }

    public void deleteRateTable(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Integer refer = ratingFilterRateTableMapRepository.countByRateTableIdAndDomainId(id, domainId);
        if (refer > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.RATE_TABLE,
                ErrorKey.RateTable.ID);
        }
        refer = blockRateTableMapRepository.countByRateTableIdAndDomainId(id, domainId);
        if (refer > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.RATE_TABLE,
                ErrorKey.RateTable.ID);
        }
        RateTable rateTable = rateTableRepository.findByIdAndDomainId(id, domainId);
        if (rateTable != null) {
            formulaRepository.deleteByIdAndDomainId(rateTable.getDefaultFormulaId(), domainId);
            rateTableColumnsRepository.deleteByRateTableIdAndDomainId(id, domainId);
            List<RateTableRow> rows = rateTableRowsRepository.findByRateTableIdAndDomainId(id, domainId);
            for (RateTableRow item : rows) {
                rateTableRowService.deleteRow(item);
            }
            rateTableRowsRepository.deleteByRateTableIdAndDomainId(id, domainId);
            rateTableRepository.deleteByIdAndDomainId(id, domainId);
        }
    }

    public RateTableDTO createRateTable(RateTableDTO rateTableDTO) {
        Integer domainId = OCSUtils.getDomain();
        // validate và save rate_table
        RateTable rateTable = rateTableMapper.toEntity(rateTableDTO);
        rateTable.setId(null);
        rateTable.setDomainId(domainId);
        checkDuplicateName(rateTable.getName(), rateTable.getId());
        checkCategory(rateTableDTO.getCategoryId());
        setPosIndex(domainId, rateTable, rateTable.getCategoryId());
        UnitType unitType = unitTypeRepository.findByIdAndDomainId(rateTable.getUnitTypeId(), domainId);
        if (unitType == null) {
            throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.RATE_TABLE,
                ErrorKey.RateTable.UNIT_TYPE);
        }
        if (rateTableDTO.getDefaultFormula() != null) {
            FormulaDTO formulaDTO = rateTableDTO.getDefaultFormula();
            validateFormula(formulaDTO, unitType.getBaseRate(), domainId, 1);
            formulaDTO.setDomainId(domainId);
            formulaDTO.setTemplateBits(Long.valueOf(Integer.parseInt(formulaDTO.getTemplateBitString(), 2)));
            Formula formula = formulaRepository.save(formulaMapper.toEntity(formulaDTO));
            rateTable.setDefaultFormulaId(formula.getId());
        }
        rateTable = rateTableRepository.save(rateTable);

        if (CollectionUtils.isEmpty(rateTableDTO.getColumns()) && !CollectionUtils.isEmpty(rateTableDTO.getRows())) {
            rateTableDTO.setRows(new ArrayList<>());
        }
        // validate và save rate_table_columns
        validateRateTableColumns(rateTableDTO.getColumns());
        for (RateTableColumnDTO column : rateTableDTO.getColumns()) {
            RateTableColumn rateTableColumn = rateTableColumnMapper.toEntity(column);
            rateTableColumn.setDomainId(domainId);
            rateTableColumn.setRateTableId(rateTable.getId());
            rateTableColumnsRepository.save(rateTableColumn);
        }

        // validate và save rate_table_rows, bao gồm cả validate formula, combinedValue, check isDefault
        // cho từng row
        validateRateTableRows(rateTableDTO.getRows(), rateTableDTO.getColumns(), rateTableDTO.getDefaultFormula(), rateTableDTO.getDefaultFormulaId(), 1, domainId, 1);

        for (RateTableRowDTO row : rateTableDTO.getRows()) {
            RateTableRow rateTableRow = rateTableRowsMapper.toEntity(row);
            rateTableRow.setRateTableId(rateTable.getId());
            rateTableRow.setFormulaId(getFormulaIdAndSaveFormula(row.getFormula(), row.getFormulaId(), domainId, unitType.getBaseRate(), 1));
            rateTableRow.setDomainId(domainId);
            rateTableRow.setCombinedValue(row.getCombinedValue());
            rateTableRowsRepository.save(rateTableRow);
            if (row.isDefault()) {
                rateTable.setDefaultCombinedValue(row.getCombinedValue());
                rateTable = rateTableRepository.save(rateTable);
            }
        }
        RateTableDTO rateTableDtoRes = rateTableMapper.toDto(rateTable);

        if (!rateTableDTO.getColumns().isEmpty()) {
            rateTableDtoRes.setHasChild(true);
        }

        return rateTableDtoRes;
    }

    private void validateFormula(FormulaDTO formulaDTO, Integer newBaseRate, Integer domainId, Integer oldBaseRate) {
        HashMap<String, Boolean> propertiesOfFormula = new HashMap<>();
        mappingPropertyByTempBits(formulaDTO.getTemplateBitString(), propertiesOfFormula);
        validateFormulaType(propertiesOfFormula.get(FORMULA_TYPE), formulaDTO.getFormulaType(), domainId);

        boolean isTypeNormal =
            !propertiesOfFormula.get(FORMULA_TYPE) && FormulaType.NORMAL.equals(FormulaType.of(Math.toIntExact(formulaDTO.getFormulaType())));
        boolean isTypeCharacteristic = propertiesOfFormula.get(FORMULA_TYPE);

        if (formulaDTO.getA() != null) {
            validateA(propertiesOfFormula.get(A), formulaDTO.getA(), domainId);
        }

        if (!propertiesOfFormula.get(A) && formulaDTO.getA() != null && (isTypeNormal || isTypeCharacteristic)) {
            BigInteger f = new BigInteger("1");
            f = f.multiply((BigInteger.valueOf(formulaDTO.getA()).divide(BigInteger.valueOf(oldBaseRate.longValue())).multiply(BigInteger.valueOf(newBaseRate.longValue()))));
            if (f.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
                throw new DataInvalidException(ErrorMessage.Formula.OVER_FLOW, Resources.FORMULA, ErrorKey.Formula.A);
            }
            formulaDTO.setA((formulaDTO.getA() / oldBaseRate.longValue()) * newBaseRate.longValue());
        }
        if (formulaDTO.getB() != null) {
            validateB(propertiesOfFormula.get(B), propertiesOfFormula.get(FORMULA_TYPE), formulaDTO, domainId);
        }
        if (!propertiesOfFormula.get(B) && formulaDTO.getB() != null && (isTypeNormal || isTypeCharacteristic)) {
            BigInteger f = new BigInteger("1");
            f = f.multiply((BigInteger.valueOf(formulaDTO.getB()).divide(BigInteger.valueOf(oldBaseRate.longValue())).multiply(BigInteger.valueOf(newBaseRate.longValue()))));

            if (f.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
                throw new DataInvalidException(ErrorMessage.Formula.OVER_FLOW, Resources.FORMULA, ErrorKey.Formula.B);
            }
            formulaDTO.setB((formulaDTO.getB() / oldBaseRate) * newBaseRate);
        }

        if (formulaDTO.getPer() != null) {
            validatePer(propertiesOfFormula.get(PER), formulaDTO.getPer(), domainId);
        }
        if (formulaDTO.getInputA() != null) {
            validateInput(propertiesOfFormula.get(INPUT_A), formulaDTO.getInputA(), ErrorKey.Formula.INPUT_A);

        }
        if (formulaDTO.getInputB() != null) {
            validateInput(propertiesOfFormula.get(INPUT_B), formulaDTO.getInputB(), ErrorKey.Formula.INPUT_B);
        }

        if (formulaDTO.getFormulaErrorCode() != null) {
            validateErrorCode(formulaDTO.getFormulaErrorCode());
        }

        if (formulaDTO.getConverterType() != null) {
            validateConverterType(formulaDTO.getConverterType());
        }

        if (!formulaDTO.getStatisticItemString().isEmpty() && formulaDTO.getStatisticItemString() != null) {
            validateStaticItem(formulaDTO.getStatisticItemString());
        }
    }

    private void validateFormulaType(Boolean isChecked, Long formulaTypeId, Integer domainId) {
        if (!isChecked) {
            try {
                FormulaType.of(Math.toIntExact(formulaTypeId));
                return;
            } catch (IllegalArgumentException e) {
                throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.Formula.FORMULA_TYPE);
            }
        }
        isExistCharacteristic(formulaTypeId, domainId, ErrorKey.Formula.FORMULA_TYPE);
    }

    private void isExistCharacteristic(Long formulaType, Integer domainId, String field) {
        Characteristic characteristic = characteristicRepository.findByIdAndDomainId(formulaType, domainId);
        if (characteristic == null) {
            throw new DataInvalidException(ErrorMessage.Formula.CHAR_NOT_FOUND, Resources.FORMULA, field);
        }
    }

    private void validateA(Boolean isChecked, Long a, Integer domainId) {
        if (isChecked) {
            isExistCharacteristic(a, domainId, ErrorKey.Formula.A);
        }
    }

    private void validateB(Boolean isChecked, Boolean isFormulaTypeChecked, FormulaDTO formulaDTO, Integer domainId) {
        if (!isChecked && !isFormulaTypeChecked) {
            FormulaType formulaType = FormulaType.of(Math.toIntExact(formulaDTO.getFormulaType()));
            if (formulaType.equals(FormulaType.ACTION_PRIORITY) && formulaDTO.getB() < -1L) {
                throw new DataInvalidException(ErrorMessage.Formula.B_NOT_FOUND, Resources.FORMULA,
                    ErrorKey.Formula.B);
            }
        }
        if (isChecked) {
            isExistCharacteristic(formulaDTO.getB(), domainId, ErrorKey.Formula.B);
        }
    }

    private void validatePer(Boolean isChecked, Long per, Integer domainId) {
        if (isChecked) {
            isExistCharacteristic(per, domainId, ErrorKey.Formula.PER);
        }
    }

    private void validateInput(Boolean isChecked, String inputA, String errorKey) {
        if (!isChecked && StringUtils.isEmpty(inputA)) {
            throw new DataInvalidException(ErrorMessage.Formula.INVALID_VALUE, Resources.FORMULA, errorKey);
        }
        if (isChecked) {
            if (StringUtils.isEmpty(inputA)) {
                throw new DataInvalidException(ErrorMessage.Formula.A_NOT_FOUND, Resources.FORMULA, errorKey);
            }
            String[] filterPaths = inputA.split(PATH_SPLITTER);
            List<NestedObject> paths = new ArrayList<>();

            Long parentId = null;
            int index = 0;
            for (String filterPath : filterPaths) {
                String path = filterPath;
                if (index++ == 0) {
                    parentId = 100L;
                }

                if (filterPath.contains(BEGIN_FILTER) || filterPath.contains(END_FILTER)) {
                    NormFilterValidator normFilterValidator = new NormFilterValidator();

                    int beginFilter = filterPath.indexOf("{");
                    int endFilter = filterPath.indexOf("}");

                    if (beginFilter == -1 || endFilter == -1) {
                        throw new DataInvalidException(ErrorMessage.Normalizer.INVALID_INPUT_FIELD, Resources.NORMALIZER, ErrorKey.Normalizer.INPUT_FIELDS);
                    }
                    path = filterPath.split(BEGIN_FILTER)[0];
                    NestedObject nestedObject = validatePath(path, parentId);
                    parentId = nestedObject.getObjClassId();
                    paths.add(nestedObject);
                    String filterAndPreFunc = filterPath.split(BEGIN_FILTER)[1];
                    // remove end of filter
                    filterAndPreFunc = filterAndPreFunc.substring(0, filterAndPreFunc.length() - 1);

                    String[] arrFilterAndPreFunc = filterAndPreFunc.split(FILTER_FUNCTION_SPLITTER);

                    String filters = arrFilterAndPreFunc[0];
                    String preFunction = (arrFilterAndPreFunc.length == 2) ? arrFilterAndPreFunc[1] : "";

                    List<FilterDTO> filterDTOs = new ArrayList<>();
                    if (!StringUtils.isEmpty(filters)) {
                        filterDTOs = toFilterDTOs(filters, parentId);
                    }
                    List<PreFunctionDTO> preFunctionDTOs = new ArrayList<>();
                    if (!StringUtils.isEmpty(preFunction)) {
                        preFunctionDTOs = toPreFunctions(preFunction);
                    }
                    normFilterValidator.setFilters(filterDTOs);
                    normFilterValidator.setPreFunctions(preFunctionDTOs);
                } else {
                    NestedObject nestedObject = validatePath(path, parentId);
                    parentId = nestedObject.getObjClassId();
                }

            }
        }
    }

    private void checkDuplicateFilterName(List<FilterDTO> filters) {
        List<String> filterNames = filters.stream().map(i -> i.getKey()).collect(Collectors.toList());
        for(String filterName : filterNames) {
            int count = Collections.frequency(filterNames, filterName);
            if (count >= 2) {
                throw new ResourceNotFoundException(ErrorMessage.Normalizer.DUPLICATE_FILTER_NAME, Resources.NESTED_OBJECT,
                    ErrorKey.Normalizer.INPUT_FIELDS);
            }
        }
    }

    private List<PreFunction> validatePreFunction(String input) {
        List<PreFunction> preFunctions = preFunctionRepository.findByFunctionName(input);
        if (CollectionUtils.isEmpty(preFunctions)) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
        }
        return preFunctions;
//        for (PreFunction preFunction : preFunctions) {
//            if (preFunction.getFunctionName().split(BEGIN_FUNCTION)[0].equals(input) ){
//                return preFunction;
//            }
//        }
//        throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
    }

    private List<PreFunctionDTO> toPreFunctions(String input) {
        List<PreFunctionDTO> results = new ArrayList<>();
        String[] functions = input.split(PRE_FUNCTION_SPLITTER);
        for (String function : functions) {
            results.add(toPreFunction(function));
        }
        return results;
    }

    private PreFunctionDTO toPreFunction(String input) {
        String[] functionWithParam = input.split(BEGIN_FUNCTION);
        String functionName = functionWithParam[0];
        String paramString = input.substring(functionName.length() + 1, input.length() - 1);
        List<String> params = new ArrayList<>();
        if (!StringUtils.isEmpty(paramString)) {
            String[] splitParams = paramString.split(PARAMETER_SPLITTER);
            params = Arrays.asList(splitParams);
        }
        PreFunctionDTO preFunctionDTO = new PreFunctionDTO(functionName, params, params.size());
        List<PreFunction> preFunctions = validatePreFunction(functionName);

        boolean found = false;
        for (PreFunction preFunction : preFunctions) {
            if (preFunctionDTO.getNumberParam().equals(preFunction.getNumberParam())) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new ResourceNotFoundException(ErrorMessage.Normalizer.INVALID_INPUT_FIELD, Resources.NESTED_OBJECT,
                ErrorKey.Normalizer.INPUT_FIELDS);
        }
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PreFunctionDTO>> violations = validator.validate(preFunctionDTO);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }
        return preFunctionDTO;
    }

    private NestedObject validateFilter(String input, Long parentId) {
        Optional<NestedObject> nestedObjects = nestedObjectRepository.findByNameAndParentClassIdAndAlistIsFalse(input, parentId);
        if (!nestedObjects.isPresent()) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
        }
        return nestedObjects.get();
    }

    private FilterDTO toFilterDTO(String input, Long parentId) {
        String[] keyValue = input.split(FILTER_KEY_VALUE);
        if (keyValue.length != 2) {
            throw new ResourceNotFoundException(ErrorMessage.Normalizer.FILTER_VALUE_NOT_EXIST, Resources.NESTED_OBJECT,
                ErrorKey.Normalizer.INPUT_FIELDS);
        }
        FilterDTO filterDTO = new FilterDTO(keyValue[0], keyValue[1].trim());
        validateFilter(filterDTO.getKey(), parentId);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<FilterDTO>> violations = validator.validate(filterDTO);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }

        return filterDTO;
    }

    private List<FilterDTO> toFilterDTOs(String input, Long parentId) {
        List<FilterDTO> results = new ArrayList<>();
        String[] filters = input.split(FILTER_SPLITTER);
        for (String filter : filters) {
            results.add(toFilterDTO(filter, parentId));
        }
        checkDuplicateFilterName(results);
        return results;
    }

    private NestedObject validatePath(String input, Long parentId) {
        Optional<NestedObject> nestedObjects = nestedObjectRepository.findByNameAndParentClassId(input, parentId);
        if (!nestedObjects.isPresent()) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
        }
        return nestedObjects.get();
    }

    private void validateErrorCode(Long errorCode) {
        Optional<FormulaErrorCode> formulaErrorCode = formulaErrorCodeRepository.findById(errorCode);
        if (!formulaErrorCode.isPresent()) {
            throw new DataInvalidException(ErrorMessage.Formula.FORMULA_ERROR_CODE_NOT_FOUND, Resources.FORMULA, ErrorKey.Formula.ERROR_CODE);
        }
    }

    private void validateConverterType(Long convertType) {
        Optional<FormulaUnitConvert> formulaUnitConvert = formulaUnitConvertRepository.findById(convertType);
        if (!formulaUnitConvert.isPresent()) {
            throw new DataInvalidException(ErrorMessage.Formula.FORMULA_UNIT_CONVERT_NOT_FOUND, Resources.FORMULA, ErrorKey.Formula.CONVERT_TYPE);
        }
    }

    private void validateStaticItem(String staticItemString) {
        String[] staticItems = staticItemString.split(";");
        for (String staticItem : staticItems) {
            Optional<StatisticItem> statisticItem = statisticItemRepository.findById(Long.parseLong(staticItem));
            if (!statisticItem.isPresent()) {
                throw new DataInvalidException(ErrorMessage.Formula.STATISTIC_ITEM_NOT_FOUND, Resources.FORMULA, ErrorKey.Formula.STATIC_ITEM);
            }
        }
    }

    private void validateRateTableColumns(List<RateTableColumnDTO> columns) {
        List<Long> normalizerIds =
            columns.stream().map(RateTableColumnDTO::getNormalizerId).collect(Collectors.toList());
        Optional<Long> duplicateId =
            normalizerIds.stream().filter(i -> Collections.frequency(normalizerIds, i) > 1).findFirst();
        if (duplicateId.isPresent()) {
            throw new DataInvalidException(ErrorMessage.RateTable.COLUMNS_NORMALIZER, Resources.RATE_TABLE,
                ErrorKey.RateTable.COLUMNS);
        }
        // validate từng column
        for (RateTableColumnDTO column : columns) {
            validateRateTableColumn(column);
        }
    }

    private void validateRateTableColumn(RateTableColumnDTO column) {
        Optional<Normalizer> normalizer = normalizerRepository.findById(column.getNormalizerId());
        if (!normalizer.isPresent()) {
            throw new DataInvalidException(ErrorMessage.RateTable.DUPLICATE_NORMALIZER, Resources.RATE_TABLE,
                ErrorKey.RateTable.COLUMNS);
        }
    }

    private void validateRateTableRows(List<RateTableRowDTO> rows, List<RateTableColumnDTO> columns,
                                       FormulaDTO defaultFormula, Long defaultFormulaId, Integer newBaseRate,
                                       Integer domainId, Integer oldBaseRate) {

        if (CollectionUtils.isEmpty(rows)) {
            if (defaultFormula == null && defaultFormulaId == null) {
                throw new DataInvalidException(ErrorMessage.Formula.MUST_NOT_BE_NULL, Resources.RATE_TABLE,
                    ErrorKey.RateTable.DEFAULT_FORMULA);
            }
        } else {
            List<RateTableRowDTO> rowDefault =
                rows.stream().filter(RateTableRowDTO::isDefault).collect(Collectors.toList());

            if (rowDefault.size() != 1) {
                throw new DataInvalidException(ErrorMessage.RateTable.DEFAULT_ROW_INVALID, Resources.RATE_TABLE,
                    ErrorKey.RateTable.ROW);
            }

            if ((rowDefault.get(0).getFormula() == null && rowDefault.get(0).getFormulaId() == null) && defaultFormula == null && defaultFormulaId == null) {
                throw new DataInvalidException(ErrorMessage.Formula.MUST_NOT_BE_NULL, Resources.RATE_TABLE,
                    ErrorKey.RateTable.DEFAULT_FORMULA);
            }

            List<String> combinedValues =
                rows.stream().map(i -> i.getCombinedValue()).collect(Collectors.toList());
            Optional<String> duplicateCombinedValue =
                combinedValues.stream().filter(i -> Collections.frequency(combinedValues, i) > 1).findFirst();

            if (duplicateCombinedValue.isPresent()) {
                throw new DataInvalidException(ErrorMessage.RateTable.DUPLICATE_COMBINED_VALUE, Resources.RATE_TABLE,
                    ErrorKey.RateTable.ROW);
            }

            Integer index = 0;
            for (RateTableRowDTO row : rows) {
                validateRateTableRow(row, columns, defaultFormula, newBaseRate, domainId, defaultFormulaId,
                    oldBaseRate, index++);
            }
        }
    }

    private void validateRateTableRow(RateTableRowDTO row, List<RateTableColumnDTO> columns, FormulaDTO defaultFormula,
                                      Integer newBaseRate, Integer domainId, Long defaultFormulaId,
                                      Integer oldBaseRate, Integer indexRow) {
        if (row.getFormula() == null && row.getFormulaId() == null && defaultFormula == null && row.isDefault() && defaultFormulaId == null) {
            throw new DataInvalidException(ErrorMessage.RateTable.DEFAULT_ROW_INVALID, Resources.RATE_TABLE,
                ErrorKey.RateTable.DEFAULT_FORMULA);
        }
        assert row.getFormula() != null;
        if (row.getFormula() != null) {
            validateFormula(row.getFormula(), newBaseRate, domainId, oldBaseRate);
        }
        String[] combinedValues = row.getCombinedValue().split("/");
        if (combinedValues.length != columns.size()) {
            throw new DataInvalidException(ErrorMessage.RateTable.INVALID_COMBINED_VALUE, Resources.RATE_TABLE,
                ErrorKey.RateTable.ROW + "[" + indexRow + "]." + ErrorKey.RateTable.COMBINED_VALUE);
        }
        int index = 1;
        for (String combinedValue : combinedValues) {
            int finalIndex = index++;
            Optional<RateTableColumnDTO> column =
                columns.stream().filter(i -> i.getColumnIndex() == finalIndex).findFirst();
            if (!column.isPresent()) {
                throw new DataInvalidException(ErrorMessage.RateTable.INVALID_COLUMN_INDEX, Resources.RATE_TABLE,
                    ErrorKey.RateTable.COLUMNS);
            }
            Normalizer normalizer = normalizerRepository.findByIdAndDomainId(column.get().getNormalizerId(),domainId);
            if(normalizer == null){
                throw new ResourceNotFoundException(ErrorMessage.Normalizer.NOT_FOUND,Resources.NORMALIZER,ErrorKey.ENTITY_NOT_FOUND);
            }
            List<NormalizerValue> normalizerValues =
                normValueRepository.findAllByNormalizerId(column.get().getNormalizerId(), domainId);
            Optional<NormalizerValue> normalizerValue =
                normalizerValues.stream().filter(i -> i.getValueId() == Integer.parseInt(combinedValue)).findFirst();

            if (!normalizerValue.isPresent()) {
                throw new DataInvalidException(ErrorMessage.RateTable.INVALID_COMBINED_VALUE , Resources.RATE_TABLE,  ErrorKey.RateTable.ROW);
            }
        }
    }

    public String getDefaultFormula(Long formulaId, Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
        Formula formula = formulaRepository.findByIdAndDomainId(formulaId, domainId);
        if (formula == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.RateTable.FORMULA);
        }
        FormulaDTO formulaDTO = formulaMapper.toDto(formula);
        formulaDTO.setTemplateBitString(Integer.toBinaryString(formula.getTemplateBits().intValue()));
        if (formulaDTO.getTemplateBitString().length() < 6) {
            for (int i = formulaDTO.getTemplateBitString().length(); i < 6; i++) {
                formulaDTO.setTemplateBitString("0" + formulaDTO.getTemplateBitString());
            }
        }
        RateTable rateTable = rateTableRepository.findByIdAndDomainId(rateTableId, domainId);
        Integer baseRate = unitTypeRepository.findByIdAndDomainId(rateTable.getUnitTypeId(), domainId).getBaseRate();
        return convertDefaultFormula(formulaDTO, baseRate);
    }

    public List<DefaultFormula> getDefaultFormulas(List<Long> formulaIds, Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
        List<DefaultFormula> defaultFormulas = new ArrayList<>();

        List<Formula> formulas = formulaRepository.findByIdInAndDomainId(formulaIds, domainId);
        for (Formula formula : formulas) {
            if (formula == null) {
                throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.RateTable.FORMULA);
            }
            FormulaDTO formulaDTO = formulaMapper.toDto(formula);
            formulaDTO.setTemplateBitString(Integer.toBinaryString(formula.getTemplateBits().intValue()));
            if (formulaDTO.getTemplateBitString().length() < 6) {
                for (int i = formulaDTO.getTemplateBitString().length(); i < 6; i++) {
                    formulaDTO.setTemplateBitString("0" + formulaDTO.getTemplateBitString());
                }
            }
            RateTable rateTable = rateTableRepository.findByIdAndDomainId(rateTableId, domainId);
            Integer baseRate = unitTypeRepository.findByIdAndDomainId(rateTable.getUnitTypeId(), domainId).getBaseRate();
            String s = convertDefaultFormula(formulaDTO, baseRate);
            DefaultFormula defaultFormula = new DefaultFormula();
            defaultFormula.setId(formulaDTO.getId());
            defaultFormula.setDefaultFormula(s);
            defaultFormulas.add(defaultFormula);
        }
        return defaultFormulas;
    }

    public String convertDefaultFormula(FormulaDTO formula, Integer baseRate) {
        Integer domainId = OCSUtils.getDomain();
        String defaultFormula = null;
//        String binary = Integer.toBinaryString(formula.getTemplateBits().intValue());
        String binary = formula.getTemplateBitString();
        HashMap<String, String> propertiesOfFormula = calculateFormula(binary, formula, domainId);


        if (REFER_TABLE.equals(propertiesOfFormula.get(FORMULA_TYPE))) {
            FormulaType formulaType = FormulaType.of(formula.getFormulaType().intValue());
            Long a = null;
            Long b = null;
            switch (formulaType) {
                case NORMAL:
                    a = checkNumeric(propertiesOfFormula.get(A),binary.charAt(3));
                    b = checkNumeric(propertiesOfFormula.get(B),binary.charAt(4));
                    String stringA = convertStringValue(a, baseRate, propertiesOfFormula, A);
                    String stringB = convertStringValue(b, baseRate, propertiesOfFormula, B);
                    defaultFormula = stringA + "x+" + stringB + "/" + propertiesOfFormula.get(PER);
                    defaultFormula = getConvertTypeAndErrorCode(formula, domainId, defaultFormula);
                    break;

                case SKIP:
                    defaultFormula = "SKIP (MOVE RT)";
                    defaultFormula = getConvertTypeAndErrorCode(formula, domainId, defaultFormula);
                    break;

                case DENY:
                    defaultFormula = "DENY (MOVE PC)";
                    defaultFormula = getConvertTypeAndErrorCode(formula, domainId, defaultFormula);
                    break;

                case EXIT:
                    defaultFormula = "EXIT (MOVE ACTION)";
                    defaultFormula = getConvertTypeAndErrorCode(formula, domainId, defaultFormula);
                    break;

                case SORT_PC:
                    try {
                        b = Long.valueOf(propertiesOfFormula.get(B));
                        defaultFormula = "SORTING - " + referTableRepository
                            .getSortNameOfFormula(b.intValue(), ReferType.SortingType);
                    } catch (Exception e) {
                        defaultFormula = "SORTING - " + propertiesOfFormula.get(B);
                    }

                    break;

                case DYNAMIC_RESERVE:
                    defaultFormula = "RESERVE INFO: MinReserve = " + propertiesOfFormula.get(A) + "; MaxResrve = "
                        + propertiesOfFormula.get(B) + ", UsageQuotaThreshold = " + propertiesOfFormula.get(PER);
                    defaultFormula = getConvertTypeAndErrorCode(formula, domainId, defaultFormula);
                    break;

                case BLOCK_FILTER:
                    try {
                        if (Integer.parseInt(propertiesOfFormula.get(B)) == 1) {
                            defaultFormula = "BLOCK FILTER: 1-ADD BLOCK";
                        } else {
                            defaultFormula = "BLOCK FILTER: #1-REMOVE BLOCK";
                        }
                    } catch (NumberFormatException e) {
                        defaultFormula = "BLOCK FILTER: " + propertiesOfFormula.get(B);
                    }
                    break;

                case ACTION_PRIORITY:
                    try {
                        if (Integer.parseInt(propertiesOfFormula.get(B)) == -1) {
                            defaultFormula = "ACTION PRIORITY = -1- REMOVE ACTION";
                        }
                        if (Integer.parseInt(propertiesOfFormula.get(B)) == 0) {
                            defaultFormula = "ACTION PRIORITY = 0- UNCHANGE PRIORITY";
                        }
                        if (Integer.parseInt(propertiesOfFormula.get(B)) > 0) {
                            defaultFormula = "ACTION PRIORITY = " + propertiesOfFormula.get(B);
                        }
                    } catch (NumberFormatException e) {
                        defaultFormula = "ACTION PRIOTIRY = " + propertiesOfFormula.get(B);
                    }
                    break;

                case EVENT_ANALYSIS:
                    try {
                        defaultFormula = "ACTION TYPE = " + propertiesOfFormula.get(B) + "-" + actionTypeRepository
                            .getActionTypeOfFormula(Long.valueOf(propertiesOfFormula.get(B)), domainId);
                    } catch (NumberFormatException e) {
                        defaultFormula = "ACTION TYPE = " + propertiesOfFormula.get(B);
                    }
            }
        } else {
            Long a = checkNumeric(propertiesOfFormula.get(A),binary.charAt(3));
            Long b = checkNumeric(propertiesOfFormula.get(B),binary.charAt(4));
            String stringA = convertStringValue(a, baseRate, propertiesOfFormula, A);
            String stringB = convertStringValue(b, baseRate, propertiesOfFormula, B);
            defaultFormula = characteristicRepository.getCharName(domainId, Long.valueOf(formula.getFormulaType()))
                + "; " + stringA + "x+" + stringB + "/" + propertiesOfFormula.get(PER);
            defaultFormula = getConvertTypeAndErrorCode(formula, domainId, defaultFormula);
        }
        if(!StringUtils.isEmpty(formula.getStatisticItemString())){
            String[] statisticItems = formula.getStatisticItemString().split(";");
            List<Long> statisticItemIds = new ArrayList<>();
            for (String statisticItem : statisticItems) {
                statisticItemIds.add(Long.parseLong(statisticItem));
            }
            List<StatisticItem> items = statisticItemRepository.findByDomainIdAndIdIn(domainId,statisticItemIds);
            if(items.size() < statisticItems.length){
                throw new ResourceNotFoundException(ErrorMessage.StatisticItem.NOT_FOUND,Resources.STATISTIC,ErrorKey.ENTITY_NOT_FOUND);
            }
        }

        return defaultFormula;
    }

    // Convert template bit của formula sang dạng binary và trả về hashmap value của
    // A, B và Per trong formula
    private HashMap<String, String> calculateFormula(String binary, FormulaDTO formula, Integer domainId) {
        HashMap<String, Boolean> propertiesOfFormula = new HashMap<>();
        mappingPropertyByTempBits(binary, propertiesOfFormula);
        HashMap<String, String> valueOfProperties = new HashMap<>();
        // Nếu input A được cấu hình thì value của A trong hasmap value sẽ là giá trị
        // của input A
        if (formula.getInputA() != null) {
            valueOfProperties.put(A, formula.getInputA());
        } else {
            if (propertiesOfFormula.get(A)) {
                String charNameA = characteristicRepository.getCharName(domainId, formula.getA());
                if(charNameA == null){
                    throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND,Resources.CHARACTERISTIC,ErrorKey.ENTITY_NOT_FOUND);
                }
                valueOfProperties.put(A, charNameA);
            } else {
                valueOfProperties.put(A, formula.getA() == null ? null : formula.getA().toString());
            }
        }

        // Nếu input A được cấu hình thì value của B trong hasmap value sẽ là giá trị
        // của input B
        if (formula.getInputB() != null) {
            valueOfProperties.put(B, formula.getInputB());
        } else {
            if (propertiesOfFormula.get(B)) {
                String charNameB = characteristicRepository.getCharName(domainId, formula.getB());
                if(charNameB == null){
                    throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND,Resources.CHARACTERISTIC,ErrorKey.ENTITY_NOT_FOUND);
                }
                valueOfProperties.put(B, charNameB);
            } else {
                valueOfProperties.put(B, formula.getB() == null ? null : formula.getB().toString());
            }
        }
        if (propertiesOfFormula.get(PER)) {
            String charNamePer = characteristicRepository.getCharName(domainId, formula.getPer());
            if(charNamePer == null){
                throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND,Resources.CHARACTERISTIC,ErrorKey.ENTITY_NOT_FOUND);
            }
            valueOfProperties.put(PER, charNamePer);
        } else {
            valueOfProperties.put(PER, formula.getPer() == null ? null : formula.getPer().toString());
        }

        if (propertiesOfFormula.get(FORMULA_TYPE)) {
            valueOfProperties.put(FORMULA_TYPE, CHARACTERISTIC);
        } else {
            valueOfProperties.put(FORMULA_TYPE, REFER_TABLE);
        }
        return valueOfProperties;
    }

    public void mappingPropertyByTempBits(String binary, HashMap<String, Boolean> propertiesOfFormula) {
        List<String> propertiesName = new ArrayList<>(List.of(INPUT_B, INPUT_A, FORMULA_TYPE, A, B, PER));
        for (String name : propertiesName) {
            propertiesOfFormula.put(name, false);
        }
        int index = propertiesName.size() - 1;
        for (int i = binary.length() - 1; i >= 0; i--) {
            if ("1".equals(Character.toString(binary.charAt(i)))) {
                propertiesOfFormula.put(propertiesName.get(index), true);
            }
            index--;
        }
    }

    // Lấy giá trị của convert type và error code cho formula
    private String getConvertTypeAndErrorCode(FormulaDTO formula, Integer domainId, String defaultFormula) {
        if (formula.getConverterType() != null) {
            defaultFormula = defaultFormula + "; "
                + formulaUnitConvertRepository.getConvertTypeOfFormula(formula.getConverterType(), domainId);
        }

        if (formula.getFormulaErrorCode() != null) {
            defaultFormula = defaultFormula + "; error = "
                + formulaErrorCodeRepository.getErrorCodeOfFormula(formula.getFormulaErrorCode(), domainId);
        }
        return defaultFormula;
    }

    public FormulaDTO getDetailFormula(Long formulaId) {
        Integer domainId = OCSUtils.getDomain();
        Formula formula = formulaRepository.findByIdAndDomainId(formulaId, domainId);
        FormulaDTO formulaDTO = formulaMapper.toDto(formula);
        formulaDTO.setTemplateBitString(Integer.toBinaryString(formula.getTemplateBits().intValue()));
        if (formulaDTO.getTemplateBitString().length() < 6) {
            for (int i = formulaDTO.getTemplateBitString().length(); i < 6; i++) {
                formulaDTO.setTemplateBitString("0" + formulaDTO.getTemplateBitString());
            }
        }
        if (formula.getStatisticItems() != null && !(formula.getStatisticItems()).equals("")) {
            List<Long> statisticItemIds = getStatisticItemIds(formula.getStatisticItems());
            List<StatisticItemDTO> statisticItemDTOs = new ArrayList<>();
            for (Long statisticItemId : statisticItemIds) {
                StatisticItemDTO dto = statisticItemMapper.toDto(statisticItemRepository.findByIdAndDomainId(statisticItemId, domainId));
                statisticItemDTOs.add(dto);
            }
            formulaDTO.setStatisticItems(statisticItemDTOs);
        }
        String binary = formulaDTO.getTemplateBitString();
        HashMap<String, Boolean> propertiesOfFormula = new HashMap<>();
        mappingPropertyByTempBits(binary, propertiesOfFormula);
        if (propertiesOfFormula.get(A)) {
            formulaDTO.setCharA(characteristicRepository.getCharName(domainId, formula.getA()));
            formulaDTO.setCharAId(formula.getA());
        }
        if (propertiesOfFormula.get(B)) {
            formulaDTO.setCharB(characteristicRepository.getCharName(domainId, formula.getB()));
            formulaDTO.setCharBId(formula.getB());
        }
        if (propertiesOfFormula.get(PER)) {
            formulaDTO.setCharPer(characteristicRepository.getCharName(domainId, formula.getPer()));
            formulaDTO.setCharPerId(formula.getPer());
        }
        if (propertiesOfFormula.get(FORMULA_TYPE)) {
            formulaDTO.setCharFormulaType(characteristicRepository.getCharName(domainId, formula.getFormulaType()));
            formulaDTO.setCharFormulaTypeId(formula.getFormulaType());
        }
        return formulaDTO;
    }

    private List<Long> getStatisticItemIds(String statisticItems) {
        String[] valueIdStrings = statisticItems.split("\\;");
        List<Long> list = new ArrayList<>();
        for (String item : valueIdStrings) {
            list.add(Long.parseLong(item));
        }
        return list;
    }

    @Transactional
    public RateTableDTO updateRateTable(RateTableDTO rateTableDTO) {
        Integer domainId = OCSUtils.getDomain();
        // validate và save rate_table
        RateTable rateTableCurrent = rateTableRepository.findByIdAndDomainId(rateTableDTO.getId(), domainId);
        if (rateTableCurrent == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.Normalizer.ID);
        }
        RateTable rateTable = rateTableMapper.toEntity(rateTableDTO);
        if (!Objects.equal(rateTableDTO.getCategoryId(), rateTableCurrent.getCategoryId())) {
            setPosIndex(domainId, rateTable, rateTableDTO.getCategoryId());
        }
        if (!rateTableDTO.getName().equals(rateTableCurrent.getName())) {
            checkDuplicateName(rateTable.getName(), rateTable.getId());
        }
        checkCategory(rateTableDTO.getCategoryId());
        UnitTypeDTO unitTypeDTO = unitTypeService.getUnitTypeDetail(rateTable.getUnitTypeId());
        UnitTypeDTO unitTypeCurrent = unitTypeService.getUnitTypeDetail(rateTableCurrent.getUnitTypeId());

        if (ActionType.DELETE.equals(rateTableDTO.getActionTypeDefaultFormula())) {
            formulaRepository.deleteById(rateTableDTO.getDefaultFormulaId());
            //Do là trường hợp delete nên cần chuyền oldBaseRate cho Formula là 1 để có thể tạo mới Formula(nếu có)
            rateTable.setDefaultFormulaId(getFormulaIdAndSaveFormula(rateTableDTO.getDefaultFormula(), null, domainId, unitTypeDTO.getBaseRate(), 1));
        } else {
            if (!unitTypeDTO.getBaseRate().equals(unitTypeCurrent.getBaseRate()) && rateTableDTO.getDefaultFormula() == null && rateTableCurrent.getDefaultFormulaId() != null) {
                rateTableDTO.setDefaultFormula(getDetailFormula(rateTableCurrent.getDefaultFormulaId()));
                //Do đây là trường hợp thay đổi baseRate nên cần chuyển cả baseRate cũ và mới để tính toán lại A, B của formula
                rateTable.setDefaultFormulaId(getFormulaIdAndSaveFormula(rateTableDTO.getDefaultFormula(), rateTableDTO.getDefaultFormulaId(), domainId, unitTypeDTO.getBaseRate(), unitTypeCurrent.getBaseRate()));
            } else {
                //Do là trường hợp baseRate không thay đổi nên cần chuyền oldBaseRate cho Formula là 1
                rateTable.setDefaultFormulaId(getFormulaIdAndSaveFormula(rateTableDTO.getDefaultFormula(), rateTableDTO.getDefaultFormulaId(), domainId, unitTypeDTO.getBaseRate(), 1));
            }

        }
        rateTable = rateTableRepository.save(rateTable);
        // update columns
        rateTableColumnService.updateColumns(rateTableDTO.getColumns(), rateTableDTO.getId());

        Integer countColumn = rateTableColumnsRepository.countByRateTableIdAndDomainId(rateTableDTO.getId(), domainId);
        if (countColumn == 0) {
            // xoa het tat ca cac rows
            List<RateTableRow> rows = rateTableRowsRepository.findByRateTableIdAndDomainId(rateTableDTO.getId(),
                domainId);
            for (RateTableRow item : rows) {
                rateTableRowService.deleteRow(item);
            }
            checkNullDefaultFormula(rateTable);
        } else {
            boolean isLstRowEmpty = false;
            if (rateTableDTO.getRows().isEmpty()) {
                List<RateTableRowDTO> lstRowNotUpdate = rateTableRowsMapper.toDto(rateTableRowsRepository
                    .findByRateTableIdAndDomainId(rateTableDTO.getId(), domainId));
                for (RateTableRowDTO rowNotUpdate : lstRowNotUpdate) {
                    if (rowNotUpdate.getCombinedValue().equals(rateTable.getDefaultCombinedValue())) {
                        rowNotUpdate.setDefault(true);
                    }
                }
                rateTableDTO.setRows(lstRowNotUpdate);
                isLstRowEmpty = true;
            }
            if (!rateTableDTO.getRows().isEmpty()) {
                // validate và save rate_table_rows, bao gồm cả validate formula, combinedValue,
                // check isDefault
                // cho từng row
                List<RateTableRowDTO> removeRows = new ArrayList<>();
                List<Long> lstRowUpdateId = new ArrayList<>();
                List<Long> lstColumnUpdateId = new ArrayList<>();
                for (RateTableRowDTO row : rateTableDTO.getRows()) {
                    if (ActionType.DELETE.equals(row.getActionType())) {
                        rateTableRowService.deleteRow(rateTableRowMapper.toEntity(row));
                        removeRows.add(row);
                    } else {
                        if (row.getId() != null) {
                            lstRowUpdateId.add(row.getId());
                        }
                        row.setRateTableId(rateTableDTO.getId());
                    }
                }
                for (RateTableColumnDTO column : rateTableDTO.getColumns()) {
                    lstColumnUpdateId.add(column.getId());
                }
                // truyển vào -1 cho list để hàm findByRateTableIdAndDomainIdAndIdNotIn sẽ query
                // where id not in (-1) là sẽ ra toàn bộ bản ghi trong DB
                // theo điều kiện còn lại
                if (CollectionUtils.isEmpty(lstRowUpdateId)) {
                    lstRowUpdateId.add(Long.valueOf(-1));
                }
                if (CollectionUtils.isEmpty(lstColumnUpdateId)) {
                    lstColumnUpdateId.add(Long.valueOf(-1));
                }
                rateTableDTO.getRows().removeAll(removeRows);
                List<RateTableRowDTO> lstRow = new ArrayList<>();
                lstRow.addAll(rateTableDTO.getRows());
                List<RateTableRowDTO> lstRowNotUpdate = new ArrayList<>();

                if (!isLstRowEmpty) {
                    lstRowNotUpdate = rateTableRowsMapper.toDto(rateTableRowsRepository
                        .findByRateTableIdAndDomainIdAndIdNotIn(rateTableDTO.getId(), domainId, lstRowUpdateId));
                    for (RateTableRowDTO rowNotUpdate : lstRowNotUpdate) {
                        if (rowNotUpdate.getCombinedValue().equals(rateTable.getDefaultCombinedValue())) {
                            rowNotUpdate.setDefault(true);
                        }
                    }
                    lstRow.addAll(lstRowNotUpdate);
                }

                List<RateTableColumnDTO> listColumnDto = rateTableColumnMapper.toDto(rateTableColumnsRepository
                    .findByRateTableIdAndDomainIdOrderByPosIndexAsc(rateTableDTO.getId(), domainId));

                //chuyền vào 2 baseRate là 1 để không thay đổi giá trị của A, B
                validateRateTableRows(lstRow, listColumnDto, rateTableDTO.getDefaultFormula(), rateTable.getDefaultFormulaId(),
                    1, domainId, 1);
                if (!unitTypeDTO.getBaseRate().equals(unitTypeCurrent.getBaseRate())) {
                    rateTableDTO.setRows(lstRow);
                }
                for (RateTableRowDTO row : rateTableDTO.getRows()) {
                    RateTableRow rateTableRow = rateTableRowsMapper.toEntity(row);
                    rateTableRow.setRateTableId(rateTable.getId());
                    if (ActionType.DELETE.equals(row.getActionTypeFormula())) {
                        formulaRepository.deleteById(row.getFormulaId());
                        rateTableRow.setFormulaId(getFormulaIdAndSaveFormula(row.getFormula(), null, domainId, unitTypeDTO.getBaseRate(), 1));
                        if (BooleanUtils.isTrue(row.isDefault()) && rateTable.getDefaultFormulaId() == null
                            && row.getFormula() == null && rateTableDTO.getDefaultFormula() == null) {
                            throw new DataInvalidException(ErrorMessage.Formula.MUST_NOT_BE_NULL, Resources.RATE_TABLE,
                                ErrorKey.RateTable.DEFAULT_FORMULA);
                        }
                    } else {
                        checkChangeBaseRate(unitTypeDTO.getBaseRate(), unitTypeCurrent.getBaseRate(), row, rateTableRow, domainId);
                    }
                    rateTableRow.setDomainId(domainId);
                    rateTableRowsRepository.save(rateTableRow);
                    if (row.isDefault()) {
                        rateTable.setDefaultCombinedValue(row.getCombinedValue());
                        rateTable = rateTableRepository.save(rateTable);
                    }
                }
            } else {
                checkNullDefaultFormula(rateTable);
            }

        }
        RateTableDTO dto = getRateTableDetail(rateTableDTO.getId());
        if (!dto.getColumns().isEmpty()) {
            dto.setHasChild(true);
        }
        rateTableRowService.getAllRowOfRateTable(dto);
        if (!dto.getColumns().isEmpty()) {
            dto.setHasChild(true);
        }
        return dto;
    }

    private void checkNullDefaultFormula(RateTable rateTable) {
        if (rateTable.getDefaultFormulaId() == null) {
            throw new DataInvalidException(ErrorMessage.Formula.MUST_NOT_BE_NULL, Resources.RATE_TABLE,
                ErrorKey.RateTable.DEFAULT_FORMULA);
        }
    }

    private void checkChangeBaseRate(Integer newBaseRate, Integer oldBaseRate, RateTableRowDTO rateTableRowDTO, RateTableRow rateTableRow, Integer domainId) {
        if (!newBaseRate.equals(oldBaseRate) && rateTableRowDTO.getFormula() == null && rateTableRowDTO.getFormulaId() != null) {
            rateTableRowDTO.setFormula(getDetailFormula(rateTableRowDTO.getFormulaId()));
            //Do đây là trường hợp thay đổi baseRate nên cần chuyển cả baseRate cũ và mới để tính toán lại A, B của formula
            rateTableRow.setFormulaId(getFormulaIdAndSaveFormula(rateTableRowDTO.getFormula(), rateTableRowDTO.getFormulaId(), domainId, newBaseRate, oldBaseRate));
        } else {
            //Do là trường hợp baseRate không thay đổi nên cần chuyền oldBaseRate cho Formula là 1
            rateTableRow.setFormulaId(getFormulaIdAndSaveFormula(rateTableRowDTO.getFormula(), rateTableRowDTO.getFormulaId(), domainId, newBaseRate, 1));
        }
    }

    public Page<RateTableDTO> getRateTables(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<RateTableDTO> dtos = rateTableRepository.getRateTables(categoryId, name, description, domainId, pageable).map(rateTableMapper::toDto);
        List<Long> rateTableIds = dtos.toList().stream().map(RateTableDTO::getId).collect(Collectors.toList());
        Map<Long, Integer> childrenRateTableMap = rateTableColumnService.countChildrenByParentMap(rateTableIds, domainId);

        for (RateTableDTO item : dtos) {
            item.setHasChild(checkHasChildOfChildren(childrenRateTableMap, item.getId()));
        }
        return dtos;
    }

    public List<NormalizerDTO> getNormalizers(Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
        List<NormalizerDTO> dtos = normalizerMapper.toDto(rateTableColumnsRepository.getNormalizers(rateTableId, domainId));
        List<Integer> indexs = rateTableColumnsRepository.getColumnIndex(rateTableId, domainId);
        for (int i = 0; i < dtos.size(); i++) {
            dtos.get(i).setPosIndex(indexs.get(i));
            dtos.get(i).setTreeType(Resources.TreeType.SUB_TEMPLATE);
        }
        return dtos;
    }

    @Override
    public TreeClone getTreeDTO() {
        return new RateTableDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.RATE_TABLE;
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList(CategoryType.RATING_FILTER, CategoryType.BLOCK);
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.NORMALIZER);
    }

    public List<Long> getRemoveIds(Long ratingFilterId, Long blockId, Integer componentType) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> removeIds = new ArrayList<>();
        if (ratingFilterId != null) {
            removeIds = ratingFilterRateTableMapRepository.getRateTableIds(ratingFilterId, domainId);
        }
        if (blockId != null && componentType != null) {
            removeIds = blockRateTableMapRepository.getRateTableIds(blockId, domainId, componentType);
        }
        return removeIds;
    }

    private Long getFormulaIdAndSaveFormula(FormulaDTO formulaDTO, Long formulaId, Integer domainId,
                                            Integer newBaseRate, Integer oldBaseRate) {
        if (formulaDTO != null) {
            validateFormula(formulaDTO, newBaseRate, domainId, oldBaseRate);
            formulaDTO.setTemplateBits(
                Long.valueOf(Integer.parseInt(formulaDTO.getTemplateBitString(), 2)));
            formulaDTO.setDomainId(domainId);
            Formula formula = formulaRepository.save(formulaMapper.toEntity(formulaDTO));
            return formula.getId();
        } else {
            return formulaId;
        }
    }

    private Long checkNumeric(String a, char bit) {
        try {
            if (bit == '1') {
                return null;
            }
            Long b = Long.valueOf(a);
            return b;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private String convertStringValue(Long a, Integer baseRate, HashMap<String, String> propertiesOfFormula, String key) {
        String stringA = null;
        if (a != null) {
            stringA = String.valueOf(a / baseRate);
        } else {
            stringA = propertiesOfFormula.get(key);
            if (A.equals(key)) {
                stringA += " ";
            }

        }
        return stringA;
    }


    @Override
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> newIds = new ArrayList<>();
        List<CloneDTO> lstCloneDTos = new ArrayList<>();
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        Map<String, CloneDTO> newNameRateTableCloneMap = new HashMap<>();
        Map<Long, List<Long>> parentNewChildrenOldIdMap = new HashMap<>();
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        Map<Long, Long> newOldFormulaIdMap = new HashMap<>();

        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);
        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if(!java.util.Objects.equals(distinctIds.size(), baseToClones.size())){
            throwResourceNotFoundException();
        }

        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
            .collect(Collectors.toMap(OCSCloneableEntity::getId, Function.identity()));


        List<Long> categoryIds = baseToClones.stream().map(OCSCloneableEntity::getParentId).distinct().collect(Collectors.toList());
        List<CountChildrenByParentDTO> categoryPosIndex = ocsCloneableRepository.getMaxPosIndexChildByParentId(categoryIds,domainId);
        Map<Long,Integer> categoryPosIndexMap = categoryPosIndex.stream()
            .collect(Collectors.toMap(CountChildrenByParentDTO::getId,CountChildrenByParentDTO::getCountChild));

        List<Long> lstDefaultFormulaIds = baseToClones.stream().map(i -> {
            RateTable rateTable = (RateTable) i;
            return rateTable.getDefaultFormulaId();
        }).filter(i -> i != null).collect(Collectors.toList());

        List<Formula> lstDefaultFormula = formulaRepository.findByDomainIdAndIdIn(domainId, lstDefaultFormulaIds);
        Map<Long, Formula> defaultFormulaMap = lstDefaultFormula.stream().collect(Collectors.toMap(Formula::getId, Function.identity()));


        for (CloneDTO cloneDTO : cloneDTOs) {
            RateTable rateTable = (RateTable) baseToCloneMap.get(cloneDTO.getId());
            if(rateTable.getDefaultFormulaId() != null){
                Formula formulaToClone = defaultFormulaMap.get(rateTable.getDefaultFormulaId());
                Formula cloneFormula = SerializationUtils.clone(formulaToClone);
                if (cloneFormula != null) {
                    cloneFormula.setId(null);
                    cloneFormula = formulaRepository.saveAndFlush(cloneFormula);
                    newOldFormulaIdMap.put( cloneFormula.getId(), formulaToClone.getId());
                }
            }
        }
        List<OCSCloneableEntity> clonedEntityNames = new ArrayList<>();
        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveRateTableAndFormula(cloneDTO, baseToCloneMap, newOldFormulaIdMap, newNameRateTableCloneMap,categoryPosIndexMap, clonedEntityNames);
            clonedEntityNames.add(clonedEntity);
            clonedEntities.add(clonedEntity);
        }
        clonedEntities = ocsCloneableRepository.saveAll(clonedEntities);
        ocsCloneableRepository.flush();
        clonedEntities.forEach(i -> {
            CloneDTO cloneDTO = newNameRateTableCloneMap.get(i.getName());
            List<Long> childrenIds = new ArrayList<>();
            if (cloneDTO.getChildren() != null){
                childrenIds = cloneDTO.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }

            cloneDTO.getChildren().forEach(cloneDTO1 -> cloneDTO1.setParentId(i.getId()));
            parentNewChildrenOldIdMap.put(i.getId(),childrenIds);
            lstCloneDTos.addAll(cloneDTO.getChildren());
            oldNewIdMap.put(i.getId(),cloneDTO);
            newIds.add(i.getId());
        });
        oldNewChildMap.putAll(oldNewIdMap);
        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            cloneMappingService.cloneMappings(newIds, lstCloneDTos, oldNewIdMap, parentNewChildrenOldIdMap);
        }

        return cloneListChildEntities(newIds, domainId, clonedEntities, oldNewIdMap);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND,getResourceName(),ErrorKey.RateTable.ID);
    }

    private OCSCloneableEntity saveRateTableAndFormula(CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> rateTableToCloneMap,
                                                       Map<Long, Long> newOldFormulaIdMap, Map<String, CloneDTO> newNameRateTableCloneMap,
                                                       Map<Long, Integer> categoryMaxPosIndexMap, List<OCSCloneableEntity> clonedEntityNames) {
        if (ocsCloneableRepository == null) {
            throw new UnsupportedOperationException("Method is not ready");
        }
        if (cloneDTO.getChildren() == null) {
            cloneDTO.setChildren(new ArrayList<CloneDTO>());
        }
        Integer domainId = OCSUtils.getDomain();
        String name = cloneDTO.getName();
        Long id = cloneDTO.getId();

        OCSCloneableEntity baseToClone = rateTableToCloneMap.get(id);

        if (baseToClone == null) {
            return null;
        }

        if (!StringUtils.isEmpty(name)) {
            checkDuplicateName(name, id);
        }
        OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
        String newName = "";
        if (StringUtils.isEmpty(name) || name.equals(clonedEntity.getName())) {
            newName = cloneName(baseToClone.getName(), domainId,clonedEntityNames);
        } else {
            newName = name;
        }

        if(baseToClone.getName().equalsIgnoreCase(newName)){
            throwDuplicatedNameException();
        }

        if (getCloneNameMaxLength() != null && newName.length() > getCloneNameMaxLength().intValue()) {
            throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
        }

        Integer maxPos = categoryMaxPosIndexMap.get(clonedEntity.getParentId());
        clonedEntity.setPosIndex(maxPos != null ? maxPos+1 : 0);
        categoryMaxPosIndexMap.put(clonedEntity.getParentId(),clonedEntity.getPosIndex());

        clonedEntity = setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);
        RateTable rateTableToClone = (RateTable) clonedEntity;
        if(rateTableToClone.getDefaultFormulaId() != null){
            Long formulaId = newOldFormulaIdMap.values().stream()
                .filter(oldId -> Objects.equal(oldId,rateTableToClone.getDefaultFormulaId())).findFirst().orElse(null);
            List<Long> newFormulaIds = newOldFormulaIdMap.entrySet().stream()
                .filter(entry -> Objects.equal(entry.getValue(), formulaId))
                .map(Map.Entry::getKey).collect(Collectors.toList());

            if(newFormulaIds != null && newFormulaIds.size() > 0){
                rateTableToClone.setDefaultFormulaId(newFormulaIds.get(0));
                newOldFormulaIdMap.remove(newFormulaIds.get(0));
            }
        }

        newNameRateTableCloneMap.put(newName, cloneDTO);
        return clonedEntity;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.RateTable.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }
}
