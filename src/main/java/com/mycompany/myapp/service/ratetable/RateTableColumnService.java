package com.mycompany.myapp.service.ratetable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.domain.ratetable.RateTableColumn;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.ratetable.RateTableColumnDTO;
import com.mycompany.myapp.mapper.ratetable.RateTableColumnMapper;
import com.mycompany.myapp.repository.normalizer.NormValueRepository;
import com.mycompany.myapp.repository.ratetable.RateTableColumnsRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.normalizer.NormalNormalizerServiceImp;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Transactional
@Service
public class RateTableColumnService extends AbstractCloneMappingService implements RateTableMappingService {

    @Autowired
    private RateTableColumnsRepository rateTableColumnsRepository;

    @Autowired
    private NormValueRepository normValueRepository;

    @Autowired
    private RateTableColumnMapper rateTableColumnMapper;

    @Autowired
    private NormalNormalizerServiceImp normalizerService;

    @Autowired
    private RateTableService rateTableService;

    public void deleteColumn(Long id) {
        Integer domainId = OCSUtils.getDomain();
        rateTableColumnsRepository.deleteByIdAndDomainId(id, domainId);
    }

    public List<NormalizerValue> getNormalizeValue(Long normalizerId) {
        Integer domainId = OCSUtils.getDomain();
        return normValueRepository.findAllByNormalizerId(normalizerId, domainId);
    }

    public List<RateTableColumnDTO> getListColumns(Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
        List<RateTableColumnDTO> dtos = rateTableColumnMapper.toDto(rateTableColumnsRepository.findByRateTableIdAndDomainIdOrderByPosIndexAsc(rateTableId, domainId));
        List<String> normalizerNames = rateTableColumnsRepository.getNormalizeNames(rateTableId, domainId);
        if(dtos != null && dtos.size() > 0 && normalizerNames.size() != dtos.size()){
            throw new ResourceNotFoundException(ErrorMessage.Normalizer.NOT_FOUND,Resources.NORMALIZER,ErrorKey.ENTITY_NOT_FOUND);
        }
        if(dtos != null){
            for (int i = 0; i < dtos.size(); i++) {
                dtos.get(i).setNormalizerName(normalizerNames.get(i));
            }
        }
        return dtos;
    }

    public void updateColumns(List<RateTableColumnDTO> columns, Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
        List<RateTableColumnDTO> removeDeletes = new ArrayList<>();
        for (RateTableColumnDTO item : columns) {
            if (ActionType.DELETE.equals(item.getActionType())) {
                deleteColumn(item.getId());
                removeDeletes.add(item);
            }
        }
        columns.removeAll(removeDeletes);

        removeDeletes = new ArrayList<>();
        for (RateTableColumnDTO item : columns) {
            if (ActionType.EDIT.equals(item.getActionType())) {
                rateTableColumnsRepository.save(rateTableColumnMapper.toEntity(item));
                removeDeletes.add(item);
            }
        }
        columns.removeAll(removeDeletes);
        Set<Integer> columnIndexSet = new HashSet<>();
        Set<Long> normalizerIdSet = new HashSet<>();
        List<Integer> columnIndexList = new ArrayList<>();
        List<Long> normalizerIdList = new ArrayList<>();
        for (RateTableColumnDTO item : columns) {
            columnIndexSet.add(item.getColumnIndex());
            columnIndexList.add(item.getColumnIndex());
            normalizerIdSet.add(item.getNormalizerId());
            normalizerIdList.add(item.getNormalizerId());
        }
        List<Integer> columnsIndexDBs = rateTableColumnsRepository.getColumnIndex(rateTableId, domainId);
        columnIndexSet.addAll(columnsIndexDBs);
        columnIndexList.addAll(columnsIndexDBs);
        if (columnIndexSet.size() != columnIndexList.size()) {
            throw new DataConstrainException(ErrorMessage.RateTable.DUPLICATE_COLUMN_INDEX, Resources.RATE_TABLE, ErrorKey.RateTable.COLUMNS);
        }
        List<Long> normalizerIdDBs = rateTableColumnsRepository.getNormalizerIds(rateTableId, domainId);
        normalizerIdSet.addAll(normalizerIdDBs);
        normalizerIdList.addAll(normalizerIdDBs);
        if (normalizerIdSet.size() != normalizerIdList.size()) {
            throw new DataConstrainException(ErrorMessage.RateTable.DUPLICATE_NORMALIZER, Resources.RATE_TABLE, ErrorKey.RateTable.COLUMNS);
        }
        for (RateTableColumnDTO item : columns) {
            RateTableColumn rateTableColumn = rateTableColumnMapper.toEntity(item);
            rateTableColumn.setDomainId(domainId);
            rateTableColumn.setRateTableId(rateTableId);
            rateTableColumn = rateTableColumnsRepository.save(rateTableColumn);
        }
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = rateTableColumnsRepository;
        this.childService = normalizerService;
        this.parentService = rateTableService;
        this.parentDependService = rateTableService;
    }

    @Override
    protected String getResource() {
        return Resources.NORMALIZER;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
