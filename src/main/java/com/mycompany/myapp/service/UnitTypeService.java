package com.mycompany.myapp.service;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;

import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.BalType;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.formula.Formula;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.UnitTypeDTO;
import com.mycompany.myapp.mapper.UnitTypeMapper;
import com.mycompany.myapp.repository.BalTypeRepository;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.UnitTypeRepository;
import com.mycompany.myapp.repository.formula.FormulaRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRowsRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.pro.packaged.ca;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class UnitTypeService extends OCSBaseService {
    @Autowired
    private UnitTypeRepository unitTypeRepository;
    @Autowired
    private UnitTypeMapper unitTypeMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RateTableRepository rateTableRepository;

    @Autowired
    private RateTableRowsRepository rateTableRowsRepository;

    @Autowired
    private FormulaRepository formulaRepository;

    @Autowired
    private BalTypeRepository balTypeRepository;

    @Autowired
    private BalancesService balancesService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = unitTypeRepository;
        this.parentService = categoryService;
    }

    public UnitTypeDTO createUnitType(UnitTypeDTO unitTypeDTO) {
        Integer domainId = OCSUtils.getDomain();

        UnitType unitType = unitTypeMapper.toEntity(unitTypeDTO);
        checkCategoryExist(unitTypeDTO, domainId);
        checkDuplicateName(unitTypeDTO.getName(), unitTypeDTO.getId());
        validate(unitTypeDTO.getPrecision());
        unitType.setDomainId(domainId);
        super.setPosIndex(domainId, unitType, unitType.getCategoryId());

        unitType = unitTypeRepository.save(unitType);

        return unitTypeMapper.toDto(unitType);
    }

    private void validate(Integer number) {
        if (number != null && number < 0) {
            throw new DataConstrainException(ErrorMessage.UnitType.PRECISION_MIN, Resources.UNIT_TYPE,
                    ErrorKey.UnitType.PRECISION);
        }
    }

    public void deleteUnitType(Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<BalType> balTypes = balTypeRepository.findByDomainIdAndUnitTypeId(domainId, id);
        if (!CollectionUtils.isEmpty(balTypes)) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.UNIT_TYPE,
                    ErrorKey.UnitType.ID);
        }

        // check unitType use by rate_table
        Long unitTypeUseByRateTable = unitTypeRepository.countUnitTypeUseByRateTable(id);
        if (unitTypeUseByRateTable > 0) {
            throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.UNIT_TYPE,
                    "unitTypeId");
        }

        unitTypeRepository.deleteByIdAndDomainId(id, domainId);
    }

    public UnitTypeDTO updateUnitType(UnitTypeDTO unitTypeDTO) {
        Integer domainId = OCSUtils.getDomain();
        UnitType unitTypeDB = unitTypeRepository.findByIdAndDomainIdForUpdate(unitTypeDTO.getId(), domainId);

        if (unitTypeDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.UNIT_TYPE,
                    ErrorKey.UnitType.ID);
        }

        // Lock old category
        checkCategoryExist(unitTypeDTO, domainId);
        checkDuplicateName(unitTypeDTO.getName(), unitTypeDTO.getId());
        validate(unitTypeDTO.getPrecision());

        // Convert to Entity to save
        UnitType unitType = unitTypeMapper.toEntity(unitTypeDTO);
        if (!Objects.equal(unitTypeDB.getCategoryId(), unitTypeDTO.getCategoryId())) {
            setPosIndex(domainId, unitType, unitTypeDTO.getCategoryId());
        }
        // cập nhật lại threshold value lúc baseRate thay đổi
        if (!Objects.equal(unitTypeDB.getBaseRate(), unitTypeDTO.getBaseRate())) {
            balancesService.updateUnitType(unitTypeDB.getId(), unitTypeDTO.getBaseRate(), unitTypeDB.getBaseRate());
        }

        unitType.setDomainId(domainId);

        // update các trường sử dụng base rate
        if (unitTypeDB.getBaseRate().equals(unitTypeDTO.getBaseRate()) == false) {
            updateFormula(unitTypeDTO.getId(), unitTypeDB.getBaseRate(), unitTypeDTO.getBaseRate(), domainId);
        }

        unitType = unitTypeRepository.save(unitType);

        return unitTypeMapper.toDto(unitType);
    }

    // Check category exist or not
    private void checkCategoryExist(UnitTypeDTO unitTypeDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(unitTypeDTO.getCategoryId(),
                domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY,
                    ErrorKey.Category.ID);
        }
    }

    public Page<UnitTypeDTO> getUnitTypes(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY,
                    ErrorKey.Category.ID);
        }

        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;

        Page<UnitTypeDTO> unitTypes = unitTypeRepository.findUnitTypes(searchCriteria.getParentId(),
                searchCriteria.getDomainId(), searchCriteria.getName(), searchCriteria.getDescription(), pageable)
                .map(unitTypeMapper::toDto);

        for (UnitTypeDTO unitTypeDTO : unitTypes) {
            unitTypeDTO.setCategoryLevel(parentCategoryLevel + 1);
        }
        return unitTypes;
    }

    public UnitTypeDTO getUnitTypeDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        UnitType unitType = unitTypeRepository.findByIdAndDomainId(id, domainId);
        if (unitType == null) {
            throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.UNIT_TYPE,
                    ErrorKey.UnitType.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(unitType.getCategoryId(), domainId);
        UnitTypeDTO unitTypeDTO = unitTypeMapper.toDto(unitType);
        unitTypeDTO.setCategoryName(category.getName());
        unitTypeDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        return unitTypeDTO;
    }

    public List<UnitTypeDTO> getUnitTypeForDropdown() {
        Integer domainId = OCSUtils.getDomain();
        Sort sort = Sort.by(Sort.Direction.ASC, UnitType.FieldNames.POS_INDEX);
        return unitTypeRepository.findByDomainId(domainId, sort).stream().map(unitTypeMapper::toDto)
                .collect(Collectors.toList());
    }

    public void existById(Long unitTypeId) {
        Integer domainId = OCSUtils.getDomain();
        if (!unitTypeRepository.existsByIdAndDomainId(unitTypeId, domainId)) {
            throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.UNIT_TYPE,
                    ErrorKey.UnitType.ID);
        }
    }

    public Integer getBaseRateById(Long unitTypeId) {
        existById(unitTypeId);
        Integer domainId = OCSUtils.getDomain();
        return unitTypeRepository.findBaseRateByIdAndDomain(unitTypeId, domainId);
    }

    public UnitTypeDTO getUnitTypeDetailSimple(Long id) {
        Integer domainId = OCSUtils.getDomain();
        UnitType unitType = unitTypeRepository.findByIdAndDomainId(id, domainId);
        if (unitType == null) {
            throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.UNIT_TYPE,
                    ErrorKey.UnitType.ID);
        }
        return unitTypeMapper.toDto(unitType);
    }

    // Update Formula use unitType if unitType update baseRate
    private void updateFormula(Long unitTypeId, Integer oldBaseRate, Integer newBaseRate, Integer domainId) {
        List<RateTable> rateTables = rateTableRepository.findByDomainIdAndUnitTypeId(domainId, unitTypeId);
        List<Long> rateTableIds = new ArrayList<>();
        List<Long> formulaIds = new ArrayList<>();
        for (RateTable rateTable : rateTables) {
            rateTableIds.add(rateTable.getId());
            formulaIds.add(rateTable.getDefaultFormulaId());
        }
        List<Long> lstFormulaIdsOfRateTableRow = rateTableRowsRepository
                .findFormulaIdByDomainIdAndRateTableIdIn(domainId, rateTableIds);
        formulaIds.addAll(lstFormulaIdsOfRateTableRow);
        List<Formula> lstFormulas = formulaRepository.findByDomainIdAndIdIn(domainId, formulaIds);
        List<Formula> lstUpdateFormulas = new ArrayList<>();
        for (Formula formula : lstFormulas) {
            String binary = Integer.toBinaryString(formula.getTemplateBits().intValue());
            if (binary.length() < 6) {
                for (int i = binary.length(); i < 6; i++) {
                    binary = 0 + binary;
                }
            }
            if (("0".equals(Character.toString(binary.charAt(2))) && formula.getFormulaType() == 0)
                    || "1".equals(Character.toString(binary.charAt(2)))) {
                Long a = formula.getA();
                Long b = formula.getB();
                int number;
                if("0".equals(Character.toString(binary.charAt(3)))){
                     number = (a.intValue() / oldBaseRate) * newBaseRate;
                    a = Long.valueOf(number);
                }
                if("0".equals(Character.toString(binary.charAt(4)))){
                     number = (b.intValue() / oldBaseRate) * newBaseRate;
                    b = Long.valueOf(number);
                }

                if (a > Long.MAX_VALUE) {
                    throw new DataInvalidException(ErrorMessage.Formula.OVER_FLOW, Resources.FORMULA,
                            ErrorKey.Formula.A);
                }
                if (b > Long.MAX_VALUE) {
                    throw new DataInvalidException(ErrorMessage.Formula.OVER_FLOW, Resources.FORMULA,
                            ErrorKey.Formula.B);
                }
                formula.setA(Long.valueOf(a));
                formula.setB(Long.valueOf(b));
                lstUpdateFormulas.add(formula);
            }
        }
        formulaRepository.saveAll(lstUpdateFormulas);
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.UNIT_TYPES;
    }
}
