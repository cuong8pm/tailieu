package com.mycompany.myapp.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mycompany.myapp.config.ApplicationProperties;
import com.mycompany.myapp.domain.GeoHomeZone;
import com.mycompany.myapp.domain.GeoNetZone;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.GeoNetZoneDTO;
import com.mycompany.myapp.mapper.GeoNetZoneMapper;
import com.mycompany.myapp.repository.GeoHomeZoneRepository;
import com.mycompany.myapp.repository.GeoNetZoneRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.FileException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.ExcelUtils;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class GeoNetZoneService {

    @Autowired
    private GeoNetZoneRepository geoNetZoneRepository;

    @Autowired
    private GeoNetZoneMapper geoNetZoneMapper;

    @Autowired
    private GeoHomeZoneRepository geoHomeZoneRepository;

    @Autowired
    private ApplicationProperties applicationProperties;

    private static final String[] HEADERs = { "ID", "Cell ID", "Update Date" };

    private static final String TITLE = "List Geo Net Zone by Geo Home Zone";

    private static final String SHEET = "ListGeoNetZonebyGeoHomeZone";

    private final Logger log = LoggerFactory.getLogger(GeoNetZoneService.class);

    public Page<GeoNetZoneDTO> getGeoNetZones(Long geoHomeZoneId, String cellId, String updateDate, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        GeoHomeZone geoHomeZone = geoHomeZoneRepository.findByIdAndDomainId(geoHomeZoneId, domainId);
        if (geoHomeZone == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE,
                    ErrorKey.GeoHomeZone.ID);
        }
        return geoNetZoneRepository.findGeoNetZones(geoHomeZoneId, domainId, cellId, updateDate, pageable)
                .map(geoNetZoneMapper::toDto);
    }

    public GeoNetZoneDTO getGeoNetZoneDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        GeoNetZone geoNetZone = geoNetZoneRepository.findByIdAndDomainId(id, domainId);
        if (geoNetZone == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoNetZone.NOT_FOUND, Resources.GEO_NET_ZONE,
                    ErrorKey.GeoNetZone.ID);
        }
        GeoHomeZone geoHomeZone = geoHomeZoneRepository.findByIdAndDomainId(geoNetZone.getGeoHomeZoneId(), domainId);
        GeoNetZoneDTO geoNetZoneDTO = geoNetZoneMapper.toDto(geoNetZone);
        geoNetZoneDTO.setGeoHomeZoneName(geoHomeZone.getName());
        geoNetZoneDTO.setHomeZoneCode(geoHomeZone.getGeoHomeZoneCode());

        return geoNetZoneDTO;
    }

    public GeoNetZoneDTO createGeoNetZone(GeoNetZoneDTO geoNetZoneDTO) {
        Integer domainId = OCSUtils.getDomain();

        GeoNetZone geoNetZone = geoNetZoneMapper.toEntity(geoNetZoneDTO);
        checkGeoHomeExist(geoNetZoneDTO, domainId);
        checkDuplicateCellID(geoNetZoneDTO.getCellId(), geoNetZoneDTO.getId(), geoNetZone.getGeoHomeZoneId());

        geoNetZone.setDomainId(domainId);

        geoNetZone = geoNetZoneRepository.save(geoNetZone);
//        geoNetZone.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        return geoNetZoneMapper.toDto(geoNetZone);
    }

    // Check category exist or not
    private void checkGeoHomeExist(GeoNetZoneDTO geoNetZoneDTO, Integer domainId) {
        GeoHomeZone geoHomeZone = geoHomeZoneRepository.findByIdAndDomainIdForUpdate(geoNetZoneDTO.getGeoHomeZoneId(),
                domainId);
        if (geoHomeZone == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE,
                    ErrorKey.GeoHomeZone.ID);
        }
    }

    public void deleteGeoNetZone(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        geoNetZoneRepository.deleteByIdInAndDomainId(ids, domainId);
    }

    public GeoNetZoneDTO updateGeoNetZone(GeoNetZoneDTO geoNetZoneDTO) {
        Integer domainId = OCSUtils.getDomain();
        GeoNetZone geoNetZone = geoNetZoneRepository.findByIdAndDomainId(geoNetZoneDTO.getId(), domainId);

        if (geoNetZone == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoNetZone.NOT_FOUND, Resources.GEO_NET_ZONE,
                    ErrorKey.GeoNetZone.ID);
        }

        // Lock old category
        checkGeoHomeExist(geoNetZoneDTO, domainId);
        checkDuplicateCellID(geoNetZoneDTO.getCellId(), geoNetZoneDTO.getId(), geoNetZoneDTO.getGeoHomeZoneId());

        // Convert to Entity to save
        geoNetZone = geoNetZoneMapper.toEntity(geoNetZoneDTO);
        geoNetZone.setDomainId(domainId);
//        geoNetZone.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        geoNetZone = geoNetZoneRepository.save(geoNetZone);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(geoNetZone, response);

        return geoNetZoneMapper.toDto(geoNetZone);
    }

    public void checkDuplicateCellID(Long cellId, Long id, Long geoHomeZoneId) {
        Integer domainId = OCSUtils.getDomain();
        GeoNetZone geoNetZone = geoNetZoneRepository.findByCellIdAndAndGeoHomeZoneIdAndDomainId(cellId, geoHomeZoneId,
                domainId);
        if (geoNetZone != null && !Objects.equals(id, geoNetZone.getId())) {
            throw new DataConstrainException(ErrorMessage.GeoNetZone.DUPLICATED_CELL_ID, Resources.GEO_NET_ZONE,
                    ErrorKey.GeoNetZone.CELL_ID);
        }
    }

    private List<Long> readXlsFile(MultipartFile readExcelDataFile) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(readExcelDataFile.getInputStream());
        HSSFSheet worksheet = workbook.getSheetAt(0);
        List<Long> lstCellIdFromFile = new ArrayList<>();
        // worksheet.getLastRowNum() + 1 lay tong so luong row duoc su dung
        for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
            HSSFRow row = worksheet.getRow(i);
            try {
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    lstCellIdFromFile.add((long) row.getCell(1).getNumericCellValue());
                }
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                    lstCellIdFromFile.add(Long.parseLong(row.getCell(1).getStringCellValue().trim()));
                }
            } catch (Exception e) {
                continue;
            }
        }
        workbook.close();
        return lstCellIdFromFile;
    }

    private List<Long> readXlsxFile(MultipartFile readExcelDataFile) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        List<Long> lstCellIdFromFile = new ArrayList<>();
        // worksheet.getLastRowNum() + 1 lay tong so luong row duoc su dung
        for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
            XSSFRow row = worksheet.getRow(i);
            try {
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    lstCellIdFromFile.add((long) row.getCell(1).getNumericCellValue());
                }
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                    lstCellIdFromFile.add(Long.parseLong(row.getCell(1).getStringCellValue().trim()));
                }

            } catch (Exception e) {
                continue;
            }
        }
        workbook.close();
        return lstCellIdFromFile;
    }

    public Quartet<Boolean, ByteArrayInputStream, Integer, Integer> importDataFromFile(MultipartFile readExcelDataFile,
            Long geoHomeZoneId) throws IOException {
        List<GeoNetZone> listGeoNetZone = new ArrayList<>();
        Boolean isSuccess = false;
        Boolean isTemplateForm = false;
        ByteArrayInputStream byteArrayInputStream = null;
        List<Long> itemRemove = new ArrayList<>();
        List<String> lstInvalidTypeValue = new ArrayList<>();
        Integer domainId = OCSUtils.getDomain();
        String[] fileName = readExcelDataFile.getOriginalFilename().split("\\.");
        if (fileName.length < 2 || (!"xlsx".equals(fileName[fileName.length - 1])
                && !"xls".equals(fileName[fileName.length - 1]) && !"txt".equals(fileName[fileName.length - 1]))) {
            throw new FileException(ErrorMessage.INVALID_FILE, Resources.ZONE_DATA,
                    readExcelDataFile.getOriginalFilename());
        }
        List<Long> lstCellIdFromFile = new ArrayList<>();

        if ("xlsx".equals(fileName[fileName.length - 1])) {
            isTemplateForm = ExcelUtils.isTemplateFormXlsx(readExcelDataFile, HEADERs, TITLE);
            if (!isTemplateForm) {
                throw new FileException(ErrorMessage.INVALID_FILE, Resources.GEO_NET_ZONE,
                        readExcelDataFile.getOriginalFilename());
            }
            lstCellIdFromFile = readXlsxFile(readExcelDataFile);
        }

        if ("xls".equals(fileName[fileName.length - 1])) {
            isTemplateForm = ExcelUtils.isTemplateFormXls(readExcelDataFile, HEADERs, TITLE);
            if (!isTemplateForm) {
                throw new FileException(ErrorMessage.INVALID_FILE, Resources.GEO_NET_ZONE,
                        readExcelDataFile.getOriginalFilename());
            }
            lstCellIdFromFile = readXlsFile(readExcelDataFile);
        }

        if ("txt".equals(fileName[fileName.length - 1])) {
            Pair<List<Long>, List<String>> pair = readTxtFile(readExcelDataFile);
            lstCellIdFromFile = pair.getValue0();
            lstInvalidTypeValue = pair.getValue1();
        }

        List<Long> lstRecordsOfFile = new ArrayList<>();
        lstRecordsOfFile.addAll(lstCellIdFromFile);

        List<GeoNetZone> lstExistGeoNetZone = geoNetZoneRepository.findByGeoHomeZoneIdAndCellIdIn(geoHomeZoneId,
                lstCellIdFromFile);
        for (GeoNetZone geoNetZone : lstExistGeoNetZone) {
            for (Long cellId : lstCellIdFromFile) {
                if (cellId.equals(geoNetZone.getCellId())) {
                    itemRemove.add(cellId);
                }
            }
        }
        for (Long cellId : lstCellIdFromFile) {
            if (cellId.toString().length() > 15 || cellId < 0) {
                itemRemove.add(cellId);
            }
        }
        lstCellIdFromFile.removeAll(itemRemove);
        final List<Long> lstCellIdFromFileCopy = lstCellIdFromFile;
        List<Long> lstCellIdDuplicateOfFile = lstCellIdFromFile.stream()
                .filter(e -> Collections.frequency(lstCellIdFromFileCopy, e) > 1).distinct()
                .collect(Collectors.toList());

        if ("xls".equals(fileName[fileName.length - 1])) {
            Quartet<Boolean, ByteArrayInputStream, Integer, Integer> quartet = ExcelUtils.ExcelUltilsGeoNetZone
                    .exportDataOfImportFileXls(readExcelDataFile, itemRemove, lstCellIdFromFile,
                            lstCellIdDuplicateOfFile, lstRecordsOfFile);
            if (quartet.getValue0() && !CollectionUtils.isEmpty(lstCellIdFromFile)) {
                saveGeoNetZones(lstCellIdFromFile, listGeoNetZone, geoHomeZoneId, domainId);
            }
            return quartet;
        }
        if ("xlsx".equals(fileName[fileName.length - 1])) {
            Quartet<Boolean, ByteArrayInputStream, Integer, Integer> quartet = ExcelUtils.ExcelUltilsGeoNetZone
                    .exportDataOfImportFileXlsx(readExcelDataFile, itemRemove, lstCellIdFromFile,
                            lstCellIdDuplicateOfFile, lstRecordsOfFile);
            if (quartet.getValue0() && !CollectionUtils.isEmpty(lstCellIdFromFile)) {
                saveGeoNetZones(lstCellIdFromFile, listGeoNetZone, geoHomeZoneId, domainId);
            }
            return quartet;
        }
        if ("txt".equals(fileName[fileName.length - 1])) {
            Quartet<Boolean, ByteArrayInputStream, Integer, Integer> quartet = ExcelUtils.ExcelUltilsGeoNetZone
                    .exportDataOfImportFileTxt(readExcelDataFile, lstRecordsOfFile, itemRemove, lstCellIdFromFile,
                            lstCellIdDuplicateOfFile, lstInvalidTypeValue,
                            applicationProperties.getExcelTemplateRootDirectoryPath());
            if (quartet.getValue0() && !CollectionUtils.isEmpty(lstCellIdFromFile)) {
                saveGeoNetZones(lstCellIdFromFile, listGeoNetZone, geoHomeZoneId, domainId);
            }
            return quartet;
        }
        return new Quartet<Boolean, ByteArrayInputStream, Integer, Integer>(isSuccess, byteArrayInputStream,
                itemRemove.size(), lstCellIdFromFile.size());
    }

    public ByteArrayInputStream exportGeoNetZoneData(Long geoHomeZoneId) throws IOException {
        Integer domainId = OCSUtils.getDomain();
        List<GeoNetZone> lstGeoNetZoneData = geoNetZoneRepository.findByDomainIdAndGeoHomeZoneId(domainId,
                geoHomeZoneId);
        ByteArrayInputStream in = ExcelUtils.ExcelUltilsGeoNetZone.exportGeoNetZoneData(lstGeoNetZoneData, SHEET,
                HEADERs, TITLE);
        return in;
    }

    private void saveGeoNetZones(List<Long> lstCellIdFromFile, List<GeoNetZone> listGeoNetZone, Long geoHomeZoneId,
            Integer domainId) {
        List<Long> cellIdOld = new ArrayList<>();
        for (Long cellId : lstCellIdFromFile) {
            if (cellIdOld.indexOf(cellId) < 0) {
                GeoNetZone geoNetZone = new GeoNetZone();
                geoNetZone.setId(null);
                geoNetZone.setCellId(cellId);
                geoNetZone.setDomainId(domainId);
                geoNetZone.setGeoHomeZoneId(geoHomeZoneId);
                listGeoNetZone.add(geoNetZone);
            }
            cellIdOld.add(cellId);
        }

        listGeoNetZone = geoNetZoneRepository.saveAll(listGeoNetZone);
    }

    private Pair<List<Long>, List<String>> readTxtFile(MultipartFile readDataFile) {
        List<Long> lstCellId = new ArrayList<>();
        List<String> lstInvalidTypeValue = new ArrayList<>();
        try {
            InputStream inputStream = readDataFile.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String data;
            while ((data = bufferedReader.readLine()) != null) {
                data = data.trim();
                if (!StringUtils.isEmpty(data) && !data.equals("\n")) {
                    try {
                        lstCellId.add(Long.parseLong(data));
                    } catch (Exception e) {
                        lstInvalidTypeValue.add(data);
                    }
                }
            }
            reader.close();
        } catch (IOException e) {
            log.error("Cannot read file", e);
        }
        return new Pair<List<Long>, List<String>>(lstCellId, lstInvalidTypeValue);
    }

}
