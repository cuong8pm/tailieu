package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ThresholdBaltypeMap;
import com.mycompany.myapp.dto.ThresholdsDTO;
import com.mycompany.myapp.repository.ThresholdBaltypeMapRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ThresholdBaltypeMapService {
    @Autowired
    private ThresholdBaltypeMapRepository thresholdBaltypeMapRepository;
    //select detail
    //select by balanceId
    public List<Long> getThresholdIdByBalanceId(Long balanceId) {
        Integer domainId = OCSUtils.getDomain();
        return thresholdBaltypeMapRepository.findThresholdIdByBalTypeId(domainId, balanceId);
    }

    public List<ThresholdBaltypeMap> getThresholdByBalanceId(Long balanceId) {
        Integer domainId = OCSUtils.getDomain();
        return thresholdBaltypeMapRepository.findThresholdByBalTypeId(domainId, balanceId);
    }

    //create
    public void createThresholdBalTypeMap(ThresholdsDTO thresholds, Long balTypeId) {
        if (thresholds.getId() == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.THRESHOLD, ErrorKey.ID);
        }
        Integer domainId = OCSUtils.getDomain();
        ThresholdBaltypeMap map = new ThresholdBaltypeMap();
        map.setBaltypeId(balTypeId);
        map.setDomainId(domainId);
        map.setThresholdId(thresholds.getId());
        thresholdBaltypeMapRepository.save(map);
    }

    //create
    public void createThresholdBalTypeMap(List<ThresholdsDTO> thresholds, Long balTypeId) {
        Integer domainId = OCSUtils.getDomain();
        List<ThresholdBaltypeMap> lstToInserts = new ArrayList<>();
        for (ThresholdsDTO thresholdId : thresholds) {
            if (thresholdId.getId() != null) {
                ThresholdBaltypeMap map = new ThresholdBaltypeMap();
                map.setBaltypeId(balTypeId);
                map.setDomainId(domainId);
                map.setThresholdId(thresholdId.getId());
                lstToInserts.add(map);
            }
        }
        thresholdBaltypeMapRepository.saveAll(lstToInserts);
    }
    //delete
    public void deleteByThresholdAndBalance(Long thresholdId, Long balanceId) {
        Integer domainId = OCSUtils.getDomain();
        List<ThresholdBaltypeMap> thresholdBaltypeMaps = thresholdBaltypeMapRepository.findByThresholdAndDomainAndBalance(domainId, thresholdId, balanceId);
        if (CollectionUtils.isEmpty(thresholdBaltypeMaps)) {
            throw new ResourceConflictException(ErrorMessage.INPUT_NOT_MATCH, Resources.THRESHOLD_BALTYPE_MAP, ErrorKey.ID);
        }
        thresholdBaltypeMapRepository.deleteByThresholdId(domainId, thresholdId, balanceId);
    }

    public Boolean existByThreshold(Long thresholdId) {
        Integer domainId = OCSUtils.getDomain();
        return thresholdBaltypeMapRepository.existsByThresholdIdAndDomainId(thresholdId, domainId);
    }
}
