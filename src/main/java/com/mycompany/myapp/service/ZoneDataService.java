package com.mycompany.myapp.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javatuples.Quartet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mycompany.myapp.config.ApplicationProperties;
import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.domain.ZoneData;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ZoneDataDTO;
import com.mycompany.myapp.mapper.ZoneDataMapper;
import com.mycompany.myapp.repository.ZoneDataRepository;
import com.mycompany.myapp.repository.ZoneRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.FileException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.ExcelUtils;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class ZoneDataService extends OCSBaseService {
    @Autowired
    private ZoneDataRepository zoneDataRepository;

    @Autowired
    private ZoneDataMapper zoneDataMapper;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private ZoneMapService zoneMapService;

    @Autowired
    private ApplicationProperties applicationProperties;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = zoneDataRepository;
        this.parentService = zoneMapService;
    }

    private static final String[] HEADERs = { "ID", "Value", "Update Time" };

    private static final String TITLE = "List Zone Data by Zone";

    private static final String SHEET = "List Zone Data by Zone";

    private final Logger log = LoggerFactory.getLogger(ZoneDataService.class);

    public Page<ZoneDataDTO> getZoneData(Long zoneId, String zoneDataValue, String updateDate, String id,
            Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        return zoneDataRepository.findZoneDatas(zoneId, domainId, zoneDataValue, updateDate, id, pageable)
                .map(zoneDataMapper::toDto);
    }

    public ZoneDataDTO getZoneDataDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        ZoneData zoneData = zoneDataRepository.findByIdAndDomainId(id, domainId);
        if (zoneData == null) {
            throw new ResourceNotFoundException(ErrorMessage.ZoneData.NOT_FOUND, Resources.ZONE_DATA,
                    ErrorKey.ZoneData.ID);
        }
        Zone zone = zoneRepository.findByIdAndDomainIdForUpdate(zoneData.getZoneId(), domainId);
        ZoneDataDTO zoneDataDTO = zoneDataMapper.toDto(zoneData);
        zoneDataDTO.setZoneName(zone.getName());
        return zoneDataDTO;
    }

    public ZoneDataDTO createZoneData(ZoneDataDTO zoneDataDTO) {
        Integer domainId = OCSUtils.getDomain();

        ZoneData zoneData = zoneDataMapper.toEntity(zoneDataDTO);
        checkZoneExist(zoneDataDTO, domainId);
        checkDuplicateValue(zoneData.getZoneDataValue(), zoneData.getZoneId(), zoneData.getId());

        zoneData.setDomainId(domainId);
//        zoneData.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        zoneData = zoneDataRepository.save(zoneData);

        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(zoneData, response);
        return zoneDataMapper.toDto(zoneData);
    }

    private void checkDuplicateValue(String value, Long zoneId, Long id) {
        Integer domainId = OCSUtils.getDomain();
        ZoneData zoneData = zoneDataRepository.findByZoneDataValueAndZoneIdAndDomainId(value, zoneId, domainId);
        if (zoneData != null && !Objects.equals(id, zoneData.getId())) {
            throw new DataConstrainException(ErrorMessage.ZoneData.DUPLICATED_VALUE, Resources.ZONE_DATA,
                    ErrorKey.ZoneData.VALUE);
        }
    }

    public void deleteZoneData(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        zoneDataRepository.deleteByIdInAndDomainId(ids, domainId);
    }

    // Check category exist or not
    private void checkZoneExist(ZoneDataDTO ZoneDataDTO, Integer domainId) {
        Zone parent = zoneRepository.findByIdAndDomainIdForUpdate(ZoneDataDTO.getZoneId(), domainId);
        if (parent == null) {
            throw new ResourceNotFoundException(ErrorMessage.Zone.NOT_FOUND, Resources.ZONE, ErrorKey.Zone.ID);
        }
    }

    public ZoneDataDTO updateZoneData(ZoneDataDTO zoneDataDTO) {
        Integer domainId = OCSUtils.getDomain();
        ZoneData zoneData = zoneDataRepository.findByIdAndDomainIdForUpdate(zoneDataDTO.getId(), domainId);

        if (zoneData == null) {
            throw new ResourceNotFoundException(ErrorMessage.ZoneData.NOT_FOUND, Resources.ZONE_DATA,
                    ErrorKey.ZoneData.ID);
        }

        // Lock old category
        checkZoneExist(zoneDataDTO, domainId);
        checkDuplicateValue(zoneDataDTO.getZoneDataValue(), zoneDataDTO.getZoneId(), zoneDataDTO.getId());

        // Convert to Entity to save
        zoneData = zoneDataMapper.toEntity(zoneDataDTO);
        zoneData.setDomainId(domainId);
//        zoneData.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        zoneData = zoneDataRepository.save(zoneData);

        return zoneDataMapper.toDto(zoneData);
    }

    private List<String> readXlsFile(MultipartFile readExcelDataFile) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(readExcelDataFile.getInputStream());
        HSSFSheet worksheet = workbook.getSheetAt(0);
        List<String> lstZoneDataValue = new ArrayList<>();
        // worksheet.getLastRowNum() + 1 lay tong so luong row duoc su dung
        for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
            HSSFRow row = worksheet.getRow(i);
            try {
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                    if (!"".equals(row.getCell(1).getStringCellValue().trim())) {
                        lstZoneDataValue.add(row.getCell(1).getStringCellValue().trim());
                    }
                }
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    lstZoneDataValue.add(String.valueOf((int) row.getCell(1).getNumericCellValue()).trim());
                }
            } catch (Exception e) {
                continue;
            }
        }
        workbook.close();
        return lstZoneDataValue;
    }

    private List<String> readXlsxFile(MultipartFile readExcelDataFile) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        List<String> lstZoneDataValue = new ArrayList<>();
        // worksheet.getLastRowNum() + 1 lay tong so luong row duoc su dung
        for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {

            XSSFRow row = worksheet.getRow(i);
            try {
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                    if (!"".equals(row.getCell(1).getStringCellValue().trim())) {
                        lstZoneDataValue.add(row.getCell(1).getStringCellValue().trim());
                    }
                }
                if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    lstZoneDataValue.add(String.valueOf((int) row.getCell(1).getNumericCellValue()).trim());
                }
            } catch (Exception e) {
                continue;
            }
        }
        workbook.close();
        return lstZoneDataValue;
    }

    @Transactional
    public Quartet<Boolean, InputStream, Integer, Integer> importDataFromFile(MultipartFile readExcelDataFile,
            Long zoneId) throws IOException {
        List<ZoneData> listZoneData = new ArrayList<>();
        Boolean isTemplateForm = false;
        Integer domainId = OCSUtils.getDomain();
        String[] fileName = readExcelDataFile.getOriginalFilename().split("\\.");
        if (fileName.length < 2 || (!"xlsx".equals(fileName[fileName.length - 1])
                && !"xls".equals(fileName[fileName.length - 1]) && !"txt".equals(fileName[fileName.length - 1]))) {
            throw new FileException(ErrorMessage.INVALID_FILE, Resources.ZONE_DATA,
                    readExcelDataFile.getOriginalFilename());
        }
        List<String> lstZoneDataValue = new ArrayList<>();
        if ("xlsx".equals(fileName[fileName.length - 1])) {
            isTemplateForm = ExcelUtils.isTemplateFormXlsx(readExcelDataFile, HEADERs, TITLE);
            if (!isTemplateForm) {
                throw new FileException(ErrorMessage.INVALID_FILE, Resources.ZONE_DATA,
                        readExcelDataFile.getOriginalFilename());
            }
            lstZoneDataValue = readXlsxFile(readExcelDataFile);
        }

        if ("xls".equals(fileName[fileName.length - 1])) {
            isTemplateForm = ExcelUtils.isTemplateFormXls(readExcelDataFile, HEADERs, TITLE);
            if (!isTemplateForm) {
                throw new FileException(ErrorMessage.INVALID_FILE, Resources.ZONE_DATA,
                        readExcelDataFile.getOriginalFilename());
            }
            lstZoneDataValue = readXlsFile(readExcelDataFile);
        }

        if ("txt".equals(fileName[fileName.length - 1])) {
            lstZoneDataValue = importTxtFile(readExcelDataFile);
        }
        List<String> lstRecordsOfFile = new ArrayList<>();
        lstRecordsOfFile.addAll(lstZoneDataValue);
        List<ZoneData> lstExistZoneData = zoneDataRepository.findByZoneIdAndZoneDataValueIn(zoneId, lstZoneDataValue);
        List<String> itemRemove = new ArrayList<>();
        // check value trong file da ton tai trong DB chua
        for (ZoneData itemZoneData : lstExistZoneData) {
            for (String zoneDataValue : lstZoneDataValue) {
                if (zoneDataValue.equals(itemZoneData.getZoneDataValue())) {
                    itemRemove.add(zoneDataValue);
                }
            }
        }
        // check validate data trong file
        for (String zoneDataValue : lstZoneDataValue) {
            if (zoneDataValue.length() > 50) {
                itemRemove.add(zoneDataValue);
            }
        }

        lstZoneDataValue.removeAll(itemRemove);

        final List<String> lstZoneDataValueCopy = lstZoneDataValue;
        List<String> lstZoneDataValueDuplicateOfFile = lstZoneDataValue.stream()
                .filter(e -> Collections.frequency(lstZoneDataValueCopy, e) > 1).distinct()
                .collect(Collectors.toList());
        if ("xls".equals(fileName[fileName.length - 1])) {
            Quartet<Boolean, InputStream, Integer, Integer> quartet = ExcelUtils.ExcelUtilsZoneData
                    .exportDataOfImportFileXls(readExcelDataFile, itemRemove, lstZoneDataValue,
                            lstZoneDataValueDuplicateOfFile, lstRecordsOfFile);
            if (quartet.getValue0() && !CollectionUtils.isEmpty(lstZoneDataValue)) {
                saveZoneDataValues(lstZoneDataValue, listZoneData, zoneId, domainId);
            }
            return quartet;
        }
        if ("xlsx".equals(fileName[fileName.length - 1])) {
            Quartet<Boolean, InputStream, Integer, Integer> quartet = ExcelUtils.ExcelUtilsZoneData
                    .exportDataOfImportFileXlsx(readExcelDataFile, itemRemove, lstZoneDataValue,
                            lstZoneDataValueDuplicateOfFile, lstRecordsOfFile);
            if (quartet.getValue0() && !CollectionUtils.isEmpty(lstZoneDataValue)) {
                saveZoneDataValues(lstZoneDataValue, listZoneData, zoneId, domainId);
            }
            return quartet;
        }
        if ("txt".equals(fileName[fileName.length - 1])) {
            Quartet<Boolean, InputStream, Integer, Integer> quartet = ExcelUtils.ExcelUtilsZoneData
                    .exportDataOfImportFileTxt(readExcelDataFile, lstRecordsOfFile, itemRemove, lstZoneDataValue,
                            lstZoneDataValueDuplicateOfFile, applicationProperties.getExcelTemplateRootDirectoryPath());
            if (quartet.getValue0() && !CollectionUtils.isEmpty(lstZoneDataValue)) {
                saveZoneDataValues(lstZoneDataValue, listZoneData, zoneId, domainId);
            }
            return quartet;
        }
        return null;
    }

    public ByteArrayInputStream exportZoneData(Long zoneId) throws IOException {
        Integer domainId = OCSUtils.getDomain();
        List<ZoneData> lstZoneData = zoneDataRepository.findByZoneIdAndDomainId(zoneId, domainId);
        ByteArrayInputStream in = ExcelUtils.ExcelUtilsZoneData.exportZoneData(lstZoneData, SHEET, HEADERs, TITLE);
        return in;
    }

    private void saveZoneDataValues(List<String> lstZoneDataValue, List<ZoneData> listZoneData, Long zoneId,
            Integer domainId) {
        List<String> zoneDataValueOld = new ArrayList<>();
        for (String zoneDataValue : lstZoneDataValue) {
            if (zoneDataValueOld.indexOf(zoneDataValue) < 0) {
                ZoneData zoneData = new ZoneData();
                zoneData.setId(null);
                zoneData.setZoneDataValue(zoneDataValue);
                LocalDateTime now = LocalDateTime.now();
//                zoneData.setUpdateDate(Timestamp.valueOf(now));
                zoneData.setDomainId(domainId);
                zoneData.setZoneId(zoneId);
                listZoneData.add(zoneData);
            }
            zoneDataValueOld.add(zoneDataValue);
        }
        listZoneData = zoneDataRepository.saveAll(listZoneData);
    }

    private List<String> importTxtFile(MultipartFile readDataFile) {
        List<String> lstZoneDataValue = new ArrayList<>();
        try {
            InputStream inputStream = readDataFile.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String data;
            while ((data = bufferedReader.readLine()) != null) {
                data = data.trim();
                if (!StringUtils.isEmpty(data) && !data.equals("\n")) {
                    lstZoneDataValue.add(data);
                }
            }
            reader.close();
        } catch (IOException e) {
            log.error("Cannot read file", e);
        }
        return lstZoneDataValue;
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

}
