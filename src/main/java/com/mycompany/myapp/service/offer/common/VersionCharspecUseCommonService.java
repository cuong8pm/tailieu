package com.mycompany.myapp.service.offer.common;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.repository.characteristic.CharacteristicRepository;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.offer.common.VersionCharspecUseCommon;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecUse;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.dto.offer.common.CharValueUseMapDTO;
import com.mycompany.myapp.dto.offer.common.VersionCharspecUseCommonDTO;
import com.mycompany.myapp.mapper.offer.common.VersionCharspecUseCommonMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.offer.common.VersionCharspecUseCommonRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecUseRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecValueUseRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public abstract class VersionCharspecUseCommonService<D extends VersionCharspecUseCommonDTO, E extends VersionCharspecUseCommon> extends AbstractCloneMappingService {

    protected VersionCharspecUseCommonRepository<E> commonRepository;

    protected VersionCharspecUseCommonMapper<D, E> commonMapper;

    @Autowired
    private OfferVersionCharspecValueUseRepository offerVersionCharspecValueUseRepository;

    @Autowired
    private CharacteristicRepository characteristicRepository;

    public void addCharacteristic(Long id, List<CharValueUseMapDTO> dtos) {
        Integer domainId = OCSUtils.getDomain();
        for (CharValueUseMapDTO item : dtos) {
            Characteristic characteristic = characteristicRepository.findByIdAndDomainId(item.getCharspecId(),domainId);
            if(characteristic == null){
                throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND, Resources.CHARACTERISTIC, ErrorKey.ENTITY_NOT_FOUND);
            }
            D dto = getDto();
            dto.setCharspecId(item.getCharspecId());
            dto.setDomainId(domainId);
            E commonEntity = commonMapper.toEntity(dto);
            setVersionIdForInsert(id, commonEntity);
            commonEntity = commonRepository.save(commonEntity);
            if (commonEntity instanceof OfferVersionCharspecUse) {
                OfferVersionCharspecValueUse valueUse = new OfferVersionCharspecValueUse(null, commonEntity.getId(), item.getValue(), domainId, null, null, null);
                offerVersionCharspecValueUseRepository.save(valueUse);
            }
        }
        List<Long> charspecIds = commonRepository.getListCharspecId(id, domainId);
        Set<Long> charspecIdSet = new HashSet<>();
        charspecIdSet.addAll(charspecIds);
        if (charspecIds.size() != charspecIdSet.size()) {
            throw new DataConstrainException(ErrorMessage.OfferVersion.DUPLICATED_CHARSPEC, "", ErrorKey.OfferVersion.CHARSPEC);
        }
    }

    public void delete(Long id) {
        Integer domainId = OCSUtils.getDomain();
        if (commonRepository instanceof OfferVersionCharspecUseRepository) {
            offerVersionCharspecValueUseRepository.deleteByCharspecUseIdAndDomainId(id, domainId);
        }
        commonRepository.deleteByIdAndDomainId(id, domainId);
    }

    public abstract void setVersionIdForInsert(Long id, E commonEntity);

    public abstract D getDto();

    @Override
    public void setBaseRepository() {
    }
}
