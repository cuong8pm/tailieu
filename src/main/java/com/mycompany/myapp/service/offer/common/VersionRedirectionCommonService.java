package com.mycompany.myapp.service.offer.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.common.VersionRedirectionCommon;
import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import com.mycompany.myapp.dto.offer.common.VersionRedirectionCommonDTO;
import com.mycompany.myapp.mapper.offer.common.VersionRedirectionCommonMapper;
import com.mycompany.myapp.repository.offer.common.VersionRedirectionCommonRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public abstract class VersionRedirectionCommonService<E extends VersionRedirectionCommon, D extends VersionRedirectionCommonDTO>
        extends AbstractCloneMappingService {

    protected VersionRedirectionCommonRepository<E> commonRepository;

    protected VersionRedirectionCommonMapper<D, E> commonMapper;

    @Override
    public void setBaseRepository() {    
    }

    public D create(D dto) {
        Integer domainId = OCSUtils.getDomain();
        checkVersion(dto);
        VersionRedirectionCommon entity = commonMapper.toEntity(dto);
        entity.setDomainId(domainId);
        entity = commonRepository.save(entity);
        dto.setId(entity.getId());
        return dto;
    }

    public D update(D dto) {
        Integer domainId = OCSUtils.getDomain();
        VersionRedirectionCommon common = commonRepository.findByIdAndDomainId(dto.getId(), domainId);
        if (common == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, getResources(), ErrorKey.VersionRedirectionCommon.VERSION_ID);
        }
        checkVersion(dto);
        VersionRedirectionCommon entity = commonMapper.toEntity(dto);
        entity.setDomainId(domainId);
        entity = commonRepository.save(entity);
        dto.setId(entity.getId());
        return dto;
    }

    public D getDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        E entity = commonRepository.findByIdAndDomainId(id, domainId);
        D dto = commonMapper.toDto(entity);
        return setReferName(dto);
    }

    public void delete(Long id) {
        Integer domainId = OCSUtils.getDomain();
        commonRepository.deleteByIdAndDomainId(id, domainId);
    }

    public Page<OfferOfVersionRedirectionDTO> findVersionRedirections(Long versionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<OfferOfVersionRedirectionDTO> dtos = commonRepository.findOfferVersionRedirections(ReferType.RedirectionType, versionId, domainId, pageable);
        return dtos;
    }

    public abstract D setReferName(D dto);

    public abstract void checkVersion(D dto);
    
    public abstract String getResources();

}
