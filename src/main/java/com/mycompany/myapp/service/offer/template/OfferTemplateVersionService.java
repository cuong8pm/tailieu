package com.mycompany.myapp.service.offer.template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.service.exception.*;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import com.mycompany.myapp.domain.offer.offer.OfferRelationship;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.offer.common.MaxNumberDTO;
import com.mycompany.myapp.dto.offer.common.OfferRelationshipCustomDTO;
import com.mycompany.myapp.dto.offer.common.RelationshipMappingOfferCommonDTO;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionActionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionRedirectionDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.action.ActionRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRelationshipRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionActionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionBundleRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionCharspecUseRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRedirectionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRelationshipRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRepository;
import com.mycompany.myapp.service.action.ActionPriceComponentMapService;
import com.mycompany.myapp.service.action.ActionService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.clone.OCSCloneService;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.OfferVersionCommonService;
import com.mycompany.myapp.service.offer.offer.OfferRelationshipService;
import com.mycompany.myapp.service.offer.offer.OfferService;
import com.mycompany.myapp.service.offer.offer.OfferVersionActionService;
import com.mycompany.myapp.service.offer.offer.OfferVersionBundleService;
import com.mycompany.myapp.service.offer.offer.OfferVersionCharspecUseService;
import com.mycompany.myapp.service.offer.offer.OfferVersionRedirectionService;
import com.mycompany.myapp.service.offer.offer.OfferVersionService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.util.BooleanUtils;

@Service
@Transactional
@Order(value = 1)
public class OfferTemplateVersionService extends OfferVersionCommonService<OfferTemplateVersion, OfferTemplateVersionDTO> {

    @Autowired
    private OfferTemplateVersionRepository offerTemplateVersionRepository;

    @Autowired
    private OfferTemplateVersionMapper offerTemplateVersionMapper;

    @Autowired
    private OfferTemplateService offerTemplateService;

    @Autowired
    private OfferTemplateRepository offerTemplateRepository;

    @Autowired
    private OfferVersionRepository offerVersionRepository;

    @Autowired
    private OfferTemplateVersionRedirectionRepository offerTemplateVersionRedirectionRepository;

    @Autowired
    private OfferTemplateVersionCharspecUseRepository offerTemplateVersionCharspecUseRepository;

    @Autowired
    private OfferRelationshipRepository offerRelationshipRepository;

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferVersionService offerVersionService;

    @Autowired
    private OfferVersionCharspecUseService offerVersionCharspecUseService;

    @Autowired
    private OfferVersionRedirectionService offerVersionRedirectionService;

    @Autowired
    private ActionService actionService;

    @Autowired
    private OfferVersionActionService offerVersionActionService;

    @Autowired
    private OfferTemplateVersionActionRepository offerTemplateVersionActionRepository;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private List<ActionPriceComponentMapService> actionPriceComponentMapServices;

    @Autowired
    private OfferVersionBundleService offerVersionBundleService;

    @Autowired
    private OfferTemplateVersionActionService offerTemplateVersionActionService;

    @Autowired
    private List<OfferTemplateVersionActionService> offerTemplateVersionActionServices;

    @Autowired
    private OfferTemplateVersionBundleRepository offerTemplateVersionBundleRepository;

    @Autowired
    private OfferRelationshipService offerRelationshipService;

    @Autowired
    private OfferTemplateVersionRelationshipRepository offerTemplateVersionRelationshipRepository;

    @Autowired
    private List<OfferTemplateService> offerTemplateServices;

    @Autowired
    private List<OfferTemplateVersionChildCloneable> childCloneables;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = offerTemplateVersionRepository;
        this.parentService = offerTemplateService;
        this.ocsCloneableRepository = offerTemplateVersionRepository;
        this.cloneMappingServices = offerTemplateVersionActionServices;
        this.offerVersionCommonMapper = offerTemplateVersionMapper;
        this.offerVersionCommonRepository = offerTemplateVersionRepository;
        this.offerCommonRepository = offerTemplateRepository;
        this.parentDependServices = offerTemplateServices;
        this.childServices = childCloneables;
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new OfferTemplateVersionDTO();
    }

    @Override
    public MaxNumberDTO getMaxNumberOfVersion(Long parentId) {
        MaxNumberDTO maxNumber = new MaxNumberDTO();
        maxNumber.setMaxNumber(offerTemplateVersionRepository.getMaxNumber(parentId));
        return maxNumber;
    }

    @Override
    public String getResourceName() {
        return Resources.OFFER_TEMPLATE_VERSION;
    }

    @Override
    protected void checkExistVersion(Long versionId, Integer domainId) {
        OfferTemplateVersion offerTemplateVersion = offerTemplateVersionRepository.findByIdAndDomainId(versionId, domainId);
        if (offerTemplateVersion == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.ID);
        }
    }

    @Override
    protected OfferTemplateVersionDTO setOfferTemplateOfOfferVersion(OfferTemplateVersionDTO offerVersionCommonDTO) {
        return offerVersionCommonDTO;
    }

    public void deleteOfferTemplateVersion(Long offerTemplateVersionId) {
        Integer domainId = OCSUtils.getDomain();
        OfferTemplateVersion offerTemplateVersion = offerTemplateVersionRepository.findByIdAndDomainId(offerTemplateVersionId, domainId);
        if (offerTemplateVersion == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.ID);
        }
        List<OfferVersion> offerVersion = offerVersionRepository.findByOfferTemplateVersionIdAndDomainId(offerTemplateVersionId, domainId);
        if (!CollectionUtils.isEmpty(offerVersion)) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.ID);
        }
        offerTemplateVersionRedirectionRepository.deleteByOfferTemplateVersionId(offerTemplateVersionId);
        offerTemplateVersionCharspecUseRepository.deleteByOfferTemplateVersionId(offerTemplateVersionId);
        offerTemplateVersionBundleRepository.deleteByOfferTemplateVersionIdAndDomainId(offerTemplateVersionId, domainId);
        offerTemplateVersionActionRepository.deleteByDomainIdAndParentId(domainId, offerTemplateVersionId);
        offerTemplateVersionRelationshipRepository.deleteByDomainIdAndParentId(domainId, offerTemplateVersionId);
        offerTemplateVersionRepository.deleteById(offerTemplateVersionId);
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerTemplateVersionRepository;
    }

    public OfferDTO createOffer(OfferDTO dto, Long offerTemplateVersionId) {
        Integer domainId = OCSUtils.getDomain();
        if (dto.getId() == null || dto.getId() == 0) {
            dto.setId(offerService.create(dto).getId());
        } else {
            offerService.update(dto, dto.getId());
        }
        addListChilds(dto.getListChilds(), domainId, dto.getId());
        addListParents(dto.getListParents(), domainId, dto.getId());
        Long versionId = createVersion(dto.getVersion(), offerTemplateVersionId, dto.getId());
        addRedirection(dto.getVersion().getRedirections(), versionId);

        offerVersionCharspecUseService.addCharacteristic(versionId, dto.getVersion().getCharacteristics());
        addAction(dto.getVersion().getActions(), versionId);
        if (!CollectionUtils.isEmpty(dto.getVersion().getActionIds())) {
            addActionNotRemove(dto.getVersion().getActionIds(), versionId);
        }
        addBundle(dto.getVersion().getBunndles(), versionId);
        dto.getVersion().setId(versionId);
        return dto;
    }

    private void addBundle(List<Long> bunndles, Long versionId) {
        offerVersionBundleService.createOfferVersionBundle(bunndles, versionId);
    }

    private void addAction(List<CloneDTO> cloneDtos, Long versionId) {
        List<OfferVersionActionDTO> offerVersionActionDTOs = new ArrayList<>();
        Map<Long, CloneDTO> mapId = new HashMap<>();
        List<Long> actionId = actionService.cloneEntities(cloneDtos, mapId).stream().map(a -> a.getId()).collect(Collectors.toList());
        for (Long aLong : actionId) {
            OfferVersionActionDTO offerVersionActionDTO = new OfferVersionActionDTO();
            offerVersionActionDTO.setActionId(aLong);
            offerVersionActionDTOs.add(offerVersionActionDTO);
        }
        offerVersionActionService.create(offerVersionActionDTOs, versionId);
    }

    private void addActionNotRemove(List<Long> actionIds, Long versionId) {
        List<OfferVersionActionDTO> offerVersionActionDTOs = new ArrayList<>();
        for (Long actionId : actionIds) {
            OfferVersionActionDTO offerVersionActionDTO = new OfferVersionActionDTO();
            offerVersionActionDTO.setActionId(actionId);
            offerVersionActionDTOs.add(offerVersionActionDTO);
        }
        offerVersionActionService.create(offerVersionActionDTOs, versionId);
    }

    private void addRedirection(List<OfferVersionRedirectionDTO> redirections, Long versionId) {
        for (OfferVersionRedirectionDTO item : redirections) {
            item.setVersionId(versionId);
            offerVersionRedirectionService.create(item);
        }
    }

    private Long createVersion(OfferVersionDTO offerVersionDTO, Long offerTemplateVersionId, Long id) {
        offerVersionDTO.setOfferTemplateVersionId(offerTemplateVersionId);
        offerVersionDTO.setOfferId(id);
        offerVersionDTO.setNumber(offerVersionService.getMaxNumberOfVersions(offerVersionDTO.getNumber(),offerVersionDTO.getOfferId()));
        BaseResponseDTO response = offerVersionService.create(offerVersionDTO);
        return response.getId();
    }

    private void addListParents(List<OfferRelationshipCustomDTO> listParents, Integer domainId, Long id) {
        for (OfferRelationshipCustomDTO item : listParents) {
            offerRelationshipService.createMapping(item.getOfferId(), id, item.getRelationshipTypes(), null);
        }
        Set<RelationshipMappingOfferCommonDTO> setParents = new HashSet<>();
        List<RelationshipMappingOfferCommonDTO> listParentRelationship = offerRelationshipRepository.getParentRelationShip(id, domainId);
        setParents.addAll(listParentRelationship);
        if (setParents.size() != listParentRelationship.size()) {
            throw new DataInvalidException(ErrorMessage.OfferVersion.DUPLICATED_PARENT_ROW_ID, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.Offer.ID);
        }
    }

    private void addListChilds(List<OfferRelationshipCustomDTO> listChilds, Integer domainId, Long id) {
        for (OfferRelationshipCustomDTO item : listChilds) {
            offerRelationshipService.createMapping(id, item.getOfferId(), item.getRelationshipTypes(), null);
        }
        Set<RelationshipMappingOfferCommonDTO> setChilds = new HashSet<>();
        List<RelationshipMappingOfferCommonDTO> listChildRelationship = offerRelationshipRepository.getChildRelationShip(id, domainId);
        setChilds.addAll(listChildRelationship);
        if (setChilds.size() != listChildRelationship.size()) {
            throw new DataInvalidException(ErrorMessage.OfferVersion.DUPLICATED_CHILD_ROW_ID, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.Offer.ID);
        }
    }

    @Override
    public List<Long> removeId(Long offerVersionId) {
        List<Long> removeIds = offerTemplateVersionActionService.getActionId(offerVersionId);
        return removeIds;
    }

    @Override
    protected Page<Action> getAllActionByVersionId(Long offerVersionCommonId, Pageable pageable, Integer domainId) {
        return actionRepository.findAllActionByOfferTemplateVersionId(offerVersionCommonId, domainId, pageable);
    }

    @Override
    protected String getNameCategory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void getParentDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices,
                                      CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();

//        List<OCSMoveableEntity> dependDBs = getParentEntity(ids, domainId, categoryType);

        for (OCSCloneService parentDependService : parentDependServices) {
            if (parentDependService != null) {
//                ids = dependDBs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
                parentDependService.getParentDependencies(dependencies, ids, true, getCategoryType());
            }
        }
    }

    @Override
    protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        return offerTemplateRepository.findOfferTemplateByOfferTemplateVersionIdAndDomainId(ids, domainId);
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.OFFER_TEMPlATE_VERSION;
    }

    @Override
    protected void cloneOfferVersionRelationship(Boolean isCloneRelationship, Long oldParentId, Integer domainId, Long newParentId) {
        if (BooleanUtils.isTrue(isCloneRelationship)) {
            List<OfferTemplateVersionRelationship> listToClone = offerTemplateVersionRelationshipRepository.findByParentIdAndDomainId(oldParentId, domainId);
            List<OfferTemplateVersionRelationship> clonedList = new ArrayList<>();
            for (OfferTemplateVersionRelationship cloneElement : listToClone) {
                OfferTemplateVersionRelationship clonedChildEntity = SerializationUtils.clone(cloneElement);
                clonedChildEntity.setId(null);
                clonedChildEntity.setOfferTemplateVersionId(newParentId);
                clonedList.add(clonedChildEntity);
            }
            List<OfferTemplateVersionRelationship> cloneableChildEntity = offerTemplateVersionRelationshipRepository.saveAll(clonedList);
        }
    }

    public Long getMaxNumberOfVersions(Long numberDTO, Long parentId) {
        Long maxDB = offerTemplateVersionRepository.getMaxNumber(parentId);
        if(maxDB == null)
            return 1L;
        if(numberDTO <= maxDB)
            return maxDB +1L;
        return numberDTO;
    }
}
