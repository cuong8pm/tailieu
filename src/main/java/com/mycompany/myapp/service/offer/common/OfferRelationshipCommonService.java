package com.mycompany.myapp.service.offer.common;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.common.OfferRelationshipCommon;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.offer.common.OfferCustomDTO;
import com.mycompany.myapp.dto.offer.common.OfferRelationshipCommonDTO;
import com.mycompany.myapp.mapper.ReferTableMapper;
import com.mycompany.myapp.mapper.offer.common.OfferRelationshipCommonMapper;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.offer.common.OfferRelationshipCommonRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public abstract class OfferRelationshipCommonService<D extends OfferRelationshipCommonDTO, E extends OfferRelationshipCommon> extends AbstractCloneMappingService {

    protected OfferRelationshipCommonRepository<E> commonRepository;
    protected OfferRelationshipCommonMapper<D, E> commonMapper;

    @Autowired
    private ReferTableRepository referTableRepository;
    
    @Autowired
    private ReferTableMapper referTableMapper;
    
    public Page<OfferCustomDTO> getChild(@Positive Long id, Integer type, Boolean isParent, String name, Pageable pageable) {
        return getListChild(id, type, isParent, name, pageable);
    }

    public void delete(Long parentId, Long childId, List<Integer> relationshipTypes, Boolean isParent) {
        Integer domainId = OCSUtils.getDomain();
        for (Integer item : relationshipTypes) {
            if (isParent != null) {
                commonRepository.deleteByParentIdAndChildIdAndRelationshipTypeAndIsParentAndDomainId(parentId, childId, item, isParent, domainId);
            }
            else {
                commonRepository.deleteByParentIdAndChildIdAndRelationshipTypeAndDomainId(parentId, childId, item, domainId);
            }
        }
    }

    public List<ReferTableDTO> getListRelationShip(Long parentId, Long childId, Boolean isParent) {
        Integer domainId = OCSUtils.getDomain();
        List<ReferTableDTO> list = referTableMapper.toDto(referTableRepository.findByReferTypeOrderByValueAsc(ReferType.OfferRelationshipType));
        List<Integer> listUses = new ArrayList<>();
        if (parentId != null && childId != null) {
            listUses = commonRepository.getListRelationshipUse(parentId, childId, isParent, domainId);
        }
        for (ReferTableDTO item : list) {
            item.setIsUsed(false);
            for (Integer value : listUses) {
                if (item.getValue().equals(value)) {
                    item.setIsUsed(true);
                    break;
                }
            }
        }
        return list;
    }

    public void createMapping(Long parentId, Long childId, List<ReferTableDTO> relationshipTypes, Boolean isParent) {
        Integer domainId = OCSUtils.getDomain();
        for (ReferTableDTO item : relationshipTypes) {
            if (item.getActionType() == ActionType.DELETE) {
                commonRepository.deleteByParentIdAndChildIdAndRelationshipTypeAndDomainId(parentId, childId, item.getValue(), domainId);
            }
            else {
                E entity = getEntity();
                entity.setDomainId(domainId);
                setProperties(entity, parentId, childId, isParent);
                entity.setRelationshipType(item.getValue());
                E mapping = commonRepository.findByParentIdAndChildIdAndRelationshipTypeAndDomainId(parentId, childId, item.getValue(), domainId);
                if (mapping == null) {
                    commonRepository.save(entity);
                }
                else {
                    throw new DataInvalidException(ErrorMessage.Offer.DUPLICATED_RELATIONSHIP, "", ErrorKey.Offer.ID);
                }
            }
        }
    }

    protected abstract E getEntity();

    protected abstract void setProperties(E entity, Long parentId, Long childId, Boolean isParent);
    
    protected abstract Page<OfferCustomDTO> getListChild(Long id, Integer type, Boolean isParent, String name, Pageable pageable);
}
