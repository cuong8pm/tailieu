package com.mycompany.myapp.service.offer.template;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionCharspecUse;
import com.mycompany.myapp.dto.offer.common.CharOfVersionDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionCharspecUseDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionCharspecUseMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionCharspecUseRepository;
import com.mycompany.myapp.service.characteristic.CharacteristicService;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildMappingCloneable;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.VersionCharspecUseCommonService;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public class OfferTemplateVersionCharspecUseService extends VersionCharspecUseCommonService<OfferTemplateVersionCharspecUseDTO, OfferTemplateVersionCharspecUse> implements OfferTemplateVersionChildCloneable{

    @Autowired
    OfferTemplateVersionCharspecUseRepository offerTemplateVersionCharspecUseRepository;

    @Autowired
    OfferTemplateVersionCharspecUseMapper offerTemplateVersionCharspecUseMapper;

    @Autowired
    private CharacteristicService characteristicService;

    @Autowired
    private OfferTemplateVersionService offerTemplateVersionService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = offerTemplateVersionCharspecUseRepository;
        this.parentService = offerTemplateVersionService;
        this.parentDependService = offerTemplateVersionService;
        this.commonRepository = offerTemplateVersionCharspecUseRepository;
        this.commonMapper = offerTemplateVersionCharspecUseMapper;
    }

    @Override
    public void setVersionIdForInsert(Long id, OfferTemplateVersionCharspecUse commonEntity) {
        commonEntity.setOfferTemplateVersionId(id);

    }

    @Override
    public OfferTemplateVersionCharspecUseDTO getDto() {
        return new OfferTemplateVersionCharspecUseDTO();
    }

    public Page<CharOfVersionDTO> listChildCharacteristic(Long id, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<CharOfVersionDTO> page = offerTemplateVersionCharspecUseRepository.getListChildCharacteristic(id, domainId, pageable);
        return page;
    }

    @Override
    protected String getResource() {
        return null;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerTemplateVersionCharspecUseRepository;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
