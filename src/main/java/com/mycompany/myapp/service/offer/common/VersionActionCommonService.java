package com.mycompany.myapp.service.offer.common;

import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.common.VersionActionCommonDTO;
import com.mycompany.myapp.mapper.offer.common.VersionActionCommonMapper;
import com.mycompany.myapp.repository.action.ActionRepository;
import com.mycompany.myapp.repository.offer.common.VersionActionCommonRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionActionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionActionRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Transactional
public abstract class VersionActionCommonService<D extends VersionActionCommonDTO, E extends VersionActionCommon> extends AbstractCloneMappingService {
    protected VersionActionCommonRepository<E> commonRepository;

    protected VersionActionCommonMapper<D, E> commonMapper;

    @Autowired
    private ActionRepository actionRepository;

    public void delete(Long id, Integer posIndex, Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
        commonRepository.deleteByIdAndDomainId(id, domainId);
        commonRepository.updatePosIndexDelete(offerVersionId, posIndex, domainId);
    }

    protected abstract void checkExistVersion(Long versionId, Integer domainId);


    public void create(List<D> versionActionDTOs, Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
        for (D dto : versionActionDTOs) {
            Action action = actionRepository.findByIdAndDomainId(dto.getActionId(),domainId);
            if(action == null){
                throw new ResourceNotFoundException(ErrorMessage.Action.NOT_FOUND, Resources.ACTION, ErrorKey.Action.ID);
            }
            E entity = commonMapper.toEntity(dto);
            checkDataContrain(entity.getActionId(), domainId, offerVersionId);
            entity.setDomainId(domainId);
            entity.setParentMappingId(offerVersionId);
            checkExistVersion(offerVersionId, domainId);
            setPosIndex(domainId, entity, entity.getParentId());
            entity.setAllowMobileMoney(false);
            commonRepository.save(entity);
        }
    }

    public BaseResponseDTO save(D dto, Long offerVersionId, Integer posIndex) {
        Integer domainId = OCSUtils.getDomain();
        E entity = commonMapper.toEntity(dto);
        checkDataContrain(entity.getActionId(), domainId, offerVersionId);
        entity.setDomainId(domainId);
        entity.setParentMappingId(offerVersionId);
        entity.setPosIndex(posIndex + 1);
        checkExistVersion(offerVersionId, domainId);
        entity.setAllowMobileMoney(false);
        commonRepository.updatePosIndex(offerVersionId, posIndex, domainId);
        commonRepository.save(entity);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(entity, response);
        return response;
    }

    public abstract void checkDataContrain(Long childId, Integer domianId, Long parentId);

    public void setPosIndex(Integer domainId, E versionActionCommon, Long parentId) {
        E entity = commonRepository.findTopByParentIdAndDomainIdOrderByPosIndexDesc(parentId, domainId);
        if (entity == null || entity.getPosIndex() == null) {
            versionActionCommon.setPosIndex(0);
        } else {
            versionActionCommon.setPosIndex(entity.getPosIndex() + 1);
        }
    }

    public void update(D versionActionDTO) {
        Integer domainId = OCSUtils.getDomain();
        E entityCheck = commonRepository.findByIdAndDomainId(versionActionDTO.getId(), domainId);
        if (entityCheck == null) {
            throw new ResourceNotFoundException(ErrorMessage.OfferVersionAction.NOT_FOUND, getResource(), ErrorKey.OfferVersionAction.ID);
        }
        entityCheck.setAllowMobileMoney(versionActionDTO.getAllowMobileMoney());
        commonRepository.save(entityCheck);
    }


    public void moveVersionAction(List<? extends VersionActionCommonDTO> versionActionCommonDTOS, Long parentId) {
        Integer domainId = OCSUtils.getDomain();

        VersionActionCommon firstEntity = commonRepository.findByIdAndDomainIdAndParentId(versionActionCommonDTOS.get(0).getId(), domainId, parentId);
        checkNull(firstEntity);
        VersionActionCommon secondEntity = commonRepository.findByIdAndDomainIdAndParentId(versionActionCommonDTOS.get(1).getId(), domainId, parentId);
        checkNull(secondEntity);

        if (!Objects.equals(firstEntity.getParentId(), secondEntity.getParentId())) {
            if (commonRepository instanceof OfferVersionActionRepository) {
                throw new PermissionDeniedException(ErrorMessage.OfferVersionAction.PARENT_INCORRECT, getResource(), ErrorKey.OfferVersionAction.PARENT_ID);
            }
            if (commonRepository instanceof OfferTemplateVersionActionRepository) {
                throw new PermissionDeniedException(ErrorMessage.OfferVersionAction.PARENT_INCORRECT, getResource(), ErrorKey.OfferVersionAction.PARENT_ID);
            }
            throw new PermissionDeniedException(ErrorMessage.OfferVersionAction.PARENT_INCORRECT, "", ErrorKey.POS_INDEX);
        }

//        if (!Objects.equals(versionActionCommonDTOS.get(0).getPosIndex(), firstEntity.getPosIndex())) {
//            throw new ResourceConflictException(ErrorMessage.POS_INDEX_INCORRECT, getResource(),
//                ErrorKey.Category.POS_INDEX);
//        }
//        if (!Objects.equals(versionActionCommonDTOS.get(1).getPosIndex(), secondEntity.getPosIndex())) {
//            throw new ResourceConflictException(ErrorMessage.POS_INDEX_INCORRECT, getResource(),
//                ErrorKey.Category.POS_INDEX);
//        }
//        Integer tempPosIndex = firstEntity.getPosIndex();
//        firstEntity.setPosIndex(secondEntity.getPosIndex());
//        secondEntity.setPosIndex(tempPosIndex);
        firstEntity.setPosIndex(versionActionCommonDTOS.get(0).getPosIndex());
        secondEntity.setPosIndex(versionActionCommonDTOS.get(1).getPosIndex());
        commonRepository.save(firstEntity);
        commonRepository.save(secondEntity);
    }

    private void checkNull(VersionActionCommon versionActionCommon) {
        if (versionActionCommon == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, "", ErrorKey.ID);
        }
    }

    public abstract List<Long> getActionId(Long versionId);
}
