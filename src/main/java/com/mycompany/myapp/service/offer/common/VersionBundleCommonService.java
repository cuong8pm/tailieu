package com.mycompany.myapp.service.offer.common;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;
import com.mycompany.myapp.dto.offer.common.VersionBundleCommonDTO;
import com.mycompany.myapp.mapper.offer.common.VersionBundleCommonMapper;
import com.mycompany.myapp.repository.offer.common.VersionBundleCommonRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public abstract class VersionBundleCommonService<D extends VersionBundleCommonDTO, E extends VersionBundleCommon> extends AbstractCloneMappingService{
    protected VersionBundleCommonRepository<E> commonRepository;

    protected VersionBundleCommonMapper<D, E> commonMapper;
    
    public void createOfferVersionBundle(List<Long> ids, Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateOffer(ids, versionId);
        for (int i = 0; i < ids.size(); i++) {
           E entity = getEntity();
           setVersionIdForInsert(versionId,entity);
           entity.setChildOfferId(ids.get(i));
           entity.setDomainId(domainId);
           commonRepository.save(entity);
        }
    }
    public void delete(Long id) {
        Integer domainId = OCSUtils.getDomain();
        commonRepository.deleteByIdAndDomainId(id, domainId);
    }
    
    public void updateBundle(Long id , Long versionId, Long childId ) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateChild(childId,versionId);
        E offerBundle = commonRepository.findByIdAndDomainId(id, domainId);
        offerBundle.setChildOfferId(childId);
        commonRepository.save(offerBundle);
    }
    
    public abstract void checkDuplicateOffer(List<Long> ids, Long versionId);
    public abstract E getEntity();
    public abstract void setVersionIdForInsert(Long versionId , E entityID);
    public abstract void checkDuplicateChild(Long childId , Long versionId);
    public abstract List<Long> getOfferId(Long versionId);

    
    @Override
    public void setBaseRepository() {    
    }
}
