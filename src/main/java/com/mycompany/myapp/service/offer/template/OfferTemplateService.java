package com.mycompany.myapp.service.offer.template;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionAction;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.offer.common.OfferCloneDTO;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionDTO;
import com.mycompany.myapp.mapper.offer.template.DropDownOfferTemplateVersionDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateMapper;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionMapper;
import com.mycompany.myapp.mapper.offer.template.TreeDependenciesOfferVersionDTO;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionActionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRelationshipRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferCommonService;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class OfferTemplateService extends OfferCommonService<OfferTemplateDTO, OfferTemplate> {
    @Autowired
    private OfferTemplateRepository offerTemplateRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferTemplateMapper offerTemplateMapper;

    @Autowired
    private OfferTemplateVersionMapper offerTemplateVersionMapper;

    @Autowired
    private OfferTemplateVersionRepository offerTemplateVersionRepository;

    @Autowired
    private OfferTemplateVersionBundleService offerTemplateVersionBundleService;

    @Autowired
    private OfferTemplateVersionService offerTemplateVersionService;

    @Autowired
    private List<OfferTemplateVersionActionService> offerTemplateVersionActionServices;

    @Autowired
    private List<OfferTemplateVersionChildCloneable> childCloneables;

    @Autowired
    private OfferVersionRepository offerVersionRepository;

    @Autowired
    private OfferTemplateVersionActionRepository offerTemplateVersionActionRepository;

    @Autowired
    private OfferTemplateVersionRelationshipService offerTemplateVersionRelationshipService;

    @Override
    protected TreeClone getTreeDTO() {
        return new OfferTemplateDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.OFFER_TEMPlATE;
    }

    @Override
    protected String getResourceName() {
        return Resources.OFFER_TEMPLATE;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = offerTemplateRepository;
        this.parentService = this.categoryService;
        this.ocsCloneableRepository = offerTemplateRepository;
        this.offerCommonRepository = offerTemplateRepository;
        this.offerCommonMapper = offerTemplateMapper;
        this.offerVersionCommonRepository = offerTemplateVersionRepository;
        this.offerVersionCommonService = offerTemplateVersionService;
        this.offerVersionCommonMapper = offerTemplateVersionMapper;
        this.cloneMappingServices = offerTemplateVersionActionServices;
        this.childServices = childCloneables;
    }

    @Override
    protected void checkDuplicateExternalId(OfferCommon offerCommon) {
    }

    @Override
    protected OCSCloneableEntity saveCloneEntity(OCSCloneableEntity clonedEntity, Long externalId, String description) {
        OfferTemplate offerTemplate = (OfferTemplate) clonedEntity;
        if (description != null) {
            offerTemplate.setDescription(description);
        }
        clonedEntity = offerTemplateRepository.saveAndFlush(offerTemplate);
        return clonedEntity;
    }

    @Override
    public List<Long> removeId(Long offerTemplateVersionId) {
        List<Long> removeIds = new ArrayList<>();
        removeIds = offerTemplateVersionBundleService.getOfferId(offerTemplateVersionId);
        return removeIds;
    }

    public List<CategoryDTO> checkDependenciesOfOfferTemplate(Long offerTemplateVersionId) {
        Integer domainId = OCSUtils.getDomain();
        List<TreeDependenciesOfferVersionDTO> lstOfferVersion = offerVersionRepository
            .getOfferVersionByOfferTemplateVersion(offerTemplateVersionId);
        List<Long> lstOfferIds = lstOfferVersion.stream().map(i -> i.getParentId()).distinct()
            .collect(Collectors.toList());
        List<CategoryDTO> lstTreeDepend = new ArrayList<>();

        List<Offer> offers = offerRepository.findByIdInAndDomainId(lstOfferIds, domainId);
        Collections.sort(offers, new Comparator<Offer>() {
            @Override
            public int compare(Offer o1, Offer o2) {
                String o1Name = o1.getName();
                String o2name = o2.getName();
                if (o1Name != null && o2name != null) {
                    return StringUtils.compare(o1.getName().toLowerCase(), o2.getName().toLowerCase());
                } else {
                    return StringUtils.compare(o1Name, o2name);
                }
            }
        });
        for (Offer offer : offers) {
            CategoryDTO offerTreeDepend = new CategoryDTO();
            offerTreeDepend.setId(offer.getId());
            offerTreeDepend.setName(offer.getName());
            offerTreeDepend.setType(Resources.OFFER);
            Collection<Tree> lstTemplates = new ArrayList<>();
            List<TreeDependenciesOfferVersionDTO> lstOfferVersionsOfOffer = lstOfferVersion.stream()
                .filter(i -> i.getParentId().equals(offer.getId())).collect(Collectors.toList());
            for (TreeDependenciesOfferVersionDTO offerVersionOfOffer : lstOfferVersionsOfOffer) {
                CategoryDTO offerVersionTreeDepend = new CategoryDTO();
                offerVersionTreeDepend.setName("Version " + offerVersionOfOffer.getNumber());
                offerVersionTreeDepend.setId(offerVersionOfOffer.getId());
                offerVersionTreeDepend.setType(Resources.OFFER_VERSION);
                lstTemplates.add(offerVersionTreeDepend);
            }
            if (!CollectionUtils.isEmpty(lstTemplates)) {
                offerTreeDepend.setHasChild(true);
            }
            offerTreeDepend.setTemplates(lstTemplates);
            lstTreeDepend.add(offerTreeDepend);
        }

        return lstTreeDepend;
    }

    @Override
    public void deleteChildVersion(Long parentId, Integer domainId) {
        List<OfferTemplateVersion> lstChildVersions = offerTemplateVersionRepository.findByParentIdAndDomainId(parentId,
            domainId);
        List<Long> lstChildVersionIds = lstChildVersions.stream().map(i -> i.getId()).collect(Collectors.toList());
        for (Long childVersionIds : lstChildVersionIds) {
            offerTemplateVersionService.deleteOfferTemplateVersion(childVersionIds);
        }
    }

    @Override
    public void validateInputDataClone(OfferCloneDTO offerCloneDTO) {
        if (offerCloneDTO.getName() == null) {
            throw new DataInvalidException(ErrorMessage.Offer.MUST_NOT_BE_NULL, Resources.OFFER_TEMPLATE,
                ErrorKey.NAME);
        }
        if (offerCloneDTO.getState() == null && !CollectionUtils.isEmpty(offerCloneDTO.getChildren())) {
            throw new DataInvalidException(ErrorMessage.Offer.MUST_NOT_BE_NULL, Resources.OFFER_TEMPLATE,
                ErrorKey.OfferTemplate.STATE);
        }
    }

    @Override
    protected String getNameCategory() {
        return "Offer Templates";
    }

    @Override
    public Boolean checkHasChildOfOfferVersion(Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferTemplateVersionAction> lstChildOfferTemplateVersion = offerTemplateVersionActionRepository
            .findByDomainIdAndParentId(domainId, offerVersionId);
        return !CollectionUtils.isEmpty(lstChildOfferTemplateVersion);
    }

    @Override
    public void deleteOfferRelationship(Long offerId, Integer domainId) {
        // TODO Auto-generated method stub
    }

    @Override
    public void validateOfferToDelete(Long id, Integer domainId) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Long> findIds(Long offerTemplateVersionnId) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> removeId = offerTemplateVersionRelationshipService.getIds(offerTemplateVersionnId);
        return removeId;
    }

    @Override
    public void getParentDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices,
                                      CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();

//        List<OfferTemplate> dependDBs = getParentEntity(ids, domainId, categoryType);

//        List<Long> idOffer = dependDBs.stream().map(id -> id.getId()).collect(Collectors.toList());

//        List<OfferTemplateVersion> offerTemplateVersions = offerTemplateVersionRepository.findByOfferTemplateIdInAndDomainId(idOffer, domainId);

        List<OfferTemplateVersion> offerTemplateVersions = offerTemplateVersionRepository.findByIdInAndDomainId(ids, domainId);

        Collections.sort(offerTemplateVersions, new Comparator<OfferTemplateVersion>() {
            @Override
            public int compare(OfferTemplateVersion o1, OfferTemplateVersion o2) {
                return o1.getNumber().compareTo(o2.getNumber());
            }
        });

        List<Long> idOffer = offerTemplateVersions.stream().map(id -> id.getParentId()).collect(Collectors.toList());

        List<OfferTemplate> offerTemplates = offerTemplateRepository.findByIdInAndDomainId(idOffer, domainId);

        Collections.sort(offerTemplates, new Comparator<OfferTemplate>() {
            @Override
            public int compare(OfferTemplate o1, OfferTemplate o2) {
                String o1Name = o1.getName();
                String o2name = o2.getName();
                if (o1Name != null && o2name != null) {
                    return StringUtils.compare(o1.getName().toLowerCase(), o2.getName().toLowerCase());
                } else {
                    return StringUtils.compare(o1Name, o2name);
                }
            }
        });
        List<Tree> depends = new ArrayList<>();
        for (OfferTemplate offerTemplate : offerTemplates) {
            for (OfferTemplateVersion offerTemplateVersion : offerTemplateVersions) {
                if (Objects.equals(offerTemplateVersion.getParentId(), offerTemplate.getId())) {
                    TreeClone tree = new OfferTemplateVersionDTO();
                    BeanUtils.copyProperties(offerTemplateVersion, tree);
                    tree.setName(offerTemplate.getName() + " - Version " + offerTemplateVersion.getNumber());
                    depends.add(tree);
                }
            }
        }

        if (dependencies.stream().noneMatch(i -> i.getName().equals("Offer Templates"))) {
            CategoryDTO categoryDTO = getCategory(getCategoryType());
            if (categoryDTO == null) {
                return;
            }
            categoryDTO.setTemplates(depends);
            categoryDTO.setName(getNameCategory());

            dependencies.add(0, categoryDTO);
            return;
        }

        Optional<Tree> firstOffers = dependencies.stream().filter(i -> i.getName().equals("Offer Templates")).findFirst();

        Collection<Tree> offerTemplates1 = new ArrayList<>();
        if (firstOffers.isPresent()) {
            offerTemplates1 = firstOffers.get().getTemplates();
        }
        Collection<Tree> finalOfferTemplates = offerTemplates1;
        depends.forEach(
            i -> {
                if (finalOfferTemplates.stream().noneMatch(j -> j.getId().equals(i.getId()))) {
                    finalOfferTemplates.add(i);
                }
            }
        );

        CategoryDTO categoryDTO = getCategory(getCategoryType());
        if (categoryDTO == null) {
            return;
        }

        Collections.sort((List) finalOfferTemplates, new Comparator<Tree>() {
            @Override
            public int compare(Tree o1, Tree o2) {
                int indexOfLastSpace1 = o1.getName().lastIndexOf(' ');
                int indexOfLastSpace2 = o2.getName().lastIndexOf(' ');
                String name1 = o1.getName().substring(0, indexOfLastSpace1);
                String name2 = o2.getName().substring(0, indexOfLastSpace2);
                if (name1.equals(name2)) {
                    String version1 = o1.getName().substring(indexOfLastSpace1 + 1);
                    String version2 = o2.getName().substring(indexOfLastSpace1 + 1);
                    return Integer.parseInt(version1) - Integer.parseInt(version2);
                }
                return o1.getName().compareTo(o2.getName());
            }
        });

        categoryDTO.setTemplates(finalOfferTemplates);
        categoryDTO.setName(getNameCategory());

        dependencies.add(0, categoryDTO);
    }


    public List<Long> getOffferId(Long offerVersion) {

        return null;
    }

    @Override
    protected void cloneOfferVersionRelationship(Boolean isCloneRelationship, Long oldParentId, Integer domainId, Long newParentId) {
        offerTemplateVersionService.cloneOfferVersionRelationship(isCloneRelationship, oldParentId, domainId, newParentId);
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.OfferTemplate.DUPLICATED_NAME, Resources.OFFER_TEMPLATE, ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.OfferTemplate.NOT_FOUND, Resources.OFFER_TEMPLATE, ErrorKey.ID);
    }
}
