package com.mycompany.myapp.service.offer.template;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.OCSCloneableRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.offer.common.OfferCustomDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionRelationshipDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionRelationshipMapper;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRelationshipRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.offer.common.OfferRelationshipCommonService;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class OfferTemplateVersionRelationshipService
        extends OfferRelationshipCommonService<OfferTemplateVersionRelationshipDTO, OfferTemplateVersionRelationship>{

    @Autowired
    private OfferTemplateVersionRelationshipRepository offerTemplateVersionRelationshipRepository;

    @Autowired
    private OfferTemplateVersionRelationshipMapper offerTemplateVersionRelationshipMapper;

    @Autowired
    private OfferTemplateVersionRepository offerTemplateVersionRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Override
    protected OfferTemplateVersionRelationship getEntity() {
        return new OfferTemplateVersionRelationship();
    }

    @Override
    protected void setProperties(OfferTemplateVersionRelationship entity, Long parentId, Long childId,
            Boolean isParent) {
        Integer domainId = OCSUtils.getDomain();
        OfferTemplateVersion offerTemplateVersion = offerTemplateVersionRepository.findByIdAndDomainId(parentId,
                domainId);
        if (offerTemplateVersion == null) {
            throw new ResourceNotFoundException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER, ErrorKey.Offer.PARENT_ID);
        }
        Offer offer = offerRepository.findByIdAndDomainId(childId, domainId);
        if (offer == null) {
            throw new ResourceNotFoundException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER, ErrorKey.Offer.CHILD_ID);
        }
        entity.setOfferTemplateVersionId(parentId);
        entity.setOfferId(childId);
        entity.setIsParent(isParent);
    }

    @Override
    protected Page<OfferCustomDTO> getListChild(Long id, Integer type, Boolean isParent, String name,
            Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<OfferCustomDTO> page = offerTemplateVersionRelationshipRepository.getListOffer(id, type, isParent, name,
                domainId, pageable);
        for (OfferCustomDTO item : page) {
            List<ReferTable> referTables = offerTemplateVersionRelationshipRepository.getListRelationshipType(id,
                    item.getId(), type, isParent, domainId);
            item.setRelationshipTypes(referTables);
        }
        return page;
    }

    @Override
    protected String getResource() {
        return Resources.OFFER_TEMPLATE_VERSION;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.commonRepository = offerTemplateVersionRelationshipRepository;
        this.commonMapper = offerTemplateVersionRelationshipMapper;
    }

    public List<Long> getIds(Long offerTemplateVersionnId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferTemplateVersionRelationship> offers = offerTemplateVersionRelationshipRepository
                .findByParentIdAndDomainId(offerTemplateVersionnId, domainId);
        List<Long> removeId = offers.stream().map(x -> x.getOfferId()).collect(Collectors.toList());
        return removeId;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }

}
