package com.mycompany.myapp.service.offer.offer;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.offer.OfferVersionBundle;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.offer.offer.OfferVersionBundleDTO;
import com.mycompany.myapp.dto.offer.offer.VersionBunndleDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferMapper;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionBundleMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionBundleRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.VersionBundleCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class OfferVersionBundleService extends VersionBundleCommonService<OfferVersionBundleDTO, OfferVersionBundle> implements OfferVersionChildCloneable{

    @Autowired
    OfferVersionBundleRepository offerVersionBundleRepository;

    @Autowired
    OfferVersionBundleMapper offerVersionBundleMapper;

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerVersionBundleRepository;
    }

    @Autowired
    private OfferMapper offerMapper;

    @Autowired
    private OfferVersionRepository offerVersionRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private OfferRepository offerRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = offerVersionBundleRepository;
        this.commonRepository = offerVersionBundleRepository;
        this.commonMapper = offerVersionBundleMapper;
    }

    @Override
    protected CategoryType getCategoryType() {
        // khong nam trong tree
        return null;
    }

    public void checkDuplicateOffer(List<Long> offerIds, Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferVersionBundle> list = offerVersionBundleRepository.findByDomainIdAndChildOfferIdInAndVersionId(domainId, offerIds, versionId);
        if (!list.isEmpty()) {
            throw new DataInvalidException(ErrorMessage.Offer.DUPLICATED_OFFER, Resources.OFFER, ErrorKey.Offer.ID);
        }
        OfferVersion offerVersion = offerVersionRepository.findByIdAndDomainId(versionId, domainId);
        for (Long item : offerIds) {
            if (item.compareTo(offerVersion.getOfferId()) == 0) {
                throw new DataInvalidException(ErrorMessage.Offer.CANNOT_ADD, Resources.OFFER, ErrorKey.Offer.ID);
            }
        }
        List<Offer> offers =  offerRepository.findAllById(offerIds);
        List<Long> ids =  offers.stream().map(Offer::getId).collect(Collectors.toList());
        if (ids.size() != offerIds.size()) {
            throw new DataInvalidException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER, ErrorKey.Offer.ID);
        }
    }

    @Override
    public OfferVersionBundle getEntity() {

        return new OfferVersionBundle();
    }

    @Override
    public void setVersionIdForInsert(Long versionId, OfferVersionBundle entityID) {
        entityID.setRootOfferVersionId(versionId);

    }

    public Page<VersionBunndleDTO> findVersionBunndleDTOs(Long versionId , Pageable pageable){
        Integer domainId = OCSUtils.getDomain();
        Page<VersionBunndleDTO> page = offerVersionBundleRepository.findBundles(versionId, domainId,pageable);
        return page;
    }

    public void checkDuplicateChild(Long childId , Long versionId) {
        Integer domainId = OCSUtils.getDomain();
       Integer count= offerVersionBundleRepository.countByDomainIdAndChildOfferIdAndVersionId(domainId, childId, versionId);
      if(count > 0) {
          throw new DataInvalidException(ErrorMessage.OfferVersionBundle.DUPLICATED_CHILDID, Resources.OFFER_VERSION_BUNDLE, ErrorKey.OfferVersionBundle.OFFER);

    }

    }

    @Override
    public List<Long> getOfferId(Long versionId) {
            Integer domainId = OCSUtils.getDomain();
            List<OfferVersionBundle> list =offerVersionBundleRepository.findByDomainIdAndParentId(domainId, versionId);
          List<Long> offerVersionId=  list.stream().map(x -> x.getChildOfferId()).collect(Collectors.toList());
          return offerVersionId;

    }

    @Override
    protected String getResource() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}



