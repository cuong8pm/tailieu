package com.mycompany.myapp.service.offer.offer;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.offer.OfferVersionRedirection;
import com.mycompany.myapp.dto.offer.offer.OfferVersionRedirectionDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionRedirectionMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRedirectionRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.VersionRedirectionCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;


@Service
@Transactional
public class OfferVersionRedirectionService extends VersionRedirectionCommonService<OfferVersionRedirection, OfferVersionRedirectionDTO> implements OfferVersionChildCloneable{

    @Autowired
    private OfferVersionRepository offerVersionRepository;

    @Autowired
    private OfferVersionRedirectionMapper offerVersionRedirectionMapper;

    @Autowired
    private OfferVersionRedirectionRepository offerVersionRedirectionRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Override
    public void checkVersion(OfferVersionRedirectionDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        OfferVersion version = offerVersionRepository.findByIdAndDomainId(dto.getVersionId(), domainId);
        if (version == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_VERSION_REDIRECTION, ErrorKey.VersionRedirectionCommon.VERSION_ID);
        }
    }

    @Override
    public String getResources() {
        return Resources.OFFER_VERSION_REDIRECTION;
    }

    @Override
    public OfferVersionRedirectionDTO setReferName(OfferVersionRedirectionDTO dto) {
        ReferTable referTable = referTableRepository.getReferTableByReferTypeAndValue(ReferType.RedirectionType, dto.getRedirectionType());
        if (referTable != null) {
            dto.setName(referTable.getName());
        }
        return dto;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = offerVersionRedirectionRepository;
        this.commonMapper = offerVersionRedirectionMapper;
        this.commonRepository = offerVersionRedirectionRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        // khong nam trong tree
        return null;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerVersionRedirectionRepository;
    }

    @Override
    protected String getResource() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
