package com.mycompany.myapp.service.offer.offer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecValueUseRepository;
import com.mycompany.myapp.service.normalizer.ChildCloneable;

@Service
public class OfferVersionCharspecValueUseService implements OfferVersionCharSpecValueUseMappingService{
    
    @Autowired
    private OfferVersionCharspecValueUseRepository offerVersionCharspecValueUseRepository;

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerVersionCharspecValueUseRepository;
    }

}
