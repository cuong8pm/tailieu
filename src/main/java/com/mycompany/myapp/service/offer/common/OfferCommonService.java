package com.mycompany.myapp.service.offer.common;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.constant.State;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.dto.offer.common.DropDownBillingCycleDTO;
import com.mycompany.myapp.dto.offer.common.OfferCloneDTO;
import com.mycompany.myapp.dto.offer.common.OfferCommonDTO;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.mapper.offer.common.OfferCommonMapper;
import com.mycompany.myapp.mapper.offer.common.OfferVersionCommonMapper;
import com.mycompany.myapp.repository.BillingCycleRepository;
import com.mycompany.myapp.repository.BillingCycleTypeRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.offer.common.OfferCommonRepository;
import com.mycompany.myapp.repository.offer.common.OfferVersionCommonRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRelationshipRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.ReferTableService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionRelationshipService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.util.BooleanUtils;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public abstract class OfferCommonService<D extends OfferCommonDTO, E extends OfferCommon> extends AbstractCloneService {

    @Autowired
    protected CategoryService categoryService;

    @Autowired
    private BillingCycleTypeRepository billingCycleTypeRepository;

    @Autowired
    private ReferTableService referTableService;

    @Autowired
    private BillingCycleRepository billingCycleRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private OfferTemplateVersionRelationshipRepository offerTemplateVersionRelationshipRepository;

    protected OfferCommonRepository<E> offerCommonRepository;

    protected OfferCommonMapper<D,E> offerCommonMapper;

    protected OfferVersionCommonMapper offerVersionCommonMapper;

    protected OfferVersionCommonRepository offerVersionCommonRepository;

    protected OfferVersionCommonService offerVersionCommonService;

    protected List<? extends ChildCloneable> childServices;

    protected List<? extends OCSCloneMappingService> cloneMappingService;

    @Override
    protected List<CategoryType> dependTypes() {
        return null;
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return null;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return OfferCommon.Length.NAME;
    }

    public OfferCommonDTO create(D offerCommonDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkCategory(offerCommonDTO.getCategoryId());

        OfferCommon offerCommon = offerCommonMapper.toEntity(offerCommonDTO);

        checkDuplicateExternalId(offerCommon);

        checkDuplicateName(offerCommon.getName(), offerCommon.getId());

        offerCommon.getEffDate().setNanos(0);
        offerCommon.getExpDate().setNanos(0);

        if (offerCommon.getEffDate().after(offerCommon.getExpDate())) {
            throw new DataInvalidException(ErrorMessage.Offer.EFFECT_DATE_LESS_THAN_EXPIRE_DATE, getResourceName(), ErrorKey.Offer.EFF_DATE);
        }

        checkExistBillingCycleType(offerCommon.getBillingCycleTypeId());

        offerCommon.setUpdateDate(new Timestamp(new Date().getTime()));
        offerCommon.setDomainId(domainId);
        offerCommon = offerCommonRepository.save(offerCommon);
        OfferCommonDTO offerCommonNew = offerCommonMapper.toDto((E) offerCommon);;

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
            Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.ID));
        List<OfferVersionCommonDTO> list = getListOfferVersion(offerCommonNew.getId(), unpaged).getContent();
        if (!list.isEmpty()) {
            offerCommonNew.setHasChild(true);
        }
        return offerCommonNew;
    }

    public OfferCommonDTO update(D offerCommonDTO, Long offerCommonId) {
        isExistOfferCommon(offerCommonId);

        offerCommonDTO.setId(offerCommonId);
        return create(offerCommonDTO);
    }

    protected void isExistOfferCommon(Long id) {
        Integer domainId = OCSUtils.getDomain();
        E offerCommon = offerCommonRepository.findByIdAndDomainId(id, domainId);
        if (offerCommon == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, getResourceName(), ErrorKey.Offer.ID);
        }
    }

    protected void checkExistBillingCycleType(Long billingCycleTypeId) {
        Integer domainId = OCSUtils.getDomain();
        BillingCycleType billingCycleType = billingCycleTypeRepository.findByIdAndDomainId(billingCycleTypeId, domainId);
        if (billingCycleType == null) {
            throw new DataInvalidException(ErrorMessage.BillingCycle.BILLING_CYCLE_TYPE_ID, getResourceName(), ErrorKey.Offer.BILLING_CYCLE_TYPE_ID);
        }
    }

    protected abstract void checkDuplicateExternalId(OfferCommon offerCommon);

    public Page<D> getOfferCommons(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }

        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;

        Page<D> offerCommonDTOs = offerCommonRepository.findOfferCommons(searchCriteria.getParentId(),
            searchCriteria.getDomainId(),
            searchCriteria.getName(),
            searchCriteria.getDescription(), pageable).map(offerCommonMapper::toDto);

        for (D offerCommonDTO : offerCommonDTOs) {
            offerCommonDTO.setCategoryName(category.getName());
            offerCommonDTO.setCategoryLevel(parentCategoryLevel);
            offerCommonDTO.setHasChild(checkHasChild(offerCommonDTO.getId(), domainId));
        }
        return offerCommonDTOs;
    }

    private boolean checkHasChild(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferVersion = offerVersionCommonRepository.getMaxNumberByDomainIdAndParentId(domainId, id);
        return countReferVersion != null && countReferVersion > 0;
    }

    public D getOfferDetail(Long offerId) {
        Integer domainId = OCSUtils.getDomain();
        E offerCommon = offerCommonRepository.findByIdAndDomainId(offerId, domainId);
        if (offerCommon == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, getResourceName(), ErrorKey.Offer.ID);
        }
        D offerCommonDTO = offerCommonMapper.toDto(offerCommon);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(offerCommon.getCategoryId(), domainId);

        if (category != null) {
            offerCommonDTO.setCategoryId(category.getId());
            offerCommonDTO.setCategoryName(category.getName());
            offerCommonDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }

        return offerCommonDTO;
    }

    public void delete(Long id) {
        Integer domainId = OCSUtils.getDomain();
        E offerCommon = offerCommonRepository.findByIdAndDomainId(id, domainId);
        if (offerCommon == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, getResourceName(), ErrorKey.Offer.ID);
        }

        //TODO
        //delete child version, relationship
        validateOfferToDelete(id,domainId);
        deleteChildVersion(id, domainId);
        deleteOfferRelationship(id, domainId);
        offerCommonRepository.deleteByIdAndDomainId(id, domainId);
    }

    public abstract void validateOfferToDelete(Long id, Integer domainId);

    public abstract void deleteChildVersion(Long parentId, Integer domainId);

    public abstract void deleteOfferRelationship(Long offerId, Integer domainId);

    @Override
    protected void setHasChild(Tree tree) {
        Integer domainId = OCSUtils.getDomain();
        TreeClone treeClone = null;
        if (tree instanceof TreeClone) {
            treeClone = (TreeClone) tree;
        }
        if (treeClone != null) {
            treeClone.setHasChild(checkHasChild(tree.getId(), domainId));
        }
    }

    public abstract void validateInputDataClone(OfferCloneDTO offerCloneDTO);

    @Override
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);
        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if(!Objects.equals(distinctIds.size(), baseToClones.size())){
            throwResourceNotFoundException();
        };
        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
                .collect(Collectors.toMap(OCSCloneableEntity::getId, Function.identity()));
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveEntity(cloneDTO, baseToCloneMap);
            clonedEntities.add(clonedEntity);
        }
        return clonedEntities;
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }

    @Override
    public OCSCloneableEntity saveEntity(CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> baseToCloneMap) {
        OfferCloneDTO offerCloneDTO = (OfferCloneDTO) cloneDTO;

        validateInputDataClone(offerCloneDTO);

        if (offerCloneDTO.getChildren() == null) {
            offerCloneDTO.setChildren(new ArrayList<>());
        }
        Integer domainId = OCSUtils.getDomain();
        String name = offerCloneDTO.getName();
        Long id = offerCloneDTO.getId();

        OCSCloneableEntity baseToClone = offerCommonRepository.findByIdAndDomainId(id, domainId);

        if (baseToClone == null) {
            return null;
        }

        checkDuplicateName(name, id);
        OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
        String newName = "";
        if (StringUtils.isEmpty(name) || name.equals(clonedEntity.getName())) {
            newName = cloneName(baseToClone.getName(), domainId);
        } else {
            newName = name;
        }

        if(baseToClone.getName().equalsIgnoreCase(newName)){
            throwDuplicatedNameException();
        }

        if (getCloneNameMaxLength() != null && newName.length() > getCloneNameMaxLength().intValue()) {
            throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
        }

        clonedEntity.setName(newName);
        setPosIndex(domainId, clonedEntity, clonedEntity.getParentId());

        checkDuplicateExternalIdClone(offerCloneDTO.getExternalId());

        clonedEntity.setId(null);
        clonedEntity = saveCloneEntity(clonedEntity, offerCloneDTO.getExternalId(), offerCloneDTO.getDescription());
        if(clonedEntity instanceof Offer){
            cloneOfferVersionRelationship(offerCloneDTO.getIsCloneRelationship(), id, domainId, clonedEntity.getId());
        }

        List<CloneDTO> cloneDTOs = new ArrayList<>();
        Map<Long, List<Long>> parentChildIdMap = new HashMap<>();
        for(CloneDTO childrenClone : cloneDTO.getChildren()) {
            childrenClone.getChildren().stream().forEach(item -> item.setParentId(childrenClone.getId()));
            cloneDTOs.addAll(childrenClone.getChildren());
            List<Long> childrenIds = new ArrayList<>();
            if (childrenClone.getChildren() != null){
                childrenIds = childrenClone.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }
            parentChildIdMap.put(childrenClone.getId(),childrenIds);
        }
        List<Long> offerVersionIds = cloneDTO.getChildren().stream().map(i -> i.getId()).collect(Collectors.toList());
        cloneChildEntities(offerVersionIds, domainId, clonedEntity, cloneDTOs,
                offerCloneDTO.getIsCloneRelationship(), offerCloneDTO.getState(), parentChildIdMap);
        return clonedEntity;
    }

    protected abstract OCSCloneableEntity saveCloneEntity(OCSCloneableEntity clonedEntity, Long externalId,
                                                          String description);

    protected void cloneChildEntities(List<Long> offerVersionIds, Integer domainId, OCSCloneableEntity clonedEntity,
            List<CloneDTO> cloneDTOs, Boolean isCloneRelationship, State state, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        List<OCSCloneableEntity> lstchildEntityToClone = offerVersionCommonRepository.findByDomainIdAndIdIn(domainId, offerVersionIds);
        Map<Long, CloneDTO> oldNewOfferVersionId = new HashMap<>();
        List<Long> newOfferVersionId = new ArrayList<>();

        List<Long> distinctIds = offerVersionIds.stream().distinct().collect(Collectors.toList());
        if(!Objects.equals(distinctIds.size(), lstchildEntityToClone.size())){
            throwResourceNotFoundException();
        };

        Long number = 1L;
        for (OCSCloneableEntity ocsCloneableEntity : lstchildEntityToClone) {
            OCSCloneableEntity clonedChileEntity = SerializationUtils.clone(ocsCloneableEntity);
            OfferVersionCommon offerVersionEntity = (OfferVersionCommon) clonedChileEntity;
            offerVersionEntity.setId(null);
            offerVersionEntity.setParentId(clonedEntity.getId());
            offerVersionEntity.setNumber(number);
            if(state != null) {
                offerVersionEntity.setState(state);
            }
            OCSCloneableEntity clonedOfferVersion = offerVersionCommonRepository.saveAndFlush(offerVersionEntity);
            if(clonedOfferVersion instanceof OfferTemplateVersion){
                offerVersionCommonService.cloneOfferVersionRelationship(isCloneRelationship,ocsCloneableEntity.getId(),domainId,clonedOfferVersion.getId());
            }

            CloneDTO cloneDTOnew = new CloneDTO();
            cloneDTOnew.setId(ocsCloneableEntity.getId());
            cloneDTOnew.setParentId(clonedEntity.getId());
            oldNewOfferVersionId.put(clonedOfferVersion.getId(), cloneDTOnew);
            cloneDTOs.forEach(cloneDTO -> {
                if(Objects.equals(cloneDTO.getParentId(),ocsCloneableEntity.getId())){
                    cloneDTO.setParentId(clonedOfferVersion.getId());
                }
            });
            List<Long> childrenIds = parentNewChildrenOldIdMap.get(ocsCloneableEntity.getId());
            parentNewChildrenOldIdMap.remove(ocsCloneableEntity.getId());
            parentNewChildrenOldIdMap.put(clonedOfferVersion.getId(),childrenIds);
            newOfferVersionId.add(clonedOfferVersion.getId());
            number++;
        }

        cloneChildEntitiesOfOfferVeriosnCommon(domainId, cloneDTOs, isCloneRelationship, newOfferVersionId, oldNewOfferVersionId, parentNewChildrenOldIdMap);
    }

    protected void cloneChildEntitiesOfOfferVeriosnCommon(Integer domainId, Collection<CloneDTO> cloneDTOs, Boolean isCloneRelationship,
            List<Long> newOfferVersionId, Map<Long, CloneDTO> oldNewOfferVersionId, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            cloneMappingService.cloneMappings(newOfferVersionId, cloneDTOs, oldNewOfferVersionId, parentNewChildrenOldIdMap);
        }

        for (ChildCloneable ocsBaseService : childServices) {
            List<Long> oldParentIds = oldNewOfferVersionId.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
            //tạo mapping offer version cũ - mới
            Map<Long, Long> oldNewVersionIdMap = new HashMap<>();
            oldParentIds.forEach( id -> {
                Long newId = oldNewOfferVersionId.entrySet().stream()
                    .filter(object -> Objects.equals(object.getValue().getId(),id)).findFirst().get().getKey();
                oldNewVersionIdMap.put(id, newId);
            });
            List<OCSCloneableEntity> listToClone = ocsBaseService.getOCSCloneableRepository().findByDomainIdAndParentIdIn(domainId, oldParentIds);
            List<OCSCloneableEntity> clonedList = new ArrayList<>();
            for (OCSCloneableEntity cloneElement : listToClone) {
                OCSCloneableEntity clonedChildEntity = SerializationUtils.clone(cloneElement);
                clonedChildEntity.setId(null);
                clonedChildEntity.setParentId(oldNewVersionIdMap.get(cloneElement.getParentId()));
                clonedList.add(clonedChildEntity);
            }

            List<OCSCloneableEntity> cloneableChildEntity = offerVersionCommonService.getOCSCloneableRepository().saveAll(clonedList);
            offerVersionCommonService.cloneChildDependOfChildEntities(domainId, cloneableChildEntity, listToClone, ocsBaseService);
        }

    }

    protected abstract void cloneOfferVersionRelationship(Boolean isCloneRelationship, Long oldParentId, Integer domainId, Long newParentId);

    public Page<OfferVersionCommonDTO> getListOfferVersion(Long offerCommonId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<OfferVersionCommon> offerVersionCommons =
            offerVersionCommonRepository.findAllVersionByOfferId(offerCommonId, domainId, pageable);
        Page<OfferVersionCommonDTO> offerVersionCommonDTOS =
            offerVersionCommons.map(i -> offerVersionCommonMapper.toDto(i));
        for (OfferVersionCommonDTO offerVersionCommonDTO : offerVersionCommonDTOS) {
            offerVersionCommonDTO.setHasChild(checkHasChildOfOfferVersion(offerVersionCommonDTO.getId()));
            ReferTable referTable = referTableRepository.findByReferTypeAndValue(ReferType.State, offerVersionCommonDTO.getState().getValue());
            offerVersionCommonDTO.setStateName(referTable.getName());
        }
        return offerVersionCommonDTOS;
    }
    protected void checkDuplicateExternalIdClone(Long externalId){};

    public abstract Boolean checkHasChildOfOfferVersion(Long offerVersionId);

    public abstract List<Long> removeId(Long offerVersionId);

    public List<DropDownBillingCycleDTO> getListBillingCycles() {
        Integer domainId = OCSUtils.getDomain();
        return billingCycleTypeRepository.findAllBillingCycles(domainId);
    }

    protected List getMappingFoundEntities(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        return offerVersionCommonRepository.findByDomainIdAndParentIdIn(domainId, ids);
    }

    public abstract List<Long> findIds(Long id);

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.DUPLICATED_NAME, "", ErrorKey.NAME);
    }
}
