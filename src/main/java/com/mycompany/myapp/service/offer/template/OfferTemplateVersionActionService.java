package com.mycompany.myapp.service.offer.template;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionAction;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionBundle;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionActionDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionActionMapper;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionActionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRepository;
import com.mycompany.myapp.service.action.ActionService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferVersionChildMappingCloneable;
import com.mycompany.myapp.service.offer.common.VersionActionCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Order(value = 2)
public class OfferTemplateVersionActionService extends VersionActionCommonService<OfferTemplateVersionActionDTO, OfferTemplateVersionAction> implements OfferVersionChildMappingCloneable {

    @Autowired
    private OfferTemplateVersionActionRepository offerTemplateVersionActionRepository;
    @Autowired
    private OfferTemplateVersionActionMapper offerTemplateVersionActionMapper;
    @Autowired
    private OfferTemplateVersionRepository offerTemplateVersionRepository;
    @Autowired
    private ActionService actionService;
    @Autowired
    private OfferTemplateVersionService offerTemplateVersionService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.commonRepository = offerTemplateVersionActionRepository;
        this.commonMapper = offerTemplateVersionActionMapper;
        this.ocsCloneMapRepository = offerTemplateVersionActionRepository;
        this.childService = actionService;
        this.parentService = offerTemplateVersionService;
        this.parentDependService = offerTemplateVersionService;
    }

    @Override
    protected void checkExistVersion(Long versionId, Integer domainId) {
        OfferTemplateVersion offerTemplateVersion = offerTemplateVersionRepository.findByIdAndDomainId(versionId, domainId);
        if (offerTemplateVersion == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.ID);
        }
    }

    @Override
    public void checkDataContrain(Long childId, Integer domianId, Long parentId) {
        OfferTemplateVersionAction entity = offerTemplateVersionActionRepository.findByOfferTemplateVersionIdAndActionIdAndDomainId(parentId, childId, domianId);
        if (entity != null) {
            throw new DataConstrainException(ErrorMessage.OfferVersionAction.DATA_CONTRAIN, getResource(), ErrorKey.OfferVersionAction.ACTION);
        }
    }

    @Override
    protected String getResource() {
        return Resources.OFFER_TEMPLATE_VERSION_ACTION;
    }

    public Page<OfferActionDTO> getListOfferTemplateVersionAction(Long offerTemplateVersionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        return offerTemplateVersionActionRepository.getListOfferTemplateVersionAction(offerTemplateVersionId, domainId, pageable);
    }

    @Override
    public List<Long> getActionId(Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferTemplateVersionAction> list =offerTemplateVersionActionRepository.findByDomainIdAndParentId(domainId, versionId);
        List<Long> actionId=  list.stream().map(x -> x.getActionId()).collect(Collectors.toList());
        return actionId;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return offerTemplateVersionActionRepository;
    }
}
