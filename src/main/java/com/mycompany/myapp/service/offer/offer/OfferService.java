package com.mycompany.myapp.service.offer.offer;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.offer.OfferVersionAction;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.offer.common.OfferCloneDTO;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferMapper;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionMapper;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRelationshipRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionActionRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionBundleRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionBundleRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRelationshipRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.offer.common.OfferCommonService;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class OfferService extends OfferCommonService<OfferDTO, Offer> {

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferMapper offerMapper;

    @Autowired
    private OfferVersionMapper offerVersionMapper;

    @Autowired
    private OfferVersionRepository offerVersionRepository;

    @Autowired
    private OfferVersionService offerVersionService;

    @Autowired
    private List<OfferVersionService> childCloneServices;

    @Autowired
    private OfferVersionBundleService offerVersionBundleService;

    @Autowired
    private List<OfferVersionChildCloneable> childCloneables;

    @Autowired
    private List<OfferVersionActionService> offerVersionActionServices;

    @Autowired
    private OfferVersionActionRepository offerVersionActionRepository;

    @Autowired
    private OfferRelationshipRepository offerRelationshipRepository;

    @Autowired
    private OfferVersionBundleRepository offerVersionBundleRepository;

    @Autowired
    private OfferTemplateVersionRelationshipRepository offerTemplateVersionRelationshipRepository;

    @Autowired
    private OfferTemplateVersionBundleRepository offerTemplateVersionBundleRepository;

    @Autowired
    OfferRelationshipService offerRelationshipService;

    @Autowired
    private BlockRepository blockRepository;

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.OFFER;
    }

    @Override
    protected String getResourceName() {
        return Resources.OFFER;
    }

    @Autowired
    private OfferTemplateRepository offerTemplateRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = offerRepository;
        this.parentService = this.categoryService;
        this.ocsCloneableRepository = offerRepository;
        this.cloneMappingServices = offerVersionActionServices;
        this.offerCommonRepository = offerRepository;
        this.offerCommonMapper = offerMapper;
        this.offerVersionCommonRepository = offerVersionRepository;
        this.offerVersionCommonService = offerVersionService;
        this.childServices = childCloneables;
        this.offerVersionCommonMapper = offerVersionMapper;
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new OfferDTO();
    }

    @Override
    protected void checkDuplicateExternalId(OfferCommon offerCommon) {
        Integer domainId = OCSUtils.getDomain();
        Offer newOffer = (Offer) offerCommon;
        List<Offer> offers = offerRepository.findAllByExternalIdAndDomainId(newOffer.getExternalId(), domainId);
        for (Offer offer : offers) {
            if (offer != null && !Objects.equals(newOffer.getId(), offer.getId())) {
                throw new DataConstrainException(ErrorMessage.Offer.DUPLICATED_EXTERNAL_ID, getResourceName(), ErrorKey.Offer.EXTERNAL_ID);
            }
        }
    }

    @Override
    protected void checkDuplicateExternalIdClone(Long externalId) {
        Integer domainId = OCSUtils.getDomain();
        Offer offer = offerRepository.findFirstByExternalIdAndDomainId(externalId, domainId).orElse(null);
        if (offer != null) {
            throw new DataConstrainException(ErrorMessage.Offer.DUPLICATED_EXTERNAL_ID, getResourceName(), ErrorKey.Offer.EXTERNAL_ID);
        }
    }

    @Override
    protected OCSCloneableEntity saveCloneEntity(OCSCloneableEntity clonedEntity, Long externalId,
                                                 String description) {
        Offer offer = (Offer) clonedEntity;
        if (externalId == null) {
            offer.setExternalId(getMaxExternalId());
        } else {
            offer.setExternalId(externalId);
        }

        if (description != null) {
            offer.setDescription(description);
        }
        clonedEntity = offerRepository.saveAndFlush(offer);
        return clonedEntity;
    }

    public Long getMaxExternalId() {
        return offerRepository.findTopByOrderByExternalIdDesc() == null ? 1L :
            offerRepository.findTopByOrderByExternalIdDesc().getExternalId();
    }

    public Object createFromTemplateVersion(@NotNull OfferDTO dto) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Long> removeId(Long offerversionId) {
        List<Long> removeIds = new ArrayList<>();
        removeIds = offerVersionBundleService.getOfferId(offerversionId);
        return removeIds;
    }

    public OfferDTO getDataCreate(Long offerTemplateId) {
        Integer domainId = OCSUtils.getDomain();
        OfferTemplate offerTemplate = offerTemplateRepository.findByIdAndDomainId(offerTemplateId, domainId);
        if (offerTemplate == null) {
            throw new ResourceNotFoundException(ErrorMessage.OfferTemplate.NOT_FOUND, Resources.OFFER_TEMPLATE, ErrorKey.ID);
        }
        OfferDTO offerDTO = new OfferDTO();
        offerDTO.setEffDate(offerTemplate.getEffDate());
        offerDTO.setExpDate(offerTemplate.getExpDate());
        offerDTO.setPriority(offerTemplate.getPriority());
        offerDTO.setExpriable(offerTemplate.getExpriable());
        offerDTO.setSuspendable(offerTemplate.getSuspendable());
        offerDTO.setIsBundle(offerTemplate.getIsBundle());
        offerDTO.setIsMultiCycle(offerTemplate.getIsMultiCycle());
        return offerDTO;
    }

    @Override
    public void deleteChildVersion(Long parentId, Integer domainId) {
        List<OfferVersion> lstChildVersions = offerVersionRepository.findByParentIdAndDomainId(parentId, domainId);
        List<Long> lstChildVersionIds = lstChildVersions.stream().map(i -> i.getId()).collect(Collectors.toList());
        for (Long childVersionIds : lstChildVersionIds) {
            offerVersionService.deleteOfferVersion(childVersionIds);
        }
    }

    @Override
    public void validateInputDataClone(OfferCloneDTO offerCloneDTO) {
        // TODO Auto-generated method stub
        if (offerCloneDTO.getExternalId() == null) {
            throw new DataInvalidException(ErrorMessage.Offer.MUST_NOT_BE_NULL, Resources.OFFER, ErrorKey.Offer.EXTERNAL_ID);
        }
    }

    @Override
    protected String getNameCategory() {
        // TODO Auto-generated method stub
        return "Offers";
    }

    @Override
    public Boolean checkHasChildOfOfferVersion(Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferVersionAction> lstChildOfOfferVersion = offerVersionActionRepository.findByDomainIdAndParentId(domainId, offerVersionId);
        return !CollectionUtils.isEmpty(lstChildOfOfferVersion);
    }

    @Override
    public void deleteOfferRelationship(Long offerId, Integer domainId) {
        if (!CollectionUtils.isEmpty(offerRelationshipRepository.findByDomainIdAndChildId(domainId, offerId))) {
            throw new DataInvalidException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.OFFER, ErrorKey.ID);
        }
        offerRelationshipRepository.deleteByParentIdAndDomainId(offerId, domainId);
    }

    @Override
    public void validateOfferToDelete(Long id, Integer domainId) {
        if (!CollectionUtils.isEmpty(offerVersionBundleRepository.findByDomainIdAndChildOfferId(domainId, id))
            || !CollectionUtils.isEmpty(offerTemplateVersionRelationshipRepository.findByDomainIdAndChildId(domainId, id))
            || !CollectionUtils.isEmpty(offerTemplateVersionBundleRepository.findByDomainIdAndChildOfferId(domainId, id))
            || !CollectionUtils.isEmpty(blockRepository.findAllByAffectedValueAndAffectedObjectTypeAndDomainId(id, 6, domainId))
        ) {
            throw new DataInvalidException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.OFFER, ErrorKey.ID);
        }
    }

    @Override
    public void getChildTemplate(List<Long> ids, Map<Long, TreeClone> resultMap) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferVersion> versions = offerVersionRepository.findByDomainIdAndParentIdIn(domainId, ids);

        List<Long> versionIds = versions.stream().map(OfferVersion::getId).distinct().collect(Collectors.toList());
        if (CollectionUtils.isEmpty(versions)) {
            return;
        }
        Map<Long, TreeClone> versionsMap = new HashMap<Long, TreeClone>();
        List<TreeClone> versionDtos = new ArrayList<>();
        for (OfferVersion baseEntity : versions) {
            TreeClone result = new OfferVersionDTO();
            result.setTemplates(new ArrayList<Tree>());
            BeanUtils.copyProperties(baseEntity, result);
            result.setName("Version " + baseEntity.getNumber());
            if (result.getTemplates() == null) {
                result.setTemplates(new ArrayList<Tree>());
            }
            versionDtos.add(result);
            versionsMap.put(ids.get(0), result);
            super.getChildTemplate(versionIds, versionsMap);
        }
        for (Long entityId : resultMap.keySet()) {
            TreeClone resultOffer = resultMap.get(entityId);
            List<? extends Tree> children = versionDtos;
            if (children != null) {
                resultOffer.getTemplates().addAll(children);
            }
            resultOffer.setHasChild(!resultOffer.getTemplates().isEmpty());
        }
    }

    @Override
    public List<Long> findIds(Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> removeid = offerRelationshipService.getids(id);
        return removeid;
    }

    @Override
    public void getParentDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices,
                                      CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();

//        List<Offer> dependDBs = getParentEntity(ids, domainId, categoryType);
//
//        List<Long> idOffer = dependDBs.stream().map(id -> id.getId()).collect(Collectors.toList());
//        List<OfferVersion> offerVersions = offerVersionRepository.findByOfferIdInAndDomainId(idOffer, domainId);

        List<OfferVersion> offerVersions = offerVersionRepository.findByIdInAndDomainId(ids, domainId);

        Collections.sort(offerVersions, new Comparator<OfferVersion>() {
            @Override
            public int compare(OfferVersion o1, OfferVersion o2) {
                return o1.getNumber().compareTo(o2.getNumber());
            }
        });

        List<Long> idOffer = offerVersions.stream().map(id -> id.getParentId()).collect(Collectors.toList());

        List<Offer> offers = offerRepository.findByIdInAndDomainId(idOffer, domainId);
        Collections.sort(offers, new Comparator<Offer>() {
            @Override
            public int compare(Offer o1, Offer o2) {
                String o1Name = o1.getName();
                String o2name = o2.getName();
                if (o1Name != null && o2name != null) {
                    return StringUtils.compare(o1.getName().toLowerCase(), o2.getName().toLowerCase());
                } else {
                    return StringUtils.compare(o1Name, o2name);
                }
            }
        });

        List<Tree> depends = new ArrayList<>();

        for (Offer offer : offers) {
            for (OfferVersion offerVersion : offerVersions) {
                if (Objects.equals(offerVersion.getParentId(), offer.getId())) {
                    TreeClone tree = new OfferVersionDTO();
                    BeanUtils.copyProperties(offerVersion, tree);
                    tree.setName(offer.getName() + " - Version " + offerVersion.getNumber());
                    depends.add(tree);
                }
            }
        }

        if (dependencies.stream().noneMatch(i ->i.getName().equals("Offers"))) {
            CategoryDTO categoryDTO = getCategory(getCategoryType());
            if (categoryDTO == null) {
                return;
            }
            categoryDTO.setTemplates(depends);
            categoryDTO.setName(getNameCategory());

            dependencies.add(0, categoryDTO);
            return;
        }

        Optional<Tree> firstOffers = dependencies.stream().filter(i -> i.getName().equals("Offers")).findFirst();

        Collection<Tree> offerTemplates = new ArrayList<>();
        if (firstOffers.isPresent()) {
            offerTemplates = firstOffers.get().getTemplates();
        }
        Collection<Tree> finalOfferTemplates = offerTemplates;
        depends.forEach(
            i -> {
                if(finalOfferTemplates.stream().noneMatch( j -> j.getId().equals(i.getId()))) {
                    finalOfferTemplates.add(i);
                }
            }
        );

        CategoryDTO categoryDTO = getCategory(getCategoryType());
        if (categoryDTO == null) {
            return;
        }

        Collections.sort((List)finalOfferTemplates, new Comparator<Tree>() {
            @Override
            public int compare(Tree o1, Tree o2) {
                int indexOfLastSpace1 = o1.getName().lastIndexOf(' ');
                int indexOfLastSpace2 = o2.getName().lastIndexOf(' ');
                String name1 = o1.getName().substring(0, indexOfLastSpace1);
                String name2 = o2.getName().substring(0, indexOfLastSpace2);
                if (name1.equals(name2)) {
                    String version1 = o1.getName().substring(indexOfLastSpace1 + 1);
                    String version2 = o2.getName().substring(indexOfLastSpace1 + 1);
                    return Integer.parseInt(version1) - Integer.parseInt(version2);
                }
                return o1.getName().compareTo(o2.getName());
            }
        });

        categoryDTO.setTemplates(finalOfferTemplates);
        categoryDTO.setName(getNameCategory());

        dependencies.add(0, categoryDTO);
    }

    @Override
    protected void cloneOfferVersionRelationship(Boolean isCloneRelationship, Long oldParentId, Integer domainId, Long newParentId) {
        offerVersionService.cloneOfferVersionRelationship(isCloneRelationship, oldParentId, domainId, newParentId);
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Offer.DUPLICATED_NAME, Resources.OFFER, ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER, ErrorKey.ID);
    }
}
