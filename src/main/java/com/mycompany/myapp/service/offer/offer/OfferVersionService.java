package com.mycompany.myapp.service.offer.offer;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.service.action.ActionPriceComponentMapService;
import com.mycompany.myapp.service.clone.OCSCloneService;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.State;
import com.mycompany.myapp.domain.offer.offer.OfferRelationship;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecUse;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.dto.offer.common.MaxNumberDTO;
import com.mycompany.myapp.dto.offer.offer.OfferCharacteristicDTO;
import com.mycompany.myapp.dto.offer.offer.OfferOfCharacteristicDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionMapper;
import com.mycompany.myapp.repository.offer.offer.OfferRelationshipRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionActionRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionBundleRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecUseRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecValueUseRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRedirectionRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.OfferVersionCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.util.BooleanUtils;

@Service
@Transactional
@Order(value = 2)
public class OfferVersionService extends OfferVersionCommonService<OfferVersion, OfferVersionDTO> {

    @Autowired
    private OfferVersionRepository offerVersionRepository;

    @Autowired
    private OfferVersionMapper offerVersionMapper;

    @Autowired
    private OfferService offerService;

    @Autowired
    private List<OfferService> offerServices;

    @Autowired
    private OfferTemplateRepository offerTemplateRepository;

    @Autowired
    private OfferTemplateVersionRepository offerTemplateVersionRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferVersionRedirectionRepository offerVersionRedirectionRepository;

    @Autowired
    private OfferVersionCharspecUseRepository offerVersionCharspecUseRepository;

    @Autowired
    private OfferVersionCharspecValueUseRepository offerVersionCharspecValueUseRepository;

    @Autowired
    private List<OfferVersionChildCloneable> childCloneables;

    @Autowired
    private List<OfferVersionActionService> offerVersionActionServices;

    @Autowired
    private OfferVersionActionService offerVersionActionService;

    @Autowired
    private OfferVersionActionRepository offerVersionActionRepository;

    @Autowired
    private OfferVersionBundleRepository offerVersionBundleRepository;

    @Autowired
    private List<ActionPriceComponentMapService> actionPriceComponentMapServices;

    @Autowired
    private OfferRelationshipRepository offerRelationshipRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = offerVersionRepository;
        this.parentService = offerService;
        this.ocsCloneableRepository = offerVersionRepository;
        this.offerVersionCommonRepository = offerVersionRepository;
        this.offerVersionCommonMapper = offerVersionMapper;
        this.offerCommonRepository = offerRepository;
        this.childServices = childCloneables;
        this.cloneMappingServices = offerVersionActionServices;
        this.parentDependServices = offerServices;

    }

    @Override
    public String getResourceName() {
        return Resources.OFFER_VERSION;
    }

    @Override
    public MaxNumberDTO getMaxNumberOfVersion(Long parentId) {
        MaxNumberDTO maxNumber = new MaxNumberDTO();
        Long max = offerVersionRepository.getMaxNumber(parentId);
        maxNumber.setMaxNumber(max);
        return maxNumber;
    }

    public Long getMaxNumberOfVersions(Long numberDTO, Long parentId) {
        Long maxDB = offerVersionRepository.getMaxNumber(parentId);
        if(maxDB == null)
            return 1L;
        if(numberDTO <= maxDB)
            return maxDB +1L;
        return numberDTO;
    }

    @Override
    public void checkExistVersion(Long versionId, Integer domainId) {
        OfferVersion offerVersion = offerVersionRepository.findByIdAndDomainId(versionId, domainId);
        if (offerVersion == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_VERSION, ErrorKey.ID);
        }
    }

    public Page<OfferOfCharacteristicDTO> findChacracteristics(List<Long> chacracteristicIds, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<OfferOfCharacteristicDTO> dtos = offerVersionRepository.findChacracteristics(chacracteristicIds, domainId, pageable);
        return dtos;
    }

    @Override
    public OfferVersionDTO setOfferTemplateOfOfferVersion(OfferVersionDTO offerVersionDTO) {
        Integer domainId = OCSUtils.getDomain();
        if (offerVersionDTO.getOfferTemplateVersionId() != null) {
            OfferTemplateVersion offerTemplateVersion = offerTemplateVersionRepository
                .findByIdAndDomainId(offerVersionDTO.getOfferTemplateVersionId(), domainId);
            OfferTemplate offerTemplate = offerTemplateRepository.findByIdAndDomainId(offerTemplateVersion.getOfferTemplateId(), domainId);
            String offerTemplateOfOfferVersion = offerTemplate.getName() + "(Version " + offerTemplateVersion.getNumber() + ")";
            offerVersionDTO.setOfferTemplate(offerTemplateOfOfferVersion);
        }
        return offerVersionDTO;
    }

    public void deleteOfferVersion(Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
        OfferVersion offerVersion = offerVersionRepository.findByIdAndDomainId(offerVersionId, domainId);
        if (offerVersion == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_VERSION, ErrorKey.ID);
        }
        if (offerVersion.getState().equals(State.ACTIVE)) {
            throw new DataConstrainException(ErrorMessage.OfferVersion.CANNOT_DELETE, Resources.OFFER_VERSION, ErrorKey.OfferVersion.STATE);
        }
        offerVersionRepository.deleteById(offerVersionId);
        offerVersionRedirectionRepository.deleteByOfferVersionId(offerVersionId);
        List<OfferVersionCharspecUse> offerVersionCharspecUses = offerVersionCharspecUseRepository.deleteByOfferVersionId(offerVersionId);
        for (OfferVersionCharspecUse offerVersionCharspecUse : offerVersionCharspecUses) {
            offerVersionCharspecValueUseRepository.deleteByCharspecUseId(offerVersionCharspecUse.getId());
        }
        offerVersionActionRepository.deleteByDomainIdAndParentId(domainId, offerVersionId);
        offerVersionBundleRepository.deleteByDomainIdAndRootOfferVersionId(domainId, offerVersionId);
    }

    public OfferCharacteristicDTO getcharacteristic(Long id) {
        Integer domainId = OCSUtils.getDomain();
        OfferCharacteristicDTO dto = offerVersionRepository.findCharacteristicById(id, domainId);
        return dto;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerVersionRepository;
    }

    @Override
    public void cloneChildDependOfChildEntities(Integer domainId, List<OCSCloneableEntity> clonedEntities, List<OCSCloneableEntity> listParentToClone, ChildCloneable parentService) {
        if (parentService instanceof OfferVersionCharspecUseService) {
            for (int i = 0; i < listParentToClone.size(); i++) {
                List<OfferVersionCharspecValueUse> listToClone = offerVersionCharspecValueUseRepository.findByParentIdAndDomainId(listParentToClone.get(i).getId(), domainId);
                List<OfferVersionCharspecValueUse> clonedList = new ArrayList<>();
                for (OfferVersionCharspecValueUse cloneElement : listToClone) {
                    OfferVersionCharspecValueUse clonedChildEntity = SerializationUtils.clone(cloneElement);
                    clonedChildEntity.setId(null);
                    clonedChildEntity.setParentId(clonedEntities.get(i).getId());
                    clonedList.add(clonedChildEntity);
                }
                offerVersionCharspecValueUseRepository.saveAll(clonedList);
            }
        }
    }


    public OfferVersionDTO getDataOfferVersion(Long offerTemplateVersionId) {
        Integer domainId = OCSUtils.getDomain();
        OfferTemplateVersion offerTemplateVersion = offerTemplateVersionRepository.findByIdAndDomainId(offerTemplateVersionId, domainId);
        if (offerTemplateVersion == null) {
            throw new ResourceNotFoundException(ErrorMessage.OfferTemplateVersion.NOT_FOUND, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.ID);
        }
        OfferVersionDTO offerVersionDTO = new OfferVersionDTO();
        offerVersionDTO.setEffDate(offerTemplateVersion.getEffDate());
        offerVersionDTO.setExpDate(offerTemplateVersion.getExpDate());
        offerVersionDTO.setSpecialMethod(offerTemplateVersion.getSpecialMethod());
        return offerVersionDTO;
    }

    @Override
    public OfferVersionDTO setExternalId(OfferVersionDTO offerVersionDTO, Long versionId, Integer domainId) {
        Long externalId = offerRepository.getExternalIdByOfferVersionId(versionId, domainId);
        offerVersionDTO.setExternalId(externalId);
        return offerVersionDTO;
    }

    @Override
    public List<Long> removeId(Long offerVersionId) {
        List<Long> removeIds = offerVersionActionService.getActionId(offerVersionId);
        return removeIds;
    }

    @Override
    protected String getNameCategory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new OfferVersionDTO();
    }

    @Override
    protected Page<Action> getAllActionByVersionId(Long offerVersionCommonId, Pageable pageable, Integer domainId) {
        return actionRepository.findAllActionByOfferVersionId(offerVersionCommonId, domainId, pageable);
    }

    @Override
    public void getParentDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices,
                                      CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();

//        List<OCSMoveableEntity> dependDBs = getParentEntity(ids, domainId, categoryType);

        for (OCSCloneService parentDependService : offerServices) {
            if (parentDependService != null) {
//                ids = dependDBs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
                parentDependService.getParentDependencies(dependencies, ids, true, getCategoryType());
            }
        }
    }

    @Override
    protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        return offerRepository.findOfferByOfferVersionIdAndDomainId(ids, domainId);
    }

    public Long getOffferId(Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
          Long offerId = offerVersionRepository.findOfferId(offerVersionId, domainId);
        return offerId;
    }

    @Override
    protected void cloneOfferVersionRelationship(Boolean isCloneRelationship, Long oldParentId, Integer domainId,
            Long newParentId) {
        if(BooleanUtils.isTrue(isCloneRelationship)) {
            List<OfferRelationship> listToCloneByParentOffer = offerRelationshipRepository.findByParentIdAndDomainId(oldParentId, domainId);
            List<OfferRelationship> clonedListParent = new ArrayList<>();
            for (OfferRelationship cloneElement : listToCloneByParentOffer) {
                OfferRelationship clonedChildEntity = SerializationUtils.clone(cloneElement);
                clonedChildEntity.setId(null);
                clonedChildEntity.setParentOfferId(newParentId);
                clonedListParent.add(clonedChildEntity);
            }
            offerRelationshipRepository.saveAll(clonedListParent);
            List<OfferRelationship> listToCloneByChildOffer = offerRelationshipRepository.findByDomainIdAndChildId(domainId, oldParentId);
            List<OfferRelationship> clonedListChild = new ArrayList<>();
            for (OfferRelationship cloneElement : listToCloneByChildOffer) {
                OfferRelationship clonedChildEntity = SerializationUtils.clone(cloneElement);
                clonedChildEntity.setId(null);
                clonedChildEntity.setChildOfferId(newParentId);
                clonedListChild.add(clonedChildEntity);
            }
            offerRelationshipRepository.saveAll(clonedListChild);
        }
    }
}
