package com.mycompany.myapp.service.offer.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.mapper.action.ActionMapper;
import com.mycompany.myapp.repository.action.ActionPriceComponentMapRepository;
import com.mycompany.myapp.repository.action.ActionRepository;
import com.mycompany.myapp.service.action.ActionPriceComponentMapService;
import com.mycompany.myapp.service.normalizer.ChildCloneable;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.constant.State;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.OfferVersionCloneDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.offer.common.MaxNumberDTO;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.mapper.offer.common.OfferVersionCommonMapper;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.offer.common.OfferCommonRepository;
import com.mycompany.myapp.repository.offer.common.OfferVersionCommonRepository;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.exception.CloneNameTooLongException;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

public abstract class OfferVersionCommonService<E extends OfferVersionCommon, D extends OfferVersionCommonDTO> extends AbstractCloneService implements ChildCloneable {

    protected OfferVersionCommonRepository<E> offerVersionCommonRepository;

    protected OfferVersionCommonMapper<D, E> offerVersionCommonMapper;

    protected OfferCommonRepository<? extends OfferCommon> offerCommonRepository;

    @Autowired
    protected ActionRepository actionRepository;

    @Autowired
    private ActionMapper actionMapper;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private ActionPriceComponentMapRepository actionPriceComponentMapRepository;

    @Autowired
    private ActionPriceComponentMapService actionPriceComponentMapService;

    @Override
    protected List<CategoryType> dependTypes() {
        return null;
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return null;
    }

    @Override
    protected TreeClone getTreeDTO() {
        return null;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.OFFER_VERSION;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Offer.Length.NAME;
    }

    @Override
    public String getResourceName() {
        return Resources.OFFER_VERSION;
    }

    protected abstract MaxNumberDTO getMaxNumberOfVersion(Long parentId);

    public D create(D offerVersionCommonDTO) {
        if(!State.IN_ACTIVE.equals(offerVersionCommonDTO.getState())) {
            throw new DataConstrainException(ErrorMessage.OfferVersion.STATE_MUST_BE_IN_ACTIVE, getResourceName(),
                    ErrorKey.OfferVersion.STATE);
        }
        Integer domainId = OCSUtils.getDomain();
        offerVersionCommonDTO.setDomainId(domainId);
        return save(offerVersionCommonDTO);
    }

    protected abstract void checkExistVersion(Long versionId, Integer domainId);

    protected abstract D setOfferTemplateOfOfferVersion(D  offerVersionCommonDTO);

    public D update(D offerVersionCommonDTO) {
        Integer domainId = OCSUtils.getDomain();
        E offerVersionCommon = offerVersionCommonRepository.findByIdAndDomainId(offerVersionCommonDTO.getId(), domainId);
        if(offerVersionCommon == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, getResourceName(), ErrorKey.OfferVersion.ID);
        }
        checkExistVersion(offerVersionCommonDTO.getId(), domainId);
        List<Long> lstActiveVersionIds = offerVersionCommonRepository.getListActiveVersion(offerVersionCommon.getParentId());
        if(State.ACTIVE.equals(offerVersionCommonDTO.getState())) {
            if(!CollectionUtils.isEmpty(lstActiveVersionIds)) {
                for (Long versionId : lstActiveVersionIds) {
                    if(!versionId.equals(offerVersionCommonDTO.getId())) {
                        offerVersionCommonRepository.updateStateOfVersion(versionId);
                    }
                }
            }
        }
        return save(offerVersionCommonDTO);
    }

    public Boolean checkHasVersionActive(Long parentOfferId) {
        List<Long> lstActiveVersionIds = offerVersionCommonRepository.getListActiveVersion(parentOfferId);
        if(!CollectionUtils.isEmpty(lstActiveVersionIds)) {
            return true;
        }
        return false;
    }

    private D save(D offerVersionCommonDTO) {
        E offerVersionCommon = offerVersionCommonMapper.toEntity(offerVersionCommonDTO);
        if (offerVersionCommon.getEffDate().after(offerVersionCommon.getExpDate())) {
            throw new DataInvalidException(ErrorMessage.Offer.EFFECT_DATE_LESS_THAN_EXPIRE_DATE, getResourceName(), ErrorKey.Offer.EFF_DATE);
        }
        offerVersionCommon = offerVersionCommonRepository.save(offerVersionCommon);

        return offerVersionCommonMapper.toDto(offerVersionCommon);
    }

    public D getVersionDetail(Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        E offerVersionCommon = offerVersionCommonRepository.findByIdAndDomainId(versionId, domainId);
        D offerVersionCommonDTO = offerVersionCommonMapper.toDto(offerVersionCommon);
        offerVersionCommonDTO.setParentName((offerCommonRepository.findByIdAndDomainId(offerVersionCommonDTO.getParentId(), domainId)).getName());
        ReferTable referOfState = referTableRepository.getReferTableByReferTypeAndValue(ReferType.State, offerVersionCommonDTO.getState().getValue());
        ReferTable referOfSpecialMethod = referTableRepository.getReferTableByReferTypeAndValue(ReferType.SpecialMethod, offerVersionCommonDTO.getState().getValue());
        offerVersionCommonDTO = setOfferTemplateOfOfferVersion(offerVersionCommonDTO);
        offerVersionCommonDTO = setExternalId(offerVersionCommonDTO,versionId, domainId);
        return offerVersionCommonDTO;
    }

    public D setExternalId(D offerVersionCommonDTO,Long versionId, Integer domainId){
        return offerVersionCommonDTO;
    }

    public List<Tree> getOfferVersionForClone(Long parentId) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableEntity> ocsCloneableEntities = offerVersionCommonRepository.findByDomainIdAndParentId(domainId, parentId);
        if (ocsCloneableEntities == null) {
            return null;
        }
        List<Tree> results = new ArrayList<Tree>();
        for (OCSCloneableEntity ocsCloneableEntity : ocsCloneableEntities) {
//            Tree tempTree = childService.getCloneableTree(ocsCloneableEntity.getChildId());
//            if (tempTree != null) {
//                results.add(tempTree);
//            }
            TreeClone result = new OfferVersionCommonDTO();
            BeanUtils.copyProperties(ocsCloneableEntity, result);
//            result.getTemplates().addAll(offerVersionCommonService.getChildren(id));
            result.setTemplates(new ArrayList<>());
            result.setHasChild(!result.getTemplates().isEmpty());
            results.add( result);
        }
        return results;
    }

    @Override
    protected OCSCloneableEntity setPropertiesOfCloneEntity(OCSCloneableEntity clonedEntity, CloneDTO cloneDTO, String newName) {
        E offerVersionEntity = (E) clonedEntity;
        OfferVersionCloneDTO offerVersionCloneDTO = (OfferVersionCloneDTO) cloneDTO;
        offerVersionEntity.setState(offerVersionCloneDTO.getState());
        offerVersionEntity.setDescription(offerVersionCloneDTO.getDescription());
        offerVersionEntity.setName(newName);
        offerVersionEntity.setId(null);
        return offerVersionEntity;
    }

    @Override
    public void cloneChildDependOfChildEntities(Integer domainId, List<OCSCloneableEntity> clonedEntities, List<OCSCloneableEntity> listParentToClone, ChildCloneable parentService) {

    }

    public abstract List<Long> removeId(Long offerVersionId);

    public Page<ActionDTO> getListAction(Long offerVersionCommonId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<Action> actions =
            getAllActionByVersionId(offerVersionCommonId, pageable, domainId);
        Page<ActionDTO> actionDTOPage =
            actions.map(i -> actionMapper.toDto(i));

        List<Long> actionIds = actionDTOPage.getContent().stream().map(ActionDTO::getId).collect(Collectors.toList());

        Map<Long,Integer> actionPriceComponentMap = actionPriceComponentMapService.countChildrenByParentMap(actionIds,domainId);

        for (ActionDTO actionDTO : actionDTOPage) {
            actionDTO.setHasChild(checkHasChildOfChildren(actionPriceComponentMap,actionDTO.getId()));
        }
        return actionDTOPage;
    }

    private boolean checkHasChild(Long id, Integer domainId) {
        Integer countReferWithPccRule = actionPriceComponentMapRepository.countByDomainIdAndParentId(domainId, id);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    protected Page<Action> getAllActionByVersionId(Long offerVersionCommonId, Pageable pageable, Integer domainId) {
        return actionRepository.findAllActionByOfferVersionId(offerVersionCommonId, domainId, pageable);
    }

    protected abstract void cloneOfferVersionRelationship(Boolean isCloneRelationship, Long oldParentId, Integer domainId, Long newParentId);

    @Override
    public OCSCloneableEntity saveEntity(CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> baseToCloneMap) {
        if (ocsCloneableRepository == null) {
            throw new UnsupportedOperationException("Method is not ready");
        }
        if (cloneDTO.getChildren() == null) {
            cloneDTO.setChildren(new ArrayList<CloneDTO>());
        }

        Integer domainId = OCSUtils.getDomain();
        String name = cloneDTO.getName();
        Long id = cloneDTO.getId();

        OCSCloneableEntity baseToClone = baseToCloneMap.get(id);

        if (baseToClone == null) {
            return null;
        }

        checkDuplicateName(name, id);
        OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
        String newName = "";
        if (StringUtils.isEmpty(name) || name.equals(clonedEntity.getName())) {
            newName = cloneName(baseToClone.getName(), domainId);
        } else {
            newName = name;
        }


        if (getCloneNameMaxLength() != null && newName.length() > getCloneNameMaxLength().intValue()) {
            throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                    StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
        }

        setPosIndex(domainId, clonedEntity, clonedEntity.getParentId());

        clonedEntity = setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);
        MaxNumberDTO maxNumber = getMaxNumberOfVersion(baseToClone.getParentId());
        OfferVersionCommon offerVersionCommon = (OfferVersionCommon) clonedEntity;
        if(offerVersionCommon.getState() == null) {
            clearCache();
            throw new DataInvalidException(ErrorMessage.Offer.MUST_NOT_BE_NULL, getResourceName(), ErrorKey.OfferTemplate.STATE);
        }
        List<Long> lstActiveVersionIds = offerVersionCommonRepository.getListActiveVersion(offerVersionCommon.getParentId());
        if(State.ACTIVE.equals(offerVersionCommon.getState())) {
            if(!CollectionUtils.isEmpty(lstActiveVersionIds)) {
                for (Long versionId : lstActiveVersionIds) {
                    if(!versionId.equals(offerVersionCommon.getId())) {
                        offerVersionCommonRepository.updateStateOfVersion(versionId);
                    }
                }
            }
        }
        offerVersionCommon.setNumber(maxNumber.getMaxNumber() + 1);
        clonedEntity = ocsCloneableRepository.saveAndFlush(offerVersionCommon);
        OfferVersionCloneDTO offerVersionCloneDTO = (OfferVersionCloneDTO) cloneDTO;
        cloneOfferVersionRelationship(offerVersionCloneDTO.getIsCloneRelationship(), id, domainId, clonedEntity.getId());
        return clonedEntity;
    }
}
