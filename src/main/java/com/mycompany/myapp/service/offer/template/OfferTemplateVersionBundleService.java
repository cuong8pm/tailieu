package com.mycompany.myapp.service.offer.template;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.offer.OfferVersionBundle;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionBundle;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.offer.offer.VersionBunndleDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionBundleDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionBundleMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionBundleRepository;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.VersionBundleCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class OfferTemplateVersionBundleService extends VersionBundleCommonService<OfferTemplateVersionBundleDTO, OfferTemplateVersionBundle> implements OfferTemplateVersionChildCloneable{

    @Autowired
    private OfferTemplateVersionBundleRepository offerTemplateVersionBundleRepository;

    @Autowired
    private OfferTemplateVersionBundleMapper offerTemplateVersionBundleMapper;

    @Autowired
    private OfferRepository offerRepository;

    @PostConstruct
    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerTemplateVersionBundleRepository;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = offerTemplateVersionBundleRepository;
        this.commonRepository = offerTemplateVersionBundleRepository;
        this.commonMapper = offerTemplateVersionBundleMapper;
    }

    @Override
    protected CategoryType getCategoryType() {
        // khong nam trong tree
        return null;
    }

    @Override
    public void checkDuplicateOffer(List<Long> ids, Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferTemplateVersionBundle> list = offerTemplateVersionBundleRepository
                .findByDomainIdAndChildOfferIdInAndVersionId(domainId, ids, versionId);
        if (!list.isEmpty()) {
            throw new DataInvalidException(ErrorMessage.Offer.DUPLICATED_OFFER, Resources.OFFER, ErrorKey.Offer.ID);
        }
        List<Offer> offers =  offerRepository.findAllById(ids);
        List<Long> offerIds =  offers.stream().map(Offer::getId).collect(Collectors.toList());
        if (ids.size() != offerIds.size()) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.OFFER, ErrorKey.Offer.ID);
        }
    }

    @Override
    public OfferTemplateVersionBundle getEntity() {
        // TODO Auto-generated method stub
        return new OfferTemplateVersionBundle();
    }

    @Override
    public void setVersionIdForInsert(Long versionId, OfferTemplateVersionBundle entityID) {
        entityID.setOfferTemplateVersionId(versionId);

    }

    public Page<VersionBunndleDTO> findVersionBunndleDTOs(Long versionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<VersionBunndleDTO> page = offerTemplateVersionBundleRepository.findBundles(versionId, domainId, pageable);
        return page;

    }

    public void checkDuplicateChild( Long childId , Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        Integer count = offerTemplateVersionBundleRepository.countByDomainIdAndChildOfferIdAndVersionId(domainId, childId, versionId);
        if (count > 0) {
            throw new DataInvalidException(ErrorMessage.OfferVersionBundle.DUPLICATED_CHILDID,
                    Resources.OFFER_VERSION_BUNDLE, ErrorKey.OfferVersionBundle.OFFER);

        }

    }

    @Override
    public List<Long> getOfferId(Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferTemplateVersionBundle> list =offerTemplateVersionBundleRepository.findByDomainIdAndParentId(domainId, versionId);
      List<Long> offerVersionId=  list.stream().map(x -> x.getChildOfferId()).collect(Collectors.toList());
      return offerVersionId;

    }

    @Override
    protected String getResource() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
