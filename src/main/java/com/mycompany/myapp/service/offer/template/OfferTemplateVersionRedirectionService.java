package com.mycompany.myapp.service.offer.template;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRedirection;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionRedirectionDTO;
import com.mycompany.myapp.mapper.offer.template.OfferTemplateVersionRedirectionMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRedirectionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferTemplateVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.VersionRedirectionCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.List;

@Service
@Transactional
public class OfferTemplateVersionRedirectionService extends VersionRedirectionCommonService<OfferTemplateVersionRedirection, OfferTemplateVersionRedirectionDTO> implements OfferTemplateVersionChildCloneable{

    @Autowired
    private OfferTemplateVersionRedirectionRepository offerTemplateVersionRedirectionRepository;

    @Autowired
    private OfferTemplateVersionRedirectionMapper offerTemplateVersionRedirectionMapper;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private OfferTemplateVersionRepository offerTemplateVersionRepository;

    @Override
    public OfferTemplateVersionRedirectionDTO setReferName(OfferTemplateVersionRedirectionDTO dto) {
        ReferTable referTable = referTableRepository.getReferTableByReferTypeAndValue(ReferType.RedirectionType, dto.getRedirectionType());
        if (referTable != null) {
            dto.setName(referTable.getName());
        }
        return dto;
    }

    @Override
    public void checkVersion(OfferTemplateVersionRedirectionDTO dto) {

        Integer domainId = OCSUtils.getDomain();
        OfferTemplateVersion version = offerTemplateVersionRepository.findByIdAndDomainId(dto.getVersionId(), domainId);
        if (version == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_VERSION_REDIRECTION, ErrorKey.VersionRedirectionCommon.VERSION_ID);
        }
    }

    @Override
    public String getResources() {
        return Resources.OFFER_TEMPLATE_VERSION_REDIRECTION;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerTemplateVersionRedirectionRepository;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = offerTemplateVersionRedirectionRepository;
        this.commonMapper = offerTemplateVersionRedirectionMapper;
        this.commonRepository = offerTemplateVersionRedirectionRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        // khong nam trong tree
        return null;
    }

    @Override
    protected String getResource() {
        // TODO Auto-generated method stub
        return null;
    }

    public List<OfferOfVersionRedirectionDTO> getOfferTemplateVersionRedirection(Long offerTemplateVersionId) {
        Integer domainId = OCSUtils.getDomain();
        return offerTemplateVersionRedirectionRepository.getOfferTemplateVersionRedirection(offerTemplateVersionId, domainId, ReferType.RedirectionType);
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
