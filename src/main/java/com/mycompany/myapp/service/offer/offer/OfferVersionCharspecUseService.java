package com.mycompany.myapp.service.offer.offer;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecUse;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.dto.offer.common.CharOfVersionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionCharspecUseDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionCharspecValueUseDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionCharspecUseMapper;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionCharspecValueUseMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecUseRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecValueUseRepository;
import com.mycompany.myapp.service.characteristic.CharacteristicService;
import com.mycompany.myapp.service.offer.common.OfferVersionChildCloneable;
import com.mycompany.myapp.service.offer.common.VersionCharspecUseCommonService;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public class OfferVersionCharspecUseService extends VersionCharspecUseCommonService<OfferVersionCharspecUseDTO, OfferVersionCharspecUse> implements OfferVersionChildCloneable{

    @Autowired
    private OfferVersionCharspecUseRepository offerVersionCharspecUseRepository;

    @Autowired
    private OfferVersionCharspecUseMapper offerVersionCharspecUseMapper;

    @Autowired
    private OfferVersionCharspecValueUseMapper offerVersionCharspecValueUseMapper;

    @Autowired
    private OfferVersionCharspecValueUseRepository offerVersionCharspecValueUseRepository;

    @Autowired
    private CharacteristicService characteristicService;

    @Autowired
    private OfferVersionService offerVersionService;

    @Autowired
    protected List<OfferVersionCharSpecValueUseMappingService> childCloneables;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = offerVersionCharspecUseRepository;
        this.parentService = offerVersionService;
        this.parentDependService = offerVersionService;
        this.commonRepository = offerVersionCharspecUseRepository;
        this.commonMapper = offerVersionCharspecUseMapper;
        this.childServices = childCloneables;
    }

    @Override
    public void setVersionIdForInsert(Long id, OfferVersionCharspecUse commonEntity) {
        commonEntity.setOfferVersionId(id);
    }

    @Override
    public OfferVersionCharspecUseDTO getDto() {
        return new OfferVersionCharspecUseDTO();
    }

    public void updateOfferVersionCharValue(OfferVersionCharspecValueUseDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        OfferVersionCharspecValueUse offerVersionCharspecValueUse = offerVersionCharspecValueUseRepository.findByIdAndDomainId(dto.getCharspecValueUseId(), domainId);
        offerVersionCharspecValueUse.setValue(dto.getValue());
        offerVersionCharspecValueUse.setDomainId(domainId);
        offerVersionCharspecValueUseRepository.save(offerVersionCharspecValueUse);
    }

    public Page<CharOfVersionDTO> listChildCharacteristic(Long id, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<CharOfVersionDTO> page = offerVersionCharspecUseRepository.getListChildCharacteristic(id, domainId, pageable);
        return page;
    }

    @Override
    protected String getResource() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return offerVersionCharspecUseRepository;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
