package com.mycompany.myapp.service.offer.common;

import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.normalizer.ChildCloneable;

public interface OfferTemplateVersionChildMappingCloneable extends OCSCloneMappingService {

}
