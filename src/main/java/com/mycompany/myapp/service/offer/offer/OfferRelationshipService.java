package com.mycompany.myapp.service.offer.offer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferRelationship;
import com.mycompany.myapp.dto.offer.common.OfferCustomDTO;
import com.mycompany.myapp.dto.offer.offer.OfferRelationshipDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferRelationshipMapper;
import com.mycompany.myapp.repository.offer.offer.OfferRelationshipRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.offer.common.OfferRelationshipCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class OfferRelationshipService extends OfferRelationshipCommonService<OfferRelationshipDTO, OfferRelationship> {

    @Autowired
    private OfferRelationshipRepository offerRelationshipRepository;

    @Autowired
    private OfferRelationshipMapper offerRelationshipMapper;

    @Autowired
    private OfferRepository offerRepository;

    @Override
    protected String getResource() {
        return Resources.OFFER;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.commonRepository = offerRelationshipRepository;
        this.commonMapper = offerRelationshipMapper;

    }

    @Override
    protected OfferRelationship getEntity() {
        return new OfferRelationship();
    }

    @Override
    protected void setProperties(OfferRelationship entity, Long parentId, Long childId, Boolean isParent) {
        Integer domainId = OCSUtils.getDomain();
        Offer offer = offerRepository.findByIdAndDomainId(parentId, domainId);
        if (offer == null) {
            throw new ResourceNotFoundException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER, ErrorKey.Offer.PARENT_ID);
        }
        offer = offerRepository.findByIdAndDomainId(childId, domainId);
        if (offer == null) {
            throw new ResourceNotFoundException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER, ErrorKey.Offer.CHILD_ID);
        }
        entity.setParentOfferId(parentId);
        entity.setChildOfferId(childId);
    }

    @Override
    protected Page<OfferCustomDTO> getListChild(Long id, Integer type, Boolean isParent, String name, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<OfferCustomDTO> page;
        if (!isParent) {
            page = offerRelationshipRepository.getListChildOffer(id, type, name, domainId, pageable);
            for (OfferCustomDTO item : page) {
                List<ReferTable> referTables = offerRelationshipRepository.getListRelationshipType(id, item.getId(), type, null, domainId);
                item.setRelationshipTypes(referTables);
            }
        }
        else {
            page = offerRelationshipRepository.getListParentOffer(id, type, name, domainId, pageable);
            for (OfferCustomDTO item : page) {
                List<ReferTable> referTables = offerRelationshipRepository.getListRelationshipType(item.getId(), id, type, null, domainId);
                item.setRelationshipTypes(referTables);
            }
        }
        return page;

    }

    public List<Long> getids(Long parentId){
        Integer domainId = OCSUtils.getDomain();
        List<Long> ids = new ArrayList<>();
       List<OfferRelationship> offerRelationships =offerRelationshipRepository.findByDomainIdAndParentId(domainId, parentId);
       List<OfferRelationship> listofferRelationships = offerRelationshipRepository.findByDomainIdAndChildId(domainId, parentId);
       List<Long> childOfferIds = offerRelationships.stream().map(x -> x.getChildOfferId()).collect(Collectors.toList());
       List<Long> parentOfferIds = listofferRelationships.stream().map(x -> x.getParentId()).collect(Collectors.toList());
       ids.addAll(childOfferIds);
       ids.addAll(parentOfferIds);
        return ids;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
