package com.mycompany.myapp.service.offer.offer;

import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.offer.OfferVersionAction;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionAction;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionActionDTO;
import com.mycompany.myapp.mapper.offer.offer.OfferVersionActionMapper;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionActionRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionRepository;
import com.mycompany.myapp.service.action.ActionService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferVersionChildMappingCloneable;
import com.mycompany.myapp.service.offer.common.VersionActionCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Order(value = 1)
public class OfferVersionActionService extends VersionActionCommonService<OfferVersionActionDTO, OfferVersionAction> implements OfferVersionChildMappingCloneable {
    @Autowired
    private OfferVersionActionRepository offerVersionActionRepository;
    @Autowired
    private OfferVersionActionMapper offerVersionActionMapper;
    @Autowired
    private OfferVersionRepository offerVersionRepository;
    @Autowired
    private ActionService actionService;
    @Autowired
    private OfferVersionService offerVersionService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.commonRepository = offerVersionActionRepository;
        this.commonMapper = offerVersionActionMapper;
        this.ocsCloneMapRepository = offerVersionActionRepository;
        this.childService = actionService;
        this.parentService = offerVersionService;
        this.parentDependService = offerVersionService;
    }

    public Page<OfferActionDTO> getListOfferVersionAction(Long offerVersionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        return offerVersionActionRepository.getListOfferVersionAction(offerVersionId, domainId, pageable);
    }

    @Override
    protected String getResource() {
        return Resources.OFFER_VERSION_ACTION;
    }

    @Override
    public void checkExistVersion(Long versionId, Integer domainId) {
        OfferVersion offerVersion = offerVersionRepository.findByIdAndDomainId(versionId, domainId);
        if (offerVersion == null) {
            throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.OFFER_VERSION, ErrorKey.ID);
        }
    }

    @Override
    public void checkDataContrain(Long childId, Integer domianId, Long parentId) {
        OfferVersionAction entity = offerVersionActionRepository.findByActionIdAndOfferVersionIdAndDomainId(childId, parentId, domianId);
        if (entity != null) {
            throw new DataConstrainException(ErrorMessage.OfferVersionAction.DATA_CONTRAIN, getResource(), ErrorKey.OfferVersionAction.ACTION);
        }
    }

    @Override
    public List<Long> getActionId(Long versionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferVersionAction> list = offerVersionActionRepository.findByDomainIdAndParentId(domainId, versionId);
        List<Long> actionId = list.stream().map(x -> x.getActionId()).collect(Collectors.toList());
        return actionId;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return offerVersionActionRepository;
    }
}
