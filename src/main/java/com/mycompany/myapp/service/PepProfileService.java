package com.mycompany.myapp.service;

import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.MonitorKey;
import com.mycompany.myapp.domain.PccRule;
import com.mycompany.myapp.domain.PepProfile;
import com.mycompany.myapp.domain.PepProfilePcc;
import com.mycompany.myapp.domain.Qos;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.PccRuleDTO;
import com.mycompany.myapp.dto.PepProfileDTO;
import com.mycompany.myapp.mapper.PepProfileMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.MonitorKeyRepository;
import com.mycompany.myapp.repository.PccRuleRepository;
import com.mycompany.myapp.repository.PepProfilePccRepository;
import com.mycompany.myapp.repository.PepProfileRepository;
import com.mycompany.myapp.repository.QosRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

@Transactional
@Service
public class PepProfileService extends OCSBaseService {

    @Autowired
    private PepProfileRepository pepProfileRepository;

    @Autowired
    private PepProfileMapper pepProfileMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private QosRepository qosRepository;

    @Autowired
    private MonitorKeyRepository monitorKeyRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PccRuleRepository pccRuleRepository;

    @Autowired
    private PepProfilePccRepository pepProfilePccRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = this.pepProfileRepository;
        this.parentService = this.categoryService;
    }


    public PepProfileDTO createProfilePep(PepProfileDTO profilePepDTO) {
        Integer domainId = OCSUtils.getDomain();

        checkCategoryExist(profilePepDTO.getCategoryId(),domainId);
        checkMonitorKeyExist(profilePepDTO.getMonitorKey(),domainId);
        checkQosExist(profilePepDTO.getQosId(),domainId);
        checkDuplicateName(profilePepDTO.getName(), profilePepDTO.getId());
        PepProfile profilePep = pepProfileMapper.toEntity(profilePepDTO);

        profilePep.setDomainId(domainId);
        super.setPosIndex(domainId, profilePep, profilePep.getCategoryId());
        profilePep = pepProfileRepository.save(profilePep);
        return pepProfileMapper.toDto(profilePep);
    }

    public Page<PepProfileDTO> getPepProfiles(Long categoryId, String name, String monitorKey, String priority, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Page<PepProfileDTO> pepProfileDTOS = pepProfileRepository.findPepProfiles(categoryId, domainId, name, monitorKey, priority, description, pageable).map(pepProfileMapper::toDto);
        for (PepProfileDTO item : pepProfileDTOS) {
            item.setHasChild(checkHasChild(item.getId(),domainId));
            item.setCategoryLevel(parentCategoryLevel);
        }
        return pepProfileDTOS;
    }

    private void checkQosExist(Long qosId, Integer domainId) {
        Qos qos = qosRepository.findByIdAndDomainId(qosId, domainId);
        if (qos == null) {
            throw new ResourceNotFoundException(ErrorMessage.Qos.NOT_FOUND, Resources.CATEGORY, ErrorKey.PepProfile.QOS_ID);
        }
    }


    private void checkMonitorKeyExist(Long monitorKey, Integer domainId) {
        MonitorKey entity = monitorKeyRepository.findByMonitorKeyAndDomainId(monitorKey, domainId);
        if (entity == null) {
            throw new ResourceNotFoundException(ErrorMessage.MonitorKey.NOT_FOUND, Resources.MONITOR_KEY, ErrorKey.MonitorKey.MONITOR_KEY);
        }
    }


    private void checkCategoryExist(Long categoryId, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(categoryId, domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.StateSet.CATEGORY_ID);
        }
        if (parentCategory.getCategoryType() != CategoryType.PEP_PROFILE) {
            throw new DataConstrainException(ErrorMessage.PepProfile.CATEGORY_TYPE, Resources.CATEGORY, ErrorKey.Category.CATEGORY_TYPE);
        }

    }

    public PepProfileDTO updateProfilePep(Long id, PepProfileDTO profilePepDTO) {
        Integer domainId = OCSUtils.getDomain();
        profilePepDTO.setId(id);
        PepProfile pepProfileDB = pepProfileRepository.findByIdAndDomainIdForUpdate(id, domainId);
        if (pepProfileDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.PepProfile.NOT_FOUND, Resources.PEP_PROFILE, ErrorKey.PepProfile.ID);
        }

        checkCategoryExist(profilePepDTO.getCategoryId(),domainId);
        checkMonitorKeyExist(profilePepDTO.getMonitorKey(),domainId);
        checkQosExist(profilePepDTO.getQosId(),domainId);
        checkDuplicateName(profilePepDTO.getName(), profilePepDTO.getId());

        PepProfile pepProfile = pepProfileMapper.toEntity(profilePepDTO);
        pepProfile.setDomainId(domainId);

        if (!Objects.equals(profilePepDTO.getCategoryId(), pepProfileDB.getCategoryId())) {
            setPosIndex(domainId, pepProfile, profilePepDTO.getCategoryId());
        }

        pepProfile = pepProfileRepository.save(pepProfile);
        return pepProfileMapper.toDto(pepProfile);
    }


    public PepProfileDTO getPepProfileDetail(@NotNull @Positive Long id) {
        Integer domainId = OCSUtils.getDomain();
        PepProfile pepProfile = pepProfileRepository.findByIdAndDomainId(id, domainId);
        if (pepProfile == null) {
            throw new ResourceNotFoundException(ErrorMessage.PepProfile.NOT_FOUND, Resources.PEP_PROFILE, ErrorKey.PepProfile.ID);
        }
        PepProfileDTO dto = pepProfileMapper.toDto(pepProfile);
        Category category = categoryRepository.findByIdAndDomainId(pepProfile.getCategoryId(), domainId);
        dto.setCategoryName(category.getName());
        dto.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        Qos qos = qosRepository.findByIdAndDomainId(pepProfile.getQosId(), domainId);
        dto.setQosName(qos.getName());
        MonitorKey key = monitorKeyRepository.findByMonitorKeyAndDomainId(pepProfile.getMonitorKey(), domainId);
        dto.setMonitorKeyName(key.getMonitorKey() + "(" + key.getRemark() + ")");
        dto.setHasChild(checkHasChild(id,domainId));
        return dto;
    }


    private boolean checkHasChild(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferWithPccRule = pepProfilePccRepository.countByPepProfileIdAndDomainId(id, domainId);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }


    public void deletePepProfile(@NotNull @Positive Long id) {
        Integer domainId = OCSUtils.getDomain();
        Integer countReferWithPccRule = pepProfilePccRepository.countByPepProfileIdAndDomainId(id, domainId);
        if (countReferWithPccRule > 0) {
            throw new DataConstrainException(ErrorMessage.PepProfile.CANNOT_DELETE, Resources.PEP_PROFILE, ErrorKey.PepProfile.ID);
        }
        pepProfileRepository.deleteById(id);
    }

    public Page<PccRuleDTO> getPccRules(@NotNull @Positive Long id, String name, String priority, String description,
            Pageable pageable) {
        List<PccRuleDTO> dtos = pepProfilePccRepository.getListPccRuleMapping(id, name, priority, description, pageable);
        List<Integer> indexes = pepProfilePccRepository.getListIndexPccRuleMapping(id, name, priority, description, pageable);
        Integer total = pepProfilePccRepository.getTotalPccRuleMapping(id, name, priority, description);
        for (int i = 0; i < dtos.size(); i ++) {
            dtos.get(i).setTreeType(TreeType.SUB_TEMPLATE);
            dtos.get(i).setPosIndex(indexes.get(i));
        }
        return new PageImpl<>(dtos, pageable, total);
    }

    public void deletePccRuleMapping(Long pepProfileId, Long pccRuleId) {
        Integer domainId = OCSUtils.getDomain();
            pepProfilePccRepository.deleteByPccRuleIdAndPepProfileIdAndDomainId(pccRuleId, pepProfileId, domainId);
    }


    public void createProfilePepPcc(Long id, List<Long> pccRuleIds) {
        Integer domainId = OCSUtils.getDomain();
        checkPepProfileExist(id, domainId);
        for (Long item : pccRuleIds) {
            checkPccRuleExist(item,domainId);
            checkDuplicateData(item,id,domainId);
            PepProfilePcc pepProfilePcc = new PepProfilePcc(null, item, id, domainId);
            setPosIndexMapping(domainId, pepProfilePcc, id);
            pepProfilePccRepository.save(pepProfilePcc);
        }
    }


    private void checkDuplicateData(Long pccRuleId, Long pepProfileId, Integer domainId) {
        Integer count  = pepProfilePccRepository.countByPccRuleIdAndPepProfileIdAndDomainId(pccRuleId, pepProfileId, domainId);
        if (count > 0) {
            throw new DataConstrainException(ErrorMessage.PepProfile.DUPLICATED_DATA, Resources.PEP_PROFILE, ErrorKey.PepProfile.PCC_RULE_ID);
        }

    }


    private void checkPccRuleExist(Long pccRuleId, Integer domainId) {
        PccRule pccRule = pccRuleRepository.findByIdAndDomainId(pccRuleId, domainId);
        if (pccRule == null) {
            throw new DataConstrainException(ErrorMessage.PccRule.NOT_FOUND, Resources.PEP_PROFILE, ErrorKey.ENTITY);
        }
    }


    private void checkPepProfileExist(Long pepProfileId, Integer domainId) {
        PepProfile pepProfile = pepProfileRepository.findByIdAndDomainId(pepProfileId, domainId);
        if (pepProfile == null) {
            throw new DataConstrainException(ErrorMessage.PepProfile.NOT_FOUND, Resources.PEP_PROFILE, ErrorKey.PepProfile.ID);
        }
    }

    private void setPosIndexMapping(Integer domainId, PepProfilePcc pepProfilePcc, Long newParentId) {
        PepProfilePcc maxIndexPepProfilePcc =
            pepProfilePccRepository.findTopByParentIdAndDomainIdOrderByPosIndexDesc(newParentId, domainId);
        if (maxIndexPepProfilePcc == null || maxIndexPepProfilePcc.getPosIndex() == null) {
            pepProfilePcc.setPosIndex(0);
        } else {
            pepProfilePcc.setPosIndex(maxIndexPepProfilePcc.getPosIndex() + 1);
        }
    }

    public BaseResponseDTO moveMapping(List<? extends Moveable> moveables, Long pepProfileId) {
        Integer domainId = OCSUtils.getDomain();
        // Check pos index đã bị xê dịch hay chua, đề phòng 2 người cùng xê dich cùng 1 lúc
        PepProfilePcc firstEntity =
            pepProfilePccRepository.findByPccRuleIdAndDomainIdAndPepProfileId(moveables.get(0).getId(), domainId, pepProfileId);
        if (firstEntity == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.PCC_RULE, ErrorKey.Category.ID);
        }
        PepProfilePcc secondEntity =
            pepProfilePccRepository.findByPccRuleIdAndDomainIdAndPepProfileId(moveables.get(1).getId(), domainId, pepProfileId);
        if (secondEntity == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.PCC_RULE, ErrorKey.Category.ID);
        }
        if (!Objects.equals(moveables.get(0).getPosIndex(), firstEntity.getPosIndex())) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.PCC_RULE, ErrorKey.Category.POS_INDEX);
        }
        if (!Objects.equals(moveables.get(1).getPosIndex(), secondEntity.getPosIndex())) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.PCC_RULE, ErrorKey.Category.POS_INDEX);
        }

        Integer tempPosIndex = firstEntity.getPosIndex();
        firstEntity.setPosIndex(secondEntity.getPosIndex());
        secondEntity.setPosIndex(tempPosIndex);

        pepProfilePccRepository.save(firstEntity);
        pepProfilePccRepository.save(secondEntity);

        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(firstEntity, response);
        return response;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.PEP_PROFILE;
    }
}
