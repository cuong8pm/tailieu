package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.MapAcmBalBal;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BalanceAcmMappingDTO;
import com.mycompany.myapp.dto.BalancesDTO;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.mapper.MapAcmBalBalMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.MapAcmBalBalRepository;
import com.mycompany.myapp.service.exception.DuplicateException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class BalanceAcmMappingService extends OCSBaseService {
    private final Logger log = LoggerFactory.getLogger(BalanceAcmMappingService.class);
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private MapAcmBalBalRepository mapAcmBalBalRepository;
    @Autowired
    private MapAcmBalBalMapper mapAcmBalBalMapper;
    @Autowired
    private BalancesService balancesService;

    @Autowired
    private CategoryService categoryService;

    @Override
    @PostConstruct
    public void setBaseRepository() {
        baseRepository = mapAcmBalBalRepository;
        this.parentService = categoryService;
    }

    public Page<BalanceAcmMappingDTO> getMapAcmBalBals(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        checkCategoryExist(categoryId, domainId);
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Page<BalanceAcmMappingDTO> mapAcmBalBalDTOS = mapAcmBalBalRepository.findMapAcmBalBals(categoryId, domainId, name, description, pageable).map(mapAcmBalBalMapper::toDto);
        for (BalanceAcmMappingDTO balanceAcmMappingDTO : mapAcmBalBalDTOS) {
            balanceAcmMappingDTO.setCategoryLevel(parentCategoryLevel);
        }
        return mapAcmBalBalDTOS;
    }

    public BalanceAcmMappingDTO getMapAcmBalBalDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        MapAcmBalBal mapAcmBalBal = mapAcmBalBalRepository.findByIdAndDomainId(id, domainId);
        if (mapAcmBalBal == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalanceAcm.NOT_FOUND, Resources.BALANCE_ACM_MAPPINGS,
                ErrorKey.MapAcmBalBal.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(mapAcmBalBal.getCategoryId(), domainId);
        BalanceAcmMappingDTO balanceAcmMappingDTO = mapAcmBalBalMapper.toDto(mapAcmBalBal);
        balanceAcmMappingDTO.setCategoryName(category.getName());
        balanceAcmMappingDTO.setCategoryLevel(getCategoryLevel(category.getId()));

        BalancesDTO fromBalancesDTO = balancesService.getBalTypeDetail(balanceAcmMappingDTO.getFromBalanceId());
        BalancesDTO toBalancesDTO = balancesService.getBalTypeDetail(balanceAcmMappingDTO.getToBalanceId());
        balanceAcmMappingDTO.setFromBalance(fromBalancesDTO);
        balanceAcmMappingDTO.setToBalance(toBalancesDTO);

        return balanceAcmMappingDTO;
    }


    public BalanceAcmMappingDTO saveMapAcmBalBal(BalanceAcmMappingDTO balanceAcmMappingDTO, boolean isCreate) {
        Integer domainId = OCSUtils.getDomain();

        validateMapAcmBalBal(balanceAcmMappingDTO);
        //check and lock
        checkCategoryExist(balanceAcmMappingDTO.getParentId(), domainId);
        balancesService.checkBalTypeInABMExist(balanceAcmMappingDTO.getFromBalanceId(),balanceAcmMappingDTO.getToBalanceId(), domainId);

        checkDuplicateName(balanceAcmMappingDTO.getName(), balanceAcmMappingDTO.getId());
        checkAcmMappingAlreadyExists(balanceAcmMappingDTO, domainId);

        MapAcmBalBal mapAcmBalBal = mapAcmBalBalMapper.toEntity(balanceAcmMappingDTO);
        mapAcmBalBal.setDomainId(domainId);
        if (isCreate) {
            setPosIndex(domainId, mapAcmBalBal);
        }

        mapAcmBalBal = mapAcmBalBalRepository.save(mapAcmBalBal);

        return mapAcmBalBalMapper.toDto(mapAcmBalBal);
    }

    public BaseResponseDTO updateMapAcmBalBal(BalanceAcmMappingDTO balanceAcmMappingDTO) {
        Integer domainId = OCSUtils.getDomain();
        MapAcmBalBal mapAcmBalBal = mapAcmBalBalRepository.findByIdAndDomainIdForUpdate(balanceAcmMappingDTO.getId(), domainId);
        if (mapAcmBalBal == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalanceAcm.NOT_FOUND, Resources.BALANCE_ACM_MAPPINGS, ErrorKey.MapAcmBalBal.ID);
        }

        return saveMapAcmBalBal(balanceAcmMappingDTO, false);
    }

    private void validateMapAcmBalBal(BalanceAcmMappingDTO balanceAcmMappingDTO) {
        if (balanceAcmMappingDTO.getFromBalanceId().equals(balanceAcmMappingDTO.getToBalanceId())) {
            log.error("From BanlanceId và End BalanceId không được trùng nhau!");
            throw new ResourceNotFoundException(ErrorMessage.BalanceAcm.A_PAIR_MUST_DIFFERENT, Resources.BALANCE_ACM_MAPPINGS, ErrorKey.MapAcmBalBal.FROM_BALANCE_ID);
        }
    }

    private void checkAcmMappingAlreadyExists(BalanceAcmMappingDTO balanceAcmMappingDTO, Integer domainId) {
        List<MapAcmBalBal> mapAcmBalBal = mapAcmBalBalRepository.findByDomainIdAndFromBalanceIdAndToBalanceId(domainId,
            balanceAcmMappingDTO.getFromBalanceId(), balanceAcmMappingDTO.getToBalanceId());
        if (!mapAcmBalBal.isEmpty() && !Objects.equals(balanceAcmMappingDTO.getId(), mapAcmBalBal.get(0).getId())) {
            log.error("mapping không được trùng");
            throw new DuplicateException(ErrorMessage.BalanceAcm.BALANCE_OR_ACM_BALANCE_ALREADY_MAPPING, Resources.BALANCE_ACM_MAPPINGS, ErrorKey.MapAcmBalBal.FROM_BALANCE_ID);
        }
    }

    // Check category exist or not
    private void checkCategoryExist(Long parentId, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(parentId, domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void checkMapAcmBalBalExist(Long mapAcmBalBalId, Integer domainId) {
        MapAcmBalBal mapAcmBalBal = mapAcmBalBalRepository.findByIdAndDomainIdForUpdate(mapAcmBalBalId, domainId);
        if (mapAcmBalBal == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalanceAcm.NOT_FOUND, Resources.BALANCE_ACM_MAPPINGS, ErrorKey.MapAcmBalBal.ID);
        }
    }

    public Boolean existByBalanceId(Long balanceId, Integer domainId){
        return mapAcmBalBalRepository.existsByDomainIdAndBalanceId(domainId,balanceId);
    }

    private void setPosIndex(Integer domainId, MapAcmBalBal mapAcmBalBal) {
        Integer maxPos = mapAcmBalBalRepository.getMaxPosIndex(domainId, mapAcmBalBal.getCategoryId());
        if (maxPos == null) {
            mapAcmBalBal.setPosIndex(0);
        } else {
            mapAcmBalBal.setPosIndex(maxPos + 1);
        }
    }

    public void deleteMapAcmBalBal(Long id) {
        Integer domainId = OCSUtils.getDomain();
        checkMapAcmBalBalExist(id, domainId);
        mapAcmBalBalRepository.deleteByIdAndDomainId(id, domainId);
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.BALANCES;
    }
}
