package com.mycompany.myapp.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.repository.ReferTableRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.Threshold;
import com.mycompany.myapp.dto.BalancesDTO;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ThresholdsDTO;
import com.mycompany.myapp.dto.UnitTypeDTO;
import com.mycompany.myapp.mapper.ThresholdMapper;
import com.mycompany.myapp.repository.ThresholdRepository;
import com.mycompany.myapp.service.exception.DuplicateException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import static org.springframework.transaction.annotation.Isolation.READ_UNCOMMITTED;

@Service
@Transactional
public class ThresholdService extends OCSBaseService {
    @Autowired
    private ThresholdRepository thresholdRepository;

    @Autowired
    private ThresholdMapper thresholdMapper;

    @Autowired
    ThresholdBaltypeMapService thresholdBaltypeMapService;
    @Autowired
    private BalancesService balancesService;

    @Autowired
    private UnitTypeService unitTypeService;

    @Autowired
    private ReferTableRepository referTableRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = thresholdRepository;
        this.parentService = balancesService;
        this.ocsCloneableRepository = thresholdRepository;
    }

    @Transactional(readOnly = true)
    public Page<ThresholdsDTO> getThresholds(Long balanceId, String name, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        balancesService.checkBalTypeExist(balanceId, domainId);
        Page<ThresholdsDTO> results = thresholdRepository.findByBalanceIdAndDomainId(balanceId, name, domainId, pageable).map(thresholdMapper::toDto);
        if (!CollectionUtils.isEmpty(results.getContent())) {
            List<ReferTable> referTable = referTableRepository.findByReferTypeOrderByValueAsc(ReferType.ThresholdType);
            BalancesDTO balance = balancesService.getBalTypeDetail(balanceId);
            UnitTypeDTO unitType = unitTypeService.getUnitTypeDetailSimple(balance.getUnitTypeId());
            for (ThresholdsDTO threshold : results) {
//                threshold.setValue(convertValueToGet(unitType.getDisplayRate(), threshold.getValue()));
                Optional<ReferTable> thresholdType = referTable.stream().filter(x -> x.getValue().equals(threshold.getThresholdType())).findFirst();
                thresholdType.ifPresent(table -> threshold.setThresholdTypeName(table.getName()));
                if (unitType != null) {
                    threshold.setBaseRate(unitType.getBaseRate());
                    threshold.setUnitPrecision(unitType.getUnitPrecision());
                }
            }
        }
        return results;
    }

    @Transactional(readOnly = true)
    public ThresholdsDTO getThresholdDetail(Long balanceId, Long thresholdId) {
        Integer domainId = OCSUtils.getDomain();
        Threshold threshold = thresholdRepository.findByIdAndDomainId(thresholdId, domainId);
        if (threshold == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.THRESHOLD, ErrorKey.ID);
        }
        ThresholdsDTO thresholdsDTO = thresholdMapper.toDto(threshold);
        balancesService.checkBalTypeExist(balanceId, domainId);
        Long unitTypeId = balancesService.getUnitTypeIdOfBalance(balanceId);
        UnitTypeDTO unitType = unitTypeService.getUnitTypeDetailSimple(unitTypeId);
        if (unitType != null) {
            thresholdsDTO.setBaseRate(unitType.getBaseRate());
            thresholdsDTO.setUnitPrecision(unitType.getUnitPrecision());
        }
        return thresholdsDTO;
    }

    public ThresholdsDTO createThreshold(ThresholdsDTO thresholds, Long balanceId) {
        Integer domainId = OCSUtils.getDomain();
        thresholds.setDomainId(domainId);

        validateNameAndExternalId(thresholds, balanceId);

        Threshold thresholdEntity = thresholdMapper.toEntity(thresholds);
        //thresholdEntity.setValue(convertValueToSave(balanceId, thresholdEntity.getValue()));
        thresholdEntity = thresholdRepository.save(thresholdEntity);
        if (thresholdEntity.getId() != null) {
            thresholds.setId(thresholdEntity.getId());
        }
        thresholdBaltypeMapService.createThresholdBalTypeMap(thresholds, balanceId);
        return thresholdMapper.toDto(thresholdEntity);
    }
    public void deleteThreshold(Long thresholdId, Long balanceId) {
        Integer domainId = OCSUtils.getDomain();
        thresholdBaltypeMapService.deleteByThresholdAndBalance(thresholdId, balanceId);
        //Nếu threshold đang được sử dụng bởi 1 balance nào khác thì không xóa.
        if (!thresholdBaltypeMapService.existByThreshold(thresholdId)) {
            thresholdRepository.deleteByIdAndId(domainId, thresholdId);
        }
    }

    public void deleteThresholdByBalanceId(Long balanceId){
        List<Long> thresholdIds = thresholdBaltypeMapService.getThresholdIdByBalanceId(balanceId);
        for (Long thresholdId: thresholdIds){
            deleteThreshold(thresholdId,balanceId);
        }
    }

    public ThresholdsDTO updateThreshold(ThresholdsDTO thresholds, Long balanceId) {
        Integer domainId = OCSUtils.getDomain();
        thresholds.setDomainId(domainId);
        Threshold thresholdCheck = thresholdRepository.findByIdAndDomainId(thresholds.getId(), thresholds.getDomainId());
        if (thresholdCheck == null) {
            throw new ResourceNotFoundException(ErrorMessage.Threshold.NOT_FOUND, Resources.THRESHOLD, ErrorKey.Threshold.ID);
        }
        validateNameAndExternalId(thresholds, balanceId);
        //thresholds.setValue(convertValueToSave(balanceId, thresholds.getValue()));
        Threshold thresholdEntity = thresholdRepository.save(thresholdMapper.toEntity(thresholds));
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(thresholds, response);
        return thresholdMapper.toDto(thresholdEntity);
    }

    @Transactional(readOnly = true)
    public void updateThresholdValue(Long balanceId, Long newUnitTypeId, Long oldUnitTypeId) {
        Integer domainId = OCSUtils.getDomain();
        List<Threshold> thresholds = thresholdRepository.findByBalanceIdAndDomainId(balanceId, domainId);
        Integer newBaseRate = unitTypeService.getBaseRateById(newUnitTypeId);
        Integer oldBaseRate = unitTypeService.getBaseRateById(oldUnitTypeId);
        for (Threshold threshold : thresholds) {
            Long newValue = (threshold.getValue() / oldBaseRate) * newBaseRate;
            threshold.setValue(newValue);
        }
        thresholdRepository.saveAll(thresholds);
    }

    public void updateThresholdValueByNewOldBaseRate(List<Long> balanceIds, Integer newBaseRate, Integer oldBaseRate) {
        Integer domainId = OCSUtils.getDomain();
        List<Threshold> thresholds = thresholdRepository.findThresholdByListBalanceIdAndDomainId(balanceIds, domainId);
        for (Threshold threshold : thresholds) {
            Long newValue = (threshold.getValue() / oldBaseRate) * newBaseRate;
            threshold.setValue(newValue);
        }
        thresholdRepository.saveAll(thresholds);
    }

    //Khi save thì lấy input nhân với displayRate của Balance
    private Long convertValueToSave(Long balanceId, Long value) {
        BalancesDTO balance = balancesService.getBalTypeDetail(balanceId);
        UnitTypeDTO unitType = unitTypeService.getUnitTypeDetail(balance.getUnitTypeId());
        return value * unitType.getDisplayRate();
    }

    // Lấy ra thì dùng giá trị đã lưu, chia cho displayRate của Balance
    private Long convertValueToGet(Long balanceId, Long value) {
        BalancesDTO balance = balancesService.getBalTypeDetail(balanceId);
        UnitTypeDTO unitType = unitTypeService.getUnitTypeDetail(balance.getUnitTypeId());
        return convertValueToGet(unitType.getDisplayRate(), value);
    }

    private Long convertValueToGet(Integer displayRate, Long value) {
        return value / displayRate;
    }

    private void validateNameAndExternalId(ThresholdsDTO thresholds, Long balanceId) {
        List<Threshold> thresholdOlds = thresholdRepository.findByBalanceIdAndDomainId(balanceId, thresholds.getDomainId());
        //validate name
        Optional<Threshold> threshold = thresholdOlds.stream().filter(x -> x.getName().equalsIgnoreCase(thresholds.getName())).findAny();
        if (threshold.isPresent() && !Objects.equals(thresholds.getId(), threshold.get().getId())) {
            throw new DuplicateException(ErrorMessage.Threshold.DUPLICATED_NAME, Resources.THRESHOLD, ErrorKey.Threshold.NAME);
        }
        //validate externalId
        threshold = thresholdOlds.stream().filter(x -> x.getExternalId().equalsIgnoreCase(thresholds.getExternalId())).findAny();
        if (threshold.isPresent() && !Objects.equals(thresholds.getId(), threshold.get().getId())) {
            throw new DuplicateException(ErrorMessage.Threshold.DUPLICATED_EXTERNAL_ID, Resources.THRESHOLD, ErrorKey.Threshold.EXTERNAL_ID);
        }
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    @Override
    protected String getResourceName() {
        return Resources.THRESHOLD;
    }
}
