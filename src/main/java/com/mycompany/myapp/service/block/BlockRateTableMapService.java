package com.mycompany.myapp.service.block;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.block.BlockRateTableMap;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.block.BlockRateTableMapDTO;
import com.mycompany.myapp.mapper.block.BlockRateTableMapMapper;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.block.BlockRateTableMapRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.clone.RateTableParentMappingService;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.ratetable.RateTableService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Order(value = 2)
public class BlockRateTableMapService extends AbstractCloneMappingService implements RateTableParentMappingService{

    @Autowired
    private BlockRateTableMapRepository blockRateTableMapRepository;
    @Autowired
    private BlockRateTableMapMapper blockRateTableMapMapper;
    @Autowired
    private BlockService blockService;
    @Autowired
    private RateTableService rateTableService;

    @Override
    protected String getResource() {
        return null;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = blockRateTableMapRepository;
        this.childService = rateTableService;
        this.parentService = blockService;
        this.parentDependService = blockService;
    }

    @Override
    public Map<Long, List<? extends Tree>> getChildren(List<Long> parentIds) {
        Integer domainId = OCSUtils.getDomain();

        List<OCSCloneableMap> cloneableMaps = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, parentIds);
        Map<Long, List<OCSCloneableMap>> parentChildrenMap = cloneableMaps.stream().collect(Collectors.groupingBy(OCSCloneableMap::getParentId));
        Map<Long, List<? extends Tree>> finalResults = new HashMap();

        List<Long> childIds = cloneableMaps.stream().map(OCSCloneableMap::getChildId).distinct().collect(Collectors.toList());

        Map<Long, ? extends Tree> childrenMap = childService.getCloneableTree(childIds);




        for (Long parentId : parentChildrenMap.keySet()) {
            Integer BASIC = 1;
            BlockRateTableMapDTO basicType = addTreeComponentType("block-basic", BASIC, "Basic");
            Integer DISCOUNT = 2;
            BlockRateTableMapDTO discountType = addTreeComponentType("block-discount", DISCOUNT, "Discount");
            Integer ACCUMULATE = 3;
            BlockRateTableMapDTO accumulateType = addTreeComponentType( "block-accumulate", ACCUMULATE, "Accumulate");
            List<OCSCloneableMap> childMaps = parentChildrenMap.get(parentId);
            Collections.sort(childMaps, (o1, o2) -> o1.getPosIndex().compareTo(o2.getPosIndex()));
            List<Tree> branches = new ArrayList<>();
            for (OCSCloneableMap childMap : childMaps) {

                List<Tree> childEntities = new ArrayList<>();
                childEntities.add(childrenMap.get(childMap.getChildId()));
                for (Tree childEntity : childEntities) {
                    BlockRateTableMap tableMap  = (BlockRateTableMap) childMap;
                    addTreeChild(basicType, tableMap, childEntity, BASIC);
                    addTreeChild(discountType, tableMap, childEntity, DISCOUNT);
                    addTreeChild(accumulateType, tableMap, childEntity, ACCUMULATE);
                }

            }
            if (!basicType.getTemplates().isEmpty()) {
                branches.add(basicType);
            }
            if (!discountType.getTemplates().isEmpty()) {
                branches.add(discountType);
            }
            if (!accumulateType.getTemplates().isEmpty()) {
                branches.add(accumulateType);
            }
            finalResults.put(parentId, branches);
        }
        return finalResults;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }

    private void addTreeChild(BlockRateTableMapDTO mapDTO, BlockRateTableMap tableMap, Tree childEntity, Integer componentType) {
        if (Objects.equals(tableMap.getComponentType(), componentType)) {
            if (childEntity != null) {
                Tree tempTree = childEntity;
                tempTree.setComponentType(componentType);
                mapDTO.getTemplates().add(tempTree);
                mapDTO.setHasChild(true);
            }
        }
    }

    private BlockRateTableMapDTO addTreeComponentType(String treeType, Integer componentType, String name) {
        BlockRateTableMapDTO basicType = new BlockRateTableMapDTO();
        basicType.setName(name);
        basicType.setType(treeType);
        basicType.setComponentType(componentType);
        basicType.setTemplates(new ArrayList<>());
        basicType.setHasChild(false);
        return basicType;
    }

    //custom clone mapping by componentType

    @Override
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        if (ocsCloneMapRepository == null || childService == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }

        Integer domainId = OCSUtils.getDomain();
        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);

        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<CloneDTO> idsToCreateNew = cloneDTOs.stream().filter(cloneDTO -> cloneDTO.getId() != null).collect(Collectors.toList());
        for (CloneDTO cloneDTO : idsToCreateNew) {
            cloneDTO.setName("");
        }
        List<OCSCloneableEntity> newClonedEntities = childService.cloneEntities(idsToCreateNew, oldNewChildMap);

        long batchSize = 30;
        long count = 0;

        Map<Long, Long> newOldParentIdMap = new HashMap<>();

        for (Long newId : oldNewIdParentMap.keySet()) {
            newOldParentIdMap.put(newId, oldNewIdParentMap.get(newId).getId());

        }

        List<OCSCloneableMap> newMap = new ArrayList<>();
        for (Long newParentId : newParentIds) {
            Long oldParentId = newOldParentIdMap.get(newParentId);
            List<OCSCloneableMap> oldMappingParents = mappingParents.stream().filter(i -> Objects.equals(i.getParentId(), oldParentId)).collect(Collectors.toList());
            for (OCSCloneableMap oldMappingParent : oldMappingParents) {
                List<Long> childrenIds = parentNewChildrenOldIdMap.get(newParentId);
                Long childId = getChildId(oldMappingParent,childrenIds,oldNewChildMap, newParentId);
                OCSCloneableMap newCloneMap = SerializationUtils.clone(oldMappingParent);
                newCloneMap.setChildMappingId(childId);
                newCloneMap.clearId();
                newCloneMap.setParentMappingId(newParentId);
                newMap.add(newCloneMap);

                entityManager.persist(newCloneMap);
                if (count % batchSize == 0 && count > 0) {
                    entityManager.flush();
                    entityManager.clear();
                    newMap.clear();
                }
                count++;
            }
        }

        if (!CollectionUtils.isEmpty(newMap)) {
            entityManager.flush();
            entityManager.clear();
            newMap.clear();
        }
    }

    @Override
    public Long getChildId(OCSCloneableMap mappingParent, List<Long> childrenIds, Map<Long, CloneDTO> oldNewChildMap, Long newParentId) {
        Long childId = mappingParent.getChildId();
        Boolean isCloneNew = childrenIds.contains(childId);
        // if this child is create new

        Long finalChildId = childId;
        Optional<Long> newChildId = oldNewChildMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue().getId(), finalChildId)
                && Objects.equals(newParentId, entry.getValue().getParentId())
                && Objects.equals(entry.getValue().getComponentType(), mappingParent.getComponentType()))
            .map(Map.Entry::getKey)
            .findAny();

        if (newChildId.isPresent() && isCloneNew) {
            return newChildId.get();
        }
        return childId;
    }
}
