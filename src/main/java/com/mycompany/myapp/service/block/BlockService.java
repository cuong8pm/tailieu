package com.mycompany.myapp.service.block;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.block.BlockRateTableMap;
import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.constant.BlockComponentType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.dto.block.BlockRateTableMapDTO;
import com.mycompany.myapp.dto.block.BlockRateTableTypeDTO;
import com.mycompany.myapp.dto.characteristic.CharacteristicDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.mapper.block.BlockMapper;
import com.mycompany.myapp.mapper.block.BlockRateTableMapMapper;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.mapper.characteristic.CharacteristicMapper;
import com.mycompany.myapp.mapper.ratetable.RateTableMapper;
import com.mycompany.myapp.mapper.ratingfilter.RatingFilterMapper;
import com.mycompany.myapp.repository.BalTypeRepository;
import com.mycompany.myapp.repository.ObjectFieldRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.block.BlockRateTableMapRepository;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentBlockMapRepository;
import com.mycompany.myapp.repository.characteristic.CharacteristicRepository;
import com.mycompany.myapp.repository.ratetable.RateTableColumnsRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRateTableMapRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.RatingFilterParentService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.pricecomponent.PriceComponentBlockMapService;
import com.mycompany.myapp.service.ratetable.RateTableColumnService;
import com.mycompany.myapp.service.ratingfilter.RatingFilterService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class BlockService extends AbstractCloneService implements RatingFilterParentService {

    @Autowired
    private BlockRepository blockRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private List<BlockRateTableMapService> blockRateTableMapServices;
    @Autowired
    private BlockMapper blockMapper;
    @Autowired
    private ReferTableRepository referTableRepository;
    @Autowired
    private BlockRateTableMapRepository blockRateTableMapRepository;
    @Autowired
    private PriceComponentBlockMapRepository priceComponentBlockMapRepository;
    @Autowired
    private BlockRateTableMapMapper blockRateTableMapMapper;
    @Autowired
    private RatingFilterRepository ratingFilterRepository;
    @Autowired
    private RatingFilterMapper ratingFilterMapper;
    @Autowired
    private List<RatingFilterService> ratingFilterService;
    @Autowired
    private RateTableRepository rateTableRepository;
    @Autowired
    private RateTableMapper rateTableMapper;
    @Autowired
    private RateTableColumnsRepository rateTableColumnsRepository;
    @Autowired
    private RatingFilterRateTableMapRepository ratingFilterRateTableMapRepository;
    @Autowired
    private List<PriceComponentBlockMapService> priceComponentBlockMapService;
    @Autowired
    private ObjectFieldRepository objectFieldRepository;
    @Autowired
    private BalTypeRepository balTypeRepository;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private CharacteristicRepository characteristicRepository;
    @Autowired
    private CharacteristicMapper characteristicMapper;
    @Autowired
    private RateTableColumnService rateTableColumnService;
    @Autowired
    private OfferRepository offerRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = blockRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = blockRepository;
        this.cloneMappingServices = blockRateTableMapServices;
        this.parentDependMappingServices = priceComponentBlockMapService;
        this.childDependServices = ratingFilterService;
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.RATE_TABLE);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new BlockDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.BLOCK;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Block.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.BLOCK;
    }

    @Override
    protected String getNameCategory() {
        return "Blocks";
    }

    public List<ReferTable> getBlockType() {
        return referTableRepository.getReferTablesByReferType(ReferType.BlockType, sortByValue());
    }

    private void checkCategoryExist(BlockDTO blockDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(blockDTO.getParentId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.RatingFilter.CATEGORY_ID);
        }
        if (parentCategory.getCategoryType() != CategoryType.RATING_FILTER) {
            throw new DataConstrainException(ErrorMessage.Block.CATEGORY_BLOCK, Resources.CATEGORY, ErrorKey.Category.CATEGORY_TYPE);
        }
    }

    private boolean checkHasChild(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferWithPccRule = blockRateTableMapRepository.countByDomainIdAndParentId(domainId, id);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    public Page<BlockDTO> getBlocks(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }

        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;

        Page<BlockDTO> blockDTOS = blockRepository.findBlocks(
            searchCriteria.getParentId(),
            searchCriteria.getDomainId(),
            searchCriteria.getName(),
            searchCriteria.getDescription(), pageable).map(blockMapper::toDto);

        for (BlockDTO blockDTO : blockDTOS) {
            blockDTO.setHasChild(checkHasChild(blockDTO.getId(), domainId));
            blockDTO.setCategoryLevel(parentCategoryLevel + 1);
        }
        return blockDTOS;
    }

    public Page<BlockDTO> getBlockWithPage(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        Page<BlockDTO> blockDTOS = blockRepository.findBlocks(categoryId, domainId, name, description, pageable).map(block -> blockMapper.toDto(block));
        for (BlockDTO blockDTO : blockDTOS) {
            blockDTO.setCategoryName(category.getName());
            blockDTO.setCategoryLevel(parentCategoryLevel);
            blockDTO.setHasChild(checkHasChild(blockDTO.getId(), domainId));
        }
        return blockDTOS;
    }

    public List<TreeClone> getBlockInTree(Long blockId) {
        Integer domainId = OCSUtils.getDomain();
        Block block = blockRepository.findByIdAndDomainId(blockId, domainId);
        if (block == null) {
            throw new ResourceNotFoundException(ErrorMessage.Block.NOT_FOUND, Resources.BLOCK, ErrorKey.Block.ID);
        }
//        BlockDTO blockDTO = blockMapper.toDto(blockRepository.findByIdAndDomainId(blockId, domainId));
        List<BlockRateTableMapDTO> blockRateTableMapDTO = blockRateTableMapMapper.toDto(blockRateTableMapRepository.findByDomainIdAndParentId(domainId, blockId));

        List<TreeClone> results = new ArrayList<>();
        BlockRateTableTypeDTO basic = getBlockRateTableMap(BlockComponentType.BASIC, blockRateTableMapDTO);
        if (basic != null) {
            results.add(basic);
        }

        BlockRateTableTypeDTO discount = getBlockRateTableMap(BlockComponentType.DISCOUNT, blockRateTableMapDTO);
        if (discount != null) {
            results.add(discount);
        }

        BlockRateTableTypeDTO accumulate = getBlockRateTableMap(BlockComponentType.ACCUMULATE, blockRateTableMapDTO);
        if (accumulate != null) {
            results.add(accumulate);
        }
        results.stream().forEach(result -> {
            result.setId(blockId);
        });

        RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(block.getBlockFilterId(), domainId);
        if (ratingFilter != null) {
//            RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilter);
            BlockRateTableTypeDTO blockFilter = new BlockRateTableTypeDTO();
            blockFilter.setName("Block Filter");
            blockFilter.setHasChild(true);
            blockFilter.setType(Resources.BLOCK_FILTER);
            blockFilter.setId(block.getBlockFilterId());
            results.add(blockFilter);
        }

        return results;
    }

    private BlockRateTableTypeDTO getBlockRateTableMap(BlockComponentType blockComponentType,
            List<BlockRateTableMapDTO> blockRateTableMaps) {
        List<BlockRateTableMapDTO> filterResult = blockRateTableMaps.stream().filter(
                blockRateTable -> Objects.equals(blockComponentType.getValue(), blockRateTable.getComponentType()))
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(filterResult)) {
            BlockRateTableTypeDTO blockRateTableTypeDTO = new BlockRateTableTypeDTO();
            switch (blockComponentType) {
            case BASIC:
                blockRateTableTypeDTO.setName("Basic");
                blockRateTableTypeDTO.setType(Resources.BLOCK_BASIC);
                break;
            case DISCOUNT:
                blockRateTableTypeDTO.setName("Discount");
                blockRateTableTypeDTO.setType(Resources.BLOCK_DISCOUNT);
                break;
            case ACCUMULATE:
                blockRateTableTypeDTO.setName("Accumulate");
                blockRateTableTypeDTO.setType(Resources.BLOCK_ACCMULATE);
                break;
            default:
                break;
            }
            blockRateTableTypeDTO.setHasChild(true);
            return blockRateTableTypeDTO;
        }

        return null;
    }

    @Transactional
    public BlockDTO createBlock(BlockDTO blockDTO) {
        Integer domainId = OCSUtils.getDomain();
        blockDTO.setDomainId(domainId);
        Block block = blockMapper.toEntity(blockDTO);
        checkDuplicateName(blockDTO.getName(), block.getId());
        checkCategory(blockDTO.getCategoryId());
        setPosIndex(domainId, block, block.getCategoryId());
        if(blockDTO.getBlockFilterId() != null){
            RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(blockDTO.getBlockFilterId(), domainId);
            if(ratingFilter == null){
                throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.Block.RATING_FILTER_NAME_NOT_FOUND);
            }
        }
        validateAffectedObject(blockDTO);
        block = blockRepository.save(block);
        if (CollectionUtils.isEmpty(blockDTO.getRateTableItems())) {
            throw new DataInvalidException(ErrorMessage.Block.INVALID_RATE_TABLE, Resources.BLOCK, ErrorKey.Block.RATE_TABLE);
        }

        List<BlockRateTableMap> lstRateTablesOfBlock = blockRateTableMapMapper.toEntity(blockDTO.getRateTableItems());
        int countRateTableOfBasicTab = 0;
        for (BlockRateTableMap blockRateTableMap : lstRateTablesOfBlock) {
            if (blockRateTableMap.getComponentType() == 1) {
                countRateTableOfBasicTab++;
            }
            blockRateTableMap.setBlockId(block.getId());
            blockRateTableMap.setDomainId(domainId);
            RateTable rateTable = rateTableRepository.findByIdAndDomainId(blockRateTableMap.getRateTableId() ,domainId);
            if (rateTable == null) {
                throw new DataInvalidException(ErrorMessage.RateTable.NOT_FOUND, Resources.BLOCK, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        if (countRateTableOfBasicTab <= 0) {
            throw new DataInvalidException(ErrorMessage.Block.INVALID_RATE_TABLE, Resources.BLOCK, ErrorKey.Block.RATE_TABLE);
        }
        lstRateTablesOfBlock = blockRateTableMapRepository.saveAll(lstRateTablesOfBlock);
        blockDTO = blockMapper.toDto(block);
        blockDTO.setRateTableItems(blockRateTableMapMapper.toDto(lstRateTablesOfBlock));
        blockDTO.setHasChild(true);
        return blockDTO;
    }

    private long timeabc = 0;


    @Transactional
    public BlockDTO updateBlock(BlockDTO blockDTO) {
        Integer domainId = OCSUtils.getDomain();
        Block blockDB = blockRepository.findByIdAndDomainIdForUpdate(blockDTO.getId(), domainId);
        if (blockDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.Block.NOT_FOUND, Resources.BLOCK, ErrorKey.Block.ID);
        }
        if(blockDTO.getBlockFilterId() != null){
            RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(blockDTO.getBlockFilterId(), domainId);
            if(ratingFilter == null){
                throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.Block.RATING_FILTER_NAME_NOT_FOUND);
            }
        }
        validateAffectedObject(blockDTO);
        blockDTO.setDomainId(domainId);
        Block block = blockMapper.toEntity(blockDTO);
        if (!Objects.equals(blockDTO.getCategoryId(), blockDB.getCategoryId())) {
            setPosIndex(domainId, block, blockDTO.getCategoryId());
        }
        if (!Objects.equals(blockDTO.getName(), blockDB.getName())) {
            checkDuplicateName(block.getName(), block.getId());
        }
        checkCategory(blockDTO.getCategoryId());
        block = blockRepository.save(block);
        List<BlockRateTableMapDTO> lstRateTablesOfBlock = blockDTO.getRateTableItems();
        List<BlockRateTableMapDTO> removeRateTables = new ArrayList<>();
        if(!CollectionUtils.isEmpty(lstRateTablesOfBlock)) {
            for (BlockRateTableMapDTO blockRateTableMapDTO : lstRateTablesOfBlock) {
                RateTable rateTable = rateTableRepository.findByIdAndDomainId(blockRateTableMapDTO.getRateTableId(), domainId);
                if (rateTable == null) {
                    throw new DataInvalidException(ErrorMessage.RateTable.NOT_FOUND, Resources.BLOCK, ErrorKey.ENTITY_NOT_FOUND);
                }
                if (ActionType.DELETE.equals(blockRateTableMapDTO.getActionType())) {
                    removeRateTables.add(blockRateTableMapDTO);
                }
                blockRateTableMapDTO.setDomainId(domainId);
            }
            lstRateTablesOfBlock.removeAll(removeRateTables);
            Long countUpdatedBasicRateTable = lstRateTablesOfBlock.stream().filter(i -> i.getComponentType() == 1 && !ActionType.DELETE.equals(i.getActionType())).count();
            blockRateTableMapRepository.deleteAll(blockRateTableMapMapper.toEntity(removeRateTables));
            Integer countRateTableOfBasicTab = blockRateTableMapRepository.countByComponentTypeAndBlockIdAndDomainId(1, blockDTO.getId(), domainId);
            if (countRateTableOfBasicTab <= 0 && countUpdatedBasicRateTable <= 0) {
                throw new DataInvalidException(ErrorMessage.Block.INVALID_RATE_TABLE, Resources.BLOCK, ErrorKey.Block.RATE_TABLE);
            }

            if (!CollectionUtils.isEmpty(lstRateTablesOfBlock)) {
                List<Long> editRateTables = lstRateTablesOfBlock.stream()
                    .filter(item -> ActionType.EDIT.equals(item.getActionType()))
                    .map(BlockRateTableMapDTO::getRateTableId).collect(Collectors.toList());
                if(editRateTables != null && editRateTables.size() > 0){
                    List<RateTable> rateTables = rateTableRepository.findAllById(editRateTables);
                    for (RateTable rateTable : rateTables) {
                        blockDTO.getOldRateTableItems().forEach(item -> {
                            if(ActionType.EDIT.equals(item.getActionType())
                                && Objects.equals(item.getId(),rateTable.getId())
                                && !Objects.equals(item.getPosIndex(),rateTable.getPosIndex())
                            ){
                                throw new DataInvalidException(ErrorMessage.Block.DATA_HAS_BEEN_MODIFIED,getResourceName(), ErrorKey.ID);
                            }
                        });
                    }
                }
                List<BlockRateTableMap> lstBlockRateTableMaps = blockRateTableMapMapper.toEntity(lstRateTablesOfBlock);
                lstBlockRateTableMaps = blockRateTableMapRepository.saveAll(lstBlockRateTableMaps);
                blockDTO.setRateTableItems(blockRateTableMapMapper.toDto(lstBlockRateTableMaps));
            }
        }
        blockDTO = blockMapper.toDto(block);
        blockDTO.setHasChild(true);
        return blockDTO;
    }

    public void validateAffectedObject(BlockDTO blockDTO){
        Integer domainId = OCSUtils.getDomain();
        Integer type = blockDTO.getAffectedObjectType();
        switch (type){
            case 3:
                BalType balType = balTypeRepository.findByIdAndDomainId(blockDTO.getAffectedObject(),domainId);
                if(balType == null){
                    throw new ResourceNotFoundException(ErrorMessage.AFFECTED_OBJECT_NOT_FOUND,Resources.BAL_TYPE,ErrorKey.Block.AFFECTED_OBJECT_NAME);
                }
                break;
            case 4:
                BalType balTypeACM = balTypeRepository.findByIdAndDomainId(blockDTO.getAffectedObject(),domainId);
                if(balTypeACM == null){
                    throw new ResourceNotFoundException(ErrorMessage.AFFECTED_OBJECT_NOT_FOUND,Resources.BAL_TYPE,ErrorKey.Block.AFFECTED_OBJECT_NAME);
                }
                break;
            case 5:
                Characteristic characteristic = characteristicRepository.findByIdAndDomainId(blockDTO.getAffectedObject(),domainId);
                if(characteristic == null){
                    throw new ResourceNotFoundException(ErrorMessage.AFFECTED_OBJECT_NOT_FOUND,Resources.CHARACTERISTIC,ErrorKey.Block.AFFECTED_OBJECT_NAME);
                }
                break;
            case 6:
                Offer offer = offerRepository.findByIdAndDomainId(blockDTO.getAffectedObject(),domainId);
                if(offer == null){
                    throw new ResourceNotFoundException(ErrorMessage.AFFECTED_OBJECT_NOT_FOUND,Resources.OFFER,ErrorKey.Block.AFFECTED_OBJECT_NAME);
                }
                break;
            default:
                break;
        }
        return;
    }

    public List<RateTableDTO> getRateTableByComponentType(Integer componentType, Long blockId) {
        Integer domainId = OCSUtils.getDomain();
        List<BlockRateTableMap> blockRateTableMaps = blockRateTableMapRepository.findByDomainIdAndBlockIdAndComponentTypeOrderByPosIndexAsc(domainId, blockId, componentType);
        List<BlockRateTableMapDTO> blockRateTableMapDTO = blockRateTableMapMapper.toDto(blockRateTableMaps);
        List<RateTableDTO> rateTableDTOS = new ArrayList<>();

        List<Long> rateTableIds = blockRateTableMapDTO.stream().map(BlockRateTableMapDTO::getRateTableId).collect(Collectors.toList());
        List<RateTable>  rateTables = rateTableRepository.findByDomainIdAndIdIn(domainId,rateTableIds);
        Map<Long, BlockRateTableMapDTO> blockRateMap = blockRateTableMapDTO.stream().collect(Collectors.toMap(BlockRateTableMapDTO::getRateTableId,blockRateTableMapDTO1 -> blockRateTableMapDTO1));
        Map<Long,Integer> childrenRateTableMap = rateTableColumnService.countChildrenByParentMap(rateTableIds,domainId);

        for (RateTable rateTable: rateTables) {
            BlockRateTableMapDTO rateTableMapDTO = blockRateMap.get(rateTable.getId());
            RateTableDTO rateTableDTO = rateTableMapper.toDto(rateTable);
            rateTableDTO.setHasChild(checkHasChildOfChildren(childrenRateTableMap,rateTable.getId()));
            rateTableDTO.setPosIndex(rateTableMapDTO.getPosIndex());
            rateTableDTO.setParentId(blockId);
            rateTableDTOS.add(rateTableDTO);
        }
        rateTableDTOS.sort(Comparator.comparing(RateTableDTO::getPosIndex));
        return rateTableDTOS;
    }


    private boolean checkHasChildRatingFilter(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferWithPccRule = ratingFilterRateTableMapRepository.countByRatingFilterIdAndDomainId(id, domainId);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    public List<RatingFilterDTO> getRatingFilter(Long ratingFilterId) {
        List<RatingFilterDTO> ratingFilterDTOS = new ArrayList<>();
        Integer domainId = OCSUtils.getDomain();
        RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterRepository.findByIdAndDomainId(ratingFilterId, domainId));
        if (ratingFilterDTO == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }
        ratingFilterDTO.setHasChild(checkHasChildRatingFilter(ratingFilterId, domainId));
        ratingFilterDTOS.add(ratingFilterDTO);
        return ratingFilterDTOS;
    }

    public List<ReferTable> getAffectedObjectType() {
        return referTableRepository.getReferTablesByReferType(ReferType.AffectedObjectType, sortByValue());
    }

    public List<ReferTable> getBlockMode() {
        return referTableRepository.getReferTablesByReferType(ReferType.BlockMode, sortByValue());
    }

    public BlockDTO getBlockDetail(Long blockId) {
        Integer domainId = OCSUtils.getDomain();
        Block block = blockRepository.findByIdAndDomainId(blockId, domainId);
        if (block == null) {
            throw new ResourceNotFoundException(ErrorMessage.Block.NOT_FOUND, Resources.BLOCK, ErrorKey.Block.ID);
        }
        BlockDTO blockDTO = blockMapper.toDto(block);
        if (blockDTO.getAffectedObject() != null && blockDTO.getAffectedObjectType() == 3) {
            BalType balType = balTypeRepository.findByIdAndDomainId(blockDTO.getAffectedObject(), domainId);
            blockDTO.setAffectedObject(balType.getId());
            blockDTO.setAffectedObjectName(balType.getName());
        }
        if (blockDTO.getAffectedObject() != null && blockDTO.getAffectedObjectType() == 4) {
            BalType balType = balTypeRepository.findByIdAndDomainId(blockDTO.getAffectedObject(), domainId);
            blockDTO.setAffectedObject(balType.getId());
            blockDTO.setAffectedObjectName(balType.getName());
        }
        if (blockDTO.getAffectedObject() != null && blockDTO.getAffectedObjectType() == 5) {
            CharacteristicDTO characteristicDTO = characteristicMapper.toDto(characteristicRepository.findByIdAndDomainId(blockDTO.getAffectedObject(), domainId));
            blockDTO.setAffectedObject(characteristicDTO.getId());
            blockDTO.setAffectedObjectName(characteristicDTO.getName());
        }
        if (blockDTO.getAffectedObject() != null && blockDTO.getAffectedObjectType() == 6) {
            String offerName = getNameOffer(blockDTO.getAffectedObject(), domainId);
            Long offerId = getOfferId(blockDTO.getAffectedObject(), domainId);
            blockDTO.setAffectedObject(offerId);
            blockDTO.setAffectedObjectName(offerName);
        }
        if (blockDTO.getBlockFilterId() != null) {
            RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterRepository.findByIdAndDomainId(blockDTO.getBlockFilterId(), domainId));
            blockDTO.setBlockFilterName(ratingFilterDTO.getName());
        }
        if (!blockDTO.getAffectedField().equals("") && blockDTO.getAffectedField() != null) {
            ObjectField objectField = objectFieldRepository.findByName(blockDTO.getAffectedField());
            blockDTO.setAffectedObjectFieldNameId(objectField.getId());
        }

        Category category = categoryRepository.findByIdAndDomainIdForUpdate(blockDTO.getCategoryId(), domainId);
        if (category != null) {
            blockDTO.setCategoryId(category.getId());
            blockDTO.setCategoryName(category.getName());
            blockDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        blockDTO.setHasChild(checkHasChild(blockDTO.getId(), domainId));
        List<BlockRateTableMapDTO> blockRateTableMapDTOS = blockRateTableMapMapper.toDto(blockRateTableMapRepository.findByDomainIdAndParentIdOrderByComponentTypeAscPosIndexAsc(domainId, blockDTO.getId()));

        List<Long> rateTableIds = blockRateTableMapDTOS.stream().map(BlockRateTableMapDTO::getRateTableId).collect(Collectors.toList());

        List<RateTable> rateTables = rateTableRepository.findByDomainIdAndIdIn(domainId,rateTableIds);

        Map<Long,RateTable> rateTableMap = rateTables.stream().collect(Collectors.toMap(RateTable::getId,rateTable -> rateTable));

        for (BlockRateTableMapDTO blockRateTableMapDTO : blockRateTableMapDTOS) {
            RateTableDTO rateTableDTO = rateTableMapper.toDto(rateTableMap.get(blockRateTableMapDTO.getRateTableId()));
            blockRateTableMapDTO.setName(rateTableDTO.getName());
            blockRateTableMapDTO.setDescription(rateTableDTO.getDescription());
        }
        blockDTO.setRateTableItems(blockRateTableMapDTOS);
        return blockDTO;
    }

    public void deleteBlock(Long blockId) {
        deleteEntity(blockId);
    }
    public List<Long> getRemoveIds(Long priceComponentId){
        Integer domainId = OCSUtils.getDomain();
       return priceComponentBlockMapRepository.getRateTableIds(priceComponentId, domainId);
    }

    public List<ObjectField> getAffectedObjectFieldName(Long id) {
        return objectFieldRepository.findByObjectFieldParent(id);
    }


    public void moveChild(List<RateTableDTO> rateTableDTOS, Long blockId, Integer componentType) {
        Integer domainId = OCSUtils.getDomain();
        if (rateTableDTOS.size() != 2) {
            throw new BadRequestAlertException(ErrorMessage.QUANTITY_INVALID, Resources.BLOCK, ErrorKey.Block.RATE_TABLE);
        }
        BlockRateTableMap firstEntity = blockRateTableMapRepository.findByDomainIdAndBlockIdAndRateTableIdAndComponentType(domainId, blockId,
            rateTableDTOS.get(0).getId(), componentType);
        checkNull(firstEntity);

        BlockRateTableMap secondEntity = blockRateTableMapRepository.findByDomainIdAndBlockIdAndRateTableIdAndComponentType(domainId, blockId,
            rateTableDTOS.get(1).getId(), componentType);
        checkNull(secondEntity);

        // Check pos index đã bị xê dịch hay chua, đề phòng 2 người cùng xê dich cùng 1
        // lúc
        if (!Objects.equals(rateTableDTOS.get(0).getPosIndex(), firstEntity.getPosIndex())) {
            throw new ResourceConflictException(ErrorMessage.POS_INDEX_INCORRECT, Resources.RATE_TABLE,
                ErrorKey.Category.POS_INDEX);
        }
        if (!Objects.equals(rateTableDTOS.get(1).getPosIndex(), secondEntity.getPosIndex())) {
            throw new ResourceConflictException(ErrorMessage.POS_INDEX_INCORRECT, Resources.RATE_TABLE,
                ErrorKey.Category.POS_INDEX);
        }

        Integer tempPosIndex = firstEntity.getPosIndex();
        firstEntity.setPosIndex(secondEntity.getPosIndex());
        secondEntity.setPosIndex(tempPosIndex);

        blockRateTableMapRepository.save(firstEntity);
        blockRateTableMapRepository.save(secondEntity);
    }

    private void checkNull(OCSCloneableMap entity) {
        if (entity == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY,
                ErrorKey.Category.ID);
        }
    }

    public String getNameOffer(Long id, Integer domainId){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" select OFFER_NAME ")
            .append(" from offer ")
            .append(" where OFFER_ID = :id ")
            .append(" and DOMAIN_ID = :domainId ");
        Query query = entityManager.createNativeQuery(stringBuilder.toString())
            .setParameter("id", id)
            .setParameter("domainId", domainId);
        return query.getSingleResult().toString();
    }

    public Long getOfferId(Long id, Integer domainId){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" select OFFER_ID ")
            .append(" from offer ")
            .append(" where OFFER_ID = :id ")
            .append(" and DOMAIN_ID = :domainId ");
        Query query = entityManager.createNativeQuery(stringBuilder.toString())
            .setParameter("id", id)
            .setParameter("domainId", domainId);
        Long offerId = ((Number) query.getSingleResult()).longValue();
        return offerId;
    }

    public List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        if(categoryType != null) {
            return blockRepository.findAllByBlockFilterIdInAndDomainId(ids, domainId);
        }
        return blockRepository.findByIdInAndDomainId(ids, domainId);
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Block.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.Block.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }
}
