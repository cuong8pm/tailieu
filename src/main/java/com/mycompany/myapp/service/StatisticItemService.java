package com.mycompany.myapp.service;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.mapper.StatisticItemMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import javax.annotation.PostConstruct;

import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.domain.StatisticItem;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.StatisticItemDTO;
import com.mycompany.myapp.repository.StatisticItemRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;

@Service
@Transactional
public class StatisticItemService extends OCSBaseService {

    @Autowired
    private StatisticItemRepository statisticItemRepository;
    @Autowired
    StatisticItemMapper statisticItemMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryService categoryService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = this.statisticItemRepository;
        this.parentService = this.categoryService;
    }

    public Page<StatisticItemDTO> getStatistics(Long categoryId, String name, String description, String id, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Page<StatisticItemDTO> statisticItemDTOS =  statisticItemRepository.findStatistics(categoryId, domainId, name, description, id, pageable).map(statisticItemMapper::toDto);
        statisticItemDTOS.forEach(s -> s.setCategoryLevel(parentCategoryLevel + 1));
        return statisticItemDTOS;
    }

    public StatisticItemDTO getStatisticItemDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        StatisticItem statistic = statisticItemRepository.findByIdAndDomainId(id, domainId);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(statistic.getParentId(), domainId);
        StatisticItemDTO statisticItemDTO = statisticItemMapper.toDto(statistic);
        if (category != null) {
            statisticItemDTO.setCategoryId(category.getId());
            statisticItemDTO.setCategoryName(category.getName());
            statisticItemDTO.setCategoryLevel(getCategoryLevel(category.getId()));
            statisticItemDTO.setCategoryLevel(getCategoryLevel(statistic.getCategoryId()) + 1);
        }
        return statisticItemDTO;
    }

    public StatisticItemDTO createStatistic(StatisticItemDTO statisticDTO) {
        Integer domainId = OCSUtils.getDomain();

        StatisticItem statisticItem = statisticItemMapper.toEntity(statisticDTO);
        checkCategoryExist(statisticDTO, domainId);
        checkDuplicateName(statisticDTO.getName(), statisticDTO.getId());

        statisticItem.setDomainId(domainId);
        super.setPosIndex(domainId, statisticItem, statisticDTO.getCategoryId());

        statisticItem = statisticItemRepository.save(statisticItem);

        return statisticItemMapper.toDto(statisticItem);
    }
    private void checkCategoryExist(StatisticItemDTO statisticItemDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(statisticItemDTO.getCategoryId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void deleteStatistic(Long id) {
        Integer domainId = OCSUtils.getDomain();
        //check statisticItem use by Formula
        Long staticUseByFormula = statisticItemRepository.countStaticUseByFormula(id);
        if (staticUseByFormula > 0) {
            throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.STATISTIC, "statisticId");
        }
        statisticItemRepository.deleteByIdAndDomainId(id, domainId);
    }

    public StatisticItemDTO updateStatistic(StatisticItemDTO statisticItemDTO) {
        Integer domainId = OCSUtils.getDomain();
        StatisticItem statisticItemDB = statisticItemRepository.findByIdAndDomainIdForUpdate(statisticItemDTO.getId(), domainId);

        if (statisticItemDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.StatisticItem.NOT_FOUND, Resources.STATISTIC, ErrorKey.Statistic.ID);
        }

        checkCategoryExist(statisticItemDTO, domainId);
        checkDuplicateName(statisticItemDTO.getName(), statisticItemDTO.getId());

        StatisticItem statisticItem = statisticItemMapper.toEntity(statisticItemDTO);
        statisticItem.setDomainId(domainId);

        if (!Objects.equal(statisticItemDTO.getCategoryId(), statisticItemDB.getCategoryId())) {
            setPosIndex(domainId, statisticItem, statisticItemDTO.getCategoryId());
        }

        statisticItem = statisticItemRepository.save(statisticItem);

        return statisticItemMapper.toDto(statisticItem);
    }
    
    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.STATISTIC;
    }
}
