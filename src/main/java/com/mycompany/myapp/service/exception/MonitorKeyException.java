package com.mycompany.myapp.service.exception;

public class MonitorKeyException extends OCSException{
    public MonitorKeyException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }
}
