package com.mycompany.myapp.service.exception;

public class MustNotBeEmptyException extends OCSException {

    public MustNotBeEmptyException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
