package com.mycompany.myapp.service.exception;

public class DataInvalidException extends OCSException {

    public DataInvalidException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
