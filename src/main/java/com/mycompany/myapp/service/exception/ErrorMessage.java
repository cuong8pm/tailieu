package com.mycompany.myapp.service.exception;

public final class ErrorMessage {

    private ErrorMessage() {}

    public static final String NOT_FOUND = "not_found";
    public static final String CATEGORY_INCORRECT = "category_incorrect";
    public static final String DATA_HAS_BEEN_UPDATED = "data_has_been_updated";
    public static final String DUPLICATED_NAME = "dupplicated_name";
    public static final String DUPLICATED_ITEM = "duplicated_item";
    public static final String DUPLICATED_CELL_ID = "dupplicated_cell_id";
    public static final String INVALID_FILE = "invalid_file";
    public static final String INVALID_CELL_ID = "cell_id_already_exists";
    public static final String NOT_CLONEABLE = "not_cloneable";
    public static final String CANNOT_DELETE_HAS_CHILD = "the_record_could_not_be_deleted_because_of_has_child";
    public static final String CANNOT_DELETE_ASSOCIATION = "the_record_could_not_be_deleted_because_of_an_association";
    public static final String INPUT_NOT_MATCH = "input_data_do_not_match";
    public static final String OBJECT_BEING_USED_IN_ANOTHER_OBJECT = "WARNING_your_object_being_used_in_another_object";

    public static final String CANT_UPDATE_CATEGORY = "cant_update_category";
    public static final String CANNOT_CLONE = "cannot_clone";
    public static final String NAME_TO_LENGTH = "length_must_be_between_0_and_255";
    public static final String POS_INDEX_INCORRECT = "pos_index_incorrect";
    public static final String MUST_HAVE_ATLEAST_ONE_CHILD = "must_have_at_least_one_child";
    public static final String CATEGORY_TYPE = "wrong_category_type";
    public static final String QUANTITY_INVALID = "quantity must be 2";
    public static final String CLONE_NAME_TOO_LONG = "clone_name_too_long";
    public static final String CLONE_DESCRIPTION_TOO_LONG = "clone_description_too_long";
    public static final String AFFECTED_OBJECT_NOT_FOUND = "affected_object_not_found";

    public static class Category {

        private Category() {}

        public static final String CANT_DELETE_ROOT = "cant_delete_root";
        public static final String NOT_FOUND = "category_not_found";
        public static final String ROOT_NOT_FOUND = "root_category_not_found";
        public static final String CANT_UPDATE_PARENT = "cant_update_parent";
        public static final String CANT_UPDATE_PARENT_FOR_ROOT = "cant_update_parent_for_root";
        public static final String CHILDREN_IN_CATEGORY = "children_in_category";
        public static final String CANT_UPDATE_ROOT = "cant_update_root";
        public static final String DUPLICATED_NAME = "duplicate_category_name";
        public static final String INFINITE_LOOP_CATEGORY = "infinite_loop_category";
    }

    public static class UnitType {

        private UnitType() {}

        public static final String NOT_FOUND = "unit_type_not_found";
        public static final String DUPLICATED_NAME = "duplicate_unit_type_name";
        public static final String PRECISION_MIN = "precision_greater_than_0";
    }

    public static class Parameter {

        private Parameter() {}

        public static final String NOT_FOUND = "parameter_not_found";
        public static final String OWNER_LEVEL_NOT_FOUND = "parameter_owner_level_not_found";
        public static final String DUPLICATED_NAME = "duplicate_parameter_name";
        public static final String CANNOT_DELETE = "WARNING_your_object_being_used_in_another_object";
    }

    public static class ReserveInfo {

        private ReserveInfo() {}

        public static final String MIN_LESS_THRES_AND_MAX = "min_must_be_less_thres_and_max";
        public static final String MIN_LESS_THRES = "min_must_be_less_thres";
        public static final String MAX_GTE_THRES = "max_must_be_gte_thres";
        public static final String MAX_GTE_THRES_AND_MIN = "max_must_be_gte_thres_and_min";
        public static final String THRES_GTE_MIN = "thres_must_be_gte_than_min";
        public static final String MAX_GTE_MIN = "max_must_be_gte_than_min";
        public static final String THRES_IN_MAX_MIN = "thres_in_max_min";
        public static final String NOT_FOUND = "reserve_info_not_found";
        public static final String DUPLICATED_NAME = "duplicate_reserve_info_name";
    }

    public static class ZoneMap {

        private ZoneMap() {}

        public static final String NOT_FOUND = "zone_map_not_found";
        public static final String DUPLICATED_NAME = "duplicate_zone_map_name";
    }

    public class Zone {

        private Zone() {}

        public static final String NOT_FOUND = "zone_not_found";
        public static final String DUPLICATED_NAME = "duplicate_zone_name";
    }

    public class ZoneData {

        private ZoneData() {}

        public static final String NOT_FOUND = "zone_data_not_found";
        public static final String DUPLICATED_VALUE = "duplicate_zone_data_value";
    }

    public static class GeoHomeZone {

        private GeoHomeZone() {}

        public static final String NOT_FOUND = "geo_home_zone_not_found";
        public static final String DUPLICATED_NAME = "duplicate_geo_home_zone_name";
        public static final String DUPLICATED_CODE = "duplicate_geo_home_zone_code";
    }

    public static class GeoNetZone {

        private GeoNetZone() {}

        public static final String NOT_FOUND = "geo_home_zone_not_found";
        public static final String DUPLICATED_CELL_ID = "duplicate_geo_net_zone_cell_id";
    }

    public static class BalType {

        private BalType() {}

        public static final String NOT_FOUND = "bal_type_not_found";
        public static final String FROM_NOT_FOUND = "from_balance_not_found";
        public static final String TO_NOT_FOUND = "to_balance_not_found";
        public static final String BALANCE_DUPLICATED_NAME = "balances_name_already_exists_please_try_again";
        public static final String ACCUMULATE_DUPLICATED_NAME = "accumulate_balances_name_already_exists_please_try_again";
        public static final String EMPTY_FIELD = "field_is_required";
        public static final String LESS_THAN_OR_EQUAL = "the_expire_date_must_be_greater_than_or_equal_to_the_start_date";
        public static final String INVALID = "not_valid";
        public static final String EFF_DATE_TYPE_ID_INVALID = "eff_date_type_id_not_valid";
        public static final String EXP_DATE_TYPE_ID_INVALID = "exp_date_type_id_not_valid";
        public static final String EFFECT_DATE_TYPE_NOT_FOUND = "eff_date_type_id_not_found";
        public static final String EXP_DATE_TYPE_ID_NOT_FOUND = "exp_date_type_id_not_found";
        public static final String CANNOT_DELETE = "WARNING_your_object_being_used_in_another_object";
    }

    public static class Normalizer {

        private Normalizer() {}

        public static final String DUPLICATED_NAME = "normalizer_duplicated_name";
        public static final String NORMALIZER_SPECIAL_FIELDS_NOT_VALID = "normalizer_special_fields_not_valid";
        public static final String NORMALIZER_TYPE_NOT_FOUND = "normalizer_type_not_found";
        public static final String NOT_FOUND = "normalizer_not_found";
        public static final String INVALID_INPUT_FIELD = "invalid_input_field";
        public static final String NORMALIZER_VALUE_NOT_FOUND = "normalizer_list_of_value_not_found";
        public static final String NESTEDE_OBJECT_WRONG_ORDER = "nestede_object_wrong_order";
        public static final String VALUE_NOT_EXIST = "value_not_exist";
        public static final String FILTER_VALUE_NOT_EXIST = "filter_value_not_exist";
        public static final String DUPLICATE_FILTER_NAME = "duplicate_filter_name";
        public static final String CANNOT_DELETE = "WARNING_your_object_being_used_in_another_object";
        public static final String END_CHARACTER_GREATER_THAN_START_CHARACTER =
            "end_character_must_be_greater_than_or_equal_to_start_character";
    }

    public static class NormalizerValue {

        private NormalizerValue() {}

        public static final String DUPLICATED_VALUE_NAME = "value_name_is_duplicate_please_try_again";
        public static final String NORMALIZER_SPECIAL_FIELDS_NOT_VALID = "normalizer_special_fields_not_valid";
        public static final String NORMALIZER_TYPE_NOT_FOUND = "normalizer_type_not_found";
        public static final String INVALID_INPUT_FIELD = "invalid_input_field";
        public static final String NORMALIZER_VALUE_NOT_FOUND = "normalizer_list_of_value_not_found";
        public static final String NESTEDE_OBJECT_WRONG_ORDER = "nestede_object_wrong_order";
        public static final String VALUE_NOT_EXIST = "value_not_exist";
        public static final String VALUE_MUST_HAVE_VALUE = "normalizer_value_must_have_value";
        public static final String VALUE_MUST_SET_FOR_PARAM = "normalizer_value_must_set_for_norm_param";
        public static final String VALUE_MUST_HAVE_2_VALUE = "normalizer_value_must_have_two_value";
        public static final String VALUE_WITH_BAD_FORMAT = "normalizer_value_has_been_wrong_format";
        public static final String VALUE_MUST_DEFAULT_VALUE = "normalizer_value_must_have_only_one_default_value";
        public static final String CANNOT_DELETE = "WARNING_your_object_being_used_in_another_object";
        public static final String NOT_FOUND = "value_not_found";
    }

    public static class NormalizerParam {

        private NormalizerParam() {}

        public static final String PARAMETERS_MUST_BE_NOT_NULL = "list_of_parameters_must_be_not_null";
        public static final String IS_CHAR_MUST_BE_NOT_NULL = "is_characteristic_must_be_not_null";
        public static final String PARAMETER_VALUE_INVALID = "parameter_value_invalid";
        public static final String END_VALUE_MUST_BE_GREATER_THAN_0 = "end_value_must_be_greater_than_to_0";
        public static final String START_VALUE_MUST_BE_GREATER_THAN_0 = "start_value_must_be_greater_than_to_0";
        public static final String MUST_BE_GREATER_THAN_0 = "must_be_greater_than_to_0";
        public static final String PARAMETER_VALUE_TOO_BIG = "value_must_be_less_than_or_equal_to_2147483647";
        public static final String PARAMETER_LONG_VALUE_TOO_BIG = "value_must_be_less_than_or_equal_to_9223372036854775807";
        public static final String PARAMETER_FLOAT_VALUE_TOO_BIG = "value_must_be_less_than_or_equal_to_340282346638528860.10^27";
        public static final String PARAMETER_DOUBLE_VALUE_TOO_BIG = "field_is_over_double";

        public static final String PARAMETER_VALUE_NOT_A_DIGIT = "value_must_be_a_digit";
        public static final String PARAMETER_VALUE_HAS_SPECIAL_CHARACTERS =
            "field_parameter_value_can't_contain_any_of_the_special_characters";
        public static final String PRIORITY_MUST_BE_NOT_NULL = "priority_must_be_not_null";
        public static final String COMPARE_TYPE_MUST_BE_NOT_NULL = "compare_type_must_be_not_null";
        public static final String START_VALUE_AND_END_VALUE = "field_start_value_must_be_less_than_to_field_end_value";
        public static final String TIME_PARAMS_REQUIRED_ALL_DATE_OF_WEEK_AT_START_OF_DAY =
            "time_params_required_all_date_of_week_at_00_00_00";
        public static final String TIME_PARAMS_ARE_DUPLICATE = "time_params_are_duplicate";
        public static final String END_VALUE_GREAT_THAN_START_VALUE = "end_value_great_than_start_value";
        public static final String CAN_NOT_SAME_CURRENT_TIME =
            "can_not_choose_same_current_time_for_start_and_end_type_please_choose_another_type";
        public static final String NOT_FOUND = "parameter_not_found";
        public static final String MUST_BE_NONE = "must_be_none";
    }

    public static class Threshold {

        private Threshold() {}

        public static final String NOT_FOUND = "threshold_not_found";
        public static final String DUPLICATED_NAME = "threshold_name_already_exists,_please_try_again";
        public static final String DUPLICATED_EXTERNAL_ID = "external_id_already_exists,_please_try_again";
        public static final String EMPTY_FIELD = "field_must_not_be_null";
        public static final String LESS_THAN_OR_EQUAL = "start_date_must_be_less_than_or_equal_to_end_data";
        public static final String INVALID = "not_valid";
        public static final String MUST_BE_NULL = "is_must_be_null";
    }

    public static class BillingCycle {

        private BillingCycle() {}

        public static final String BILLING_CYCLE_IN_USE = "billing_cycle_in_use";
        public static final String BILLING_CYCLE_TYPE_ID = "billingCycleTypeId_not_found";
        public static final String BEGIN_DATE_END_DATE = "begin_date_end_date";
        public static final String INVALID_DAY_OF_WEEK = "invalid_day_of_week";
        public static final String INVALID_DAY_OF_MONTH = "invalid_day_of_month";
        public static final String INVALID_HOUR_OF_DAY = "invalid_hour_of_day";
        public static final String DELETE_OLDEST_BILLING_CYCLE = "delete_oldest_billing_cycle";
        public static final String INVALID_BEGIN_DATE = "invalid_begin_date";
        public static final String INVALID_UNIT_TYPE = "invalid_unit_type";
        public static final String NOT_ENOUGHT_FOR_CYCLE = "not_enought_for_cycle";
        public static final String MAX_DATE = "must_be_less_than_or_equal_to_31/12/2050";
        public static final String TYPE_DUPLICATED_NAME = "billing_cycle_type_duplicated_name";
    }
    public static class BillingCycleType{
        private BillingCycleType(){}
        public static final String NOT_FOUND = "billing_cycle_type_not_found";
    }
    public static class StatisticItem {

        private StatisticItem() {}

        public static final String NOT_FOUND = "statistic_item_not_found";
        public static final String DUPLICATED_NAME = "duplicate_statistic_name";
    }

    public static class BalanceAcm {

        private BalanceAcm() {}

        public static final String NOT_FOUND = "balance_acm_mapping_not_found";
        public static final String DUPLICATED_NAME = "balance_acm_mapping_name_already_exist_please_try_again";
        public static final String BALANCE_OR_ACM_BALANCE_ALREADY_MAPPING = "from_balance_and_to_balance_already_mapping";
        public static final String A_PAIR_MUST_DIFFERENT = "id_object_already_exists_please_try_again";
    }

    public static class AccountBalance {

        private AccountBalance() {}

        public static final String NOT_FOUND = "account_balance_mapping_not_found";
        public static final String DUPLICATED_NAME = "account_balance_mapping_name_already_exist_please_try_again";
        public static final String FROM_BALANCE_MAPPING_TYPE_ALREADY_MAPPING = "from_balance_and_mapping_type_already_mapping";
        public static final String A_PAIR_MUST_DIFFERENT = "id_object_already_exists_please_try_again";
    }

    public static class PccRule {

        private PccRule() {}

        public static final String NOT_FOUND = "pcc_rule_not_found";
        public static final String DUPLICATED_NAME = "duplicate_pcc_rule_name";
        public static final String EXPIRE_DATE_AFTER_EFFECT_DATE = "expire_date_must_be_after_effect_date";
        public static final String EFFECT_DATE_LESS_THAN_EXPIRE_DATE = "effect_date_must_be_less_than_expire_date";
    }

    public static class StateSet {

        private StateSet() {}

        public static final String STATE_GROUP_BEING_USED = "state_groups_being_used_in_state_type";
        public static final String STATE_GROUP_NOT_FOUND = "state_group_not_found";
        public static final String CATEGORY_GROUP_TYPE = "category_must_be_state_group";
        public static final String STATE_GROUP_DUPLICATED_NAME = "duplicate_state_group_name";

        public static final String STATE_TYPE_BEING_USED = "state_type_being_used";
        public static final String STATE_TYPE_NOT_FOUND = "state_type_not_found";
        public static final String CATEGORY_STATE_TYPE = "category_must_be_state_group";
        public static final String STATE_TYPE_DUPLICATED_NAME = "duplicate_state_type_name";
    }

    public static class ReferTable {

        private ReferTable() {}

        public static final String NOT_FOUND = "refer_table_not_found";
        public static final String REFER_STATE_TYPE = "refer_type_must_be_state_type";
        public static final String WRONG_FORMAT = "wrong_format";
        public static final String CHARSPEC_TYPE_NOT_FOUND = "charpec_type_not_found";
    }

    public static class Block {

        private Block() {}

        public static final String AFFECTED_OBJECT_TYPE_NOT_FOUND = "affected_object_type_not_found";
        public static final String CATEGORY_BLOCK = "category_must_be_block";
        public static final String INVALID_RATE_TABLE = "must_have_at_least_one_rate_table_of_basic_tab";
        public static final String NOT_FOUND = "block_not_found";
        public static final String BLOCK_COMPONENT_TYPE_NOT_FOUND = "block_component_type_not_found";
        public static final String DUPLICATED_NAME = "block_duplicated_name";
        public static final String DATA_HAS_BEEN_MODIFIED = "data_has_been_modified";
    }

    public static class MonitorKey {

        private MonitorKey() {}

        public static final String INVALID_MONITOR_KEY = "invalid_monitor_key";
        public static final String CANNOT_UPDATE_ASSOCIATION = "the_record_could_not_be_updated_because_of_an_association";
        public static final String NOT_FOUND = "monitor_key_not_found";
    }

    public static class Qos {

        private Qos() {}

        public static final String DUPLICATED_NAME = "duplicate_qos_name";
        public static final String NOT_FOUND = "qos_not_found";
    }

    public static class PepProfile {

        private PepProfile() {}

        public static final String DUPLICATED_NAME = "duplicate_pep_profile_name";
        public static final String NOT_FOUND = "pep_profile_not_found";
        public static final String CATEGORY_TYPE = "category_must_be_pep_profile";
        public static final String PEP_PROFILE_BEING_USED = "pep_profile_being_use";
        public static final String DUPLICATED_DATA = "duplicate_data";
        public static final String CANNOT_DELETE = "please_delete_all_pcc_rule_of_pep_profile";
    }

    public static class Characteristic {

        private Characteristic() {}

        public static final String NOT_FOUND = "characteristic_not_found";

        public static final String CYCLE_INHERIT =
            "the_relationship_of_objects_in_the_database_is_infinitely_recursive_review_configuration";
    }

    public static class RatingFilter {

        public RatingFilter() {}

        public static final String RATING_FILTER_NOT_FOUND = "rating_filter_not_found";
        public static final String CATEGORY_RATING_FILTER = "category_must_be_rating_filter";
        public static final String DUPLICATED_NAME = "duplicate_rating_filter_name";
        public static final String RATE_TABLE_NOT_FOUND = "rating_filter_must_have_rate_table";
        public static final String CANNOT_DELETE = "WARNING_your_object_being_used_in_another_object";
        public static final String NOT_FOUND = "rating_filter_not_found";
    }

    public static class RatingFilterRateTableMap {

        public RatingFilterRateTableMap() {}

        public static final String RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND = "rating_filter_rate_table_not_found";
    }

    public static class RateTable {

        private RateTable() {}

        public static final String DUPLICATED_NAME = "rate_table_duplicated_name";
        public static final String COLUMNS_NORMALIZER = "column_normalizer_id";
        public static final String DUPLICATE_NORMALIZER = "duplicate_normalizer_id";
        public static final String DEFAULT_ROW_INVALID = "default_row_invalid";
        public static final String INVALID_ROW = "invalid_row";
        public static final String INVALID_VALUE = "invalid_value";
        public static final String DEFAULT_FORMULA_IS_NULL = "default_formula_is_null";
        public static final String DUPLICATE_COLUMN_INDEX = "duplicate_column_index";
        public static final String INVALID_COLUMN_INDEX = "invalid_column_index";
        public static final String DUPLICATE_COMBINED_VALUE = "duplicate_combined_value";
        public static final String INVALID_COMBINED_VALUE = "please_select_values";
        public static final String NOT_FOUND = "rate_table_not_found";
        public static final String MUST_NOT_BE_NULL = "must_not_be_null";
        public static final String DUPLICATED_DISPLAY_NAME = "rate_table_column_duplicated_display_name";
    }

    public static class Formula {

        private Formula() {}

        public static final String OVER_FLOW = "over_flow";
        public static final String CHAR_NOT_FOUND = "characteristic_not_found";
        public static final String INVALID_VALUE = "invalid_value";
        public static final String MUST_BE_NULL = "must_be_null";
        public static final String MUST_NOT_BE_NULL = "must_not_be_null";
        public static final String MUST_GREATER_THAN = "must_greater_than_";
        public static final String B_NOT_FOUND = "B_not_found";
        public static final String A_NOT_FOUND = "A_not_found";
        public static final String FORMULA_ERROR_CODE_NOT_FOUND = "formula_error_code_not_found";
        public static final String FORMULA_UNIT_CONVERT_NOT_FOUND = "formula_unit_convert_not_found";
        public static final String STATISTIC_ITEM_NOT_FOUND = "statistic_item_not_found";
    }

    public static class FormulaErrorCode {

        private FormulaErrorCode() {}

        public static final String DUPLICATED_NAME = "formula_error_code_duplicated_name";
        public static final String DUPLICATED_ERROR_CODE = "formula_error_code_duplicated_error_code";
    }

    public static class PriceComponent {

        private PriceComponent() {}

        public static final String NOT_FOUND = "price_component_not_found";
        public static final String DUPLICATED_NAME = "price_component_duplicated_name";
        public static final String CATEGORY_TYPE = "category_must_be_price_component";
        public static final String DUPLICATED_BLOCK = "price_component_duplicated_Block";
        public static final String DUPLICATED_BLOCK_MAP_INDEX = "price_component_block_map_duplicated_pos_index";
        public static final String NOT_EMPTY = "must_not_be_empty_block_map_items";
        public static final String GENNERATING_TYPE =
            "at_least_one_Block_must_satisfy_the_following_conditions_block_for_generating_is_not_selected_or_generating_type_NONE";
    }

    public static class ActionType {

        private ActionType() {}

        public static final String ID_NOT_FOUND = "action_type_not_found";

        public static final String DYNAMIC_RESERVE_FILTER_ID_NOT_FOUND = "dynamic_reserve_filter_id_not_found";
        public static final String SORTING_PC_FILTER_ID_NOT_FOUND = "sorting_pc_filter_id_not_found";
        public static final String PRIORITY_FILTER_ID_NOT_FOUND = "priority_filter_id_not_found";

        public static final String CATEGORY_TYPE = "category_must_be_action_type";
        public static final String DUPLICATED_NAME = "action_type_duplicated_name";
        public static final String DUPLICATED_ID = "duplicated_id";
        public static final String NOT_FOUND = "action_type_not_found";
    }

    public static class Event {

        private Event() {}

        public static final String DUPLICATED_NAME = "event_duplicated_name";
        public static final String NOT_EMPTY = "Please_add_at_least_one_Action_Type";
        public static final String DUPLICATED_ACTIONTYPEID = "event_duplicated_actiontype";
        public static final String DUPLICATED_ID = "event_duplicated_id";
        public static final String EVEN_ANALYSIS_MUST_BE_NULL = "field_event_analysis_must_be_deleted";
        public static final String EVEN_ANALYSIS_MUST_BE_NOT_NULL = "field_event_analysis_is_required";
        public static final String NOT_FOUND = "event_not_found";
    }

    public static class Action {

        private Action() {}

        public static final String DUPLICATED_NAME = "action_duplicated_name";
        public static final String CATEGORY_TYPE = "category_must_be_action";
        public static final String NOT_EMPTY = "must_not_be_empty";
        public static final String NOT_FOUND = "action_not_found";
        public static final String CANNOT_DELETE = "WARNING_your_object_being_used_in_another_object";
        public static final String TIME_VALIDATE = "exp_date_must_be_greater_than_eff_date";
        public static final String MUST_HAVE_ATLEAST_ONE_CHILD = "must_have_at_least_one_child_action";
    }

    public static class ActionPriceComponentMap {

        private ActionPriceComponentMap() {}

        public static final String NOT_FOUND = "action_price_component_not_found";
    }

    public static class Offer {

        private Offer() {}

        public static final String NOT_FOUND = "offer_not_found";
        public static final String EFFECT_DATE_LESS_THAN_EXPIRE_DATE = "effect_date_must_be_less_than_expire_date";
        public static final String DUPLICATED_NAME = "duplicated_offer_name";
        public static final String DUPLICATED_EXTERNAL_ID = "duplicated_external_id";
        public static final String DUPLICATED_OFFER = "duplicated_offer";
        public static final String CANNOT_ADD = "can_not_add_parent_offer";
        public static final String DUPLICATED_RELATIONSHIP = "dupplicated_relationship";
        public static final String MUST_NOT_BE_NULL = "must_not_be_null";
    }

    public static class OfferVersion {

        private OfferVersion() {}

        public static final String STATE_MUST_BE_IN_ACTIVE = "stast_must_be_in_active";
        public static final String DUPLICATED_CHARSPEC = "duplicated_charspec";
        public static final String CANNOT_DELETE = "offer_version_cannot_delete_because_state_is_active";
        public static final String DUPLICATED_CHILD_ROW_ID = "dupplicated_child_row";
        public static final String DUPLICATED_PARENT_ROW_ID = "dupplicated_parent_row";
        public static final String MUST_BE_NULL = "must_be_null";
        public static final String MUST_NOT_BE_NULL = "must_not_be_null";
    }

    public static class OfferVersionBundle {

        private OfferVersionBundle() {}

        public static final String DUPLICATED_CHILDID = "duplicated_child_id";
    }

    public static class OfferTemplateVersionBundle {

        private OfferTemplateVersionBundle() {}

        public static final String DUPLICATED_CHILDID = "duplicated_child_id";
    }

    public static class OfferVersionAction {

        private OfferVersionAction() {}

        public static final String DATA_CONTRAIN = "action_already_exists";
        public static final String NOT_FOUND = "offer_version_actions_not_found";
        public static final String PARENT_INCORRECT = "parent_incorrect";
    }

    public static class OfferTemplate {

        private OfferTemplate() {}

        public static final String NOT_FOUND = "offer_template_not_found";
        public static final String DUPLICATED_NAME = "duplicated_offer_template_name";
    }

    public static class OfferTemplateVersion {

        private OfferTemplateVersion() {}

        public static final String NOT_FOUND = "offer_template_version_not_found";
    }

    public static class BuilderType {

        private BuilderType() {}
        public static final String ITEM_NOT_FOUND = "item_not_found";
        public static final String DUPLICATED_NAME = "builder_type_duplicated_name";
        public static final String NOT_FOUND = "builder_type_not_found";

    }

    public static class BuilderTypeItem {

        private BuilderTypeItem() {}

        public static final String DUPLICATED_NAME = "builder_type_item_duplicated_name";
    }

    public static class Builder {

        private Builder() {}

        public static final String DUPLICATED_NAME = "builder_duplicated_name";
        public static final String NOT_FOUND = "builder_not_found";
        public static final String NOTIFY_NOT_FOUND= "notify_not_found";
        public static final String BUILDER_ITEM_NOT_FOUND= "builder_item_not_found";

    }

    public static class BuilderItem {

        private BuilderItem() {}

        public static final String  NOT_FOUND = "builder_item_not_found";
        public static final String VALUE = "value_not_found";
    }

    public static class Notify {

        private Notify() {}

        public static final String DUPLICATED_NAME = "notify_duplicated_name";
        public static final String DUPLICATED_LANGUAGE = "notify_duplicated_language";

    }

    public static class Router {

        private Router() {}
        public static final String NOT_FOUND = "router_not_found";
        public static final String DUPLICATED_NAME = "router_duplicated_name";
    }

    public static class Function {

        private Function() {}
        public static final String NOT_FOUND = "function_alias_not_found";
        public static final String DUPLICATED_ALIAS = "duplicated_function_alias";
        public static final String CANNOT_DELETE = "function_is_used_by_other_objects";
        public static final String CANNOT_DELETE_OBJECT = "object_is_used_by_other_objects";
        public static final String FUNCTION_INCORRECT = "function_incorrect";
        public static final String CANNOT_UPDATE_FUNCTION = "cannot_update_data_type_and_level_because_function_has_instance";
        public static final String CANNOT_UPDATE_PARAM = "cannot_update_data_type_because_function_parameter_has_instance";
        public static final String PARAM_DUPLICATED_NAME = "parameter_name_duplicated_name";
        public static final String DATA_HAS_BEEN_CHANGED = "data_has_been_changed";
    }
    public static class FunctionParam {

        private FunctionParam() {}
        public static final String NOT_FOUND = "function_param_not_found";
        public static final String DATA_HAS_BEEN_CHANGED = "data_has_been_changed";
    }
    public static class Verification {


        private Verification() {}

        public static final String DUPLICATED_NAME = "verification_duplicated_name";
        public static final String NOT_FOUND = "verification_not_found";
        public static final String ITEM_NOT_FOUND = "verification_item_not_found";
    }

    public static class Condition {

        private Condition() {}

        public static final String DUPLICATED_NAME = "condition_duplicated_name";
        public static final String NOT_FOUND = "condition_not_found";
    }

    public static class Expression {

        private Expression() {}

        public static final String NOT_FOUND = "expression_not_found";
    }

    public static class Filter {
        private Filter(){}
        public static final String NOT_FOUND = "filter_not_found";
        public static final String MUST_HAVE_ATLEAST_ONE_CHILD = "must_have_at_least_one_child_expression";
        public static final String VALUE_INVALID_FORMAT = "value_invalid_format";
        public static final String DUPLICATED_NAME = "filter_duplicated_name";
        public static final String FILTER_FIELD_NOT_FOUND = "filter_field_not_found";
        public static final String EXPRESSION_NOT_FOUND = "expression_not_found";
    }

    public static class EventPolicy {

        private EventPolicy() {}

        public static final String NOT_FOUND = "event_policy_not_found";
        public static final String DUPLICATED_NAME = "event_policy_duplicated_name";
    }
}
