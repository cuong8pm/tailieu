package com.mycompany.myapp.service.exception;

import java.util.concurrent.locks.Condition;

public final class ErrorKey {

    private ErrorKey() {}

    public static final String ID = "id";
    public static final String POS_INDEX = "posIndex";
    public static final String ENTITY = "entity_not_found";
    public static final String CATEGORY_ID = "categoryId";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String ENTITY_NOT_FOUND = "entity_not_found";
    public static final String CATEGORY_NAME = "categoryName";

    public static class Category {

        private Category() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String PARENT_CATEGORY = "parentCategory";
        public static final String PARENT_CATEGORY_NAME = "parentCategoryName";
        public static final String TEMPLATES = "templates";
        public static final String CATEGORY_TYPE = "categoryType";
        public static final String POS_INDEX = "posIndex";
    }

    public static class UnitType {

        private UnitType() {}

        public static final String NAME = "name";
        public static final String ID = "id";
        public static final String UNIT_TYPE_ID = "unitTypeId";
        public static final String UNIT_ORDER = "unitOrder";
        public static final String PRECISION = "precision";
    }

    public static class ReserveInfo {

        private ReserveInfo() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String MAX_RESERVE_VALUE = "maxReserveValue";
        public static final String MIN_RESERVE_VALUE = "minReserveValue";
        public static final String USAGE_QUOTA_THRESHOLD = "usageQuotaThreshold";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Parameter {

        private Parameter() {}

        public static final String NAME = "name";
        public static final String ID = "id";
        public static final String OWNER_LEVEL = "ownerLevel";
        public static final String POS_INDEX = "posIndex";
    }

    public static class GeoHomeZone {

        private GeoHomeZone() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String GEO_CODE = "geoHomeZoneCode";
    }

    public class ZoneMap {

        private ZoneMap() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
    }

    public class Zone {

        private Zone() {}

        public static final String ID = "id";
        public static final String NAME = "name";
    }

    public class ZoneData {

        private ZoneData() {}

        public static final String ID = "id";
        public static final String VALUE = "zoneDataValue";
    }

    public static class GeoNetZone {

        private GeoNetZone() {}

        public static final String CELL_ID = "cellId";
        public static final String ID = "id";
        public static final String NAME = "name";
    }

    public static class BalType {

        private BalType() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Threshold {

        private Threshold() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String EXTERNAL_ID = "externalId";
    }

    public static class BillingCycle {

        private BillingCycle() {}

        public static final String Billing_Cycle_Type_Id = "billingCycleTypeId";
        public static final String BEGIN_DATE = "beginDate";
        public static final String END_DATE = "endDate";
        public static final String TIME_VALUE = "timeValue";
        public static final String UNIT_TYPE = "unitType";
        public static final String UNIT_ID = "unitId";
    }

    public static class Service {

        private Service() {}

        public static final String ID = "id";
    }

    public static class Statistic {

        private Statistic() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
    }

    public static class MapAcmBalBal {

        private MapAcmBalBal() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String FROM_BALANCE_ID = "fromBalanceId";
        public static final String TO_BALANCE_ID = "toBalanceId";
    }

    public static class MapSharebalBal {

        private MapSharebalBal() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String FROM_BALANCE_ID = "fromBalanceId";
        public static final String TO_BALANCE_ID = "toBalanceId";
        public static final String SHARE_TYPE = "mappingTypeId";
    }

    public static class StateSet {

        private StateSet() {}

        public static final String ID = "id";
        public static final String STATE_GROUP_ID = "stateGroupId";
        public static final String STATE_TYPE = "stateType";
        public static final String CATEGORY_ID = "categoryId";
        public static final String POS_INDEX = "posIndex";
        public static final String FILTER_VALUE = "filterValue";
        public static final String STATE_MASK = "stateMask";
    }

    public static class MonitorKey {

        private MonitorKey() {}

        public static final String MONITOR_KEY = "monitorKey";
        public static final String ID = "id";
    }

    public static class PccRule {

        private PccRule() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String EXPIRE_DATE = "expireDate";
        public static final String EFFECT_DATE = "effectDate";
    }

    public static class Qos {

        private Qos() {}

        public static final String ID = "id";
        public static final String NAME = "name";
    }

    public static class PepProfile {

        private PepProfile() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String QOS_ID = "qosId";
        public static final String PCC_RULE_ID = "pccRuleId";
    }

    public static class NormalizerParam {

        private NormalizerParam() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String IS_CHARACTERISTIC = "isCharacteristic";
        public static final String PARAMETER_VALUE = "parameterValue";
        public static final String PRIORITY = "priority";
        public static final String COMPARE_TYPE = "compareType";
        public static final String START_VALUE = "startValue";
        public static final String END_VALUE = "endValue";
    }

    public static class Characteristic {

        private Characteristic() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Normalizer {

        private Normalizer() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String INPUT_FIELDS = "inputField";
        public static final String VALUES = "listOfValues";
        public static final String SPECIAL_FIELDS = "specialFields";
        public static final String NORM_PARAMS = "norm_params";
    }

    public static final class RatingFilter {

        public RatingFilter() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String CATEGORY_ID = "categoryId";
        public static final String RATE_TABLE = "rate_table";
        public static final String TYPE = "rating_filter_type";
    }

    public static final class RatingFilterRateTableMap {

        public RatingFilterRateTableMap() {}

        public static final String ID = "id";
        public static final String NAME = "name";
    }

    public static class RateTable {

        private RateTable() {}

        public static final String ID = "id";
        public static final String COLUMNS = "columns";
        public static final String ROW = "rows";
        public static final String UNIT_TYPE = "unit_type";
        public static final String DEFAULT_FORMULA = "defaultFormula";
        public static final String FORMULA = "formulaId";
        public static final String COMBINED_VALUE = "combinedValue";
        public static final String DISPLAY_NAME = "displayName";
    }

    public static class Builder {

        private Builder() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String BUILDER_TYPE_ID = "builderTypeId";
        public static final String CONDITION_ID = "conditionId";
        public static final String CONDITION_NAME = "conditionName";
    }

    public static class BuilderItem {

        private BuilderItem() {}

        public static final String ID = "builderItemId";
        public static final String NAME = "name";
        public static final String VALUE = "value";
        public static final String VALUE_ID = "valueId";

    }

    public static class Notify {

        private Notify() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String LANGUAGEID = "languageId";

    }

    public static class Router {

        private Router() {}

        public static final String ID = "id";
        public static final String ROUTER_ID = "routerId";
        public static final String NAME = "name";
    }

    public static class Formula {

        private Formula() {}

        public static final String ID = "id";
        public static final String FORMULA_TYPE = "formula_type";
        public static final String INPUT_B = "input_B";
        public static final String INPUT_A = "input_A";
        public static final String A = "A";
        public static final String B = "B";
        public static final String PER = "per";
        public static final String CONVERT_TYPE = "convert_type";
        public static final String ERROR_CODE = "error_code";
        public static final String STATIC_ITEM = "static_item";
    }

    public static class NormSpecialField {

        private NormSpecialField() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String POS_INDEX = "posIndex";
        public static final String VALUES = "listOfValues";
        public static final String START_CHARACTER = "startCharacter";
        public static final String NORM_PARAMS = "norm_params";
    }

    public static class FormulaErrorCode {

        private FormulaErrorCode() {}

        public static final String TYPE = "type";
        public static final String ERROR_CODE = "errorCode";
    }

    public static class PriceComponent {

        private PriceComponent() {}

        public static final String BLOCK_ID = "blockId";
        public static final String INDEX = "posIndex";
        public static final String PRICE_COMPONENT_ID = "id";
        public static final String BLOCK_ITEM = "blockMapItems";
        public static final String GENNERATING_TYPE = "genneratingType";
    }

    public static class Block {

        private Block() {}

        public static final String RATE_TABLE = "RATE_TABLE";
        public static final String ID = "id";
        public static final String RATING_FILTER_NAME_NOT_FOUND = "blockFilterName";
        public static final String BLOCK_ENTITY = "entity_not_found";
        public static final String AFFECTED_OBJECT_NAME = "affectedObjectName";

    }

    public static class ActionType {

        private ActionType() {}

        public static final String ACTION_TYPE_ID = "id";
        public static final String ACTION_TYPE_NAME = "actionTypeName";
        public static final String DYNAMIC_RESERVE_FILTER_NAME = "dynamicReserveFilterName";
        public static final String SORTING_PC_FILTER_NAME = "sortingFilterName";
        public static final String PRIORITY_FILTER_NAME = "priorityFilterName";

    }

    public static class Action {

        private Action() {}

        public static final String ID = "id";
        public static final String PRICE_COMPONENT = "price_component";
        public static final String EXP_DATE = "expDate";
        public static final String RESERVE_INFO = "reserve_info";
        public static final String RESERVE_INFO_NAME = "reserveInfoName";
        public static final String RATING_FILTER = "rating_filter";
        public static final String EFF_DATE = "effDate";
    }

    public static class ActionPriceComponentMap {

        private ActionPriceComponentMap() {}

        public static final String ID = "id";
        public static final String PRICE_COMPONENT = "price_component";
    }

    public static class Offer {

        private Offer() {}

        public static final String ID = "id";
        public static final String EFF_DATE = "effDate";
        public static final String BILLING_CYCLE_TYPE = "billing_cycle_type";
        public static final String BILLING_CYCLE_TYPE_ID = "billingCycleTypeId";
        public static final String EXTERNAL_ID = "externalId";
        public static final String PARENT_ID = "parentId";
        public static final String CHILD_ID = "childId";
        public static final String NAME = "name";
        public static final String ENTITY_NOT_FOUND = "entity_not_found";

    }

    public static class OfferTemplate {

        private OfferTemplate() {}

        public static final String STATE = "state";
    }

    public static class Event {

        private Event() {}

        public static final String ID = "id";
        public static final String EVENT_ANA_LYSIS_FILTER = "eventAnalysisFilter";
        public static final String MAP_ITEM = "actionTypeMapDTOs";
        public static final String ACTION_TYPE_ID = "actionTypeId";
        public static final String RATING_FILTER_ID = "ratingFilterId";
        public static final String RATING_FILTER_NAME = "ratingFilterName";

    }

    public static class VersionRedirectionCommon {

        private VersionRedirectionCommon() {}

        public static final String VERSION_ID = "versionId";
    }

    public static class OfferVersion {

        private OfferVersion() {}

        public static final String ID = "id";
        public static final String STATE = "state";
        public static final String CHARSPEC = "charspecId";
    }

    public static class OfferVersionBundle {

        private OfferVersionBundle() {}

        public static final String OFFER = "childOfferId";
    }

    public static class OfferTemplateVersionBundle {

        private OfferTemplateVersionBundle() {}

        public static final String OFFER = "childOfferId";
    }

    public static class OfferVersionAction {

        private OfferVersionAction() {}

        public static final String ID = "id";
        public static final String ACTION = "actions";
        public static final String PARENT_ID = "parent_id";
    }

    public static class ReferTable {

        private ReferTable() {}

        public static final String ID = "id";
        public static final String VALUE = "value";
    }

    public static class BuilderType {

        private BuilderType() {}

        public static final String ID = "id";
        public static final String BUILDER_TYPE_ITEM_ID = "builderTypeItemId";
        public static final String NAME = "name";
    }

    public static class Verification {

        private Verification() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String ITEM_ID = "itemId";
    }

    public static class Condition {

        private Condition() {}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
    }

    public static class Function {

        private Function() {}
        public static final String ALIAS_ID = "entity_not_found";
        public static final String ALIAS = "alias";
        public static final String PARAM_NAME = "name";
    }

    public static class Filter {

        private Filter() {}

        public static final String filter_ID = "filterId";
        public static final String filter_NAME = "filterName";
        public static final String VALUE = "value";
        public static final String FIELD_ID = "fieldId";
    }

    public static class EventPolicy {

        private EventPolicy() {}

        public static final String ID = "eventPolicyId";
        public static final String FILTER_FIELD_HAS_BEEN_CHANGED = "filter_field_has_been_changed";
        public static final String EXPRESSION_HAS_BEEN_CHANGED = "expression_has_been_changed";
        public static final String BUILDER_HAS_BEEN_CHANGED = "builder_has_been_changed";
        public static final String VERIFICATION_HAS_BEEN_CHANGED = "verification_has_been_changed";
    }
}
