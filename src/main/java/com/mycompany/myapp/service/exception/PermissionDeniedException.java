package com.mycompany.myapp.service.exception;

public class PermissionDeniedException extends OCSException {

    public PermissionDeniedException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
