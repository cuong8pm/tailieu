package com.mycompany.myapp.service.exception;

public class FileException extends OCSException{

    public FileException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
