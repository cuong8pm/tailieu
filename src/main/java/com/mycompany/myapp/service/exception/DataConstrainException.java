package com.mycompany.myapp.service.exception;

public class DataConstrainException extends OCSException {

    public DataConstrainException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
