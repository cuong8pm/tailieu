package com.mycompany.myapp.service.exception;

public class ResourceNotFoundException extends OCSException {

    public ResourceNotFoundException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }
    
}
