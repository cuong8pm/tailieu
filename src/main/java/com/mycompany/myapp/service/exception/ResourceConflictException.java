package com.mycompany.myapp.service.exception;

public class ResourceConflictException extends OCSException {

    public ResourceConflictException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
