package com.mycompany.myapp.service.exception;

public class DuplicateException extends OCSException {

    public DuplicateException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

}
