package com.mycompany.myapp.service.exception;

import lombok.Data;
import lombok.Getter;

@Data
public class CloneNameTooLongException extends OCSException {

    private static final long serialVersionUID = 1L;
    private Long objectId;
    private String objectName;
    
    
    public CloneNameTooLongException(String title, String entityName, String errorKey) {
        super(title, entityName, errorKey);
    }

    public CloneNameTooLongException(String title, String entityName, String errorKey, Long objectId,
            String objectName) {
        super(title, entityName, errorKey);
        this.objectId = objectId;
        this.objectName = objectName;
    }
    
    
    
}
