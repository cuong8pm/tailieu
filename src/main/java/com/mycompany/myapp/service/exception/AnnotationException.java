package com.mycompany.myapp.service.exception;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * Exception sử dụng lúc validate theo annotation
 */

public class AnnotationException extends ConstraintViolationException {
    public final String objectName;

    public AnnotationException(Set<? extends ConstraintViolation<?>> constraintViolations, String objectName) {
        super(constraintViolations);
        this.objectName = objectName;
    }
}
