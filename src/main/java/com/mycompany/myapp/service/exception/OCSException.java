package com.mycompany.myapp.service.exception;

import org.zalando.problem.ThrowableProblem;

public class OCSException extends ThrowableProblem {

    private final String entityName;

    private final String errorKey;

    private final String title;

    public OCSException(String title, String entityName, String errorKey) {
        super();
        this.entityName = entityName;
        this.errorKey = errorKey;
        this.title = title;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public String getTitle() {
        return title;
    }
}
