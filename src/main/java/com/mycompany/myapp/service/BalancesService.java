package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.constant.AffectedObjectType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.DropdownType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.mapper.BalTypeMapper;
import com.mycompany.myapp.repository.*;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.util.BooleanUtils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
@Transactional
public class BalancesService extends OCSBaseService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BalTypeRepository balTypeRepository;

    @Autowired
    private BalTypeMapper balTypeMapper;

    @Autowired
    private UnitTypeRepository unitTypeRepository;

    @Autowired
    private ThresholdService thresholdService;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ThresholdBaltypeMapRepository thresholdBaltypeMapRepository;

    @Autowired
    private AccountBalanceMappingService accountBalanceMappingService;

    @Autowired
    private BalanceAcmMappingService balanceAcmMappingService;

    @Autowired
    private BlockRepository blockRepository;

    @Autowired
    private BillingCycleTypeRepository billingCycleTypeRepository;
    @Override
    @PostConstruct
    public void setBaseRepository() {
        baseRepository = balTypeRepository;
        ocsCloneableRepository = balTypeRepository;
        parentService = categoryService;
        relationRepository = thresholdBaltypeMapRepository;
        relationalChildService = thresholdService;
    }

    @Transactional(readOnly = true)
    public Page<BalancesDTO> getBalTypes(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        Page<BalancesDTO> balTypeDTOS = balTypeRepository.findBalTypes(categoryId, domainId, name, description, pageable).map(balTypeMapper::toDto);
        for (BalancesDTO balancesDTO : balTypeDTOS) {
            balancesDTO.setCategoryLevel(parentCategoryLevel);
            balancesDTO.setHasChild(checkHasChild(balancesDTO.getId(), domainId));
            if (BooleanUtils.isTrue(balancesDTO.isAcm())) {
                balancesDTO.setType(Resources.ACM_BAL_TYPE);
            }
        }
        return balTypeDTOS;
    }

    @Transactional(readOnly = true)
    public BalancesDTO getBalTypeDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        BalType balType = balTypeRepository.findByIdAndDomainId(id, domainId);
        if (balType == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalType.NOT_FOUND, Resources.BAL_TYPE, ErrorKey.BalType.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(balType.getCategoryId(), domainId);
        BalancesDTO balancesDTO = balTypeMapper.toDto(balType);
        balancesDTO.setCategoryName(category.getName());
        balancesDTO.setCategoryLevel(getCategoryLevel(category.getId()));

        UnitType unitType = unitTypeRepository.findByIdAndDomainId(balancesDTO.getUnitTypeId(), domainId);
        if (unitType != null) {
            balancesDTO.setBaseRate(unitType.getBaseRate());
        }
        if(BooleanUtils.isTrue(balancesDTO.isAcm())){
            balancesDTO.setType(Resources.ACM_BAL_TYPE);
        }

        return balancesDTO;
    }

    public BalancesDTO saveBalType(BalancesDTO balancesDTO) {
        Integer domainId = OCSUtils.getDomain();

        validateBalType(balancesDTO, domainId);
        BalType balType = balTypeMapper.toEntity(balancesDTO);
        balType.setDomainId(domainId);
        setPosIndex(domainId, balType);

        balType = balTypeRepository.save(balType);

        balancesDTO = balTypeMapper.toDto(balType);
        if(BooleanUtils.isTrue(balancesDTO.isAcm())){
            balancesDTO.setType(Resources.ACM_BAL_TYPE);
        }

        return balancesDTO;
    }

    public BalancesDTO updateBalType(BalancesDTO balancesDTO) {
        Integer domainId = OCSUtils.getDomain();
        BalType balType = balTypeRepository.findByIdAndDomainIdForUpdate(balancesDTO.getId(), domainId);
        if (balType == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalType.NOT_FOUND, Resources.BAL_TYPE, ErrorKey.BalType.ID);
        }

        // Lock old category
        validateBalType(balancesDTO, domainId);

        if (!balType.getUnitTypeId().equals(balancesDTO.getUnitTypeId())) {
            thresholdService.updateThresholdValue(balancesDTO.getId(), balancesDTO.getUnitTypeId(), balType.getUnitTypeId());
        }

        // Convert to Entity to save
        balType = balTypeMapper.toEntity(balancesDTO);
        balType.setDomainId(domainId);

        balType = balTypeRepository.save(balType);

        balancesDTO = balTypeMapper.toDto(balType);
        if(BooleanUtils.isTrue(balancesDTO.isAcm())){
            balancesDTO.setType(Resources.ACM_BAL_TYPE);
        }

        return balancesDTO;
    }

    public void deleteBalType(Long balanceId, Integer balanceType) {
        Integer domainId = OCSUtils.getDomain();
        checkBalTypeExist(balanceId, domainId);
        if (balTypeRepository.isUsingByBlockTable(balanceType, balanceId, domainId) > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BAL_TYPE, BalancesDTO.FieldNames.ID);
        }
        if (accountBalanceMappingService.existByBalanceId(balanceId, domainId)) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BAL_TYPE, BalancesDTO.FieldNames.ID);
        }
        if (balanceAcmMappingService.existByBalanceId(balanceId, domainId)) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BAL_TYPE, BalancesDTO.FieldNames.ID);
        }
        try {
            thresholdService.deleteThresholdByBalanceId(balanceId);
            balTypeRepository.deleteByIdAndDomainId(balanceId, domainId);
        } catch (Exception | Error e) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BAL_TYPE, BalancesDTO.FieldNames.ID);
        }
    }

    private void setPosIndex(Integer domainId, BalType balType) {
        Integer maxPos = balTypeRepository.getMaxPosIndex(domainId, balType.getCategoryId());
        if (maxPos == null) {
            balType.setPosIndex(0);
        } else {
            balType.setPosIndex(maxPos + 1);
        }
    }

    public Long getUnitTypeIdOfBalance(Long id) {
        Integer domainId = OCSUtils.getDomain();
        BalType balType = balTypeRepository.findByIdAndDomainId(id, domainId);
        if (balType == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalType.NOT_FOUND, Resources.BAL_TYPE, ErrorKey.BalType.ID);
        }
        return  balType.getUnitTypeId();
    }

    //hàm được gọi từ bên màn hình unitType
    //khi unitType phát sinh cập nhật thay đổi baseRate
    public void updateUnitType(Long unitTypeId, Integer newBaseRate, Integer oldBaseRate) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> balTypeIds = balTypeRepository.findIdByUnitTypeId(unitTypeId, domainId);
        if (!CollectionUtils.isEmpty(balTypeIds)) {
            thresholdService.updateThresholdValueByNewOldBaseRate(balTypeIds, newBaseRate, oldBaseRate);
        }
    }

    //get drop down effect_data_type
    public List<ReferTable> getDDByReferType(ReferType referType) {
        return referTableRepository.findByReferTypeOrderByValueAsc(referType);
    }

    private void validateBalType(BalancesDTO balancesDTO, Integer domainId) {
        checkCategoryExist(balancesDTO, domainId);
        checkUnitTypeExist(balancesDTO.getUnitTypeId(), domainId);
        checkBillingCycleTypeExist(balancesDTO.getBillingCycleTypeId(), domainId);
        checkDuplicateName(balancesDTO.getName(), balancesDTO.getId());
        checkStartDateType(balancesDTO);
        checkExpireDateType(balancesDTO);
        checkMultiBalType(balancesDTO);
        checkStartDateAndEndDate(balancesDTO);
        checkAcmAndOwnerLevel(balancesDTO);
    }

    private void checkAcmAndOwnerLevel(BalancesDTO balancesDTO) {
        //is_acm (ẩn ko hiển thị ở giao diện) = false và dropdown list owner level ko cho hiển thị giá trị "Membership"
        //s_acm (ẩn ko hiển thị ở giao diện) = true và dropdown list owner level hiển thị thêm giá trị "Membership"
        // Integer dropdownValue = referTableRepository.findValueById(balTypeDTO.getOwnerLevelId());
        if (!balancesDTO.isAcm() && balancesDTO.getOwnerLevelId() == DropdownType.OwnerLevel.MEMBERSHIP) {
            throw new DataConstrainException(ErrorMessage.BalType.INVALID, Resources.BAL_TYPE, BalancesDTO.FieldNames.OWNER_LEVEL);
        }
    }

    private void checkStartDateType(BalancesDTO balancesDTO) {
        DropdownType.EffectDateType effectDateType = DropdownType.EffectDateType.of(balancesDTO.getEffDateTypeId());
        switch (effectDateType) {
            case None:
                balancesDTO.setStartDate(null);
                balancesDTO.setStartDateData(null);
                break;
            case StartDateField:
            case FromProvisioning:
            case FirstEvent:
            case BillingCycle:
            case PurcharseDate:
            case StartOfHour:
            case StartOfDay:
            case StartOfWeek:
            case StartOfMonth:
            case StartOfYear:
                if (balancesDTO.getStartDate() == null) {
                    throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.START_DATE);
                }
                balancesDTO.setStartDateData(null);
                break;
            case HoursFromStartOfDay:
            case DaysFromStartOfWeek:
            case WeeksFromStartOfMonth:
            case MonthsFromStartOfYear:
                if (balancesDTO.getStartDateData() == null) {
                    throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.START_DATE_DATA);
                }
                balancesDTO.setStartDate(null);
                break;
            default:
                throw new DataInvalidException(ErrorMessage.BalType.EFF_DATE_TYPE_ID_INVALID, Resources.BAL_TYPE, BalancesDTO.FieldNames.EFF_DATE_TYPE_ID);
        }
    }

    private void checkExpireDateType(BalancesDTO balancesDTO) {
        DropdownType.ExpireDateType expireDateType = DropdownType.ExpireDateType.of(balancesDTO.getExpDateTypeId());
        switch (expireDateType) {
            case None:
                balancesDTO.setExpDateData(null);
                balancesDTO.setExpDate(null);
                break;
            case ExpireDateField:
            case FromProvisioning:
            case BillingCycle:
            case EndOfHour:
            case EndOfDay:
            case EndOfWeek:
            case EndOfMonth:
            case EndOfYear:
                if (balancesDTO.getExpDate() == null) {
                    throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.EXP_DATE);
                }
                balancesDTO.setExpDateData(null);
                break;
            case Hours:
            case Days:
            case Weeks:
            case Months:
            case Years:
                if (balancesDTO.getExpDateData() == null) {
                    throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.EXP_DATE_DATA);
                }
                balancesDTO.setExpDate(null);
                break;
            default:
                throw new DataInvalidException(ErrorMessage.BalType.EXP_DATE_TYPE_ID_INVALID, Resources.BAL_TYPE, BalancesDTO.FieldNames.EXP_DATE_TYPE_ID);
        }
    }

    private void checkStartDateAndEndDate(BalancesDTO balancesDTO) {
        if (balancesDTO.getStartDateData() != null && balancesDTO.getExpDateData() != null && balancesDTO.getStartDateData() > balancesDTO.getExpDateData()) {
            throw new DataInvalidException(ErrorMessage.BalType.LESS_THAN_OR_EQUAL, Resources.BAL_TYPE, BalancesDTO.FieldNames.START_DATE_DATA);
        }
        if (balancesDTO.getStartDate() != null && balancesDTO.getExpDate() != null && balancesDTO.getStartDate().after(balancesDTO.getExpDate())) {
            throw new DataInvalidException(ErrorMessage.BalType.LESS_THAN_OR_EQUAL, Resources.BAL_TYPE, BalancesDTO.FieldNames.START_DATE);
        }
    }

    private void checkMultiBalType(BalancesDTO balancesDTO) {
        if (balancesDTO.getMultiBalTypeId().equals(DropdownType.MultiBalType.PERIODIC)) {
            //List<String> errorKeys = new ArrayList<>();
            if (balancesDTO.getWindowSize() == null) {
                //errorKeys.add(BalancesDTO.FieldNames.WINDOW_SIZE);
                throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.WINDOW_SIZE);
            }
            if (balancesDTO.getLowLevel() == null) {
                //errorKeys.add(BalancesDTO.FieldNames.LOW_LEVEL);
                throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.LOW_LEVEL);
            }
            if (balancesDTO.getHighLevel() == null) {
                //errorKeys.add(BalancesDTO.FieldNames.HIGH_LEVEL);
                throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, BalancesDTO.FieldNames.HIGH_LEVEL);
            }
//            if (errorKeys.size() > 0) {
//                throw new MustNotBeEmptyException(ErrorMessage.BalType.EMPTY_FIELD, Resources.BAL_TYPE, String.join(" ,", errorKeys));
//            }
        } else {
            balancesDTO.setPeriodicTypeId(null);
            balancesDTO.setPeriodicPeriod(null);
            balancesDTO.setWindowSize(null);
            balancesDTO.setLowLevel(null);
            balancesDTO.setHighLevel(null);
        }
    }

    // Check category exist or not
    private void checkCategoryExist(BalancesDTO balancesDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(balancesDTO.getParentId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void checkBalTypeExist(Long balTypeId, Integer domainId) {
        BalType balType = balTypeRepository.findByIdAndDomainIdForUpdate(balTypeId, domainId);
        if (balType == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalType.NOT_FOUND, Resources.BAL_TYPE, ErrorKey.BalType.ID);
        }
    }
    public void checkBalTypeInABMExist(Long fromId, Long toId, Integer domainId) {
        BalType balTypeFrom = balTypeRepository.findByIdAndDomainIdForUpdate(fromId, domainId);
        if (balTypeFrom == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalType.FROM_NOT_FOUND, Resources.BAL_TYPE, ErrorKey.ENTITY);
        }
        BalType balTypeTo = balTypeRepository.findByIdAndDomainIdForUpdate(toId, domainId);
        if (balTypeTo == null) {
            throw new ResourceNotFoundException(ErrorMessage.BalType.TO_NOT_FOUND, Resources.BAL_TYPE, ErrorKey.ENTITY);
        }
    }
    private void checkUnitTypeExist(Long unitTypeId, Integer domainId) {
        UnitType unitType = unitTypeRepository.findByIdAndDomainIdForUpdate(unitTypeId, domainId);
        if (unitType == null) {
            throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.UNIT_TYPE, ErrorKey.UnitType.UNIT_TYPE_ID);
        }
    }
    private void checkBillingCycleTypeExist(Integer billingCycleTypeIdInt, Integer domainId ){
        if (billingCycleTypeIdInt == null)
            return;
        Long billingCycleTypeId = Long.valueOf( billingCycleTypeIdInt);
        BillingCycleType billingCycleType = billingCycleTypeRepository.findByIdAndDomainIdForUpdate(billingCycleTypeId, domainId);
        if (billingCycleType == null) {
            throw new ResourceNotFoundException(ErrorMessage.BillingCycle.BILLING_CYCLE_TYPE_ID, Resources.BILLING_CYCLE_TYPE, ErrorKey.BillingCycle.Billing_Cycle_Type_Id);
        }
    }

    public OCSCloneableEntity cloneBalType(Long balanceId, int deep, String name) {
        return cloneEntity(balanceId, 1, deep, name);
    }

    public boolean checkHasChild(Long balanceId, Integer domainId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, balanceId);
        searchCriteria.setDomainId(domainId);
        Pageable pageable = PageRequest.of(0, 1);
        return thresholdService.getThresholds(balanceId, null, pageable).getTotalElements() > 0L;
    }

    public boolean checkIsUsingBillingCycle(Long billingCycleId) {
        Integer numOfBalanceUsing = balTypeRepository.isUsingBillingCycle(Math.toIntExact(billingCycleId));
        return numOfBalanceUsing != null && numOfBalanceUsing > 0;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.BALANCES;
    }

    @Override
    protected void setTypeForTree (OCSMoveableEntity entity, Tree tree) {
        BalType balance = (BalType) entity;
        if (balance.isAcm()) {
            ((BalancesDTO) tree).setType(Resources.ACM_BAL_TYPE);
            ((BalancesDTO) tree).setIcon(Resources.ACM_BAL_TYPE);
        }
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.BalType.BALANCE_DUPLICATED_NAME, Resources.BAL_TYPE, ErrorKey.NAME);
    }

    @Override
    protected String getResourceName() {
        return Resources.BAL_TYPE;
    }
}
