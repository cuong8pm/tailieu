package com.mycompany.myapp.service.event;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.repository.actiontype.EventActionTypeMapReponsitory;
import com.mycompany.myapp.service.actiontype.ActionTypeService;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class EventActionTypeMapService extends AbstractCloneMappingService {

    @Autowired
    EventActionTypeMapReponsitory eventActionTypeMapReponsitory;

    @Autowired
    EventService eventService;

    @Autowired
    ActionTypeService actionTypeService;

    @Override
    protected String getResource() {
        return Resources.ACTION_TYPE;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = eventActionTypeMapReponsitory;
        this.parentService = eventService;
        this.childService = actionTypeService;
        this.parentDependService = eventService;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
