package com.mycompany.myapp.service.event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import com.mycompany.myapp.dto.pricecomponent.PriceComponentBlockMapDTO;
import com.mycompany.myapp.repository.actiontype.ActionTypeRepository;
import com.mycompany.myapp.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.RatingFilter;
import com.mycompany.myapp.domain.actiontype.EventActionTypeMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.event.Event;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.actiontype.EventActionTypeMapDTO;
import com.mycompany.myapp.dto.event.EventDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.mapper.actiontype.ActiontypeMapper;
import com.mycompany.myapp.mapper.actiontype.EventActionTypeMapMapper;
import com.mycompany.myapp.mapper.event.EventMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.actiontype.EventActionTypeMapReponsitory;
import com.mycompany.myapp.repository.event.EventRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.RatingFilterParentService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class EventService extends AbstractCloneService implements RatingFilterParentService{

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CategoryService categoryService;

    @Autowired
    List<EventActionTypeMapService> eventActionTypeMapService;

    @Autowired
    EventMapper eventMapper;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    RatingFilterRepository ratingFilterRepository;

    @Autowired
    EventActionTypeMapReponsitory eventActionTypeMapReponsitory;

    @Autowired
    EventActionTypeMapMapper eventActionTypeMapMapper;

    @Autowired
    ActiontypeMapper actiontypeMapper;

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.ACTION_TYPE);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new EventDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.EVENT;
    }

    @Autowired
    private ActionTypeRepository actionTypeRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = eventRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = eventRepository;
        this.cloneMappingServices = eventActionTypeMapService;
    }

    public Page<EventDTO> listChildEvents(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<EventDTO> page = eventRepository.getListChildEvents(categoryId, name, description, domainId, pageable).map(eventMapper::toDto);
        return page;
    }

    public EventDTO getEventDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Event event = eventRepository.findByIdAndDomainId(id, domainId);
        if (event == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.EVENT, ErrorKey.ID);
        }
        EventDTO dto = eventMapper.toDto(event);
        Category category = categoryRepository.findByIdAndDomainId(dto.getCategoryId(), domainId);
        if (category != null) {
            dto.setCategoryName(category.getName());
            dto.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(dto.getEventAnalysisFilter(), domainId);
        if (ratingFilter != null) {
            dto.setRatingFilterName(ratingFilter.getName());
        }
        List<EventActionTypeMap> list = eventActionTypeMapReponsitory.findByDomainIdAndParentIdOrderByPosIndex(domainId, id);
        List<EventActionTypeMapDTO> actionTypeMapDTOs = eventActionTypeMapMapper.toDto(list);
        List<com.mycompany.myapp.domain.actiontype.ActionType> actionTypes = eventActionTypeMapReponsitory.getActionTypes(id, domainId);
        for (int i = 0; i < actionTypeMapDTOs.size(); i++) {
            actionTypeMapDTOs.get(i).setActionTypeName(actionTypes.get(i).getName());
            actionTypeMapDTOs.get(i).setDescription(actionTypes.get(i).getDescription());
        }
        dto.setActionTypeMapDTOs(actionTypeMapDTOs);
        return dto;
    }

    public List<ActionTypeDTO> getListActionTypes(Long eventId) {
        Integer domainId = OCSUtils.getDomain();
        List<ActionTypeDTO> list = actiontypeMapper.toDto(eventActionTypeMapReponsitory.getActionTypes(eventId, domainId));
        return list;
    }

    public BaseResponseDTO updateEvent(EventDTO eventDTO) {
        Integer domainId = OCSUtils.getDomain();
        Event eventDB = eventRepository.findByIdAndDomainId(eventDTO.getId(), domainId);
        if (eventDB == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.EVENT, ErrorKey.ID);
        }
        checkCategory(eventDTO.getCategoryId());
        checkDuplicateName(eventDTO.getName(), eventDTO.getId());
        Event event = eventMapper.toEntity(eventDTO);
        event.setDomainId(domainId);
        if (!Objects.equals(eventDTO.getCategoryId(), eventDB.getCategoryId())) {
            setPosIndex(domainId, event, eventDTO.getCategoryId());
        }
        event = eventRepository.save(event);
        List<EventActionTypeMapDTO> removeDTOs = new ArrayList<>();
        for (EventActionTypeMapDTO item : eventDTO.getActionTypeMapDTOs()) {
            if (item.getActionType() == ActionType.DELETE) {
                eventActionTypeMapReponsitory.deleteByDomainIdAndChildIdAndParentId(domainId, item.getActionTypeId(), eventDTO.getId());
                removeDTOs.add(item);
            }
        }
        for (EventActionTypeMapDTO item : eventDTO.getActionTypeMapDTOs()) {
            if (item.getActionType() == ActionType.EDIT) {
                com.mycompany.myapp.domain.actiontype.ActionType actionType = actionTypeRepository.findByIdAndDomainId(item.getActionTypeId(),domainId);
                if(actionType == null){
                    throw new ResourceNotFoundException(ErrorMessage.ActionType.ID_NOT_FOUND,Resources.ACTION_TYPE,ErrorKey.ENTITY_NOT_FOUND);
                }
                EventActionTypeMap eventActionTypeMap = eventActionTypeMapMapper.toEntity(item);
                eventActionTypeMap.setEventId(eventDTO.getId());
                eventActionTypeMap.setDomainId(domainId);
                eventActionTypeMapReponsitory.save(eventActionTypeMap);
                removeDTOs.add(item);
            }
        }
        eventDTO.getActionTypeMapDTOs().removeAll(removeDTOs);
        Integer posIndex = eventActionTypeMapReponsitory.getMaxPosIndex(eventDTO.getId(), domainId);
        if (posIndex == null) {
            posIndex = 0;
        }
        else {
            posIndex++;
        }
        List<Long> actionTypeIds = eventDTO.getActionTypeMapDTOs().stream()
            .filter(item -> ActionType.ADD.equals(item.getActionType()))
            .map(EventActionTypeMapDTO::getActionTypeId).collect(Collectors.toList());
        List<com.mycompany.myapp.domain.actiontype.ActionType> actionTypes = actionTypeRepository.findAllByDomainIdAndIdIn(domainId,actionTypeIds);

        for (EventActionTypeMapDTO item : eventDTO.getActionTypeMapDTOs()) {
            if (item.getActionType() == ActionType.ADD) {
                if (actionTypes.stream().noneMatch(actionType -> Objects.equals(actionType.getId(), item.getActionTypeId()))) {
                    throw new ResourceNotFoundException(ErrorMessage.ActionType.ID_NOT_FOUND,Resources.ACTION_TYPE,ErrorKey.ENTITY_NOT_FOUND);
                }
                EventActionTypeMap eventActionTypeMap = eventActionTypeMapMapper.toEntity(item);
                eventActionTypeMap.setEventId(eventDTO.getId());
                eventActionTypeMap.setDomainId(domainId);
                eventActionTypeMap.setPosIndex(posIndex);
                eventActionTypeMapReponsitory.save(eventActionTypeMap);
                posIndex++;
            }
        }
        Integer countChild = eventActionTypeMapReponsitory.countByDomainIdAndParentId(domainId, eventDTO.getId());
        if (countChild == 0) {
            throw new DataInvalidException(ErrorMessage.Event.NOT_EMPTY, Resources.EVENT, ErrorKey.Event.MAP_ITEM);
        }
        if (countChild == 1 && eventDTO.getEventAnalysisFilter() != null) {
            throw new DataConstrainException(ErrorMessage.Event.EVEN_ANALYSIS_MUST_BE_NULL, Resources.EVENT, ErrorKey.Event.RATING_FILTER_ID);
        }
        if (countChild >= 2) {
            if (eventDTO.getEventAnalysisFilter() == null) {
                throw new DataConstrainException(ErrorMessage.Event.EVEN_ANALYSIS_MUST_BE_NOT_NULL, Resources.EVENT, ErrorKey.Event.RATING_FILTER_ID);
            } else {
                RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(eventDTO.getEventAnalysisFilter(), domainId);
                if (ratingFilter == null) {
                    throw new DataInvalidException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.Event.RATING_FILTER_NAME);
                }
            }
        }
        eventDTO = getEventDetail(event.getId());
        return eventDTO;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Event.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.EVENT;
    }

    @Override
    protected String getNameCategory() {
        return "Events";
    }

    public BaseResponseDTO createEvent(EventDTO eventDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateId(eventDTO.getId());
        checkCategory(eventDTO.getCategoryId());
        checkDuplicateName(eventDTO.getName(), eventDTO.getId());
        checkDuplicateActionType(eventDTO.getActionTypeMapDTOs());
        if (eventDTO.getActionTypeMapDTOs().size() == 1 && eventDTO.getEventAnalysisFilter() != null) {
            throw new DataConstrainException(ErrorMessage.Event.EVEN_ANALYSIS_MUST_BE_NULL, Resources.EVENT, ErrorKey.Event.RATING_FILTER_ID);
        }
        if (eventDTO.getActionTypeMapDTOs().size() >= 2) {
            if (eventDTO.getEventAnalysisFilter() == null) {
                throw new DataConstrainException(ErrorMessage.Event.EVEN_ANALYSIS_MUST_BE_NOT_NULL, Resources.EVENT, ErrorKey.Event.RATING_FILTER_ID);
            } else {
                RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(eventDTO.getEventAnalysisFilter(), domainId);
                if (ratingFilter == null) {
                    throw new DataInvalidException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.Event.RATING_FILTER_NAME);
                }
            }
        }
        Event event = eventMapper.toEntity(eventDTO);
        setPosIndex(domainId, event, event.getCategoryId());
        event.setDomainId(domainId);
        event = eventRepository.save(event);
        List<Long> actionTypeIds = eventDTO.getActionTypeMapDTOs().stream()
            .map(EventActionTypeMapDTO::getActionTypeId).collect(Collectors.toList());
        List<com.mycompany.myapp.domain.actiontype.ActionType> actionTypes = actionTypeRepository.findAllByDomainIdAndIdIn(domainId,actionTypeIds);
        for(EventActionTypeMapDTO item : eventDTO.getActionTypeMapDTOs()) {
            if(actionTypes.stream().noneMatch(actionType -> Objects.equals(actionType.getId(),item.getActionTypeId()))){
                throw new ResourceNotFoundException(ErrorMessage.ActionType.ID_NOT_FOUND,Resources.ACTION_TYPE,ErrorKey.ENTITY_NOT_FOUND);
            }
            EventActionTypeMap map = eventActionTypeMapMapper.toEntity(item);
            map.setEventId(event.getId());
            map.setDomainId(domainId);
            eventActionTypeMapReponsitory.save(map);
        }
        eventDTO = getEventDetail(event.getId());
        return eventDTO;
    }

    private void checkDuplicateId(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Event event = eventRepository.findByIdAndDomainId(id, domainId);
        if (event != null) {
            throw new DataConstrainException(ErrorMessage.Event.DUPLICATED_ID, Resources.EVENT, ErrorKey.Event.ID);
        }

    }

    public void deleteEvent(Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<EventActionTypeMap> eventActionTypeMaps= eventActionTypeMapReponsitory.findByEventIdAndDomainId(id, domainId);
         eventActionTypeMapReponsitory.deleteAll(eventActionTypeMaps);
         eventRepository.deleteByIdAndDomainId(id, domainId);
    }

   public void checkDuplicateActionType(List<EventActionTypeMapDTO> list) {
       Set<Long> actionTypes  = new HashSet<>();
        for(int i =0 ; i< list.size() ; i++) {
            actionTypes.add(list.get(i).getActionTypeId());
        }
        if(actionTypes.size() != list.size()) {
            throw new DataConstrainException(ErrorMessage.Event.DUPLICATED_ACTIONTYPEID,Resources.EVENT,ErrorKey.Event.ACTION_TYPE_ID);
        }
    }

   @Override
   protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
       return eventRepository.findByDomainIdAndEventAnalysisFilterIn(domainId, ids);
   }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Event.DUPLICATED_NAME, Resources.EVENT, ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.Event.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }
}
