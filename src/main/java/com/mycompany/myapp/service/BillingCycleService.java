package com.mycompany.myapp.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.domain.BillingCycleType;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.constant.BillingCycleStatus;
import com.mycompany.myapp.domain.constant.CalcUnitType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.BillingCycleDTO;
import com.mycompany.myapp.dto.BillingCycleTypeDTO;
import com.mycompany.myapp.dto.GenerateBillingCycleDTO;
import com.mycompany.myapp.mapper.BillingCycleMapper;
import com.mycompany.myapp.mapper.BillingCycleTypeMapper;
import com.mycompany.myapp.repository.BillingCycleRepository;
import com.mycompany.myapp.repository.BillingCycleTypeRepository;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.service.billingcycle.BillingCycleCalculation;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class BillingCycleService extends OCSBaseService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BillingCycleRepository billingCycleRepository;

    @Autowired
    private BillingCycleTypeRepository billingCycleTypeRepository;

    @Autowired
    private BillingCycleTypeMapper billingCycleTypeMapper;

    @Autowired
    private BillingCycleMapper billingCycleMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BalancesService balancesService;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferTemplateRepository offerTemplateRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = billingCycleTypeRepository;
        this.parentService = categoryService;
    }

    public BaseResponseDTO createBillingCycleType(BillingCycleTypeDTO billingCycleTypeDTO) {
        Integer domainId = OCSUtils.getDomain();

        checkParentAndLock(billingCycleTypeDTO.getCategoryId(), domainId);

        checkBillingCycleType(billingCycleTypeDTO);

        checkDuplicateName(billingCycleTypeDTO.getName(), billingCycleTypeDTO.getId());

        BillingCycleType billingCycleType = billingCycleTypeMapper.toEntity(billingCycleTypeDTO);
        billingCycleType.setDomainId(domainId);
        super.setPosIndex(domainId, billingCycleType, billingCycleType.getCategoryId());
        billingCycleType.setName(billingCycleType.getName().trim());
        billingCycleType = billingCycleTypeRepository.save(billingCycleType);


        return billingCycleTypeMapper.toDto(billingCycleType);
    }

    public BaseResponseDTO updateBillingCycleType(BillingCycleTypeDTO billingCycleTypeDTO) {
        Integer domainId = OCSUtils.getDomain();

        checkParentAndLock(billingCycleTypeDTO.getCategoryId(), domainId);
        checkBillingCycleType(billingCycleTypeDTO);

        BillingCycleType billingCycleTypeDB = billingCycleTypeRepository.findByIdAndDomainIdForUpdate(billingCycleTypeDTO.getId(), domainId);

        if (billingCycleTypeDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BILLING_CYCLE_TYPE, ErrorKey.ID);
        }

        Long countBillingCycles = billingCycleRepository.countByParentIdAndDomainId(billingCycleTypeDTO.getId(), domainId);

        if(!billingCycleTypeDTO.getUnitId().equals(billingCycleTypeDB.getCalcUnitId()) && countBillingCycles != 0) {
            throw new DataConstrainException(ErrorMessage.BillingCycle.INVALID_UNIT_TYPE, "", ErrorKey.BillingCycle.UNIT_TYPE);
        }

        checkDuplicateName(billingCycleTypeDTO.getName(), billingCycleTypeDTO.getId());

        BillingCycleType billingCycleType = billingCycleTypeMapper.toEntity(billingCycleTypeDTO);

        if (!Objects.equal(billingCycleTypeDB.getCategoryId(), billingCycleTypeDTO.getCategoryId())) {
            super.setPosIndex(domainId, billingCycleType, billingCycleType.getCategoryId());
        }
        billingCycleType.setDomainId(domainId);
        billingCycleType.setName(billingCycleType.getName().trim());
        billingCycleTypeRepository.save(billingCycleType);

        return billingCycleTypeMapper.toDto(billingCycleType);
    }

    private void checkBillingCycleType(BillingCycleTypeDTO billingCycleTypeDTO) {

        List<CalcUnitType> hasFromOfDay = Arrays.asList(CalcUnitType.HOUR_OF_DAY, CalcUnitType.DAY_OF_MONTH, CalcUnitType.DAY_OF_WEEK, CalcUnitType.MONTH_FOR_MAIN_PP);

        CalcUnitType calcUnitType = CalcUnitType.of(billingCycleTypeDTO.getUnitId().intValue());

        if (!hasFromOfDay.contains(calcUnitType)) {
            billingCycleTypeDTO.setFromOfDay(null);
        }
    }

    public void deleteBillingCycleType(Long id) {
        Integer domainId = OCSUtils.getDomain();
        BillingCycleType billingCycleType = billingCycleTypeRepository.findByIdAndDomainIdForUpdate(id, domainId);

        if (billingCycleType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, "", ErrorKey.ID);
        }

        Long countBillingCycles = billingCycleRepository.countByParentIdAndDomainId(id, domainId);

        if (countBillingCycles != 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_HAS_CHILD, "", ErrorKey.ID);
        }

        if(balancesService.checkIsUsingBillingCycle(id)){
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
        }

        if ((offerRepository.countByBillingCycleTypeIdAndAndDomainId(id, domainId)) > 0 ) {
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
        }

        if(offerTemplateRepository.countByBillingCycleTypeIdAndDomainId(id, domainId) >0) {
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
        }

        billingCycleTypeRepository.deleteByIdAndDomainId(id, domainId);
    }

    public Page<BillingCycleTypeDTO> searchBillingCycleType(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();

        Page<BillingCycleType> billingCycleTypes = billingCycleTypeRepository.findBillingCycles(categoryId, domainId, name, description, pageable);

        return billingCycleTypes.map(billingCycleTypeMapper::toDto);
    }

    public BillingCycleTypeDTO getBillingCycleTypeDetails(Long id) {
        Integer domainId = OCSUtils.getDomain();

        BillingCycleType billingCycleType = billingCycleTypeRepository.findByIdAndDomainId(id, domainId);

        BillingCycleTypeDTO billingCycleTypeDTO = billingCycleTypeMapper.toDto(billingCycleType);
        // Get last end date of billing cycle
        Pageable pageable = PageRequest.of(0, 1, Sort.by(Direction.DESC, BillingCycle.FieldNames.BEGIN_DATE));
        List<BillingCycle> lastBillingCycle = billingCycleRepository.findBillingCycle(id, domainId, null, null, pageable).getContent();
        if (!CollectionUtils.isEmpty(lastBillingCycle)) {
            billingCycleTypeDTO.setLastestDate(lastBillingCycle.get(0).getCycleEndDate());
        }
        billingCycleTypeDTO.setCategoryLevel(getCategoryLevel(billingCycleTypeDTO.getCategoryId()) + 1);

        return billingCycleTypeDTO;
    }

    private Category checkParentAndLock(Long parentId, Integer domainId) {
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(parentId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, "", ErrorKey.CATEGORY_ID);
        }
        if (category.getCategoryType() != CategoryType.BILLING_CYCLE) {
            throw new ResourceNotFoundException(ErrorMessage.CATEGORY_INCORRECT, "", ErrorKey.CATEGORY_ID);
        }
        return category;
    }

    public List<BillingCycle> generateBillingCycle(GenerateBillingCycleDTO generation, Long billingCycleTypeId) {
        Integer domainId = OCSUtils.getDomain();
        LocalDateTime maxDate = LocalDateTime.of(2050, 12, 31, 0, 0);
        if (generation.getBeginDate().compareTo(Timestamp.valueOf(maxDate)) > 0) {
            throw new DataConstrainException(ErrorMessage.BillingCycle.MAX_DATE, Resources.BILLING_CYCLE, ErrorKey.BillingCycle.BEGIN_DATE);
        }
        if (generation.getEndDate().compareTo(Timestamp.valueOf(maxDate)) > 0) {
            throw new DataConstrainException(ErrorMessage.BillingCycle.MAX_DATE, Resources.BILLING_CYCLE, ErrorKey.BillingCycle.END_DATE);
        }
        BillingCycleType billingCycleType = billingCycleTypeRepository.findByIdAndDomainIdForUpdate(billingCycleTypeId,
                domainId);

        // Check last end date of billing cycle
        Pageable pageable = PageRequest.of(0, 1, Sort.by(Direction.DESC, BillingCycle.FieldNames.ID));
        List<BillingCycle> lastBillingCycle = billingCycleRepository.findBillingCycle(billingCycleTypeId, domainId, null, null, pageable).getContent();
        if (!CollectionUtils.isEmpty(lastBillingCycle)) {
            generation.setBeginDate(lastBillingCycle.get(0).getCycleEndDate());
        }

        CalcUnitType calcUnitType = CalcUnitType.of(billingCycleType.getCalcUnitId().intValue());

        BillingCycleCalculation billingCycleCalculation = BillingCycleCalculation.create(
                generation.getBeginDate(), generation.getEndDate(), generation.getQuantity(),
                calcUnitType, billingCycleType.getId(), generation.getFromOfDay());

        List<BillingCycle> billingCycles = billingCycleCalculation.generateBillingCycles();
        billingCycles = billingCycleRepository.saveAll(billingCycles);
        return billingCycles;
    }

    public Page<BillingCycleDTO> findBillingCycles(Long billingCycleTypeId, String beginDate, String endDate, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();

        Page<BillingCycle> billingCycles = billingCycleRepository.findBillingCycle(billingCycleTypeId, domainId, beginDate, endDate, pageable);

        return billingCycles.map(billingCycleMapper::toDto);
    }

    public void deleteBillingCycles(List<Long> ids, Long billingCycleTypeId) {
        Integer domainId = OCSUtils.getDomain();

        if (checkDeleteBillingCycles(ids, billingCycleTypeId)) {
            billingCycleRepository.deleteByIdInAndDomainIdAndBillingCycleTypeId(ids, domainId, billingCycleTypeId);
        }
    }

    public boolean checkDeleteBillingCycles(List<Long> ids, Long billingCycleTypeId) {
        Integer domainId = OCSUtils.getDomain();
        Long minId = ids.stream().min(Long::compareTo).orElse(Long.valueOf(0L));

        // Must delete newest data first
        Long countUpDeleteId = billingCycleRepository.countUpIdToDelete(minId, ids, billingCycleTypeId, domainId);

        Long maxId = ids.stream().max(Long::compareTo).orElse(Long.valueOf(0L));

        Long countDownDeleteId = billingCycleRepository.countDownIdToDelete(maxId, ids, billingCycleTypeId, domainId);

        if (countUpDeleteId.longValue() == 0L || countDownDeleteId.longValue() == 0L) {
            return true;
        }
        throw new DataConstrainException(ErrorMessage.BillingCycle.DELETE_OLDEST_BILLING_CYCLE, Resources.BILLING_CYCLE_TYPE, ErrorKey.ID);

    }

    public void changeStatus(Long billingCycleTypeId, Long billingCycleId) {
        Integer domainId = OCSUtils.getDomain();

        // Lock parent
        billingCycleTypeRepository.findByIdAndDomainIdForUpdate(billingCycleTypeId, domainId);

        BillingCycle billingCycle = billingCycleRepository.findByIdAndDomainIdForUpdate(billingCycleId, domainId);

        // Change all billing cycle to non active
        billingCycleRepository.updateBillingCycleStatus(domainId, billingCycleTypeId, BillingCycleStatus.NON_ACTIVE);
        if (billingCycle.getState() == BillingCycleStatus.ACTIVE) {
            billingCycle.setState(BillingCycleStatus.NON_ACTIVE);
        } else {
            billingCycle.setState(BillingCycleStatus.ACTIVE);
        }

        // Update billing cycle
        billingCycleRepository.save(billingCycle);

    }
    public List<BaseResponseDTO> getForDropdown(){
        Integer domainId = OCSUtils.getDomain();
        Sort sort = Sort.by(Direction.ASC, BillingCycleType.FieldNames.POS_INDEX);
        List<BillingCycleType> cycleTypes = billingCycleTypeRepository.findByDomainId(domainId, sort);

        List<BaseResponseDTO> baseResponseDTOS;
        baseResponseDTOS = cycleTypes.stream()
            .map(unitType -> {
                BaseResponseDTO response = new BaseResponseDTO();
                BeanUtils.copyProperties(unitType, response);
                return response;
            })
            .collect(Collectors.toList());
        return baseResponseDTOS;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.BILLING_CYCLE;
    }
}
