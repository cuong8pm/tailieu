package com.mycompany.myapp.service.actiontype;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import com.mycompany.myapp.service.exception.DataConstrainException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.formula.Formula;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.actiontype.ActionTypeCustomDTO;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.actiontype.EventOfActionTypeDTO;
import com.mycompany.myapp.mapper.actiontype.ActionTypeCustomMapper;
import com.mycompany.myapp.mapper.actiontype.ActiontypeMapper;
import com.mycompany.myapp.repository.actiontype.ActionTypeRepository;
import com.mycompany.myapp.repository.actiontype.EventActionTypeMapReponsitory;
import com.mycompany.myapp.repository.formula.FormulaRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.ratetable.RateTableService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Transactional
@Service
public class ActionTypeService extends AbstractCloneService {

    @Autowired
    private ActionTypeRepository actionTypeRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ActiontypeMapper actionTypeMapper;

    @Autowired
    private EventActionTypeMapReponsitory eventActionTypeMapReponsitory;

    @Autowired
    private FormulaRepository formulaRepository;

    @Autowired
    private ActionTypeCustomMapper actionTypeCustomMapper;

    @Autowired
    private RateTableService rateTableService;

    public static final String FORMULA_TYPE = "formula_type";
    public static final String B = "B";
    public static final Long EVENT_ANALYSIS = 8l;

    public List<ActionType> getAllActionType() {
        Integer domainId = OCSUtils.getDomain();
        return actionTypeRepository.findByDomainId(domainId);
    }

    @Override
    protected List<CategoryType> dependTypes() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<CategoryType> dependOnTypes() {

        return Arrays.asList();
    }

    @Override
    protected TreeClone getTreeDTO() {

        return new ActionTypeDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.ACTION_TYPE;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = actionTypeRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = actionTypeRepository;
        // this.cloneMappingServices =

    }

    public ActionTypeDTO createActiontype(ActionTypeCustomDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkCategory(dto.getCategoryId());
        ActionType actionType = actionTypeCustomMapper.toEntity(dto);
        checkDuplicateName(actionType.getName(), actionType.getId());
        checkDuplicateId(actionType.getId(), domainId);
        setPosIndex(domainId, actionType, actionType.getCategoryId());
        actionType.setDomainId(domainId);
        actionType = actionTypeRepository.save(actionType);
        ActionTypeDTO actionTypeDTO = actionTypeMapper.toDto(actionType);
        actionTypeDTO.setHasChild(false);
        return actionTypeDTO;
    }

    private void checkDuplicateId(Long id, Integer domainId) {
        ActionType actionType = actionTypeRepository.findByIdAndDomainId(id, domainId);
        if (actionType != null) {
            throw new DataInvalidException(ErrorMessage.ActionType.DUPLICATED_ID, Resources.ACTION_TYPE, ErrorKey.ActionType.ACTION_TYPE_ID);
        }
    }

    public void deleteActionType(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Integer count = eventActionTypeMapReponsitory.countByActionTypeIdAndDomainId(id, domainId);
        if(count > 0) {
            throw new DataInvalidException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.ACTION_TYPE, ErrorKey.ActionType.ACTION_TYPE_ID);
        }
        Integer countActionTypeInAction = actionTypeRepository.countActionType(id, domainId);
        if (countActionTypeInAction > 0) {
            throw new DataInvalidException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.ACTION_TYPE,
                    ErrorKey.ActionType.ACTION_TYPE_ID);
        }
        List<Formula> formulas = formulaRepository.findByDomainIdAndFormulaType(domainId, EVENT_ANALYSIS);
        for (Formula item : formulas) {
            String bit = Integer.toBinaryString(item.getTemplateBits().intValue());
            for (int i = bit.length(); i < 6; i++) {
                bit = "0" + bit;
            }
            HashMap<String, Boolean> propertiesOfFormula = new HashMap<>();
            rateTableService.mappingPropertyByTempBits(bit, propertiesOfFormula);
            if (!propertiesOfFormula.get(FORMULA_TYPE) && !propertiesOfFormula.get(B)) {
                if (id.compareTo(item.getB()) == 0) {
                    throw new DataInvalidException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.ACTION_TYPE, ErrorKey.ActionType.ACTION_TYPE_ID);
                }
            }
        }
        actionTypeRepository.deleteByIdAndDomainId(id, domainId);
    }

    public ActionTypeDTO getActionTypeDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        ActionType actionType = actionTypeRepository.findByIdAndDomainId(id, domainId);
        if (actionType == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.ACTION_TYPE, ErrorKey.ID);
        }
        ActionTypeDTO dto = actionTypeMapper.toDto(actionType);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(dto.getCategoryId(), domainId);
        if (category != null) {
            dto.setCategoryId(category.getId());
            dto.setCategoryName(category.getName());
            dto.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        return dto;
    }

    public Page<ActionTypeDTO> ActionTypes(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<ActionTypeDTO> actionTypes = actionTypeRepository
                .getActionTypes(domainId, categoryId, name, description, pageable).map(actionTypeMapper::toDto);

        return actionTypes;
    }

    public Page<EventOfActionTypeDTO> listChildEvent(@Valid Long actionTypeId, Pageable pageable) {
        Page<EventOfActionTypeDTO> page = eventActionTypeMapReponsitory.getListEvent(actionTypeId, pageable);
        return page;
    }

    public BaseResponseDTO updateActiontype(ActionTypeDTO actionTypeDTO, Long id) {

        Integer domainId = OCSUtils.getDomain();
        actionTypeDTO.setId(id);
        ActionType actionType = actionTypeRepository.findByIdAndDomainId(id, domainId);
        if (actionType == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.ACTION_TYPE, ErrorKey.ID);
        }
        checkCategory(actionTypeDTO.getCategoryId());
        checkDuplicateName(actionTypeDTO.getName(), actionTypeDTO.getId());
        ActionType actionTypeMap = actionTypeMapper.toEntity(actionTypeDTO);
        if (!Objects.equal(actionTypeDTO.getCategoryId(), actionType.getCategoryId())) {
            setPosIndex(domainId, actionTypeMap, actionTypeDTO.getCategoryId());
        }
        actionTypeMap.setDomainId(domainId);
        actionTypeMap = actionTypeRepository.save(actionTypeMap);
        actionTypeDTO = actionTypeMapper.toDto(actionTypeMap);
        actionTypeDTO.setHasChild(false);
        return actionTypeDTO;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return ActionType.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.ACTION_TYPE;
    }

    @Override
    protected String getNameCategory() {
        return "Action Types";
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.ActionType.DUPLICATED_NAME, Resources.ACTION_TYPE, ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.ActionType.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }
}
