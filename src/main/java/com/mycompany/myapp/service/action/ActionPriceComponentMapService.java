package com.mycompany.myapp.service.action;

import com.mycompany.myapp.domain.action.ActionPriceComponentMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.dto.action.ActionPriceComponentMapDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.action.ActionPriceComponentMapRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.pricecomponent.PriceComponentService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ActionPriceComponentMapService extends AbstractCloneMappingService {

    @Autowired
    private ActionPriceComponentMapRepository actionPriceComponentMapRepository;

    @Autowired
    private ActionService actionService;

    @Autowired
    private PriceComponentService priceComponentService;

    @Autowired
    private PriceComponentRepository priceComponentRepository;

    @Override
    protected String getResource() {
        return Resources.PRICE_COMPONENT;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = actionPriceComponentMapRepository;
        this.childService = priceComponentService;
        this.parentService = actionService;
        this.parentDependService = actionService;
    }

    public void deletePriceComponent(Long actionId, Long priceComponentId) {
        Integer domainId = OCSUtils.getDomain();
//        ActionPriceComponentMap actionPriceComponentMap = actionPriceComponentMapRepository.findByDomainIdAndChildIdAndParentId(domainId, priceComponentId, actionId);
//        if (actionPriceComponentMap == null) {
//            throw new ResourceNotFoundException("price_component_not_found", getResource(), "id");
//        }
        actionPriceComponentMapRepository.deleteByDomainIdAndChildIdAndParentId(domainId, priceComponentId, actionId);
    }

    public void editActionPriceComponentMap(List<ActionPriceComponentMapDTO> actionPriceComponentMapDTOS,
                                            Long actionId) {
        Integer domainId = OCSUtils.getDomain();
        List<ActionPriceComponentMapDTO> removeDeletes = new ArrayList<>();
        Integer add = 0;
        for (ActionPriceComponentMapDTO item : actionPriceComponentMapDTOS) {
            if (ActionType.ADD.equals(item.getActionType())) {
                add = add + 1;
            }
        }
        for (ActionPriceComponentMapDTO item : actionPriceComponentMapDTOS) {

            if (ActionType.DELETE.equals(item.getActionType())) {
                Integer countByParentId = actionPriceComponentMapRepository.countByDomainIdAndParentId(domainId, actionId);
                if (countByParentId.intValue() <= 1 && add < 1) {
                    throw new DataConstrainException(ErrorMessage.Action.MUST_HAVE_ATLEAST_ONE_CHILD, getResource(), ErrorKey.Action.PRICE_COMPONENT);
                }
                deletePriceComponent(actionId, item.getPriceComponentId());
                removeDeletes.add(item);
            }
        }
        actionPriceComponentMapDTOS.removeAll(removeDeletes);
        removeDeletes = new ArrayList<>();
        for (ActionPriceComponentMapDTO item : actionPriceComponentMapDTOS) {
            if (ActionType.EDIT.equals(item.getActionType())) {
                PriceComponent priceComponent = priceComponentRepository.
                    findByIdAndDomainId(item.getPriceComponentId(), domainId);
                if (priceComponent == null) {
                    throw new ResourceNotFoundException(ErrorMessage.PriceComponent.NOT_FOUND, Resources.PRICE_COMPONENT, ErrorKey.ENTITY_NOT_FOUND);
                }
                ActionPriceComponentMap actionPriceComponentMap = actionPriceComponentMapRepository.findByIdAndDomainId(item.getId(), domainId);
                if (actionPriceComponentMap == null) {
                    throw new ResourceNotFoundException(ErrorMessage.ActionPriceComponentMap.NOT_FOUND, Resources.ACTION_PRICE_COMPONENT_MAP,
                        ErrorKey.ActionPriceComponentMap.PRICE_COMPONENT);
                }
                actionPriceComponentMap.setPosIndex(item.getPosIndex());
                actionPriceComponentMap.setPriceComponentId(item.getPriceComponentId());
                actionPriceComponentMapRepository.save(actionPriceComponentMap);
                removeDeletes.add(item);
            }
        }
        actionPriceComponentMapDTOS.removeAll(removeDeletes);
        for (ActionPriceComponentMapDTO item : actionPriceComponentMapDTOS) {
            if (ActionType.ADD.equals(item.getActionType())) {
                PriceComponent priceComponent = priceComponentRepository.
                    findByIdAndDomainId(item.getPriceComponentId(), domainId);
                if (priceComponent == null) {
                    throw new ResourceNotFoundException(ErrorMessage.PriceComponent.NOT_FOUND, Resources.PRICE_COMPONENT, ErrorKey.ENTITY_NOT_FOUND);
                }
                ActionPriceComponentMap actionPriceComponentMap = new ActionPriceComponentMap();
                actionPriceComponentMap.setPosIndex(item.getPosIndex());
                actionPriceComponentMap.setActionId(actionId);
                actionPriceComponentMap.setDomainId(domainId);
                actionPriceComponentMap.setPosIndex(item.getPosIndex());
                actionPriceComponentMap.setPriceComponentId(item.getPriceComponentId());
                actionPriceComponentMapRepository.save(actionPriceComponentMap);
                removeDeletes.add(item);
            }
        }
        actionPriceComponentMapDTOS.removeAll(removeDeletes);
    }

    @Override
    protected CategoryType getCategoryType() {
        return priceComponentService.getCategoryType();
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
