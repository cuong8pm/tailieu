package com.mycompany.myapp.service.action;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.RatingFilter;
import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.action.ActionPriceComponentMap;
import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ReserveInfoDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.action.ActionPriceComponentMapDTO;
import com.mycompany.myapp.dto.action.ActionRatingFilterDTO;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.mapper.ReserveInfoMapper;
import com.mycompany.myapp.mapper.action.ActionMapper;
import com.mycompany.myapp.mapper.action.ActionPriceComponentMapMapper;
import com.mycompany.myapp.mapper.actiontype.ActiontypeMapper;
import com.mycompany.myapp.mapper.pricecomponent.PriceComponentMapper;
import com.mycompany.myapp.mapper.ratingfilter.RatingFilterMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReserveInfoRepository;
import com.mycompany.myapp.repository.action.ActionPriceComponentMapRepository;
import com.mycompany.myapp.repository.action.ActionRepository;
import com.mycompany.myapp.repository.actiontype.ActionTypeRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionActionRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionActionRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentBlockMapRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRateTableMapRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.RatingFilterParentService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.offer.common.OfferVersionChildMappingCloneable;
import com.mycompany.myapp.service.pricecomponent.PriceComponentBlockMapService;
import com.mycompany.myapp.service.ratingfilter.RatingFilterService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ActionService extends AbstractCloneService implements RatingFilterParentService {
    @Autowired
    private ActionRepository actionRepository;
    @Autowired
    private ActionPriceComponentMapRepository actionPriceComponentMapRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private List<ActionPriceComponentMapService> actionPriceComponentMapServices;
    @Autowired
    private ActionMapper actionMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private OfferVersionActionRepository offerVersionActionRepository;
    @Autowired
    private ActionPriceComponentMapMapper actionPriceComponentMapMapper;
    @Autowired
    private ActionPriceComponentMapService actionPriceComponentMapService;
    @Autowired
    private PriceComponentRepository priceComponentRepository;
    @Autowired
    private PriceComponentMapper priceComponentMapper;
    @Autowired
    private PriceComponentBlockMapRepository priceComponentBlockMapRepository;
    @Autowired
    private RatingFilterRepository ratingFilterRepository;
    @Autowired
    private RatingFilterMapper ratingFilterMapper;
    @Autowired
    private ActionTypeRepository actionTypeRepository;
    @Autowired
    private ActiontypeMapper actiontypeMapper;
    @Autowired
    private ReserveInfoRepository reserveInfoRepository;
    @Autowired
    private ReserveInfoMapper reserveInfoMapper;
    @Autowired
    private List<RatingFilterService> ratingFilterServices;
    @Autowired
    private RatingFilterRateTableMapRepository ratingFilterRateTableMapRepository;
    @Autowired
    private OfferTemplateVersionActionRepository offerTemplateVersionActionRepository;
    @Autowired
    private List<OfferVersionChildMappingCloneable> offerVersionChildMappingCloneables;
    @Autowired
    private PriceComponentBlockMapService priceComponentBlockMapService;

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.PRICE_COMPONENT);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new ActionDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.ACTION;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Action.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.ACTION;
    }

    @Override
    protected String getNameCategory() {
        return "Actions";
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = actionRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = actionRepository;
        this.cloneMappingServices = actionPriceComponentMapServices;
        this.childDependServices = ratingFilterServices;
        this.parentDependMappingServices = offerVersionChildMappingCloneables;
    }

    public void deleteAction(Long actionId) {
        Integer domainId = OCSUtils.getDomain();
        Action action = actionRepository.findByIdAndDomainId(actionId, domainId);
        if (action == null) {
            throw new ResourceNotFoundException(ErrorMessage.Action.NOT_FOUND, Resources.ACTION,
                ErrorKey.Action.ID);
        }

        Integer checkUsed = offerVersionActionRepository.countByActionIdAndDomainId(actionId, domainId);
        if (checkUsed > 0) {
            throw new DataConstrainException(ErrorMessage.Action.CANNOT_DELETE, Resources.ACTION,
                ErrorKey.Action.ID);
        }
        Integer checkUsed1 = offerTemplateVersionActionRepository.countByActionIdAndDomainId(actionId, domainId);
        if (checkUsed1 > 0) {
            throw new DataConstrainException(ErrorMessage.Action.CANNOT_DELETE, Resources.ACTION,
                ErrorKey.Action.ID);
        }
        actionPriceComponentMapRepository.deleteByDomainIdAndParentId(domainId, actionId);
        actionRepository.deleteByIdAndDomainId(actionId, domainId);
    }

    public BaseResponseDTO createAction(ActionDTO actionDTO) throws ParseException {
        actionDTO.setId(null);
        Integer domainId = OCSUtils.getDomain();
        checkCategory(actionDTO.getCategoryId());
        boolean now = false;
        if (actionDTO.getEffDate() == null) {
            actionDTO.setEffDate(new Timestamp(System.currentTimeMillis()));
            now = true;
        }
        if (actionDTO.getExpDate() == null) {
//            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2100-12-31 23:59:59");
            LocalDateTime localDateTime = LocalDateTime.of(2100, 12, 31, 16, 59, 59, 0);
            Timestamp timestamp = Timestamp.valueOf(localDateTime);
            actionDTO.setExpDate(timestamp);
        }
        if (actionDTO.getEffDate().compareTo(actionDTO.getExpDate()) > 0) {
            throw new DataInvalidException(ErrorMessage.Action.TIME_VALIDATE, Resources.ACTION, ErrorKey.Action.EXP_DATE);
        }
        if (actionDTO.getState() == null) {
            actionDTO.setState(false);
        }
        checkValidateActionFourFields(actionDTO, domainId);
        Action action = actionMapper.toEntity(actionDTO);
        checkDuplicateName(action.getName(), action.getId());

        if (actionDTO.getActionPriceComponentMaps().isEmpty()) {
            throw new DataInvalidException(ErrorMessage.Action.MUST_HAVE_ATLEAST_ONE_CHILD, Resources.ACTION,
                ErrorKey.Action.PRICE_COMPONENT);
        }

        action.setDomainId(domainId);
        setPosIndex(domainId, action, action.getCategoryId());
        action = actionRepository.saveAndFlush(action);
        List<ActionPriceComponentMapDTO> actionPriceComponentMaps = new ArrayList<>();

        for (ActionPriceComponentMapDTO actionPriceComponentMapDTO : actionDTO.getActionPriceComponentMaps()) {

            PriceComponent priceComponent = priceComponentRepository.
                findByIdAndDomainId(actionPriceComponentMapDTO.getPriceComponentId(), domainId);
            if (priceComponent == null) {
                throw new ResourceNotFoundException(ErrorMessage.PriceComponent.NOT_FOUND, Resources.PRICE_COMPONENT, ErrorKey.ENTITY_NOT_FOUND);
            }
            ActionPriceComponentMapDTO actionPriceComponentMapDTO1 = new ActionPriceComponentMapDTO();
            actionPriceComponentMapDTO1.setActionId(action.getId());
            actionPriceComponentMapDTO1.setPriceComponentId(actionPriceComponentMapDTO.getPriceComponentId());
            actionPriceComponentMapDTO1.setPosIndex(actionPriceComponentMapDTO.getPosIndex());
            actionPriceComponentMapDTO1.setDomainId(domainId);

            actionPriceComponentMaps.add(actionPriceComponentMapDTO1);
        }
        actionPriceComponentMapRepository.saveAll(actionPriceComponentMapMapper.toEntity(actionPriceComponentMaps));
        actionDTO = actionMapper.toDto(action);
        actionDTO.setHasChild(true);
        return actionDTO;
    }

    private void checkValidateActionFourFields(ActionDTO actionDTO, Integer domainId) {
        ActionType actionType = actionTypeRepository.findByIdAndDomainId(actionDTO.getActionTypeId(), domainId);
        if (actionType == null) {
            throw new ResourceNotFoundException(ErrorMessage.ActionType.ID_NOT_FOUND, Resources.ACTION_TYPE, ErrorKey.ActionType.ACTION_TYPE_NAME);
        }
        if (actionDTO.getReserveInfoId() != null) {
            ReserveInfoDTO reserveInfoDTO = reserveInfoMapper.toDto(reserveInfoRepository.findByIdAndDomainId(actionDTO.getReserveInfoId(), domainId));
            if (reserveInfoDTO == null) {
                throw new ResourceNotFoundException(ErrorMessage.ReserveInfo.NOT_FOUND, Resources.ACTION, ErrorKey.Action.RESERVE_INFO_NAME);
            }
        }
        if (actionDTO.getDynamicReserveFilterId() != null) {
            RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(actionDTO.getDynamicReserveFilterId(), domainId);
            if (ratingFilter == null) {
                throw new ResourceNotFoundException(ErrorMessage.ActionType.DYNAMIC_RESERVE_FILTER_ID_NOT_FOUND, Resources.ACTION_TYPE, ErrorKey.ActionType.DYNAMIC_RESERVE_FILTER_NAME);
            }
        }
        if (actionDTO.getSortingFilterId() != null) {
            RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(actionDTO.getSortingFilterId(), domainId);
            if (ratingFilter == null) {
                throw new ResourceNotFoundException(ErrorMessage.ActionType.SORTING_PC_FILTER_ID_NOT_FOUND, Resources.ACTION_TYPE, ErrorKey.ActionType.SORTING_PC_FILTER_NAME);
            }
        }
        if (actionDTO.getPriorityFilterId() != null) {
            RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(actionDTO.getPriorityFilterId(), domainId);
            if (ratingFilter == null) {
                throw new ResourceNotFoundException(ErrorMessage.ActionType.PRIORITY_FILTER_ID_NOT_FOUND, Resources.ACTION_TYPE, ErrorKey.ActionType.PRIORITY_FILTER_NAME);
            }
        }
    }

    public BaseResponseDTO updateAction(ActionDTO actionDTO) {
        Integer domainId = OCSUtils.getDomain();
        Action actioncheck = actionRepository.findByIdAndDomainId(actionDTO.getId(), domainId);
        if (actioncheck == null) {
            throw new ResourceNotFoundException(ErrorMessage.Action.NOT_FOUND, Resources.ACTION, ErrorKey.Action.ID);
        }
        checkCategory(actionDTO.getCategoryId());
        if (actionDTO.getEffDate().compareTo(actionDTO.getExpDate()) > 0) {
            throw new DataInvalidException(ErrorMessage.Action.TIME_VALIDATE, Resources.ACTION, ErrorKey.Action.EXP_DATE);
        }
        Action action = actionMapper.toEntity(actionDTO);
        if (!Objects.equal(actionDTO.getCategoryId(), actioncheck.getCategoryId())) {
            setPosIndex(domainId, action, actionDTO.getCategoryId());
        }
        if (!Objects.equal(actionDTO.getName(), actioncheck.getName())) {
            checkDuplicateName(action.getName(), action.getId());
        }
        checkValidateActionFourFields(actionDTO, domainId);
        action = actionRepository.save(action);
        if (actionDTO.getActionPriceComponentMaps() != null) {
            actionPriceComponentMapService.editActionPriceComponentMap(actionDTO.getActionPriceComponentMaps(),
                actionDTO.getId());
        }
        actionDTO = actionMapper.toDto(action);
        actionDTO.setHasChild(true);
        return actionDTO;
    }

    public ActionDTO getActionDetail(Long actionId) {
        Integer domainId = OCSUtils.getDomain();
        Action actioncheck = actionRepository.findByIdAndDomainId(actionId, domainId);
        if (actioncheck == null) {
            throw new ResourceNotFoundException(ErrorMessage.Action.NOT_FOUND, Resources.ACTION, ErrorKey.Action.ID);
        }
        ActionDTO actionDTO = actionMapper.toDto(actioncheck);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(actionDTO.getCategoryId(), domainId);
        if (category != null) {
            actionDTO.setCategoryId(category.getId());
            actionDTO.setCategoryName(category.getName());
            actionDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        ActionTypeDTO actionTypeDTO = actiontypeMapper.toDto(actionTypeRepository.findByIdAndDomainId(actionDTO.getActionTypeId(), domainId));
        actionDTO.setActionTypeName(actionTypeDTO.getName());
        if (actionDTO.getReserveInfoId() != null) {
            ReserveInfoDTO reserveInfoDTO = reserveInfoMapper.toDto(reserveInfoRepository.findByIdAndDomainId(actionDTO.getReserveInfoId(), domainId));
            if (reserveInfoDTO == null) {
                throw new ResourceNotFoundException(ErrorMessage.ReserveInfo.NOT_FOUND, Resources.ACTION, ErrorKey.Action.RESERVE_INFO);
            }
            actionDTO.setReserveInfoName(reserveInfoDTO.getName());
        }
        if (actionDTO.getDynamicReserveFilterId() != null) {
            RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterRepository.findByIdAndDomainId(actionDTO.getDynamicReserveFilterId(), domainId));
            if (ratingFilterDTO == null) {
                throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.ACTION, ErrorKey.Action.RATING_FILTER);
            }
            actionDTO.setDynamicReserveFilterName(ratingFilterDTO.getName());
        }
        if (actionDTO.getSortingFilterId() != null) {
            RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterRepository.findByIdAndDomainId(actionDTO.getSortingFilterId(), domainId));
            if (ratingFilterDTO == null) {
                throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.ACTION, ErrorKey.Action.RATING_FILTER);
            }
            actionDTO.setSortingFilterName(ratingFilterDTO.getName());
        }
        if (actionDTO.getPriorityFilterId() != null) {
            RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterRepository.findByIdAndDomainId(actionDTO.getPriorityFilterId(), domainId));
            if (ratingFilterDTO == null) {
                throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.ACTION, ErrorKey.Action.RATING_FILTER);
            }
            actionDTO.setPriorityFilterName(ratingFilterDTO.getName());
        }
        Pageable unPaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC,
            ActionPriceComponentMap.FieldNames.POS_INDEX));

        List<ActionPriceComponentMapDTO> actionPriceComponentMapDTOS = actionPriceComponentMapMapper.toDto(actionPriceComponentMapRepository.findByDomainIdAndParentId(domainId, actionId, unPaged));

        List<Long> priceComponentIds = actionPriceComponentMapDTOS.stream().map(ActionPriceComponentMapDTO::getPriceComponentId).collect(Collectors.toList());

        List<PriceComponent> priceComponents = priceComponentRepository.findByDomainIdAndIdIn(domainId, priceComponentIds);

        Map<Long, PriceComponent> priceComponentMap = priceComponents.stream().collect(Collectors.toMap(PriceComponent::getId, priceComponent -> priceComponent));

        for (ActionPriceComponentMapDTO actionPriceComponentMapDTO : actionPriceComponentMapDTOS) {
            PriceComponent priceComponent = priceComponentMap.get(actionPriceComponentMapDTO.getPriceComponentId());
            actionPriceComponentMapDTO.setName(priceComponent.getName());
            actionPriceComponentMapDTO.setDescription(priceComponent.getDescription());
            actionPriceComponentMapDTO.setPosIndex(actionPriceComponentMapDTO.getPosIndex());
        }
        actionDTO.setActionPriceComponentMaps(actionPriceComponentMapDTOS);
        return actionDTO;
    }

    private boolean checkHasChild(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferWithPccRule = actionPriceComponentMapRepository.countByDomainIdAndParentId(domainId, id);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    public Page<ActionDTO> getActionWithPage(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        Page<ActionDTO> actionDTOS = actionRepository.getActions(categoryId, domainId, name, description, pageable)
            .map(actionMapper::toDto);
        for (ActionDTO actionDTO : actionDTOS) {
            actionDTO.setCategoryName(category.getName());
            actionDTO.setCategoryLevel(parentCategoryLevel);
            actionDTO.setHasChild(checkHasChild(actionDTO.getId(), domainId));
        }
        return actionDTOS;
    }

    public Page<ActionDTO> getActions(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }

        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;

        Page<ActionDTO> actionDTOS = actionRepository.getActions(searchCriteria.getParentId(),
            searchCriteria.getDomainId(),
            searchCriteria.getName(),
            searchCriteria.getDescription(), pageable).map(actionMapper::toDto);

        for (ActionDTO actionDTO : actionDTOS) {
            actionDTO.setCategoryName(category.getName());
            actionDTO.setCategoryLevel(parentCategoryLevel);
            actionDTO.setHasChild(checkHasChild(actionDTO.getId(), domainId));
        }
        return actionDTOS;
    }

    public List<PriceComponentDTO> getPriceComponentByActionId(Long actionId) {
        Integer domainId = OCSUtils.getDomain();
        Action action = actionRepository.findByIdAndDomainId(actionId, domainId);
        if (action == null) {
            throw new ResourceNotFoundException(ErrorMessage.Action.NOT_FOUND, Resources.ACTION,
                ErrorKey.Action.ID);
        }
        Pageable unPaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC,
            ActionPriceComponentMap.FieldNames.POS_INDEX));

        List<ActionPriceComponentMap> actionPriceComponentMaps =
            actionPriceComponentMapRepository.findByDomainIdAndParentId(domainId, actionId, unPaged);
        if (actionPriceComponentMaps == null) {
            throw new ResourceNotFoundException(ErrorMessage.ActionPriceComponentMap.NOT_FOUND,
                Resources.ACTION_PRICE_COMPONENT_MAP, ErrorKey.ActionPriceComponentMap.ID);
        }

        List<PriceComponentDTO> priceComponentDTOS = new ArrayList<>();

        List<ActionPriceComponentMapDTO> actionPriceComponentMapDTOS =
            actionPriceComponentMapMapper.toDto(actionPriceComponentMaps);

        List<Long> priceComponentIds = actionPriceComponentMapDTOS.stream().map(ActionPriceComponentMapDTO::getPriceComponentId).collect(Collectors.toList());

        List<PriceComponent> priceComponents = priceComponentRepository.findByDomainIdAndIdIn(domainId, priceComponentIds);
        Map<Long, ActionPriceComponentMapDTO> actionPriceComponentMapDTOMap =
            actionPriceComponentMapDTOS.stream().collect(Collectors.toMap(ActionPriceComponentMapDTO::getPriceComponentId, actionPriceComponentMapDTO -> actionPriceComponentMapDTO));
        Map<Long, Integer> priceComponentCountChildMap = priceComponentBlockMapService.countChildrenByParentMap(priceComponentIds, domainId);

        for (PriceComponent priceComponent : priceComponents) {
            PriceComponentDTO priceComponentDTO = priceComponentMapper.toDto(priceComponent);
            priceComponentDTO.setHasChild(checkHasChildOfChildren(priceComponentCountChildMap, priceComponent.getId()));
            priceComponentDTO.setPosIndex(actionPriceComponentMapDTOMap.get(priceComponent.getId()).getPosIndex());
            priceComponentDTO.setParentId(actionId);
            priceComponentDTOS.add(priceComponentDTO);
        }
        priceComponentDTOS.sort(Comparator.comparing(PriceComponentDTO::getPosIndex));
        return priceComponentDTOS;
    }

    public List<TreeClone> getActionInTree(Long actionId) {
        Integer domainId = OCSUtils.getDomain();
        Action actionDB = actionRepository.findByIdAndDomainId(actionId, domainId);
        if (actionDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.Action.NOT_FOUND, Resources.ACTION, ErrorKey.Action.ID);
        }
        List<TreeClone> results = new ArrayList<>();
        ActionDTO actionDTO = actionMapper.toDto(actionDB);

        ActionRatingFilterDTO actionRatingFilterDTO1 = new ActionRatingFilterDTO();
        actionRatingFilterDTO1.setId(actionId);
        actionRatingFilterDTO1.setName("PC List");
        actionRatingFilterDTO1.setType(Resources.ACTION_PC_LIST);
        actionRatingFilterDTO1.setHasChild(true);
        results.add(actionRatingFilterDTO1);

        if (actionDTO.getDynamicReserveFilterId() != null) {
            ActionRatingFilterDTO actionRatingFilterDTO = new ActionRatingFilterDTO();
            actionRatingFilterDTO.setId(actionDTO.getDynamicReserveFilterId());
            actionRatingFilterDTO.setName("Dynamic Reverve");
            actionRatingFilterDTO.setType(Resources.ACTION_DYNAMIC_REVERVE);
            actionRatingFilterDTO.setHasChild(true);
            results.add(actionRatingFilterDTO);
        }
        if (actionDTO.getSortingFilterId() != null) {
            ActionRatingFilterDTO actionRatingFilterDTO = new ActionRatingFilterDTO();
            actionRatingFilterDTO.setId(actionDTO.getSortingFilterId());
            actionRatingFilterDTO.setName("Sorting PC Filter");
            actionRatingFilterDTO.setType(Resources.ACTION_SORTING_PC_FILTER);
            actionRatingFilterDTO.setHasChild(true);
            results.add(actionRatingFilterDTO);
        }
        if (actionDTO.getPriorityFilterId() != null) {
            ActionRatingFilterDTO actionRatingFilterDTO = new ActionRatingFilterDTO();
            actionRatingFilterDTO.setId(actionDTO.getPriorityFilterId());
            actionRatingFilterDTO.setName("Priority Filter");
            actionRatingFilterDTO.setType(Resources.ACTION_PRIORITY_FILTER);
            actionRatingFilterDTO.setHasChild(true);
            results.add(actionRatingFilterDTO);
        }
        return results;
    }

    public List<RatingFilterDTO> getRatingFilter(Long ratingFilterId) {
        Integer domainId = OCSUtils.getDomain();
        List<RatingFilterDTO> ratingFilterDTOS = new ArrayList<>();
        RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterRepository.findByIdAndDomainId(ratingFilterId, domainId));
        if (ratingFilterDTO == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }
        ratingFilterDTO.setHasChild(checkHasChildRatingFilter(ratingFilterId, domainId));
        ratingFilterDTOS.add(ratingFilterDTO);
        return ratingFilterDTOS;
    }

    private boolean checkHasChildRatingFilter(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferWithPccRule = ratingFilterRateTableMapRepository.countByRatingFilterIdAndDomainId(id, domainId);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    public List getListRatingFilterIds(List<Long> ids, Integer domainId) {
        List<Action> actions = actionRepository.findByDomainIdAndIdIn(domainId, ids);
        List<Long> childIds = new ArrayList<>();
        for (Action action : actions) {
            if (action.getDynamicReserveFilterId() != null) {
                childIds.add(action.getDynamicReserveFilterId());
            }
            if (action.getPriorityFilterId() != null) {
                childIds.add(action.getPriorityFilterId());
            }
            if (action.getSortingFilterId() != null) {
                childIds.add(action.getSortingFilterId());
            }
        }
        return childIds;
    }

    @Override
    protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        if (CategoryType.RATING_FILTER.equals(categoryType)) {
            return actionRepository.findActionByRatingFilterIds(ids);
        }
        return actionRepository.findByIdInAndDomainId(ids, domainId);

    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Action.DUPLICATED_NAME, Resources.ACTION, ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.Action.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }
}
