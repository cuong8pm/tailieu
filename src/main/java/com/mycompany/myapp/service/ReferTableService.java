package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.constant.ReferType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.repository.ReferTableRepository;

@Service
public class ReferTableService {

    @Autowired
    private ReferTableRepository referTableRepository;

    public List<ReferTable> getReferTables(Integer type) {
        return referTableRepository.findByReferTypeOrderByValueAsc(ReferType.of(type));
    }
    
    public List<ReferTable> getReferTables(ReferType referType) {
        return referTableRepository.findByReferTypeOrderByValueAsc(referType);
    }
    
    public List<ReferTable> findReferTables(ReferType referType) {
        return referTableRepository.findByReferTypeOrderByValueAsc(referType);
    }

}
