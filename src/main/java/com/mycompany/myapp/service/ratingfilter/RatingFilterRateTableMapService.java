package com.mycompany.myapp.service.ratingfilter;

import javax.annotation.PostConstruct;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.RatingFilterRateTableMap;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.ratetable.RateTableColumnDTO;
import com.mycompany.myapp.dto.ratingfilter.RateTableRequest;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.utils.OCSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.ratingfilter.RatingFilterRateTableMapDTO;
import com.mycompany.myapp.mapper.ratingfilter.RatingFilterRateTableMapMapper;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRateTableMapRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.clone.RateTableParentMappingService;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.ratetable.RateTableService;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Order(value = 1)
public class RatingFilterRateTableMapService extends AbstractCloneMappingService implements RateTableParentMappingService{

    @Autowired
    private RatingFilterRateTableMapRepository ratingFilterRateTableMapRepository;

    @Autowired
    private RatingFilterRateTableMapMapper ratingFilterRateTableMapMapper;

    @Autowired
    private RateTableService rateTableService;

    @Autowired
    private RatingFilterService ratingFilterService;

    @Autowired
    private RateTableRepository rateTableRepository;

    /**
     * @param id RatingFilterRateTableMap
     * @return
     */
    @Transactional(readOnly = true)
    public RatingFilterRateTableMapDTO getRatingFilterRateTableMapDetail(Long id) {
        return ratingFilterRateTableMapRepository.findById(id)
            .map(ratingFilterRateTableMapMapper::toDto)
            .orElseThrow(() -> new ResourceNotFoundException(ErrorMessage.RatingFilterRateTableMap.RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND, Resources.RATING_FILTER_RATE_TABLE_MAP, ErrorKey.RatingFilterRateTableMap.ID));
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = ratingFilterRateTableMapRepository;
        this.childService = rateTableService;
        this.parentService = ratingFilterService;
        this.parentDependService = ratingFilterService;
    }

    public void deleteRatingFilterRateTableMap(Long id) {
        Integer domainId = OCSUtils.getDomain();
        RatingFilterRateTableMap ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByIdAndDomainId(id, domainId);
        if (ratingFilterRateTableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilterRateTableMap.RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND, Resources.RATING_FILTER_RATE_TABLE_MAP, ErrorKey.RatingFilterRateTableMap.ID);
        }

        ratingFilterRateTableMapRepository.deleteByIdAndDomainId(id, domainId);
    }

    public void deleteRateTableInRatingFilter(Long ratingFilterId, Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
//        RatingFilterRateTableMap ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByDomainIdAndChildIdAndParentId(domainId, rateTableId, ratingFilterId);
//        if (ratingFilterRateTableMap == null) {
//            throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, getResource(), ErrorKey.RateTable.ID);
//        }

//        Integer countByParentId = ratingFilterRateTableMapRepository.countByDomainIdAndParentId(domainId, ratingFilterId);
//
//        if (countByParentId.intValue() <= 1) {
//            throw new DataConstrainException(ErrorMessage.MUST_HAVE_ATLEAST_ONE_CHILD, getResource(), ErrorKey.RatingFilter.RATE_TABLE);
//        }

        ratingFilterRateTableMapRepository.deleteByDomainIdAndChildIdAndParentId(domainId, rateTableId, ratingFilterId);
    }

    public void updateRatingFilterRateTableMap(List<RateTableRequest> rateTableRequests, Long ratingFilterId) {
        Integer domainId = OCSUtils.getDomain();
        List<RateTableRequest> removeDeletes = new ArrayList<>();
        Integer add = 0;
        for (RateTableRequest item : rateTableRequests) {
            if (ActionType.ADD.equals(item.getActionType())) {
                add = add + 1;
            }
        }
        for (RateTableRequest item : rateTableRequests) {
            if (ActionType.DELETE.equals(item.getActionType())) {
                Integer countByParentId = ratingFilterRateTableMapRepository.countByDomainIdAndParentId(domainId, ratingFilterId);
                if (countByParentId.intValue() <= 1 && add < 1) {
                    throw new DataConstrainException(ErrorMessage.MUST_HAVE_ATLEAST_ONE_CHILD, getResource(), ErrorKey.RatingFilter.RATE_TABLE);
                }
                deleteRateTableInRatingFilter(ratingFilterId, item.getId());
                removeDeletes.add(item);
            }
        }
        rateTableRequests.removeAll(removeDeletes);
        removeDeletes = new ArrayList<>();
        for (RateTableRequest item : rateTableRequests) {
            if (ActionType.EDIT.equals(item.getActionType())) {
                RateTable rateTable = rateTableRepository.findByIdAndDomainId(item.getId(), domainId);
                if(rateTable == null){
                    throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.ENTITY_NOT_FOUND);
                }
                RatingFilterRateTableMap ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByIdAndDomainId(item.getRatingFilterRateTableMapId(), domainId);
                if (ratingFilterRateTableMap == null) {
                    throw new ResourceNotFoundException(ErrorMessage.RatingFilterRateTableMap.RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND, Resources.RATING_FILTER_RATE_TABLE_MAP, ErrorKey.RatingFilterRateTableMap.ID);
                }
                ratingFilterRateTableMap.setRateTableId(item.getId());
                ratingFilterRateTableMap.setPosIndex(item.getIndex());
                ratingFilterRateTableMapRepository.save(ratingFilterRateTableMap);
                removeDeletes.add(item);
            }
        }
        rateTableRequests.removeAll(removeDeletes);
        removeDeletes = new ArrayList<>();
        List<Long> rateTableIds = rateTableRequests.stream().filter(rateTableRequest -> ActionType.ADD.equals(rateTableRequest.getActionType())).map(RateTableRequest::getId).collect(Collectors.toList());
        List<RateTable> rateTables = rateTableRepository.findByDomainIdAndIdIn(domainId,rateTableIds);
        for (RateTableRequest item : rateTableRequests) {
            if (ActionType.ADD.equals(item.getActionType())) {
                if(rateTables.stream().noneMatch(rateTable -> Objects.equal(rateTable.getId(),item.getId()))){
                    throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.ENTITY_NOT_FOUND);
                }
                RatingFilterRateTableMap ratingFilterRateTableMap = new RatingFilterRateTableMap();
                ratingFilterRateTableMap.setRateTableId(item.getId());
                ratingFilterRateTableMap.setDomainId(domainId);
                ratingFilterRateTableMap.setPosIndex(item.getIndex());
                ratingFilterRateTableMap.setRatingFilterId(ratingFilterId);
                ratingFilterRateTableMapRepository.save(ratingFilterRateTableMap);
                removeDeletes.add(item);
            }
        }
        rateTableRequests.removeAll(removeDeletes);
    }

    @Override
    protected String getResource() {
        return Resources.RATE_TABLE;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
