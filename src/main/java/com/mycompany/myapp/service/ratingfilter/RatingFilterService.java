package com.mycompany.myapp.service.ratingfilter;

import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.repository.action.ActionRepository;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.repository.event.EventRepository;
import com.mycompany.myapp.service.clone.RatingFilterParentService;
import com.mycompany.myapp.service.ratetable.RateTableColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import com.mycompany.myapp.domain.RatingFilter;
import com.mycompany.myapp.domain.RatingFilterRateTableMap;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratingfilter.RateTableProjection;
import com.mycompany.myapp.dto.ratingfilter.RateTableRequest;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterRateTableMapDTO;
import com.mycompany.myapp.mapper.ratetable.RateTableMapper;
import com.mycompany.myapp.mapper.ratingfilter.RatingFilterMapper;
import com.mycompany.myapp.mapper.ratingfilter.RatingFilterRateTableMapMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.ratetable.RateTableColumnsRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRateTableMapRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.action.ActionService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.ratetable.RateTableService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.util.BooleanUtils;

@Service
@Transactional
public class RatingFilterService extends AbstractCloneService {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RatingFilterRepository ratingFilterRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private RatingFilterMapper ratingFilterMapper;
    @Autowired
    private RatingFilterRateTableMapRepository ratingFilterRateTableMapRepository;
    @Autowired
    private ReferTableRepository referTableRepository;
    @Autowired
    private List<RatingFilterRateTableMapService> ratingFilterRateTableMapService;
    @Autowired
    private RatingFilterRateTableMapMapper ratingFilterRateTableMapMapper;
    @Autowired
    private RateTableService rateTableService;
    @Autowired
    private RatingFilterRateTableMapService ratingFilterRateTableMapService1;
    @Autowired
    private RateTableColumnService rateTableColumnService;
    @Autowired
    private RateTableRepository rateTableRepository;
    @Autowired
    private RateTableMapper rateTableMapper;
    @Autowired
    private BlockRepository blockRepository;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ActionRepository actionRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private List<RatingFilterParentService> blockService;
    @Autowired
    private ActionService actionService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = ratingFilterRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = ratingFilterRepository;
        this.cloneMappingServices = ratingFilterRateTableMapService;
        this.parentDependServices = blockService;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return RatingFilter.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.RATING_FILTER;
    }

    @Override
    protected String getNameCategory() {
        return "Rating Filters";
    }

    public List<RatingFilterDTO> getAllRatingFilter() {
        return ratingFilterRepository.findAll().stream().map(ratingFilterMapper::toDto).collect(Collectors.toList());
    }

    public List<ReferTable> getRatingFilterType() {
        return referTableRepository.getReferTablesByReferType(ReferType.RatingFilter, sortByValue());
    }

    public RatingFilterDTO createRatingFilter(RatingFilterDTO ratingFilterDTO) {
        ratingFilterDTO.setId(null);
        Integer domainId = OCSUtils.getDomain();
        checkCategory(ratingFilterDTO.getCategoryId());
        RatingFilter ratingFilter = ratingFilterMapper.toEntity(ratingFilterDTO);
        checkDuplicateName(ratingFilter.getName(), ratingFilter.getId());
        if (ratingFilterDTO.getRateTableInRatingFilter().isEmpty()) {
            throw new DataInvalidException(ErrorMessage.MUST_HAVE_ATLEAST_ONE_CHILD, Resources.RATING_FILTER, ErrorKey.RatingFilter.RATE_TABLE);
        }
        ratingFilter.setDomainId(domainId);
        setPosIndex(domainId, ratingFilter, ratingFilter.getCategoryId());
        ratingFilter = ratingFilterRepository.saveAndFlush(ratingFilter);
        List<RatingFilterRateTableMap> ratingFilterRateTableMaps = new ArrayList<>();
        List<Long> rateTableIds = ratingFilterDTO.getRateTableInRatingFilter().stream().map(RateTableRequest::getId).collect(Collectors.toList());
        List<RateTable> rateTables = rateTableRepository.findByDomainIdAndIdIn(domainId,rateTableIds);
        for (RateTableRequest rateTableRequest : ratingFilterDTO.getRateTableInRatingFilter()) {
            if(rateTables.stream().noneMatch(rateTable -> Objects.equal(rateTable.getId(),rateTableRequest.getId()))){
                throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.ENTITY_NOT_FOUND);
            }
            RatingFilterRateTableMap ratingFilterRateTableMap = new RatingFilterRateTableMap();
            ratingFilterRateTableMap.setDomainId(domainId);
            ratingFilterRateTableMap.setRateTableId(rateTableRequest.getId());
            ratingFilterRateTableMap.setRatingFilterId(ratingFilter.getId());
            ratingFilterRateTableMap.setPosIndex(rateTableRequest.getIndex());
            ratingFilterRateTableMaps.add(ratingFilterRateTableMap);
        }
        ratingFilterRateTableMapRepository.saveAll(ratingFilterRateTableMaps);
        ratingFilterDTO = ratingFilterMapper.toDto(ratingFilter);
        ratingFilterDTO.setHasChild(true);
        return ratingFilterDTO;
    }

    public RatingFilterDTO getRatingFilterDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        RatingFilter ratingFilterDB = ratingFilterRepository.findByIdAndDomainId(id, domainId);
        if (ratingFilterDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }
        List<RatingFilterRateTableMap> ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByRatingFilterIdAndDomainId(id, domainId);
        if (ratingFilterRateTableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilterRateTableMap.RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND, Resources.RATING_FILTER_RATE_TABLE_MAP, ErrorKey.RatingFilterRateTableMap.ID);
        }

        RatingFilterDTO ratingFilterDTO = ratingFilterMapper.toDto(ratingFilterDB);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(ratingFilterDB.getCategoryId(), domainId);
        if (category != null) {
            ratingFilterDTO.setCategoryId(category.getId());
            ratingFilterDTO.setCategoryName(category.getName());
            ratingFilterDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        List<RateTableProjection> rateTables = ratingFilterRateTableMapRepository.getRateTable(ratingFilterDTO.getId(), domainId);
        ratingFilterDTO.setRateTables(rateTables);
        return ratingFilterDTO;
    }

    public void deleteRatingFilter(Long id) {
        Integer domainId = OCSUtils.getDomain();
        // TODO -- check refer
        RatingFilter ratingFilter = ratingFilterRepository.findByIdAndDomainId(id, domainId);
        if (ratingFilter == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }
        checkUsedDeleteRatingFilter(id, domainId);
        ratingFilterRateTableMapRepository.deleteByRatingFilterIdAndDomainId(id, domainId);
        ratingFilterRepository.deleteByIdAndDomainId(id, domainId);
    }

    @Override
    public TreeClone getTreeDTO() {
        return new RatingFilterDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.RATING_FILTER;
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.RATE_TABLE, CategoryType.NORMALIZER);
    }

    public List<RateTableDTO> getRateTableByRatingFilterId(Long ratingFilterId) {
        Integer domainId = OCSUtils.getDomain();
        RatingFilter ratingFilterDB = ratingFilterRepository.findByIdAndDomainId(ratingFilterId, domainId);
        if (ratingFilterDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Direction.ASC, RatingFilterRateTableMap.FieldNames.POS_INDEX));

        List<RatingFilterRateTableMap> ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByDomainIdAndParentId(domainId, ratingFilterId, unpaged);

        if (ratingFilterRateTableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilterRateTableMap.RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND, Resources.RATING_FILTER_RATE_TABLE_MAP, ErrorKey.RatingFilterRateTableMap.ID);
        }
        List<RateTableDTO> rateTableDTOS = new ArrayList<>();

        List<RatingFilterRateTableMapDTO> ratingFilterRateTableMapDTOS = ratingFilterRateTableMapMapper.toDto(ratingFilterRateTableMap);

        List<Long> rateTableIds = ratingFilterRateTableMapDTOS.stream().map(RatingFilterRateTableMapDTO::getRateTableId).collect(Collectors.toList());

        List<RateTable> rateTables = rateTableRepository.findByDomainIdAndIdIn(domainId,rateTableIds);

        Map<Long,RatingFilterRateTableMapDTO> ratingFilterRateTableMapDTOMap = ratingFilterRateTableMapDTOS.stream().collect(Collectors.toMap(RatingFilterRateTableMapDTO::getRateTableId,ratingFilterRateTableMapDTO -> ratingFilterRateTableMapDTO));

        Map<Long,Integer> childrenRateTableMap = rateTableColumnService.countChildrenByParentMap(rateTableIds,domainId);

        for (RateTable rateTable : rateTables) {
            RateTableDTO rateTableDTO = rateTableMapper.toDto(rateTable);
            rateTableDTO.setHasChild(checkHasChildOfChildren(childrenRateTableMap,rateTable.getId()));
            rateTableDTO.setPosIndex(ratingFilterRateTableMapDTOMap.get(rateTable.getId()).getIndex());
            rateTableDTO.setParentId(ratingFilterId);
            rateTableDTOS.add(rateTableDTO);
        }
        rateTableDTOS.sort(Comparator.comparing(RateTableDTO::getPosIndex));
        return rateTableDTOS;
    }

    public RatingFilterDTO editRatingFilter(RatingFilterDTO ratingFilterDTO) {
        Integer domainId = OCSUtils.getDomain();
        RatingFilter ratingFilterDB = ratingFilterRepository.findByIdAndDomainId(ratingFilterDTO.getId(), domainId);
        if (ratingFilterDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilter.RATING_FILTER_NOT_FOUND, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }
        List<RatingFilterRateTableMap> ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByRatingFilterIdAndDomainId(ratingFilterDTO.getId(), domainId);
        if (ratingFilterRateTableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RatingFilterRateTableMap.RATING_FILTER_RATE_TABLE_MAP_NOT_FOUND, Resources.RATING_FILTER_RATE_TABLE_MAP, ErrorKey.RatingFilterRateTableMap.ID);
        }
        checkCategory(ratingFilterDTO.getCategoryId());
        RatingFilter ratingFilter = ratingFilterMapper.toEntity(ratingFilterDTO);
        if (!Objects.equal(ratingFilterDTO.getCategoryId(), ratingFilterDB.getCategoryId())) {
            setPosIndex(domainId, ratingFilter, ratingFilterDTO.getCategoryId());
        }
        if (!Objects.equal(ratingFilterDTO.getName(), ratingFilterDB.getName())) {
            checkDuplicateName(ratingFilter.getName(), ratingFilter.getId());
        }

        Integer blockFilter = blockRepository.countByBlockFilterIdAndDomainId(ratingFilter.getId(), domainId);
        Integer checkAction = actionRepository.checkUsedRatingFilter(ratingFilter.getId(), domainId);
        Integer checkEvent = eventRepository.checkUsedRatingFilter(ratingFilter.getId(), domainId);

        if (checkAction > 0 || blockFilter > 0 || checkEvent > 0) {
            if (!Objects.equal(ratingFilterDTO.getRatingFilterType(), ratingFilterDB.getRatingFilterType())){
                throw new DataConstrainException(ErrorMessage.RatingFilter.CANNOT_DELETE, Resources.RATING_FILTER, ErrorKey.RatingFilter.TYPE);
            }
        }
        ratingFilter = ratingFilterRepository.save(ratingFilter);
        if (ratingFilterDTO.getRateTableInRatingFilter() != null) {
            ratingFilterRateTableMapService1.updateRatingFilterRateTableMap(ratingFilterDTO.getRateTableInRatingFilter(), ratingFilter.getId());
        }
        ratingFilterDTO = ratingFilterMapper.toDto(ratingFilter);
        ratingFilterDTO.setHasChild(true);
        return ratingFilterDTO;
    }

    public Page<RatingFilterDTO> getRatingFilters(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }

        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;

        Page<RatingFilterDTO> ratingFilterDTOS = ratingFilterRepository.findRatingFilterWithPage(
            searchCriteria.getParentId(),
            searchCriteria.getDomainId(),
            searchCriteria.getName(),
            searchCriteria.getDescription(), pageable).map(ratingFilterMapper::toDto);

        for (RatingFilterDTO ratingFilterDTO : ratingFilterDTOS) {
            ratingFilterDTO.setHasChild(checkHasChild(ratingFilterDTO.getId(), domainId));
            ratingFilterDTO.setCategoryLevel(parentCategoryLevel + 1);
        }
        return ratingFilterDTOS;
    }

    private boolean checkHasChild(@NotNull @Positive Long id, Integer domainId) {
        Integer countReferWithPccRule = ratingFilterRateTableMapRepository.countByRatingFilterIdAndDomainId(id, domainId);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    public Page<RatingFilterDTO> getRatingFilterWithPage(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        Page<RatingFilterDTO> ratingFilterDTOS = ratingFilterRepository.findRatingFilterWithPage(categoryId, domainId, name, description, pageable).map(ratingFilterMapper::toDto);
        for (RatingFilterDTO ratingFilterDTO : ratingFilterDTOS) {
            ratingFilterDTO.setCategoryLevel(parentCategoryLevel);
            ratingFilterDTO.setCategoryName(category.getName());
            ratingFilterDTO.setHasChild(checkHasChild(ratingFilterDTO.getId(), domainId));
        }
        return ratingFilterDTOS;
    }

    public void deleteRateTableInRatingFilter(Long ratingFilterId, Long rateTableId) {
        Integer domainId = OCSUtils.getDomain();
        RatingFilterRateTableMap ratingFilterRateTableMap = ratingFilterRateTableMapRepository.findByRateTableIdAndRatingFilterIdAndDomainId(rateTableId, ratingFilterId, domainId);
        if (ratingFilterRateTableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.RateTable.ID);
        }
        ratingFilterRateTableMapRepository.deleteByRateTableIdAndRatingFilterIdAndDomainId(rateTableId, ratingFilterId, domainId);
    }

    public void checkUsedDeleteRatingFilter(Long ratingFilterId, Integer domainId) {
        Integer blockFilter = blockRepository.countByBlockFilterIdAndDomainId(ratingFilterId, domainId);
        if (blockFilter > 0) {
            throw new DataConstrainException(ErrorMessage.RatingFilter.CANNOT_DELETE, Resources.RATING_FILTER, ErrorKey.RatingFilter.ID);
        }
        Integer checkAction = actionRepository.checkUsedRatingFilter(ratingFilterId, domainId);
        if (checkAction > 0) {
            throw new DataConstrainException(ErrorMessage.RatingFilter.CANNOT_DELETE, Resources.RATING_FILTER, ErrorKey.ID);
        }
        Integer checkEvent = eventRepository.checkUsedRatingFilter(ratingFilterId, domainId);
        if (checkEvent > 0) {
            throw new DataConstrainException(ErrorMessage.RatingFilter.CANNOT_DELETE, Resources.RATING_FILTER, ErrorKey.ID);
        }
    }

	@Override
	protected List<? extends OCSMoveableEntity> findBaseEntity(String name, Integer domainId, CategoryType categoryType,
                List<Long> removeIds, Map<String, Object> params, Long type) {
		Sort sort = (Sort) params.get(Keys.SORT);
		if (params.get(Keys.TYPE) != null) {
			Integer ratingFilterType = (Integer) params.get(Keys.TYPE);
			return ratingFilterRepository.findByNameContainingAndDomainIdAndRatingFilterType(name, domainId, ratingFilterType, sort);
		} else {
                    return super.findBaseEntity(name, domainId, categoryType, removeIds, params, null);
		}

	}

    @Override
    public List getChildEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        switch (categoryType) {
        case BLOCK:
            return ratingFilterRepository.findRatingFiltersByBlockIds(ids);
        case ACTION:
            ids = actionService.getListRatingFilterIds(ids, domainId);
            return ratingFilterRepository.findRatingFiltersByActionIds(ids);
        default:
            return null;
        }

    }

	@Override
    protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        return ratingFilterRepository.findByIdInAndDomainId(ids, domainId);
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.RatingFilter.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.RatingFilter.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }
}
