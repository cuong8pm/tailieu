package com.mycompany.myapp.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.StateType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.StateGroupsDTO;
import com.mycompany.myapp.dto.StateTypeDTO;
import com.mycompany.myapp.mapper.StateTypeMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.StateGroupsRepository;
import com.mycompany.myapp.repository.StateTypeRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class StateTypeService extends OCSBaseService {
    @Autowired
    private StateTypeRepository stateTypeRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private StateGroupsService stateGroupsService;
    @Autowired
    private StateTypeMapper stateTypeMapper;
    @Autowired
    private ReferTableRepository referTableRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private StateGroupsRepository stateGroupsRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = stateTypeRepository;
        this.parentService = categoryService;
    }
    
    public Page<StateTypeDTO> getStateType(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        return stateTypeRepository.findStateType(categoryId, domainId, name, description, pageable).map(stateTypeMapper::toDto);
    }

    public StateTypeDTO getStateTypeDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        StateType stateType = stateTypeRepository.findByIdAndDomainId(id, domainId);
        if (stateType == null) {
            throw new ResourceNotFoundException(ErrorMessage.StateSet.STATE_TYPE_NOT_FOUND, Resources.STATETYPE, ErrorKey.StateSet.ID);
        }

        StateTypeDTO stateTypeDTO = stateTypeMapper.toDto(stateType);
        Category category = categoryRepository.findByIdAndDomainId(stateTypeDTO.getCategoryId(), domainId);
        stateTypeDTO.setCategoryName(category.getName());
        stateTypeDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        StateGroupsDTO stateGroupsDTO = stateGroupsService.getStateGroupsDetail(stateTypeDTO.getStateGroupId());
        stateTypeDTO.setStateGroupName(stateGroupsDTO.getName());
        ReferTable referTable = referTableRepository.getReferTableByReferTypeAndValue(ReferType.StateType, stateTypeDTO.getStateType());
        if (referTable == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReferTable.NOT_FOUND, Resources.STATETYPE, ErrorKey.StateSet.STATE_TYPE);
        }
        stateTypeDTO.setStateTypeName(referTable.getName());
        return stateTypeDTO;
    }

    public void deleteStateType(Long id) {
        Integer domainId = OCSUtils.getDomain();
        stateTypeRepository.deleteByIdAndDomainId(id, domainId);
    }

    public StateTypeDTO createStateType(StateTypeDTO stateTypeDTO) {
        Integer domainId = OCSUtils.getDomain();

        StateType stateType = stateTypeMapper.toEntity(stateTypeDTO);
        stateType.setDomainId(domainId);
        checkCategoryExist(stateTypeDTO, domainId);
        categoryRepository.findByIdAndDomainIdForUpdate(stateTypeDTO.getParentId(), domainId);
        checkDuplicateName(stateTypeDTO.getName(), stateTypeDTO.getId());
        checkStateGroupExist(stateTypeDTO);
        stateGroupsRepository.findByIdAndDomainIdForUpdate(stateTypeDTO.getStateGroupId(), domainId);
        checkReferTableExist(stateTypeDTO);
        super.setPosIndex(domainId, stateType, stateTypeDTO.getCategoryId());

        stateType = stateTypeRepository.save(stateType);
        return stateTypeMapper.toDto(stateType);
    }

    private void checkReferTableExist(StateTypeDTO stateTypeDTO) {
        ReferTable referTable = referTableRepository.getReferTableByReferTypeAndValue(ReferType.StateType, stateTypeDTO.getStateType());
        if (referTable == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReferTable.NOT_FOUND, Resources.STATETYPE, ErrorKey.StateSet.STATE_TYPE);
        }
    }

    public StateTypeDTO updateStateType(StateTypeDTO stateTypeDTO) {
        Integer domainId = OCSUtils.getDomain();

        StateType stateTypeDB = stateTypeRepository.findByIdAndDomainId(stateTypeDTO.getId(), domainId);
        if (stateTypeDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.StateSet.STATE_TYPE_NOT_FOUND, Resources.STATETYPE, ErrorKey.StateSet.ID);
        }
        checkCategoryExist(stateTypeDTO, domainId);
        checkStateGroupExist(stateTypeDTO);
        checkDuplicateName(stateTypeDTO.getName(), stateTypeDTO.getId());
        checkReferTableExist(stateTypeDTO);

        StateType stateType = stateTypeMapper.toEntity(stateTypeDTO);
        stateType.setDomainId(domainId);
        
        if (!Objects.equal(stateTypeDTO.getCategoryId(), stateTypeDB.getCategoryId())) {
            setPosIndex(domainId, stateType, stateTypeDTO.getCategoryId());
        }
        
        stateType = stateTypeRepository.save(stateType);

        return stateTypeMapper.toDto(stateType);
    }

    // Check category exist or not
    private void checkCategoryExist(StateTypeDTO stateTypeDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(stateTypeDTO.getParentId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.StateSet.CATEGORY_ID);
        }
        if (parentCategory.getCategoryType() != CategoryType.STATE_TYPE) {
            throw new DataConstrainException(ErrorMessage.StateSet.CATEGORY_STATE_TYPE, Resources.CATEGORY, ErrorKey.Category.CATEGORY_TYPE);
        }
    }

    private void checkStateGroupExist(StateTypeDTO stateTypeDTO) {
        StateGroupsDTO stateGroupsDTO = stateGroupsService.getStateGroupsDetail(stateTypeDTO.getStateGroupId());
        if (stateGroupsDTO == null) {
            throw new ResourceNotFoundException(ErrorMessage.StateSet.STATE_TYPE_NOT_FOUND, Resources.STATEGROUPS, ErrorKey.StateSet.STATE_GROUP_ID);
        }
    }

    public List<StateTypeDTO> getListStateTypeByCategoryId(Long categoryId) {
        Integer domainId = OCSUtils.getDomain();
        StateTypeDTO stateTypeDTO = new StateTypeDTO();
        stateTypeDTO.setCategoryId(categoryId);
        checkCategoryExist(stateTypeDTO, domainId);
        List<StateTypeDTO> stateTypeDTOs = stateTypeRepository.findByParentIdAndDomainIdOrderByPosIndexAsc(categoryId, domainId).stream().map(stateTypeMapper::toDto).collect(Collectors.toList());
        return stateTypeDTOs;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.STATE_TYPE;
    }
}
