package com.mycompany.myapp.service.billingcycle;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.domain.constant.BillingCycleStatus;
import com.mycompany.myapp.domain.constant.CalcUnitType;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.OCSException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public abstract class BillingCycleCalculation {
    
    protected LocalDateTime beginDate;
    protected LocalDateTime endDate;
    protected Integer quantity;
    protected CalcUnitType calcUnitType;
    protected Long billingCycleTypeId;
    
    protected Integer fromOfDay;
    
    public List<BillingCycle> generateBillingCycles() {
        Integer domainId = OCSUtils.getDomain();
        beginDate = getNextDate(beginDate);
        List<BillingCycle> result = new ArrayList<>();
        while (beginDate.compareTo(endDate) < 0) {
            LocalDateTime tempEndDate = plusDate(beginDate);
            BillingCycle billingCycle = new BillingCycle();
            billingCycle.setBillingCycleTypeId(billingCycleTypeId);
            billingCycle.setCycleBeginDate(Timestamp.valueOf(beginDate));
            billingCycle.setCycleEndDate(Timestamp.valueOf(tempEndDate));
            billingCycle.setDomainId(domainId);
            billingCycle.setState(BillingCycleStatus.NON_ACTIVE);
            if (tempEndDate.compareTo(endDate) <= 0) {
                result.add(billingCycle);
            }
            beginDate = plusDate(beginDate);
        }
        
        if (CollectionUtils.isEmpty(result)) {
            throw new DataConstrainException(ErrorMessage.BillingCycle.NOT_ENOUGHT_FOR_CYCLE, "",
                    ErrorKey.BillingCycle.END_DATE);
        }
        
        return result;
    }

    public static BillingCycleCalculation create(LocalDateTime beginDate, LocalDateTime endDate, Integer quantity,
            CalcUnitType calcUnitType, Long billingCycleTypeId, Integer timeValue) {
        if (beginDate.compareTo(endDate) > 0) {
            throw new DataConstrainException(ErrorMessage.BillingCycle.BEGIN_DATE_END_DATE, "",
                    ErrorKey.BillingCycle.END_DATE);
        }
        switch (calcUnitType) {
        case MINUTES:
            return new BillingMinutes(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case HOUR:
            return new BillingHour(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case DAY:
            return new BillingDay(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case WEEK:
            return new BillingWeek(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case MONTH:
            return new BillingMonth(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case YEAR:
            return new BillingYear(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case DAY_OF_WEEK:
            if (timeValue == null || timeValue > 7) {
                throw new DataConstrainException(ErrorMessage.BillingCycle.INVALID_DAY_OF_WEEK, Resources.BILLING_CYCLE_TYPE, ErrorKey.BillingCycle.TIME_VALUE);
            }
            return new BillingDayOfWeek(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        case DAY_OF_MONTH:
        case MONTH_FOR_MAIN_PP:
            if (timeValue == null || timeValue > 31) {
                throw new DataConstrainException(ErrorMessage.BillingCycle.INVALID_DAY_OF_MONTH, Resources.BILLING_CYCLE_TYPE, ErrorKey.BillingCycle.TIME_VALUE);
            }
            return new BillingDayOfMonth(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        // TODO Implement later if have requirement
        // case MONTH_FOR_MAIN_PP:
        // return new BillingMonthForMainPP(beginDate, endDate, quantity, calcUnitType,
        // billingCycleTypeId, timeValue);
        case HOUR_OF_DAY:
            if (timeValue == null || timeValue > 23) {
                throw new DataConstrainException(ErrorMessage.BillingCycle.INVALID_HOUR_OF_DAY, Resources.BILLING_CYCLE_TYPE, ErrorKey.BillingCycle.TIME_VALUE);
            }
            return new BillingHourOfDay(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
        default:
            throw new OCSException("This case must not be existed!!!", "", "");
        }
    }
    
    public static BillingCycleCalculation create(Timestamp beginDate, Timestamp endDate, Integer quantity,
            CalcUnitType calcUnitType, Long billingCycleTypeId, Integer timeValue) {
        return create(beginDate.toLocalDateTime(), endDate.toLocalDateTime(), quantity, calcUnitType, billingCycleTypeId, timeValue);
        
    }
    
    public Date toDate(LocalDateTime dateToConvert) {
        return Timestamp.valueOf(dateToConvert);
    }
    
    public LocalDateTime toLocalDateTime(Date dateToConvert) {
        return LocalDateTime.ofInstant(
          dateToConvert.toInstant(), ZoneId.systemDefault());
    }
    
    protected abstract LocalDateTime plusDate(LocalDateTime localDateTime);
    
    protected abstract LocalDateTime getNextDate(LocalDateTime localDateTime);
    
}

