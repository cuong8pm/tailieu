package com.mycompany.myapp.service.billingcycle;

import java.time.LocalDateTime;
import java.util.Date;

import com.mycompany.myapp.domain.constant.CalcUnitType;

public class BillingMonthForMainPP extends BillingCycleCalculation  {

    public BillingMonthForMainPP(LocalDateTime beginDate, LocalDateTime endDate, Integer quantity, CalcUnitType calcUnitType,
            Long billingCycleTypeId, Integer timeValue) {
        super(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
    }

    @Override
    protected LocalDateTime plusDate(LocalDateTime localDateTime) {
        return localDateTime.plusMonths(quantity);
    }

    @Override
    protected LocalDateTime getNextDate(LocalDateTime localDateTime) {
        return localDateTime;
    }

}
