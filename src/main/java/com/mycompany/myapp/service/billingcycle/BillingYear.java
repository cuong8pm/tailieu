package com.mycompany.myapp.service.billingcycle;

import java.time.LocalDateTime;

import com.mycompany.myapp.domain.constant.CalcUnitType;

public class BillingYear extends BillingCycleCalculation {

    public BillingYear(LocalDateTime beginDate, LocalDateTime endDate, Integer quantity, CalcUnitType calcUnitType,
            Long billingCycleTypeId, Integer timeValue) {
        super(beginDate.plusHours(7), endDate.plusHours(7), quantity, calcUnitType, billingCycleTypeId, timeValue);
    }

    @Override
    protected LocalDateTime plusDate(LocalDateTime localDateTime) {
        return localDateTime.plusYears(quantity);
    }

    @Override
    protected LocalDateTime getNextDate(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.withDayOfYear(1);
        if (temp.compareTo(localDateTime) < 0) {
            temp = temp.plusYears(1L);
        }
        temp = temp.plusHours(-7);
        return temp;
    }

}
