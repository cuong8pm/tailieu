package com.mycompany.myapp.service.billingcycle;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.domain.constant.BillingCycleStatus;
import com.mycompany.myapp.domain.constant.CalcUnitType;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;
import org.springframework.util.CollectionUtils;

public class BillingWeek extends BillingCycleCalculation {

    public BillingWeek(LocalDateTime beginDate, LocalDateTime endDate, Integer quantity, CalcUnitType calcUnitType,
            Long billingCycleTypeId, Integer timeValue) {
        /// sum 7h để lấy chính xác ngày lỗi múi giờ
        super(beginDate.plusHours(7), endDate.plusHours(7), quantity, calcUnitType, billingCycleTypeId, timeValue);
    }

    @Override
    protected LocalDateTime plusDate(LocalDateTime localDateTime) {
        return localDateTime.plusWeeks(quantity);
    }

    @Override
    protected LocalDateTime getNextDate(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY));
        if (temp.compareTo(localDateTime) < 0) {
            temp = localDateTime.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        }
        return temp;
    }
    /// trừ đi 7h để chỉnh giờ về 0h00
    @Override
    public List<BillingCycle> generateBillingCycles() {
        Integer domainId = OCSUtils.getDomain();
        beginDate = getNextDate(beginDate);
        List<BillingCycle> result = new ArrayList<>();
        while (beginDate.compareTo(endDate) < 0) {
            LocalDateTime tempEndDate = plusDate(beginDate);
            BillingCycle billingCycle = new BillingCycle();
            billingCycle.setBillingCycleTypeId(billingCycleTypeId);
            billingCycle.setCycleBeginDate(Timestamp.valueOf(beginDate.plusHours(-7)));
            billingCycle.setCycleEndDate(Timestamp.valueOf(tempEndDate.plusHours(-7)));
            billingCycle.setDomainId(domainId);
            billingCycle.setState(BillingCycleStatus.NON_ACTIVE);
            if (tempEndDate.compareTo(endDate) <= 0) {
                result.add(billingCycle);
            }
            beginDate = plusDate(beginDate);
        }

        if (CollectionUtils.isEmpty(result)) {
            throw new DataConstrainException(ErrorMessage.BillingCycle.NOT_ENOUGHT_FOR_CYCLE, "",
                ErrorKey.BillingCycle.END_DATE);
        }

        return result;
    }
}
