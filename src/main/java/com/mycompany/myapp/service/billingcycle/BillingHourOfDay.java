package com.mycompany.myapp.service.billingcycle;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.domain.constant.CalcUnitType;

public class BillingHourOfDay extends BillingCycleCalculation  {

    public BillingHourOfDay(LocalDateTime beginDate, LocalDateTime endDate, Integer quantity, CalcUnitType calcUnitType,
            Long billingCycleTypeId, Integer timeValue) {
        super(beginDate, endDate, quantity, calcUnitType, billingCycleTypeId, timeValue);
    }


    @Override
    protected LocalDateTime plusDate(LocalDateTime localDateTime) {
        return localDateTime.plusDays(quantity);
    }


    @Override
    protected LocalDateTime getNextDate(LocalDateTime localDateTime) {
        LocalDateTime result = localDateTime.withHour(fromOfDay);
        if (result.compareTo(beginDate) < 0) {
            return result.plusDays(1);
        }
        return result;
    }

}
