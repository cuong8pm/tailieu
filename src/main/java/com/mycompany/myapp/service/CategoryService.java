package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.mapper.CategoryMapper;
import com.mycompany.myapp.repository.*;
import com.mycompany.myapp.repository.action.ActionRepository;
import com.mycompany.myapp.repository.actiontype.ActionTypeRepository;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.repository.characteristic.CharacteristicRepository;
import com.mycompany.myapp.repository.event.EventRepository;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.repository.normalizer.NormalizerRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentRepository;
import com.mycompany.myapp.repository.ratetable.RateTableRepository;
import com.mycompany.myapp.repository.ratingfilter.RatingFilterRepository;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.PermissionDeniedException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Service
@Transactional
public class CategoryService extends OCSBaseService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private UnitTypeRepository unitTypeRepository;

    @Autowired
    private ParameterRepository parameterRepository;

    @Autowired
    private ReserveInfoRepository reserveInfoRepository;

    @Autowired
    private ZoneMapRepository zoneMapRepository;

    @Autowired
    private GeoHomeZoneRepository geoHomeZoneRepository;

    @Autowired
    private OcsServiceRepository ocsServiceRepository;

    @Autowired
    private StatisticItemRepository statisticItemRepository;

    @Autowired
    private PccRuleRepository pccRuleRepository;

    @Autowired
    private BillingCycleTypeRepository billingCycleTypeRepository;

    @Autowired
    private StateTypeRepository stateTypeRepository;

    @Autowired
    private StateGroupsRepository stateGroupsRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PepProfileRepository pepProfileRepository;

    @Autowired
    private BalTypeRepository balTypeRepository;

    @Autowired
    private MapAcmBalBalRepository mapAcmBalBalRepository;

    @Autowired
    private MapSharebalBalRepository mapSharebalBalRepository;

    @Autowired
    private CharacteristicRepository characteristicRepository;

    @Autowired
    private NormalizerRepository normalizerRepository;

    @Autowired
    private RateTableRepository rateTableRepository;

    @Autowired
    private RatingFilterRepository ratingFilterRepository;

    @Autowired
    private BlockRepository blockRepository;

    @Autowired
    private PriceComponentRepository priceComponentRepository;

    @Autowired
    private ActionTypeRepository actionTypeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferTemplateRepository offerTemplateRepository;

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @Autowired
    private RouterRepository routerRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private EventPolicyRepository eventPolicyRepository;

    @Autowired
    private FunctionRepository functionRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = categoryRepository;
        this.parentService = categoryService;
    }

    @Transactional(readOnly = true)
    public Page<CategoryDTO> getCategory(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);

        Category rootCategory = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (rootCategory == null) {
            throw new ResourceNotFoundException(
                ErrorMessage.Category.ROOT_NOT_FOUND,
                Resources.CATEGORY,
                ErrorKey.Category.PARENT_CATEGORY
            );
        }

        int categoryParentLevel = getCategoryLevel(searchCriteria.getParentId()) + 2;

        Page<CategoryDTO> result = categoryRepository
            .findCategory(
                searchCriteria.isSearchAll() ? null : searchCriteria.getParentId(),
                domainId,
                searchCriteria.getName(),
                searchCriteria.getDescription(),
                pageable
            )
            .map(categoryMapper::toDto);
        result
            .get()
            .forEach(
                category -> {
                    boolean hasCategoryChild = categoryRepository.getMaxPosIndex(category.getId(), domainId) != null;
                    boolean hasUnitChild = checkHasChild(category.getId(), category.getCategoryType(), domainId);
                    category.setHasChild(hasCategoryChild || hasUnitChild);
                    category.setTreeType("category");
                    category.setCategoryLevel(categoryParentLevel);
                }
            );
        return result;
    }

    public CategoryDTO createCategory(CategoryDTO categoryDTO) {
        Integer domainId = OCSUtils.getDomain();

        Category category = categoryMapper.toEntity(categoryDTO);
        category.setDomainId(domainId);
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(categoryDTO.getParentCategoryId(), domainId);

        if (parentCategory == null) {
            throw new ResourceNotFoundException(
                ErrorMessage.Category.ROOT_NOT_FOUND,
                Resources.CATEGORY,
                ErrorKey.Category.PARENT_CATEGORY
            );
        }

        super.setPosIndex(domainId, category, parentCategory.getId());
        //        category.setTreeType("Test");
        category.setCategoryParentId(parentCategory.getId());
        category.setCategoryType(parentCategory.getCategoryType());
        category.setTreeType(parentCategory.getTreeType());
        checkDuplicateName(category.getName(), category.getId(), category.getCategoryType());
        category = categoryRepository.save(category);
        CategoryDTO dto = categoryMapper.toDto(category);
        dto.setType(parentCategory.getTreeType());
        dto.setTreeType(Resources.TreeType.CATEGORY);
        return dto;
    }

    private void checkDuplicateName(String name, Long id, CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();
        List<Category> categories = categoryRepository.findAndDomainIdAndCategoryTypeAndName(domainId, categoryType, name);
        for (Category category : categories) {
            if (category != null && !Objects.equals(id, category.getId())) {
                throw new DataConstrainException(ErrorMessage.Category.DUPLICATED_NAME, Resources.CATEGORY, ErrorKey.Category.NAME);
            }
        }
    }

    public CategoryDTO updateCategory(CategoryDTO categoryDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateName(categoryDTO.getName(), categoryDTO.getId(), categoryDTO.getCategoryType());
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(categoryDTO.getId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.ID);
        }

        category.setName(categoryDTO.getName());
        category.setRemark(categoryDTO.getDescription());

        validateParentCategory(categoryDTO, domainId, category);

        if (!Objects.equals(category.getCategoryParentId(), categoryDTO.getParentCategoryId())) {
            super.setPosIndex(domainId, category, categoryDTO.getParentCategoryId());
            category.setCategoryParentId(categoryDTO.getParentCategoryId());
        }

        category = categoryRepository.save(category);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(category, response);

        return categoryMapper.toDto(category);
    }

    public void validateParentCategory(CategoryDTO categoryDTO, Integer domainId, Category category) {
        if (Objects.equals(categoryDTO.getId(), categoryDTO.getParentCategoryId())) {
            throw new DataConstrainException(ErrorMessage.Category.INFINITE_LOOP_CATEGORY, Resources.CATEGORY, ErrorKey.CATEGORY_ID);
        }

        if (category.getCategoryParentId() != null) {
            // Check new parent Category type
            Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(categoryDTO.getParentCategoryId(), domainId);

            if (parentCategory == null) {
                throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.PARENT_CATEGORY_NAME);
            }

            if (parentCategory.getCategoryType() != categoryDTO.getCategoryType()) {
                throw new ResourceNotFoundException(
                    ErrorMessage.Category.CANT_UPDATE_PARENT,
                    Resources.CATEGORY,
                    ErrorKey.Category.PARENT_CATEGORY
                );
            }

            if (parentCategory.getCategoryParentId() != null) {
                Set<Category> categories = new HashSet<>();
                categories.add(category);
                categories.add(parentCategory);
                checkParentCategory(categories, parentCategory.getCategoryParentId(), domainId);
            }
        } else {
            if (categoryDTO.getParentCategoryId() != null) {
                throw new ResourceNotFoundException(
                    ErrorMessage.Category.CANT_UPDATE_PARENT_FOR_ROOT,
                    Resources.CATEGORY,
                    ErrorKey.Category.PARENT_CATEGORY
                );
            }
        }
    }

    public void deleteCategories(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(id, domainId);

        checkRootCategoryForDelete(category);

        // Lock parent for update
        categoryRepository.findByIdAndDomainIdForUpdate(category.getCategoryParentId(), category.getDomainId());

        boolean hasCategoryChild = categoryRepository.getMaxPosIndex(category.getId(), domainId) != null;
        boolean hasUnitChild = checkHasChild(category.getId(), category.getCategoryType(), domainId);

        if (hasCategoryChild || hasUnitChild) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_HAS_CHILD, Resources.CATEGORY, ErrorKey.Category.TEMPLATES);
        }

        categoryRepository.deleteById(id);
    }

    private void checkRootCategoryForDelete(Category category) {
        if (category.getCategoryParentId() == null) {
            throw new PermissionDeniedException(ErrorMessage.Category.CANT_DELETE_ROOT, Resources.CATEGORY, ErrorKey.Category.TEMPLATES);
        }
    }

    public CategoryDTO getCategoryDetail(Long categoryParentId, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainId(categoryParentId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.PARENT_CATEGORY);
        }

        CategoryDTO categoryDTO = categoryMapper.toDto(category);

        if (category.getCategoryParentId() != null) {
            Category parentCategory = categoryRepository.findByIdAndDomainId(category.getCategoryParentId(), domainId);
            categoryDTO.setParentCategoryName(parentCategory.getName());
        }

        boolean hasCategoryChild = categoryRepository.getMaxPosIndex(category.getId(), domainId) != null;
        boolean hasUnitChild = checkHasChild(categoryParentId, category.getCategoryType(), domainId);
        categoryDTO.setHasChild(hasCategoryChild || hasUnitChild);
        categoryDTO.setParentCategoryLevel(getCategoryLevel(categoryParentId) + 1);
        return categoryDTO;
    }

    public List<CategoryDTO> getRootCategory(CategoryType categoryType, boolean loadAll) {
        return this.getRootCategory(Arrays.asList(categoryType), loadAll);
    }

    public List<CategoryDTO> getRootCategory(List<CategoryType> categoryTypes, boolean loadAll) {
        Integer domainId = OCSUtils.getDomain();

        List<CategoryDTO> categoryDTOs = categoryRepository
            .findRootCategory(categoryTypes, domainId)
            .stream()
            .map(category -> categoryMapper.toDto(category))
            .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(categoryDTOs)) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.CATEGORY_TYPE);
        }

        for (CategoryDTO categoryDTO : categoryDTOs) {
            categoryDTO.setHasChild(categoryRepository.getMaxPosIndex(categoryDTO.getId(), domainId) != null);
            categoryDTO.setTreeType("category");
            int parentCategoryLevel = 0;
            if (loadAll) {
                List<Tree> subCategories = loadAllData(categoryDTO.getId(), domainId, parentCategoryLevel + 1);
                categoryDTO.setTemplates(subCategories);
            }
            categoryDTO.setParentCategoryLevel(parentCategoryLevel);
        }
        return categoryDTOs;
    }

    private List<Tree> loadAllData(Long parentCategory, Integer domainId, int parentCategoryLevel) {
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Direction.ASC, Category.FieldNames.POS_INDEX));
        List<CategoryDTO> categories = categoryRepository
            .findCategory(parentCategory, domainId, null, null, pageable)
            .map(categoryMapper::toDto)
            .getContent();
        List<Tree> result = new ArrayList<>();
        for (CategoryDTO category : categories) {
            boolean hasUnitChild = categoryRepository.countByParentIdAndDomainId(category.getId(), domainId) != 0;
            category.setHasChild(hasUnitChild);
            category.setTreeType("category");
            category.setTemplates(loadAllData(category.getId(), domainId, parentCategoryLevel + 1));
            result.add(category);
            category.setParentCategoryLevel(parentCategoryLevel);
        }
        return result;
    }

    // Trong category, Ngoai cac sub category ra, con co cac child,
    // ham nay se check xem co children hay khong?
    private boolean checkHasChild(Long id, CategoryType categoryType, Integer domainId) {
        switch (categoryType) {
            case UNIT_TYPES:
                return unitTypeRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case PARAMETER:
                return parameterRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case RESERVE_INFO:
                return reserveInfoRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case ZONE:
                return zoneMapRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case GEO_HOME_ZONE:
                return geoHomeZoneRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case STATISTIC:
                return statisticItemRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case PCC_RULE:
                return pccRuleRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case BILLING_CYCLE:
                return billingCycleTypeRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case STATE_TYPE:
                return stateTypeRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case STATE_GROUP:
                return stateGroupsRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case SERVICE:
                return ocsServiceRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case PEP_PROFILE:
                return pepProfileRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case BALANCES:
            case ACCUMULATE_BALANCES:
                return balTypeRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case ACCOUNT_BALANCE_MAPPING:
                return mapSharebalBalRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case ACM_BALANCE_MAPPING:
                return mapAcmBalBalRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case CHARACTERISTIC:
                return characteristicRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case NORMALIZER:
                return normalizerRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case RATE_TABLE:
                return rateTableRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case RATING_FILTER:
                return ratingFilterRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case BLOCK:
                return blockRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case PRICE_COMPONENT:
                return priceComponentRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case ACTION_TYPE:
                return actionTypeRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case EVENT:
                return eventRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case OFFER:
                return offerRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case OFFER_TEMPlATE:
                return offerTemplateRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case ACTION:
                return actionRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case VERIFICATION:
                return verificationRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case BUILDER_TYPE:
                return builderTypeRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0l;
            case ROUTER:
                return routerRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case BUILDER:
                return builderRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case CONDITION:
                return conditionRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case FILTER:
                return filterRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case EVENT_POLICY:
                return eventPolicyRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            case FUNCTION:
                return functionRepository.countByParentIdAndDomainId(id, domainId).longValue() != 0L;
            default:
                return false;
        }
    }

    public void checkInfinityLoop(CategoryDTO categoryDTO) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainId(categoryDTO.getId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.ID);
        }
        validateParentCategory(categoryDTO, domainId, category);
    }

    private void checkParentCategory(Set<Category> categories, Long parentId, Integer domainId) {
        Category category = categoryRepository.findByIdAndDomainId(parentId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(
                ErrorMessage.Category.ROOT_NOT_FOUND,
                Resources.CATEGORY,
                ErrorKey.Category.PARENT_CATEGORY
            );
        }
        if (categories.add(category)) {
            if (category.getCategoryParentId() == null) {
                return;
            }
            checkParentCategory(categories, category.getCategoryParentId(), domainId);
        } else {
            throw new DataConstrainException(ErrorMessage.Category.INFINITE_LOOP_CATEGORY, Resources.CATEGORY, ErrorKey.CATEGORY_ID);
        }
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    public List<Tree> getAllCategory(CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();
        List<Tree> list = new ArrayList<>();
        List<Category> categories = categoryRepository.findByDomainIdAndCategoryTypeOrderByPosIndexAsc(domainId, categoryType);
        int categoryLevel = 0;
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getParentId() == null) {
                Tree tree = new CategoryDTO();
                BeanUtils.copyProperties(categories.get(i), tree);
                tree.setType(tree.getTreeType());
                tree.setTreeType("category");
                tree.setCategoryLevel(categoryLevel);
                categoryBuild(tree, categories, categoryLevel + 1);
                if (!CollectionUtils.isEmpty(tree.getTemplates())) {
                    ((CategoryDTO) tree).setHasChild(true);
                } else {
                    ((CategoryDTO) tree).setHasChild(false);
                }
                list.add(tree);
            }
        }
        return list;
    }

    public void categoryBuild(Tree treeDto, List<Category> categories, int categoryLevel) {
        List<Tree> list = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getParentId() != null && categories.get(i).getParentId().compareTo(treeDto.getId()) == 0) {
                Tree tree = new CategoryDTO();
                BeanUtils.copyProperties(categories.get(i), tree);
                tree.setType(tree.getTreeType());
                tree.setTreeType("category");
                tree.setCategoryLevel(categoryLevel);
                categoryBuild(tree, categories, categoryLevel + 1);
                if (!CollectionUtils.isEmpty(tree.getTemplates())) {
                    ((CategoryDTO) tree).setHasChild(true);
                } else {
                    ((CategoryDTO) tree).setHasChild(false);
                }
                list.add(tree);
            }
        }
        treeDto.setTemplates(list);
    }
}
