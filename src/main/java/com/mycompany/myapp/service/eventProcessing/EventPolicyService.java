package com.mycompany.myapp.service.eventProcessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.service.exception.DataConstrainException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.dto.eventProcessing.BuilderDTO;
import com.mycompany.myapp.dto.eventProcessing.CreateEventPolicyDTO;
import com.mycompany.myapp.dto.eventProcessing.EventPolicyDTO;
import com.mycompany.myapp.dto.eventProcessing.ExpressionDTO;
import com.mycompany.myapp.dto.eventProcessing.FilterDTO;
import com.mycompany.myapp.dto.eventProcessing.FilterFieldDTO;
import com.mycompany.myapp.dto.eventProcessing.VerificationDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.mapper.eventProcessing.EventPolicyMapper;
import com.mycompany.myapp.mapper.eventProcessing.FilterMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionBuilderMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionRepository;
import com.mycompany.myapp.repository.eventProcessing.EventPolicyRepository;
import com.mycompany.myapp.repository.eventProcessing.FilterIgnoreProfileRepository;
import com.mycompany.myapp.repository.eventProcessing.FilterRepository;
import com.mycompany.myapp.repository.eventProcessing.RouterRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class EventPolicyService extends AbstractCloneService {

    @Autowired
    private EventPolicyRepository eventPolicyRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FilterService filterService;

    @Autowired
    private EventPolicyMapper eventPolicyMapper;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FilterMapper filterMapper;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private ConditionService conditionService;

    @Autowired
    private RouterRepository routerRepository;

    @Autowired
    private BuilderService builderService;

    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private ConditionVerificationMapService conditionVerificationMapService;

    @Autowired
    private ConditionBuilderMapRepository conditionBuilderMapRepository;

    @Autowired
    private ConditionBuilderMapService conditionBuilderMapService;

    @Autowired
    private FilterIgnoreProfileRepository filterIgnoreProfileRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = eventPolicyRepository;
        this.ocsCloneableRepository = eventPolicyRepository;
        this.parentService = categoryService;
        this.childServices = Arrays.asList(filterService);
        this.childDependServices = Arrays.asList(filterService);
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.EVENT_POLICY);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new EventPolicyDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.EVENT_POLICY;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return EventPolicy.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.EVENT_POLICY;
    }

    @Override
    protected String getNameCategory() {
        return "Event Policy";
    }

    public EventPolicyDTO createEventPolicy(EventPolicyDTO dto){
        Integer domainId = OCSUtils.getDomain();
        checkCategory(dto.getCategoryId());
        EventPolicy eventPolicy = eventPolicyMapper.toEntity(dto);
        checkDuplicateName(eventPolicy.getName(),eventPolicy.getId());
        setPosIndex(domainId,eventPolicy,eventPolicy.getCategoryId());
        eventPolicy.setDomainId(domainId);
        eventPolicy = eventPolicyRepository.save(eventPolicy);
        dto = eventPolicyMapper.toDto(eventPolicy);

        return dto;
    }

    public EventPolicyDTO updateEventPolicy(EventPolicyDTO dto){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy oldEventPolicy = eventPolicyRepository.findByIdAndDomainId(dto.getId(), domainId);
        if(oldEventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        checkCategory(dto.getCategoryId());
        checkDuplicateName(dto.getName(),dto.getId());
        EventPolicy newEventPolicy = eventPolicyMapper.toEntity(dto);
        if (!Objects.equal(newEventPolicy.getCategoryId(), oldEventPolicy.getCategoryId())) {
            setPosIndex(domainId, newEventPolicy, dto.getCategoryId());
        }
        newEventPolicy.setDomainId(domainId);
        newEventPolicy = eventPolicyRepository.save(newEventPolicy);
        dto = eventPolicyMapper.toDto(newEventPolicy);

        return dto;
    }

    public void deleteEventPolicy(Long eventPolicyId){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(eventPolicyId, domainId);
        if(eventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        Integer count = eventPolicyRepository.countEventUsedInFormula(eventPolicyId.toString(),domainId);

        Integer countThreshold = eventPolicyRepository.countEventUsedInThreshold(eventPolicyId,domainId);
        if(count + countThreshold > 0){
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        List<Filter> filters = filterRepository.findAllByDomainIdAndEventPolicyId(domainId,eventPolicyId);
        List<Long> filterIds = filters.stream().map(Filter::getId).collect(Collectors.toList());
        filterService.deleteFilter(filterIds);
        eventPolicyRepository.deleteByIdAndDomainId(eventPolicyId,domainId);
    }

    public EventPolicyDTO getDetailEventPolicy(Long eventPolicyId){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(eventPolicyId, domainId);
        if(eventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        EventPolicyDTO dto = eventPolicyMapper.toDto(eventPolicy);
        Category category = categoryRepository.findByIdAndDomainId(dto.getCategoryId(),domainId);
        if(category != null){
            dto.setCategoryId(category.getId());
            dto.setCategoryName(category.getName());
            dto.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        dto.setHasChild(filterRepository.countAllByEventPolicyIdAndDomainId(eventPolicyId,domainId).intValue() > 0);
        return dto;
    }

    public Page<EventPolicyDTO> getListEventPolicyByCategoryId(Long categoryId,String name, String description, Pageable pageable){
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CATEGORY, ErrorKey.ID);
        }
        Page<EventPolicyDTO> page = eventPolicyRepository
            .getListEventPolicyPaging(domainId,name,description,categoryId,pageable).map(eventPolicyMapper::toDto);
        return page;
    }

    public List<EventPolicyDTO> getListEventPolicyNotPaging(Long categoryId, Pageable unpage){
        Integer domainId = OCSUtils.getDomain();
        Page<EventPolicy> entities = (Page<EventPolicy>) (Page<?>) getListObjectByCategoryId(categoryId, unpage);
        List<EventPolicyDTO> dtos = entities.stream().map(eventPolicyMapper::toDto).collect(Collectors.toList());
        List<Long> ids = dtos.stream().map(EventPolicyDTO::getId).collect(Collectors.toList());
        List<CountChildrenByParentDTO> countChildren = eventPolicyRepository.countChildrenByParentId(ids, domainId);
        Map<Long, Integer> childrenMap = countChildren.stream()
            .collect(Collectors.toMap(CountChildrenByParentDTO::getId, CountChildrenByParentDTO::getCountChild));
        for (EventPolicyDTO dto : dtos) {
            Integer count = childrenMap.get(dto.getId());
            if (count != null && count > 0) {
                dto.setHasChild(true);
            }
        }
        dtos.sort(Comparator.comparing(EventPolicyDTO::getPosIndex));
        return dtos;
    }

    public List<FilterDTO> getListFilterByEventPolicyId(Long eventPolicyId){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(eventPolicyId,domainId);
        if(eventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        List<FilterDTO> filterDTOs = filterMapper
            .toDto(filterRepository.findAllByDomainIdAndEventPolicyId(domainId,eventPolicyId));
        List<Long> filterIds = filterDTOs.stream().map(FilterDTO::getId).collect(Collectors.toList());
        List<CountChildrenByParentDTO> countChildren = conditionRepository.countChildrenByParentId(filterIds,domainId);
        Map<Long,Integer> childrenMap = countChildren.stream().collect(Collectors.toMap(CountChildrenByParentDTO::getId,CountChildrenByParentDTO::getCountChild));
        for (FilterDTO filterDTO : filterDTOs) {
            filterDTO.setHasChild(checkHasChildOfChildren(childrenMap,filterDTO.getId()));
        }
        filterDTOs.sort(Comparator.comparing(FilterDTO::getId));
        return filterDTOs;
    }

    @Override
    public void getChildTemplate(List<Long> ids, Map<Long, TreeClone> resultMap) {
        Integer domainId = OCSUtils.getDomain();
        List<Filter> filters = filterRepository.findAllByDomainIdAndEventPolicyIdIn(domainId,ids);
        filters.sort(Comparator.comparing(Filter::getPosIndex));

        List<Long> filterIds = filters.stream().map(Filter::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(filterIds)) {
            return;
        }

        Map<Long, TreeClone> filterMap = new HashMap<Long, TreeClone>();
        List<TreeClone> filterDTOs = new ArrayList<>();
        for (Filter baseEntity : filters) {
            TreeClone result = new FilterDTO();
            result.setTemplates(new ArrayList<Tree>());
            BeanUtils.copyProperties(baseEntity, result);
            result.setName(baseEntity.getName());
            if (result.getTemplates() == null) {
                result.setTemplates(new ArrayList<Tree>());
            }
            filterDTOs.add(result);
            filterMap.put(result.getId(), result);
        }
        filterService.getChildTemplate(filterIds,filterMap);
        for (Long entityId : resultMap.keySet()) {
            TreeClone resultEventPolicy = resultMap.get(entityId);
            List<? extends Tree> children = filterDTOs;
            if (children != null) {
                resultEventPolicy.getTemplates().addAll(children);
            }
            resultEventPolicy.setHasChild(!resultEventPolicy.getTemplates().isEmpty());
        }
    }

    @Override
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        List<CloneDTO> lstCloneDTos = new ArrayList<>();
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        Map<Long, List<Long>> parentChildIdMap = new HashMap<>();
        List<Long> newIds = new ArrayList<>();
        Map<Long, Long> oldNewFilterIdMap = new HashMap<>();

        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);

        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if (!java.util.Objects.equals(distinctIds.size(), baseToClones.size())) {
            throwResourceNotFoundException();
        }

        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
            .collect(Collectors.toMap(OCSCloneableEntity::getId, java.util.function.Function.identity()));

        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveEntity(cloneDTO, baseToCloneMap);
            if (!ObjectUtils.isEmpty(clonedEntity)) {
                oldNewIdMap.put(clonedEntity.getId(), cloneDTO);
                clonedEntities.add(clonedEntity);
                newIds.add(clonedEntity.getId());
            }
            cloneDTO.getChildren().forEach(cloneDTO1 -> cloneDTO1.setParentId(clonedEntity.getId()));
            lstCloneDTos.addAll(cloneDTO.getChildren());
            List<Long> childrenIds = new ArrayList<>();
            if (cloneDTO.getChildren() != null) {
                childrenIds = cloneDTO.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }
            oldNewFilterIdMap.put(cloneDTO.getId(),clonedEntity.getId());
            parentChildIdMap.put(clonedEntity.getId(), childrenIds);
        }

        filterService.cloneEntities(lstCloneDTos,oldNewChildMap);
        return clonedEntities;
    }

    public void deleteFilter(Long eventPolicyId, Long filterId){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(eventPolicyId, domainId);
        if(eventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        filterService.deleteFilter(Arrays.asList(filterId));
    }
    public void deleteCondition(Long eventPolicyId, Long conditionId){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(eventPolicyId, domainId);
        if(eventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        conditionService.deleteCondition(conditionId);
    }
    public void deleteBuilder(Long eventPolicyId, Long builderId){
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(eventPolicyId, domainId);
        if(eventPolicy == null){
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        builderService.deleteBuilder(Arrays.asList(builderId));
    }

    public CreateEventPolicyDTO createEventPolicy(Long id, CreateEventPolicyDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(id, domainId);
        if (eventPolicy == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        Long filterId = filterService.createFilter(dto.getFilter()).getId();
        List<ExpressionDTO> filterExpressionDTOs = dto.getFilter().getExpressions();
        for (ExpressionDTO item : filterExpressionDTOs) {
            expressionService.createExpression(filterId, item);
        }
        List<FilterFieldDTO> filterFieldDTOs = dto.getFilter().getOutputs().getListField();
        for (FilterFieldDTO item : filterFieldDTOs) {
            item.setFilterId(filterId);
            filterService.addField(item);
        }
        List<ReferTableDTO> profiles = dto.getFilter().getOutputs().getProfile();
        changeProfiles(filterId, profiles);
        dto.getCondition().setFilterId(filterId);
        Long conditionId = conditionService.createCondition(dto.getCondition()).getId();
        List<ExpressionDTO> conditionExpressionDTOs = dto.getCondition().getExpressions();
        for (ExpressionDTO item : conditionExpressionDTOs) {
            conditionService.addExpressions(conditionId, item);
        }
        List<VerificationDTO> verificationDTOs = dto.getCondition().getVerifications();
        conditionVerificationMapService.createConditionVerificationMap(verificationDTOs, conditionId);

        List<BuilderDTO> builders = dto.getCondition().getBuilders();
        for (BuilderDTO item : builders) {
            Builder builder = (Builder) builderService.findByIdAndDomainId(item.getId(),domainId);
            if(builder == null){
                throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ENTITY_NOT_FOUND);
            }
            ConditionBuilderMap map = new ConditionBuilderMap();
            map.setBuilderId(item.getId());
            map.setConditionId(conditionId);
            map.setDomainId(domainId);
            conditionBuilderMapRepository.save(map);
        }
        dto.getCondition().setHasChild(!builders.isEmpty());
        dto.getCondition().setId(conditionId);
        dto.getFilter().setId(filterId);
        dto.getFilter().setHasChild(true);
        return dto;
    }

    public void changeProfiles(Long id, List<ReferTableDTO> dtos) {
        Integer domainId = OCSUtils.getDomain();
        for (ReferTableDTO item : dtos) {
            if (item.getIsUsed() == true) {
                filterIgnoreProfileRepository.deleteByFilterIdAndReferTableId(id, item.getId());
            } else if (item.getIsUsed() == false) {
                FilterIgnoreProfile filterIgnoreProfile = new FilterIgnoreProfile();
                filterIgnoreProfile.setFilterId(id);
                filterIgnoreProfile.setReferTableId(item.getId());
                filterIgnoreProfile.setDomainId(domainId);
                filterIgnoreProfileRepository.save(filterIgnoreProfile);
            }
        }
    }

    @Override
    protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        return eventPolicyRepository.findByIdInAndDomainId(ids, domainId);
    }

    @Override
    protected List getMappingFoundEntities(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        return filterRepository.findAllByDomainIdAndEventPolicyIdIn(domainId,ids);
    }

    @Override
    protected Map<Long, Object> getMappingFoundEntities(List<Long> ids, Boolean isPopup) {
        Map<Long, Object> mappingOfFoundEntity = new HashMap<>();
        List list = getMappingFoundEntities(ids);
        if (isPopup == null || (list != null && isPopup != true)) {
            if (!list.isEmpty() && list.get(0) instanceof Filter) {
                mappingOfFoundEntity =
                    (Map<Long, Object>) list.stream().collect(Collectors.groupingBy(Filter::getEventPolicyId, Collectors.toList()));
            }
        }
        return mappingOfFoundEntity;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.EventPolicy.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.EventPolicy.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }
}
