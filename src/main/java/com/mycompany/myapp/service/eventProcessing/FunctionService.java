package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.mapper.eventProcessing.FunctionMapper;
import com.mycompany.myapp.mapper.eventProcessing.FunctionParamMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
public class FunctionService extends AbstractCloneService {
    @Autowired
    private FunctionRepository functionRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FunctionMapper functionMapper;

    @Autowired
    private FunctionParamRepository functionParamRepository;

    @Autowired
    private ExpressionRepository expressionRepository;

    @Autowired
    private FilterFieldRepository filterFieldRepository;

    @Autowired
    private BuilderItemRepository builderItemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FunctionParamService paramService;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private FunctionParamMapper functionParamMapper;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private FunctionParamInstanceRepository functionParamInstanceRepository;

    @Autowired
    private FunctionParamService functionParamService;

    @Override
    protected List<CategoryType> dependTypes() {
        return null;
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return null;
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new FunctionDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.FUNCTION;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Function.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.FUNCTION;
    }

    @Override
    protected String getNameCategory() {
        return "Functions";
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = functionRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = functionRepository;
        this.childServices = Arrays.asList(paramService);
    }

    public FunctionDTO createFunction(FunctionDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkCategory(dto.getCategoryId());
        checkDuplicateName(dto.getName(), dto.getId());
        Function function = functionMapper.toEntity(dto);
        function.setId(null);
        setPosIndex(domainId, function, dto.getCategoryId());
        function.setDomainId(domainId);
        function.setModifyDate(new Timestamp((new Date()).getTime()));
        function = functionRepository.save(function);
        dto = functionMapper.toDto(function);

        return dto;
    }

    public FunctionDTO updateFunction(FunctionDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        Function functionOld = functionRepository.findByIdAndDomainId(dto.getId(), domainId);
        if (functionOld == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        checkCategory(dto.getCategoryId());
        checkDuplicateName(dto.getName(), dto.getId());
        if (expressionRepository.countAllByInputTypeAndLinkedIdAndDomainId(Expression.InputType.FUNCTION, dto.getId(), domainId) > 0 ||
            filterFieldRepository.countFilterFieldByValueTypeAndLinkedIdAndDomainId(3, dto.getId(), domainId) > 0) {
            if(!Objects.equals(functionOld.getFunctionType(), dto.getFunctionType()) || !Objects.equals(functionOld.getDataType(), dto.getDataType())){
                throw new DataConstrainException(ErrorMessage.Function.CANNOT_DELETE, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        if (!dto.getEdit() &&
            (!Objects.equals(dto.getDataType(), functionOld.getDataType()) || !Objects.equals(dto.getFunctionType(), functionOld.getFunctionType()))) {
            throw new DataConstrainException(ErrorMessage.Function.CANNOT_UPDATE_FUNCTION, Resources.FUNCTION, "");
        }
        Function functionNew = functionMapper.toEntity(dto);
        if (!Objects.equals(functionNew.getCategoryId(), functionOld.getCategoryId())) {
            setPosIndex(domainId, functionNew, dto.getCategoryId());
        }
        functionNew.setDomainId(domainId);
        functionNew.setModifyDate(new Timestamp((new Date()).getTime()));
        functionNew = functionRepository.save(functionNew);
        dto = functionMapper.toDto(functionNew);
        functionParamService.setValueFilterField(dto.getId());
        return dto;
    }

    public void deleteFunction(Long functionId) {
        Integer domainId = OCSUtils.getDomain();
        Function functionOld = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (functionOld == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        validateDeleteFunction(functionId,domainId);
        List<FunctionParam> params = functionParamRepository.findAllByFunctionIdAndDomainId(functionId, domainId);
        functionParamRepository.deleteAll(params);
        functionRepository.deleteByIdAndDomainId(functionId, domainId);
    }

    public void validateDeleteFunction(Long functionId, Integer domainId){
        if (expressionRepository.countAllByInputTypeAndLinkedIdAndDomainId(Expression.InputType.FUNCTION, functionId, domainId) > 0 ||
            filterFieldRepository.countFilterFieldByValueTypeAndLinkedIdAndDomainId(3, functionId, domainId) > 0) {
            throw new DataConstrainException(ErrorMessage.Function.CANNOT_DELETE_OBJECT, Resources.FUNCTION, ErrorKey.ID);
        }
    }

    public FunctionDTO getDetailFunction(Long functionId) {
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        FunctionDTO functionDTO = functionMapper.toDto(function);
        Category category = categoryRepository.findByIdAndDomainId(function.getCategoryId(), domainId);
        if (expressionRepository.countAllByInputTypeAndLinkedIdAndDomainId(Expression.InputType.FUNCTION, functionId, domainId) > 0 ||
            filterFieldRepository.countFilterFieldByValueTypeAndLinkedIdAndDomainId(3, functionId, domainId) > 0 ) {
            functionDTO.setEdit(false);
        }
        if (category != null) {
            functionDTO.setCategoryId(category.getId());
            functionDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
            functionDTO.setCategoryName(category.getName());
        }

        return functionDTO;
    }

    public List<FunctionDTO> getListChildByCategoryId(Long categoryId, Pageable pageable) {
        Page<Function> entities = (Page<Function>) (Page<?>) getListObjectByCategoryId(categoryId, pageable);
        List<FunctionDTO> dtos = entities.getContent().stream().map(functionMapper::toDto).collect(Collectors.toList());
        return dtos;
    }

    @Override
    protected OCSCloneableEntity setPropertiesOfCloneEntity(OCSCloneableEntity clonedEntity, CloneDTO cloneDTO, String newName) {
        FunctionCloneDTO functionCloneDTO = (FunctionCloneDTO) cloneDTO;
        clonedEntity.setName(newName);
        clonedEntity.setId(null);
        if (clonedEntity instanceof EventPoliciesEntity) {
            ((EventPoliciesEntity) clonedEntity).setDescription(functionCloneDTO.getDescription());
        }

        if (clonedEntity instanceof Function) {
            ((Function) clonedEntity).setModifyDate(new Timestamp((new Date()).getTime()));
            ((Function) clonedEntity).setFunctionName(functionCloneDTO.getFunctionName());
        }
        return clonedEntity;
    }

    public List<ReferTable> getDropdownLevel() {
        List<ReferTable> dropdownLevels = referTableRepository.getReferTablesByReferType(ReferType.FunctionType, sortByValue());

        return dropdownLevels;
    }

    public List<ReferTable> getDropdownDataType() {
        List<ReferTable> dropdownDataTypes = referTableRepository.getReferTablesByReferType(ReferType.DataType, sortByValue());

        return dropdownDataTypes;
    }

    public Page<FunctionDTO> getListFunctionsWithPaging(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CATEGORY, ErrorKey.ID);
        }
        Page<FunctionDTO> page = functionRepository.getListFunctionsPaging(domainId, categoryId, name, description, pageable).map(functionMapper::toDto);

        return page;
    }

    @Override
    public Map<String, Set<CategoryDTO>> checkDependencies(Long id) {
        CategoryDTO categoryDTO = new CategoryDTO();
        Map<String, Set<CategoryDTO>> dependMaps = new HashMap<>();
        Set<CategoryDTO> set = new HashSet<>();
        List<Tree> depends = new ArrayList<>();
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(id, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        if (Objects.equals(Function.FunctionType.FILTER_TYPE, function.getFunctionType())) {
            List<Filter> filters = filterRepository.getListFiltersUsingFunction(id);
            for (Filter filter : filters) {
                TreeClone tree = new FilterDTO();
                BeanUtils.copyProperties(filter, tree);
                depends.add(tree);
            }
            categoryDTO.setTemplates(depends);
            categoryDTO.setHasChild(depends.size() > 0);
            categoryDTO.setName("Filters");
        } else if (Objects.equals(Function.FunctionType.CONDITION_TYPE, function.getFunctionType())) {
            List<Condition> conditions = conditionRepository.getListConditionsUsingFunction(id);
            for (Condition condition : conditions) {
                TreeClone tree = new ConditionDTO();
                BeanUtils.copyProperties(condition, tree);
                depends.add(tree);
            }
            categoryDTO.setTemplates(depends);
            categoryDTO.setHasChild(depends.size() > 0);
            categoryDTO.setName("Conditions");
        }
        set.add(categoryDTO);
        dependMaps.put("depend", set);
        return dependMaps;
    }

    public FunctionDTO getFunctionByExpression(Long expressionId, Long functionId) {
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        FunctionDTO functionDTO = functionMapper.toDto(function);
        List<FunctionParamDTO> functionParamDTOs = functionParamMapper.toDto(functionParamRepository.findAllByFunctionIdAndDomainIdAndEnable(functionId, domainId, true));
        functionParamDTOs.sort(Comparator.comparing(FunctionParamDTO::getIndexOrder));
        for (FunctionParamDTO functionParamDTO : functionParamDTOs) {
            FunctionParamInstance instance = functionParamInstanceRepository.findFirstByDomainIdAndFunctionParameterIdAndOwnerId(
                domainId,
                functionParamDTO.getId(),
                expressionId
            );
            if (instance != null) {
                functionParamDTO.setValue(instance.getValue());
                functionParamDTO.setValueType(instance.getValueType());
            }
        }
        functionDTO.setFunctionParamDTOS(functionParamDTOs);
        return functionDTO;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Function.DUPLICATED_ALIAS, getResourceName(), ErrorKey.Function.ALIAS);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.Function.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }
}
