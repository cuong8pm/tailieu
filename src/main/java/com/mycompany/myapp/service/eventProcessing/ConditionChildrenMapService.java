package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import org.springframework.stereotype.Service;

@Service
public interface ConditionChildrenMapService extends OCSCloneMappingService {
}
