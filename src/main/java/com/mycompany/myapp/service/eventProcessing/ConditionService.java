package com.mycompany.myapp.service.eventProcessing;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.service.exception.DataConstrainException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.mapper.eventProcessing.BuilderMapper;
import com.mycompany.myapp.mapper.eventProcessing.ConditionMapper;
import com.mycompany.myapp.mapper.eventProcessing.ExpressionMapper;
import com.mycompany.myapp.mapper.eventProcessing.FunctionMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;


@Service
@Transactional
public class ConditionService extends AbstractCloneService implements ChildCloneable {

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ConditionMapper conditionMapper;

    @Autowired
    private FunctionParamService functionParamService;

    @Autowired
    private ConditionBuilderMapService conditionBuilderMapService;

    @Autowired
    private BuilderService builderService;

    @Autowired
    private FilterService filterService;

    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private ExpressionMapper expressionMapper;

    @Autowired
    private ExpressionRepository expressionRepository;

    @Autowired
    private FunctionParamInstanceRepository functionParamInstanceRepository;

    @Autowired
    private ExpressionConditionMapRepository expressionConditionMapRepository;

    @Autowired
    private List<ConditionChildrenMapService> conditionChildrenMapServices;

    @Autowired
    private FunctionParamRepository functionParamRepository;

    @Autowired
    private FunctionMapper functionMapper;

    @Autowired
    private FunctionRepository functionRepository;


    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private BuilderMapper builderMapper;

    @Autowired
    private ConditionBuilderMapRepository conditionBuilderMapRepository;

    @Autowired
    private ConditionVerificationMapRepository conditionVerificationMapRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = this.conditionRepository;
        this.parentService = this.categoryService;
        this.ocsCloneableRepository = conditionRepository;
        this.cloneMappingServices = conditionChildrenMapServices;
        this.parentDependServices = Arrays.asList(filterService);
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList();
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new ConditionDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.CONDITION;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Condition.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.CONDITION;
    }

    @Override
    protected String getNameCategory() {
        return "Conditions";
    }

    public ConditionDTO getConditionDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(id, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.CONDITION, ErrorKey.ENTITY_NOT_FOUND);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(condition.getParentId(), domainId);
        ConditionDTO conditionDTO = conditionMapper.toDto(condition);
        if (category != null) {
            conditionDTO.setCategoryId(category.getId());
            conditionDTO.setCategoryName(category.getName());
            conditionDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        Filter filter = filterRepository.findByIdAndDomainId(conditionDTO.getFilterId(), domainId);
        conditionDTO.setFilterName(filter.getName());
        conditionDTO.setHasChild(conditionBuilderMapRepository.countByParentIdAndDomainId(id, domainId).intValue() > 0);
        return conditionDTO;
    }

    public ConditionDTO createCondition(ConditionDTO conditionDTO) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionMapper.toEntity(conditionDTO);
        condition.setId(null);
        condition.setDomainId(domainId);

        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp modifyDate = Timestamp.valueOf(localDateTime);
        condition.setModifyDate(modifyDate);

        checkDuplicateName(condition.getName(), condition.getId());
        checkCategory(conditionDTO.getCategoryId());
        checkFilter(condition.getFilterId());
        setPosIndex(domainId, condition, condition.getCategoryId());
        condition = conditionRepository.save(condition);
        ConditionDTO conditionDTORes = conditionMapper.toDto(condition);
        return conditionDTORes;
    }

    public void checkFilter(Long filterId){
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId,domainId);
        if(filter == null){
            throw new ResourceNotFoundException(ErrorMessage.Filter.NOT_FOUND,Resources.FILTER, ErrorKey.Filter.filter_NAME);
        }
    }

    public ConditionDTO updateCondition(ConditionDTO conditionDTO) {
        Integer domainId = OCSUtils.getDomain();
        Condition conditionCurrent = conditionRepository.findByIdAndDomainId(conditionDTO.getId(), domainId);
        if (conditionCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.CONDITION, ErrorKey.Condition.ID);
        }
        Condition condition = conditionMapper.toEntity(conditionDTO);
        if (!Objects.equals(conditionDTO.getCategoryId(), conditionCurrent.getCategoryId())) {
            setPosIndex(domainId, condition, conditionDTO.getParentId());
        }
        if (!conditionDTO.getName().equals(conditionCurrent.getName())) {
            checkDuplicateName(condition.getName(), condition.getId());
        }
        checkCategory(conditionDTO.getCategoryId());
        checkFilter(conditionDTO.getFilterId());
        condition.setDomainId(domainId);

        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp modifyDate = Timestamp.valueOf(localDateTime);
        condition.setModifyDate(modifyDate);

        conditionRepository.save(condition);
        ConditionDTO conditionDTORes = conditionMapper.toDto(condition);
        Integer count = conditionBuilderMapRepository.countByParentIdAndDomainId(condition.getId(), domainId);
        if (count != null && count > 0) {
            conditionDTORes.setHasChild(true);
        }
        return conditionDTORes;
    }

    public void deleteCondition(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Condition conditionCurrent = conditionRepository.findByIdAndDomainId(id, domainId);
        if (conditionCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CONDITION, ErrorKey.Condition.ID);
        }

        List<ConditionVerificationMap> conditionVerificationMaps = conditionVerificationMapRepository.findByDomainIdAndParentId(domainId, id);
        List<ConditionBuilderMap> conditionBuilderMaps = conditionBuilderMapRepository.findByDomainIdAndParentId(domainId, id);
        List<Long> builderIds = conditionBuilderMaps.stream().map(ConditionBuilderMap::getBuilderId).collect(Collectors.toList());
        List<ExpressionConditionMap> expressionConditionMaps = expressionConditionMapRepository.findByDomainIdAndParentId(domainId, id);
        List<Long> expressionIds = expressionConditionMaps.stream().map(ExpressionConditionMap::getExpressionId).collect(Collectors.toList());

        //delete verification condition mapping
        conditionVerificationMapRepository.deleteAll(conditionVerificationMaps);
        //delete condition builder mapping
        conditionBuilderMapRepository.deleteAll(conditionBuilderMaps);
        //delete builders related to condition
        builderService.deleteBuilder(builderIds);
        //delete expression condition mapping
        expressionConditionMapRepository.deleteAll(expressionConditionMaps);
        //delete functionParamInstance
        functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdInAndOwnerType(domainId, expressionIds, Function.FunctionType.CONDITION_TYPE);
        //delete expressions related to condition
        expressionRepository.deleteAllByDomainIdAndIdIn(domainId, expressionIds);

        conditionRepository.deleteById(id);
    }

    public void deleteCondition(List<Long> conditionIds) {
        Integer domainId = OCSUtils.getDomain();
        List<ConditionBuilderMap> conditionBuilderMaps = conditionBuilderMapRepository.findByDomainIdAndParentIdIn(domainId, conditionIds);
        List<Long> builderIds = conditionBuilderMaps.stream().map(ConditionBuilderMap::getBuilderId).collect(Collectors.toList());
        List<ExpressionConditionMap> expressionConditionMaps = expressionConditionMapRepository.findByDomainIdAndParentIdIn(domainId, conditionIds);
        List<Long> expressionIds = expressionConditionMaps.stream().map(ExpressionConditionMap::getExpressionId).collect(Collectors.toList());
        List<ConditionVerificationMap> conditionVerificationMaps = conditionVerificationMapRepository.findByDomainIdAndParentIdIn(domainId, conditionIds);
        //delete verification condition mapping
        conditionVerificationMapRepository.deleteAll(conditionVerificationMaps);
        //delete expression condition mapping
        expressionConditionMapRepository.deleteAll(expressionConditionMaps);
        //delete condition builder mapping
        conditionBuilderMapRepository.deleteAll(conditionBuilderMaps);
        //delete functionParamInstance
        functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdInAndOwnerType(domainId, expressionIds, Function.FunctionType.CONDITION_TYPE);
        //delete expressions related to condition
        expressionRepository.deleteAllByDomainIdAndIdIn(domainId, expressionIds);
        //delete builders related to condition
        builderService.deleteBuilder(builderIds);
        //delete condition
        conditionRepository.deleteAllByDomainIdAndIdIn(domainId, conditionIds);
    }

    public Page<ConditionDTO> getConditions(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<ConditionDTO> dtos = conditionRepository
            .getConditions(categoryId, name, description, domainId, pageable)
            .map(conditionMapper::toDto);
        List<Long> ids = dtos.stream().map(i -> i.getId()).collect(Collectors.toList());
        Set<Long> set = conditionBuilderMapRepository.findDistinctAllConditionIdByDomainIdAndParentIdIn(domainId, ids);
        dtos.forEach(dto -> {
            if (set.contains(dto.getId())) {
                dto.setHasChild(true);
            }
        });
        return dtos;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return conditionRepository;
    }

    public void addExpressions(Long id, ExpressionDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(id, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.CONDITION, ErrorKey.ENTITY_NOT_FOUND);
        }
        Expression expression = expressionMapper.toEntity(dto);
        expression.setId(null);
        expression.setDomainId(domainId);
        if (expression.getValueType() == 0 && expression.getDataType() == 1) {
            expression.setValue("\"" + expression.getValue() + "\"");
        }
        expression = expressionRepository.save(expression);
        //check function null and function type
        if(dto.getInputType()==1 && dto.getLinkedId()!=null) {
            Function function = functionRepository.findByIdAndDomainId(dto.getLinkedId(), domainId);
            expressionService.checkFunction(function,domainId,1 );
            if(!Objects.equals(dto.getDataType(),function.getDataType())){
                throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        List<FunctionParamDTO> functionParamDTOS = dto.getFunctionParams();
        functionParamService.setFunctionParamInstanceValue(functionParamDTOS, dto.getLinkedId(), domainId, expression.getId(), Function.FunctionType.CONDITION_TYPE);

        ExpressionConditionMap map = new ExpressionConditionMap();
        map.setConditionId(id);
        map.setDomainId(domainId);
        map.setExpressionId(expression.getId());
        expressionConditionMapRepository.save(map);
    }

    public void deleteExpressions(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Expression expression = expressionRepository.findByIdAndDomainId(id, domainId);
        if(expression == null){
            throw new DataConstrainException(ErrorMessage.Filter.EXPRESSION_NOT_FOUND, Resources.EXPRESSION, ErrorKey.ENTITY_NOT_FOUND);
        }
        expressionConditionMapRepository.deleteByDomainIdAndExpressionId(domainId, id);
        functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, id, Function.FunctionType.CONDITION_TYPE);
        expressionRepository.deleteByIdAndDomainId(id, domainId);
    }

    public void deleteBuilders(Long builderId) {
        Integer domainId = OCSUtils.getDomain();
        conditionBuilderMapRepository.deleteByBuilderIdAndDomainId(builderId, domainId);
        builderService.deleteBuilder(Arrays.asList(builderId));
    }

    public void updateExpressionCondition(@NotNull @Positive Long id, ExpressionDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        Expression exceptiondb = expressionRepository.findByIdAndDomainId(id, domainId);
        if (exceptiondb == null) {
            throw new ResourceNotFoundException(ErrorMessage.Expression.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ENTITY_NOT_FOUND);
        }
        List<FunctionParamDTO> functionParamDTOS = dto.getFunctionParams();
        //check if input type is changed from function to object
        if (!Objects.equals(dto.getInputType(), exceptiondb.getInputType()) && Objects.equals(exceptiondb.getInputType(), 1)) {
            functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, exceptiondb.getId(), Function.FunctionType.CONDITION_TYPE);
        }
        // check if linkedId is changed and old input type and new input type are 1 then delete all old function param instances
        if (!Objects.equals(dto.getLinkedId(), exceptiondb.getLinkedId())
            && Objects.equals(dto.getInputType(), 1) && Objects.equals(exceptiondb.getInputType(), 1)) {
            functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, exceptiondb.getId(), Function.FunctionType.CONDITION_TYPE);
        }

        Expression expression = expressionMapper.toEntity(dto);
        expression.setId(id);
        expression.setDomainId(domainId);

        if (Objects.equals(expression.getValueType(), 0)) {
            expression.setValue(validateExpressionValue(dto.getValue(), dto.getDataType()));
        }
        //check function null and function type
        if(dto.getInputType()==1 && dto.getLinkedId()!=null) {
            Function function = functionRepository.findByIdAndDomainId(dto.getLinkedId(), domainId);
            expressionService.checkFunction(function, domainId,1);
            if(!Objects.equals(dto.getDataType(),function.getDataType())){
                throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        // check if list function params is not null then save to database
        functionParamService.setFunctionParamInstanceValue(functionParamDTOS, dto.getLinkedId(), domainId, expression.getId(), Function.FunctionType.CONDITION_TYPE);
        expressionRepository.save(expression);
    }

    private String validateExpressionValue(String value, Integer dataType) {
        if (Objects.equals(dataType, 1)) {
            value = "\"" + value + "\"";
        }
        return value;
    }

    @Override
    public void getChildTemplate(List<Long> ids, Map<Long, TreeClone> resultMap) {
        Map<Long, List<? extends Tree>> templates = conditionBuilderMapService.getChildren(ids);
        for (Long entityId : resultMap.keySet()) {
            TreeClone result = resultMap.get(entityId);
            List<? extends Tree> children = templates.get(result.getId());
            if (children != null) {
                result.getTemplates().addAll(children);
            }
            result.setHasChild(!result.getTemplates().isEmpty());
        }
    }

    @Override
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        List<CloneDTO> lstCloneDTos = new ArrayList<>();
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        Map<Long, List<Long>> parentChildIdMap = new HashMap<>();
        List<Long> newIds = new ArrayList<>();
        Map<Long, Long> oldNewConditionIdMap = new HashMap<>();

        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);

        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if (!Objects.equals(distinctIds.size(), baseToClones.size())) {
            throwResourceNotFoundException();
        }

        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
            .collect(Collectors.toMap(OCSCloneableEntity::getId, java.util.function.Function.identity()));

        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveEntity(cloneDTO, baseToCloneMap);
            if (!ObjectUtils.isEmpty(clonedEntity)) {
                oldNewIdMap.put(clonedEntity.getId(), cloneDTO);
                clonedEntities.add(clonedEntity);
                newIds.add(clonedEntity.getId());
            }
            cloneDTO.getChildren().forEach(cloneDTO1 -> cloneDTO1.setParentId(clonedEntity.getId()));
            lstCloneDTos.addAll(cloneDTO.getChildren());
            List<Long> childrenIds = new ArrayList<>();
            if (cloneDTO.getChildren() != null) {
                childrenIds = cloneDTO.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }
            oldNewConditionIdMap.put(cloneDTO.getId(),clonedEntity.getId());
            parentChildIdMap.put(clonedEntity.getId(), childrenIds);
        }

        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            if (cloneMappingService instanceof ConditionBuilderMapService) {
                cloneMappingService.cloneMappings(newIds, lstCloneDTos, oldNewIdMap,parentChildIdMap);
            } else if (cloneMappingService instanceof ConditionVerificationMapService) {
                cloneMappingService.cloneMappings(newIds, null, oldNewIdMap,null);
            } else if (cloneMappingService instanceof ExpressionConditionMapService) {
                cloneMappingService.cloneMappings(newIds, null, oldNewIdMap,null);
            }
        }

        return clonedEntities;
    }

    public FunctionDTO getValueDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Expression expression = expressionRepository.findByIdAndDomainId(id, domainId);
        if (expression == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ID);
        }
        FunctionDTO dto = functionMapper
            .toDto(functionRepository.findByIdAndDomainId(expression.getLinkedId(), domainId));
        if (dto != null) {
            List<FunctionParameterDTO> parameterDTOs = functionParamRepository.getListFunctionParamDetail(id, domainId,
                ReferType.DataType, Function.FunctionType.CONDITION_TYPE, expression.getLinkedId());
            dto.setFunctionParameterDTOs(parameterDTOs);
        }
        return dto;
    }

    @Override
    protected OCSCloneableEntity setPropertiesOfCloneEntity(OCSCloneableEntity clonedEntity, CloneDTO cloneDTO, String newName) {
        if (clonedEntity instanceof Condition) {
            ((Condition) clonedEntity).setModifyDate(new Timestamp((new Date()).getTime()));
            if(cloneDTO.getParentId() != null){
                ((Condition) clonedEntity).setFilterId(cloneDTO.getParentId());
            }
        }
        return super.setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);
    }

    public List<BuilderDTO> getlistChildBuilder(Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<BuilderDTO> dtos = builderMapper.toDto(builderRepository.findBuilderByCondition(id, domainId));
        return dtos;
    }

    public List<ConditionDTO> searchConditionByExpression(List<ExpressionDTO> dtos) {
        Integer domainId = OCSUtils.getDomain();
        List<List<Long>> list = new ArrayList<>();
        for (ExpressionDTO item : dtos) {
            if(item.getValue() != null){
                item.setValue(item.getValue().replaceAll("%", "\\\\%").replaceAll("_", "\\\\_"));
            }
            List<Long> ids = conditionRepository.findIdByExpression(item.getInput(), item.getDataType(),
                item.getInputType(), item.getLinkedId(), item.getValue(), domainId);
            list.add(ids);
        }
        List<Long> resultIds = new ArrayList<>();
        List<ConditionDTO> result = new ArrayList<>();
        if (list.isEmpty()) {
            return result;
        }
        List<Long> firstIds = list.get(0);
        for (Long id : firstIds) {
            Integer count = 0;
            for (List<Long> ids : list) {
                for (Long item : ids) {
                    if (Long.compare(id, item) == 0) {
                        count++;
                        break;
                    }
                }
            }
            if (count == list.size()) {
                resultIds.add(id);
            }
        }
        if (resultIds.isEmpty()) {
            return result;
        }
        result = conditionMapper.toDto(conditionRepository.findByIdInAndDomainId(resultIds, domainId));
        return result;

    }

    public Page<ConditionDTO> getConditionByEventPolicyId(Long eventPolicyId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<ConditionDTO> conditionDTOs = conditionRepository.findAllByEventPolicyIdAndDomainId(eventPolicyId, domainId, pageable);
        return conditionDTOs;
    }

    public List<BuilderDTO> validatorConditions(ConditionDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateName(dto.getName(), dto.getId());
        checkCategory(dto.getCategoryId());
        List<Integer> errorRows = new ArrayList<>();
        // validate expressions
        List<ExpressionDTO> expressionDTOs = dto.getExpressions();
        for (int i = 0; i < expressionDTOs.size(); i++) {
            ExpressionDTO expressionDTO = expressionDTOs.get(i);
            //check function null and function type
            if(expressionDTO.getInputType()==1 && expressionDTO.getLinkedId()!=null) {
                Function function = functionRepository.findByIdAndDomainId(expressionDTO.getLinkedId(), domainId);
                checkFunction(function, domainId,1);
                if(!java.util.Objects.equals(expressionDTO.getDataType(),function.getDataType())){
                    if(!errorRows.contains(i+1)){
                        errorRows.add(i+1);
                    }
                }
            }
            functionParamService.validateFunctionParams(errorRows, i,expressionDTO.getFunctionParams(), expressionDTO.getLinkedId(), domainId, expressionDTO.getId(),Function.FunctionType.CONDITION_TYPE);
        }
        if(errorRows.size() > 0){
            errorRows.stream().distinct();
            StringBuilder rows = new StringBuilder("[");
            for (Integer errorRow : errorRows) {
                rows.append(errorRow.intValue() +",");
            }
            rows.deleteCharAt(rows.length()-1);
            rows.append("]");
            throw new DataConstrainException(rows.toString(), Resources.EVENT_POLICY, ErrorKey.EventPolicy.EXPRESSION_HAS_BEEN_CHANGED);
        }

        //validate builders
        errorRows = new ArrayList<>();
        List<BuilderDTO> builders = dto.getBuilders();
        if (!CollectionUtils.isEmpty(builders)) {
            for (int i = 0; i < builders.size(); i++) {
                BuilderDTO builder = builders.get(i);
                if(builder.getId() != null && builder.getId() > 0){
                    Builder entity = builderRepository.findByIdAndDomainId(builder.getId(), domainId);
                    if(entity == null){
                        if(!errorRows.contains(i+1)){
                            errorRows.add(i+1);
                        }
                    }
                }
            }
        }

        if(errorRows.size() > 0){
            errorRows.stream().distinct();
            StringBuilder rows = new StringBuilder("[");
            for (Integer errorRow : errorRows) {
                rows.append(errorRow.intValue() +",");
            }
            rows.deleteCharAt(rows.length()-1);
            rows.append("]");
            throw new DataConstrainException(rows.toString(), Resources.EVENT_POLICY, ErrorKey.EventPolicy.BUILDER_HAS_BEEN_CHANGED);
        }

        //validate verifications
        errorRows = new ArrayList<>();
        List<VerificationDTO> verificationDTOs = dto.getVerifications();
        if(!CollectionUtils.isEmpty(verificationDTOs)){
            for (int i = 0; i < verificationDTOs.size(); i++) {
                VerificationDTO verificationDTO = verificationDTOs.get(i);
                Verification entity = verificationRepository.findByIdAndDomainId(verificationDTO.getId(),domainId);
                if(entity == null){
                    if(!errorRows.contains(i+1)){
                        errorRows.add(i+1);
                    }
                }
            }
        }

        if(errorRows.size() > 0){
            errorRows.stream().distinct();
            StringBuilder rows = new StringBuilder("[");
            for (Integer errorRow : errorRows) {
                rows.append(errorRow.intValue() +",");
            }
            rows.deleteCharAt(rows.length()-1);
            rows.append("]");
            throw new DataConstrainException(rows.toString(), Resources.EVENT_POLICY, ErrorKey.EventPolicy.VERIFICATION_HAS_BEEN_CHANGED);
        }

        if(CollectionUtils.isEmpty(builders)){
            return new ArrayList<>();
        } else {
            List<Long> builderIds = builders.stream().map(BuilderDTO::getId).collect(Collectors.toList());
            List<BuilderDTO> routers = builderRepository.getBuilderRouterByBuilderTypeIds(builderIds, domainId);
            return routers;
        }

    }

    public void checkFunction(Function function,Integer domainId,Integer functionType){
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.Function.NOT_FOUND, Resources.FUNCTION, ErrorKey.Function.ALIAS_ID);
        }
        if(!java.util.Objects.equals(function.getFunctionType(), functionType)){
            throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
        }
    }

    public ConditionDTO getDetailConditionClone(@NotNull @Positive Long id) {
        Integer domainId = OCSUtils.getDomain();
        ConditionDTO dto = getConditionDetail(id);
        dto.setId(0l);
        dto.setName(cloneName(dto.getName(), domainId));
        return dto;
    }

    @Override
    protected List<Long> getListDependId(List<OCSMoveableEntity> dependDbs) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> ids = dependDbs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
        return conditionRepository.findFilterIdsByConditionId(ids, domainId);
    }

    @Override
    protected List<OCSCloneableMap> getMappingFoundEntities(List<Long> ids) {
        List<OCSCloneableMap> mapping = new ArrayList();
        mapping.addAll(conditionBuilderMapService.getMappingByParent(ids));
        return mapping;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Condition.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }
}
