package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.VerificationConditionDTO;
import com.mycompany.myapp.dto.eventProcessing.VerificationDTO;
import com.mycompany.myapp.mapper.eventProcessing.VerificationMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionRepository;
import com.mycompany.myapp.repository.eventProcessing.FilterRepository;
import com.mycompany.myapp.repository.eventProcessing.VerificationItemRepository;
import com.mycompany.myapp.repository.eventProcessing.VerificationRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("verificationService")
@Transactional
public class VerificationService extends AbstractCloneService {
    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private VerificationItemRepository verificationItemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private VerificationMapper verificationMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private VerificationItemService verificationItemService;

    @Autowired
    private ConditionVerificationMapService conditionVerificationMapService;

    @Autowired
    private List<ConditionVerificationMapService> conditionVerificationMapServices;

    @Autowired
    private ConditionRepository conditionRepository;

    @Override
    @PostConstruct
    public void setBaseRepository() {
        this.baseRepository = this.verificationRepository;
        this.parentService = this.categoryService;
        this.parentDependMappingServices = conditionVerificationMapServices;
        this.ocsCloneableRepository = verificationRepository;
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return null;
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return null;
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new VerificationDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.VERIFICATION;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return null;
    }

    @Override
    protected String getResourceName() {
        return Resources.VERIFICATION;
    }

    @Override
    protected String getNameCategory() {
        return "Verification";
    }

    public VerificationDTO getVerificationDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Verification verification = verificationRepository.findByIdAndDomainId(id, domainId);
        if (verification == null) {
            throw new ResourceNotFoundException(ErrorMessage.Verification.NOT_FOUND, Resources.VERIFICATION, ErrorKey.Builder.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(verification.getParentId(), domainId);
        VerificationDTO verificationDTO = verificationMapper.toDto(verification);
        if (category != null) {
            verificationDTO.setCategoryId(category.getId());
            verificationDTO.setCategoryName(category.getName());
            verificationDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        return verificationDTO;
    }

    public void deleteVerification(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Verification verification = verificationRepository.findByIdAndDomainId(id, domainId);
        if (verification != null) {
            if (conditionVerificationMapService.existByVerificationId(id)) {
                throw new ResourceConflictException(
                    ErrorMessage.CANNOT_DELETE_ASSOCIATION,
                    Resources.VERIFICATION,
                    ErrorKey.Verification.ID
                );
            }
            List<VerificationItem> verificationItems = verificationItemRepository.findAllByVerificationIdAndDomainId(
                verification.getId(),
                domainId
            );
            if (!verificationItems.isEmpty()) {
                for (VerificationItem verificationItem : verificationItems) {
                    verificationItemService.deleteVerificationItem(verificationItem.getId());
                }
            }
            verificationRepository.deleteByIdAndDomainId(id, domainId);
        }
    }

    public VerificationDTO createVerification(VerificationDTO verificationDTO) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(verificationDTO.getCategoryId(), domainId);
        if(category == null){
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND,Resources.CATEGORY,ErrorKey.CATEGORY_ID);
        }
        Verification verification = verificationMapper.toEntity(verificationDTO);
        verification.setId(null);
        verification.setDomainId(domainId);
        checkDuplicateName(verification.getName(), verification.getId());
        checkCategory(verificationDTO.getCategoryId());
        setPosIndex(domainId, verification, verification.getCategoryId());
        verification = verificationRepository.save(verification);
        VerificationDTO verificationDTORes = verificationMapper.toDto(verification);
        return verificationDTORes;
    }

    public VerificationDTO updateVerification(VerificationDTO verificationDTO) {
        Integer domainId = OCSUtils.getDomain();
        Verification verificationCurrent = verificationRepository.findByIdAndDomainId(verificationDTO.getId(), domainId);
        if (verificationCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.Verification.NOT_FOUND, Resources.VERIFICATION, ErrorKey.Verification.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(verificationDTO.getCategoryId(), domainId);
        if(category == null){
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND,Resources.CATEGORY,ErrorKey.CATEGORY_ID);
        }
        Verification verification = verificationMapper.toEntity(verificationDTO);
        if (!Objects.equals(verificationDTO.getCategoryId(), verificationCurrent.getCategoryId())) {
            setPosIndex(domainId, verification, verificationDTO.getCategoryId());
        }
        if (!verificationDTO.getName().equals(verificationCurrent.getName())) {
            checkDuplicateName(verification.getName(), verification.getId());
        }
        checkCategory(verificationDTO.getCategoryId());
        verification.setDomainId(domainId);
        verificationRepository.save(verification);
        VerificationDTO verificationDTORes = getVerificationDetail(verificationDTO.getId());
        return verificationDTORes;
    }

    public Page<VerificationDTO> getVerifications(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<VerificationDTO> verificationDTOPage = verificationRepository
            .getVerifications(categoryId, name, description, domainId, pageable)
            .map(verificationMapper::toDto);
        return verificationDTOPage;
    }

    public List<ReferTable> getVerificationTypes() {
        return referTableRepository.getReferTablesByReferType(ReferType.VerificationType, sortByValue());
    }

    public List<ReferTable> getListTypes() {
        return referTableRepository.getReferTablesByReferType(ReferType.ListType, sortByValue());
    }

    public Page<VerificationDTO> getListVerificationByConditionId(Long ConditionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(ConditionId, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CONDITION, ErrorKey.ID);
        }
        Page<VerificationDTO> page = verificationRepository.getListVerificationByConditionId(ConditionId, pageable);
        return page;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Verification.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.Verification.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }
}
