package com.mycompany.myapp.service.eventProcessing;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeDTO;
import com.mycompany.myapp.mapper.eventProcessing.BuilderTypeMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRouterMapRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Transactional
@Service
public class BuilderTypeService extends AbstractCloneService {

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BuilderTypeMapper builderTypeMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private BuilderTypeItemService itemService;

    @Autowired
    private BuilderTypeRouterMapService builderTypeRouterMapService;

    @Autowired
    private BuilderTypeRouterMapRepository builderTypeRouterMapRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = builderTypeRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = builderTypeRepository;
        this.childServices = Arrays.asList(itemService);
        this.cloneMappingServices = Arrays.asList(builderTypeRouterMapService);
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList();
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new BuilderTypeDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.BUILDER_TYPE;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return BuilderType.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.BUILDER_TYPE;
    }

    @Override
    protected String getNameCategory() {
        return "Builder Type";
    }

    public BuilderTypeDTO createBuilderType(BuilderTypeDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkCategory(dto.getCategoryId());
        BuilderType builderType = builderTypeMapper.toEntity(dto);
        builderType.setId(null);
        checkDuplicateName(builderType.getName(), builderType.getId());
        setPosIndex(domainId, builderType, builderType.getCategoryId());
        builderType.setDomainId(domainId);
        builderType = builderTypeRepository.save(builderType);
        BuilderTypeDTO builderTypeDTO = builderTypeMapper.toDto(builderType);

        return builderTypeDTO;
    }

    public BuilderTypeDTO updateBuilderType(BuilderTypeDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderTypeOld = builderTypeRepository.findByIdAndDomainId(dto.getId(), domainId);
        if (builderTypeOld == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        checkCategory(dto.getCategoryId());
        checkDuplicateName(dto.getName(), dto.getId());
        BuilderType builderTypeNew = builderTypeMapper.toEntity(dto);
        if (!Objects.equal(builderTypeNew.getCategoryId(), builderTypeOld.getCategoryId())) {
            setPosIndex(domainId, builderTypeNew, dto.getCategoryId());
        }
        builderTypeNew.setDomainId(domainId);
        builderTypeNew = builderTypeRepository.save(builderTypeNew);
        dto = builderTypeMapper.toDto(builderTypeNew);

        return dto;
    }

    public void deleteBuilderType(Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderTypeOld = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderTypeOld == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        if (builderRepository.countBuildersByBuilderTypeIdAndDomainId(builderTypeId, domainId) > 0) {
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        builderTypeRouterMapRepository.deleteAllByDomainIdAndBuilderTypeId(domainId, builderTypeId);
        itemService.deleteBuilderTypeItemsByBuilderTypeId(builderTypeId);
        builderTypeRepository.deleteByIdAndDomainId(builderTypeId, domainId);
    }

    public BuilderTypeDTO getDetailBuilderTypeById(Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        BuilderTypeDTO builderTypeDTO = builderTypeMapper.toDto(builderType);
        Category category = categoryRepository.findByIdAndDomainId(builderType.getParentId(), domainId);
        if (category != null) {
            builderTypeDTO.setCategoryId(category.getId());
            builderTypeDTO.setCategoryName(category.getName());
            builderTypeDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }

        return builderTypeDTO;
    }

    public List<BuilderTypeDTO> getListChildByCategoryId(Long categoryId, Pageable pageable) {
        Page<BuilderType> entities = (Page<BuilderType>) (Page<?>) getListObjectByCategoryId(categoryId, pageable);
        List<BuilderTypeDTO> dtos = entities.getContent().stream().map(builderTypeMapper::toDto).collect(Collectors.toList());
        dtos.sort(Comparator.comparing(BuilderTypeDTO::getPosIndex));
        return dtos;
    }

    public Page<BuilderTypeDTO> getListBuilderTypesPaging(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CATEGORY, ErrorKey.ID);
        }
        Page<BuilderTypeDTO> page = builderTypeRepository.getListBuilderTypeWithPaging(domainId, categoryId, name, description, pageable).map(builderTypeMapper::toDto);
        return page;
    }

    public Map<Long, String> getListBuilderTypesByBuilderIds(List<Long> builderIds){
        Integer domainId = OCSUtils.getDomain();
        List<BuilderType> builderTypes = builderTypeRepository.getListBuilderTypeByBuilderIds(builderIds,domainId);
        Map<Long, String> builderMap = builderTypes.stream().collect(Collectors.toMap(BuilderType::getId, BuilderType::getName));

        return builderMap;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.BuilderType.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.BuilderType.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }

}
