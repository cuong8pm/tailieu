package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.mapper.eventProcessing.ExpressionMapper;
import com.mycompany.myapp.mapper.eventProcessing.FunctionMapper;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExpressionService extends AbstractCloneService {

    @Autowired
    private ExpressionRepository expressionRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ExpressionFilterMapService expressionFilterMapService;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private ExpressionMapper expressionMapper;

    @Autowired
    private FunctionParamInstanceRepository functionParamInstanceRepository;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private FunctionRepository functionRepository;

    @Autowired
    private FunctionMapper functionMapper;

    @Autowired
    private FunctionParamRepository functionParamRepository;

    @Autowired
    private FunctionParamService functionParamService;

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList();
    }

    @Override
    protected TreeClone getTreeDTO() {
        return null;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.EXPRESSION;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Expression.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.EXPRESSION;
    }

    @Override
    protected String getNameCategory() {
        return null;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = expressionRepository;
        this.ocsCloneableRepository = expressionRepository;
        this.parentService = categoryService;
        this.parentDependMappingServices = Arrays.asList(expressionFilterMapService);
    }

    public Page<ExpressionConditionDTO> getListExpressionByFilterId(Long filterId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        Page<ExpressionConditionDTO> page;
        Sort.Order order = pageable.getSort().getOrderFor("input");
        if (order == null) {
            page = expressionRepository.getExpressionsByFilterId(filterId, pageable);
        } else {
            List<ExpressionConditionDTO> list = expressionRepository.getListExpressionsByFilterId(filterId);
            int start = (int) pageable.getOffset();
            int end = Math.min((start + pageable.getPageSize()), list.size());
            Sort.Direction direction = order.getDirection();
            if (direction.toString().equals("DESC")) {
//                list.sort(Comparator.comparing(ExpressionConditionDTO::getInput).reversed());
                list.sort((c1, c2) -> c2.getInput().compareToIgnoreCase(c1.getInput()));
            } else {
//                list.sort(Comparator.comparing(ExpressionConditionDTO::getInput));
                list.sort((c1, c2) -> c1.getInput().compareToIgnoreCase(c2.getInput()));
            }
            page = new PageImpl<ExpressionConditionDTO>(list.subList(start, end), pageable, list.size());
        }
        for (ExpressionConditionDTO item : page) {
            if (item.getValueType() == 0 && item.getDataType() == 1) {
                item.setValue(getStringValue(item.getValue()));
            }

            if(item.getInputType() == 1 && item.getLinkedId() != null){
                List<FunctionParamDTO> functionParamDTOs = functionParamRepository
                    .getListFunctionParam(item.getId(),domainId,ReferType.DataType,Function.FunctionType.FILTER_TYPE,item.getLinkedId());
                item.setFunctionParams(functionParamDTOs);
            }
        }
        return page;
    }

    public Page<ExpressionConditionDTO> getListExpressionByConditionId(Long ConditionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(ConditionId, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CONDITION, ErrorKey.ID);
        }
        Page<ExpressionConditionDTO> page;
        Sort.Order order = pageable.getSort().getOrderFor("input");
        if (order == null) {
            page = expressionRepository.getExpressionByConditionId(ConditionId, pageable);
            ;
        } else {
            List<ExpressionConditionDTO> list = expressionRepository.getListExpressionByConditionId(ConditionId);
            int start = (int) pageable.getOffset();
            int end = Math.min((start + pageable.getPageSize()), list.size());
            Sort.Direction direction = order.getDirection();
            if (direction.toString().equals("DESC")) {
                list.sort((c1, c2) -> c2.getInput().compareToIgnoreCase(c1.getInput()));
            } else {
                list.sort((c1, c2) -> c1.getInput().compareToIgnoreCase(c2.getInput()));
            }
            page = new PageImpl<ExpressionConditionDTO>(list.subList(start, end), pageable, list.size());
        }
        for (ExpressionConditionDTO item : page) {
            if (item.getValueType() == 0 && item.getDataType() == 1) {
                item.setValue(getStringValue(item.getValue()));
            }
        }
        return page;
    }

    public ExpressionDTO createExpression(Long filterId, ExpressionDTO expressionDTO) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }

        Expression expression = expressionMapper.toEntity(expressionDTO);
        if (Objects.equals(expression.getValueType(), 0)) {
            expression.setValue(validateExpressionValue(expressionDTO.getValue(), expressionDTO.getDataType()));
        }
        expression.setId(null);
        expression.setDomainId(domainId);
        expression = expressionRepository.save(expression);
        //check function null and function type
        if(expressionDTO.getInputType()==1 && expressionDTO.getLinkedId()!=null) {
            Function function = functionRepository.findByIdAndDomainId(expressionDTO.getLinkedId(), domainId);
            checkFunction(function, domainId,0);
            if(!Objects.equals(expressionDTO.getDataType(),function.getDataType())){
                throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        List<FunctionParamDTO> functionParamDTOS = expressionDTO.getFunctionParams();
        functionParamService.setFunctionParamInstanceValue(functionParamDTOS, expressionDTO.getLinkedId(), domainId, expression.getId(), Function.FunctionType.FILTER_TYPE );

        expressionFilterMapService.createExpressionFilterMap(filterId, expression.getId());
        expressionDTO = expressionMapper.toDto(expression);
        return expressionDTO;
    }

    public ExpressionDTO getDetailExpression(Long filterId, Long expressionId) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        Expression expression = expressionRepository.findByIdAndDomainId(expressionId, domainId);
        if (expression == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ID);
        }

        return expressionMapper.toDto(expression);
    }

    public void deleteExpression(Long filterId, Long expressionId) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        Expression expression = expressionRepository.findByIdAndDomainId(expressionId, domainId);
        if (expression == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ID);
        }

        functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, expressionId, 0);
        expressionFilterMapService.deleteMapping(filterId, expressionId);
        expressionRepository.deleteByIdAndDomainId(expressionId, domainId);
    }

    public ExpressionDTO updateExpression(Long filterId, ExpressionDTO expressionDTO) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        Expression oldExpression = expressionRepository.findByIdAndDomainId(expressionDTO.getId(), domainId);
        if (oldExpression == null) {
            throw new ResourceNotFoundException(ErrorMessage.Expression.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ENTITY_NOT_FOUND);

        }
        //check function null and function type
        if(expressionDTO.getInputType()==1 && expressionDTO.getLinkedId()!=null) {
            Function function = functionRepository.findByIdAndDomainId(expressionDTO.getLinkedId(), domainId);
            checkFunction(function, domainId,0);
            if(!Objects.equals(expressionDTO.getDataType(),function.getDataType())){
                throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
            }
        }

        List<FunctionParamDTO> functionParamDTOS = expressionDTO.getFunctionParams();
        //check if input type is changed from function to object
        if (!Objects.equals(expressionDTO.getInputType(), oldExpression.getInputType()) && Objects.equals(oldExpression.getInputType(), 1)) {
            functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, oldExpression.getId(), Function.FunctionType.FILTER_TYPE);
        }
        // check if linkedId is changed and old input type and new input type are 1 then delete all old function param instances
        if (!Objects.equals(expressionDTO.getLinkedId(), oldExpression.getLinkedId())
            && Objects.equals(expressionDTO.getInputType(), 1) && Objects.equals(oldExpression.getInputType(), 1)) {
            functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, oldExpression.getId(), Function.FunctionType.FILTER_TYPE);
        }
        // get information of updated expression
        Expression newExpression = expressionMapper.toEntity(expressionDTO);
        newExpression.setDomainId(domainId);
        if (Objects.equals(newExpression.getValueType(), 0)) {
            newExpression.setValue(validateExpressionValue(expressionDTO.getValue(), expressionDTO.getDataType()));
        }
        newExpression = expressionRepository.save(newExpression);
        final Long ownerId = newExpression.getId();
        // check if list function params is not null then save to database
        functionParamService.setFunctionParamInstanceValue(functionParamDTOS, expressionDTO.getLinkedId(), domainId, expressionDTO.getId(), Function.FunctionType.FILTER_TYPE);
        expressionDTO = expressionMapper.toDto(newExpression);

        return expressionDTO;
    }
    public void checkFunction(Function function,Integer domainId,Integer functionType){
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.Function.NOT_FOUND, Resources.FUNCTION, ErrorKey.Function.ALIAS_ID);
        }
        if(!Objects.equals(function.getFunctionType(), functionType)){
            throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
        }
    }

    private String getValueInstance(FunctionParameterDTO functionParamDTO) {
        String value = functionParamDTO.getValue();
        if (Objects.equals(functionParamDTO.getValueType(), 0)
            && Objects.equals(functionParamDTO.getDataType(), 1)) {
            return getStringValue(value);
        }
        return value;
    }

    private String getStringValue(String oldValue) {
        String value = oldValue;
        if (!StringUtils.isEmpty(value) && value.charAt(0) == '\"') {
            value = value.substring(1);
        }
        if (!StringUtils.isEmpty(value) && value.charAt(value.length() - 1) == '\"') {
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }

    public void findExpressionByExpressionIdAndLinkedId(Long expressionId, Long linkedId) {
        Integer domainId = OCSUtils.getDomain();
        Expression expression = expressionRepository.findFirstByIdAndLinkedIdAndDomainIdAndInputType(expressionId, linkedId, domainId, 1);
        if (expression == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ID);
        }
    }

    private String validateExpressionValue(String value, Integer dataType) {
        if (Objects.equals(dataType, 1)) {
            value = "\"" + value + "\"";
        }
        return value;
    }

    public List<OCSCloneableEntity> cloneExpressions(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap, Integer expressionType) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        Map<Long, Long> oldNewExpressionIdMap = new HashMap<>();
        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);
        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
            .collect(Collectors.toMap(OCSCloneableEntity::getId, java.util.function.Function.identity()));

        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveEntity(cloneDTO, baseToCloneMap);
            if (!ObjectUtils.isEmpty(clonedEntity)) {
                oldNewIdMap.put(clonedEntity.getId(), cloneDTO);
                clonedEntities.add(clonedEntity);
                oldNewExpressionIdMap.put(cloneDTO.getId(), clonedEntity.getId());
            }
        }
        oldNewChildMap.putAll(oldNewIdMap);

        //clone function param instance
        List<FunctionParamInstance> cloneInstances = new ArrayList<>();
        List<FunctionParamInstance> instanceToClones = functionParamInstanceRepository.findAllByDomainIdAndOwnerTypeAndOwnerIdIn(domainId, expressionType, ids);
        for (FunctionParamInstance instanceToClone : instanceToClones) {
            FunctionParamInstance cloneInstance = SerializationUtils.clone(instanceToClone);
            cloneInstance.setId(null);
            cloneInstance.setOwnerId(oldNewExpressionIdMap.get(instanceToClone.getOwnerId()));
            cloneInstances.add(cloneInstance);
        }
        functionParamInstanceRepository.saveAll(cloneInstances);

        return clonedEntities;
    }

    @Override
    public OCSCloneableEntity saveEntity(CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> baseToCloneMap) {
        if (ocsCloneableRepository == null) {
            throw new UnsupportedOperationException("Method is not ready");
        }
        Integer domainId = OCSUtils.getDomain();
        Long idToClone = cloneDTO.getId();
        Expression baseToClone = (Expression) baseToCloneMap.get(idToClone);
        if (baseToClone == null) {
            return null;
        }
        Expression clonedEntity = SerializationUtils.clone(baseToClone);
        setPosIndex(domainId, clonedEntity, clonedEntity.getParentId());
        clonedEntity.setId(null);
        clonedEntity = expressionRepository.save(clonedEntity);

        return clonedEntity;
    }

    public ExpressionDTO getExpressionFunctionDetail(@NotNull @Positive Long id, Integer type) {
        Integer domainId = OCSUtils.getDomain();
        Expression expression = expressionRepository.findByIdAndDomainId(id, domainId);
        if (expression == null) {
            throw new ResourceNotFoundException(ErrorMessage.Filter.EXPRESSION_NOT_FOUND, Resources.EXPRESSION, ErrorKey.ENTITY_NOT_FOUND);
        }
        List<FunctionParamDTO> functionParamDTOs = new ArrayList<>();
        List<FunctionParameterDTO> parameterDTOs = functionParamRepository.getListFunctionParamDetail(id, domainId,
            ReferType.DataType, type, expression.getLinkedId());
        for (FunctionParameterDTO item : parameterDTOs) {
            FunctionParamDTO functionParamDTO = new FunctionParamDTO();
            functionParamDTO.setId(item.getId());
            functionParamDTO.setName(item.getName());
            functionParamDTO.setDataType(item.getDataType());
            functionParamDTO.setValue(getValueInstance(item));
            functionParamDTO.setValueType(item.getValueType());
            functionParamDTO.setEnable(item.getEnable());
            functionParamDTOs.add(functionParamDTO);
        }
        FunctionDTO dto = functionMapper
            .toDto(functionRepository.findByIdAndDomainId(expression.getLinkedId(), domainId));
        ExpressionDTO expressionDTO = expressionMapper.toDto(expression);
        if (expressionDTO.getValueType() == 0 && expressionDTO.getDataType() == 1) {
            expressionDTO.setValue(getStringValue(expressionDTO.getValue()));
        }
        expressionDTO.setFunctionParams(functionParamDTOs);
        expressionDTO.setFunctionAlias(dto.getName());
        expressionDTO.setFunctionName(dto.getFunctionName());

        return expressionDTO;
    }
}
