package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Language;
import com.mycompany.myapp.dto.eventProcessing.LanguageDTO;
import com.mycompany.myapp.mapper.eventProcessing.LanguageMapper;
import com.mycompany.myapp.repository.eventProcessing.LanguageRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.utils.OCSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LanguageService extends OCSBaseService {

    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private LanguageMapper languageMapper;

    @PostConstruct
    @Override
    public void setBaseRepository() {

    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    public List<LanguageDTO> getLanguages() {
        Integer domainId = OCSUtils.getDomain();
        List<Language> languages = languageRepository.findByDomainId(domainId);
        List<LanguageDTO> languageDTOs = languageMapper.toDto(languages);
        return languageDTOs;
    }

}
