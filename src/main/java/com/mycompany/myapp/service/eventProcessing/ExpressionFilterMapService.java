package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.eventProcessing.Expression;
import com.mycompany.myapp.domain.eventProcessing.ExpressionFilterMap;
import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.eventProcessing.ExpressionDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ExpressionFilterMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ExpressionRepository;
import com.mycompany.myapp.repository.eventProcessing.FilterRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import javax.annotation.PostConstruct;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
public class ExpressionFilterMapService extends AbstractCloneMappingService {
    @Autowired
    private ExpressionFilterMapRepository expressionFilterMapRepository;

    @Autowired
    private FilterService filterService;

    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private ExpressionRepository expressionRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = expressionFilterMapRepository;
        this.childService = expressionService;
        this.parentService = filterService;
        this.parentDependService = filterService;
    }

    @Override
    protected String getResource() {
        return Resources.EXPRESSION;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }

    public void createExpressionFilterMap(Long filterId, Long expressionId) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        Expression expression = expressionRepository.findByIdAndDomainId(expressionId, domainId);
        if (expression == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EXPRESSION, ErrorKey.ID);
        }
        ExpressionFilterMap expressionFilterMap = new ExpressionFilterMap();
        expressionFilterMap.setExpressionId(expressionId);
        expressionFilterMap.setFilterId(filterId);
        expressionFilterMap.setDomainId(domainId);

        expressionFilterMapRepository.save(expressionFilterMap);
    }

    @Override
    public void deleteMapping(Long parentId, Long childId) {
        Integer domainId = OCSUtils.getDomain();
        OCSCloneableMap ocsCloneableMap = ocsCloneMapRepository.findByDomainIdAndChildIdAndParentId(domainId, childId, parentId);
        if (ocsCloneableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, getResource(), ErrorKey.ID);
        }

        ocsCloneMapRepository.deleteByDomainIdAndChildIdAndParentId(domainId, childId, parentId);
    }

    @Override
    public List<OCSCloneableMap> getMappingByParent(List<Long> ids) {
        return new ArrayList<>();
    }

    // không truyền vào cloneDTOs và map cha mới- list con cũ || tạo mới tất cả expression, ko được mapping condition mới - expression cũ
    @Override
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        if (ocsCloneMapRepository == null || childService == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }
        Integer domainId = OCSUtils.getDomain();
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        Map<Long, Long> newOldParentIdMap = new HashMap<>();
        Map<Long, List<Long>> oldChildNewParentsMap = new HashMap<>();
        long batchSize = 30;
        long count = 0;

        for (Long newId : oldNewIdParentMap.keySet()) {
            newOldParentIdMap.put(newId, oldNewIdParentMap.get(newId).getId());
        }

        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());

        //thêm mapping giữa con cũ - cha mới
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);
        List<Long> oldChildIds = mappingParents.stream().map(OCSCloneableMap::getChildId).collect(Collectors.toList());
        mappingParents.forEach(item ->{
            List<Long> newConditionIds = oldNewIdParentMap.entrySet().stream()
                .filter(entry -> Objects.equals(entry.getValue().getId(),item.getParentId()))
                .map(Map.Entry::getKey).collect(Collectors.toList());
            oldChildNewParentsMap.put(item.getChildId(),newConditionIds);
        });

        // query trong db lấy list expression để clone
        List<Expression> expressionToClones = expressionRepository.findByDomainIdAndIdIn(domainId,oldChildIds);
        List<CloneDTO> idsToCreateNew = new ArrayList<>();

        expressionToClones.forEach(item ->{
            List<Long> parentsIds = oldChildNewParentsMap.get(item.getId());
            if(parentsIds != null){
                parentsIds.forEach(id ->{
                    CloneDTO cloneDTO = new CloneDTO();
                    cloneDTO.setId(item.getId());
                    cloneDTO.setParentId(id);
                    idsToCreateNew.add(cloneDTO);
                });
            }
        });

        // clone tầng tiếp theo
        expressionService.cloneExpressions(idsToCreateNew, oldNewChildMap, Function.FunctionType.FILTER_TYPE);

        List<OCSCloneableMap> newMap = new ArrayList<>();
        for (Long newParentId : newParentIds) {
            Long oldParentId = newOldParentIdMap.get(newParentId);
            List<OCSCloneableMap> oldMappingParents = mappingParents.stream().filter(i -> Objects.equals(i.getParentId(), oldParentId)).collect(Collectors.toList());
            for (OCSCloneableMap oldMappingParent : oldMappingParents) {
                Long childId = getChildId(oldMappingParent,null,oldNewChildMap, newParentId);
                if(childId != null){
                    OCSCloneableMap newCloneMap = SerializationUtils.clone(oldMappingParent);
                    newCloneMap.setChildMappingId(childId);
                    newCloneMap.clearId();
                    newCloneMap.setParentMappingId(newParentId);
                    newMap.add(newCloneMap);

                    entityManager.persist(newCloneMap);
                    if (count % batchSize == 0 && count > 0) {
                        entityManager.flush();
                        entityManager.clear();
                        newMap.clear();
                    }
                    count++;
                }
            }

        }

        if (!CollectionUtils.isEmpty(newMap)) {
            entityManager.flush();
            entityManager.clear();
            newMap.clear();
        }
    }

    @Override
    public Long getChildId(OCSCloneableMap mappingParent, List<Long> childrenIds, Map<Long, CloneDTO> oldNewChildMap, Long newParentId) {
        // toàn bộ expression là tạo mới và mapping với cha mới
        Optional<Long> newChildId = oldNewChildMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue().getId(), mappingParent.getChildId())
                && Objects.equals(newParentId, entry.getValue().getParentId()))
            .map(Map.Entry::getKey)
            .findAny();

        if (newChildId.isPresent()) {
            return newChildId.get();
        }
        return null;
    }
}
