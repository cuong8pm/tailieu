package com.mycompany.myapp.service.eventProcessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.eventProcessing.DropdownDTO;
import com.mycompany.myapp.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.BuilderConditionDTO;
import com.mycompany.myapp.dto.eventProcessing.BuilderDTO;
import com.mycompany.myapp.mapper.eventProcessing.BuilderMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderItemRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionBuilderMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionRepository;
import com.mycompany.myapp.repository.eventProcessing.FilterRepository;
import com.mycompany.myapp.repository.eventProcessing.NotifyRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service("builderService")
@Transactional
public class BuilderService extends AbstractCloneService {
    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BuilderMapper builderMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private NotifyRepository notifyRepository;

    @Autowired
    private BuilderItemRepository builderItemRepository;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private ConditionBuilderMapService conditionBuilderMapService;

    @Autowired
    private List<ChildBuilderService> childBuilderServices;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @Autowired
    private ConditionBuilderMapRepository conditionBuilderMapRepository;

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList();
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new BuilderDTO();
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Builder.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.BUILDER;
    }

    @Override
    protected String getNameCategory() {
        return "Builder";
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = this.builderRepository;
        this.parentService = this.categoryService;
        this.ocsCloneableRepository = builderRepository;
        this.childServices = childBuilderServices;
        this.parentDependMappingServices = Arrays.asList(conditionBuilderMapService);
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.BUILDER;
    }

    public BuilderDTO getBuilderDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(id, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(builder.getParentId(), domainId);
        ConditionBuilderMap conditionBuilderMap = conditionBuilderMapRepository.findFirstByBuilderIdAndDomainId( builder.getId(), domainId);
        Condition condition = new Condition();
        if(conditionBuilderMap != null){
            condition = conditionRepository.findByIdAndDomainId( conditionBuilderMap.getConditionId(), domainId);
        }

        BuilderDTO builderDTO = builderMapper.toDto(builder);
        if (category != null) {
            builderDTO.setCategoryId(category.getId());
            builderDTO.setCategoryName(category.getName());
            builderDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
            builderDTO.setConditionId(condition == null ? null : condition.getId());
            builderDTO.setConditionName(condition == null ? "" : condition.getName());
        }
        return builderDTO;
    }

    public void deleteBuilder(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(id, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND,Resources.BUILDER,ErrorKey.ID);
        }
        if (conditionBuilderMapService.exitsByBuilderId(id)) {
            throw new ResourceConflictException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BUILDER, ErrorKey.Builder.ID);
        }
        notifyRepository.deleteAllByDomainIdAndBuilderIdIn(domainId, Arrays.asList(id));
        builderItemRepository.deleteAllByDomainIdAndBuilderIdIn(domainId, Arrays.asList(id));
        builderRepository.deleteAllByDomainIdAndIdIn(domainId, Arrays.asList(id));
    }

    public void deleteBuilder(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        conditionBuilderMapRepository.deleteByDomainIdAndBuilderIdIn(domainId,ids);
        notifyRepository.deleteAllByDomainIdAndBuilderIdIn(domainId, ids);
        builderItemRepository.deleteAllByDomainIdAndBuilderIdIn(domainId, ids);
        builderRepository.deleteAllByDomainIdAndIdIn(domainId, ids);
    }

    public BuilderDTO createBuilder(BuilderDTO builderDTO) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderDTO.getBuilderTypeId(), domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.BuilderType.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.Builder.BUILDER_TYPE_ID);
        }
        Builder builder = builderMapper.toEntity(builderDTO);
        builder.setId(null);
        builder.setDomainId(domainId);
        checkDuplicateName(builder.getName(), builder.getId());
        checkCategory(builderDTO.getCategoryId());
        setPosIndex(domainId, builder, builder.getCategoryId());

        builder = builderRepository.save(builder);
        if (builderDTO.getConditionId() != null) {
            Condition condition = conditionRepository.findByIdAndDomainId(builderDTO.getConditionId(), domainId);
            if (condition == null) {
                throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.BUILDER,
                        ErrorKey.Builder.CONDITION_NAME);
            }
            ConditionBuilderMap map = new ConditionBuilderMap();
            map.setBuilderId(builder.getId());
            map.setConditionId(builderDTO.getConditionId());
            map.setDomainId(domainId);
            conditionBuilderMapRepository.save(map);
        }
        BuilderDTO builderDtoRes = builderMapper.toDto(builder);
        builderDtoRes.setBuilderTypeName(builderType.getName());

        return builderDtoRes;
    }

    public BuilderDTO updateBuilder(BuilderDTO builderDTO) {
        Integer domainId = OCSUtils.getDomain();
        Builder builderCurrent = builderRepository.findByIdAndDomainId(builderDTO.getId(), domainId);
        if (builderCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.Builder.ID);
        }
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderDTO.getBuilderTypeId(), domainId);
        Builder builder = builderMapper.toEntity(builderDTO);
        if (!Objects.equal(builderDTO.getCategoryId(), builderCurrent.getCategoryId())) {
            setPosIndex(domainId, builder, builderDTO.getCategoryId());
        }
        if (!builderDTO.getName().equals(builderCurrent.getName())) {
            checkDuplicateName(builder.getName(), builder.getId());
        }
        checkCategory(builderDTO.getCategoryId());
        builder.setDomainId(domainId);

        builderRepository.save(builder);

        BuilderDTO dto = builderMapper.toDto(builder);
        dto.setBuilderTypeName(builderType.getName());

        return dto;
    }

    public Page<BuilderDTO> getBuilders(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<BuilderDTO> dtos = builderRepository.getBuilders(categoryId, name, description, domainId, pageable).map(builderMapper::toDto);
        return dtos;
    }

    public List<OCSCloneableEntity> cloneableEntities(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        List<CloneDTO> cloneDTOs = new ArrayList<>();
        for (Long id : ids) {
            Builder builder = builderRepository.findByIdAndDomainId(id, domainId);
            if (builder == null) {
                throwResourceNotFoundException();
            }
            BuilderDTO builderDTO = getBuilderDetail(id);
            CloneDTO builderCloneDTO = new CloneDTO();
            builderCloneDTO.setId(builderDTO.getId());
            builderCloneDTO.setName(builderDTO.getName());
            builderCloneDTO.setDescription(builderDTO.getDescription());
            cloneDTOs.add(builderCloneDTO);
        }
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        return cloneEntities(cloneDTOs, oldNewChildMap);
    }

    public Page<BuilderDTO> getListBuilderByConditionId(Long ConditionId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(ConditionId, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CONDITION, ErrorKey.ID);
        }
        Page<BuilderDTO> page = builderRepository.getListBuilderByConditionId(ConditionId, pageable);
        return page;
    }
    public List<BuilderDTO> getListBuilderCloneByConditionId(Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(conditionId, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.CONDITION, ErrorKey.ID);
        }
        List<Builder> lstBuilder = builderRepository.findBuilderByCondition(conditionId,domainId);
        List<Long> lstBuilderTypeId = lstBuilder.stream().map(Builder::getBuilderTypeId).collect(Collectors.toList());
        List<BuilderType> builderTypes = builderTypeRepository.findByDomainIdAndIdIn(domainId,lstBuilderTypeId);
        Map<Long, String> builderTypeMap = builderTypes.stream().collect(Collectors.toMap(BuilderType::getId,BuilderType::getName));
        List<CloneDTO> lstToClone = new ArrayList<>();
        for (Builder builder : lstBuilder) {
            CloneDTO toCloneDTO = new CloneDTO();
            toCloneDTO.setId(builder.getId());
            toCloneDTO.setName(builder.getName());
            toCloneDTO.setDescription(builder.getDescription());
            lstToClone.add(toCloneDTO);
        }
        List<BuilderDTO> lstBuilderDTOs = new ArrayList<>();
        Map<Long,CloneDTO> map = new HashMap<>();
        List<OCSCloneableEntity> lstCloned = cloneEntities(lstToClone,map);
        for (OCSCloneableEntity ocsCloneableEntity : lstCloned) {
            if(ocsCloneableEntity instanceof  Builder){
                Builder builder = (Builder) ocsCloneableEntity;
                BuilderDTO dto = builderMapper.toDto(builder);
                dto.setBuilderTypeName(builderTypeMap.get(dto.getBuilderTypeId()));
                lstBuilderDTOs.add(dto);
            }
        }

        return lstBuilderDTOs;
    }

    @Override
    public void getChildTemplate(List<Long> ids, Map<Long, TreeClone> resultMap) {
        for (Long entityId : resultMap.keySet()) {
            TreeClone result = resultMap.get(entityId);
            result.setTemplates(new ArrayList<>());
            result.setHasChild(false);
        }
    }

    public Page<BuilderDTO> getBuilderByEventPolicyId(Long eventPolicyId, Pageable pageable){
        Integer domainId = OCSUtils.getDomain();
        Page<BuilderDTO> builderDTOs = builderRepository.findAllByEventPolicyIdAndDomainId(eventPolicyId,domainId,pageable);
        return builderDTOs;
    }

    public void deleteBuilderByIds(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        conditionBuilderMapRepository.deleteByDomainIdAndBuilderIdIn(domainId, ids);
        builderItemRepository.deleteAllByDomainIdAndBuilderIdIn(domainId, ids);
        notifyRepository.deleteAllByDomainIdAndBuilderIdIn(domainId, ids);
        builderRepository.deleteAllByDomainIdAndIdIn(domainId, ids);
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Builder.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }
}
