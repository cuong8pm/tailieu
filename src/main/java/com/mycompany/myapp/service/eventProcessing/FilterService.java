package com.mycompany.myapp.service.eventProcessing;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.EventObject;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.mapper.ReferTableMapper;
import com.mycompany.myapp.mapper.eventProcessing.*;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Transactional
@Service
public class FilterService extends AbstractCloneService implements ChildCloneable {

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FilterMapper filterMapper;

    @Autowired
    private EventPolicyRepository eventPolicyRepository;

    @Autowired
    private RouterRepository routerRepository;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private EventObjectRepository eventObjectRepository;

    @Autowired
    private EventObjectFieldRepository eventObjectFieldRepository;

    @Autowired
    private EventObjectFieldMapper eventObjectFieldMapper;

    @Autowired
    private FunctionParamInstanceRepository functionParamInstanceRepository;

    @Autowired
    private ConditionService conditionService;

    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private ExpressionFilterMapService expressionFilterMapService;

    @Autowired
    private ConditionBuilderMapService conditionBuilderMapService;

    @Autowired
    private ConditionMapper conditionMapper;

    @Autowired
    private FunctionParamRepository functionParamRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private FieldMapper fieldMapper;

    @Autowired
    private FilterFieldRepository filterFieldRepository;

    @Autowired
    private FilterFieldMapper filterFieldMapper;

    @Autowired
    private FunctionParamInstanceMapper functionParamInstanceMapper;

    @Autowired
    private FunctionRepository functionRepository;

    @Autowired
    private FunctionMapper functionMapper;

    @Autowired
    private ReferTableMapper referTableMapper;

    @Autowired
    private FilterIgnoreProfileRepository filterIgnoreProfileRepository;

    @Autowired
    private ExpressionFilterMapRepository expressionFilterMapRepository;

    @Autowired
    private ExpressionRepository expressionRepository;

    @Autowired
    private EventPolicyService eventPolicyService;

    @Autowired
    private FunctionParamService functionParamService;

    @Autowired
    private FunctionService functionService;

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.CONDITION);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new FilterDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.FILTER;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Filter.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.FILTER;
    }

    @Override
    protected String getNameCategory() {
        return "Filters";
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = filterRepository;
        this.ocsCloneableRepository = filterRepository;
        this.parentService = categoryService;
        this.childServices = Arrays.asList(conditionService);
        this.cloneMappingServices = Arrays.asList(expressionFilterMapService);
        this.childDependServices = Arrays.asList(conditionService);
        this.parentDependServices = Arrays.asList(eventPolicyService);
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return filterRepository;
    }

    public FilterDTO createFilter(FilterDTO filterDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkCategory(filterDTO.getCategoryId());
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(filterDTO.getEventPolicyId(), domainId);
        if (eventPolicy == null) {
            throw new ResourceNotFoundException(ErrorMessage.EventPolicy.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.EventPolicy.ID);
        }
        Router router = routerRepository.findByIdAndDomainId(filterDTO.getRouterId(), domainId);
        if (router == null) {
            throw new ResourceNotFoundException(ErrorMessage.Router.NOT_FOUND, Resources.ROUTER, ErrorKey.Router.ROUTER_ID);
        }
        Filter filter = filterMapper.toEntity(filterDTO);
        filter.setId(null);
        checkDuplicateName(filter.getName(), filter.getId());
        setPosIndex(domainId, filter, filter.getCategoryId());
        filter.setDomainId(domainId);
        filter.setModifyDate(new Timestamp(new Date().getTime()));
        filter = filterRepository.save(filter);
        filterDTO = filterMapper.toDto(filter);

        return filterDTO;
    }

    public FilterDTO updateFilter(FilterDTO filterDTO) {
        Integer domainId = OCSUtils.getDomain();
        Filter filterOld = filterRepository.findByIdAndDomainId(filterDTO.getId(), domainId);
        if (filterOld == null) {
            throw new ResourceNotFoundException(ErrorMessage.Filter.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(filterDTO.getEventPolicyId(), domainId);
        if (eventPolicy == null) {
            throw new ResourceNotFoundException(ErrorMessage.EventPolicy.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.EventPolicy.ID);
        }
        Router router = routerRepository.findByIdAndDomainId(filterDTO.getRouterId(), domainId);
        if (router == null) {
            throw new ResourceNotFoundException(ErrorMessage.Router.NOT_FOUND, Resources.ROUTER, ErrorKey.Router.ROUTER_ID);
        }
        checkCategory(filterDTO.getCategoryId());
        checkDuplicateName(filterDTO.getName(), filterDTO.getId());
        Filter filterNew = filterMapper.toEntity(filterDTO);
        if (!Objects.equal(filterNew.getCategoryId(), filterOld.getCategoryId())) {
            setPosIndex(domainId, filterNew, filterDTO.getCategoryId());
        }
        filterNew.setDomainId(domainId);
        filterNew.setModifyDate(new Timestamp(new Date().getTime()));
        filterNew = filterRepository.save(filterNew);
        filterDTO = filterMapper.toDto(filterNew);

        return filterDTO;
    }

    public void deleteFilter(Long filterId) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        filterRepository.deleteByIdAndDomainId(filterId, domainId);
    }

    public void deleteFilter(List<Long> filterIds) {
        Integer domainId = OCSUtils.getDomain();
        List<ExpressionFilterMap> expressionFilterMaps = expressionFilterMapRepository.findByDomainIdAndParentIdIn(domainId, filterIds);
        List<Long> expressionIds = expressionFilterMaps.stream().map(ExpressionFilterMap::getExpressionId).collect(Collectors.toList());
        List<Condition> conditions = conditionRepository.findAllByFilterIdInAndDomainId(filterIds, domainId);
        List<Long> conditionIds = conditions.stream().map(Condition::getId).collect(Collectors.toList());
        filterIgnoreProfileRepository.deleteAllByFilterIdInAndDomainId(filterIds, domainId);
        functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdInAndOwnerType(domainId, filterIds, Function.FunctionType.FILTER_TYPE);
        filterFieldRepository.deleteAllByDomainIdAndFilterIdIn(domainId, filterIds);
        expressionFilterMapRepository.deleteAllByDomainIdAndParentIdIn(domainId, filterIds);
        expressionRepository.deleteAllByDomainIdAndIdIn(domainId, expressionIds);
        conditionService.deleteCondition(conditionIds);
        filterRepository.deleteAllByDomainIdAndIdIn(domainId, filterIds);
    }

    public FilterDTO getDetailFilter(Long filterId) {
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.Filter.NOT_FOUND, Resources.FILTER, ErrorKey.ENTITY_NOT_FOUND);
        }
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(filter.getEventPolicyId(), domainId);
        if (eventPolicy == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        Router router = routerRepository.findByIdAndDomainId(filter.getRouterId(), domainId);
        if (router == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.ROUTER, ErrorKey.ID);
        }
        FilterDTO filterDTO = filterMapper.toDto(filter);
        Category category = categoryRepository.findByIdAndDomainId(filter.getCategoryId(), domainId);
        if (category != null) {
            filterDTO.setCategoryId(category.getId());
            filterDTO.setCategoryName(category.getName());
            filterDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        filterDTO.setHasChild(conditionRepository.countAllByFilterIdAndDomainId(filterId, domainId).intValue() > 0);
        filterDTO.setEventPolicyName(eventPolicy.getName());
        filterDTO.setRouterName(router.getName());
        return filterDTO;
    }

    public List<FilterDTO> getListFiltersByCategoryId(Long categoryId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<Filter> entities = (Page<Filter>) (Page<?>) getListObjectByCategoryId(categoryId, pageable);
        List<FilterDTO> dtos = entities.stream().map(filterMapper::toDto).collect(Collectors.toList());
        List<Long> ids = dtos.stream().map(FilterDTO::getId).collect(Collectors.toList());
        List<CountChildrenByParentDTO> countChildren = filterRepository.countChildrenByParentId(ids, domainId);
        Map<Long, Integer> childrenMap = countChildren.stream().collect(Collectors.toMap(CountChildrenByParentDTO::getId, CountChildrenByParentDTO::getCountChild));
        for (FilterDTO dto : dtos) {
            Integer count = childrenMap.get(dto.getId());
            if (count != null && count > 0) {
                dto.setHasChild(true);
            }
        }
        dtos.sort(Comparator.comparing(FilterDTO::getPosIndex));
        return dtos;
    }

    public List<ConditionDTO> getListConditionByFilterId(Long filterId) {
        Integer domainId = OCSUtils.getDomain();
        List<ConditionDTO> conditionDTOs = new ArrayList<>();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }

        List<Condition> conditions = conditionRepository.findAllByFilterIdAndDomainId(filterId, domainId);
        List<Long> conditionIds = conditions.stream().map(Condition::getId).collect(Collectors.toList());

        Map<Long, Integer> conditionCountChildMap = conditionBuilderMapService.countChildrenByParentMap(conditionIds, domainId);
        for (Condition condition : conditions) {
            ConditionDTO conditionDTO = conditionMapper.toDto(condition);
            conditionDTO.setHasChild(checkHasChildOfChildren(conditionCountChildMap, condition.getId()));
            conditionDTO.setParentId(filterId);
            conditionDTOs.add(conditionDTO);
        }
        conditionDTOs.sort(Comparator.comparing(ConditionDTO::getId));
        return conditionDTOs;
    }

    public Page<FilterDTO> getFiltersWithPaging(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CATEGORY, ErrorKey.ID);
        }
        Page<FilterDTO> page = filterRepository.getListFilterPaging(domainId, name, description, categoryId, pageable).map(filterMapper::toDto);
        return page;
    }

    public List<ReferTable> getDropdownModule() {
        return referTableRepository.getReferTablesByReferType(ReferType.ModuleName, sortByValue());
    }

    public List<ReferTable> getDropdownInputType() {
        return referTableRepository.getReferTablesByReferType(ReferType.ExpressionInputType, sortByValue());
    }

    public List<ReferTable> getDropdownExpressionValueType() {
        return referTableRepository.getReferTablesByReferType(ReferType.ExpressionValueType, sortByValue());
    }

    public List<ReferTable> getDropdownParameterValue() {
        return referTableRepository.getReferTablesByReferType(ReferType.ParameterValueType, sortByValue());
    }

    public List<ReferTable> getDropdownOperator(Integer type) {
        if (Objects.equal(0, type)) {
            return referTableRepository.getReferTablesByReferType(ReferType.OperatorNumber, sortByValue());
        }
        return referTableRepository.getReferTablesByReferType(ReferType.OperatorString, sortByValue());
    }

    public List<DropdownDTO> getDropdownObjectSource(Integer type) {
        return eventObjectRepository.getEventObjectsByType(type);
    }

    public List<EventObjectFieldDTO> getListEventFields(Long evenObjectId, String name) {
        if (name == null) {
            name = "";
        }
        Integer domainId = OCSUtils.getDomain();
        EventObject object = eventObjectRepository.findByIdAndDomainId(evenObjectId, domainId);
        if (object == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, "", ErrorKey.ID);
        }
        List<EventObjectFieldDTO> parentDTOs = eventObjectFieldRepository
            .findParentEventFieldsByEventObjectId(evenObjectId)
            .stream()
            .map(eventObjectFieldMapper::toDto)
            .collect(Collectors.toList());
        List<EventObjectFieldDTO> tree = new ArrayList<>();
        for (EventObjectFieldDTO parentDTO : parentDTOs) {
            boolean result = parentDTO.getName().toLowerCase().contains(name.toLowerCase());
            List<EventObjectFieldDTO> listChilds = getListChildEventFields(parentDTO.getId(), name);
            if (result || !listChilds.isEmpty()) {
                parentDTO.setTemplates(listChilds);
                parentDTO.setHasChild(!listChilds.isEmpty());
                tree.add(parentDTO);
            }
        }
        return tree;
    }

    private List<EventObjectFieldDTO> getListChildEventFields(Long eventFieldId, String name) {
        List<EventObjectFieldDTO> childDTOs = eventObjectFieldRepository
            .findChildrenEventFieldsByParentId(eventFieldId)
            .stream()
            .map(eventObjectFieldMapper::toDto)
            .collect(Collectors.toList());
        List<EventObjectFieldDTO> tree = new ArrayList<>();
        for (EventObjectFieldDTO childDTO : childDTOs) {
            boolean result = childDTO.getName().toLowerCase().contains(name.toLowerCase());
            List<EventObjectFieldDTO> listChilds = getListChildEventFields(childDTO.getId(), name);
            if (result || !listChilds.isEmpty()) {
                childDTO.setTemplates(listChilds);
                childDTO.setHasChild(!listChilds.isEmpty());
                tree.add(childDTO);
            }
        }
        return tree;
    }

    @Transactional(readOnly = true)
    public List<Map<String, Object>> getDropdownParamInstanceValue(Long paramId) {
        Integer domainId = OCSUtils.getDomain();
        List<Map<String, Object>> list = new ArrayList<>();
        FunctionParam param = functionParamRepository.findByIdAndDomainId(paramId, domainId);
        if (param == null) {
            throw new ResourceNotFoundException(ErrorMessage.FunctionParam.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
        }
        List<FunctionParamInstance> instances = functionParamInstanceRepository.findAllByDomainIdAndFunctionParameterId(domainId, paramId);
        List<ReferTable> referTables = referTableRepository.getReferTablesByReferType(ReferType.ParameterValueType, sortByValue());
        Map<Integer, String> referTypeMap = referTables.stream().collect(Collectors.toMap(ReferTable::getValue, ReferTable::getName));
        for (FunctionParamInstance instance : instances) {
            if (Objects.equal(param.getDataType(), 1)) {
                String value = instance.getValue();
                if (!StringUtils.isEmpty(value) && value.charAt(0) == '\"') {
                    value = value.substring(1);
                }
                if (!StringUtils.isEmpty(value) && value.charAt(value.length() - 1) == '\"') {
                    value = value.substring(0, value.length() - 1);
                }
                instance.setValue(value);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("id", instance.getId());
            map.put("name", referTypeMap.get(instance.getValueType()) + " : " + instance.getValue());
            map.put("instanceValue", instance.getValue());
            map.put("ownerId", instance.getOwnerId());
            list.add(map);
        }

        return list;
    }

    public Map<String, Object> getListProfile(Long filterId) {
        Map<String, Object> profiles = new HashMap<>();
        Integer domainId = OCSUtils.getDomain();
        Filter filter = filterRepository.findByIdAndDomainId(filterId, domainId);
        if (filter == null && filterId != 0) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        List<ReferType> referTypes = Arrays.asList(ReferType.ProfileBalance, ReferType.ProfileOffer, ReferType.ProfileCustomer, ReferType.ProfileOther);
        List<ReferTable> referTables = referTableRepository.getReferTablesByReferTypeIn(referTypes, sortByValue());
        List<Long> ignoreIds = filterRepository.getListIgnoreIdReferTable(filterId);
        if (ignoreIds != null && ignoreIds.size() > 0) {
            for (int i = 0; i < referTables.size(); i++) {
                for (Long ignoreId : ignoreIds) {
                    if (Objects.equal(ignoreId, referTables.get(i).getId())) {
                        referTables.remove(referTables.get(i));
                        i--;
                        break;
                    }
                }

            }
        }
        List<ReferTable> profileBalances = referTables.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileBalance)).collect(Collectors.toList());
        profiles.put("profile-balances", profileBalances);
        List<ReferTable> profileOffers = referTables.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileOffer)).collect(Collectors.toList());
        profiles.put("profile-offers", profileOffers);
        List<ReferTable> profileCustomers = referTables.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileCustomer)).collect(Collectors.toList());
        profiles.put("profile-customers", profileCustomers);
        List<ReferTable> profileOther = referTables.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileOther)).collect(Collectors.toList());
        profiles.put("profile-others", profileOther);

        return profiles;
    }

    public Map<String, Object> getListPopupProfile(Long id, String name) {
        Map<String, Object> profiles = new HashMap<>();
        if (name == null) {
            name = "";
        }
        List<ReferType> referTypes = Arrays.asList(ReferType.ProfileBalance, ReferType.ProfileOffer, ReferType.ProfileCustomer, ReferType.ProfileOther);
        List<ReferTable> referTables = referTableRepository.findByReferTypeInAndNameContaining(referTypes,
            name, sortByValue());

        List<ReferTableDTO> dtos = referTableMapper.toDto(referTables);
        List<Long> ignoreIds = filterRepository.getListIgnoreIdReferTable(id);
        for (ReferTableDTO dto : dtos) {
            dto.setIsUsed(true);
            for (Long item : ignoreIds) {
                if (Objects.equal(item, dto.getId())) {
                    dto.setIsUsed(false);
                    break;
                }
            }
        }
        List<ReferTableDTO> profileBalances = dtos.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileBalance)).collect(Collectors.toList());
        profiles.put("profile-balances", profileBalances);
        List<ReferTableDTO> profileOffers = dtos.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileOffer)).collect(Collectors.toList());
        profiles.put("profile-offers", profileOffers);
        List<ReferTableDTO> profileCustomers = dtos.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileCustomer)).collect(Collectors.toList());
        profiles.put("profile-customers", profileCustomers);
        List<ReferTableDTO> profileOther = dtos.stream().filter(referTable -> Objects.equal(referTable.getReferType(), ReferType.ProfileOther)).collect(Collectors.toList());
        profiles.put("profile-others", profileOther);

        return profiles;
    }

    public List<FieldDTO> getListFields(Long id, String name) {
        Integer domainId = OCSUtils.getDomain();
        List<FieldDTO> fieldDTOs = fieldMapper.toDto(fieldRepository.findByFilterIdAndDomainId(id, name, domainId));
        return fieldDTOs;
    }

    public void addField(FilterFieldDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        FilterField entity = filterFieldMapper.toEntity(dto);
        if (entity.getValueType() == 0 && entity.getDataType() == 1) {
            entity.setValue("\"" + entity.getValue() + "\"");
        }
        entity.setDomainId(domainId);
        if (dto.getValueType() == 2) {
            EventObjectField eventObjectField = eventObjectFieldRepository.findByIdAndDomainId(dto.getLinkedId(), domainId);
            dto.setDataType(eventObjectField.getDataType());
        }
        if (dto.getValueType() == 3 && dto.getLinkedId() != null) {
            //check function null and function type
            Function function = functionRepository.findByIdAndDomainId(dto.getLinkedId(), domainId);
            expressionService.checkFunction(function, domainId, 0);
            String functionName = dto.getValue().substring(0, dto.getValue().indexOf('('));
            if (functionName != null) {
                dto.setFunctionName(functionName);
                if (!dto.getFunctionName().equalsIgnoreCase(function.getFunctionName())) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
                }
            }
            List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainIdOrderByIndexOrder(function.getId(), domainId);
            if (functionParams.size() != dto.getParamOrder().size()) {
                throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
            }
            for (int i = 0; i < functionParams.size(); i++) {
                if (!Objects.equal(functionParams.get(i).getId(), dto.getParamOrder().get(i))) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                }
            }
        }
        filterFieldRepository.save(entity);
        List<FunctionParamDTO> paramDTOs = dto.getFunctionParams();
        if (dto.getValueType() == 3) {
            functionParamService.setFunctionParamInstanceValue(paramDTOs, entity.getLinkedId(), domainId, entity.getId(), Function.FunctionType.FIELD_TYPE);
        }

    }

    public Page<FilterFieldDTO> getListFilterFields(@NotNull @Positive Long filterId, String name, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Sort.Order order = pageable.getSort().getOrderFor(Filter.FieldNames.POS_INDEX);
        if (order != null) {
            pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.ASC, Filter.FieldNames.FIELD_ID));
        }
        Page<FilterFieldDTO> page = filterFieldRepository.getListFilterFields(filterId, name, pageable, domainId);
        for (FilterFieldDTO item : page) {
            if (item.getValueType() == 0 && item.getDataType() == 1) {
                item.setValue(item.getValue().substring(1, item.getValue().length() - 1));
            }
            if (item.getValueType() == 3 && item.getLinkedId() != null) {
                List<FunctionParamDTO> functionParamDTOs = functionParamRepository
                    .getListFunctionParam(item.getId(), domainId, ReferType.DataType, Function.FunctionType.FIELD_TYPE, item.getLinkedId());
                List<Long> paramIds = functionParamDTOs.stream().map(FunctionParamDTO::getId).collect(Collectors.toList());
                item.setFunctionParams(functionParamDTOs);
                item.setParamOrder(paramIds);
            }
        }
        return page;
    }

    public void deleteFilterField(Long id) {
        Integer domainId = OCSUtils.getDomain();
        FilterField field = filterFieldRepository.findByIdAndDomainId(id, domainId);
        if(field == null){
            throw new ResourceNotFoundException(ErrorMessage.Filter.FILTER_FIELD_NOT_FOUND, Resources.FILTER, ErrorKey.ENTITY_NOT_FOUND);
        }
        filterFieldRepository.deleteByIdAndDomainId(id, domainId);
        functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, id, Function.FunctionType.FIELD_TYPE);
    }

    public void updateFilterField(Long id, FilterFieldDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        FilterField filterFieldDB = filterFieldRepository.findByIdAndDomainId(id, domainId);
        if (filterFieldDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.Filter.FILTER_FIELD_NOT_FOUND, Resources.FILTER, ErrorKey.ENTITY_NOT_FOUND);
        }
        if (dto.getValueType() == 3 && dto.getLinkedId() != null) {
            //check function null and function type
            Function function = functionRepository.findByIdAndDomainId(dto.getLinkedId(), domainId);
            expressionService.checkFunction(function, domainId, 0);
            String functionName = dto.getValue().substring(0, dto.getValue().indexOf('('));
            if (functionName != null) {
                dto.setFunctionName(functionName);
                if (!dto.getFunctionName().equalsIgnoreCase(function.getFunctionName())) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
                }
            }
            if (dto.getParamOrder() != null && dto.getParamOrder().size() > 0) {
                List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainIdOrderByIndexOrder(function.getId(), domainId);
                if (functionParams.size() != dto.getParamOrder().size()) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
                }
                for (int i = 0; i < functionParams.size(); i++) {
                    if (!Objects.equal(functionParams.get(i).getId(), dto.getParamOrder().get(i))) {
                        throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                    }
                }
            }
        }
        if (filterFieldDB.getValueType() == 3 && !Objects.equal(filterFieldDB.getLinkedId(), dto.getLinkedId())) {
            functionParamInstanceRepository.deleteAllByDomainIdAndOwnerIdAndOwnerType(domainId, id, Function.FunctionType.FIELD_TYPE);
        }

        List<FunctionParamDTO> paramDTOs = dto.getFunctionParams();
        if (dto.getValueType() == 3) {
            functionParamService.setFunctionParamInstanceValue(paramDTOs, dto.getLinkedId(), domainId, dto.getId(), Function.FunctionType.FIELD_TYPE);
        }

        dto.setId(id);
        FilterField entity = filterFieldMapper.toEntity(dto);
        if (entity.getValueType() == 0 && entity.getDataType() == 1) {
            entity.setValue("\"" + entity.getValue() + "\"");
        }
        entity.setDomainId(domainId);
        filterFieldRepository.save(entity);
    }

    public List<FunctionParamDTO> getListFunctionParam(Long id, Long functionId, Integer ownerType) {
        Integer domainId = OCSUtils.getDomain();
        List<FunctionParamDTO> functionParameterDTOs = functionParamRepository.getListFunctionParamEnable(id, domainId, ReferType.DataType, ownerType, functionId);
        for (FunctionParamDTO parameterDTO : functionParameterDTOs) {
            parameterDTO.setValue(getValueInstance(parameterDTO));
        }
        return functionParameterDTOs;
    }

    public FunctionDTO getValueDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        FilterField filterField = filterFieldRepository.findByIdAndDomainId(id, domainId);
        if (filterField == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        FunctionDTO dto = functionMapper.toDto(functionRepository.findByIdAndDomainId(filterField.getLinkedId(), domainId));
        if (dto != null) {
            List<FunctionParamDTO> parameterDTOs = functionParamRepository.getListFunctionParam(id, domainId,
                ReferType.DataType, Function.FunctionType.FIELD_TYPE, dto.getId());
            for (FunctionParamDTO parameterDTO : parameterDTOs) {
                parameterDTO.setValue(getValueInstance(parameterDTO));
            }
            dto.setFunctionParamDTOS(parameterDTOs);
        }
        return dto;
    }

    public void updateProfiles(@NotNull @Positive Long id, @Valid List<ReferTableDTO> dtos) {
        Integer domainId = OCSUtils.getDomain();
        for (ReferTableDTO item : dtos) {
            if (item.getActionType() == ActionType.ADD) {
                filterIgnoreProfileRepository.deleteByFilterIdAndReferTableId(id, item.getId());
            } else if (item.getActionType() == ActionType.DELETE) {
                FilterIgnoreProfile filterIgnoreProfile = new FilterIgnoreProfile();
                filterIgnoreProfile.setFilterId(id);
                filterIgnoreProfile.setReferTableId(item.getId());
                filterIgnoreProfile.setDomainId(domainId);
                filterIgnoreProfileRepository.save(filterIgnoreProfile);
            }
        }
    }

    @Override
    public void getChildTemplate(List<Long> ids, Map<Long, TreeClone> resultMap) {
        Integer domainId = OCSUtils.getDomain();
        List<Condition> conditions = conditionRepository.findAllByFilterIdInAndDomainId(ids, domainId);
        conditions.sort(Comparator.comparing(Condition::getPosIndex));

        List<Long> conditionIds = conditions.stream().map(Condition::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(conditionIds)) {
            return;
        }
        Map<Long, List<Long>> filterIdConditionIdMap = new HashMap<>();
        for (Long id : ids) {
            List<Long> childIds = conditions.stream().filter(condition -> Objects.equal(condition.getFilterId(), id)).map(Condition::getId).collect(Collectors.toList());
            filterIdConditionIdMap.put(id, childIds);
        }
        Map<Long, TreeClone> conditionMap = new HashMap<Long, TreeClone>();
        List<TreeClone> conditionDTOs = new ArrayList<>();
        for (Condition baseEntity : conditions) {
            TreeClone result = new ConditionDTO();
            result.setTemplates(new ArrayList<Tree>());
            BeanUtils.copyProperties(baseEntity, result);
            result.setName(baseEntity.getName());
            if (result.getTemplates() == null) {
                result.setTemplates(new ArrayList<Tree>());
            }
            conditionDTOs.add(result);
            conditionMap.put(result.getId(), result);
        }
        conditionService.getChildTemplate(conditionIds, conditionMap);
        for (Long entityId : resultMap.keySet()) {
            TreeClone resultFilter = resultMap.get(entityId);
            List<? extends Tree> children = conditionDTOs;
            if (children != null) {
                List<Long> childIds = filterIdConditionIdMap.get(entityId);
                List<? extends Tree> listConditionToAdd = children.stream().filter(tree -> childIds.contains(tree.getId())).collect(Collectors.toList());
                resultFilter.getTemplates().addAll(listConditionToAdd);
            }
            resultFilter.setHasChild(!resultFilter.getTemplates().isEmpty());
        }

    }

    @Override
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        List<CloneDTO> lstCloneDTos = new ArrayList<>();
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        Map<Long, List<Long>> parentChildIdMap = new HashMap<>();
        List<Long> newIds = new ArrayList<>();
        Map<Long, Long> oldNewFilterIdMap = new HashMap<>();

        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);

        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if (!java.util.Objects.equals(distinctIds.size(), baseToClones.size())) {
            throwResourceNotFoundException();
        }

        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
            .collect(Collectors.toMap(OCSCloneableEntity::getId, java.util.function.Function.identity()));

        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveEntity(cloneDTO, baseToCloneMap);
            if (!ObjectUtils.isEmpty(clonedEntity)) {
                oldNewIdMap.put(clonedEntity.getId(), cloneDTO);
                clonedEntities.add(clonedEntity);
                newIds.add(clonedEntity.getId());
            }
            cloneDTO.getChildren().forEach(cloneDTO1 -> cloneDTO1.setParentId(clonedEntity.getId()));
            lstCloneDTos.addAll(cloneDTO.getChildren());
            List<Long> childrenIds = new ArrayList<>();
            if (cloneDTO.getChildren() != null) {
                childrenIds = cloneDTO.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }
            oldNewFilterIdMap.put(cloneDTO.getId(), clonedEntity.getId());
            parentChildIdMap.put(clonedEntity.getId(), childrenIds);
        }

        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            cloneMappingService.cloneMappings(newIds, lstCloneDTos, oldNewIdMap, parentChildIdMap);
        }
        // clone list conditions
        conditionService.cloneEntities(lstCloneDTos, oldNewChildMap);

        //clone list filter field
        List<FilterField> toCloneFilterFields = filterFieldRepository.findAllByDomainIdAndFilterIdIn(domainId, ids);
        List<Long> fieldIds = toCloneFilterFields.stream().map(FilterField::getId).collect(Collectors.toList());
        List<FunctionParamInstance> toCloneInstances = functionParamInstanceRepository.findAllByDomainIdAndOwnerTypeAndOwnerIdIn(domainId, Function.FunctionType.FIELD_TYPE, fieldIds);
        List<FilterField> clonedFilterFields = new ArrayList<>();
        List<FunctionParamInstance> clonedInstances = new ArrayList<>();
        for (FilterField filterField : toCloneFilterFields) {
            FilterField clonedField = SerializationUtils.clone(filterField);
            clonedField.setId(null);
            clonedField.setFilterId(oldNewFilterIdMap.get(filterField.getFilterId()));
            clonedFilterFields.add(clonedField);
        }
        clonedFilterFields = filterFieldRepository.saveAll(clonedFilterFields);

        // clone list function param instances
        Map<Long, Long> clonedFilterFieldMap = new HashMap<>();
        clonedFilterFields.forEach(clonedItem -> {
            Long toCloneItemId = toCloneFilterFields.stream().filter(toCloneItem ->
                Objects.equal(toCloneItem.getFieldId(), clonedItem.getFieldId())
            ).map(FilterField::getId).findFirst().get();
            if (toCloneItemId != null) {
                clonedFilterFieldMap.put(toCloneItemId, clonedItem.getId());
            }
        });

        toCloneInstances.forEach(item -> {
            FunctionParamInstance clonedItem = SerializationUtils.clone(item);
            clonedItem.setId(null);
            clonedItem.setOwnerId(clonedFilterFieldMap.get(item.getOwnerId()));
            clonedInstances.add(clonedItem);
        });
        functionParamInstanceRepository.saveAll(clonedInstances);

        //clone list filter ignore profile
        List<FilterIgnoreProfile> profilesToClone = filterIgnoreProfileRepository.findAllByFilterIdInAndDomainId(ids, domainId);
        List<FilterIgnoreProfile> clonedProfiles = new ArrayList<>();
        for (FilterIgnoreProfile filterIgnoreProfile : profilesToClone) {
            FilterIgnoreProfile clonedIgnoreProfile = SerializationUtils.clone(filterIgnoreProfile);
            clonedIgnoreProfile.setId(null);
            clonedIgnoreProfile.setFilterId(oldNewFilterIdMap.get(filterIgnoreProfile.getFilterId()));
            clonedProfiles.add(clonedIgnoreProfile);
        }
        filterIgnoreProfileRepository.saveAll(clonedProfiles);
        return clonedEntities;
    }

    @Override
    protected OCSCloneableEntity setPropertiesOfCloneEntity(OCSCloneableEntity clonedEntity, CloneDTO cloneDTO, String newName) {
        if (clonedEntity instanceof Filter) {
            ((Filter) clonedEntity).setModifyDate(new Timestamp((new Date()).getTime()));
            if (cloneDTO.getParentId() != null) {
                ((Filter) clonedEntity).setEventPolicyId(cloneDTO.getParentId());
            }
        }
        return super.setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);
    }

    public List<FilterDTO> searchFilterByExpression(List<ExpressionDTO> dtos) {
        Integer domainId = OCSUtils.getDomain();
        List<List<Long>> list = new ArrayList<>();
        for (ExpressionDTO item : dtos) {
            if(item.getValue() != null){
                item.setValue(item.getValue().replaceAll("%", "\\\\%").replaceAll("_", "\\\\_"));
            }
            List<Long> ids = filterRepository.findIdByExpression(item.getInput(), item.getDataType(),
                item.getInputType(), item.getLinkedId(), item.getValue(), domainId);
            list.add(ids);
        }
        List<Long> resultIds = new ArrayList<>();
        List<FilterDTO> result = new ArrayList<>();
        if (list.isEmpty()) {
            return result;
        }
        List<Long> firstIds = list.get(0);
        for (Long id : firstIds) {
            Integer count = 0;
            for (List<Long> ids : list) {
                for (Long item : ids) {
                    if (Long.compare(id, item) == 0) {
                        count++;
                        break;
                    }
                }
            }
            if (count == list.size()) {
                resultIds.add(id);
            }
        }
        if (resultIds.isEmpty()) {
            return result;
        }
        result = filterMapper.toDto(filterRepository.findByIdInAndDomainId(resultIds, domainId));
        return result;
    }

    public Page<FilterDTO> getFiltersByEventPolicyId(Long eventPolicyId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<FilterDTO> filterDTOs = filterRepository.findAllByDomainIdAndEventPolicyId(domainId, eventPolicyId, pageable).map(filterMapper::toDto);
        return filterDTOs;
    }

    public FilterDTO getDetailFilterClone(Long filterId) {
        Integer domainId = OCSUtils.getDomain();
        FilterDTO dto = getDetailFilter(filterId);
        dto.setId(0l);
        dto.setName(cloneName(dto.getName(), domainId));
        return dto;
    }

    public void validatorFilters(FilterDTO filterDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkCategory(filterDTO.getCategoryId());
        EventPolicy eventPolicy = eventPolicyRepository.findByIdAndDomainId(filterDTO.getEventPolicyId(), domainId);
        if (eventPolicy == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.ID);
        }
        Router router = routerRepository.findByIdAndDomainId(filterDTO.getRouterId(), domainId);
        if (router == null) {
            throw new ResourceNotFoundException(ErrorMessage.Router.NOT_FOUND, Resources.ROUTER, ErrorKey.Router.ROUTER_ID);
        }
        checkDuplicateName(filterDTO.getName(), filterDTO.getId());
        List<FilterFieldDTO> filterFieldDTOs = filterDTO.getOutputs().getListField();
        Set<Long> setFieldIds = new HashSet<>();
        List<Integer> errorRows = new ArrayList<>();
        //validate filter fields
        for (int i = 0; i < filterFieldDTOs.size(); i++) {
            FilterFieldDTO item = filterFieldDTOs.get(i);
            boolean isError = false;
            Field field = fieldRepository.findByIdAndDomainId(item.getFieldId(), domainId);
            if (field == null) {
                throw new DataConstrainException(ErrorMessage.NOT_FOUND, Resources.EVENT_POLICY, ErrorKey.Filter.FIELD_ID);
            }
            setFieldIds.add(item.getFieldId());
            if (Objects.equal(item.getValueType(), 3) && item.getLinkedId() != null) {
                //check function null and function type changed
                Function function = functionRepository.findByIdAndDomainId(item.getLinkedId(), domainId);
                if (function == null) {
                    if (!errorRows.contains(i + 1)) {
                        errorRows.add(i + 1);
                    }
                    isError = true;
                }
                if (!java.util.Objects.equals(function.getFunctionType(), 0) && !isError) {
                    if (!errorRows.contains(i + 1)) {
                        errorRows.add(i + 1);
                    }
                    isError = true;
                }
                // check function name changed
                String functionName = item.getValue().substring(0, item.getValue().indexOf('('));
                if (functionName != null && !isError) {
                    item.setFunctionName(functionName);
                    if (!item.getFunctionName().equalsIgnoreCase(function.getFunctionName())) {
                        if (!errorRows.contains(i + 1)) {
                            errorRows.add(i + 1);
                        }
                        isError = true;
                    }
                }
                // check number of function params and their order
                List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainIdOrderByIndexOrder(function.getId(), domainId);
                if (functionParams.size() != item.getParamOrder().size() && !isError) {
                    if (!errorRows.contains(i + 1)) {
                        errorRows.add(i + 1);
                    }
                    isError = true;
                }
                if (!isError) {
                    for (int j = 0; j < functionParams.size(); j++) {
                        if (!Objects.equal(functionParams.get(j).getId(), item.getParamOrder().get(j))) {
                            if (!errorRows.contains(i + 1)) {
                                errorRows.add(i + 1);
                            }
                            isError = true;
                        }
                    }
                }
                if (!isError) {
                    functionParamService.validateFunctionParams(errorRows, i, item.getFunctionParams(), item.getLinkedId(), domainId, item.getId(), Function.FunctionType.FIELD_TYPE);
                }
            }
        }
        if (errorRows.size() > 0) {

            String rows = "[";
            for (Integer errorRow : errorRows) {
                rows += errorRow.intValue() + ",";
            }
            rows = rows.substring(0, rows.length() - 1);
            rows += "]";
            throw new DataConstrainException(rows, Resources.EVENT_POLICY, ErrorKey.EventPolicy.FILTER_FIELD_HAS_BEEN_CHANGED);
        }
        if (setFieldIds.size() != filterFieldDTOs.size()) {
            throw new DataConstrainException(ErrorMessage.DUPLICATED_CELL_ID, Resources.EVENT_POLICY, ErrorKey.Filter.FIELD_ID);
        }
        // validate expressions
        errorRows = new ArrayList<>();
        List<ExpressionDTO> expressionDTOs = filterDTO.getExpressions();
        for (int i = 0; i < expressionDTOs.size(); i++) {
            ExpressionDTO expressionDTO = expressionDTOs.get(i);
            //check function null and function type
            if (expressionDTO.getInputType() == 1 && expressionDTO.getLinkedId() != null) {
                Function function = functionRepository.findByIdAndDomainId(expressionDTO.getLinkedId(), domainId);
                checkFunction(function, domainId, 0);
                if (!java.util.Objects.equals(expressionDTO.getDataType(), function.getDataType())) {
                    if (!errorRows.contains(i + 1)) {
                        errorRows.add(i + 1);
                    }
                }
            }
            functionParamService.validateFunctionParams(errorRows, i, expressionDTO.getFunctionParams(), expressionDTO.getLinkedId(), domainId, expressionDTO.getId(), Function.FunctionType.FILTER_TYPE);
        }
        if (errorRows.size() > 0) {
            errorRows.stream().distinct();
            StringBuilder rows = new StringBuilder("[");
            for (Integer errorRow : errorRows) {
                rows.append(errorRow.intValue() + ",");
            }
            rows.deleteCharAt(rows.length() - 1);
            rows.append("]");
            throw new DataConstrainException(rows.toString(), Resources.EVENT_POLICY, ErrorKey.EventPolicy.EXPRESSION_HAS_BEEN_CHANGED);
        }
    }

    public void checkFunction(Function function, Integer domainId, Integer functionType) {
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.Function.NOT_FOUND, Resources.FUNCTION, ErrorKey.Function.ALIAS_ID);
        }
        if (!java.util.Objects.equals(function.getFunctionType(), functionType)) {
            throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
        }
    }

    @Override
    protected List<Long> getListDependId(List<OCSMoveableEntity> dependDbs) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> ids = dependDbs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
        return filterRepository.findAllEventPolicyIdByFilterIdAnd(ids, domainId);
    }

    @Override
    protected List getMappingFoundEntities(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        return conditionRepository.findAllByFilterIdInAndDomainId(ids, domainId);
    }

    @Override
    protected Map<Long, Object> getMappingFoundEntities(List<Long> ids, Boolean isPopup) {
        Map<Long, Object> mappingOfFoundEntity = new HashMap<>();
        List list = getMappingFoundEntities(ids);
        if (isPopup == null || (list != null && isPopup != true)) {
            if (!list.isEmpty() && list.get(0) instanceof Condition) {
                mappingOfFoundEntity =
                    (Map<Long, Object>) list.stream().collect(Collectors.groupingBy(Condition::getFilterId, Collectors.toList()));
            }
        }
        return mappingOfFoundEntity;
    }

    public FunctionDTO getListAllFunctionParam(Long fieldId, Long functionId) {
        Integer domainId = OCSUtils.getDomain();
        com.mycompany.myapp.domain.eventProcessing.Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FILTER, ErrorKey.ID);
        }
        FunctionDTO dto = functionMapper.toDto(function);
        if (dto != null) {
            List<FunctionParamDTO> parameterDTOs = functionParamRepository.getListFunctionParam(fieldId, domainId,
                ReferType.DataType, Function.FunctionType.FIELD_TYPE, functionId);
            for (FunctionParamDTO parameterDTO : parameterDTOs) {
                parameterDTO.setValue(getValueInstance(parameterDTO));
            }
            dto.setFunctionParamDTOS(parameterDTOs);
        }
        return dto;
    }

    private String getValueInstance(FunctionParamDTO functionParamDTO) {
        String value = functionParamDTO.getValue();
        if (java.util.Objects.equals(functionParamDTO.getValueType(), 0)
            && java.util.Objects.equals(functionParamDTO.getDataType(), 1)) {
            return getStringValue(value);
        }
        return value;
    }

    private String getStringValue(String oldValue) {
        String value = oldValue;
        if (!StringUtils.isEmpty(value) && value.charAt(0) == '\"') {
            value = value.substring(1);
        }
        if (!StringUtils.isEmpty(value) && value.charAt(value.length() - 1) == '\"') {
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }


    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Filter.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new ResourceNotFoundException(ErrorMessage.Filter.NOT_FOUND, getResourceName(), ErrorKey.ENTITY_NOT_FOUND);
    }

}
