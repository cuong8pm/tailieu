package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Condition;
import com.mycompany.myapp.domain.eventProcessing.ConditionBuilderMap;
import com.mycompany.myapp.domain.eventProcessing.ConditionVerificationMap;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.eventProcessing.BuilderDTO;
import com.mycompany.myapp.dto.eventProcessing.ConditionBuilderMapDTO;
import com.mycompany.myapp.mapper.eventProcessing.ConditionBuilderMapMapper;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionBuilderMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionVerificationMapRepository;
import com.mycompany.myapp.repository.eventProcessing.VerificationRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Transactional
public class ConditionBuilderMapService extends AbstractCloneMappingService implements ConditionChildrenMapService{
    @Autowired
    private ConditionBuilderMapRepository conditionBuilderMapRepository;

    @Autowired
    private BuilderService builderService;

    @Autowired
    private ConditionService conditionService;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private ConditionBuilderMapMapper conditionBuilderMapMapper;

    @Autowired
    private ConditionVerificationMapService conditionVerificationMapService;

    @Autowired
    private ConditionVerificationMapRepository conditionVerificationMapRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private  ExpressionConditionMapService expressionConditionMapService;
    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = conditionBuilderMapRepository;
        this.childService = builderService;
        this.parentService = conditionService;
        this.parentDependService = conditionService;
    }

    @Override
    protected String getResource() {
        return Resources.BUILDER;
    }

    @Override
    protected CategoryType getCategoryType() {
        return builderService.getCategoryType();
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return conditionBuilderMapRepository;
    }

    public List<Long> getBuilderIdByConditionId(Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        return conditionBuilderMapRepository.findBuilderIdByConditionId(domainId, conditionId);
    }

    public List<ConditionBuilderMap> getBuilderByConditionId(Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        return conditionBuilderMapRepository.findBuilderByConditionId(domainId, conditionId);
    }

    public ConditionBuilderMapDTO createConditionBuilderMapByAdd(BuilderDTO builderDTO, Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(conditionId, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.CONDITION, ErrorKey.ENTITY_NOT_FOUND);
        }
        if (builderDTO.getId() == null || builderDTO.getId() == 0) {
            builderDTO = builderService.createBuilder(builderDTO);
        }
        ConditionBuilderMap conditionBuilderMap = new ConditionBuilderMap();
        conditionBuilderMap.setBuilderId(builderDTO.getId());
        conditionBuilderMap.setConditionId(conditionId);
        conditionBuilderMap.setDomainId(domainId);
        conditionBuilderMap = conditionBuilderMapRepository.save(conditionBuilderMap);
        ConditionBuilderMapDTO conditionBuilderMapDTO = conditionBuilderMapMapper.toDto(conditionBuilderMap);
        return conditionBuilderMapDTO;
    }

    public Boolean exitsByBuilderId(Long builderId) {
        Integer domainId = OCSUtils.getDomain();
        return conditionBuilderMapRepository.existsByBuilderIdAndDomainId(builderId, domainId);
    }

    @Override
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        if (ocsCloneMapRepository == null || childService == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }

        Integer domainId = OCSUtils.getDomain();
        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<CloneDTO> idsToCreateNew = cloneDTOs.stream().filter(cloneDTO -> cloneDTO.getId() != null).collect(Collectors.toList());

        childService.cloneEntities(idsToCreateNew, oldNewChildMap);

        long batchSize = 30;
        long count = 0;

        Map<Long, Long> newOldParentIdMap = new HashMap<>();
        for (Long newId : oldNewIdParentMap.keySet()) {
            newOldParentIdMap.put(newId, oldNewIdParentMap.get(newId).getId());
        }

        List<OCSCloneableMap> newMap = new ArrayList<>();
        for (Long newParentId : newParentIds) {
            Long oldParentId = newOldParentIdMap.get(newParentId);
            List<OCSCloneableMap> oldMappingParents = mappingParents.stream().filter(i -> Objects.equals(i.getParentId(), oldParentId)).collect(Collectors.toList());
            for (OCSCloneableMap oldMappingParent : oldMappingParents) {
                List<Long> childrenIds = parentNewChildrenOldIdMap.get(newParentId);
                Long childId = getChildId(oldMappingParent,childrenIds,oldNewChildMap, newParentId);
                if(childId != null){
                    OCSCloneableMap newCloneMap = SerializationUtils.clone(oldMappingParent);
                    newCloneMap.setChildMappingId(childId);
                    newCloneMap.clearId();
                    newCloneMap.setParentMappingId(newParentId);
                    newMap.add(newCloneMap);

                    entityManager.persist(newCloneMap);
                    if (count % batchSize == 0 && count > 0) {
                        entityManager.flush();
                        entityManager.clear();
                        newMap.clear();
                    }
                    count++;
                }
            }

        }

        if (!CollectionUtils.isEmpty(newMap)) {
            entityManager.flush();
            entityManager.clear();
            newMap.clear();
        }
    }

    @Override
    public Long getChildId(OCSCloneableMap mappingParent, List<Long> childrenIds,Map<Long, CloneDTO> oldNewChildMap, Long newParentId){
        Long childId = mappingParent.getChildId();
        Boolean isCloneNew = childrenIds.contains(childId);
        // if this child is create new

        Long finalChildId = childId;
        Optional<Long> newChildId = oldNewChildMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue().getId(), finalChildId)
                && Objects.equals(newParentId, entry.getValue().getParentId()))
            .map(Map.Entry::getKey)
            .findAny();

        if (newChildId.isPresent() && isCloneNew) {
            return newChildId.get();
        }
        return null;
    }
}
