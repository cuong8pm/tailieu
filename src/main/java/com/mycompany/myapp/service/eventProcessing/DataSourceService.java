package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.eventProcessing.DropdownDTO;
import com.mycompany.myapp.repository.eventProcessing.DataSourceRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.utils.OCSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataSourceService extends OCSBaseService {

    @Autowired
    private DataSourceRepository dataSourceRepository;

    @Override
    public void setBaseRepository() {
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }
}
