package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO;
import com.mycompany.myapp.mapper.eventProcessing.FunctionParamMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.ap.shaded.freemarker.cache.FileTemplateLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Transactional
@Service
public class FunctionParamService extends OCSBaseService implements ChildCloneable {

    @Autowired
    private FunctionParamRepository functionParamRepository;

    @Autowired
    private FunctionParamMapper functionParamMapper;

    @Autowired
    private FunctionRepository functionRepository;

    @Autowired
    private FunctionParamInstanceRepository functionParamInstanceRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private FunctionService functionService;

    @Autowired
    private FilterFieldRepository filterFieldRepository;

    @Autowired
    private ExpressionRepository expressionRepository;


    @Override
    public void setBaseRepository() {
        this.baseRepository = functionParamRepository;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return functionParamRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    public FunctionParamDTO createFunctionParam(FunctionParamDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateFunctionParamName(dto, domainId);
        Function function = functionRepository.findByIdAndDomainId(dto.getFunctionId(), domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
        }
        if (expressionRepository.countAllByInputTypeAndLinkedIdAndDomainId(Expression.InputType.FUNCTION, dto.getFunctionId(), domainId) > 0 ||
            filterFieldRepository.countFilterFieldByValueTypeAndLinkedIdAndDomainId(3, dto.getFunctionId(), domainId) > 0) {
            throw new DataConstrainException(ErrorMessage.Function.CANNOT_DELETE, Resources.FUNCTION, ErrorKey.ENTITY_NOT_FOUND);
        }
        FunctionParam functionParam = functionParamMapper.toEntity(dto);
        Integer maxOrderIndex = functionParamRepository.getMaxFunctionParamIdByFunctionIdAndDomainId(dto.getFunctionId(), domainId);
        if (maxOrderIndex == null) {
            functionParam.setIndexOrder(0);
        } else {
            functionParam.setIndexOrder(maxOrderIndex + 1);
        }
        functionParam.setId(null);
        functionParam.setDomainId(domainId);
        dto = functionParamMapper.toDto(functionParamRepository.save(functionParam));

        return dto;
    }

    public FunctionParamDTO updateFunctionParam(FunctionParamDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateFunctionParamName(dto, domainId);
        FunctionParam param = functionParamRepository.findByIdAndDomainId(dto.getId(), domainId);
        if (param == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ID);
        }
        Function function = functionRepository.findByIdAndDomainId(dto.getFunctionId(), domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        if (!dto.getEdit() && !Objects.equals(param.getDataType(), dto.getDataType())) {
            throw new DataConstrainException(ErrorMessage.Function.CANNOT_DELETE, Resources.FUNCTION_PARAM, "");
        }
        if (expressionRepository.countAllByInputTypeAndLinkedIdAndDomainId(Expression.InputType.FUNCTION, dto.getFunctionId(), domainId) > 0 ||
            filterFieldRepository.countFilterFieldByValueTypeAndLinkedIdAndDomainId(3, dto.getFunctionId(), domainId) > 0) {
            if (!Objects.equals(param.getDataType(), dto.getDataType()) || !Objects.equals(param.isEnable(), dto.isEnable())) {
                throw new DataConstrainException(ErrorMessage.Function.CANNOT_DELETE, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        List<FunctionParamInstance> instances = functionParamInstanceRepository.findAllByDomainIdAndFunctionParameterId(domainId, dto.getId());
        if (instances != null && !instances.isEmpty()) {
            if (!Objects.equals(param.getDataType(), dto.getDataType()) || !Objects.equals(param.isEnable(), dto.isEnable())) {
                throw new DataConstrainException(ErrorMessage.Function.CANNOT_DELETE, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
            }
        }
        FunctionParam functionParam = functionParamMapper.toEntity(dto);
        functionParam.setDomainId(domainId);
        dto = functionParamMapper.toDto(functionParamRepository.save(functionParam));
        setValueFilterField(dto.getFunctionId());
        return dto;
    }

    public void changeOrderIndex(List<Long> ids, Long functionId) {
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        FunctionParam firstParam = functionParamRepository.findByIdAndDomainIdForUpdate(ids.get(0), domainId);
        FunctionParam secondParam = functionParamRepository.findByIdAndDomainIdForUpdate(ids.get(1), domainId);
        if (firstParam == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ID);
        }
        if (secondParam == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ID);
        }
        if (!Objects.equals(firstParam.getFunctionId(), functionId) ||
            !Objects.equals(secondParam.getFunctionId(), functionId) ||
            !Objects.equals(firstParam.getFunctionId(), secondParam.getFunctionId())) {
            throw new PermissionDeniedException(ErrorMessage.Function.FUNCTION_INCORRECT, Resources.FUNCTION_PARAM, ErrorKey.ID);
        }
        functionRepository.findByIdAndDomainIdForUpdate(functionId, domainId);
        Integer tempIndex = firstParam.getIndexOrder();
        firstParam.setIndexOrder(secondParam.getIndexOrder());
        secondParam.setIndexOrder(tempIndex);
        functionParamRepository.save(firstParam);
        functionParamRepository.save(secondParam);
        setValueFilterField(functionId);
    }

    public void deleteFunctionParam(Long functionId, Long functionParamId) {
        Integer domainId = OCSUtils.getDomain();
        FunctionParam param = functionParamRepository.findByIdAndDomainId(functionParamId, domainId);
        if (param == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ID);
        }
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        functionService.validateDeleteFunction(functionId, domainId);
        functionParamRepository.deleteByIdAndDomainId(functionParamId, domainId);
        setValueFilterField(functionId);
    }


    public void checkDuplicateFunctionParamName(FunctionParamDTO dto, Integer domainId) {
        List<FunctionParam> functionParams = functionParamRepository
            .findAllByFunctionIdAndNameAndDomainId(dto.getFunctionId(), dto.getName(), domainId);
        if (functionParams != null
            && functionParams.stream().anyMatch(functionParam -> !Objects.equals(functionParam.getId(), dto.getId()))) {
            throw new DataConstrainException(ErrorMessage.Function.PARAM_DUPLICATED_NAME, Resources.FUNCTION_PARAM, ErrorKey.Function.PARAM_NAME);
        }
    }

    public Page<FunctionParamDTO> getFunctionParamsWithPaging(Long functionId, String name, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        Page<FunctionParamDTO> functionParamDTOS =
            functionParamRepository.findAllByFunctionIdAndNameAndDomainId(functionId, name, domainId, pageable).map(functionParamMapper::toDto);
        List<ReferTable> referTables = referTableRepository.getReferTablesByReferType(ReferType.DataType, sortByValue());
        Map<Integer, String> referTypeMap = referTables.stream().collect(Collectors.toMap(ReferTable::getValue, ReferTable::getName));
        boolean exitFilterField = filterFieldRepository.existsFilterFieldByLinkedIdAndValueTypeAndDomainId(function.getId(), 3, domainId);
        boolean exitExpression = expressionRepository.existsExpressionByLinkedIdAndInputTypeAndDomainId(function.getId(), 1, domainId);
        for (FunctionParamDTO functionParamDTO : functionParamDTOS) {
            functionParamDTO.setDataTypeName(referTypeMap.get(functionParamDTO.getDataType()));
            functionParamDTO.setEdit(!(exitFilterField || exitExpression));
        }
        return functionParamDTOS;
    }

    public FunctionParamDTO getDetailFunctionParam(Long functionId, Long functionParamId) {
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        if (function == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION, ErrorKey.ID);
        }
        FunctionParam param = functionParamRepository.findByIdAndDomainId(functionParamId, domainId);
        if (param == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ID);
        }
        FunctionParamDTO dto = functionParamMapper.toDto(functionParamRepository.findByIdAndDomainId(functionParamId, domainId));

        return dto;
    }

    public void setValueFilterField(Long functionId) {
        Integer domainId = OCSUtils.getDomain();
        Function function = functionRepository.findByIdAndDomainId(functionId, domainId);
        List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainIdOrderByIndexOrderAsc(functionId, domainId);
        List<FilterField> filterFields = filterFieldRepository.findAllByDomainIdAndValueTypeAndLinkedId(domainId, 3, functionId);
        List<Long> fieldIds = filterFields.stream().map(FilterField::getId).collect(Collectors.toList());
        List<FunctionParamInstance> instances =
            functionParamInstanceRepository.findAllByDomainIdAndOwnerTypeAndOwnerIdIn(domainId, Function.FunctionType.FIELD_TYPE, fieldIds);
        for (FilterField filterField : filterFields) {
            String value = function.getFunctionName() + "(";
            for (FunctionParam functionParam : functionParams) {
                String valueParam = "";
                if (functionParam.isEnable()) {
                    valueParam = instances.stream()
                        .filter(item ->
                            Objects.equals(filterField.getId(), item.getOwnerId())
                                && Objects.equals(item.getFunctionParameterId(), functionParam.getId()))
                        .map(FunctionParamInstance::getValue).findFirst().orElse(" ");
                } else {
                    valueParam = functionParam.getName();
                }
                value += valueParam + ",";
            }
            if (value.charAt(value.length() - 1) == ',') {
                value = value.substring(0, value.length() - 1);
            }
            value += ")";
            filterField.setValue(value);
        }
        filterFieldRepository.saveAll(filterFields);
    }

    public void setFunctionParamInstanceValue(List<FunctionParamDTO> paramDTOs, Long functionId, Integer domainId, Long ownerId, Integer ownerType) {
        if (paramDTOs != null && !paramDTOs.isEmpty()) {
            List<FunctionParamInstance> instances = new ArrayList<>();
            List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainId(functionId, domainId);
            functionParams.forEach(param -> {
                //kiểm tra function param trong db có bị trường hợp nào enalbe nhưng ko đc truyền value vào hay không
                Boolean check = false;
                for (FunctionParamDTO paramDTO : paramDTOs) {
                    if (java.util.Objects.equals(paramDTO.getId(), param.getId())) {
                        check = true;
                        if (!Objects.equals(param.isEnable(),paramDTO.isEnable())) {
                            throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                        }
                    }
                }
                Integer countInstance = functionParamInstanceRepository.countAllByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(domainId, param.getId(), ownerId, ownerType);
                if (!check && BooleanUtils.isTrue(param.isEnable()) && countInstance <= 0) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                }
            });
            for (FunctionParamDTO item : paramDTOs) {
                //check functionParam null
                FunctionParam functionParam = functionParams.stream().filter(param -> java.util.Objects.equals(param.getId(), item.getId())).findFirst().orElse(null);
                if (functionParam == null) {
                    throw new ResourceNotFoundException(ErrorMessage.FunctionParam.NOT_FOUND, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                }
                // kiểm tra nếu function param đã bị disable hoặc đã bị thay đổi data type so với lúc truyền vào
                if (!java.util.Objects.equals(item.getDataType(), functionParam.getDataType())
                    || !Objects.equals(item.isEnable(), functionParam.isEnable())) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                }
                // kiem tra function param name so doi voi owner type = filter field type
                if(ownerType == Function.FunctionType.FIELD_TYPE){
                    if(!StringUtils.equalsIgnoreCase(item.getName(), functionParam.getName())){
                        throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                    }
                }
                if(BooleanUtils.isTrue(item.isEnable())){
                    //check if instance is existed or not
                    FunctionParamInstance instance = functionParamInstanceRepository.findFirstByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(domainId, item.getId(), ownerId, ownerType);
                    // if not existed, add new
                    if (instance == null) {
                        instance = new FunctionParamInstance();
                        instance.setId(null);
                        instance.setOwnerId(ownerId);
                        instance.setDomainId(domainId);
                        instance.setOwnerType(ownerType);
                        instance.setFunctionParameterId(item.getId());
                    }
                    instance.setValue(setValueInstance(item));
                    instance.setValueType(item.getValueType());
                    instances.add(instance);
                }
            }
            functionParamInstanceRepository.saveAll(instances);
        } else {
            List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainId(functionId, domainId);
            functionParams.forEach(param -> {
                Integer countInstance = functionParamInstanceRepository.countAllByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(domainId, param.getId(), ownerId, ownerType);
                if (BooleanUtils.isTrue(param.isEnable()) && countInstance <= 0) {
                    throw new DataConstrainException(ErrorMessage.FunctionParam.DATA_HAS_BEEN_CHANGED, Resources.FUNCTION_PARAM, ErrorKey.ENTITY_NOT_FOUND);
                }
            });
        }
    }

    public void validateFunctionParams(List<Integer> errorRows, Integer index, List<FunctionParamDTO> paramDTOs, Long functionId, Integer domainId, Long ownerId, Integer ownerType){
        if(paramDTOs != null && !paramDTOs.isEmpty()){
            List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainId(functionId, domainId);
            functionParams.forEach(param -> {
                //kiểm tra function param trong db có bị trường hợp nào enalbe nhưng ko đc truyền value vào hay không
                Boolean check = false;
                for (FunctionParamDTO paramDTO : paramDTOs) {
                    if (java.util.Objects.equals(paramDTO.getId(), param.getId())) {
                        check = true;
                        if (!Objects.equals(paramDTO.isEnable(),param.isEnable())) {
                            if(!errorRows.contains(index+1)){
                                errorRows.add(index+1);
                            }
                            return;
                        }
                    }
                }
                Integer countInstance = functionParamInstanceRepository.countAllByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(domainId, param.getId(), ownerId, ownerType);
                if (!check && BooleanUtils.isTrue(param.isEnable()) && countInstance <= 0) {
                    if(!errorRows.contains(index+1)){
                        errorRows.add(index+1);
                    }
                    return;
                }
            });
            for (FunctionParamDTO item : paramDTOs) {
                //check functionParam null
                FunctionParam functionParam = functionParams.stream().filter(param -> java.util.Objects.equals(param.getId(), item.getId())).findFirst().orElse(null);
                if (functionParam == null) {
                    if(!errorRows.contains(index+1)){
                        errorRows.add(index+1);
                    }
                    return;
                }
                // kiểm tra nếu function param đã bị disable hoặc đã bị thay đổi data type so với lúc truyền vào
                if (!java.util.Objects.equals(item.getDataType(), functionParam.getDataType())
                    || !java.util.Objects.equals(item.isEnable(), functionParam.isEnable())) {
                    if(!errorRows.contains(index+1)){
                        errorRows.add(index+1);
                    }
                    return;
                }
                // kiem tra function param name so doi voi owner type = filter field type
                if(ownerType == Function.FunctionType.FIELD_TYPE){
                    if(!StringUtils.equalsIgnoreCase(item.getName(), functionParam.getName())){
                        if(!errorRows.contains(index+1)){
                            errorRows.add(index+1);
                        }
                        return;
                    }
                }
            }
        } else {
            List<FunctionParam> functionParams = functionParamRepository.findAllByFunctionIdAndDomainId(functionId, domainId);
            functionParams.forEach(param -> {
                Integer countInstance = functionParamInstanceRepository.countAllByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(domainId, param.getId(), ownerId, ownerType);
                if (BooleanUtils.isTrue(param.isEnable()) && countInstance <= 0) {
                    if(!errorRows.contains(index+1)){
                        errorRows.add(index+1);
                    }
                    return;
                }
            });
        }
    }

    private String setValueInstance(FunctionParamDTO functionParamDTO) {
        if (java.util.Objects.equals(functionParamDTO.getValueType(), 0)
            && java.util.Objects.equals(functionParamDTO.getDataType(), 1)) {
            return "\"" + functionParamDTO.getValue() + "\"";
        }
        return functionParamDTO.getValue();
    }

}
