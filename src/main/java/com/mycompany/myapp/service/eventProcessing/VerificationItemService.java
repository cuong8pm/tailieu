package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.MonitorKey;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Verification;
import com.mycompany.myapp.domain.eventProcessing.VerificationItem;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeDTO;
import com.mycompany.myapp.dto.eventProcessing.VerificationItemDTO;
import com.mycompany.myapp.mapper.eventProcessing.VerificationItemMapper;
import com.mycompany.myapp.mapper.eventProcessing.VerificationMapper;
import com.mycompany.myapp.repository.eventProcessing.VerificationItemRepository;
import com.mycompany.myapp.repository.eventProcessing.VerificationRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.*;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.Comparator;
import liquibase.pro.packaged.S;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class VerificationItemService extends OCSBaseService {
    private final String REGEX_ITEM_ID = "([0-9]*)";

    @Autowired
    private VerificationItemRepository verificationItemRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private VerificationItemMapper verificationItemMapper;

    public List<VerificationItemDTO> getVerificationItemByVerificationIdAndItem(Long verificationId, String item) {
        Integer domainId = OCSUtils.getDomain();
        Verification verification = verificationRepository.findByIdAndDomainId(verificationId, domainId);
        if (verification == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.VERIFICATION, ErrorKey.Verification.ITEM_ID);
        }
        List<VerificationItemDTO> verificationItemDTOList = verificationItemRepository
            .findVerificationItem(verificationId, item)
            .stream()
            .map(verificationItemMapper::toDto)
            .collect(Collectors.toList());
        return verificationItemDTOList;
    }

    public Page<VerificationItemDTO> getVerificationItemByVerificationId(String itemId, Long verificationId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Verification verification = verificationRepository.findByIdAndDomainId(verificationId, domainId);
        if (verification == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.VERIFICATION, ErrorKey.Verification.ITEM_ID);
        }
       Sort.Order order = pageable.getSort().getOrderFor("itemId");
        //khong sort theo itemID
        if (order== null){
            Page<VerificationItemDTO> page = verificationItemRepository.findVerificationItemWithPage(verificationId,domainId,itemId,pageable).map(verificationItemMapper::toDto);
            return page;
        }
        //sort theo itemId
        List<VerificationItemDTO> verificationItemsDTO = verificationItemRepository
            .findAllByVerificationIdAndDomainIdAndItem(verificationId,domainId, itemId)
            .stream()
            .map(verificationItemMapper::toDto)
            .collect(Collectors.toList());

        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), verificationItemsDTO.size());
//        int subend = (start + pageable.getPageSize()) > verificationItemsDTO.size() ? verificationItemsDTO.size()
//            : (start + pageable.getPageSize());
        Sort.Direction direction = order.getDirection();
        for (VerificationItemDTO item: verificationItemsDTO
             ) {
            item.setItemIdNumber();
        }
        if (direction.toString().equals("DESC")) {
            verificationItemsDTO.sort(Comparator.comparing(VerificationItemDTO::getItemIdNumber).reversed());
        }
        else {
            verificationItemsDTO.sort(Comparator.comparing(VerificationItemDTO::getItemIdNumber));
        }
        return new PageImpl<VerificationItemDTO>(verificationItemsDTO.subList(start,end), pageable, verificationItemsDTO.size());
    }

    public VerificationItemDTO createVerificationItem(VerificationItemDTO verificationItemDTO) {
        Integer domainId = OCSUtils.getDomain();
        Verification verification = verificationRepository.findByIdAndDomainId(verificationItemDTO.getVerificationId(), domainId);
        if (verification == null) {
            throw new ResourceNotFoundException(ErrorMessage.Verification.NOT_FOUND, Resources.VERIFICATION, ErrorKey.Verification.ID);
        }
        checkValidateForItemId(verificationItemDTO, domainId);
        return verificationItemDTO;
    }

    public VerificationItemDTO updateVerificationItem(VerificationItemDTO verificationItemDTO, Long verificationId) {
        Integer domainId = OCSUtils.getDomain();
        Verification verification = verificationRepository.findByIdAndDomainId(verificationId, domainId);
        if (verification == null) {
            throw new ResourceNotFoundException(ErrorMessage.Verification.NOT_FOUND, Resources.VERIFICATION, ErrorKey.Verification.ID);
        }
        VerificationItem verificationItem = verificationItemRepository.findByIdAndDomainId(verificationItemDTO.getId(), domainId);
        if (verificationItem == null) {
            throw new ResourceNotFoundException(ErrorMessage.Verification.ITEM_NOT_FOUND, Resources.VERIFICATION_ITEM, ErrorKey.Verification.ITEM_ID);
        }
        verificationItemDTO.setVerificationId(verificationId);
        checkValidateForItemId(verificationItemDTO, domainId);
        return verificationItemDTO;
    }

    private void checkValidateForItemId(VerificationItemDTO verificationItemDTO, Integer domainId) {
        List<VerificationItem> verificationItemList = verificationItemRepository.findAllByVerificationIdAndDomainId(
            verificationItemDTO.getVerificationId(),
            domainId
        );
        if (ObjectUtils.isEmpty(verificationItemDTO.getItemId())) {
            throw new DataInvalidException(ErrorMessage.Verification.ITEM_NOT_FOUND, Resources.VERIFICATION_ITEM, ErrorKey.Verification.ITEM_ID);
        }
        List<String> values;
        if (!verificationItemDTO.getIsMultiple()) {
            values = Collections.singletonList(verificationItemDTO.getItemId());
        } else {
            String value = verificationItemDTO.getItemId();
            values = Arrays.asList(value.split(","));
        }
        for (String str : values) {
            int number = Integer.parseInt(str);
            str = String.valueOf(number);
            if (str.length() > 10) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_TOO_BIG,
                    Resources.VERIFICATION_ITEM, ErrorKey.Verification.ITEM_ID);
            }
            Long value = Long.valueOf(str);
            if (value > Integer.MAX_VALUE) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_TOO_BIG,
                    Resources.VERIFICATION_ITEM, ErrorKey.Verification.ITEM_ID);
            }
            Matcher matcher = Pattern.compile(REGEX_ITEM_ID).matcher(str);
            if (matcher.matches()) {
                checkDuplicateItemId(verificationItemList, values, str);
                VerificationItem verificationItem = verificationItemMapper.toEntity(verificationItemDTO);
                verificationItem.setItemId(str);
                verificationItem.setDomainId(domainId);
                verificationItemRepository.save(verificationItem);
            } else {
                throw new DataConstrainException(ErrorMessage.INPUT_NOT_MATCH, Resources.VERIFICATION_ITEM, ErrorKey.Verification.NAME);
            }
        }
    }

    public void deleteVerificationItem(Long id) {
        Integer domainId = OCSUtils.getDomain();
        VerificationItem verificationItem = verificationItemRepository.findByIdAndDomainId(id, domainId);
        if (verificationItem != null) {
            verificationItemRepository.delete(verificationItem);
        }
    }

    private void checkDuplicateItemId(List<VerificationItem> verificationItemList, List<String> values, String str) {
        if (values.stream().distinct().count() != (long) values.size()) {
            throw new DataInvalidException(ErrorMessage.DUPLICATED_ITEM, Resources.VERIFICATION_ITEM, ErrorKey.Verification.ITEM_ID);
        }
        for (VerificationItem verificationItem : verificationItemList) {
            if (verificationItem.getItemId().equals(str)) {
                throw new DataInvalidException(ErrorMessage.DUPLICATED_ITEM, Resources.VERIFICATION_ITEM, ErrorKey.Verification.ITEM_ID);
            }
        }
    }

    @Override
    public void setBaseRepository() {}

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }
}
