package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.Language;
import com.mycompany.myapp.domain.eventProcessing.Notify;
import com.mycompany.myapp.dto.eventProcessing.LanguageDTO;
import com.mycompany.myapp.dto.eventProcessing.NotifyDTO;
import com.mycompany.myapp.mapper.eventProcessing.NotifyMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderRepository;
import com.mycompany.myapp.repository.eventProcessing.NotifyRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class NotifyService extends OCSBaseService implements ChildBuilderService {
    static final Long BUILDER_TYPE_SMS = 1L;

    @Autowired
    private NotifyRepository notifyRepository;

    @Autowired
    private NotifyMapper notifyMapper;

    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private LanguageService languageService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = notifyRepository;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return notifyRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    public NotifyDTO getNotifyDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Notify notify = notifyRepository.findByIdAndDomainId(id, domainId);
        if (notify == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOTIFY_NOT_FOUND, Resources.NOTIFY, ErrorKey.ENTITY_NOT_FOUND);
        }
        NotifyDTO notifyDTO = notifyMapper.toDto(notify);
        return notifyDTO;
    }

    public Page<NotifyDTO> getNotifies(Long builderId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(builderId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        Page<NotifyDTO> notifyDTOS = null;
        List<LanguageDTO> languages = languageService.getLanguages();
        if (builder.getBuilderTypeId().equals(BUILDER_TYPE_SMS)) {
            Page<Notify> notifies = notifyRepository.findNotifyByBuilderId(builderId, domainId, pageable);
            notifyDTOS = notifies.map(notifyMapper::toDto);
            for (NotifyDTO notifyDTO : notifyDTOS) {
                for (LanguageDTO languageDTO : languages) {
                    if (languageDTO.getId().equals(notifyDTO.getLanguageId())) {
                        notifyDTO.setLanguageName(languageDTO.getName());
                    }
                }
            }
        }
        return notifyDTOS;
    }

    public NotifyDTO createNotify(NotifyDTO notifyDTO, Long id) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(id, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }

        Notify notify = notifyMapper.toEntity(notifyDTO);
        notify.setId(null);
        notify.setDomainId(domainId);
        notify.setBuilderId(id);
        List<Notify> notifys = notifyRepository.findAllByBuilderIdAndLanguageId(id, notifyDTO.getLanguageId());
        if (!notifys.isEmpty()) {
            throw new ResourceNotFoundException(ErrorMessage.Notify.DUPLICATED_LANGUAGE, Resources.NOTIFY, ErrorKey.Notify.LANGUAGEID);
        }
        notify.setLanguageId(notifyDTO.getLanguageId());
        notify = notifyRepository.save(notify);
        NotifyDTO notifyDtoRes = notifyMapper.toDto(notify);
        return notifyDtoRes;
    }

    public NotifyDTO updateNotify(NotifyDTO notifyDTO, Long builderId) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(builderId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        Notify notifyCurrent = notifyRepository.findByIdAndDomainId(notifyDTO.getId(), domainId);
        if (notifyCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NOTIFY, ErrorKey.Notify.ID);
        }
        List<Notify> notifys = notifyRepository.findAllByBuilderIdAndLanguageId(builderId, notifyDTO.getLanguageId());
        if (!notifys.isEmpty()) {
            if (notifys.size() != 1 || !notifys.get(0).getId().equals(notifyDTO.getId())) {
                throw new ResourceNotFoundException(ErrorMessage.Notify.DUPLICATED_LANGUAGE, Resources.NOTIFY, ErrorKey.Notify.LANGUAGEID);
            }
        }
        Notify notify = notifyMapper.toEntity(notifyDTO);
        notify.setDomainId(domainId);
        notifyRepository.save(notify);
        NotifyDTO dto = notifyMapper.toDto(notify);
        return dto;
    }

    public void deleteNotify(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Notify notify = notifyRepository.findByIdAndDomainId(id, domainId);
        if(notify == null){
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOTIFY_NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        Builder builder = builderRepository.findByIdAndDomainId(notify.getBuilderId(), domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        if (notify != null) {
            notifyRepository.delete(notify);
        }
    }
}
