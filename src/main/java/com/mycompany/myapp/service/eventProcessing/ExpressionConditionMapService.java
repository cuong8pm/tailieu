package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.eventProcessing.Expression;
import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ExpressionConditionMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ExpressionRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.utils.OCSUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExpressionConditionMapService extends AbstractCloneMappingService implements ConditionChildrenMapService {

    @Autowired
    private ExpressionConditionMapRepository expressionConditionMapRepository;

    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private ConditionService conditionService;

    @Autowired
    private ExpressionRepository expressionRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = expressionConditionMapRepository;
        this.childService = expressionService;
        this.parentService = conditionService;
    }

    @Override
    protected String getResource() {
        return null;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return expressionConditionMapRepository;
    }

    @Override
    public List<OCSCloneableMap> getMappingByParent(List<Long> ids) {
        return new ArrayList<>();
    }

    // không truyền vào cloneDTOs và map cha mới- list con cũ || tạo mới tất cả expression, ko được mapping condition mới - expression cũ
    @Override
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        if (ocsCloneMapRepository == null || childService == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }
        Integer domainId = OCSUtils.getDomain();
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        Map<Long, Long> newOldParentIdMap = new HashMap<>();
        Map<Long, List<Long>> oldChildNewParentsMap = new HashMap<>();
        long batchSize = 30;
        long count = 0;

        for (Long newId : oldNewIdParentMap.keySet()) {
            newOldParentIdMap.put(newId, oldNewIdParentMap.get(newId).getId());
        }

        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());

        //thêm mapping giữa con cũ - cha mới
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);
        List<Long> oldChildIds = mappingParents.stream().map(OCSCloneableMap::getChildId).collect(Collectors.toList());
        mappingParents.forEach(item ->{
            List<Long> newConditionIds = oldNewIdParentMap.entrySet().stream()
                .filter(entry -> Objects.equals(entry.getValue().getId(),item.getParentId()))
                .map(Map.Entry::getKey).collect(Collectors.toList());
            oldChildNewParentsMap.put(item.getChildId(),newConditionIds);
        });

        // query trong db lấy list expression để clone
        List<Expression> expressionToClones = expressionRepository.findByDomainIdAndIdIn(domainId,oldChildIds);
        List<CloneDTO> idsToCreateNew = new ArrayList<>();

        expressionToClones.forEach(item ->{
            List<Long> parentsIds = oldChildNewParentsMap.get(item.getId());
            if(parentsIds != null){
                parentsIds.forEach(id ->{
                    CloneDTO cloneDTO = new CloneDTO();
                    cloneDTO.setId(item.getId());
                    cloneDTO.setParentId(id);
                    idsToCreateNew.add(cloneDTO);
                });
            }
        });

        // clone tầng tiếp theo
        expressionService.cloneExpressions(idsToCreateNew, oldNewChildMap, Function.FunctionType.CONDITION_TYPE);

        List<OCSCloneableMap> newMap = new ArrayList<>();
        for (Long newParentId : newParentIds) {
            Long oldParentId = newOldParentIdMap.get(newParentId);
            List<OCSCloneableMap> oldMappingParents = mappingParents.stream().filter(i -> Objects.equals(i.getParentId(), oldParentId)).collect(Collectors.toList());
            for (OCSCloneableMap oldMappingParent : oldMappingParents) {
                Long childId = getChildId(oldMappingParent,null,oldNewChildMap, newParentId);
                if(childId != null){
                    OCSCloneableMap newCloneMap = SerializationUtils.clone(oldMappingParent);
                    newCloneMap.setChildMappingId(childId);
                    newCloneMap.clearId();
                    newCloneMap.setParentMappingId(newParentId);
                    newMap.add(newCloneMap);

                    entityManager.persist(newCloneMap);
                    if (count % batchSize == 0 && count > 0) {
                        entityManager.flush();
                        entityManager.clear();
                        newMap.clear();
                    }
                    count++;
                }
            }

        }

        if (!CollectionUtils.isEmpty(newMap)) {
            entityManager.flush();
            entityManager.clear();
            newMap.clear();
        }
    }

    @Override
    public Long getChildId(OCSCloneableMap mappingParent, List<Long> childrenIds, Map<Long, CloneDTO> oldNewChildMap, Long newParentId) {
        // toàn bộ expression là tạo mới và mapping với cha mới
        Optional<Long> newChildId = oldNewChildMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue().getId(), mappingParent.getChildId())
                && Objects.equals(newParentId, entry.getValue().getParentId()))
            .map(Map.Entry::getKey)
            .findAny();

        if (newChildId.isPresent()) {
            return newChildId.get();
        }
        return null;
    }
}
