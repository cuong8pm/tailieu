package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.domain.eventProcessing.BuilderTypeRouterMap;
import com.mycompany.myapp.domain.eventProcessing.Router;
import com.mycompany.myapp.dto.eventProcessing.RouterDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRouterMapRepository;
import com.mycompany.myapp.repository.eventProcessing.RouterRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class BuilderTypeRouterMapService extends AbstractCloneMappingService implements OCSCloneMappingService {
    @Autowired
    private BuilderTypeRouterMapRepository builderTypeRouterMapRepository;

    @Autowired
    private BuilderTypeService builderTypeService;

    @Autowired
    private RouterService routerService;

    @Autowired
    private RouterRepository routerRepository;

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = builderTypeRouterMapRepository;
        this.childService = routerService;
        this.parentService = builderTypeService;
        this.parentDependService = builderTypeService;
    }

    @Override
    protected String getResource() {
        return null;
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }

    public List<Long> getRouterIdByBuilderTypeId(Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        return builderTypeRouterMapRepository.findRouterIdByBuilderTypeId(domainId, builderTypeId);
    }

    public List<BuilderTypeRouterMap> getBuilderTypeRouterMapByBuilderTypeId(Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        return builderTypeRouterMapRepository.findBuilderTypeRouterMapByBuilderTypeId(domainId, builderTypeId);
    }

    public void createBuilderTypeRouterMap(ArrayList<Long> routerIds, Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.BuilderType.ID);
        }
        Integer countRouter = routerRepository.countRoutersByIdIn(routerIds);
        if (!Objects.equals(countRouter,routerIds.size())) {
            throw new ResourceNotFoundException(ErrorMessage.Router.NOT_FOUND, Resources.ROUTER, ErrorKey.ENTITY_NOT_FOUND);
        }
        List<BuilderTypeRouterMap> list = new ArrayList<>();
        for (Long routerId : routerIds) {
            BuilderTypeRouterMap builderTypeRouterMap = new BuilderTypeRouterMap();
            builderTypeRouterMap.setId(null);
            builderTypeRouterMap.setDomainId(domainId);
            builderTypeRouterMap.setBuilderTypeId(builderTypeId);
            builderTypeRouterMap.setRouterId(routerId);
            list.add(builderTypeRouterMap);
        }
        builderTypeRouterMapRepository.saveAll(list);
    }

    public void deleteBuilderTypeRouterMap(Long routerId, Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        Router router = routerRepository.findByIdAndDomainId(routerId, domainId);
        if (router == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.ROUTER, ErrorKey.Router.ID);
        }
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.BuilderType.ID);
        }

        deleteMapping(builderTypeId,routerId);
    }

    public Boolean existByRouterId(Long routerId) {
        Integer domainId = OCSUtils.getDomain();
        return builderTypeRouterMapRepository.existsByRouterIdAndDomainId(routerId, domainId);
    }
    @Override
    public void deleteMapping(Long parentId, Long childId) {
        Integer domainId = OCSUtils.getDomain();
        OCSCloneableMap ocsCloneableMap = ocsCloneMapRepository.findByDomainIdAndChildIdAndParentId(domainId, childId, parentId);
        if (ocsCloneableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, getResource(), ErrorKey.RateTable.ID);
        }

        ocsCloneMapRepository.deleteByDomainIdAndChildIdAndParentId(domainId, childId, parentId);
    }
}
