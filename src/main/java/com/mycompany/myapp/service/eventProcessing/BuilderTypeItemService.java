package com.mycompany.myapp.service.eventProcessing;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.domain.eventProcessing.BuilderTypeItem;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeItemDTO;
import com.mycompany.myapp.mapper.eventProcessing.BuilderTypeItemMapper;
import com.mycompany.myapp.repository.eventProcessing.BuilderItemRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeItemRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.eventProcessing.*;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class BuilderTypeItemService extends OCSBaseService implements ChildCloneable {
    @Autowired
    private BuilderTypeItemRepository builderTypeItemRepository;

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @Autowired
    private BuilderTypeItemMapper builderTypeItemMapper;

    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private BuilderItemRepository builderItemRepository;

    @Override
    public void setBaseRepository() {}

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return builderTypeItemRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    public Page<BuilderTypeItemDTO> getBuilderTypeItemsByBuilderTypeId(Long builderTypeId, String name, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        Page<BuilderTypeItemDTO> builderTypeItemDTOS = builderTypeItemRepository
            .findBuilderTypeItemsWithPage(builderType.getId(), domainId, name, pageable)
            .map(builderTypeItemMapper::toDto);
        return builderTypeItemDTOS;
    }

    public BuilderTypeItemDTO createBuilderTypeItem(BuilderTypeItemDTO builderTypeItemDTO) {
        Integer domainId = OCSUtils.getDomain();
        validateForeignId(builderTypeItemDTO.getBuilderTypeId(), 0l, domainId, "create");
        BuilderTypeItem builderTypeItem = builderTypeItemMapper.toEntity(builderTypeItemDTO);
        checkDuplicateItemName(builderTypeItem.getName(), builderTypeItem.getId(), builderTypeItemDTO.getBuilderTypeId());
        builderTypeItem.setDomainId(domainId);
        builderTypeItem.setId(null);
        builderTypeItem = builderTypeItemRepository.save(builderTypeItem);
        builderTypeItemDTO = builderTypeItemMapper.toDto(builderTypeItem);
        return builderTypeItemDTO;
    }

    public BuilderTypeItemDTO updateBuilderTypeItem(BuilderTypeItemDTO builderTypeItemDTO) {
        Integer domainId = OCSUtils.getDomain();
        validateForeignId(builderTypeItemDTO.getBuilderTypeId(), builderTypeItemDTO.getId(), domainId, "update");
        BuilderTypeItem builderTypeItem = builderTypeItemMapper.toEntity(builderTypeItemDTO);
        checkDuplicateItemName(builderTypeItem.getName(), builderTypeItem.getId(), builderTypeItemDTO.getBuilderTypeId());
        builderTypeItem.setDomainId(domainId);
        builderTypeItem = builderTypeItemRepository.save(builderTypeItem);
        builderTypeItemDTO = builderTypeItemMapper.toDto(builderTypeItem);
        return builderTypeItemDTO;
    }

    public void deleteBuilderTypeItem(Long builderTypeId, Long builderTypeItemId) {
        Integer domainId = OCSUtils.getDomain();
        validateForeignId(builderTypeId, builderTypeItemId, domainId, "delete");
        if (builderItemRepository.countBuilderItemsByBuilderTypeItemIdAndDomainId(builderTypeItemId, domainId) > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BUILDER_TYPE_ITEM, ErrorKey.ID);
        }
        builderTypeItemRepository.deleteByIdAndDomainId(builderTypeItemId, domainId);
    }

    private void validateForeignId(Long builderTypeId, Long builderTypeItemId, Integer domainId, String action) {
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        if (!action.equalsIgnoreCase("create")) {
            BuilderTypeItem builderTypeItem = builderTypeItemRepository.findByIdAndDomainId(builderTypeItemId, domainId);
            if (builderTypeItem == null) {
                throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE_ITEM, ErrorKey.ID);
            }
        }
    }


    public void checkDuplicateItemName(String name, Long itemId, Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        List<BuilderTypeItem> builderTypeItems = builderTypeItemRepository.findAllByNameAndBuilderTypeIdAndDomainId(name,builderTypeId, domainId);
        if (
            builderTypeItems != null && builderTypeItems.stream().anyMatch(builderTypeItem -> !Objects.equal(itemId, builderTypeItem.getId()))
        ) {
            throw new DataConstrainException(ErrorMessage.BuilderTypeItem.DUPLICATED_NAME, Resources.BUILDER_TYPE_ITEM, ErrorKey.NAME);
        }
    }

    public Page<BuilderTypeItemDTO> getBuilderTypeItemByBuilder(Long buildId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(buildId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER, ErrorKey.ID);
        }
        Page<BuilderTypeItem> builderTypeItems = builderTypeItemRepository.findBuilderTypeItemsWithPage(
            builder.getBuilderTypeId(),
            domainId,
            pageable
        );
        Page<BuilderTypeItemDTO> builderTypeItemDTOS = builderTypeItems.map(builderTypeItemMapper::toDto);
        return builderTypeItemDTOS;
    }

    public void deleteBuilderTypeItemsByBuilderTypeId(Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        if (builderItemRepository.countBuilderItemsByBuilderTypeItemIdInAndDomainId(builderTypeId, domainId) > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.BUILDER_TYPE_ITEM, ErrorKey.ID);
        }
        builderTypeItemRepository.deleteAllByBuilderTypeIdAndDomainId(builderTypeId, domainId);
    }

    public BuilderTypeItemDTO getDetailBuilderTypeItem(Long builderTypeId, Long builderTypeItemId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.ID);
        }
        BuilderTypeItem builderTypeItem = builderTypeItemRepository.findByIdAndDomainId(builderTypeItemId, domainId);
        if (builderTypeItem == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE_ITEM, ErrorKey.ID);
        }
        BuilderTypeItemDTO dto = builderTypeItemMapper.toDto(builderTypeItem);

        return dto;
    }
    public List<BuilderTypeItemDTO> getListBuilderTypeItemDTOByBuilderTypeId(Long builderId, Long builderTypeId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.BuilderType.ID);
        }
        List<BuilderTypeItemDTO> builderTypeItemDTOList = builderTypeItemRepository
            .findAllItemToAddInstanceValue(builderTypeId,builderId, domainId)
            .stream()
            .map(builderTypeItemMapper::toDto)
            .collect(Collectors.toList());
        return builderTypeItemDTOList;
    }
}
