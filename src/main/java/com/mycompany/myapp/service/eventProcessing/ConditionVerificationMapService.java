package com.mycompany.myapp.service.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Condition;
import com.mycompany.myapp.domain.eventProcessing.ConditionVerificationMap;
import com.mycompany.myapp.domain.eventProcessing.Verification;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.eventProcessing.ConditionVerificationMapDTO;
import com.mycompany.myapp.dto.eventProcessing.VerificationDTO;
import com.mycompany.myapp.mapper.eventProcessing.ConditionVerificationMapMapper;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionRepository;
import com.mycompany.myapp.repository.eventProcessing.ConditionVerificationMapRepository;
import com.mycompany.myapp.repository.eventProcessing.VerificationRepository;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Transactional
public class ConditionVerificationMapService extends AbstractCloneMappingService implements ConditionChildrenMapService{
    @Autowired
    private ConditionVerificationMapRepository conditionVerificationMapRepository;

    @Autowired
    private VerificationService verificationService;

    @Autowired
    private ConditionService conditionService;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private ConditionVerificationMapMapper conditionVerificationMapMapper;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = conditionVerificationMapRepository;
        this.childService = verificationService;
        this.parentService = conditionService;
        this.parentDependService = conditionService;
    }

    @Override
    protected String getResource() {
        return Resources.VERIFICATION;
    }

    @Override
    protected CategoryType getCategoryType() {
        return verificationService.getCategoryType();
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return conditionVerificationMapRepository;
    }

    public List<Long> getConditionIdAndVerificationId(Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        return conditionVerificationMapRepository.findVerificationIdByConditionId(domainId, conditionId);
    }

    public List<ConditionVerificationMap> getVerificationByConditionId(Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        return conditionVerificationMapRepository.findVerificationByConditionId(domainId, conditionId);
    }

    public List<ConditionVerificationMapDTO> createConditionVerificationMap(List<VerificationDTO> verificationDTOList, Long conditionId) {
        Integer domainId = OCSUtils.getDomain();
        Condition condition = conditionRepository.findByIdAndDomainId(conditionId, domainId);
        if (condition == null) {
            throw new ResourceNotFoundException(ErrorMessage.Condition.NOT_FOUND, Resources.CONDITION, ErrorKey.ENTITY_NOT_FOUND);
        }
        List<ConditionVerificationMap> conditionVerificationMapList = new ArrayList<>();
        for (VerificationDTO verificationDTO : verificationDTOList) {
            Verification verification = verificationRepository.findByIdAndDomainId(verificationDTO.getId(), domainId);
            if (verification == null) {
                throw new ResourceNotFoundException(ErrorMessage.Verification.NOT_FOUND, Resources.VERIFICATION, ErrorKey.ENTITY_NOT_FOUND);
            }
            if (conditionVerificationMapRepository.existsByVerificationIdAndConditionIdAndDomainId(verificationDTO.getId(), conditionId, domainId)) {
                throw new DataConstrainException(ErrorMessage.DUPLICATED_CELL_ID, Resources.VERIFICATION, ErrorKey.Verification.ID);
            }
            ConditionVerificationMap conditionVerificationMap = new ConditionVerificationMap();
            conditionVerificationMap.setDomainId(domainId);
            conditionVerificationMap.setConditionId(conditionId);
            conditionVerificationMap.setVerificationId(verificationDTO.getId());
            conditionVerificationMapList.add(conditionVerificationMap);
        }
        conditionVerificationMapList = conditionVerificationMapRepository.saveAll(conditionVerificationMapList);
        List<ConditionVerificationMapDTO> conditionVerificationMapDTOList = conditionVerificationMapMapper.toDto(
            conditionVerificationMapList
        );
        return conditionVerificationMapDTOList;
    }

    public void deleteByConditionAndVerification(Long conditionId, Long verificationId) {
        Integer domainId = OCSUtils.getDomain();
        List<ConditionVerificationMap> conditionVerificationMaps = conditionVerificationMapRepository.findByVerificationIdAndConditionId(
            domainId,
            verificationId,
            conditionId
        );
        if (CollectionUtils.isEmpty(conditionVerificationMaps)) {
            throw new ResourceConflictException(ErrorMessage.Verification.NOT_FOUND, Resources.CONDITION, ErrorKey.ENTITY_NOT_FOUND);
        }
        conditionVerificationMapRepository.deleteByVerificationId(domainId, verificationId, conditionId);
    }

    public Boolean existByVerificationId(Long verificationId) {
        Integer domainId = OCSUtils.getDomain();
        return conditionVerificationMapRepository.existsByVerificationIdAndDomainId(verificationId, domainId);
    }

    // không truyền vào cloneDTOs và map cha mới- list con cũ || chỉ tạo mapping mới, ko clone mới verification
    @Override
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        if (ocsCloneMapRepository == null || childService == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }
        Integer domainId = OCSUtils.getDomain();
        Map<Long, Long> newOldParentIdMap = new HashMap<>();
        List<OCSCloneableMap> newMap = new ArrayList<>();
        long batchSize = 30;
        long count = 0;

        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);

        for (Long newId : oldNewIdParentMap.keySet()) {
            newOldParentIdMap.put(newId, oldNewIdParentMap.get(newId).getId());
        }

        for (Long newParentId : newParentIds) {
            Long oldParentId = newOldParentIdMap.get(newParentId);
            List<OCSCloneableMap> oldMappingParents = mappingParents.stream().filter(i -> Objects.equals(i.getParentId(), oldParentId)).collect(Collectors.toList());
            for (OCSCloneableMap oldMappingParent : oldMappingParents) {
                Long childId = oldMappingParent.getChildId();
                if(childId != null){
                    OCSCloneableMap newCloneMap = SerializationUtils.clone(oldMappingParent);
                    newCloneMap.setChildMappingId(childId);
                    newCloneMap.clearId();
                    newCloneMap.setParentMappingId(newParentId);
                    newMap.add(newCloneMap);

                    entityManager.persist(newCloneMap);
                    if (count % batchSize == 0 && count > 0) {
                        entityManager.flush();
                        entityManager.clear();
                        newMap.clear();
                    }
                    count++;
                }
            }

        }

        if (!CollectionUtils.isEmpty(newMap)) {
            entityManager.flush();
            entityManager.clear();
            newMap.clear();
        }
    }
}
