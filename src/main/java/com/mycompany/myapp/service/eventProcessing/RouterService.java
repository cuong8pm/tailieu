package com.mycompany.myapp.service.eventProcessing;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.mapper.eventProcessing.RouterMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.repository.eventProcessing.RouterRepository;
import com.mycompany.myapp.repository.eventProcessing.RouterTypeRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service("routerService")
@Transactional
public class RouterService extends AbstractCloneService {
    @Autowired
    private RouterRepository routerRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RouterMapper routerMapper;

    @Autowired
    private RouterTypeRepository routerTypeRepository;

    @Autowired
    private BuilderTypeRouterMapService builderTypeRouterMapService;

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = routerRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = routerRepository;
        this.parentDependMappingServices = Arrays.asList(builderTypeRouterMapService);
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Collections.emptyList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Collections.emptyList();
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new RouterDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.ROUTER;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Router.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.ROUTER;
    }

    @Override
    protected String getNameCategory() {
        return "Router";
    }

    public RouterDTO getRouterDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Router router = routerRepository.findByIdAndDomainId(id, domainId);
        if (router == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.ROUTER, ErrorKey.Router.ID);
        }
        List<PropertiesDTO> propertiesDTOList = getPropertiesDTOS(router);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(router.getParentId(), domainId);
        RouterDTO routerDTO = routerMapper.toDto(router);
        routerDTO.setPropertiesDTOS(propertiesDTOList);
        if (category != null) {
            routerDTO.setCategoryId(category.getId());
            routerDTO.setCategoryName(category.getName());
            routerDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        return routerDTO;
    }

    private List<PropertiesDTO> getPropertiesDTOS(Router router) {
        List<PropertiesDTO> propertiesDTOList = new ArrayList<>();
        String prop = router.getProperties();
        if(!StringUtils.isEmpty(prop)){
            List<String> props = Arrays.asList(prop.split(";"));
            for (int i = 0; i < props.size(); i++) {
                List<String> propItem = Arrays.asList(props.get(i).split("="));
                PropertiesDTO propertiesDTO = new PropertiesDTO();
                propertiesDTO.setId(Long.parseLong(String.valueOf(i + 1)));
                propertiesDTO.setName(propItem.get(0));
                propertiesDTO.setValue(propItem.get(1));
                propertiesDTOList.add(propertiesDTO);
            }
        }
        return propertiesDTOList;
    }

    public void deleteRouter(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Router router = routerRepository.findByIdAndDomainId(id, domainId);
        if (builderTypeRouterMapService.existByRouterId(id)) {
            throw new ResourceConflictException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.ROUTER, ErrorKey.Router.ID);
        }
        if(routerRepository.countFilterByRouterId(id, domainId) > 0){
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT,Resources.ROUTER,ErrorKey.ID);
        }
        if (router != null) {
            routerRepository.deleteByIdAndDomainId(id, domainId);
        }
    }

    public RouterDTO createRouter(RouterDTO routerDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateName(routerDTO.getName(), routerDTO.getId());
        checkCategory(routerDTO.getCategoryId());
        StringBuilder prop = getProperties(routerDTO);
        Router router = routerMapper.toEntity(routerDTO);
        router.setId(null);
        router.setDomainId(domainId);
        if(prop.length() > 0){
            router.setProperties(prop.substring(0, prop.length() - 1));
        } else {
            router.setProperties(null);
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp modifyDate = Timestamp.valueOf(localDateTime);
        router.setModifyDate(modifyDate);
        setPosIndex(domainId, router, router.getCategoryId());
        router = routerRepository.save(router);
        RouterDTO routerDTORes = routerMapper.toDto(router);

        return routerDTORes;
    }

    public RouterDTO updateRouter(RouterDTO routerDTO) {
        Integer domainId = OCSUtils.getDomain();
        StringBuilder prop = getProperties(routerDTO);
        Router routeCurrent = routerRepository.findByIdAndDomainId(routerDTO.getId(), domainId);
        if (routeCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.ROUTER, ErrorKey.Router.ID);
        }
        Router router = routerMapper.toEntity(routerDTO);
        if (!Objects.equal(routerDTO.getCategoryId(), routeCurrent.getCategoryId())) {
            setPosIndex(domainId, router, routerDTO.getCategoryId());
        }
        if (!routerDTO.getName().equals(routeCurrent.getName())) {
            checkDuplicateName(router.getName(), router.getId());
        }
        checkCategory(routerDTO.getCategoryId());
        router.setDomainId(domainId);
        if(prop.length() > 0){
            router.setProperties(prop.substring(0, prop.length() - 1));
        } else {
            router.setProperties(null);
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp modifyDate = Timestamp.valueOf(localDateTime);
        router.setModifyDate(modifyDate);
        routerRepository.save(router);
        RouterDTO dto = routerMapper.toDto(router);
        return dto;
    }

    public Page<RouterDTO> getRouters(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<RouterDTO> dtos = routerRepository.getRouters(domainId, categoryId, name, description, pageable).map(routerMapper::toDto);
        return dtos;
    }

    private StringBuilder getProperties(RouterDTO routerDTO) {
        List<PropertiesDTO> propertiesDTOList = routerDTO.getPropertiesDTOS();
        StringBuilder prop = new StringBuilder();
        if(propertiesDTOList != null){
            for (PropertiesDTO  propertiesDTO : propertiesDTOList) {
                String name = propertiesDTO.getName();
                String value = propertiesDTO.getValue();
                prop.append(name).append("=").append(value).append(";");
            }
        }
        return prop;
    }

    public Page<RouterDTO> getListRouterDTOByBuilderTypeId(Long builderTypeId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.BuilderType.ID);
        }
        Page<RouterDTO> routerDTOS = routerRepository.getRouterByBuilderTypeId(builderTypeId, pageable).map(routerMapper::toDto);

        return routerDTOS;
    }

    public List<RouterDTO> getListRouterNotPaging(Long builderTypeId, Pageable unpaged){
        Integer domainId = OCSUtils.getDomain();
        BuilderType builderType = builderTypeRepository.findByIdAndDomainId(builderTypeId, domainId);
        if (builderType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE, ErrorKey.BuilderType.ID);
        }
        List<RouterDTO> routerDTOs = routerRepository.getRouterByBuilderTypeId(builderTypeId, unpaged).map(routerMapper::toDto).getContent();
        return routerDTOs;
    }

    public List<DropdownDTO> getDropdownRouterType(){
        Integer domainId = OCSUtils.getDomain();
        return routerTypeRepository.findAllByDomainId(domainId);
    }


    @Override
    protected OCSCloneableEntity setPropertiesOfCloneEntity(OCSCloneableEntity clonedEntity, CloneDTO cloneDTO, String newName) {
        if(clonedEntity instanceof Router){
            ((Router) clonedEntity).setModifyDate(new Timestamp((new Date()).getTime()));

        }
        return super.setPropertiesOfCloneEntity(clonedEntity,cloneDTO,newName);

    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Router.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.Router.NOT_FOUND,getResourceName(),ErrorKey.ENTITY_NOT_FOUND);
    }
}
