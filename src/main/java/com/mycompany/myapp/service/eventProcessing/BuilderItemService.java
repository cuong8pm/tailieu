package com.mycompany.myapp.service.eventProcessing;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.BuilderItem;
import com.mycompany.myapp.domain.eventProcessing.BuilderTypeItem;
import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.dto.eventProcessing.BuilderItemDTO;
import com.mycompany.myapp.mapper.eventProcessing.BuilderItemMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderItemRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeItemRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BuilderItemService extends OCSBaseService implements ChildBuilderService {

    @Autowired
    private BuilderItemRepository builderItemRepository;

    @Autowired
    private BuilderItemMapper builderItemMapper;

    @Autowired
    private BuilderRepository builderRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private BuilderTypeItemRepository builderTypeItemRepository;

    @Override
    public void setBaseRepository() {
        this.baseRepository = builderItemRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    public BuilderItemDTO getBuilderItemDetail(Long builderId, Long builderItemId) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(builderId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        BuilderItem builderItem = builderItemRepository.findByIdAndDomainId(builderItemId, domainId);
        if (builderItem == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.BUILDER_ITEM_NOT_FOUND, Resources.BUILDER_ITEM, ErrorKey.ENTITY_NOT_FOUND);
        }
        BuilderItemDTO builderItemDTO = builderItemMapper.toDto(builderItem);
        builderItemDTO = checkDataTypeEqualString(builderItemDTO);
        return builderItemDTO;
    }

    private BuilderItemDTO checkDataTypeEqualString(BuilderItemDTO builderItemDTO) {
        if (builderItemDTO.getValueType().equals(BuilderItem.ValueType.MANUAL)) {
            if (builderItemDTO.getDataType().equals(BuilderItem.DataType.STRING)) {
                builderItemDTO.setValue(getStringValue(builderItemDTO.getValue()));
                builderItemDTO.setName(builderItemDTO.getValue());
            }
        }
        return builderItemDTO;
    }

    private String getStringValue(String oldValue) {
        String value = oldValue;
        if (!org.springframework.util.StringUtils.isEmpty(value) && value.charAt(0) == '\"') {
            value = value.substring(1);
        }
        if (!org.springframework.util.StringUtils.isEmpty(value) && value.charAt(value.length() - 1) == '\"') {
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }

    public Page<BuilderItemDTO> getBuilderItemsByBuilderId(Long builderId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();

        Sort.Order order = pageable.getSort().getOrderFor(Filter.FieldNames.POS_INDEX);
        if (order != null) {
            pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.ASC, "builderTypeItemName"));
        }
        Builder builder = builderRepository.findByIdAndDomainId(builderId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        Page<BuilderItemDTO> builderItemDTOS = builderItemRepository.findBuilderItemByBuilderId(builderId, domainId, pageable);

        for (BuilderItemDTO builderItemDTO : builderItemDTOS.getContent()) {
            if (builderItemDTO.getValueType().equals(BuilderItem.ValueType.MANUAL)) {
                if (Objects.equal(builderItemDTO.getDataType(), BuilderItem.DataType.STRING)) {
                    builderItemDTO.setValue(getStringValue(builderItemDTO.getValue()));
                    builderItemDTO.setName(builderItemDTO.getValue());
                }
            }
        }
        return builderItemDTOS;
    }

    public BuilderItemDTO createBuilderItem(BuilderItemDTO builderItemDTO, Long builderId) {
        Integer domainId = OCSUtils.getDomain();

        Builder builder = builderRepository.findByIdAndDomainId(builderId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        if (builderItemDTO.getValueType() == 3) {
            if (builderItemDTO.getValue() == null|| builderItemDTO.getValue().equals("") ) {
                throw new ResourceNotFoundException(ErrorMessage.BuilderItem.VALUE, Resources.BUILDER_TYPE_ITEM, ErrorKey.BuilderItem.VALUE_ID);
            }
        }
        BuilderTypeItem builderTypeItem = builderTypeItemRepository.findByIdAndDomainId(builderItemDTO.getBuilderTypeItemId(), domainId);
        if (builderTypeItem == null) {
            throw new ResourceNotFoundException(ErrorMessage.BuilderType.ITEM_NOT_FOUND, Resources.BUILDER_TYPE_ITEM, ErrorKey.BuilderType.BUILDER_TYPE_ITEM_ID);
        }
        checkReferTable(builderItemDTO);
        BuilderItem builderItem = builderItemMapper.toEntity(builderItemDTO);
        checkValueMax(builderItem);
        builderItem.setId(null);
        builderItem.setDomainId(domainId);
        builderItem.setBuilderId(builderId);
        checkValueType(builderItemDTO, builderItem);
        if (StringUtils.isEmpty(builderItem.getValue())) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_ITEM, ErrorKey.BuilderItem.VALUE_ID);
        }
        builderItem = builderItemRepository.save(builderItem);
        BuilderItemDTO builderItemDtoRes = builderItemMapper.toDto(builderItem);
        return builderItemDtoRes;
    }

    private void checkValueMax(BuilderItem builderItem) {
        String errorKey = ErrorKey.BuilderItem.VALUE;
        if (builderItem.getValueType() == 3){
            errorKey = ErrorKey.BuilderItem.VALUE_ID;
        }
        if (builderItem.getValueType() == 0 && builderItem.getDataType() == 0) {
            if (builderItem.getValue().length() > 10) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_TOO_BIG,
                    Resources.BUILDER_ITEM, errorKey);
            }
            Long numberValue = Long.valueOf(builderItem.getValue());
            if (numberValue > Integer.MAX_VALUE) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_TOO_BIG,
                    Resources.BUILDER_ITEM, errorKey);
            }
        }
        if (builderItem.getValueType() == 0 && builderItem.getDataType() == 4) {

            BigInteger numberValue = new BigInteger(builderItem.getValue());
            Long max = Long.MAX_VALUE;
            BigInteger maxLong = new BigInteger(max.toString(), 10);
            if (numberValue.compareTo(maxLong) > 0) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_LONG_VALUE_TOO_BIG,
                    Resources.BUILDER_ITEM, errorKey);
            }
        }
        if (builderItem.getValueType() == 0 && builderItem.getDataType() == 2) {
            Double numberValue = Double.valueOf(builderItem.getValue());
            if (numberValue > Float.MAX_VALUE) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_FLOAT_VALUE_TOO_BIG,
                    Resources.BUILDER_ITEM, errorKey);
            }
        }
        if (builderItem.getValueType() == 0 && builderItem.getDataType() == 3) {
            try {
                BigDecimal maxDouble = BigDecimal.valueOf(Double.MAX_VALUE);
                BigDecimal numberValue = new BigDecimal(builderItem.getValue());
                if (numberValue.compareTo(maxDouble) > 0) {
                    throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_DOUBLE_VALUE_TOO_BIG,
                        Resources.BUILDER_ITEM, errorKey);
                }
                if (numberValue.compareTo(BigDecimal.valueOf(0)) < 0) {
                    throw new DataConstrainException(ErrorMessage.NormalizerParam.MUST_BE_GREATER_THAN_0,
                        Resources.BUILDER_ITEM, errorKey);
                }
            } catch (Exception e) {
                throw new DataConstrainException(ErrorMessage.NormalizerParam.PARAMETER_DOUBLE_VALUE_TOO_BIG,
                    Resources.BUILDER_ITEM, errorKey);
            }
        }


    }

    private void checkValueType(BuilderItemDTO builderItemDTO, BuilderItem builderItem) {
        if (builderItem.getValueType().equals(BuilderItem.ValueType.MANUAL)) {
            if (Objects.equal(builderItem.getDataType(), BuilderItem.DataType.STRING)) {
                builderItem.setValue('"' + builderItemDTO.getValue() + '"');
                builderItem.setLinkedId(null);
            } else {
                builderItem.setValue(builderItemDTO.getValue());
                builderItem.setLinkedId(null);
            }
        } else if (builderItem.getValueType().equals(BuilderItem.ValueType.SOURCE_CODE)) {
            builderItem.setValue(builderItemDTO.getValue());
            builderItem.setLinkedId(null);
        } else if (builderItem.getValueType().equals(BuilderItem.ValueType.EXITS_CONFIGURATION)) {
            builderItem.setValue(builderItemDTO.getValue());
            builderItem.setLinkedId(null);
        } else if (builderItem.getValueType().equals(BuilderItem.ValueType.OBJECT)) {
            builderItem.setValue(builderItemDTO.getValue());
            builderItem.setLinkedId(builderItemDTO.getLinkedId());
        }
    }

    private void checkReferTable(BuilderItemDTO builderItemDTO) {
        ReferTable valueType = referTableRepository.findByReferTypeAndValue(ReferType.BuilderValueType, builderItemDTO.getValueType());
        if (valueType == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.RateTable.ID);
        }
        if (builderItemDTO.getValueType() == 0) {
            ReferTable dataType = referTableRepository.findByReferTypeAndValue(ReferType.DataType, builderItemDTO.getDataType());
            if (dataType == null) {
                throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.RATE_TABLE, ErrorKey.RateTable.ID);
            }
        }
    }

    public BuilderItemDTO updateBuilderItem(BuilderItemDTO builderItemDTO, Long builderId) {
        Integer domainId = OCSUtils.getDomain();
        Builder builder = builderRepository.findByIdAndDomainId(builderId, domainId);
        if (builder == null) {
            throw new ResourceNotFoundException(ErrorMessage.Builder.NOT_FOUND, Resources.BUILDER, ErrorKey.ENTITY_NOT_FOUND);
        }
        BuilderItem builderItemCurrent = builderItemRepository.findByIdAndDomainId(builderItemDTO.getId(), domainId);
        if (builderItemCurrent == null) {
            throw new ResourceNotFoundException(ErrorMessage.BuilderItem.NOT_FOUND, Resources.BUILDER_ITEM, ErrorKey.BuilderItem.ID);
        }
        if (builderItemDTO.getValue() == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_ITEM, ErrorKey.BuilderItem.VALUE_ID);
        }
        checkReferTable(builderItemDTO);
        BuilderItem builderItem = builderItemMapper.toEntity(builderItemDTO);
        builderItem.setId(builderItemCurrent.getId());
        builderItem.setDomainId(domainId);
        builderItem.setBuilderId(builderId);
        checkValueType(builderItemDTO, builderItem);
        if (StringUtils.isEmpty(builderItem.getValue())) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_ITEM, ErrorKey.BuilderItem.VALUE_ID);
        }
        BuilderTypeItem builderTypeItem = builderTypeItemRepository.findByIdAndDomainId(builderItemDTO.getBuilderTypeItemId(), domainId);
        if (builderTypeItem == null) {
            throw new ResourceNotFoundException(ErrorMessage.BuilderType.ITEM_NOT_FOUND, Resources.BUILDER_TYPE_ITEM, ErrorKey.BuilderType.BUILDER_TYPE_ITEM_ID);
        }
        checkValueMax(builderItem);

        builderItemRepository.save(builderItem);
        BuilderItemDTO dto = getBuilderItemDetail(builderId, builderItemDTO.getId());
        return dto;
    }

    public void deleteBuilderItem(Long id) {
        Integer domainId = OCSUtils.getDomain();
        BuilderItem builderItem = builderItemRepository.findByIdAndDomainId(id, domainId);
        if (builderItem != null) {
            builderItemRepository.delete(builderItem);
        }
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return builderItemRepository;
    }

    public List<ReferTable> getBuilderItemValueTypes() {
        return referTableRepository.getReferTablesByReferType(ReferType.BuilderValueType, sortByValue());
    }

    public List<ReferTable> getBuilderItemDataTypes() {
        return referTableRepository.getReferTablesByReferType(ReferType.BuilderDataType, sortByValue());
    }

    public List<BuilderItemDTO> getListBuilderItemByBuilderTypeItemId(Long builderTypeItemId) {
        Integer domainId = OCSUtils.getDomain();
        BuilderTypeItem builderTypeItem = builderTypeItemRepository.findByIdAndDomainId(builderTypeItemId, domainId);
        if (builderTypeItem == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.BUILDER_TYPE_ITEM, ErrorKey.BuilderType.ID);
        }
        List<BuilderItemDTO> builderItemDTOS = builderItemRepository.findBuilderItemByBuilderTypeItemId(builderTypeItemId, domainId);
        for (BuilderItemDTO builderItemDTO : builderItemDTOS) {
            if (builderItemDTO.getValueType().equals(BuilderItem.ValueType.MANUAL)) {
                if (builderItemDTO.getDataType().equals(BuilderItem.DataType.STRING)) {
                    builderItemDTO.setValue(getStringValue(builderItemDTO.getValue()));
                    builderItemDTO.setName(builderItemDTO.getValue());
                }
            }
        }
        return builderItemDTOS;
    }
}
