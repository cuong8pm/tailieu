package com.mycompany.myapp.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ZoneMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.ZoneMapDTO;
import com.mycompany.myapp.mapper.ZoneMapMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ZoneMapRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class ZoneMapService extends OCSBaseService {
    @Autowired
    private ZoneMapRepository zoneMapRepository;

    @Autowired
    private ZoneMapMapper zoneMapMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ZoneService zoneService;
    @Autowired
    private CategoryService categoryService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = zoneMapRepository;
        this.parentService = categoryService;
    }


    public Page<ZoneMapDTO> getZoneMaps(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        int level = getCategoryLevel(category.getId()) + 1;

        Page<ZoneMapDTO> zoneMapDTOS = zoneMapRepository.findZoneMaps(
            searchCriteria.getParentId(),
            searchCriteria.getDomainId(),
            searchCriteria.getName(),
            searchCriteria.getDescription(), pageable).map(zoneMapMapper::toDto);
        for (ZoneMapDTO zoneMapDTO : zoneMapDTOS) {
            zoneMapDTO.setCategoryLevel(level);
            zoneMapDTO.setHasChild(checkHasChild(zoneMapDTO.getId(), domainId));
        }
        return zoneMapDTOS;
    }

    public ZoneMapDTO getZoneMapDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        ZoneMap zoneMap = zoneMapRepository.findByIdAndDomainId(id, domainId);
        if (zoneMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.ZoneMap.NOT_FOUND, Resources.ZONE_MAP, ErrorKey.ZoneMap.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(zoneMap.getCategoryId(), domainId);
        ZoneMapDTO zoneMapDTO = zoneMapMapper.toDto(zoneMap);
        zoneMapDTO.setCategoryName(category.getName());
        zoneMapDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        zoneMapDTO.setHasChild(checkHasChild(zoneMapDTO.getId(), domainId));
        return zoneMapDTO;
    }


    public ZoneMapDTO createZoneMap(ZoneMapDTO zoneMapDTO) {
        Integer domainId = OCSUtils.getDomain();

        ZoneMap zoneMap = zoneMapMapper.toEntity(zoneMapDTO);
        checkCategoryExist(zoneMapDTO, domainId);
        checkDuplicateName(zoneMapDTO.getName(), zoneMapDTO.getId());

        zoneMap.setDomainId(domainId);
        super.setPosIndex(domainId, zoneMap, zoneMap.getCategoryId());

        zoneMap = zoneMapRepository.save(zoneMap);

        return zoneMapMapper.toDto(zoneMap);
    }

    // Check category exist or not
    private void checkCategoryExist(ZoneMapDTO zoneMapDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(zoneMapDTO.getCategoryId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void deleteZoneMap(Long id) {
        Integer domainId = OCSUtils.getDomain();
        if (zoneService.getZones(id, null, null, null, null, Pageable.unpaged()).getTotalElements() > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_HAS_CHILD, "", ErrorKey.ID);
        }
        //check Normalizer use Char
        Long countNormUseZoneMap = zoneMapRepository.countNormUseZoneMap(id);
        if (countNormUseZoneMap > 0) {
            throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.ZONE_MAP, "zoneMapId");
        }
        zoneMapRepository.deleteByIdAndDomainId(id, domainId);
    }

    public ZoneMapDTO updateZoneMap(ZoneMapDTO zoneMapDTO) {
        Integer domainId = OCSUtils.getDomain();
        ZoneMap zoneMapDB = zoneMapRepository.findByIdAndDomainIdForUpdate(zoneMapDTO.getId(), domainId);

        if (zoneMapDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.ZoneMap.NOT_FOUND, Resources.ZONE_MAP, ErrorKey.ZoneMap.ID);
        }

        // Lock old category
        checkCategoryExist(zoneMapDTO, domainId);
        checkDuplicateName(zoneMapDTO.getName(), zoneMapDTO.getId());

        // Convert to Entity to save
        ZoneMap zoneMap = zoneMapMapper.toEntity(zoneMapDTO);
        zoneMap.setDomainId(domainId);

        if (!Objects.equal(zoneMapDB.getCategoryId(), zoneMapDTO.getCategoryId())) {
            setPosIndex(domainId, zoneMap, zoneMapDTO.getCategoryId());
        }
        zoneMap = zoneMapRepository.save(zoneMap);

        return zoneMapMapper.toDto(zoneMap);
    }

    public boolean checkHasChild(Long zoneMapId, Integer domainId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, zoneMapId);
        searchCriteria.setDomainId(domainId);
        Pageable pageable = PageRequest.of(0, 1);
        return zoneService.getZones(zoneMapId, null, null, null, null, pageable).getTotalElements() > 0L;
    }

    public void checkZoneMapExist(Long id) {
        Integer domainId = OCSUtils.getDomain();
        ZoneMap zoneMap = zoneMapRepository.findByIdAndDomainId(id, domainId);
        if (zoneMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.ZoneMap.NOT_FOUND, Resources.ZONE_MAP, ErrorKey.ZoneMap.ID);
        }
    }
    public String getNameById(Long zoneMapId){
        checkZoneMapExist(zoneMapId);
        Integer domainId = OCSUtils.getDomain();
        return zoneMapRepository.findNameByIdAndDomain(zoneMapId,domainId);
    }
    
    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.ZONE;
    }

    @Override
    protected void setHasChild(Tree tree) {
        Integer domainId = OCSUtils.getDomain();
        ZoneMapDTO treeClone = (ZoneMapDTO) tree;
        treeClone.setHasChild(checkHasChild(tree.getId(), domainId));
        tree = treeClone;
    }
}
