package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.MapSharebalBal;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ShareType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.AccountBalanceMappingDTO;
import com.mycompany.myapp.dto.BalancesDTO;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.mapper.MapSharebalBalMapper;
import com.mycompany.myapp.repository.MapSharebalBalRepository;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ShareTypeRepository;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class AccountBalanceMappingService extends OCSBaseService {
    private final Logger log = LoggerFactory.getLogger(AccountBalanceMappingService.class);
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private MapSharebalBalRepository mapSharebalBalRepository;
    @Autowired
    private MapSharebalBalMapper mapSharebalBalMapper;
    @Autowired
    private BalancesService balancesService;
    @Autowired
    private ShareTypeRepository shareTypeRepository;
    @Autowired
    private CategoryService categoryService;

    @Override
    @PostConstruct
    public void setBaseRepository() {
        baseRepository = mapSharebalBalRepository;
        parentService = categoryService;
    }

    public Page<AccountBalanceMappingDTO> getMapSharebalBals(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        checkCategoryExist(categoryId, domainId);
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Page<AccountBalanceMappingDTO> mapSharebalBalDTOS = mapSharebalBalRepository.findMapSharebalBals(categoryId, domainId, name, description, pageable).map(mapSharebalBalMapper::toDto);
        for (AccountBalanceMappingDTO accountBalanceMappingDTO : mapSharebalBalDTOS) {
            accountBalanceMappingDTO.setCategoryLevel(parentCategoryLevel);
        }
        return mapSharebalBalDTOS;
    }

    public AccountBalanceMappingDTO getMapSharebalBalDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        MapSharebalBal mapSharebalBal = mapSharebalBalRepository.findByIdAndDomainId(id, domainId);
        if (mapSharebalBal == null) {
            throw new ResourceNotFoundException(ErrorMessage.AccountBalance.NOT_FOUND, Resources.ACCOUNT_BALANCE_MAPPINGS, ErrorKey.MapSharebalBal.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(mapSharebalBal.getCategoryId(), domainId);
        AccountBalanceMappingDTO accountBalanceMappingDTO = mapSharebalBalMapper.toDto(mapSharebalBal);
        accountBalanceMappingDTO.setCategoryName(category.getName());
        accountBalanceMappingDTO.setCategoryLevel(getCategoryLevel(category.getId()));

        BalancesDTO fromBalancesDTO = balancesService.getBalTypeDetail(mapSharebalBal.getBalTypeId());
        accountBalanceMappingDTO.setFromBalance(fromBalancesDTO);
        BalancesDTO toBalancesDTO = balancesService.getBalTypeDetail(mapSharebalBal.getShareBalTypeId());
        accountBalanceMappingDTO.setToBalance(toBalancesDTO);

        return accountBalanceMappingDTO;
    }


    public AccountBalanceMappingDTO saveMapSharebalBal(AccountBalanceMappingDTO accountBalanceMappingDTO, boolean isCreate) {
        Integer domainId = OCSUtils.getDomain();

        validateMapSharebalBal(accountBalanceMappingDTO);
        //check and lock
        checkCategoryExist(accountBalanceMappingDTO.getParentId(), domainId);
        balancesService.checkBalTypeInABMExist(accountBalanceMappingDTO.getFromBalanceId(),accountBalanceMappingDTO.getToBalanceId(), domainId);
        checkDuplicateName(accountBalanceMappingDTO.getName(), accountBalanceMappingDTO.getId());
        checkBalanceAndShareTypeAlreadyExists(accountBalanceMappingDTO, domainId);

        MapSharebalBal mapSharebalBal = mapSharebalBalMapper.toEntity(accountBalanceMappingDTO);
        mapSharebalBal.setDomainId(domainId);
        if (isCreate) {
            setPosIndex(domainId, mapSharebalBal);
        }

        mapSharebalBal = mapSharebalBalRepository.save(mapSharebalBal);

        return mapSharebalBalMapper.toDto(mapSharebalBal);
    }

    public BaseResponseDTO updateMapSharebalBal(AccountBalanceMappingDTO accountBalanceMappingDTO) {
        Integer domainId = OCSUtils.getDomain();
        MapSharebalBal mapSharebalBal = mapSharebalBalRepository.findByIdAndDomainIdForUpdate(accountBalanceMappingDTO.getId(), domainId);
        if (mapSharebalBal == null) {
            throw new ResourceNotFoundException(ErrorMessage.AccountBalance.NOT_FOUND, Resources.ACCOUNT_BALANCE_MAPPINGS, ErrorKey.MapSharebalBal.ID);
        }

        return saveMapSharebalBal(accountBalanceMappingDTO, false);
    }

    private void validateMapSharebalBal(AccountBalanceMappingDTO accountBalanceMappingDTO) {
        if (accountBalanceMappingDTO.getFromBalanceId().equals(accountBalanceMappingDTO.getToBalanceId())) {
            log.error("From BanlanceId và End BalanceId không được trùng nhau!");
            throw new ResourceNotFoundException(ErrorMessage.AccountBalance.A_PAIR_MUST_DIFFERENT, Resources.ACCOUNT_BALANCE_MAPPINGS, ErrorKey.MapSharebalBal.FROM_BALANCE_ID);
        }
    }

    private void checkBalanceAndShareTypeAlreadyExists(AccountBalanceMappingDTO accountBalanceMappingDTO, Integer domainId) {
        List<MapSharebalBal> accountBalanceMappings =
            mapSharebalBalRepository.findByDomainIdAndFromBalanceIdAndMappingType(domainId,
                accountBalanceMappingDTO.getFromBalanceId(), accountBalanceMappingDTO.getMappingTypeId());
        if(!CollectionUtils.isEmpty(accountBalanceMappings)){
            for(MapSharebalBal accountBalanceMapping: accountBalanceMappings){
                if (!Objects.equals(accountBalanceMappingDTO.getId(), accountBalanceMapping.getId())) {
                    log.error("cặp mapping không được trùng");
                    throw new DuplicateException(ErrorMessage.AccountBalance.FROM_BALANCE_MAPPING_TYPE_ALREADY_MAPPING, Resources.ACCOUNT_BALANCE_MAPPINGS, ErrorKey.MapSharebalBal.FROM_BALANCE_ID);
                }
            }
        }
    }

    // Check category exist or not
    private void checkCategoryExist(Long parentId, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(parentId, domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void checkMapSharebalBalExist(Long mapSharebalBalId, Integer domainId) {
        MapSharebalBal mapSharebalBal = mapSharebalBalRepository.findByIdAndDomainIdForUpdate(mapSharebalBalId, domainId);
        if (mapSharebalBal == null) {
            throw new ResourceNotFoundException(ErrorMessage.AccountBalance.NOT_FOUND, Resources.ACCOUNT_BALANCE_MAPPINGS, ErrorKey.MapSharebalBal.ID);
        }
    }

    public Boolean existByBalanceId(Long balanceId, Integer domainId){
       return mapSharebalBalRepository.existsByDomainIdAndBalanceId(domainId,balanceId);
    }
    private void setPosIndex(Integer domainId, MapSharebalBal mapSharebalBal) {
        Integer maxPos = mapSharebalBalRepository.getMaxPosIndex(domainId, mapSharebalBal.getCategoryId());
        if (maxPos == null) {
            mapSharebalBal.setPosIndex(0);
        } else {
            mapSharebalBal.setPosIndex(maxPos + 1);
        }
    }

    public void deleteMapSharebalBal(Long id) {
        Integer domainId = OCSUtils.getDomain();
        checkMapSharebalBalExist(id, domainId);
        mapSharebalBalRepository.deleteByIdAndDomainId(id, domainId);
    }
    public List<ShareType> getDDMappingType(){
        Integer domainId = OCSUtils.getDomain();
        return shareTypeRepository.findByDomainId(domainId);
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }
}
