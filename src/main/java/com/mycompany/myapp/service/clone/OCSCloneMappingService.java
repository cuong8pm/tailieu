package com.mycompany.myapp.service.clone;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.repository.OCSCloneMapRepository;

public interface OCSCloneMappingService {
    void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap);
    void checkParent(List<Tree> dependencies, List<Long> childId);
    void checkChild(List<Tree> dependencies, List<Long> parentId);

    void move(Long parentId, List<? extends Moveable> moveables);
    void checkChildUsed(Long childId);
    void deleteByParentId(Long parentId);
    Integer countChildByParentId(Long parentId);
    List<OCSCloneableMap> getMappingByParent(List<Long> ids);
    Map<Long, List<? extends Tree>> getChildren(List<Long> parentIds);

    OCSCloneMapRepository getOcsCloneMapRepository();

    Map<Long,Integer> countChildrenByParentMap(List<Long> parentIds, int domainId);

}
