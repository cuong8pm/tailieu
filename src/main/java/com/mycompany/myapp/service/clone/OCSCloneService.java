package com.mycompany.myapp.service.clone;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface OCSCloneService {

    List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap);
    void getParentDependencies(List<Tree> dependencies, List<Long> ids, boolean hasParentDependServices, CategoryType categoryType);
    void getChildDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices, CategoryType categoryType);
    Map<String, Set<CategoryDTO>> checkDependencies(Long id);

    Map<Long, ? extends Tree> getCloneableTree(List<Long> ids);
    void moveCategory(Long id, Long categoryId);

    BaseResponseDTO move(List<? extends Moveable> moveables);
    void clearCache();
    void deleteEntity(Long id);
//    OCSCloneableEntity cloneEntity(List<Long> )
}
