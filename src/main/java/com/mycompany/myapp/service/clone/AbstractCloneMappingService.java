package com.mycompany.myapp.service.clone;

import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.RatingFilterRateTableMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.ratetable.RateTableColumn;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.service.formula.FormulaService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

public abstract class AbstractCloneMappingService extends OCSBaseService implements OCSCloneMappingService {

    protected OCSCloneService childService;

    protected OCSCloneService parentDependService;

    protected OCSCloneMapRepository ocsCloneMapRepository;

    @Autowired
    protected EntityManager entityManager;
    // truyền vào: list id cha mới, list cloneDTO cần clone(chứa id cũ, id cha mới), map gồm id cha mới - list cloneDTO cha, map id cha mới - list id con cũ
    public void cloneMappings(List<Long> newParentIds, Collection<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewIdParentMap, Map<Long, List<Long>> parentNewChildrenOldIdMap) {
        if (ocsCloneMapRepository == null || childService == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }

        Integer domainId = OCSUtils.getDomain();
        // lấy list id cha cũ để lấy list data của bảng map
        List<Long> oldParentIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
        // lấy toàn bộ thông tin bảng map có chứa tất cả id của tầng cha
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, oldParentIds);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<CloneDTO> idsToCreateNew = cloneDTOs.stream().filter(cloneDTO -> cloneDTO.getId() != null).collect(Collectors.toList());
        // clone tầng tiếp theo
        childService.cloneEntities(idsToCreateNew, oldNewChildMap);

        long batchSize = 30;
        long count = 0;
        // tạo mapping giữa id cha mới - id cha cũ
        Map<Long, Long> newOldParentIdMap = new HashMap<>();

        // thêm vào map
        for (Long newId : oldNewIdParentMap.keySet()) {
                newOldParentIdMap.put(newId, oldNewIdParentMap.get(newId).getId());
        }

        List<OCSCloneableMap> newMap = new ArrayList<>();
        for (Long newParentId : newParentIds) {
            Long oldParentId = newOldParentIdMap.get(newParentId);
            List<OCSCloneableMap> oldMappingParents = mappingParents.stream().filter(i -> Objects.equals(i.getParentId(), oldParentId)).collect(Collectors.toList());
            for (OCSCloneableMap oldMappingParent : oldMappingParents) {
                List<Long> childrenIds = parentNewChildrenOldIdMap.get(newParentId);
                Long childId = getChildId(oldMappingParent,childrenIds,oldNewChildMap, newParentId);
                OCSCloneableMap newCloneMap = SerializationUtils.clone(oldMappingParent);
                newCloneMap.setChildMappingId(childId);
                newCloneMap.clearId();
                newCloneMap.setParentMappingId(newParentId);
                newMap.add(newCloneMap);

                entityManager.persist(newCloneMap);
                if (count % batchSize == 0 && count > 0) {
                    entityManager.flush();
                    entityManager.clear();
                    newMap.clear();
                }
                count++;
            }

        }

        if (!CollectionUtils.isEmpty(newMap)) {
            entityManager.flush();
            entityManager.clear();
            newMap.clear();
        }

    }

    public Long getChildId(OCSCloneableMap mappingParent, List<Long> childrenIds,Map<Long, CloneDTO> oldNewChildMap, Long newParentId){
        Long childId = mappingParent.getChildId();
        Boolean isCloneNew = childrenIds.contains(childId);
        // if this child is create new

        Long finalChildId = childId;
        Optional<Long> newChildId = oldNewChildMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue().getId(), finalChildId)
                            && Objects.equals(newParentId, entry.getValue().getParentId()))
            .map(Map.Entry::getKey)
            .findAny();

        if (newChildId.isPresent() && isCloneNew) {
            return newChildId.get();
        }
        return childId;
    }

    public void checkParent(List<Tree> dependencies, List<Long> childId) {
        if (ocsCloneMapRepository == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndChildIdIn(domainId, childId);
        List<Long> parentIds = mappingParents.stream().map(OCSCloneableMap::getParentId).collect(Collectors.toList());
        if (parentDependService != null) {
            parentDependService.getParentDependencies(dependencies, parentIds, false, getCategoryType());
        }
    }

    public void checkChild(List<Tree> dependencies, List<Long> parentId) {
        if (ocsCloneMapRepository == null) {
            throw new UnsupportedOperationException("this operation is not ready");
        }
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableMap> mappingParents = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, parentId);
        List<Long> childIds = mappingParents.stream().map(OCSCloneableMap::getChildId).collect(Collectors.toList());
        if (childService != null) {
            childService.getChildDependencies(dependencies, childIds, false, null);
        }
    }

    @Override
    public Map<Long, List<? extends Tree>> getChildren(List<Long> parentIds) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableMap> cloneableMaps = ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, parentIds);
        Map<Long, List<OCSCloneableMap>> parentChildrenMap = cloneableMaps.stream().collect(Collectors.groupingBy(OCSCloneableMap::getParentId));
        Map<Long, List<? extends Tree>> finalResults = new HashMap();

        List<Long> childIds = cloneableMaps.stream().map(OCSCloneableMap::getChildId).distinct().collect(Collectors.toList());
        Map<Long, ? extends Tree> childrenMap = new HashMap();
        if(!(childService instanceof FormulaService)) {
            childrenMap = childService.getCloneableTree(childIds);
        }

        for (Long parentId : parentChildrenMap.keySet()) {
            List<OCSCloneableMap> childMaps = parentChildrenMap.get(parentId);
            Collections.sort(childMaps, (o1, o2) -> o1.getPosIndex().compareTo(o2.getPosIndex()));
            List<Tree> branches = new ArrayList<>();
            for (OCSCloneableMap childMap : childMaps) {
                if(childrenMap.get(childMap.getChildId()) != null) {
                    branches.add(childrenMap.get(childMap.getChildId()));
                }
            }
            finalResults.put(parentId, branches);
        }
        return finalResults;
    }

    public void move(Long parnetId, List<? extends Moveable> moveables) {
        Integer domainId = OCSUtils.getDomain();

        OCSCloneableMap firstEntity = ocsCloneMapRepository.findByDomainIdAndChildIdAndParentId(domainId,
                moveables.get(0).getId(), parnetId);
        checkNull(firstEntity);

        OCSCloneableMap secondEntity = ocsCloneMapRepository.findByDomainIdAndChildIdAndParentId(domainId,
                moveables.get(1).getId(), parnetId);
        checkNull(secondEntity);

        // Check pos index đã bị xê dịch hay chua, đề phòng 2 người cùng xê dich cùng 1
        // lúc
        if (!Objects.equals(moveables.get(0).getPosIndex(), firstEntity.getPosIndex())) {
            throw new ResourceConflictException(ErrorMessage.POS_INDEX_INCORRECT, getResource(),
                    ErrorKey.Category.POS_INDEX);
        }
        if (!Objects.equals(moveables.get(1).getPosIndex(), secondEntity.getPosIndex())) {
            throw new ResourceConflictException(ErrorMessage.POS_INDEX_INCORRECT, getResource(),
                    ErrorKey.Category.POS_INDEX);
        }

        Integer tempPosIndex = firstEntity.getPosIndex();
        firstEntity.setPosIndex(secondEntity.getPosIndex());
        secondEntity.setPosIndex(tempPosIndex);

        ocsCloneMapRepository.save(firstEntity);
        ocsCloneMapRepository.save(secondEntity);

    }

    public void deleteMapping(Long parentId, Long childId) {
        Integer domainId = OCSUtils.getDomain();
        OCSCloneableMap ocsCloneableMap = ocsCloneMapRepository.findByDomainIdAndChildIdAndParentId(domainId, childId, parentId);
        if (ocsCloneableMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.RateTable.NOT_FOUND, getResource(), ErrorKey.RateTable.ID);
        }

        Integer countByParentId = ocsCloneMapRepository.countByDomainIdAndParentId(domainId, parentId);

        if (countByParentId.intValue() <= 1) {
            throw new DataConstrainException(ErrorMessage.MUST_HAVE_ATLEAST_ONE_CHILD, getResource(), ErrorKey.ID);
        }

        ocsCloneMapRepository.deleteByDomainIdAndChildIdAndParentId(domainId, childId, parentId);
    }

    private void checkNull(OCSCloneableMap entity) {
        if (entity == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY,
                    ErrorKey.Category.ID);
        }
    }

    @Override
    public Integer countChildByParentId(Long parentId) {
        Integer domainId = OCSUtils.getDomain();
        return ocsCloneMapRepository.countByDomainIdAndParentId(domainId, parentId);
    }

    protected abstract String getResource();

    public void checkChildUsed(Long childId) {
        Integer domainId = OCSUtils.getDomain();
        Integer check = ocsCloneMapRepository.countByDomainIdAndChildId(domainId, childId);
        if (check > 0) {
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, getResource(), ErrorKey.ID);
        }
    }

    public void deleteByParentId(Long parentId) {
        Integer domainId = OCSUtils.getDomain();
        ocsCloneMapRepository.deleteByDomainIdAndParentId(domainId, parentId);
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }

    @Override
    public List<OCSCloneableMap> getMappingByParent(List<Long> ids) {
        Integer domainId = OCSUtils.getDomain();
        return ocsCloneMapRepository.findByDomainIdAndParentIdIn(domainId, ids);
    }

    public Map<Long,Integer> countChildrenByParentMap(List<Long> parentIds, int domainId){
        List<CountChildrenByParentDTO> countChildren = ocsCloneMapRepository.countChildrenByParentIdInAndDomainId(parentIds,domainId);
        Map<Long,Integer> childrenMap = countChildren.stream().collect(Collectors.toMap(CountChildrenByParentDTO::getId,CountChildrenByParentDTO::getCountChild));
        return  childrenMap;
    };
}
