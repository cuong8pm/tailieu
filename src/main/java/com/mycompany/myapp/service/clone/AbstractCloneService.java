package com.mycompany.myapp.service.clone;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.EventPoliciesEntity;
import com.mycompany.myapp.service.exception.*;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.mapper.CategoryMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.utils.OCSUtils;

public abstract class AbstractCloneService extends OCSBaseService implements OCSCloneService {

    protected List<? extends OCSCloneMappingService> cloneMappingServices = new ArrayList<>();

    protected List<? extends OCSCloneService> childDependServices = new ArrayList<>();

    @Autowired
    protected CategoryRepository categoryRepository;

    @Autowired
    protected CategoryMapper categoryMapper;

    protected List<? extends OCSCloneService> parentDependServices = new ArrayList<>();

    protected List<? extends OCSCloneMappingService> parentDependMappingServices = new ArrayList<>();

    protected static ThreadLocal<Map<String, OCSCloneableEntity>> cacheLocal = new ThreadLocal<>();

    // protected OCSCloneMappingService childDependService;

    @Override
    public void setBaseRepository() {

    }

    @Override
    protected Integer getCategoryLevel(Long categoryId) {
        return super.getCategoryLevel(categoryId);
    }

    public OCSCloneableEntity saveEntity(CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> baseToCloneMap) {
        if (ocsCloneableRepository == null) {
            throw new UnsupportedOperationException("Method is not ready");
        }
        if (cloneDTO.getChildren() == null) {
            cloneDTO.setChildren(new ArrayList<CloneDTO>());
        }
        Integer domainId = OCSUtils.getDomain();
        String name = cloneDTO.getName();
        Long id = cloneDTO.getId();

        OCSCloneableEntity baseToClone = baseToCloneMap.get(id);

        if (baseToClone == null) {
            return null;
        }

        checkDuplicateName(name, id);
        OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
        String newName = "";
        if (StringUtils.isEmpty(name) || name.equals(clonedEntity.getName())) {
            newName = cloneName(baseToClone.getName(), domainId);
        } else {
            newName = name;
        }
        if(baseToClone.getName().equalsIgnoreCase(newName)){
            throwDuplicatedNameException();
        }

        if (getCloneNameMaxLength() != null && newName.length() > getCloneNameMaxLength().intValue()) {
            throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
        }

        setPosIndex(domainId, clonedEntity, clonedEntity.getParentId());

        clonedEntity = setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);
        clonedEntity = ocsCloneableRepository.saveAndFlush(clonedEntity);
        return clonedEntity;
    }

    @Transactional
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        //list id lấy ra từ danh sách entity cần clone, có chứ giá trị giống nhau
        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        // list id mới để truyền xuống clone tầng dưới
        List<Long> newIds = new ArrayList<>();
        //tìm toàn bộ entity cần clone trong database, không chứa giá trị giống nhau
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);
        //kiểm tra xem các id cần clone truyền vào có tồn tại trong db hay ko
        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if(!Objects.equals(distinctIds.size(), baseToClones.size())){
            throwResourceNotFoundException();
        };
        // tạo mapping giữa id - entity cần clone
        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
                .collect(Collectors.toMap(OCSCloneableEntity::getId, Function.identity()));
        // list những entity đã được clone
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        // list những entity con sẽ được clone ở lớp tầng dưới
        List<CloneDTO> lstCloneDTos = new ArrayList<>();
        // mapping giữa id mới - cloneDTO gồm id cũ và parentId mới
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        // mapping giữa id cha mới - list id con sẽ được clone mới
        Map<Long, List<Long>> parentChildIdMap = new HashMap<>();
        for (CloneDTO cloneDTO : cloneDTOs) {
            // clone entity mới
            OCSCloneableEntity clonedEntity = saveEntity(cloneDTO, baseToCloneMap);
            if(!ObjectUtils.isEmpty(clonedEntity)) {
                // thêm mapping giữa id mới - cloneDTO gồm id cũ và parentId mới vào map
                oldNewIdMap.put(clonedEntity.getId(),cloneDTO);
                // thêm entity đc tạo mới vào list
                clonedEntities.add(clonedEntity);
                // thêm id mới vào list
                newIds.add(clonedEntity.getId());
            }
            // set parentId của các entity con là Id của entity mới tạo
            cloneDTO.getChildren().forEach(cloneDTO1 -> cloneDTO1.setParentId(clonedEntity.getId()));
            // add toàn bộ entity con của entity hiện tại vào list entity con sẽ được clone ở tầng dưới
            lstCloneDTos.addAll(cloneDTO.getChildren());
            List<Long> childrenIds = new ArrayList<>();
            // lấy toàn bộ id cũ của các entity con
            if (cloneDTO.getChildren() != null){
                childrenIds = cloneDTO.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }
            // tạo mapping id mới - list id của entity con cũ
            parentChildIdMap.put(clonedEntity.getId(),childrenIds);
        }
        oldNewChildMap.putAll(oldNewIdMap);
        // clone mapping tầng dưới
        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            cloneMappingService.cloneMappings(newIds, lstCloneDTos, oldNewIdMap, parentChildIdMap);
        }
       // clone tầng dưới trực tiếp
       return cloneListChildEntities(newIds, domainId, clonedEntities, oldNewIdMap);

    }

    protected OCSCloneableEntity setPropertiesOfCloneEntity(OCSCloneableEntity clonedEntity, CloneDTO cloneDTO,
                                                            String newName) {
        clonedEntity.setName(newName);
        clonedEntity.setId(null);
        if(clonedEntity instanceof EventPoliciesEntity){
            if(cloneDTO.getDescription() != null && !Objects.equals(((EventPoliciesEntity) clonedEntity).getDescription(),cloneDTO.getDescription())){
                ((EventPoliciesEntity) clonedEntity).setDescription(cloneDTO.getDescription());
            }
        }
        return clonedEntity;
    }

    public void throwResourceNotFoundException(){
        throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND,getResourceName(),ErrorKey.ID);
    }

    public void getParentDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices,
                                      CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();

        List<OCSMoveableEntity> dependDBs = getParentEntity(ids, domainId, categoryType);

        List<Tree> depends = new ArrayList<>();

        for (OCSMoveableEntity ocsMoveableEntity : dependDBs) {
            TreeClone tree = getTreeDTO();
            BeanUtils.copyProperties(ocsMoveableEntity, tree);
            depends.add(tree);
        }
        CategoryDTO categoryDTO = getCategory(getCategoryType());
        if (categoryDTO == null) {
            return;
        }
        categoryDTO.setTemplates(depends);
        categoryDTO.setHasChild(depends.size() > 0);
        categoryDTO.setName(getNameCategory());
        dependencies.add(0, categoryDTO);

        for (OCSCloneService parentDependService : parentDependServices) {
            if (parentDependService != null) {
                ids = getListDependId(dependDBs);
                parentDependService.getParentDependencies(dependencies, ids, true, getCategoryType());
            }
        }

        for (OCSCloneMappingService parentDependService : parentDependMappingServices) {
            if (hasChildDependServices) {
                ids = getListDependId(dependDBs);
            }
            if (parentDependService != null) {
                parentDependService.checkParent(dependencies, ids);
            }
        }

    }

    protected List<Long> getListDependId(List<OCSMoveableEntity> dependDbs){
        return dependDbs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
    }

    protected List getParentEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        return baseRepository.findByIdInAndDomainId(ids, domainId);
    }

    @Override
    public void checkDuplicateName(String name, Long id) {
        super.checkDuplicateName(name, id);
    }

    public void getChildDependencies(List<Tree> dependencies, List<Long> ids, boolean hasChildDependServices,
                                     CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();

        List<OCSMoveableEntity> dependDBs = getChildEntity(ids, domainId, categoryType);

        List<Tree> depends = new ArrayList<>();

        for (OCSMoveableEntity ocsMoveableEntity : dependDBs) {
            Tree tree = getTreeDTO();
            BeanUtils.copyProperties(ocsMoveableEntity, tree);
            depends.add(tree);
        }
        CategoryDTO categoryDTO = getCategory(getCategoryType());
        if (categoryDTO == null) {
            return;
        }
        categoryDTO.setTemplates(depends);
        categoryDTO.setName(getNameCategory());
        dependencies.add(categoryDTO);

        for (OCSCloneService childService : childDependServices) {
            if (childService != null) {
                ids = dependDBs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
                childService.getChildDependencies(dependencies, ids, true, getCategoryType());
            }
        }

        for (OCSCloneMappingService childService : cloneMappingServices) {
            if (hasChildDependServices) {
                ids = dependDBs.stream().map(OCSBaseEntity::getId).collect(Collectors.toList());
            }
            if (childService != null) {
                childService.checkChild(dependencies, ids);
            }
        }
    }

    protected List getChildEntity(List<Long> ids, Integer domainId, CategoryType categoryType) {
        return baseRepository.findByIdInAndDomainId(ids, domainId);
    }

    public Map<String, Set<CategoryDTO>> checkDependencies(Long id) {
        // List<Tree> results = new ArrayList<>();
        List<Tree> parents = new ArrayList<>();

        for (OCSCloneService parentDependService : parentDependServices) {

            if (parentDependService != null) {
                parentDependService.getParentDependencies(parents, Arrays.asList(id), true, getCategoryType());
            }
        }

        for (OCSCloneMappingService parentDependService : parentDependMappingServices) {
            if (parentDependService != null) {
                parentDependService.checkParent(parents, Arrays.asList(id));
            }
        }
        List<Tree> children = new ArrayList<>();
        for (OCSCloneMappingService childService : cloneMappingServices) {
            childService.checkChild(children, Arrays.asList(id));
        }

        for (OCSCloneService childService : childDependServices) {
            if (childService != null) {
                childService.getChildDependencies(children, Arrays.asList(id), true, getCategoryType());
            }
        }

        // List<Tree> depend = results.stream().filter(tree ->
        // dependTypes().contains(((CategoryDTO)tree).getCategoryType())).collect(Collectors.toList());
        // List<Tree> dependOn = results.stream().filter(tree ->
        // dependOnTypes().contains(((CategoryDTO)tree).getCategoryType())).collect(Collectors.toList());

        Map<String, Set<CategoryDTO>> dependMaps = new HashMap<>();
        Set<CategoryDTO> setParents = getParentTrees(parents);

        Set<CategoryDTO> setChilds = getChildTrees(children);
        dependMaps.put("depend", setParents);
        dependMaps.put("dependOn", setChilds);
        return dependMaps;

    }

    private Set<CategoryDTO> getParentTrees(List<Tree> parents) {
        AtomicReference<Integer> index = new AtomicReference<>(0);
        Set<CategoryDTO> setParents = new TreeSet<>(new Comparator<CategoryDTO>() {
            @Override
            public int compare(CategoryDTO o1, CategoryDTO o2) {
                return o2.getCategoryType().getValue() - o1.getCategoryType().getValue();
            }
        });
        parents.forEach(i -> {
            Optional<CategoryDTO> tree = setParents.stream().filter(s -> s.getId().equals(i.getId())).findFirst();
            if (tree.isPresent()) {
                tree.get().getTemplates().addAll(i.getTemplates().stream()
                    .filter(t -> !tree.get().getTemplates().contains(t)).collect(Collectors.toList()));
            } else {
                i.setPosIndex(index.getAndSet(index.get() + 1));
                setParents.add((CategoryDTO) i);

            }
            i.setTemplates(i.getTemplates().stream().distinct().collect(Collectors.toList()));
        });
        return setParents;
    }

    private Set<CategoryDTO> getChildTrees(List<Tree> childs) {
        AtomicReference<Integer> index = new AtomicReference<>(0);
        Set<CategoryDTO> setChilds = new TreeSet<>(new Comparator<CategoryDTO>() {
            @Override
            public int compare(CategoryDTO o1, CategoryDTO o2) {
                return o2.getCategoryType().getValue() - o1.getCategoryType().getValue();
            }
        });
        childs.forEach(i -> {
            Optional<CategoryDTO> tree = setChilds.stream().filter(s -> s.getId().equals(i.getId())).findFirst();
            if (tree.isPresent()) {
                tree.get().getTemplates().addAll(i.getTemplates().stream()
                    .filter(t -> !tree.get().getTemplates().contains(t)).collect(Collectors.toList()));
            } else {
                i.setPosIndex(index.getAndSet(index.get() + 1));
                setChilds.add((CategoryDTO) i);

            }
        });

        return setChilds;
    }

    protected CategoryDTO getCategory(CategoryType categoryType) {
        Integer domainId = OCSUtils.getDomain();
        List<Category> categories = categoryRepository.findRootCategory(Arrays.asList(categoryType), domainId);
        CategoryDTO result = categoryMapper.toDto(categories.get(0));
        result.setTreeType("category");
        return result;
    }

    public Map<Long, ? extends Tree> getCloneableTree(List<Long> ids) {
        // Get base
        Integer domainId = OCSUtils.getDomain();
        List<OCSMoveableEntity> baseEntities = baseRepository.findByDomainIdAndIdIn(domainId, ids);

        if (CollectionUtils.isEmpty(baseEntities)) {
            if(!CollectionUtils.isEmpty(ids)){
                throwResourceNotFoundException();
            }
            return null;
        }

        Map<Long, TreeClone> resultMap = new HashMap<Long, TreeClone>();

        for (OCSMoveableEntity baseEntity : baseEntities) {
            TreeClone result = getTreeDTO();
            if (result != null) {
                result.setTemplates(new ArrayList<Tree>());
                BeanUtils.copyProperties(baseEntity, result);
                if (result.getTemplates() == null) {
                    result.setTemplates(new ArrayList<Tree>());
                }
                resultMap.put(result.getId(), result);
            }
        }
        getChildTemplate(ids, resultMap);
        return resultMap;
    }

    public void getChildTemplate(List<Long> ids, Map<Long, TreeClone> resultMap) {
        for (OCSCloneMappingService ocsCloneMappingService : cloneMappingServices) {
            Map<Long, List<? extends Tree>> templates = ocsCloneMappingService.getChildren(ids);
            for (Long entityId : resultMap.keySet()) {
                TreeClone result = resultMap.get(entityId);
                List<? extends Tree> children = templates.get(result.getId());
                if (children != null) {
                    result.getTemplates().addAll(children);
                }
                result.setHasChild(!result.getTemplates().isEmpty());
            }
        }
    }

    public void checkCategory(Long categoryId) {
        Integer domainId = OCSUtils.getDomain();
        CategoryType categoryType = getCategoryType();
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, "", ErrorKey.CATEGORY_NAME);
        }
        if (category.getCategoryType() != categoryType) {
            throw new DataConstrainException(ErrorMessage.CATEGORY_TYPE, "", ErrorKey.Category.CATEGORY_TYPE);
        }
    }

    protected abstract List<CategoryType> dependTypes();

    protected abstract List<CategoryType> dependOnTypes();

    protected abstract TreeClone getTreeDTO();

    protected abstract CategoryType getCategoryType();

    protected String getCacheKey(Long id) {
        return getCategoryType() != null ? getCategoryType().toString() + id : "formula" + id;
    }

    public void clearCache() {
//        cacheLocal.get().clear();
    }

    public void deleteEntity(Long id) {
        Integer domainId = OCSUtils.getDomain();
        for (OCSCloneMappingService parentDependService : parentDependMappingServices) {
            parentDependService.checkChildUsed(id);
        }
        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            cloneMappingService.deleteByParentId(id);
        }
        ocsCloneableRepository.deleteByIdAndDomainId(id, domainId);
    }

    protected abstract Integer getCloneNameMaxLength();

    @Override
    protected void setHasChild(Tree tree) {
        TreeClone treeClone = null;
        if (tree instanceof TreeClone) {
            treeClone = (TreeClone) tree;
        }
        for (OCSCloneMappingService ocsCloneMappingService : cloneMappingServices) {
            Integer countChildren = ocsCloneMappingService.countChildByParentId(tree.getId());
            if (treeClone != null && countChildren.intValue() > 0) {
                treeClone.setHasChild(true);
            }
        }
    }

    @Override
    protected List<OCSCloneableMap> getMappingFoundEntities(List<Long> ids) {
        List<OCSCloneableMap> mapping = new ArrayList();
        for (OCSCloneMappingService ocsCloneMappingService : cloneMappingServices) {
            mapping.addAll(ocsCloneMappingService.getMappingByParent(ids));
        }
        return mapping;
    }

    protected abstract String getNameCategory();

    protected boolean checkHasChildOfChildren(Map<Long,Integer> parentCountChildMap,Long parentId){
       return (parentCountChildMap.get(parentId) != null && parentCountChildMap.get(parentId) != 0);
    }

    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.DUPLICATED_NAME, "", ErrorKey.NAME);
    }

}
