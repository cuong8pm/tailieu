package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.repository.formula.FormulaRepository;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.Parameter;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ParameterDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.mapper.Parameter2Mapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ParameterRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class ParameterService extends OCSBaseService {

    private String SCREEN = "parameter";

    @Autowired
    private FormulaRepository formulaRepository;

    @Autowired
    private BlockRepository blockRepository;

    @Autowired
    private ParameterRepository parameterRepository;

    @Autowired
    private Parameter2Mapper parameter2Mapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ReferTableRepository referTableRepository;
    @Autowired
    private CategoryService categoryService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = parameterRepository;
        this.parentService = categoryService;

    }
    public ParameterDTO createParameter(ParameterDTO parameterDTO) {
        Integer domainId = OCSUtils.getDomain();

        Parameter parameter = parameter2Mapper.toEntity(parameterDTO);
        checkCategoryExist(parameterDTO, domainId);
        checkDuplicateName(parameterDTO.getName(), parameterDTO.getId());

        parameter.setDomainId(domainId);
        super.setPosIndex(domainId, parameter, parameter.getCategoryId());

        parameter = parameterRepository.save(parameter);

        return parameter2Mapper.toDto(parameter);
    }

    public void deleteParameter(Long id) {
        Integer domainId = OCSUtils.getDomain();
        if (!CollectionUtils.isEmpty(blockRepository.findBlockByParameterId(id))){
            throw new DataConstrainException(ErrorMessage.Parameter.CANNOT_DELETE, Resources.PARAMETER, ErrorKey.Parameter.ID);
        }
        if (formulaRepository.countFormulaUseChar(id) >0 ){
            throw new DataConstrainException(ErrorMessage.Parameter.CANNOT_DELETE, Resources.PARAMETER, ErrorKey.Parameter.ID);
        }
        Long normUseParameter = parameterRepository.countNormUseChar(id);
        if (normUseParameter >0){
            throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, SCREEN,
                "parameterId");
        }
        parameterRepository.deleteByIdAndDomainId(id, domainId);
    }

    public ParameterDTO updateParameter(ParameterDTO parameterDTO) {
        Integer domainId = OCSUtils.getDomain();
        Parameter parameterDB = parameterRepository.findByIdAndDomainIdForUpdate(parameterDTO.getId(), domainId);

        if (parameterDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.Parameter.NOT_FOUND, Resources.PARAMETER, ErrorKey.Parameter.ID);
        }

        // Lock old category
        checkCategoryExist(parameterDTO, domainId);
        checkDuplicateName(parameterDTO.getName(), parameterDTO.getId());

        Parameter parameter = parameter2Mapper.toEntity(parameterDTO);
        parameter.setDomainId(domainId);

        if (!Objects.equal(parameterDTO.getCategoryId(), parameterDB.getCategoryId())) {
            super.setPosIndex(domainId, parameter, parameterDTO.getCategoryId());
        }

        parameter = parameterRepository.save(parameter);

        // Convert to Entity to save
        return parameter2Mapper.toDto(parameter);
    }

    public Page<ParameterDTO> getParameters(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;
        Page<ParameterDTO> parameters = parameterRepository.findParameters(searchCriteria.getParentId(),
            searchCriteria.getDomainId(), searchCriteria.getName(), searchCriteria.getDescription(), pageable)
            .map(parameter2Mapper::toDto);

        for (ParameterDTO parameter : parameters) {
            parameter.setCategoryLevel(parentCategoryLevel);
        }

        return parameters;
    }

    public ParameterDTO getParameterDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Parameter parameter = parameterRepository.findByIdAndDomainId(id, domainId);
        if (parameter == null) {
            throw new ResourceNotFoundException(ErrorMessage.Parameter.NOT_FOUND, Resources.PARAMETER, ErrorKey.Parameter.ID);
        }
        ReferTable ownerLevel = referTableRepository.getReferTableByReferTypeAndValue(ReferType.OwnerLevel,
            parameter.getOwnerLevel());
        if (ownerLevel == null) {
            throw new ResourceNotFoundException(ErrorMessage.Parameter.OWNER_LEVEL_NOT_FOUND, Resources.PARAMETER, ErrorKey.Parameter.OWNER_LEVEL);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(parameter.getCategoryId(), domainId);
        ParameterDTO parameterDTO = parameter2Mapper.toDto(parameter);
        parameterDTO.setCategoryName(category.getName());
        parameterDTO.setOwnerLevelName(ownerLevel.getName());
        parameterDTO.setCategoryLevel(getCategoryLevel(parameter.getCategoryId()) + 1);
        return parameterDTO;
    }

    // Check category exist or not
    private void checkCategoryExist(ParameterDTO parameterDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(parameterDTO.getCategoryId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public BaseResponseDTO cloneParameter(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Parameter parameterToClone = parameterRepository.findByIdAndDomainId(id, domainId);

        if (parameterToClone == null) {
            throw new ResourceNotFoundException(ErrorMessage.Parameter.NOT_FOUND, Resources.PARAMETER, ErrorKey.Parameter.ID);
        }

        Parameter newParameter = new Parameter();
        newParameter.setName(parameterToClone.getName());
        newParameter.setDomainId(parameterToClone.getDomainId());
        newParameter.setPosIndex(parameterToClone.getPosIndex());
        newParameter.setCategoryId(parameterToClone.getCategoryId());
        newParameter.setForTemplate(parameterToClone.getForTemplate());
        newParameter.setOwnerLevel(parameterToClone.getOwnerLevel());
        newParameter.setParameterValue(parameterToClone.getParameterValue());
        newParameter.setRemark(parameterToClone.getRemark());
        super.setPosIndex(domainId, newParameter, newParameter.getCategoryId());

        //Check xem co phai clone truoc khong
        List<Parameter> list = parameterRepository.findParametersByName(newParameter.getName() + "_clone", domainId);
        boolean hasBeenCloned = !list.isEmpty();
        String name = newParameter.getName();
        String newName = "";
        if (!hasBeenCloned) {
            // Chua duoc clone bao gio
            newName = name + "_clone";
        } else {
            // Da duoc Clone
            // => di tim xem ten clone to nhat la bao nhieu
            int currentCloned = countCloned(name);
            List<Parameter> listToCount = new ArrayList<>();
            for (Parameter parameter : list) {
                if (countCloned(parameter.getName()) <= currentCloned + 1) {
                    listToCount.add(parameter);
                }
            }
            if (listToCount.size() == 0) {
                newName = name + "_clone";
            } else {
                List<String> listNameClone = listToCount.stream().filter(i -> !i.getName().equals(name + "_clone")).map(i -> i.getName()).collect(Collectors.toList());
                if (listNameClone.size() == 0) {
                    newName = name + "_clone(1)";
                } else {
                    List<Integer> listIndexOfName = listNameClone.stream().map(i -> {
                        int beginIndex = i.lastIndexOf("(") + 1;
                        try {
                            return Integer.parseInt(i.substring(beginIndex, i.length() - 1));
                        } catch (NumberFormatException e) {
                            return 0;
                        }
                    }).sorted((o1, o2) -> o1 - o2).collect(Collectors.toList());
                    newName = name + "_clone(" + (listIndexOfName.get(listIndexOfName.size() - 1) + 1) + ")";
                }
            }
        }
        newParameter.setName(newName);
        if (newParameter.getName().length() > Parameter.Length.NAME) {
            throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, Resources.PARAMETER, ErrorKey.NAME,
                    id, parameterToClone.getName());
        }

        newParameter = parameterRepository.save(newParameter);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(newParameter, response);

        return response;

    }


    public List<ReferTable> getOwnerLevels() {
        return referTableRepository.
            getReferTablesByReferType(ReferType.OwnerLevel, sortByValue());

    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.PARAMETER;
    }
}
