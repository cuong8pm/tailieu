package com.mycompany.myapp.service.characteristic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.characteristic.Type;
import com.mycompany.myapp.dto.characteristic.TypeDTO;
import com.mycompany.myapp.mapper.characteristic.TypeMapper;
import com.mycompany.myapp.repository.characteristic.TypeRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Type}.
 */
@Service
@Transactional
public class TypeService {

    private final Logger log = LoggerFactory.getLogger(TypeService.class);

    private final TypeRepository charSpecTypeRepository;

    private final TypeMapper charSpecTypeMapper;

    public TypeService(TypeRepository charSpecTypeRepository,
            TypeMapper charSpecTypeMapper) {
        this.charSpecTypeRepository = charSpecTypeRepository;
        this.charSpecTypeMapper = charSpecTypeMapper;
    }

    public TypeDTO save(TypeDTO charSpecTypeDTO) {
        log.debug("Request to save CharSpecType : {}", charSpecTypeDTO);
        Type charSpecType = charSpecTypeMapper.toEntity(charSpecTypeDTO);
        charSpecType = charSpecTypeRepository.save(charSpecType);
        return charSpecTypeMapper.toDto(charSpecType);
    }

    @Transactional(readOnly = true)
    public List<TypeDTO> findAll() {
        log.debug("Request to get all CharSpecTypes");
        return charSpecTypeRepository.findAll().stream().map(charSpecTypeMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public Optional<TypeDTO> findOne(Long id) {
        log.debug("Request to get CharSpecType : {}", id);
        return charSpecTypeRepository.findById(id).map(charSpecTypeMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete CharSpecType : {}", id);
        charSpecTypeRepository.deleteById(id);
    }
}
