package com.mycompany.myapp.service.characteristic;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.characteristic.RelationshipType;
import com.mycompany.myapp.dto.characteristic.RelationshipTypeDTO;
import com.mycompany.myapp.mapper.characteristic.RelationshipTypeMapper;
import com.mycompany.myapp.repository.characteristic.RelationshipTypeRepository;

/**
 * Service for managing {@link RelationshipType}.
 */
@Service
@Transactional
public class RelationshipTypeService {

    private final Logger log = LoggerFactory.getLogger(RelationshipTypeService.class);

    private final RelationshipTypeRepository relationshipTypeRepository;

    private final RelationshipTypeMapper relationshipTypeMapper;

    public RelationshipTypeService(RelationshipTypeRepository relationshipTypeRepository,
            RelationshipTypeMapper relationshipTypeMapper) {
        this.relationshipTypeRepository = relationshipTypeRepository;
        this.relationshipTypeMapper = relationshipTypeMapper;
    }

    public RelationshipTypeDTO save(RelationshipTypeDTO relationshipTypeDTO) {
        log.debug("Request to save RelationshipType : {}", relationshipTypeDTO);
        RelationshipType relationshipType = relationshipTypeMapper.toEntity(relationshipTypeDTO);
        relationshipType = relationshipTypeRepository.save(relationshipType);
        return relationshipTypeMapper.toDto(relationshipType);
    }

    @Transactional(readOnly = true)
    public List<RelationshipTypeDTO> findAll() {
        log.debug("Request to get all ValueTypes");
        return relationshipTypeRepository.findAll().stream().map(relationshipTypeMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public Optional<RelationshipTypeDTO> findOne(Long id) {
        log.debug("Request to get RelationshipType : {}", id);
        return relationshipTypeRepository.findById(id).map(relationshipTypeMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete RelationshipType : {}", id);
        relationshipTypeRepository.deleteById(id);
    }
}
