package com.mycompany.myapp.service.characteristic;

import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.management.relation.InvalidRelationIdException;

import com.mycompany.myapp.domain.constant.AffectedObjectType;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecUse;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionCharspecUse;
import com.mycompany.myapp.repository.block.BlockRepository;
import com.mycompany.myapp.repository.offer.offer.OfferVersionCharspecUseRepository;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateVersionCharspecUseRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.characteristic.Relationship;
import com.mycompany.myapp.domain.characteristic.RelationshipPojo;
import com.mycompany.myapp.domain.characteristic.Value;
import com.mycompany.myapp.domain.characteristic.ValueGroup;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.CharacteristicType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.characteristic.CharacteristicDTO;
import com.mycompany.myapp.dto.characteristic.MappingValueDTO;
import com.mycompany.myapp.dto.characteristic.RelationshipDTO;
import com.mycompany.myapp.dto.characteristic.ValuesDTO;
import com.mycompany.myapp.mapper.characteristic.CharacteristicMapper;
import com.mycompany.myapp.mapper.characteristic.MappingValueMapper;
import com.mycompany.myapp.mapper.characteristic.RelationshipMapper;
import com.mycompany.myapp.mapper.characteristic.ValueMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.characteristic.CharacteristicRepository;
import com.mycompany.myapp.repository.characteristic.RelationshipRepository;
import com.mycompany.myapp.repository.characteristic.ValueGroupRepository;
import com.mycompany.myapp.repository.characteristic.ValueRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.ErrorConstants;
import com.mycompany.myapp.web.rest.errors.Resources;

import liquibase.util.BooleanUtils;

@Service
@Transactional
public class CharacteristicService extends AbstractCloneService {
    private final Logger log = LoggerFactory.getLogger(CharacteristicService.class);
    private String SCREEN = "characteristic";

    @Autowired
    CharacteristicRepository characteristicRepository;

    @Autowired
    private RelationshipRepository characteristicRelationshipRepository;

    @Autowired
    private ValueGroupRepository characteristicValueGroupRepository;

    @Autowired
    private ValueRepository characteristicValueRepository;

    @Autowired
    private CharacteristicMapper characteristicMapper;

    @Autowired
    private ValueMapper valueMapper;

    @Autowired
    private RelationshipMapper relationshipMapper;

    @Autowired
    private MappingValueMapper mappingValueMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private OfferVersionCharspecUseRepository offerVersionCharspecUseRepository;

    @Autowired
    private OfferTemplateVersionCharspecUseRepository offerTemplateVersionCharspecUseRepository;

    @Autowired
    private BlockRepository blockRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = characteristicRepository;
        this.parentService = categoryService;
    }

    public Page<CharacteristicDTO> searchCharacteristics(Long categoryId, String name, String description,
                                                         Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();

        Page<Characteristic> characteristicTypes = characteristicRepository.findCharacteristics(categoryId, domainId,
            name, description, pageable);
        List<ReferTable> listCharacteristicTypeName = referTableRepository.getReferTablesByReferType(ReferType.CharSpecType,Sort.unsorted());
        Map<Integer, String> characteristicNameMap = listCharacteristicTypeName.stream().collect(Collectors.toMap(ReferTable::getValue,item -> item.getName()));

        Page<CharacteristicDTO> dtos = characteristicTypes.map(characteristicMapper::toDto);
        dtos.getContent().stream().forEach(item -> item.setCharacteristicTypeName(characteristicNameMap.get(item.getCharacteristicTypeId())));
        return dtos;
    }

    public CharacteristicDTO save(CharacteristicDTO characteristicDTO) {
        Integer domainId = OCSUtils.getDomain();

        checkValueisExits(characteristicDTO.getCharacteristicTypeId());
        if ((characteristicDTO.getCharacteristicId() == null || characteristicDTO.getCharacteristicId() <= 0)
            && characteristicRepository.existsByName(characteristicDTO.getCharacteristicName())) {
            throw new BadRequestAlertException("duplicate_characteristic_name", SCREEN, "characteristicName");
        } else if (characteristicDTO.getCharacteristicId() != null && characteristicDTO.getCharacteristicId() > 0
            && characteristicRepository.existsByIdAndName(characteristicDTO.getCharacteristicId(),
            characteristicDTO.getCharacteristicName()) > 0) {
            throw new BadRequestAlertException("duplicate_characteristic_name", SCREEN, "characteristicName");
        }

        if (characteristicDTO.getStartDate().compareTo(characteristicDTO.getEndDate()) != 0
            && characteristicDTO.getStartDate().compareTo(characteristicDTO.getEndDate()) > 0) {
            throw new BadRequestAlertException("start_date_must_be_less_than_end_date", SCREEN, "startDate");
        }

        if (characteristicDTO.getMinCardinality() == null) {
            characteristicDTO.setMinCardinality(0);
        }

        if (characteristicDTO.getMaxCardinality() == null) {
            characteristicDTO.setMaxCardinality(10000);
        }

        if (characteristicDTO.getMinCardinality() >= characteristicDTO.getMaxCardinality()) {
            throw new BadRequestAlertException("min_cardinality_must_be_less_than_max_cardinality", SCREEN,
                "minCardinality");
        }

        // Luu charSpec
        Characteristic characteristic = characteristicMapper.toEntity(characteristicDTO);
        characteristic.setDomainId(domainId);
        // nếu là thêm mới lấy max và set giá trị cho orderIndex
        if (characteristicDTO.getCharacteristicId() == null || characteristicDTO.getCharacteristicId() <= 0) {
            setPosIndex(domainId, characteristic, characteristicDTO.getCategoryId());
        } else {
            Characteristic characteristicDb = characteristicRepository
                .findByIdAndDomainIdForUpdate(characteristicDTO.getCharacteristicId(), domainId);

            if (characteristicDb == null) {
                throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.CHARACTERISTIC, ErrorKey.ID);
            }
            if (!Objects.equal(characteristicDb.getParentId(), characteristicDTO.getCategoryId())) {
                super.setPosIndex(domainId, characteristic, characteristicDTO.getCategoryId());
            }
        }

        characteristic = characteristicRepository.save(characteristic);
        if (characteristicDTO.getCharacteristicId() != null && characteristicDTO.getCharacteristicId() > 0
            && characteristic.getValueType() != null) {
            characteristicRepository.updateAllValueTypeIdByCharacteristicId(characteristic.getValueType(),
                characteristic.getId());
        }
        if (characteristic.getId() != null) {
            characteristicDTO.setCharacteristicId(characteristic.getId());
        }
        return characteristicDTO;
    }


    /**
     * @author LamHV
     */
    public CharacteristicDTO findCharacteristicById(Long characteristicId) {
        log.debug("Request to get CharacteristicDTO : {}", characteristicId);
        Optional<CharacteristicDTO> characteristicDTO = characteristicRepository.findById(characteristicId)
            .map(characteristicMapper::toDto);
        if (characteristicDTO.isPresent()) {
            return characteristicDTO.get();
        }
        return null;
    }

    @Transactional
    public void deleteCharacteristic(Long id) {
        Integer domainId = OCSUtils.getDomain();
        log.debug("Request to delete Characteristic with Id: {}", id);
        Optional<Characteristic> charSpec = characteristicRepository.findById(id);
        if (charSpec.isPresent()) {

            List<Relationship> charSpecRelationshipList = characteristicRelationshipRepository
                .findByChildCharSpec(charSpec.get());
            if (charSpecRelationshipList != null && !charSpecRelationshipList.isEmpty()) {
                throw new BadRequestAlertException("cannot_be_deleted_because_it_is_being_referenced", SCREEN,
                    "characteristicId");
            }

            //check Normalizer use Char
            Long normUseCharacteristic = characteristicRepository.countNormUseChar(id);
            Long formulaUseCharacteristic = characteristicRepository.countFormulaUseChar(id);
            if (normUseCharacteristic > 0 || formulaUseCharacteristic > 0) {
                throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, SCREEN,
                    "characteristicId");
            }

            List<Value> values = characteristicValueRepository.findByCharSpecId(charSpec.get().getId());
            characteristicValueGroupRepository.deleteByParentCharValueIn(values);
            characteristicValueRepository.deleteAll(values);

            List<Relationship> CharSpecRelationshipByParentList = characteristicRelationshipRepository
                .findByParentCharSpec(charSpec.get());
            if (CharSpecRelationshipByParentList != null && !CharSpecRelationshipByParentList.isEmpty()) {
                for (Relationship charSpecRelationship : CharSpecRelationshipByParentList) {
                    characteristicValueGroupRepository.deleteByCharSpecRelationShipId(charSpecRelationship.getId());
                    characteristicRelationshipRepository.delete(charSpecRelationship);
                }
            }
            if ((offerVersionCharspecUseRepository.countByCharspecIdAndDomainId(id, domainId) > 0)) {
                throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
            }

            if (offerTemplateVersionCharspecUseRepository.countByCharspecIdAndDomainId(id, domainId)>0) {
                throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
            }

            if (blockRepository.findAllByAffectedValueAndAffectedObjectTypeAndDomainId(id, AffectedObjectType.CHARACTERISTIC.getValue(),domainId).size() > 0){
                throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
            }
            characteristicRepository.deleteById(id);
        } else {
            throw new BadRequestAlertException(ErrorConstants.NOT_FOUND, SCREEN, "characteristicId");
        }
    }

    @Transactional
    public void deleteAllValues(long characteristicId) {
        List<Value> charSpecValues = characteristicValueRepository.findByCharacteristicId(characteristicId);
        if (charSpecValues != null && !charSpecValues.isEmpty()) {
            for (Value charSpecValue : charSpecValues) {
                deleteValues(characteristicId, charSpecValue.getId());
            }
        }
        characteristicRepository.deleteDefaultValue(characteristicId);
    }

    @Transactional
    public void deleteValues(long id, long valueId) {
        deleteValues(id, valueId, false);
    }

    @Transactional
    public void deleteValues(long characteristicId, long valueId, boolean isCheckDefaultValue) {
        List<Value> listCharSpecValue = characteristicValueRepository.findByCharacteristicId(characteristicId);
        List<Long> listDefaultValueIds = new ArrayList<>();
        if (CollectionUtils.isEmpty(listCharSpecValue)) {
            return;
        }
        listDefaultValueIds = listCharSpecValue.stream().filter(x -> x.getIsDefault()).map(Value::getId)
            .collect(Collectors.toList());

        if (isCheckDefaultValue && !CollectionUtils.isEmpty(listDefaultValueIds)
            && listDefaultValueIds.contains(valueId) && listCharSpecValue.size() > 1) {
            throw new BadRequestAlertException("do_not_delete_characteristic_value_whose_default_value_is_true", SCREEN,
                "isDefault");
        }

        if (characteristicValueGroupRepository.countCharSpecValueGroups(valueId) > 0) {
            throw new BadRequestAlertException("cannot_be_deleted_because_it_is_being_referenced", SCREEN, "valueId");
        }
        characteristicValueRepository.deleteById(valueId);
    }

    public ValuesDTO save(ValuesDTO valuesDTO) {
        // Trong charSpec value thì phải có 1 giá trị default
        List<Long> listDefaultValueIds = characteristicValueRepository.findIdByIsDefaultAndCharacteristicId(true,
            valuesDTO.getCharacteristicId());

        if (CollectionUtils.isEmpty(listDefaultValueIds) && !valuesDTO.getIsDefault()) {
            throw new BadRequestAlertException("must_have_at_least_one_characteristic_value_with_a_default_is_true",
                SCREEN, "isDefault");
        } else if (!CollectionUtils.isEmpty(listDefaultValueIds) && listDefaultValueIds.contains(valuesDTO.getValueId())
            && !valuesDTO.getIsDefault()) {
            throw new BadRequestAlertException("must_have_at_least_one_characteristic_value_with_a_default_is_true",
                SCREEN, "isDefault");
        }
        // trong 1 charSpec thì ten của charSpec value không được trùng nhau
        if ((valuesDTO.getValueId() == null || valuesDTO.getValueId() <= 0) && characteristicValueRepository
            .countByNameAndCharacteristicId(valuesDTO.getValueName(), valuesDTO.getCharacteristicId()) > 0) {
            throw new BadRequestAlertException("the_names_of_the_characteristic_value_must_be_different", SCREEN,
                "valueName");
        } else if (valuesDTO.getValueId() != null && valuesDTO.getValueId() > 0
            && characteristicValueRepository.countByNameAndIdAndCharacteristicId(valuesDTO.getValueId(),
            valuesDTO.getValueName(), valuesDTO.getCharacteristicId()) > 0) {
            throw new BadRequestAlertException("the_names_of_the_characteristic_value_must_be_different", SCREEN,
                "valueName");
        }
        if (valuesDTO.getIsDefault()) {
            characteristicValueRepository.setDefaultInCharacteristicId(false, valuesDTO.getCharacteristicId());
        }

        Value charSpecValue = valueMapper.toEntity(valuesDTO);
        charSpecValue = characteristicValueRepository.save(charSpecValue);
        if(BooleanUtils.isTrue(valuesDTO.getIsDefault())) {
            characteristicRepository.updateDefaultValue(charSpecValue.getValue(), charSpecValue.getCharacteristic().getId());
        }
        ValuesDTO resultValuesDTO = valueMapper.toDto(charSpecValue);

        return resultValuesDTO;
    }

    @Transactional
    public RelationshipDTO save(long characteristicId, RelationshipDTO relationshipDTO) {
        Optional<RelationshipDTO> charSpecRela = findByParentIdAndChildId(characteristicId,
            relationshipDTO.getChildCharacteristicId());

        // trường hợp update
        if (relationshipDTO.getId() != null && relationshipDTO.getId() > 0) {
            if (!charSpecRela.isPresent()) {
                log.error("Khong ton tai relationship giua parentCharacteristicId: {}, va childCharacteristicId: {}",
                    characteristicId, relationshipDTO.getChildCharacteristicId());
                throw new BadRequestAlertException("relationship_id_invalid", SCREEN, "id");
            }
            if (charSpecRela.isPresent() && !charSpecRela.get().getId().equals(relationshipDTO.getId())) {
                log.error("Id relationship trong db va Input khac nhau");
                throw new BadRequestAlertException("relationship_id_invalid", SCREEN, "id");
            }
            if (!CollectionUtils.isEmpty(relationshipDTO.getMappingValue())) {
                updateMappingValueDTO(relationshipDTO.getMappingValue(), relationshipDTO);
            }
            relationshipDTO = save(relationshipDTO);
        } else { // trường hợp thêm mới
            if (charSpecRela.isPresent()) {
                log.error("Da ton tai relationship giua parentCharacteristicId: {}, va childCharacteristicId: {}",
                    characteristicId, relationshipDTO.getChildCharacteristicId());
                throw new BadRequestAlertException("relationship_id_already_exist", SCREEN, "id");
            }
            relationshipDTO.setParentCharacteristicId(characteristicId);
            relationshipDTO.setId(null);
            List<MappingValueDTO> mappingValues = relationshipDTO.getMappingValue();
            relationshipDTO = save(relationshipDTO);
            if (!CollectionUtils.isEmpty(mappingValues)) {
                updateMappingValueDTO(mappingValues, relationshipDTO);
            }
        }
        return relationshipDTO;
    }

    public Optional<RelationshipDTO> findByParentIdAndChildId(Long charSpecParentId, Long childSpecChildId) {
        Optional<Relationship> charSpeOptional = characteristicRelationshipRepository
            .findByParentIdAndChildId(charSpecParentId, childSpecChildId);
        return charSpeOptional.map(relationshipMapper::toDto);
    }

    public RelationshipDTO save(RelationshipDTO relationshipDTO) {
        log.debug("Request to save CharSpecRelationship : {}", relationshipDTO);
        Relationship charSpecRelationship = relationshipMapper.toEntity(relationshipDTO);
        charSpecRelationship = characteristicRelationshipRepository.save(charSpecRelationship);
        RelationshipDTO result = relationshipMapper.toDto(charSpecRelationship);
        return result;
    }

    private void updateMappingValueDTO(List<MappingValueDTO> mappingValueDTOs, RelationshipDTO relationshipDTO) {
        // xóa phần tử cần delete
        List<MappingValueDTO> lstToDels = mappingValueDTOs.stream().filter(x -> "delete".equals(x.getActionType()))
            .collect(Collectors.toList());
        for (MappingValueDTO mappingValueDTO : lstToDels) {
            characteristicValueGroupRepository.deleteById(mappingValueDTO.getId());
        }
        mappingValueDTOs.removeAll(lstToDels);

        if (CollectionUtils.isEmpty(mappingValueDTOs)) {
            return;
        }

        // Check validate
        List<Long> charValueDefaultId = characteristicValueRepository
            .getCharValueGroupDefaultId(relationshipDTO.getId());
        // Check phai co 1 gia tri default
        long numDefault = mappingValueDTOs.stream().filter(x -> x.getIsDefault() == true).count();
        // Hoặc là trong database phải có một thằng set default = true hoặc là bên ngoài
        // truyền vào phải có 1 thằng defauult = true, nếu ko thỏa mãn thì báo lỗi
        if ((CollectionUtils.isEmpty(charValueDefaultId) && numDefault <= 0)) {
            throw new BadRequestAlertException("must_have_at_least_one_mapping_value_with_a_default_is_true", SCREEN,
                "isDefault");
        }

        // Value of parent không được trùng
        List<Long> charValueParents = new ArrayList();
        mappingValueDTOs.forEach(charVal -> {
            if (charValueParents.contains(charVal.getValueOfParentId())) {
                throw new BadRequestAlertException("valueOfParentName_cannot_be_duplicated", SCREEN,
                    "valueOfParentName");
            } else {
                charValueParents.add(charVal.getValueOfParentId());
            }
        });

        for (MappingValueDTO mappingValueDTO : mappingValueDTOs) {
            if (mappingValueDTO.getIsDefault()) {
                characteristicValueGroupRepository.setDefaultByCharSpecRelationshipId(false, relationshipDTO.getId());
            }
            mappingValueDTO.setRelationshipTypeId(Resources.DEFAULT_RELATIONSHIP);
            mappingValueDTO.setRelationshipId(relationshipDTO.getId());
            mappingValueDTO = saveValueGroup(mappingValueDTO);
        }

        List<Long> parentValueIds = characteristicValueRepository.getCharValueIdByRelationshipId(relationshipDTO.getId());
        List<Long> distinctValueIds = parentValueIds.stream().distinct().collect(Collectors.toList());
        if(parentValueIds.size() > distinctValueIds.size()){
            throw new BadRequestAlertException("valueOfParentName_cannot_be_duplicated", SCREEN,"valueOfParentName");
        }
    }

    public MappingValueDTO saveValueGroup(MappingValueDTO charSpecValueGroupDTO) {
        log.debug("Request to save CharSpecValueGroup : {}", charSpecValueGroupDTO);
        ValueGroup charSpecValueGroup = mappingValueMapper.toEntity(charSpecValueGroupDTO);
        charSpecValueGroup = characteristicValueGroupRepository.save(charSpecValueGroup);
        return mappingValueMapper.toDto(charSpecValueGroup);
    }

    public Page<ValuesDTO> getValuesDTOs(Long charSpecId, Pageable pageable, String textSearch) {
        log.debug("Request to get CharSpecValues by charSpecId");
        Page<Value> listCharSpecValue = characteristicValueRepository.findByCharacteristicId(charSpecId, pageable,
            textSearch);
        return new PageImpl<>(listCharSpecValue.stream().map(valueMapper::toDto).collect(Collectors.toList()), pageable,
            listCharSpecValue.getTotalElements());
    }

    public ValuesDTO getValuesDTO(Long valueId) {
        Optional<ValuesDTO> valueDTO = characteristicValueRepository.findById(valueId).map(valueMapper::toDto);
        if (valueDTO.isPresent()) {
            return valueDTO.get();
        }
        return null;
    }

    public List<ValuesDTO> getAllValuesDTO(long characteristicId) {
        log.debug("Request to get CharSpecValues by charSpecId");
        List<Value> listCharSpecValue = characteristicValueRepository.findByCharacteristicId(characteristicId);
        return listCharSpecValue.stream().map(valueMapper::toDto).collect(Collectors.toList());
    }

    /**
     * @author LamHV get CharSpec theo parentId
     */
    public Page<RelationshipDTO> findRelationshipByParentId(Long parentId, Pageable pageable, String textSearch) {
        log.debug("Request to get CharSpec by charSpecParentId");

        List<RelationshipDTO> allRelationshipDTOs = new ArrayList<>();

        generateRelationLevelList(parentId, 0, 1, "", allRelationshipDTOs);

        List<RelationshipDTO> resultList = null;

        if (textSearch == null || textSearch.equals("")) {
            // Thực hiện phân trang trước khi xử lý để giảm thiểu performance
            resultList = allRelationshipDTOs;
            // Lấy tất cả các ChildId sẽ hiển thị trên một page
            Set<Long> allChildIdOnPage = resultList.stream().map(RelationshipDTO::getChildCharacteristicId)
                .collect(Collectors.toSet());
            // Lấy thông tin Charspec trong Database và lưu dưới dạng Map để phục vụ cho
            // việc tham chiếu ở dưới
            Map<Long, Characteristic> mapCharacteristic = (Map<Long, Characteristic>) characteristicRepository
                .findByListId(new ArrayList(allChildIdOnPage)).stream()
                .collect(Collectors.toMap(o -> ((Characteristic) o).getId(), o -> o));
            // Cập nhật Characteristic Name và Description
            updateRelationshipDTO(resultList, mapCharacteristic);

        } else {
            // Đối với Search thì không thể phân trang trước nên lấy tất cả các ChildId sau
            // đó mới xử lý
            Set<Long> allChildId = allRelationshipDTOs.stream().map(RelationshipDTO::getChildCharacteristicId)
                .collect(Collectors.toSet());
            Map<Long, Characteristic> mapCharacteristic = (Map<Long, Characteristic>) characteristicRepository
                .searchByNameAndListId(textSearch, new ArrayList(allChildId)).stream()
                .collect(Collectors.toMap(o -> ((Characteristic) o).getId(), o -> o));
            // Cập nhật Characteristic Name và Description
            resultList = updateRelationshipDTO(allRelationshipDTOs, mapCharacteristic);
        }
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), resultList.size());
        int subend = (start + pageable.getPageSize()) > resultList.size() ? resultList.size()
            : (start + pageable.getPageSize());
        return new PageImpl<RelationshipDTO>(resultList.subList(start, end), pageable, resultList.size());
    }

    /**
     * Hàm này sử dụng để cập nhật Characteristic Name và Description cho
     * RelationshipDTO, giúp giảm performance vì không cần phải truy vấn vào
     * Database nhiều lần
     *
     * @param listRelationshipDTO: Danh sách RelationshipDTO chưa cập nhật Characteristic Name và
     *                             Description
     * @param mapCharacteristic:   Map chứa thông tin key = CharacteristicId và Value là đối tượng
     *                             CharSpec (có chứa thông tin Characteristic Name và Description)
     * @return List<RelationshipDTO> - danh sách RelationshipDTO đã cập nhật
     * Characteristic Name và Description
     */
    private List<RelationshipDTO> updateRelationshipDTO(List<RelationshipDTO> listRelationshipDTO,
                                                        Map<Long, Characteristic> mapCharacteristic) {
        List<RelationshipDTO> resultList = new ArrayList<RelationshipDTO>();
        for (RelationshipDTO relationshipDTO : listRelationshipDTO) {
            if (mapCharacteristic.containsKey(relationshipDTO.getChildCharacteristicId())) {
                Characteristic charSpec = mapCharacteristic.get(relationshipDTO.getChildCharacteristicId());
                relationshipDTO.setChildCharacteristicName(charSpec.getName());
                relationshipDTO.setDescription(charSpec.getDescription());
                resultList.add(relationshipDTO);
            }
        }

        return resultList;
    }

    /**
     * Hàm này sử dụng để lấy dữ liệu Characteristic theo 3 cấp, tính toán level
     * hiển thị ví dụ như 1 -> 1.1 -> 1.1.1, bằng cách đệ qui danh sách kết quả lấy
     * được từ Database, giúp giảm thiểu số lần truy vấn vào Database để giúp chương
     * trình chạy nhanh hơn
     *
     * @param parentId:            CharacteristicId cha
     * @param level:               cấp đầu tiên = 0, tiếp theo = 1, sau đó là = 2...
     * @param index:               dùng để hiển thị thứ tự trong 1 cấp, bắt đầu bằng 1
     * @param indexLevel:          dùng để hiển thị cột thông tin 1, 1.1, 1.1.1 hoặc 2, 2.1, 2.1.1...
     * @param allRelationshipDTOs: dùng để lưu kết quả có được sau khi đã tính toán đệ qui các cấp
     */
    private void generateRelationLevelList(Long parentId, int level, int index, String indexLevel,
                                           List<RelationshipDTO> allRelationshipDTOs) {
        // Chỉ đệ qui đến 3 cấp (level bắ đầu từ 0)
        if (level < 3) {
            // Lấy toàn bộ Relationship có ParentId = parentId (CharacteristicId của chính
            // Characterictis đang mở trên màn hình)
            List<RelationshipPojo> allRelationships = characteristicRelationshipRepository.listRelationship(parentId);

            // Tạo Map chứa với Key là ParentId và Value là một Set chưa toàn bộ
            // Relationship con của nó
            Map<Long, TreeSet<RelationshipPojo>> mapLevel = allRelationships.stream()
                .collect(Collectors.groupingBy(RelationshipPojo::getParentId, Collectors.toCollection(TreeSet::new)));

            // Duyệt từng cha (ParentID trong Map tạo ở trên) để tìm tiếp các con, cháu của
            // nó
            for (Map.Entry<Long, TreeSet<RelationshipPojo>> entryLevel : mapLevel.entrySet()) {
                int i = index;
                for (RelationshipPojo relationship : entryLevel.getValue()) {
                    // Nếu ParentId = ChildId thì báo lôi dính vòng lặp vô hạn
                    if (relationship.getParentId().equals(relationship.getChildId())) {
                        throw new BadRequestAlertException(relationship.getChildId()+"_"+ErrorMessage.Characteristic.CYCLE_INHERIT,
                            Resources.CHARACTERISTIC, ErrorKey.Characteristic.ID);
                    }
                    RelationshipDTO relationshipDTO = new RelationshipDTO();
                    relationshipDTO.setId(relationship.getId());
                    relationshipDTO.setParentCharacteristicId(relationship.getParentId());
                    relationshipDTO.setChildCharacteristicId(relationship.getChildId());
                    relationshipDTO.setLevel(level);
                    // Ban đầu indexLevel sẽ = empty sau đó sẽ thành 1, 2, 3...; nếu có con thì sẽ
                    // thành 1.1, 1.2, 2.1, 2.2...; nếu có cháu sẽ thành 1.1.1, 1.1.2, 1.1.3...
                    relationshipDTO.setIndex((StringUtils.isEmpty(indexLevel) ? "" : indexLevel + ".") + i);
                    relationshipDTO.setRelationshipTypeId(relationship.getRelationshipTypeId());
                    relationshipDTO.setRelationshipTypeName(relationship.getRelationshipTypeName());
                    allRelationshipDTOs.add(relationshipDTO);
                    // Đệ qui để lấy ra các relationship cấp con
                    generateRelationLevelList(relationship.getChildId(), level + 1, index, relationshipDTO.getIndex(),
                        allRelationshipDTOs);
                    //Tăng index trong cùng 1 cấp
                    i++;
                }
            }
        }
    }

    public List<MappingValueDTO> getAllMappingValueDTOs(Long parentCharId, Long childCharId) {
        List<MappingValueDTO> listMappingValueDTOs = new ArrayList<>();
        MappingValueDTO mappingValueDTO;
        Optional<RelationshipDTO> charSpecRelationshipDTO = findByParentIdAndChildId(parentCharId, childCharId);
        Characteristic parentCharSpec = characteristicRepository.getOne(parentCharId);
        Characteristic childCharSpec = characteristicRepository.getOne(childCharId);
        if (charSpecRelationshipDTO.isPresent() && parentCharSpec != null && childCharSpec != null) {
            log.debug("char spec relationship: ", charSpecRelationshipDTO.get());
            List<ValueGroup> charSpecValueGroups = characteristicValueGroupRepository
                .findByCharSpecRelationshipId(charSpecRelationshipDTO.get().getId());
            for (ValueGroup valueGroup : charSpecValueGroups) {
                mappingValueDTO = new MappingValueDTO();
                mappingValueDTO.setId(valueGroup.getId());
                mappingValueDTO.setValueOfParentId(valueGroup.getParentCharValue().getId());
                mappingValueDTO.setValueOfParent(valueGroup.getParentCharValue().getName());
                mappingValueDTO.setValueOfChildId(valueGroup.getChildCharValue().getId());
                mappingValueDTO.setValueOfChild(valueGroup.getChildCharValue().getName());
                mappingValueDTO.setIsDefault(valueGroup.getIsDefault());
                mappingValueDTO.setRelationshipId(valueGroup.getCharSpecRelationship().getId());
                mappingValueDTO.setParentCharacteristicId(parentCharSpec.getId());
                mappingValueDTO.setParentCharacteristicName(parentCharSpec.getName());
                mappingValueDTO.setChildCharacteristicId(childCharSpec.getId());
                mappingValueDTO.setChildCharacteristicName(childCharSpec.getName());
                listMappingValueDTOs.add(mappingValueDTO);
            }
        }
        return listMappingValueDTOs;
    }

    @Transactional
    public void deleteRelationship(long characteristicId, long relationshipId) {
        List<Long> listCharValueGroupId = characteristicValueGroupRepository
            .listcharValueGroupByRelationship(relationshipId);
        for (Long valueGroupId : listCharValueGroupId) {
            characteristicValueGroupRepository.deleteById(valueGroupId);
        }
        characteristicRelationshipRepository.deleteById(relationshipId);
    }

    @Transactional
    public void deleteAllRelationships(long characteristicId) {
        Optional<Characteristic> charSpec = characteristicRepository.findById(characteristicId);
        if (charSpec.isPresent()) {
            List<Relationship> CharSpecRelationshipByParentList = characteristicRelationshipRepository
                .findByParentCharSpec(charSpec.get());
            if (CharSpecRelationshipByParentList != null && !CharSpecRelationshipByParentList.isEmpty()) {
                for (Relationship charSpecRelationship : CharSpecRelationshipByParentList) {
                    deleteRelationship(characteristicId, charSpecRelationship.getId());
                }
            }
        }
    }

    public CharacteristicDTO getCharacteristicDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Characteristic characteristic = characteristicRepository.findByIdAndDomainId(id, domainId);
        if (characteristic == null) {
            throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND, Resources.CHARACTERISTIC,
                ErrorKey.Characteristic.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(characteristic.getCategoryId(), domainId);
        CharacteristicDTO characteristicDTO = characteristicMapper.toDto(characteristic);
        characteristicDTO.setCategoryName(category.getName());
        characteristicDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        return characteristicDTO;
    }

    public List<Long> getRemoveIds(Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> removeIds = new ArrayList<>();
        removeIds = characteristicRelationshipRepository.getChildIdByParentId(id);
        Characteristic characteristic = characteristicRepository.findByIdAndDomainId(id, domainId);
        if (characteristic == null) {
            throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND, Resources.CHARACTERISTIC,
                ErrorKey.Characteristic.ID);
        }
        removeIds.addAll(characteristicRepository.getCharspecIdByDomainIdAndTypeNot(domainId, characteristic.getValueRefeTable()));
        removeIds.add(id);
        return removeIds;
    }

    public void checkRepeatRelationship(Long childId, List<Long> parentIds, Integer level) {
        if(level < 3){
            List<Long> grandParentIds =
                characteristicRelationshipRepository.findParentCharSpecIdByChildrenId(parentIds.get(parentIds.size()-1));
            if (grandParentIds == null) {
                grandParentIds = new ArrayList<>();
            }
            for (Long grandParentId : grandParentIds) {
                if (Objects.equal(grandParentId, childId) || parentIds.stream().anyMatch(parentId -> Objects.equal(grandParentId, parentId))) {
                    throw new ResourceConflictException(childId+ "_" + ErrorMessage.Characteristic.CYCLE_INHERIT, Resources.CHARACTERISTIC, ErrorKey.Characteristic.ID);
                }

                List<Long> newParentIds = new ArrayList<>();
                newParentIds.addAll(parentIds);
                newParentIds.add(grandParentId);

                checkRepeatRelationship(childId, newParentIds, level + 1);
            }
        }
    }

    public void checkCharacteristicExits(Long charSpecId) {
        Integer domainId = OCSUtils.getDomain();
        Characteristic characteristic = characteristicRepository.findByIdAndDomainIdForUpdate(charSpecId, domainId);
        if (characteristic == null) {
            throw new ResourceNotFoundException(ErrorMessage.Characteristic.NOT_FOUND, Resources.CHARACTERISTIC,
                ErrorKey.Characteristic.ID);
        }
    }

    public String findNameById(Long charSpecId) {
        checkCharacteristicExits(charSpecId);
        Integer domainId = OCSUtils.getDomain();
        return characteristicRepository.findNameByIdAndDomain(charSpecId, domainId);
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList(CategoryType.OFFER_VERSION, CategoryType.OFFER_TEMPlATE_VERSION);
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.CHARACTERISTIC);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new CharacteristicDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.CHARACTERISTIC;
    }

    public void checkValueisExits(Integer value) {
        Integer domainId = OCSUtils.getDomain();
        ReferTable refertable = referTableRepository.getReferTableByReferTypeAndValue(ReferType.CharSpecType, value);
        if (refertable == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReferTable.CHARSPEC_TYPE_NOT_FOUND, Resources.CHARACTERISTIC, ErrorKey.ReferTable.VALUE);
        }
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Characteristic.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.CHARACTERISTIC;
    }

    @Override
    protected String getNameCategory() {
        // TODO Auto-generated method stub
        return null;
    }

    public List<Long> getIds(Long offerVersionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferVersionCharspecUse> offerVersionCharspecUses = offerVersionCharspecUseRepository.findByDomainIdAndOfferVersionId(domainId, offerVersionId);
        return offerVersionCharspecUses.stream().map(id -> id.getCharspecId()).collect(Collectors.toList());
    }

    public List<Long> getIdsInOfferTemplateVersion(Long offerTemplateVersionId) {
        Integer domainId = OCSUtils.getDomain();
        List<OfferTemplateVersionCharspecUse> offerTemplateVersionCharspecUses = offerTemplateVersionCharspecUseRepository.findByDomainIdAndOfferTemplateVersionId(domainId, offerTemplateVersionId);
        return offerTemplateVersionCharspecUses.stream().map(id -> id.getCharspecId()).collect(Collectors.toList());
    }

    public List<Long> getRemoveId(CharacteristicType offer) {
        List<Long> idCharacteristic = characteristicRepository.findByValue(offer.getValue());
        return idCharacteristic;
    }

    public Integer getCharspecType(Long id) {
       return characteristicRepository.findCharspectypeId(id);
    }

    public List<Long> RemoveIdByCharSpecId(Long charspecId){
        List<Long> removeID = new ArrayList<>();
        List<Long> childCharspecIds = new ArrayList<>();
        List<Long> parentCharspectIds = new ArrayList<>();
        List<Long> removeIds = new ArrayList<>();

        Integer charspecType = characteristicRepository.findCharspectypeId(charspecId);
        removeID = characteristicRepository.findByValue(charspecType);
        childCharspecIds  = characteristicRelationshipRepository.findChildCharspecId(charspecId);
        parentCharspectIds = characteristicRelationshipRepository.findParentCharSpecId(charspecId);
        removeIds.addAll(removeID);
        removeIds.addAll(childCharspecIds);
        removeIds.addAll(parentCharspectIds);
        return removeIds;
    }

    public void saveRelationship(String SCREEN_NAME, Long id, List<RelationshipDTO> relationshipDTOS){

        for (RelationshipDTO relationshipDTO : relationshipDTOS) {
            relationshipDTO.setParentCharacteristicId(id);
            log.debug("REST request to Add new Characteristic Relationship: {}", relationshipDTO);
            if (relationshipDTO.getId() != null) {
                throw new BadRequestAlertException("relation_ship_id_already_already_exist", SCREEN_NAME, "id");
            }

            if (relationshipDTO.getRelationshipTypeId() == null) {
                throw new BadRequestAlertException("relation_ship_type_id_must_not_be_null", SCREEN_NAME, "relationshipTypeId");
            }

            checkRepeatRelationship(relationshipDTO.getChildCharacteristicId(), Arrays.asList(id),0);
            relationshipDTO = save(id, relationshipDTO);
        }
    }

    @Transactional
    public void deleteByCategoryId(Long categoryId) {
        Integer domainId = OCSUtils.getDomain();
        List<Long> charSpecIds = characteristicRepository.findIdByCategoryId(categoryId);
        for (Long charSpecId : charSpecIds) {
            Optional<Characteristic> charSpec = characteristicRepository.findById(charSpecId);
            if (charSpec.isPresent()) {
                List<Relationship> charSpecRelationshipList = characteristicRelationshipRepository
                    .findByChildCharSpec(charSpec.get());
                if (charSpecRelationshipList != null && !charSpecRelationshipList.isEmpty()) {
                    throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, SCREEN,ErrorKey.ID);
                }
                //check Normalizer use Char
                Long normUseCharacteristic = characteristicRepository.countNormUseChar(charSpecId);
                Long formulaUseCharacteristic = characteristicRepository.countFormulaUseChar(charSpecId);
                if (normUseCharacteristic > 0 || formulaUseCharacteristic > 0) {
                    throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, SCREEN,ErrorKey.ID);
                }

                this.deleteAllValues(charSpecId);
                List<Relationship> CharSpecRelationshipByParentList = characteristicRelationshipRepository
                    .findByParentCharSpec(charSpec.get());
                if (CharSpecRelationshipByParentList != null && !CharSpecRelationshipByParentList.isEmpty()) {
                    for (Relationship charSpecRelationship : CharSpecRelationshipByParentList) {
                        characteristicValueGroupRepository.deleteByCharSpecRelationShipId(charSpecRelationship.getId());
                        characteristicRelationshipRepository.delete(charSpecRelationship);
                    }
                }
                if ((offerVersionCharspecUseRepository.countByCharspecIdAndDomainId(charSpecId, domainId) > 0)) {
                    throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
                }

                if (offerTemplateVersionCharspecUseRepository.countByCharspecIdAndDomainId(charSpecId, domainId)>0) {
                    throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
                }

                if (blockRepository.findAllByAffectedValueAndAffectedObjectTypeAndDomainId(charSpecId, AffectedObjectType.CHARACTERISTIC.getValue(),domainId).size() > 0){
                    throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, "", ErrorKey.ID);
                }
            } else {
                throw new BadRequestAlertException(ErrorConstants.NOT_FOUND, SCREEN, ErrorKey.ID);
            }
        }
        characteristicRepository.deleteAllByIdIn(charSpecIds);
    }

}
