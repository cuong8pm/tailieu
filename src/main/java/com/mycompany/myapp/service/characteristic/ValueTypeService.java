package com.mycompany.myapp.service.characteristic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.characteristic.ValueType;
import com.mycompany.myapp.dto.characteristic.ValueTypeDTO;
import com.mycompany.myapp.mapper.characteristic.ValueTypeMapper;
import com.mycompany.myapp.repository.ValueTypeRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ValueType}.
 */
@Service
@Transactional
public class ValueTypeService {

    private final Logger log = LoggerFactory.getLogger(ValueTypeService.class);

    private final ValueTypeRepository valueTypeRepository;

    private final ValueTypeMapper valueTypeMapper;

    public ValueTypeService(ValueTypeRepository valueTypeRepository, ValueTypeMapper valueTypeMapper) {
        this.valueTypeRepository = valueTypeRepository;
        this.valueTypeMapper = valueTypeMapper;
    }

    public ValueTypeDTO save(ValueTypeDTO valueTypeDTO) {
        log.debug("Request to save ValueType : {}", valueTypeDTO);
        ValueType valueType = valueTypeMapper.toEntity(valueTypeDTO);
        valueType = valueTypeRepository.save(valueType);
        return valueTypeMapper.toDto(valueType);
    }

    @Transactional(readOnly = true)
    public List<ValueTypeDTO> findAll() {
        log.debug("Request to get all ValueTypes");
        return valueTypeRepository.findAll().stream().map(valueTypeMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public Optional<ValueTypeDTO> findOne(Long id) {
        log.debug("Request to get ValueType : {}", id);
        return valueTypeRepository.findById(id).map(valueTypeMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete ValueType : {}", id);
        valueTypeRepository.deleteById(id);
    }
}
