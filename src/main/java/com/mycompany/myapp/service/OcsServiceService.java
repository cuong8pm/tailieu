package com.mycompany.myapp.service;

import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.mapper.OcsSeriviceMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.OcsServiceRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.OCSService;
import com.mycompany.myapp.domain.Parameter;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.OcsServiceDTO;

@Service
@Transactional
public class OcsServiceService extends OCSBaseService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private OcsServiceRepository ocsServiceRepository;
    @Autowired
    private OcsSeriviceMapper ocsSeriviceMapper;
    @Autowired
    private CategoryService categoryService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = ocsServiceRepository;
        this.parentService = categoryService;
    }
    
    public void deleteService(Long id) {
        Integer domainId = OCSUtils.getDomain();
        ocsServiceRepository.deleteByIdAndDomainId(id, domainId);
    }

    public Page<OcsServiceDTO> getServices(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);
        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY,
                    ErrorKey.Category.ID);
        }
        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;
        Page<OcsServiceDTO> services = ocsServiceRepository.findServices(searchCriteria.getParentId(),
                searchCriteria.getDomainId(), searchCriteria.getName(), searchCriteria.getDescription(), pageable)
                .map(ocsSeriviceMapper::toDto);

        for (OcsServiceDTO service : services) {
            service.setCategoryLevel(parentCategoryLevel);
        }
        return services;
    }

    public Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCategorySearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC, Parameter.FieldNames.POS_INDEX) : sort);
    }

    public BaseResponseDTO createService(OcsServiceDTO ocsServiceDTO) {
        Integer domainId = OCSUtils.getDomain();

        OCSService service = ocsSeriviceMapper.toEntity(ocsServiceDTO);
        checkDuplicateName(ocsServiceDTO.getName(), ocsServiceDTO.getId());
        super.setPosIndex(domainId, service, service.getCategoryId());
        service = ocsServiceRepository.save(service);

        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(service, response);
        return response;
    }

    public BaseResponseDTO updateService(OcsServiceDTO ocsServiceDTO) {
        Integer domainId = OCSUtils.getDomain();
        OCSService serviceDB = ocsServiceRepository.findByIdAndDomainId(ocsServiceDTO.getId(), domainId);

        if (serviceDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.SERVICE, ErrorKey.Service.ID);
        }

        checkDuplicateName(ocsServiceDTO.getName(), ocsServiceDTO.getId());

        OCSService service = ocsSeriviceMapper.toEntity(ocsServiceDTO);
        service.setDomainId(domainId);

        if (!Objects.equal(serviceDB.getCategoryId(), ocsServiceDTO.getCategoryId())) {
            setPosIndex(domainId, service, ocsServiceDTO.getCategoryId());
        }
        
        service = ocsServiceRepository.save(service);
        
        return ocsSeriviceMapper.toDto(service);
    }

    public OcsServiceDTO getServiceDetail(Long serviceId) {
        Integer domainId = OCSUtils.getDomain();
        OCSService service = ocsServiceRepository.findByIdAndDomainId(serviceId, domainId);
        if (service == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.SERVICE, ErrorKey.Service.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(service.getCategoryId(), domainId);
        OcsServiceDTO ocsServiceDTO = new OcsServiceDTO();
        ocsServiceDTO.setName(service.getName());
        ocsServiceDTO.setId(service.getId());
        ocsServiceDTO.setDescription(service.getServiceDesc());
        ocsServiceDTO.setCategoryName(category.getName());
        ocsServiceDTO.setCategoryLevel(getCategoryLevel(service.getCategoryId()) + 1);
        return ocsServiceDTO;
    }
    
    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.SERVICE;
    }

}
