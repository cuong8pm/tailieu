package com.mycompany.myapp.service;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.repository.action.ActionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ReserveInfo;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ReserveInfoDTO;
import com.mycompany.myapp.mapper.ReserveInfoMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.ReserveInfoRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class ReserveInfoService extends OCSBaseService {

    @Autowired
    private ReserveInfoRepository reserveInfoRepository;

    @Autowired
    private ReserveInfoMapper reserveInfoMapper;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ActionRepository actionRepository;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = reserveInfoRepository;
        this.parentService = categoryService;
    }

    public Page<ReserveInfoDTO> getReserveInfo(Long categoryId, String name, String description, String id, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Page<ReserveInfoDTO> reverseInfos = reserveInfoRepository.findReserveInfos(categoryId, domainId, name, description, id, pageable).map(reserveInfoMapper::toDto);

        for (ReserveInfoDTO reverseInfo : reverseInfos) {
            reverseInfo.setCategoryLevel(parentCategoryLevel);
        }

        return reverseInfos;
    }

    public ReserveInfoDTO getReserveInfoDetails(Long id) {
        Integer domainId = OCSUtils.getDomain();
        ReserveInfo reserveInfo = reserveInfoRepository.findByIdAndDomainId(id, domainId);
        if (reserveInfo == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReserveInfo.NOT_FOUND, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.ID);
        }

        Category category = categoryRepository.findByIdAndDomainIdForUpdate(reserveInfo.getCategoryId(), domainId);
        ReserveInfoDTO reserveInfoDTO = reserveInfoMapper.toDto(reserveInfo);
        reserveInfoDTO.setCategoryName(category.getName());
        reserveInfoDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);

        return reserveInfoDTO;
    }

    public ReserveInfoDTO createReserveInfo(ReserveInfoDTO reserveInfoDTO) {
        Integer domainId = OCSUtils.getDomain();
        validateReserveInfo(reserveInfoDTO);

        checkCategoryExist(reserveInfoDTO, domainId);
        checkDuplicateName(reserveInfoDTO.getName(), reserveInfoDTO.getId());

        ReserveInfo reserveInfo = reserveInfoMapper.toEntity(reserveInfoDTO);
        reserveInfo.setDomainId(domainId);

        super.setPosIndex(domainId, reserveInfo, reserveInfo.getCategoryId());

        if (reserveInfo.getCanRollback() == null) {
            reserveInfo.setCanRollback(false);
        }

        reserveInfo = reserveInfoRepository.save(reserveInfo);

        return reserveInfoMapper.toDto(reserveInfo);
    }

    public ReserveInfoDTO updateReserveInfo(ReserveInfoDTO reserveInfoDTO) {
        Integer domainId = OCSUtils.getDomain();
        validateReserveInfo(reserveInfoDTO);
        // Lock old category
        checkCategoryExist(reserveInfoDTO, domainId);
        checkDuplicateName(reserveInfoDTO.getName(), reserveInfoDTO.getId());
        ReserveInfo reserveInfoDB = reserveInfoRepository.findByIdAndDomainIdForUpdate(reserveInfoDTO.getId(), domainId);

        if (reserveInfoDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReserveInfo.NOT_FOUND, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.ID);
        }

        // Convert to Entity to save
        ReserveInfo reserveInfo = reserveInfoMapper.toEntity(reserveInfoDTO);
        reserveInfo.setDomainId(domainId);

        if (!Objects.equal(reserveInfoDTO.getCategoryId(), reserveInfoDB.getCategoryId())) {
            setPosIndex(domainId, reserveInfo, reserveInfoDTO.getCategoryId());
        }

        reserveInfo = reserveInfoRepository.save(reserveInfo);

        return reserveInfoMapper.toDto(reserveInfo);
    }

    public void deleteReserveInfo(Long id) {
        Integer domainId = OCSUtils.getDomain();
        checkReserveInfoUsed(id, domainId);
        reserveInfoRepository.deleteByIdAndDomainId(id, domainId);
    }

    private void checkReserveInfoUsed(Long id, Integer domainId) {
        Integer checkUsed = actionRepository.countByReserveInfoIdAndDomainId(id, domainId);
        if (checkUsed > 0) {
            throw new DataConstrainException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.RESERVE_INFO,
                ErrorKey.ReserveInfo.ID);
        }
    }

    // Check category exist or not
    private void checkCategoryExist(ReserveInfoDTO reserveInfoDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(reserveInfoDTO.getCategoryId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        if (parentCategory.getCategoryType() != CategoryType.RESERVE_INFO) {
            throw new DataConstrainException(ErrorMessage.CATEGORY_INCORRECT, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    private void validateReserveInfo(ReserveInfoDTO reserveInfoDTO) {
        if (reserveInfoDTO.getUsageQuotaThreshold() == null) {
            if (!(reserveInfoDTO.getMaxReserveValue() > reserveInfoDTO.getMinReserveValue())) {
                throw new DataConstrainException(ErrorMessage.ReserveInfo.MAX_GTE_MIN, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MAX_RESERVE_VALUE);
            }
            return;
        }
        if(reserveInfoDTO.getUsageQuotaThreshold() >= reserveInfoDTO.getMaxReserveValue()){
            if(reserveInfoDTO.getMaxReserveValue() > reserveInfoDTO.getMinReserveValue()){
                throw new DataConstrainException(ErrorMessage.ReserveInfo.MAX_GTE_THRES_AND_MIN, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MAX_RESERVE_VALUE);
            }
            if(reserveInfoDTO.getMaxReserveValue() <= reserveInfoDTO.getMinReserveValue()){
                throw new DataConstrainException(ErrorMessage.ReserveInfo.MAX_GTE_THRES_AND_MIN, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MAX_RESERVE_VALUE);
            }
            throw new DataConstrainException(ErrorMessage.ReserveInfo.MAX_GTE_THRES_AND_MIN, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MAX_RESERVE_VALUE);
        }
        if(reserveInfoDTO.getUsageQuotaThreshold() <= reserveInfoDTO.getMinReserveValue()){
            if(reserveInfoDTO.getMaxReserveValue() > reserveInfoDTO.getMinReserveValue()){
                throw new DataConstrainException(ErrorMessage.ReserveInfo.MIN_LESS_THRES_AND_MAX, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MIN_RESERVE_VALUE);
            }
            if(reserveInfoDTO.getUsageQuotaThreshold() < reserveInfoDTO.getMaxReserveValue() && reserveInfoDTO.getMaxReserveValue() <= reserveInfoDTO.getMinReserveValue()){
                throw new DataConstrainException(ErrorMessage.ReserveInfo.MAX_GTE_THRES_AND_MIN, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MAX_RESERVE_VALUE);
            }
            throw new DataConstrainException(ErrorMessage.ReserveInfo.MIN_LESS_THRES_AND_MAX, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.MIN_RESERVE_VALUE);
        }
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.RESERVE_INFO;
    }
}
