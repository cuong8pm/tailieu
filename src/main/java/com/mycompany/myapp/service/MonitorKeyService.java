package com.mycompany.myapp.service;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.MonitorKey;
import com.mycompany.myapp.domain.PepProfile;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.MonitorKeyDTO;
import com.mycompany.myapp.mapper.MonitorKeyMapper;
import com.mycompany.myapp.repository.MonitorKeyRepository;
import com.mycompany.myapp.repository.PepProfileRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class MonitorKeyService {

    @Autowired
    private MonitorKeyRepository monitorKeyRepository;

    @Autowired
    private MonitorKeyMapper monitorKeyMapper;

    @Autowired
    private PepProfileRepository pepProfileRepository;
    
    private static final String UPDATE = "update";
    
    private static final String DELETE = "delete";

    public void deleteMonitorKey(Long id) {
        Integer domainId = OCSUtils.getDomain();
        MonitorKey currentMonitorKey = monitorKeyRepository.findByIdAndDomainId(id, domainId);
        checkMonitorKeyUsedByPepProfile(currentMonitorKey, domainId, DELETE);
        monitorKeyRepository.deleteByIdAndDomainId(id, domainId);
    }

    public Page<MonitorKeyDTO> getMonitoKeys(String monitorKey, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<MonitorKeyDTO> monitorkeys = monitorKeyRepository
                .findMonitorKeys(domainId, monitorKey, description, pageable).map(monitorKeyMapper::toDto);
        return monitorkeys;
    }

    public Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getMonitorKeys(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC, MonitorKey.FieldNames.MONITOR_KEY_ID) : sort);
    }

    public BaseResponseDTO createMonitorKey(MonitorKeyDTO monitorKeyDTO) {
        Integer domainId = OCSUtils.getDomain();
        //check duplicate monitor_key
        MonitorKey monitorKeys = monitorKeyRepository.findByMonitorKeyAndDomainId(monitorKeyDTO.getMonitorKey(), domainId);
        if(monitorKeys != null) {
            throw new ResourceNotFoundException(ErrorMessage.MonitorKey.INVALID_MONITOR_KEY, Resources.MONITOR_KEY, ErrorKey.MonitorKey.MONITOR_KEY);
        }
        return saveMoniterKey(monitorKeyDTO);
    }

    public BaseResponseDTO updateMonitorKey(MonitorKeyDTO monitorKeyDTO, Long monitorKeyId) {

        Integer domainId = OCSUtils.getDomain();
        MonitorKey monitorKey = monitorKeyRepository.findByIdAndDomainId(monitorKeyId, domainId);

        //check monitor key muon update co ton tai khong
        if (monitorKey == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.SERVICE, ErrorKey.Service.ID);
        }
        if(!monitorKey.getMonitorKey().equals(monitorKeyDTO.getMonitorKey())) {
            checkMonitorKeyUsedByPepProfile(monitorKey, domainId, "update");
        }
        //check duplicate monitor_key
        MonitorKey monitorKeys = monitorKeyRepository.findByMonitorKeyAndDomainId(monitorKeyDTO.getMonitorKey(), domainId);
        if(monitorKeys != null && !monitorKeys.getId().equals(monitorKeyId)) {
            throw new ResourceNotFoundException(ErrorMessage.MonitorKey.INVALID_MONITOR_KEY, Resources.MONITOR_KEY, ErrorKey.MonitorKey.MONITOR_KEY);
        }
        return saveMoniterKey(monitorKeyDTO);
    }

    private MonitorKeyDTO saveMoniterKey(MonitorKeyDTO monitorKeyDTO) {
        Integer domainId = OCSUtils.getDomain();
        
        MonitorKey monitorKey = monitorKeyMapper.toEntity(monitorKeyDTO);
        monitorKey.setDomainId(domainId);
        monitorKey = monitorKeyRepository.save(monitorKey);
        return monitorKeyMapper.toDto(monitorKey);
    }

    public MonitorKeyDTO getMonitorDetail(Long monitorKeyId) {
        Integer domainId = OCSUtils.getDomain();
        MonitorKey monitorKey = monitorKeyRepository.findByIdAndDomainId(monitorKeyId, domainId);
        if(monitorKey == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.MONITOR_KEY, ErrorKey.MonitorKey.ID);
        }
        MonitorKeyDTO monitorKeyDTO = new MonitorKeyDTO();
        monitorKeyDTO.setDescription(monitorKey.getRemark());
        monitorKeyDTO.setId(monitorKeyId);
        monitorKeyDTO.setMonitorKey(monitorKey.getMonitorKey());
        return monitorKeyDTO;
    }

    public List<MonitorKeyDTO> getAllMonitorKey() {
        List<MonitorKeyDTO> list = monitorKeyMapper.toDto(monitorKeyRepository.findByDomainId(OCSUtils.getDomain()));
        return list;
    }
    
    private void checkMonitorKeyUsedByPepProfile(MonitorKey monitorKey, Integer domainId, String action) {
        List<PepProfile> listPepProfileUseQos = pepProfileRepository.findByDomainIdAndMonitorKey(domainId, monitorKey.getMonitorKey());
        if(!CollectionUtils.isEmpty(listPepProfileUseQos)) {
            if(UPDATE.equals(action)) {
                throw new DataConstrainException(ErrorMessage.MonitorKey.CANNOT_UPDATE_ASSOCIATION, Resources.MONITOR_KEY, ErrorKey.MonitorKey.MONITOR_KEY);
            }else {
                throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.MONITOR_KEY, ErrorKey.MonitorKey.MONITOR_KEY);
            }
        }
    }
}
