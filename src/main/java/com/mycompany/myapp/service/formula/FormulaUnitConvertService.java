package com.mycompany.myapp.service.formula;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.formula.FormulaUnitConvert;
import com.mycompany.myapp.repository.formula.FormulaUnitConvertRepository;
import com.mycompany.myapp.utils.OCSUtils;

@Service
public class FormulaUnitConvertService {
    
    @Autowired
    private FormulaUnitConvertRepository formulaUnitConvertRepository;
    
    public List<FormulaUnitConvert> getAll() {
        Integer domainId = OCSUtils.getDomain();
        return formulaUnitConvertRepository.findByDomainId(domainId);
    }
}
