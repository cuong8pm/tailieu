package com.mycompany.myapp.service.formula;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.formula.FormulaErrorCode;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.formula.FormulaErrorCodeDTO;
import com.mycompany.myapp.mapper.formula.FormulaErrorCodeMapper;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.formula.FormulaErrorCodeRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
public class FormulaErrorCodeService {
    
    @Autowired
    private FormulaErrorCodeRepository formulaErrorCodeRepository;
    
    @Autowired
    private ReferTableRepository referTableRepository;
    
    @Autowired
    private FormulaErrorCodeMapper formulaErrorCodeMapper;
    
    public List<FormulaErrorCode> getAll() {
        Integer domainId = OCSUtils.getDomain();
        List<FormulaErrorCode> list = formulaErrorCodeRepository.findByDomainIdOrderByErrorCodeAsc(domainId);
        for (FormulaErrorCode item : list) {
            item.setName(item.getErrorCode() + "-" + item.getName());
        }
        return list;
    }

    public BaseResponseDTO createNewFormulaErrorCode(FormulaErrorCodeDTO dto) {
        Integer domainId = OCSUtils.getDomain();
        checkDuplicateName(dto.getName(), domainId);
        checkReferTableExist(dto.getType());
        checkDuplicateErrorCode(dto.getErrorCode(), domainId);
        FormulaErrorCode formulaErrorCode = formulaErrorCodeMapper.toEntity(dto);
        formulaErrorCode.setDomainId(domainId);
        formulaErrorCode = formulaErrorCodeRepository.save(formulaErrorCode);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(formulaErrorCode, response);
        return response;
    }

    private void checkDuplicateErrorCode(Long errorCode, Integer domainId) {
        FormulaErrorCode formulaErrorCode = formulaErrorCodeRepository.findByErrorCodeAndDomainId(errorCode, domainId);
        if (formulaErrorCode != null) {
            throw new DataConstrainException(ErrorMessage.FormulaErrorCode.DUPLICATED_ERROR_CODE, Resources.FORMULA_ERROR_CODE, ErrorKey.FormulaErrorCode.ERROR_CODE);
        }
    }

    private void checkDuplicateName(String name, Integer domainId) {
        FormulaErrorCode formulaErrorCode = formulaErrorCodeRepository.findByNameAndDomainId(name, domainId);
        if (formulaErrorCode != null) {
            throw new DataConstrainException(ErrorMessage.FormulaErrorCode.DUPLICATED_NAME, Resources.FORMULA_ERROR_CODE, ErrorKey.NAME);
        }
    }
    
    private void checkReferTableExist(Integer type) {
        ReferTable referTable = referTableRepository.getReferTableByReferTypeAndValue(ReferType.FormulaErrorCodeType,
                type);
        if (referTable == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReferTable.NOT_FOUND, Resources.FORMULA_ERROR_CODE, ErrorKey.FormulaErrorCode.TYPE);
        }
    }
}
