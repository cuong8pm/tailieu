package com.mycompany.myapp.service.formula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.formula.Formula;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.repository.formula.FormulaRepository;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.CloneNameTooLongException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Transactional
@Service
public class FormulaService extends AbstractCloneService {

    @Autowired
    private FormulaRepository formulaRepository;

    public void deleteFormula(Long id) {
        Integer domainId = OCSUtils.getDomain();
        formulaRepository.deleteByIdAndDomainId(id, domainId);
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = formulaRepository;
        this.ocsCloneableRepository = formulaRepository;
    }

    @Override
    public void checkDuplicateName(String name, Long id) {
        // Do not check duplicate name for formula
    }

    @Override
    protected void setPosIndex(Integer domainId, OCSMoveableEntity ocsCloneableEntity, Long newParentId) {
        // Do not set posIndex for formula
    }


    @Override
    protected Integer getCloneNameMaxLength() {
        return Formula.Length.NAME;
    }

    @Override
    public TreeClone getTreeDTO() {
        // Formula does not have tree
        return null;
    }

    @Override
    protected CategoryType getCategoryType() {
        // Formula does not have tree
        return null;
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList();
    }

    @Override
    protected String getResourceName() {
        return Resources.FORMULA;
    }

    @Override
    protected String getNameCategory() {
        return "Formulas";
    }

    @Override
    public OCSCloneableEntity saveEntity(CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> baseToCloneMap) {
        if (ocsCloneableRepository == null) {
            throw new UnsupportedOperationException("Method is not ready");
        }
        if (cloneDTO.getChildren() == null) {
            cloneDTO.setChildren(new ArrayList<CloneDTO>());
        }
        Integer domainId = OCSUtils.getDomain();
        Long id = cloneDTO.getId();

        // check cloned data in cache
//        if (cacheLocal.get() == null) {
//            cacheLocal.set(new HashMap<>());
//        }
//        if (cacheLocal.get().get(getCacheKey(id)) != null) {
//            return cacheLocal.get().get(getCacheKey(id));
//        }

        OCSCloneableEntity baseToClone = baseToCloneMap.get(id);

        if (baseToClone == null) {
            return null;
        }

        OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
        String newName = "";

        setPosIndex(domainId, clonedEntity, clonedEntity.getParentId());

        clonedEntity = setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);
        clonedEntity = ocsCloneableRepository.saveAndFlush(clonedEntity);
        // put to cache
//        cacheLocal.get().put(getCacheKey(id), clonedEntity);
        return clonedEntity;
    }
}
