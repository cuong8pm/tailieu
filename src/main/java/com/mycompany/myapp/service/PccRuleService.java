package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.PccRule;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.PccRuleDTO;
import com.mycompany.myapp.mapper.PccRuleMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.PccRuleRepository;
import com.mycompany.myapp.repository.PepProfilePccRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class PccRuleService extends OCSBaseService {

    @Autowired
    private PccRuleRepository pccRuleRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private PccRuleMapper pccRuleMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PepProfilePccRepository pepProfilePccRepository;

    @Override
    @PostConstruct
    public void setBaseRepository() {
        this.baseRepository = this.pccRuleRepository;
        this.parentService = this.categoryService;
    }

    public Page<PccRuleDTO> getPccRules(Long categoryId, String name, String description, String id, String priority, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Page<PccRuleDTO> pccRuleDTOs = pccRuleRepository.findPccRules(categoryId, domainId, name, description, id, priority,  pageable).map(pccRuleMapper::toDto);
        pccRuleDTOs.forEach(s -> s.setCategoryLevel(parentCategoryLevel + 1) );
        return pccRuleDTOs;
    }

    public PccRuleDTO getPccRuleDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        PccRule pccRule = pccRuleRepository.findByIdAndDomainId(id, domainId);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(pccRule.getParentId(), domainId);
        PccRuleDTO pccRuleDTO = pccRuleMapper.toDto(pccRule);
        if (category != null) {
            pccRuleDTO.setCategoryId(category.getId());
            pccRuleDTO.setCategoryName(category.getName());
            pccRuleDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        return pccRuleDTO;
    }

    public PccRuleDTO createPccRule(PccRuleDTO pccRuleDTO) {
        Integer domainId = OCSUtils.getDomain();

        if (pccRuleDTO.getExpireDate() != null && pccRuleDTO.getExpireDate().compareTo(pccRuleDTO.getEffectDate()) < 0) {
            throw new DataConstrainException(ErrorMessage.PccRule.EFFECT_DATE_LESS_THAN_EXPIRE_DATE, Resources.PCC_RULE, ErrorKey.PccRule.EFFECT_DATE);
        }
        PccRule pccRuleItem = pccRuleMapper.toEntity(pccRuleDTO);
        checkCategoryExist(pccRuleDTO, domainId);
        checkDuplicateName(pccRuleDTO.getName(), pccRuleDTO.getId());

        pccRuleItem.setDomainId(domainId);
        super.setPosIndex(domainId, pccRuleItem, pccRuleItem.getCategoryId());

        pccRuleItem = pccRuleRepository.save(pccRuleItem);

        return pccRuleMapper.toDto(pccRuleItem);
    }

    private void checkCategoryExist(PccRuleDTO PccRuleDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(PccRuleDTO.getCategoryId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void deletePccRule(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Integer countMapping = pepProfilePccRepository.countByPccRuleIdAndDomainId(id, domainId);
        if (countMapping > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.PCC_RULE, ErrorKey.PccRule.ID);
        }
        pccRuleRepository.deleteByIdAndDomainId(id, domainId);
    }

    public BaseResponseDTO updatePccRule(PccRuleDTO pccRuleDTO) {
        Integer domainId = OCSUtils.getDomain();
        PccRule pccRuleItemDB = pccRuleRepository.findByIdAndDomainIdForUpdate(pccRuleDTO.getId(), domainId);

        if (pccRuleItemDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.PccRule.NOT_FOUND, Resources.PCC_RULE, ErrorKey.PccRule.ID);
        }

        if (pccRuleDTO.getExpireDate() != null && pccRuleDTO.getExpireDate().compareTo(pccRuleDTO.getEffectDate()) < 0) {
            throw new DataConstrainException(ErrorMessage.PccRule.EFFECT_DATE_LESS_THAN_EXPIRE_DATE, Resources.PCC_RULE, ErrorKey.PccRule.EFFECT_DATE);
        }

        checkCategoryExist(pccRuleDTO, domainId);
        checkDuplicateName(pccRuleDTO.getName(), pccRuleDTO.getId());

        PccRule pccRuleItem = pccRuleMapper.toEntity(pccRuleDTO);
        if (!Objects.equal(pccRuleItemDB.getCategoryId(), pccRuleDTO.getCategoryId())) {
            setPosIndex(domainId, pccRuleItem, pccRuleDTO.getCategoryId());
        }
        pccRuleItem.setDomainId(domainId);
        pccRuleItem = pccRuleRepository.save(pccRuleItem);
        return pccRuleMapper.toDto(pccRuleItem);
    }

    public List<Long> getRemoveIds(Long pepProfileId) {
        List<Long> removeIds = new ArrayList<>();
        removeIds = pepProfilePccRepository.getPccRuleIdByPepProfileId(pepProfileId);
        return removeIds;
    }
    
    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.PCC_RULE;
    }

}
