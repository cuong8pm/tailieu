package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.constant.StartType;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.param.DateNormParamDTO;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service("dateNormalizerService")
public class DateNormalizerService extends NormalizerAbstractService {

    public void validateDateTimeParams(List<NormParamDTO> params, NormalizerType normalizerType, Long normalizerId) {
        //List<DateNormParamDTO> params = Arrays.asList(jacksonObjectMapper.convertValue(normParams, DateNormParamDTO[].class));
        validateParamByAnnotation(params);

        int index = 0;
        for (NormParamDTO param_ : params) {
            DateNormParamDTO param = (DateNormParamDTO)param_;
            checkValueOfTypeDelta(param, param.getStartType());
            checkValueOfTypeDelta(param, param.getEndType());

            compareStartAndEndValue(param, index++);
        }
    }

    private void compareStartAndEndValue(DateNormParamDTO param, int index) {
        if (param.getStartType().equals(param.getEndType()) && StartType.of(param.getStartType()).equals(StartType.DELTA)
            && Long.parseLong(param.getStartValue()) >= Long.parseLong(param.getEndValue())
        ) {
            throw new DataInvalidException(
                ErrorMessage.NormalizerParam.END_VALUE_GREAT_THAN_START_VALUE,
                Resources.NORMALIZER,
                ErrorKey.NormalizerParam.PARAMETER_VALUE
            );
        }

        if (param.getActionType() != null
            && !param.getActionType().equals(ActionType.OLD)
            && param.getStartType().equals(param.getEndType())
            && StartType.of(param.getStartType()).equals(StartType.NONE)) {
            LocalDateTime startValue = LocalDateTime.parse(param.getStartValue(), DateTimeFormatter.ofPattern("yyyy"
                + "/MM/dd/HH/mm/ss"));
            LocalDateTime endValue = LocalDateTime.parse(param.getEndValue(), DateTimeFormatter.ofPattern("yyyy/MM"
                + "/dd/HH/mm/ss"));
            if (startValue.isAfter(endValue)||startValue.isEqual(endValue)) {
                throw new DataInvalidException(
                    ErrorMessage.NormalizerParam.END_VALUE_GREAT_THAN_START_VALUE,
                    Resources.NORMALIZER,
                    ErrorKey.NormalizerParam.PARAMETER_VALUE
                );
            }
        }

        if (param.getStartType().equals(param.getEndType()) && StartType.of(param.getStartType()).equals(StartType.CURRENT_TIME)) {
            throw new DataInvalidException(ErrorMessage.NormalizerParam.CAN_NOT_SAME_CURRENT_TIME, Resources.NORMALIZER,
                ErrorKey.NormalizerParam.PARAMETER_VALUE);
        }

        if (StartType.of(param.getStartType()).equals(StartType.DELTA) && !StartType.of(param.getEndType()).equals(StartType.DELTA)
            && Long.parseLong(param.getStartValue()) <= 0) {
            throw new DataInvalidException(ErrorMessage.NormalizerParam.START_VALUE_MUST_BE_GREATER_THAN_0, Resources.NORMALIZER,
                ErrorKey.NormalizerParam.START_VALUE);
        }

        if (StartType.of(param.getEndType()).equals(StartType.DELTA) && !StartType.of(param.getStartType()).equals(StartType.DELTA)
            && Long.parseLong(param.getEndValue()) <= 0) {
            throw new DataInvalidException(ErrorMessage.NormalizerParam.END_VALUE_MUST_BE_GREATER_THAN_0, Resources.NORMALIZER,
                ErrorKey.NormalizerParam.END_VALUE);
        }
    }

    private void checkValueOfTypeDelta(DateNormParamDTO param, Integer type) {
        if (StartType.of(type).equals(StartType.DELTA) && OCSUtils.isNumeric(param.getStartValue())) {
            if (Long.parseLong(param.getStartValue()) > Integer.MAX_VALUE){
                throw new DataInvalidException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_TOO_BIG, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
            }
        }
    }
}
