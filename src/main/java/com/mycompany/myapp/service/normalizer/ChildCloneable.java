package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface ChildCloneable {
    OCSCloneableRepository getOCSCloneableRepository();
}
