package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.dto.normailizer.NormValueDTO;
import com.mycompany.myapp.mapper.normalizer.NormValueMapper;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.normalizer.NormValueRepository;
import com.mycompany.myapp.repository.normalizer.NormalizerRepository;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceConflictException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class NormValueService extends OCSBaseService implements NormChildCloneable {
    @Autowired
    private NormValueRepository normValueRepository;

    @Autowired
    private NormValueMapper normValueMapper;

    @Autowired
    private NormalizerRepository normalizerRepository;

    public List<NormalizerValue> saveListNormValue(List<NormalizerValue> normValues) {
        Integer domainId = OCSUtils.getDomain();

        for (NormalizerValue normValue: normValues) {
            normValue.setDomainId(domainId);
        }

        normValueRepository.saveAll(normValues);

        return normValues;
    }

    public List<NormValueDTO> getAllByNormalizerId(Long normalizerId) {
        Integer domainId = OCSUtils.getDomain();
        List<NormalizerValue> normalizerValues = normValueRepository.findAllByNormalizerId(normalizerId, domainId);
        if (!CollectionUtils.isEmpty(normalizerValues)) {
            Long defaultValue = normalizerRepository.findDefaultValueByIdAndDomainId(normalizerId, domainId);
            if(defaultValue != null) {
                normalizerValues.forEach(norm -> {
                    if (norm.getValueId().equals(defaultValue)) {
                        norm.setDefault(true);
                    } else {
                        norm.setDefault(false);
                    }
                });
            }
        }
        return normalizerValues.stream().map(normValueMapper::toDto).collect(Collectors.toList());
    }

    public void delete(List<NormValueDTO> normValues) {
        Integer domainId = OCSUtils.getDomain();
        if (!CollectionUtils.isEmpty(normValues)) {
            List<Long> idToDels = normValues.stream().map(NormValueDTO::getId).distinct().collect(Collectors.toList());
            List<NormalizerValue> normValueToDel = normValueRepository.findByDomainIdAndIdIn(domainId, idToDels);
            if (idToDels.size() != normValueToDel.size()) {
                throw new ResourceConflictException(ErrorMessage.Normalizer.VALUE_NOT_EXIST, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
            }
            // delete normalizer param
            normValueRepository.deleteByDomainIdAndIdIn(domainId, idToDels);
        }
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = normValueRepository;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return normValueRepository;
    }

    public void deleteByNormalizerId(Integer domainId, Long id) {
        normValueRepository.deleteByDomainIdAndNormalizerId(domainId, id);
    }

    public NormalizerValue checkNormValueExist(Long normValueId) {
        Integer domainId = OCSUtils.getDomain();
        NormalizerValue normalizerValue = normValueRepository.findByIdAndDomainId(normValueId, domainId);
        if (normalizerValue == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
        }
        return normalizerValue;
    }

    public Long getMaxValueIdInNormalizer(Long normalizerId){
        Integer domainId = OCSUtils.getDomain();
        Long maxValue = normValueRepository.getMaxValueIdInNormalizer(domainId, normalizerId);
        if (maxValue == null) {
            maxValue = -1L;
        }
        return maxValue;
    }
    
    @Override
    protected CategoryType getCategoryType() {
        return null;
    }
}
