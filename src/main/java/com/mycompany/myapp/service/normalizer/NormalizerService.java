package com.mycompany.myapp.service.normalizer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.service.clone.OCSCloneService;

public interface NormalizerService extends OCSCloneService {
    Page<NormalizerDTO> getNormalizerForm(SearchCriteria searchCriteria, Pageable pageable);

    NormalizerDTO getNormalizerDetail(Long id);

    BaseResponseDTO move(List<? extends Moveable> moveables);

    List<Tree> searchInTree(String name, CategoryType categoryType);

    BaseResponseDTO createNormalizer(NormalizerDTO normalizerDTO);

    BaseResponseDTO updateNormalizer(NormalizerDTO normalizerForm);

    OCSCloneableEntity cloneEntity(Long id, int levelCount, int deep, String name);

    void deleteNormalizer(Long id);

    void moveCategory(Long entityId, Long categoryId);
}
