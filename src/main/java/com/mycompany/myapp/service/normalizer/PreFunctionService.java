package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.domain.normalizer.PreFunction;
import com.mycompany.myapp.dto.normailizer.validator.PreFunctionDTO;
import com.mycompany.myapp.mapper.PreFunctionMapper;
import com.mycompany.myapp.repository.normalizer.PreFunctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class PreFunctionService {
    @Autowired
    private PreFunctionRepository preFunctionRepository;

    @Autowired
    private PreFunctionMapper preFunctionMapper;

    public List<PreFunctionDTO> getAll() {
       return preFunctionRepository.findAll()
           .stream().sorted(Comparator.comparing(PreFunction::getFunctionName))
           .map(preFunctionMapper::toDto).collect(Collectors.toList());
    }
}
