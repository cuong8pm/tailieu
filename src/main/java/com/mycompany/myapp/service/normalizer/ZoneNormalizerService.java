package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.domain.GeoHomeZone;
import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.domain.ZoneMap;
import com.mycompany.myapp.domain.constant.DropdownType;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.NormValueDTO;
import com.mycompany.myapp.dto.normailizer.param.ZoneParamDTO;
import com.mycompany.myapp.repository.GeoHomeZoneRepository;
import com.mycompany.myapp.repository.ZoneMapRepository;
import com.mycompany.myapp.repository.ZoneRepository;
import com.mycompany.myapp.service.GeoHomeZoneService;
import com.mycompany.myapp.service.ZoneMapService;
import com.mycompany.myapp.service.ZoneService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("zoneNormalizerService")
public class ZoneNormalizerService extends NormalizerAbstractService {

    @Autowired
    private GeoHomeZoneRepository geoHomeZoneRepository;

    @Autowired
    private ZoneMapRepository zoneMapRepository;

    @Autowired
    private ZoneRepository zoneRepository;


    @Override
    public void validateNormParams(List<NormParamDTO> params, NormalizerType normalizerType, Long normalizerId) {
        //List<ZoneParamDTO> params = Arrays.asList(jacksonObjectMapper.convertValue(normParams, ZoneParamDTO[].class));
        Integer domainId = OCSUtils.getDomain();
        validateParamByAnnotation(params);
        List<NormValueDTO> normValueDTOs = normValueService.getAllByNormalizerId(normalizerId);
        for (NormParamDTO param_ : params) {
            ZoneParamDTO param = (ZoneParamDTO) param_;
            DropdownType.ZoneDataType zoneDataType = DropdownType.ZoneDataType.of(param.getZoneValueTypeId());
            switch (zoneDataType) {
                case ZONE:
                    Zone zone = zoneRepository.findByIdAndDomainId(param.getZoneValueId(), domainId);
                    if (zone == null) {
                        throw new ResourceNotFoundException(ErrorMessage.Zone.NOT_FOUND, Resources.ZONE, ErrorKey.ENTITY_NOT_FOUND);
                    }
                    break;
                case ZONE_MAP:
                    ZoneMap zoneMap = zoneMapRepository.findByIdAndDomainId(param.getZoneValueId(), domainId);
                    if (zoneMap == null) {
                        throw new ResourceNotFoundException(ErrorMessage.ZoneMap.NOT_FOUND, Resources.ZONE_MAP, ErrorKey.ENTITY_NOT_FOUND);
                    }
                    break;
                case GEO_HOME_ZONE:
                    GeoHomeZone geoHomeZone = geoHomeZoneRepository.findByIdAndDomainId(param.getZoneValueId(), domainId);
                    if (geoHomeZone == null) {
                        throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE, ErrorKey.ENTITY_NOT_FOUND);
                    }
                    break;
                default:
                    throw new DataInvalidException("zone_data_type_id_invalid", Resources.NORMALIZER, "zone_value_type_id");
            }

            if (param.getActionType() != null && ActionType.EDIT.equals(param.getActionType())) {
                normParamService.checkNormParamsExist(param.getId());
            } else if (param.getActionType() != null && ActionType.ADD.equals(param.getActionType())) {
                param.setId(null);
            }

            Optional<NormValueDTO> existValue = normValueDTOs.stream().filter(normValue -> param.getValueId().equals(normValue.getValueId())).findFirst();
            if (existValue.isEmpty()) {
                throw new DataInvalidException(ErrorMessage.NormalizerValue.NOT_FOUND, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
            }
            existValue.get().setHasParams(true);
        }

        //kiểm tra 1 value nếu không là default hoặc thuộc TimeNormalizer đã có params chưa
        normValueDTOs.forEach(normValue -> {
            if (!normValue.isHasParams() && !normValue.isDefault()) {
                throw new DataInvalidException(ErrorMessage.NormalizerValue.VALUE_MUST_SET_FOR_PARAM, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
            }
        });
    }
}
