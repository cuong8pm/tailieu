package com.mycompany.myapp.service.normalizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.service.exception.*;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.NestedObject;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.Constants;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.domain.normalizer.PreFunction;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.NormValueDTO;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.dto.normailizer.SpecialFieldDTO;
import com.mycompany.myapp.dto.normailizer.param.DateNormParamDTO;
import com.mycompany.myapp.dto.normailizer.param.NormalParamDTO;
import com.mycompany.myapp.dto.normailizer.param.StartEndParamDTO;
import com.mycompany.myapp.dto.normailizer.param.TimeNormParamDTO;
import com.mycompany.myapp.dto.normailizer.param.ValidateDTO;
import com.mycompany.myapp.dto.normailizer.param.ZoneParamDTO;
import com.mycompany.myapp.dto.normailizer.validator.FilterDTO;
import com.mycompany.myapp.dto.normailizer.validator.NormFilterValidator;
import com.mycompany.myapp.dto.normailizer.validator.NormInputValidator;
import com.mycompany.myapp.dto.normailizer.validator.NormalizerValidator;
import com.mycompany.myapp.dto.normailizer.validator.PreFunctionDTO;
import com.mycompany.myapp.mapper.normalizer.NormValueMapper;
import com.mycompany.myapp.mapper.normalizer.NormalizerMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.normalizer.NestedObjectClassRepository;
import com.mycompany.myapp.repository.normalizer.NestedObjectRepository;
import com.mycompany.myapp.repository.normalizer.NormParamRepository;
import com.mycompany.myapp.repository.normalizer.NormValueRepository;
import com.mycompany.myapp.repository.normalizer.NormalizerRepository;
import com.mycompany.myapp.repository.normalizer.PreFunctionRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.characteristic.CharacteristicService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.ratetable.RateTableColumnService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Transactional
public abstract class NormalizerAbstractService extends AbstractCloneService implements NormalizerService {
    private static final String OBJECT_NAME ="normalizer";

    //<editor-fold desc="Khai báo biến">
    protected NormalizerRepository normalizerRepository;

    protected NormalizerMapper normalizerMapper;

    protected NormValueMapper normValueMapper;

    protected NormParamService normParamService;
    @Autowired
    protected NormValueService normValueService;

    protected ObjectMapper jacksonObjectMapper;

    protected NestedObjectRepository nestedObjectRepository;

    protected PreFunctionRepository preFunctionRepository;

    protected NestedObjectClassRepository nestedObjectClassRepository;

    protected CategoryRepository categoryRepository;

    protected CategoryService categoryService;

    protected CharacteristicService characteristicService;

    @Autowired
    protected NormValueRepository normValueRepository;

    @Autowired
    protected NormParamRepository normParamRepository;

    @Autowired
    protected List<NormChildCloneable> childCloneables;

    @Autowired
    protected List<RateTableColumnService> rateTableColumnService;

    protected String regexSpecialField = "compareType:\\d";

    protected Pattern patternSpecialField = Pattern.compile(regexSpecialField);
    //</editor-fold>

    //<editor-fold desc="Static var">
    protected static final String BEGIN_FILTER = "\\{";
    protected static final String END_FILTER = "}";
    protected static final String PATH_SPLITTER = "\\.";
    protected static final String BEGIN_FUNCTION = "\\(";
    protected static final String END_FUNCTION = ")";
    protected static final String FILTER_FUNCTION_SPLITTER = ";";
    protected static final String FILTER_SPLITTER = "&";
    protected static final String FILTER_KEY_VALUE = "=";
    protected static final String PRE_FUNCTION_SPLITTER = ":";
    protected static final String PARAMETER_SPLITTER = ",";
    //</editor-fold>

    //<editor-fold desc="Autowired region">

    @Autowired
    public final void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Autowired
    public final void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public final void setNormParamService(NormParamService normParamService) {
        this.normParamService = normParamService;
    }


//    public void setNormValueService(NormValueService normValueService) {
//        this.normValueService = normValueService;
//    }

    @Autowired
    public final void setNormalizerRepository(NormalizerRepository normalizerRepository) {
        this.normalizerRepository = normalizerRepository;
    }

    @Autowired
    public final void setNormalizerMapper(NormalizerMapper normalizerMapper) {
        this.normalizerMapper = normalizerMapper;
    }

    @Autowired
    public final void setNormValueMapper(NormValueMapper normValueMapper) {
        this.normValueMapper = normValueMapper;
    }

    @Autowired
    public final void setJacksonObjectMapper(ObjectMapper jacksonObjectMapper) {
        this.jacksonObjectMapper = jacksonObjectMapper;
    }

    @Autowired
    public final void setPreFunctionRepository(PreFunctionRepository preFunctionRepository) {
        this.preFunctionRepository = preFunctionRepository;
    }

    @Autowired
    public final void setNestedObjectRepository(NestedObjectRepository nestedObjectRepository) {
        this.nestedObjectRepository = nestedObjectRepository;
    }

    @Autowired
    public final void setNestedObjectClassRepository(NestedObjectClassRepository nestedObjectClassRepository) {
        this.nestedObjectClassRepository = nestedObjectClassRepository;
    }

    @Autowired
    public final void setCharacteristicService(CharacteristicService characteristicService) {
        this.characteristicService = characteristicService;
    }
    //</editor-fold>

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = this.normalizerRepository;
        this.parentService = this.categoryService;

        this.ocsCloneableRepository = normalizerRepository;
        this.childServices = childCloneables;

        this.parentDependMappingServices = rateTableColumnService;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return Normalizer.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.NORMALIZER;
    }

    public NormalizerDTO createNormalizer(NormalizerDTO normalizerForm) {
        Integer domainId = OCSUtils.getDomain();
        Normalizer normalizer = normalizerMapper.toEntity(normalizerForm);
        normalizer.setId(null);
        normalizer.setDomainId(domainId);
        checkCategory(normalizerForm.getCategoryId());
        if (!StringUtils.isEmpty(normalizerForm.getInputField2())) {
            String inputField = normalizer.getInputField();
            normalizer.setInputField(inputField + "/" + normalizerForm.getInputField2());
        }

        checkDuplicateName(normalizerForm.getName().trim(), normalizerForm.getId());
        validateInputFields(normalizer.getInputField());

        validateSpecialFields(normalizerForm.getSpecialFieldDTO(), normalizerForm.getTypeId());
        if (normalizerForm.getSpecialFieldDTO() != null) {
            String specialField = normalizerForm.getSpecialFieldDTO().toSpecialString(normalizerForm.getTypeId());
            normalizer.setSpecialField(specialField);
        }

        setPosIndex(domainId, normalizer, normalizerForm.getCategoryId());
        normalizer = normalizerRepository.save(normalizer);

        List<NormValueDTO> normValueDTOs = normalizerForm.getValues();

        validateValues(normValueDTOs, normalizerForm.getTypeId(), normalizerForm.getId());

        List<NormalizerValue> normValues = normValueMapper.toEntity(normValueDTOs);
        for (NormalizerValue normalizerValue : normValues) {
            normalizerValue.setNormalizerId(normalizer.getId());
            normalizerValue.setDomainId(domainId);
        }

        Map<String, Long> oldNewKeyMaps = new HashMap<>();
        saveNormValues(normValues, oldNewKeyMaps);

        // lưu giá trị valueId default vào  normalizer
        Optional<NormValueDTO> normValueDTO = normalizerForm.getValues().stream().filter(NormValueDTO::isDefault).findFirst();
        if(normValueDTO.isPresent()) {
            normalizer.setDefaultValue(normValueDTO.get().getValueId());
        }
        normalizer = normalizerRepository.save(normalizer);

        List<Map<String, Object>> normParamDTOS = normalizerForm.getParameters();
        for (Map<String, Object> normParam : normParamDTOS) {
            Long newId = oldNewKeyMaps.get(normParam.get("valueId").toString());
            normParam.put("valueId", newId);
        }

        List<NormParamDTO> normParamInputs = convertListMapToListNormParam(normParamDTOS, normalizerForm.getTypeId());
        validateNormParams(normParamInputs, normalizerForm.getTypeId(), normalizer.getId());

        List<NormalizerParam> normParams = convertDtoToEntity(normParamInputs, normalizerForm.getTypeId());

        for (NormalizerParam normalizerParam : normParams) {
            normalizerParam.setDomainId(domainId);
            normalizerParam.setNormalizerId(normalizer.getId());
        }

        normParamService.saveNormParams(normParams);
        return normalizerMapper.toDto(normalizer);
    }

    public NormalizerDTO updateNormalizer(NormalizerDTO normalizerForm) {
        Normalizer oldNormalizer = checkNormalizerExist(normalizerForm.getId());
        Integer domainId = OCSUtils.getDomain();
        Normalizer normalizer = normalizerMapper.toEntity(normalizerForm);
        normalizer.setDomainId(domainId);

        if (!StringUtils.isEmpty(normalizerForm.getInputField2())) {
            String inputField = normalizer.getInputField();
            normalizer.setInputField(inputField + "/" + normalizerForm.getInputField2());
        }

        checkDuplicateName(normalizerForm.getName(), normalizerForm.getId());
        checkCategory(normalizerForm.getCategoryId());
        validateInputFields(normalizer.getInputField());

        validateSpecialFields(normalizerForm.getSpecialFieldDTO(), normalizerForm.getTypeId());
        if (normalizerForm.getSpecialFieldDTO() != null) {
            normalizer.setSpecialField(normalizerForm.getSpecialFieldDTO().toSpecialString(normalizerForm.getTypeId()));
        }

        if (!Objects.equals(normalizerForm.getCategoryId(), oldNormalizer.getCategoryId())) {
            setPosIndex(domainId, normalizer, normalizerForm.getCategoryId());
        } else {
            normalizer.setPosIndex(oldNormalizer.getPosIndex());
        }

        normalizer.setDefaultValue(oldNormalizer.getDefaultValue());

        Integer oldNormalizerType = oldNormalizer.getNormalizerType();

        normalizer = normalizerRepository.save(normalizer);

        //nếu thay đổi loại normalizer
        if(!oldNormalizerType.equals(normalizer.getNormalizerType())){
            //delete list of param
            normParamService.deleteByNormalizer(domainId, normalizer.getId());
            //delete list of value
            normValueService.deleteByNormalizerId(domainId, normalizer.getId());
        }

        List<NormValueDTO> valueInputs = normalizerForm.getValues();
        Map<String, Long> oldNewKeyMaps = new HashMap<>();
        //delete values
        List<NormValueDTO> valueToDelete = valueInputs.stream().filter(x -> x.getActionType() == ActionType.DELETE).collect(Collectors.toList());
        normValueService.delete(valueToDelete);
        valueInputs.removeAll(valueToDelete);

        //ket hop value da co trong db va list value truyen vao de validate
        List<Long> valuesIdToUpdates = valueInputs.stream()
            .filter(x -> x.getActionType().equals(ActionType.EDIT))
            .map(NormValueDTO::getId)
            .collect(Collectors.toList());
        List<NormValueDTO> valueOlds = normValueService.getAllByNormalizerId(normalizerForm.getId());

        //gán lại oldValueId cho NormValueUpdate
        valueInputs.forEach(newValue -> {
            Optional<NormValueDTO> oldValueUpdate =
                valueOlds.stream().filter(oldValue -> newValue.getActionType() == ActionType.EDIT && newValue.getId().equals(oldValue.getId())).findFirst();
            oldValueUpdate.ifPresent(normValueDTO -> newValue.setValueId(normValueDTO.getValueId()));
        });

        valueOlds.removeAll(valueOlds.stream().filter(x -> valuesIdToUpdates.contains(x.getId())).collect(Collectors.toList()));
        List<NormValueDTO> valueToValidates = new ArrayList<>();
        valueToValidates.addAll(valueInputs);
        valueToValidates.addAll(valueOlds);

        //validate norm values
        validateValues(valueToValidates, normalizerForm.getTypeId(), normalizerForm.getId());

        List<NormalizerValue> normValues = normValueMapper.toEntity(valueInputs);
        for (NormalizerValue normalizerValue : normValues) {
            normalizerValue.setNormalizerId(normalizer.getId());
            normalizerValue.setDomainId(domainId);
        }

        saveNormValues(normValues, oldNewKeyMaps);

        // lưu giá trị valueId default vào  normalizer
        Optional<NormValueDTO> normValueDTO = valueToValidates.stream().filter(NormValueDTO::isDefault).findFirst();
        if(normValueDTO.isPresent()) {
            normalizer.setDefaultValue(normValueDTO.get().getValueId());
        }
        normalizer = normalizerRepository.save(normalizer);

        List<Map<String, Object>> paramInputs = normalizerForm.getParameters();
        if (paramInputs == null) {
            paramInputs = new ArrayList<>();
        }
        for (Map<String, Object> normParam : paramInputs) {
            Long newId = oldNewKeyMaps.get(normParam.get("valueId"));
            if (newId != null) {
                normParam.put("valueId", newId);
            }
        }

        //delete param
        List<Map<String, Object>> paramToDelete = paramInputs.stream()
            .filter(x -> x.containsKey(Constants.ACTION_TYPE)
                && ActionType.of(x.get(Constants.ACTION_TYPE).toString()) == ActionType.DELETE)
            .collect(Collectors.toList());
        List<Long> paramIdToDels = new ArrayList<>();
        paramToDelete.forEach(x -> {
            if (x.containsKey("id")) {
                paramIdToDels.add(Long.parseLong(x.get("id").toString()));
            }
        });
        normParamService.deleteNormParams(paramIdToDels, normalizerForm.getId());
        paramInputs.removeAll(paramToDelete);

        List<? extends NormParamDTO> oldNormParam = normParamService.getAllByNormalizerId(normalizerForm.getId(), normalizerForm.getTypeId());
        List<NormParamDTO> inputNormParam = convertListMapToListNormParam(paramInputs, normalizerForm.getTypeId());
        List<NormParamDTO> listToValidate = new ArrayList<>();

        if (!CollectionUtils.isEmpty(inputNormParam)) {
            listToValidate.addAll(inputNormParam);

        }
        if (!CollectionUtils.isEmpty(oldNormParam)) {
            if (!CollectionUtils.isEmpty(inputNormParam)) {
                List<Long> listIdToUpdate = inputNormParam.stream()
                    .filter(x -> x.getActionType() != null && x.getActionType().equals(ActionType.EDIT)).
                        mapToLong(NormParamDTO::getId).boxed().collect(Collectors.toList());
                oldNormParam.removeAll(oldNormParam.stream().filter(x -> listIdToUpdate.contains(x.getId())).collect(Collectors.toList()));
            }
            listToValidate.addAll(oldNormParam);
        }

        validateNormParams(listToValidate, normalizerForm.getTypeId(), normalizer.getId());

        List<NormalizerParam> normParams = convertDtoToEntity(inputNormParam, normalizerForm.getTypeId());
        if (!CollectionUtils.isEmpty(normParams)) {
            for (NormalizerParam normalizerParam : normParams) {
                normalizerParam.setDomainId(domainId);
                normalizerParam.setNormalizerId(normalizer.getId());
            }

            normParamService.saveNormParams(normParams);
        }

        return normalizerMapper.toDto(normalizer);
    }

    private List<NormalizerParam> convertDtoToEntity(List<NormParamDTO> params, NormalizerType normalizerType) {
        if (CollectionUtils.isEmpty(params))
            return new ArrayList<>();
        List<NormalizerParam> paramsResult = new ArrayList<>();
        for (NormParamDTO param : params) {
            paramsResult.add(param.toEntity(normalizerType));
        }
        return paramsResult;
    }

    private List<NormParamDTO> convertListMapToListNormParam(List<Map<String, Object>> normParams, NormalizerType normalizerType) {
        switch (normalizerType) {
            case StringNormalizer:
            case NumberNormalizer:
            case NumberParameterNormalizer:
                return Arrays.asList(jacksonObjectMapper.convertValue(normParams, NormalParamDTO[].class));
            case QuantityNormalizer:
            case BalanceNormalizer:
            case AcmBalanceNormalizer:
                return Arrays.asList(jacksonObjectMapper.convertValue(normParams, StartEndParamDTO[].class));
            case DateNormalizer:
                return Arrays.asList(jacksonObjectMapper.convertValue(normParams, DateNormParamDTO[].class));
            case TimeNormalizer:
                return Arrays.asList(jacksonObjectMapper.convertValue(normParams, TimeNormParamDTO[].class));
            case ZoneNormalizer:
                return Arrays.asList(jacksonObjectMapper.convertValue(normParams, ZoneParamDTO[].class));
            default:
                return new ArrayList<>();
        }
    }

    public void validateNormParams(List<NormParamDTO> normParams, NormalizerType normalizerType, Long normalizerId) {
        if (!CollectionUtils.isEmpty(normParams)) {
            switch (normalizerType) {
                case StringNormalizer:
                case NumberNormalizer:
                case NumberParameterNormalizer:
                    validateNormNormalParams(normParams, normalizerType);
                    break;
                case QuantityNormalizer:
                case BalanceNormalizer:
                case AcmBalanceNormalizer:
                    validateNormStartEndParams(normParams, normalizerType);
                    break;
                case DateNormalizer:
                case TimeNormalizer:
                    validateDateTimeParams(normParams, normalizerType, normalizerId);
                    break;
                default:
                    break;
            }

            List<NormValueDTO> normValueDTOs = normValueService.getAllByNormalizerId(normalizerId);
            for (NormParamDTO param : normParams) {
                if (param.getActionType() != null && ActionType.EDIT.equals(param.getActionType())) {
                    normParamService.checkNormParamsExist(param.getId());
                } else if (param.getActionType() != null && ActionType.ADD.equals(param.getActionType())) {
                    param.setId(null);
                }

                Optional<NormValueDTO> existValue = normValueDTOs.stream().filter(normValue -> param.getValueId().equals(normValue.getValueId())).findFirst();
                if (existValue.isEmpty()) {
                    throw new DataInvalidException(ErrorMessage.NormalizerValue.NOT_FOUND, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
                }
                existValue.get().setHasParams(true);
            }

            //kiểm tra 1 value nếu không là default hoặc thuộc TimeNormalizer đã có params chưa
            normValueDTOs.forEach(normValue -> {
                if (!normValue.isHasParams() && (!normValue.isDefault() || normalizerType == NormalizerType.TimeNormalizer)) {
                    throw new DataInvalidException(ErrorMessage.NormalizerValue.VALUE_MUST_SET_FOR_PARAM, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
                }
            });
        } else {
            switch (normalizerType) {
                case StringMatchNormalizer:
                case CheckRegisterListNormalizer:
                case CheckInListNormalizer:
                    break;
                default:
                    throw new DataInvalidException(ErrorMessage.NormalizerParam.PARAMETERS_MUST_BE_NOT_NULL, Resources.NORMALIZER, ErrorKey.Normalizer.NORM_PARAMS);
            }
        }
    }

    protected void validateSpecialFields(SpecialFieldDTO specialField, NormalizerType normalizerType) {
        switch (normalizerType) {
            case StringMatchNormalizer:
                validateSpecialByAnnotation(specialField, SpecialFieldDTO.StringType.class);
                if (specialField.getEndCharacter() < specialField.getStartCharacter()) {
                    throw new DataInvalidException(ErrorMessage.Normalizer.END_CHARACTER_GREATER_THAN_START_CHARACTER, Resources.NORMALIZER, ErrorKey.NormSpecialField.START_CHARACTER);
                }
                break;
            case DateNormalizer:
            case QuantityNormalizer:
                validateSpecialByAnnotation(specialField, SpecialFieldDTO.AllType.class);
                break;
            case BalanceNormalizer:
            case AcmBalanceNormalizer:
                validateSpecialByAnnotation(specialField, SpecialFieldDTO.Balance.class);
                break;
            default:
                break;
        }
    }

    private void validateSpecialByAnnotation(SpecialFieldDTO specialFieldDTO, Class<?>... groups) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SpecialFieldDTO>> violations = validator.validate(specialFieldDTO, groups);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }
    }

    public void validateDateTimeParams(List<NormParamDTO> params, NormalizerType normalizerType, Long normalizerId){};

    private void validateInputFields(String inputFields) {
        NormalizerValidator normalizerValidator = new NormalizerValidator();

        String[] splittedInputFields = inputFields.split("/");
        NormInputValidator inputField1 = validateInputField(splittedInputFields[0]);
        normalizerValidator.setInputField1(inputField1);

        if (splittedInputFields.length > 1) {
            NormInputValidator inputField2 = validateInputField(splittedInputFields[1]);
            normalizerValidator.setInputField2(inputField2);
        }

        validateNormalizer(normalizerValidator);
    }

    private NormInputValidator validateInputField(String inputField) {
        String[] filterPaths = inputField.split(PATH_SPLITTER);
        List<NestedObject> paths = new ArrayList<>();

        Long parentId = null;
        int index = 0;
        NormInputValidator normInputValidator = new NormInputValidator();
        for (String filterPath : filterPaths) {
            normInputValidator.setPaths(filterPath);
            String path = filterPath;
            if (index++ == 0) {
                parentId = 100L;
            }

            if (filterPath.contains(BEGIN_FILTER) || filterPath.contains(END_FILTER)) {
                NormFilterValidator normFilterValidator = new NormFilterValidator();

                int beginFilter = filterPath.indexOf("{");
                int endFilter = filterPath.indexOf("}");

                if (beginFilter == -1 || endFilter == -1) {
                    throw new DataInvalidException(ErrorMessage.Normalizer.INVALID_INPUT_FIELD, Resources.NORMALIZER, ErrorKey.Normalizer.INPUT_FIELDS);
                }
                path = filterPath.split(BEGIN_FILTER)[0];
                NestedObject nestedObject = validatePath(path, parentId);
                parentId = nestedObject.getObjClassId();
                paths.add(nestedObject);
                String filterAndPreFunc = filterPath.split(BEGIN_FILTER)[1];
                // remove end of filter
                filterAndPreFunc = filterAndPreFunc.substring(0, filterAndPreFunc.length() - 1);

                String[] arrFilterAndPreFunc = filterAndPreFunc.split(FILTER_FUNCTION_SPLITTER);

                String filters = arrFilterAndPreFunc[0];
                String preFunction = (arrFilterAndPreFunc.length == 2) ? arrFilterAndPreFunc[1] : "";

                List<FilterDTO> filterDTOs = new ArrayList<>();
                if (!StringUtils.isEmpty(filters)) {
                    filterDTOs = toFilterDTOs(filters, parentId);
                }
                List<PreFunctionDTO> preFunctionDTOs = new ArrayList<>();
                if (!StringUtils.isEmpty(preFunction)) {
                    preFunctionDTOs = toPreFunctions(preFunction);
                }
                normFilterValidator.setFilters(filterDTOs);
                normFilterValidator.setPreFunctions(preFunctionDTOs);
            } else {
                NestedObject nestedObject = validatePath(path, parentId);
                parentId = nestedObject.getObjClassId();
            }

        }
        return normInputValidator;
    }

    private List<FilterDTO> toFilterDTOs(String input, Long parentId) {
        List<FilterDTO> results = new ArrayList<>();
        String[] filters = input.split(FILTER_SPLITTER);
        for (String filter : filters) {
            results.add(toFilterDTO(filter, parentId));
        }
        checkDuplicateFilterName(results);
        return results;
    }

    private void checkDuplicateFilterName(List<FilterDTO> filters) {
        List<String> filterNames = filters.stream().map(i -> i.getKey()).collect(Collectors.toList());
        for(String filterName : filterNames) {
            int count = Collections.frequency(filterNames, filterName);
            if (count >= 2) {
                throw new ResourceNotFoundException(ErrorMessage.Normalizer.DUPLICATE_FILTER_NAME, Resources.NESTED_OBJECT,
                    ErrorKey.Normalizer.INPUT_FIELDS);
            }
        }
    }

    private FilterDTO toFilterDTO(String input, Long parentId) {
        String[] keyValue = input.split(FILTER_KEY_VALUE);
        if (keyValue.length != 2) {
            throw new ResourceNotFoundException(ErrorMessage.Normalizer.FILTER_VALUE_NOT_EXIST, Resources.NESTED_OBJECT,
                ErrorKey.Normalizer.INPUT_FIELDS);
        }
        FilterDTO filterDTO = new FilterDTO(keyValue[0], keyValue[1].trim());
        validateFilter(filterDTO.getKey(), parentId);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<FilterDTO>> violations = validator.validate(filterDTO);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }

        return filterDTO;
    }

    private NestedObject validateFilter(String input, Long parentId) {
        Optional<NestedObject> nestedObjects = nestedObjectRepository.findByNameAndParentClassIdAndAlistIsFalse(input, parentId);
        if (!nestedObjects.isPresent()) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
        }
        return nestedObjects.get();
    }

    private NestedObject validatePath(String input, Long parentId) {
        Optional<NestedObject> nestedObjects = nestedObjectRepository.findByNameAndParentClassId(input, parentId);
        if (!nestedObjects.isPresent()) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
        }
        return nestedObjects.get();
    }

    private List<PreFunctionDTO> toPreFunctions(String input) {
        List<PreFunctionDTO> results = new ArrayList<>();
        String[] functions = input.split(PRE_FUNCTION_SPLITTER);
        for (String function : functions) {
            results.add(toPreFunction(function));
        }
        return results;
    }

    private PreFunctionDTO toPreFunction(String input) {
        String[] functionWithParam = input.split(BEGIN_FUNCTION);
        String functionName = functionWithParam[0];
        String paramString = input.substring(functionName.length() + 1, input.length() - 1);
        List<String> params = new ArrayList<>();
        if (!StringUtils.isEmpty(paramString)) {
            String[] splitParams = paramString.split(PARAMETER_SPLITTER);
            params = Arrays.asList(splitParams);
        }
        PreFunctionDTO preFunctionDTO = new PreFunctionDTO(functionName, params, params.size());
        List<PreFunction> preFunctions = validatePreFunction(functionName);

        boolean found = false;
        for (PreFunction preFunction : preFunctions) {
            if (preFunctionDTO.getNumberParam().equals(preFunction.getNumberParam())) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new ResourceNotFoundException(ErrorMessage.Normalizer.INVALID_INPUT_FIELD, Resources.NESTED_OBJECT,
                ErrorKey.Normalizer.INPUT_FIELDS);
        }
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PreFunctionDTO>> violations = validator.validate(preFunctionDTO);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }
        return preFunctionDTO;
    }

    private List<PreFunction> validatePreFunction(String input) {
        List<PreFunction> preFunctions = preFunctionRepository.findByFunctionName(input);
        if (CollectionUtils.isEmpty(preFunctions)) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
        }
        return preFunctions;
//        for (PreFunction preFunction : preFunctions) {
//            if (preFunction.getFunctionName().split(BEGIN_FUNCTION)[0].equals(input) ){
//                return preFunction;
//            }
//        }
//        throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.NESTED_OBJECT, ErrorKey.Normalizer.INPUT_FIELDS);
    }

    private void validateValues(List<NormValueDTO> normValues, NormalizerType normalizerType, Long normalizerId) {
        if (CollectionUtils.isEmpty(normValues)) {
            throw new DataInvalidException(ErrorMessage.NormalizerValue.VALUE_MUST_HAVE_VALUE, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
        }
        switch (normalizerType) {
            case StringMatchNormalizer:
            case CheckRegisterListNormalizer:
            case CheckInListNormalizer:
                if (normValues.size() != 2) {
                    throw new DataInvalidException(ErrorMessage.NormalizerValue.VALUE_MUST_HAVE_2_VALUE, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
                }
                if (normValues.stream().noneMatch(x -> Objects.equals(x.getValueId(), 0L) && x.getName().equals("false"))
                    || normValues.stream().noneMatch(x -> Objects.equals(x.getValueId(), 1L) && x.getName().equals("true"))) {
                    throw new DataInvalidException(ErrorMessage.NormalizerValue.VALUE_WITH_BAD_FORMAT, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
                }
                break;
            default:
                // đánh lại value Id
                Long maxValueId = normValueService.getMaxValueIdInNormalizer(normalizerId);
                for (NormValueDTO value : normValues) {
                    if (value.getActionType() != null && value.getActionType().equals(ActionType.ADD)) {
                        maxValueId += 1;
                        value.setValueId(maxValueId);
                    }
                }
                break;
        }
        //check: must have default value
        //check: unique name in a normalizer
        int countDefault = 0;
        Set<String> setName = new HashSet<>();
        for (NormValueDTO value : normValues) {
            if (value.isDefault()) {
                countDefault++;
            }
            if (!setName.add(value.getName().toLowerCase())) {
                throw new DataInvalidException(ErrorMessage.NormalizerValue.DUPLICATED_VALUE_NAME, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
            }
        }
        if (countDefault != 1) {
            throw new DataInvalidException(ErrorMessage.NormalizerValue.VALUE_MUST_DEFAULT_VALUE, Resources.NORMALIZER, ErrorKey.Normalizer.VALUES);
        }
    }

    private void saveNormValues(List<NormalizerValue> normValues, Map<String, Long> oldNewKeyMap) {
        normValues = normValueService.saveListNormValue(normValues);

        for (NormalizerValue normValue : normValues) {
            oldNewKeyMap.put(normValue.getTempId(), normValue.getValueId());
        }
    }


    public Page<NormalizerDTO> getNormalizerForm(SearchCriteria searchCriteria, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        searchCriteria.setDomainId(domainId);

        Category category = categoryRepository.findByIdAndDomainId(searchCriteria.getParentId(), domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }

        int parentCategoryLevel = getCategoryLevel(category.getId()) + 1;

        Page<NormalizerDTO> normalizerDTOs = normalizerRepository.findNormalizers(
            searchCriteria.getParentId(),
            searchCriteria.getDomainId(),
            searchCriteria.getName(),
            searchCriteria.getDescription(), pageable).map(normalizerMapper::toDto);

        for (NormalizerDTO normalizerDTO : normalizerDTOs) {
            normalizerDTO.setCategoryLevel(parentCategoryLevel + 1);
        }
        return normalizerDTOs;
    }

    public NormalizerDTO getNormalizerDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();

        Normalizer normalizer = normalizerRepository.findByIdAndDomainId(id, domainId);
        if (normalizer == null) {
            throw new ResourceNotFoundException(ErrorMessage.Normalizer.NOT_FOUND, Resources.NORMALIZER, ErrorKey.Normalizer.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(normalizer.getCategoryId(), domainId);
        NormalizerDTO normalizerDTO = normalizerMapper.toDto(normalizer);
        // tách input field thành 2 input field
        String[] inputFields = normalizerDTO.getInputField().split("/");
        if (inputFields.length == 2) {
            normalizerDTO.setInputField(inputFields[0]);
            normalizerDTO.setInputField2(inputFields[1]);
        }
        normalizerDTO.setCategoryName(category.getName());
        normalizerDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        normalizerDTO.setValues(normValueService.getAllByNormalizerId(id));

        List<? extends NormParamDTO> paramDTOs = normParamService.getAllByNormalizerId(id, normalizerDTO.getTypeId());
        List<Map<String, Object>> parameters = jacksonObjectMapper.convertValue(paramDTOs, List.class);
        normalizerDTO.setParameters(parameters);
        return normalizerDTO;
    }

    public void validateParamByAnnotation(List<? extends NormParamDTO> object, Class<?>... groups) {
        ValidateDTO validateDTO = new ValidateDTO();
        validateDTO.setListOfParameters(object);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ValidateDTO>> violations = validator.validate(validateDTO, groups);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }
    }

    /**
     * validate phần parameter cho những loại có trường start value, end value
     *
     * @param params List parameter nhận từ frontEnd
     * @return List Normalizer Param đã validate, và convert input
     */
    public void validateNormStartEndParams(List<NormParamDTO> params, NormalizerType normalizerType) {
        //List<StartEndParamDTO> params = Arrays.asList(jacksonObjectMapper.convertValue(normParams, StartEndParamDTO[].class));
        validateParamByAnnotation(params);
        for (NormParamDTO param_ : params) {
            StartEndParamDTO param = (StartEndParamDTO) param_;
            // Nếu cả Start Value và End Value đều không tick vào Is Characteristic thì bắt buộc nhập End Value > Start Value
            if (!param.getEndIsCharacteristic() && !param.getStartIsCharacteristic() && param.getStartValue() >= param.getEndValue()) {
                throw new DataInvalidException(ErrorMessage.NormalizerParam.START_VALUE_AND_END_VALUE, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
            }

            if (param.getStartIsCharacteristic()) {
                try {
                    characteristicService.checkCharacteristicExits(param.getStartValue());
                } catch (Exception e) {
                    throw new DataInvalidException(ErrorMessage.NormalizerParam.NOT_FOUND, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
                }
            }
            if (param.getEndIsCharacteristic()) {
                try {
                    characteristicService.checkCharacteristicExits(param.getEndValue());
                } catch (Exception e) {
                    throw new DataInvalidException(ErrorMessage.NormalizerParam.NOT_FOUND, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
                }
            }
        }
    }

    /**
     * validate phần parameter cho những loại prameter bình thường, không có trường start value, end value
     *
     * @param params List parameter nhận từ frontEnd
     * @return List Normalizer Param đã validate, và convert input
     */
    public void validateNormNormalParams(List<NormParamDTO> params, NormalizerType normalizerType) {
        //List<NormalParamDTO> params = Arrays.asList(jacksonObjectMapper.convertValue(normParams, NormalParamDTO[].class));
        switch (normalizerType) {
            case StringNormalizer:
                validateParamByAnnotation(params, NormalParamDTO.StringType.class);
                break;
            case NumberNormalizer:
                validateParamByAnnotation(params, NormalParamDTO.NumberType.class);
                break;
            case NumberParameterNormalizer:
                validateParamByAnnotation(params, NormalParamDTO.NumberParamType.class);
                for (NormParamDTO param : params) {
                    ((NormalParamDTO) param).setIsCharacteristic(true);
                }
                break;
            default:
                throw new ResourceNotFoundException("normalizer_type_not_found", "balances", "normalizer_type");
        }
        for (NormParamDTO param1 : params) {
            NormalParamDTO param = (NormalParamDTO) param1;
            if (param.getIsCharacteristic() && !NumberUtils.isDigits(param.getParameterValue())) {
                throw new DataInvalidException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_INVALID, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
            }

            if (NumberUtils.isDigits(param.getParameterValue()) && Long.parseLong(param.getParameterValue()) > Integer.MAX_VALUE) {
                throw new DataInvalidException(ErrorMessage.NormalizerParam.PARAMETER_VALUE_TOO_BIG, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
            }

            if (param.getIsCharacteristic()) {
                try {
                    characteristicService.checkCharacteristicExits(Long.parseLong(param.getParameterValue()));
                } catch (Exception e) {
                    throw new DataInvalidException(ErrorMessage.NormalizerParam.NOT_FOUND, Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
                }
            }
        }
    }

    protected void validateNormalizer(NormalizerValidator normalizerValidator) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<NormalizerValidator>> violations = validator.validate(normalizerValidator);
        if (!violations.isEmpty()) {
            throw new AnnotationException(violations, OBJECT_NAME);
        }
    }

    public void deleteNormalizer(Long id) {
        Integer domainId = OCSUtils.getDomain();
        checkNormalizerExist(id);

        Long countRateTableUsingNormalizer = normalizerRepository.countNormalizerUsed(id);
        if (countRateTableUsingNormalizer > 0) {
            throw new DataInvalidException(ErrorMessage.Normalizer.CANNOT_DELETE, Resources.NORMALIZER, ErrorKey.Normalizer.ID);
        }
        normParamService.deleteByNormalizer(domainId, id);
        normValueService.deleteByNormalizerId(domainId, id);
        normalizerRepository.deleteByIdAndDomainId(id, domainId);
    }

    private Normalizer checkNormalizerExist(Long id) {
        Optional<Normalizer> normalizer = normalizerRepository.findById(id);
        if (!normalizer.isPresent()) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.NORMALIZER, ErrorKey.Normalizer.ID);
        }
        return normalizer.get();
    }

    @Override
    public TreeClone getTreeDTO() {
        return new NormalizerDTO();
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.NORMALIZER;
    }

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList(CategoryType.RATE_TABLE, CategoryType.RATING_FILTER);
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return new ArrayList<>();
    }

    @Override
    protected String getNameCategory() {
        return "Normalizers";
    }

    @Override
    public List<OCSCloneableEntity> cloneEntities(List<CloneDTO> cloneDTOs, Map<Long, CloneDTO> oldNewChildMap) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSCloneableEntity> clonedEntities = new ArrayList<>();
        List<Long> newIds = new ArrayList<>();
        List<CloneDTO> lstCloneDTos = new ArrayList<>();
        Map<Long, CloneDTO> oldNewIdMap = new HashMap<>();
        Map<String, CloneDTO> newNameNormalizerCloneMap = new HashMap<>();
        Map<Long, List<Long>> parentNewChildrenOldIdMap = new HashMap<>();
        List<OCSCloneableEntity> clonedEntityNames = new ArrayList<>();

        List<Long> ids = cloneDTOs.stream().map(i -> i.getId()).collect(Collectors.toList());
        List<OCSCloneableEntity> baseToClones = ocsCloneableRepository.findByIdInAndDomainId(ids, domainId);
        List<Long> distinctIds = ids.stream().distinct().collect(Collectors.toList());
        if(!Objects.equals(distinctIds.size(), baseToClones.size())){
            throwResourceNotFoundException();
        };
        Map<Long, OCSCloneableEntity> baseToCloneMap = baseToClones.stream()
            .collect(Collectors.toMap(OCSCloneableEntity::getId, Function.identity()));

        List<Long> categoryIds = baseToClones.stream().map(OCSCloneableEntity::getParentId).distinct().collect(Collectors.toList());
        List<CountChildrenByParentDTO> categoryPosIndex = ocsCloneableRepository.getMaxPosIndexChildByParentId(categoryIds,domainId);
        Map<Long,Integer> categoryPosIndexMap = categoryPosIndex.stream()
            .collect(Collectors.toMap(CountChildrenByParentDTO::getId,CountChildrenByParentDTO::getCountChild));

        for (CloneDTO cloneDTO : cloneDTOs) {
            OCSCloneableEntity clonedEntity = saveNormalizer(cloneDTO, baseToCloneMap, newNameNormalizerCloneMap, categoryPosIndexMap,clonedEntityNames);
            clonedEntityNames.add(clonedEntity);
            clonedEntities.add(clonedEntity);


        }
        clonedEntities = ocsCloneableRepository.saveAll(clonedEntities);
        ocsCloneableRepository.flush();
        clonedEntities.forEach(i -> {
            CloneDTO cloneDTO = newNameNormalizerCloneMap.get(i.getName());
            List<Long> childrenIds = new ArrayList<>();
            if (cloneDTO.getChildren() != null){
                childrenIds = cloneDTO.getChildren().stream().map(CloneDTO::getId).collect(Collectors.toList());
            }
            cloneDTO.getChildren().forEach(cloneDTO1 -> cloneDTO1.setParentId(i.getId()));

            parentNewChildrenOldIdMap.put(i.getId(),childrenIds);
            oldNewIdMap.put(i.getId(), cloneDTO);
            newIds.add(i.getId());
            lstCloneDTos.addAll(cloneDTO.getChildren());

        });
        oldNewChildMap.putAll(oldNewIdMap);
        for (OCSCloneMappingService cloneMappingService : cloneMappingServices) {
            cloneMappingService.cloneMappings(newIds, lstCloneDTos, oldNewIdMap, parentNewChildrenOldIdMap);
        }

        return cloneListChildEntities(newIds, domainId, clonedEntities, oldNewIdMap);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new ResourceNotFoundException(ErrorMessage.Normalizer.NOT_FOUND,getResourceName(),ErrorKey.Normalizer.ID);
    }

    private OCSCloneableEntity saveNormalizer (CloneDTO cloneDTO, Map<Long, OCSCloneableEntity> normalizerToCloneMap,
                                               Map<String, CloneDTO> newNameNormalizerCloneMap, Map<Long,Integer> categoryPosIndexMap,
                                               List<OCSCloneableEntity> clonedEntityNames) {
        if (ocsCloneableRepository == null) {
            throw new UnsupportedOperationException("Method is not ready");
        }
        if (cloneDTO.getChildren() == null) {
            cloneDTO.setChildren(new ArrayList<CloneDTO>());
        }
        Integer domainId = OCSUtils.getDomain();
        String name = cloneDTO.getName();
        Long id = cloneDTO.getId();

        OCSCloneableEntity baseToClone = normalizerToCloneMap.get(id);

        if (baseToClone == null) {
            return null;
        }

        if(!StringUtils.isEmpty(name)) {
            checkDuplicateName(name, id);
        }
        OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
        String newName = "";
        if (StringUtils.isEmpty(name) || name.equals(clonedEntity.getName())) {
            newName = cloneName(baseToClone.getName(), domainId,clonedEntityNames);
        } else {
            newName = name;
        }

        if(baseToClone.getName().equalsIgnoreCase(newName)){
            throwDuplicatedNameException();
        }

        if (getCloneNameMaxLength() != null && newName.length() > getCloneNameMaxLength().intValue()) {
            throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
        }

        Integer maxPos = categoryPosIndexMap.get(clonedEntity.getParentId());
        clonedEntity.setPosIndex(maxPos != null ? maxPos+1 : 0);
        categoryPosIndexMap.put(clonedEntity.getParentId(),clonedEntity.getPosIndex());

        clonedEntity = setPropertiesOfCloneEntity(clonedEntity, cloneDTO, newName);

        newNameNormalizerCloneMap.put(newName, cloneDTO);
        return clonedEntity;
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.Normalizer.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }
}
