package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.domain.constant.DateEnum;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.param.TimeNormParamDTO;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("timeNormalizerService")
public class TimeNormalizerService extends NormalizerAbstractService {

    public void validateDateTimeParams(List<NormParamDTO> params, NormalizerType normalizerType, Long normalizerId) {
        validateParamByAnnotation(params);

        //check time params have all date of week at 00:00:00
        boolean hasRequiredValue = false;
        List<TimeNormParamDTO> requiredValues = getRequiredValue();
        for (TimeNormParamDTO requiredValue : requiredValues) {
            hasRequiredValue = params.stream().anyMatch(param -> param.equals(requiredValue));
            if (!hasRequiredValue) {
                throw new DataInvalidException(ErrorMessage.NormalizerParam.TIME_PARAMS_REQUIRED_ALL_DATE_OF_WEEK_AT_START_OF_DAY,
                    Resources.NORMALIZER, ErrorKey.NormalizerParam.PARAMETER_VALUE);
            }
        }

        //check duplicate param
        boolean duplicateParam = params.stream()
            .anyMatch(e -> Collections.frequency(params, e) > 1);
        if (duplicateParam) {
            throw new DataInvalidException(ErrorMessage.NormalizerParam.TIME_PARAMS_ARE_DUPLICATE, Resources.NORMALIZER,
                ErrorKey.NormalizerParam.PARAMETER_VALUE);
        }
    }

    public List<TimeNormParamDTO> getRequiredValue() {
        String startOfDay = "00:00:00";
        List<TimeNormParamDTO> timeNormParamDTOs = new ArrayList<>();
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Sunday, startOfDay));
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Monday, startOfDay));
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Tuesday, startOfDay));
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Wednesday, startOfDay));
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Thursday, startOfDay));
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Friday, startOfDay));
        timeNormParamDTOs.add(new TimeNormParamDTO(DateEnum.Saturday, startOfDay));
        return timeNormParamDTOs;
    }
}
