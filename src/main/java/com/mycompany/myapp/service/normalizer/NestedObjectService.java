package com.mycompany.myapp.service.normalizer;

import java.util.ArrayList;
import com.mycompany.myapp.domain.NestedObject;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.repository.normalizer.NestedObjectRepository;
import com.mycompany.myapp.service.OCSBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.mycompany.myapp.domain.NestedObject;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.repository.normalizer.NestedObjectRepository;
import com.mycompany.myapp.service.OCSBaseService;

@Service
public class NestedObjectService extends OCSBaseService {

    @Autowired
    private NestedObjectRepository nestedObjectRepository;


    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = nestedObjectRepository;
        this.parentService = this;
    }

//    @Cacheable(value = "nestedObjects", key = "#parentClassId")
    public Collection<NestedObject> searchTree(Long parentClassId) {
        Collection<NestedObject> results = nestedObjectRepository.findByParentClassId(parentClassId);
        for (NestedObject result : results) {
//            if (Objects.equals(result.getAlist(), true)) {
            Collection<NestedObject> children = searchTree(result.getObjClassId());
            result.setTemplates(children.stream().map(nestedObject -> (Tree) nestedObject).collect(Collectors.toList()));
            result.setHasChild(!CollectionUtils.isEmpty(result.getTemplates()));
//            }
        }
        return results;
    }

    public List<NestedObject> findByParentClassIdNotHaveChild(Long parentClassId) {
        return nestedObjectRepository.findByParentIdAndNotHaveChild(parentClassId)
            .stream().sorted(Comparator.comparingLong(NestedObject::getId))
            .collect(Collectors.toList());
    }
    public List<NestedObject> findByParentClassId(Long parentClassId) {
        return nestedObjectRepository.findByParentId(parentClassId)
            .stream().sorted(Comparator.comparingLong(NestedObject::getId))
            .collect(Collectors.toList());
    }

//    @PostConstruct
//    public void initCache() {
//        this.searchTree(100L);
//    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }
}
