package com.mycompany.myapp.service.normalizer;

import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.DropdownType;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.param.*;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.service.GeoHomeZoneService;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.repository.normalizer.NormParamRepository;
import com.mycompany.myapp.service.ZoneMapService;
import com.mycompany.myapp.service.ZoneService;
import com.mycompany.myapp.service.characteristic.CharacteristicService;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class NormParamService extends OCSBaseService implements NormChildCloneable {
    @Autowired
    private NormParamRepository normParamRepository;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private ZoneMapService zoneMapService;

    @Autowired
    private GeoHomeZoneService geoHomeZoneService;

    @Autowired
    private CharacteristicService characteristicService;

//    @Autowired
//    private NormParamMapper normalizerParamMapper;

    public List<NormalizerParam> saveNormParams(List<NormalizerParam> params) {
        Integer domainId = OCSUtils.getDomain();
        params.forEach(normParam -> normParam.setDomainId(domainId));
        params = normParamRepository.saveAll(params);
        return params;
    }

    public void deleteNormParams(List<Long> normParamIds, Long normalizerId) {
        Integer domainId = OCSUtils.getDomain();
        normParamRepository.deleteByDomainIdAndNormalizerIdAndIdIn(domainId, normalizerId, normParamIds);
    }

    public List<NormalizerParam> getByNormalizerId(Long normalizerId) {
        Integer domainId = OCSUtils.getDomain();
        return normParamRepository.findAllByNormalizerId(normalizerId, domainId);
    }

    public void delete(Long normParamId) {
        Integer domainId = OCSUtils.getDomain();
        checkNormParamsExist(normParamId);

        normParamRepository.deleteByDomainIdAndId(domainId, normParamId);
    }

    public NormalizerParam checkNormParamsExist(Long normParamId) {
        Optional<NormalizerParam> normalizerParam = normParamRepository.findById(normParamId);
        if (!normalizerParam.isPresent()) {
            throw new DataInvalidException(ErrorMessage.NormalizerParam.NOT_FOUND, Resources.NORMALIZER_PARAMS, ErrorKey.Normalizer.NORM_PARAMS);
        }
        return normalizerParam.get();
    }

    public void deleteByNormalizer(Integer domainId, Long normParamId) {
        normParamRepository.deleteByDomainIdAndNormalizerId(domainId, normParamId);
    }

    public List<? extends NormParamDTO> getAllByNormalizerId(Long normalizerId, NormalizerType normalizerType) {
        Integer domainId = OCSUtils.getDomain();
        List<NormalizerParam> normalizerParams = normParamRepository.findAllByNormalizerId(normalizerId, domainId);
        return toDto(normalizerParams, normalizerType);
    }

    public List<NormalizerParam> getAllEntityByNormalizerId(Long normalizerId) {
        Integer domainId = OCSUtils.getDomain();
        return normParamRepository.findAllByNormalizerId(normalizerId, domainId);
    }

    private List<? extends NormParamDTO> toDto(List<NormalizerParam> normalizerValues, NormalizerType normalizerType) {
        List<NormParamDTO> results = new ArrayList<>();
        for (NormalizerParam param : normalizerValues) {
            NormParamDTO paramDTO;
            switch (normalizerType) {
                case StringNormalizer:
                case NumberNormalizer:
                case NumberParameterNormalizer:
                    paramDTO = normalToDto(param);
                    break;
                case QuantityNormalizer:
                case BalanceNormalizer:
                case AcmBalanceNormalizer:
                    paramDTO = startEndToDto(param);
                    break;
                case DateNormalizer:
                    paramDTO = new DateNormParamDTO(param);
                    break;
                case TimeNormalizer:
                    paramDTO = new TimeNormParamDTO(param);
                    break;
                case ZoneNormalizer:
                    paramDTO = zoneToDto(param);
                    break;
                default:
                    return null;
            }
            results.add(paramDTO);
        }
        return results;
    }

    private NormParamDTO zoneToDto(NormalizerParam param) {
        ZoneParamDTO zoneParam = new ZoneParamDTO(param);
        DropdownType.ZoneDataType zoneDataType = DropdownType.ZoneDataType.of(zoneParam.getZoneValueTypeId());
        switch (zoneDataType) {
            case ZONE:
                zoneParam.setZoneDataName(zoneService.getNameById(zoneParam.getZoneValueId()));
                break;
            case ZONE_MAP:
                zoneParam.setZoneDataName(zoneMapService.getNameById(zoneParam.getZoneValueId()));
                break;
            case GEO_HOME_ZONE:
                zoneParam.setZoneDataName(geoHomeZoneService.getNameById(zoneParam.getZoneValueId()));
                break;
            default:
                throw new DataInvalidException("zone_data_type_id_invalid", Resources.NORMALIZER, "zone_value_type_id");
        }
        return zoneParam;
    }

    private NormParamDTO normalToDto(NormalizerParam param) {
        NormalParamDTO paramDTO = new NormalParamDTO(param);
        if (BooleanUtils.isTrue(paramDTO.getIsCharacteristic())) {
            if (paramDTO.getParameterValue() != null) {
                Long charSpecId = Long.parseLong(paramDTO.getParameterValue());
                paramDTO.setParameterName(characteristicService.findNameById(charSpecId));
            }
        }
        return paramDTO;
    }

    private NormParamDTO startEndToDto(NormalizerParam param) {
        StartEndParamDTO paramDTO = new StartEndParamDTO(param);
        if (paramDTO.getStartIsCharacteristic()) {
            Long charSpecId = paramDTO.getStartValue();
            paramDTO.setStartName(characteristicService.findNameById(charSpecId));
        }
        if (paramDTO.getEndIsCharacteristic()) {
            Long charSpecId = paramDTO.getEndValue();
            paramDTO.setEndName(characteristicService.findNameById(charSpecId));
        }
        return paramDTO;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneableRepository = normParamRepository;
    }

    @Override
    public OCSCloneableRepository getOCSCloneableRepository() {
        return this.normParamRepository;
    }

    @Override
    protected CategoryType getCategoryType() {
        return null;
    }
}
