package com.mycompany.myapp.service;

import java.util.Objects;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.domain.ZoneMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ZoneDTO;
import com.mycompany.myapp.mapper.ZoneMapper;
import com.mycompany.myapp.repository.ZoneMapRepository;
import com.mycompany.myapp.repository.ZoneRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class ZoneService extends OCSBaseService {
    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private ZoneMapper zoneMapper;

    @Autowired
    private ZoneMapRepository zoneMapRepository;

    @Autowired
    private ZoneDataService zoneDataService;

    @Autowired
    private ZoneMapService zoneMapService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = zoneRepository;
        this.parentService = zoneMapService;
    }

    public Page<ZoneDTO> getZones(Long zoneMapId, String name, String description, String zoneCode, String id, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        ZoneMap zoneMap = zoneMapRepository.findByIdAndDomainId(zoneMapId, domainId);
        if (zoneMap == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.ZONE, ErrorKey.ID);
        }
        return zoneRepository.findZones(
            zoneMapId, domainId, name, description, zoneCode, id, pageable).map(zoneMapper::toDto);
    }

    public ZoneDTO getZoneDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Zone zone = zoneRepository.findByIdAndDomainId(id, domainId);
        if (zone == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.ZONE, ErrorKey.ID);
        }
        ZoneMap zoneMap = zoneMapRepository.findByIdAndDomainIdForUpdate(zone.getZoneMapId(), domainId);
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);
        zoneDTO.setZoneMapName(zoneMap.getName());
        return zoneDTO;
    }

    public ZoneDTO createZone(ZoneDTO zoneDTO) {
        Integer domainId = OCSUtils.getDomain();

        Zone zone = zoneMapper.toEntity(zoneDTO);
        checkZoneMapExist(zoneDTO, domainId);
        checkDuplicateName(zoneDTO.getName(), zoneDTO.getId(), zoneDTO.getZoneMapId());

        zone.setDomainId(domainId);

        zone = zoneRepository.save(zone);

        return zoneMapper.toDto(zone);
    }

    private void checkDuplicateName(String name, Long id, Long zoneMapId) {
        Integer domainId = OCSUtils.getDomain();
        Zone zone = zoneRepository.findZoneByNameAndDomainIdAndZoneMapId(name, domainId, zoneMapId);
        if (zone != null && !Objects.equals(id, zone.getId())) {
            throw new DataConstrainException(ErrorMessage.Zone.DUPLICATED_NAME, Resources.ZONE, ErrorKey.Zone.NAME);
        }
    }

    public void deleteZone(Long id) {
        Integer domainId = OCSUtils.getDomain();
        if (zoneDataService.getZoneData(id, null, null, null, Pageable.unpaged()).getTotalElements() > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_HAS_CHILD, "", ErrorKey.ID);
        }
        //check Normalizer use Zone
        Long normUseZone = zoneRepository.countNormUseZone(id);
        if (normUseZone > 0) {
            throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.ZONE, "zoneId");
        }
        zoneRepository.deleteByIdAndDomainId(id, domainId);
    }


    // Check category exist or not
    private void checkZoneMapExist(ZoneDTO zoneDTO, Integer domainId) {
        ZoneMap parent = zoneMapRepository.findByIdAndDomainIdForUpdate(zoneDTO.getZoneMapId(), domainId);
        if (parent == null) {
            throw new ResourceNotFoundException(ErrorMessage.ZoneMap.NOT_FOUND, Resources.ZONE_MAP, ErrorKey.ZoneMap.ID);
        }
    }

    public ZoneDTO updateZone(ZoneDTO zoneDTO) {
        Integer domainId = OCSUtils.getDomain();
        Zone zone = zoneRepository.findByIdAndDomainIdForUpdate(zoneDTO.getId(), domainId);

        if (zone == null) {
            throw new ResourceNotFoundException(ErrorMessage.Zone.NOT_FOUND, Resources.ZONE, ErrorKey.Zone.ID);
        }

        // Lock old category
        checkZoneMapExist(zoneDTO, domainId);
        checkDuplicateName(zoneDTO.getName(), zoneDTO.getId(), zoneDTO.getZoneMapId());

        // Convert to Entity to save
        zone = zoneMapper.toEntity(zoneDTO);
        zone.setDomainId(domainId);

        zone = zoneRepository.save(zone);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(zone, response);

        return zoneMapper.toDto(zone);
    }

    public void checkZoneExist(Long id) {
        Integer domainId = OCSUtils.getDomain();
        Zone zone = zoneRepository.findByIdAndDomainId(id, domainId);
        if (zone == null) {
            throw new ResourceNotFoundException(ErrorMessage.Zone.NOT_FOUND, Resources.ZONE, ErrorKey.ID);
        }
    }

    public String getNameById(Long zoneId) {
        checkZoneExist(zoneId);
        Integer domainId = OCSUtils.getDomain();
        return zoneRepository.findNameByIdAndDomain(zoneId, domainId);
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.ZONE;
    }
}
