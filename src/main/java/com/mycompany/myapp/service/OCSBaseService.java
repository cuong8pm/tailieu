package com.mycompany.myapp.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.event.Event;
import com.mycompany.myapp.domain.eventProcessing.*;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.dto.characteristic.CharacteristicDTO;
import com.mycompany.myapp.dto.event.EventDTO;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateDTO;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.repository.*;
import com.mycompany.myapp.repository.characteristic.CharacteristicRepository;
import com.mycompany.myapp.repository.eventProcessing.BuilderRepository;
import com.mycompany.myapp.repository.eventProcessing.FunctionRepository;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.normalizer.ChildCloneable;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

//@Service
@Transactional
public abstract class OCSBaseService {
    //    protected OCSMoveableRepository parentRepository;
    protected OCSBaseService parentService;
    protected OCSMoveableRepository baseRepository;
    protected OCSCloneableRepository ocsCloneableRepository;

    protected OCSBaseService relationalChildService;
    protected OCSCloneMapRepository relationRepository;

    protected List<? extends ChildCloneable> childServices = new ArrayList<>();

    private final Logger logger = LoggerFactory.getLogger(OCSBaseService.class);

    @Autowired
    private CategoryRepository categoryRepositoryInBase;

    @Autowired
    private ZoneRepository zoneRepositoryInBase;

    @Autowired
    private PepProfilePccRepository pepProfilePccRepositoryInBase;

    @Autowired
    private ThresholdBaltypeMapRepository thresholdBaltypeMapRepositoryInBase;

    protected static final String KEY_LEVEL = "key-level";

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static class Keys {

        private Keys() {
        }

        public static final String REMOVE_IDS = "removeIds";
        public static final String TYPE = "type";
        public static final String SORT = "sort";
    }

    //    protected List<? extends MagicalInteface> magicalIntefaces;

    @SuppressWarnings("unchecked")
    public BaseResponseDTO move(List<? extends Moveable> moveables) {
        Integer domainId = OCSUtils.getDomain();

        OCSMoveableEntity firstEntity = baseRepository.findByIdAndDomainIdForUpdate(moveables.get(0).getId(), domainId);
        checkNull(firstEntity);

        OCSMoveableEntity secondEntity = baseRepository.findByIdAndDomainIdForUpdate(moveables.get(1).getId(), domainId);
        checkNull(secondEntity);

        // Check xem co cùng parent hay không
        if (!Objects.equals(firstEntity.getParentId(), secondEntity.getParentId())) {
            if (baseRepository instanceof CategoryRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.CATEGORY, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof UnitTypeRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.UNIT_TYPE, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof ParameterRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.PARAMETER, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof ReserveInfoRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.RESERVE_INFO, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof ZoneMapRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.ZONE_MAP, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof GeoHomeZoneRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.GEO_HOME_ZONE, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof BalTypeRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.BAL_TYPE, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof PccRuleRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.PCC_RULE, ErrorKey.CATEGORY_ID);
            }
            if (baseRepository instanceof CharacteristicRepository) {
                throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, Resources.CHARACTERISTIC, ErrorKey.CATEGORY_ID);
            }

            throw new PermissionDeniedException(ErrorMessage.CATEGORY_INCORRECT, "", ErrorKey.POS_INDEX);
        }

        if (firstEntity instanceof Category) {
            checkRootCategory((Category) firstEntity);
            checkRootCategory((Category) secondEntity);
        }

        // Lock parent for update
        parentService.findByIdAndDomainIdForUpdate(firstEntity.getParentId(), domainId);

        // Check pos index đã bị xê dịch hay chua, đề phòng 2 người cùng xê dich cùng 1 lúc
        if (!Objects.equals(moveables.get(0).getPosIndex(), firstEntity.getPosIndex())) {
            getExceptionFromCategoryIncorrect();
        }
        if (!Objects.equals(moveables.get(1).getPosIndex(), secondEntity.getPosIndex())) {
            getExceptionFromCategoryIncorrect();
        }

        Integer tempPosIndex = firstEntity.getPosIndex();
        firstEntity.setPosIndex(secondEntity.getPosIndex());
        secondEntity.setPosIndex(tempPosIndex);

        baseRepository.save(firstEntity);
        baseRepository.save(secondEntity);

        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(firstEntity, response);
        return response;
    }

    private void getExceptionFromCategoryIncorrect() {
        if (baseRepository instanceof CategoryRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.CATEGORY, ErrorKey.Category.POS_INDEX);
        }
        if (baseRepository instanceof UnitTypeRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.UNIT_TYPE, ErrorKey.UnitType.UNIT_ORDER);
        }
        if (baseRepository instanceof ParameterRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.PARAMETER, ErrorKey.Parameter.POS_INDEX);
        }
        if (baseRepository instanceof ReserveInfoRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.POS_INDEX);
        }
        if (baseRepository instanceof ZoneMapRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.ZONE_MAP, ErrorKey.ZoneMap.POS_INDEX);
        }
        if (baseRepository instanceof GeoHomeZoneRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.GEO_HOME_ZONE, ErrorKey.GeoHomeZone.POS_INDEX);
        }
        if (baseRepository instanceof BalTypeRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.BAL_TYPE, ErrorKey.BalType.POS_INDEX);
        }
        if (baseRepository instanceof PccRuleRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.PCC_RULE, ErrorKey.PccRule.POS_INDEX);
        }
        if (baseRepository instanceof StateTypeRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.STATETYPE, ErrorKey.StateSet.POS_INDEX);
        }
        if (baseRepository instanceof StateGroupsRepository) {
            throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, Resources.STATEGROUPS, ErrorKey.StateSet.POS_INDEX);
        }
        if (baseRepository instanceof CharacteristicRepository) {
            throw new ResourceConflictException(
                ErrorMessage.CATEGORY_INCORRECT,
                Resources.CHARACTERISTIC,
                ErrorKey.Characteristic.POS_INDEX
            );
        }
        throw new ResourceConflictException(ErrorMessage.CATEGORY_INCORRECT, "", ErrorKey.POS_INDEX);
    }

    private void checkNull(OCSMoveableEntity entity) {
        if (entity == null) {
            if (baseRepository instanceof CategoryRepository) {
                throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
            }
            if (baseRepository instanceof UnitTypeRepository) {
                throw new ResourceNotFoundException(ErrorMessage.UnitType.NOT_FOUND, Resources.UNIT_TYPE, ErrorKey.UnitType.ID);
            }
            if (baseRepository instanceof ParameterRepository) {
                throw new ResourceNotFoundException(ErrorMessage.Parameter.NOT_FOUND, Resources.PARAMETER, ErrorKey.Parameter.ID);
            }
            if (baseRepository instanceof ReserveInfoRepository) {
                throw new ResourceNotFoundException(ErrorMessage.ReserveInfo.NOT_FOUND, Resources.RESERVE_INFO, ErrorKey.ReserveInfo.ID);
            }
            if (baseRepository instanceof ZoneMapRepository) {
                throw new ResourceNotFoundException(ErrorMessage.ZoneMap.NOT_FOUND, Resources.ZONE_MAP, ErrorKey.ZoneMap.ID);
            }
            if (baseRepository instanceof GeoHomeZoneRepository) {
                throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE, ErrorKey.GeoHomeZone.ID);
            }
            if (baseRepository instanceof BalTypeRepository) {
                throw new ResourceNotFoundException(ErrorMessage.BalType.NOT_FOUND, Resources.BAL_TYPE, ErrorKey.BalType.ID);
            }
            if (baseRepository instanceof CharacteristicRepository) {
                throw new ResourceNotFoundException(
                    ErrorMessage.Characteristic.NOT_FOUND,
                    Resources.CHARACTERISTIC,
                    ErrorKey.Characteristic.ID
                );
            }
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, "", ErrorKey.ID);
        }
    }

    private void checkRootCategory(Category category) {
        if (category.getCategoryParentId() == null) {
            throw new PermissionDeniedException(ErrorMessage.Category.CANT_UPDATE_ROOT, "", Resources.CATEGORY);
        }
    }

    @SuppressWarnings("unchecked")
    public void checkDuplicateName(String name, Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<OCSMoveableEntity> ocsMoveableEntities = baseRepository.findByNameAndDomainId(name, domainId);
        for (OCSMoveableEntity ocsMoveableEntity : ocsMoveableEntities) {
            if (ocsMoveableEntity != null && !Objects.equals(id, ocsMoveableEntity.getId())) {
                if (ocsMoveableEntity instanceof Category) {
                    throw new DataConstrainException(ErrorMessage.Category.DUPLICATED_NAME, Resources.CATEGORY, ErrorKey.Category.NAME);
                }
                if (ocsMoveableEntity instanceof UnitType) {
                    throw new DataConstrainException(ErrorMessage.UnitType.DUPLICATED_NAME, Resources.UNIT_TYPE, ErrorKey.UnitType.NAME);
                }
                if (ocsMoveableEntity instanceof Parameter) {
                    throw new DataConstrainException(ErrorMessage.Parameter.DUPLICATED_NAME, Resources.PARAMETER, ErrorKey.Parameter.NAME);
                }
                if (ocsMoveableEntity instanceof ReserveInfo) {
                    throw new DataConstrainException(
                        ErrorMessage.ReserveInfo.DUPLICATED_NAME,
                        Resources.RESERVE_INFO,
                        ErrorKey.ReserveInfo.NAME
                    );
                }
                if (ocsMoveableEntity instanceof ZoneMap) {
                    throw new DataConstrainException(ErrorMessage.ZoneMap.DUPLICATED_NAME, Resources.ZONE_MAP, ErrorKey.ZoneMap.NAME);
                }
                if (ocsMoveableEntity instanceof GeoHomeZone) {
                    throw new DataConstrainException(
                        ErrorMessage.GeoHomeZone.DUPLICATED_NAME,
                        Resources.GEO_HOME_ZONE,
                        ErrorKey.GeoHomeZone.NAME
                    );
                }
                if (ocsMoveableEntity instanceof StatisticItem) {
                    throw new DataConstrainException(
                        ErrorMessage.StatisticItem.DUPLICATED_NAME,
                        Resources.STATISTIC,
                        ErrorKey.Statistic.NAME
                    );
                }
                if (ocsMoveableEntity instanceof StateGroups) {
                    throw new ResourceNotFoundException(
                        ErrorMessage.StateSet.STATE_GROUP_DUPLICATED_NAME,
                        Resources.STATEGROUPS,
                        ErrorKey.Statistic.NAME
                    );
                }
                if (ocsMoveableEntity instanceof StateType) {
                    throw new ResourceNotFoundException(
                        ErrorMessage.StateSet.STATE_TYPE_DUPLICATED_NAME,
                        Resources.STATETYPE,
                        ErrorKey.Statistic.NAME
                    );
                }
                if (ocsMoveableEntity instanceof PccRule) {
                    throw new DataConstrainException(ErrorMessage.PccRule.DUPLICATED_NAME, Resources.PCC_RULE, ErrorKey.Statistic.NAME);
                }
                if (ocsMoveableEntity instanceof BalType) {
                    if (((BalType) ocsMoveableEntity).isAcm()) {
                        throw new DataConstrainException(
                            ErrorMessage.BalType.ACCUMULATE_DUPLICATED_NAME,
                            Resources.BAL_TYPE,
                            ErrorKey.BalType.NAME
                        );
                    }
                    throw new DataConstrainException(
                        ErrorMessage.BalType.BALANCE_DUPLICATED_NAME,
                        Resources.BAL_TYPE,
                        ErrorKey.BalType.NAME
                    );
                }
                if (ocsMoveableEntity instanceof MapSharebalBal) {
                    throw new DataConstrainException(
                        ErrorMessage.AccountBalance.DUPLICATED_NAME,
                        Resources.ACCOUNT_BALANCE_MAPPINGS,
                        ErrorKey.MapSharebalBal.NAME
                    );
                }
                if (ocsMoveableEntity instanceof PepProfile) {
                    throw new ResourceNotFoundException(
                        ErrorMessage.PepProfile.DUPLICATED_NAME,
                        Resources.PEP_PROFILE,
                        ErrorKey.PepProfile.NAME
                    );
                }
                if (ocsMoveableEntity instanceof MapAcmBalBal) {
                    throw new DataConstrainException(
                        ErrorMessage.BalanceAcm.DUPLICATED_NAME,
                        Resources.BALANCE_ACM_MAPPINGS,
                        ErrorKey.MapAcmBalBal.NAME
                    );
                }
                if (ocsMoveableEntity instanceof Characteristic) {
                    throw new DataConstrainException(
                        ErrorMessage.BalanceAcm.DUPLICATED_NAME,
                        Resources.CHARACTERISTIC,
                        ErrorKey.Characteristic.NAME
                    );
                }
                if (ocsMoveableEntity instanceof Normalizer) {
                    throw new DataConstrainException(ErrorMessage.Normalizer.DUPLICATED_NAME, Resources.NORMALIZER, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof RatingFilter) {
                    throw new DataConstrainException(ErrorMessage.RatingFilter.DUPLICATED_NAME, Resources.RATING_FILTER, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof RateTable) {
                    throw new DataConstrainException(ErrorMessage.RateTable.DUPLICATED_NAME, Resources.RATE_TABLE, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof Action) {
                    throw new DataConstrainException(ErrorMessage.Action.DUPLICATED_NAME, Resources.ACTION, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof ActionType) {
                    throw new DataConstrainException(ErrorMessage.ActionType.DUPLICATED_NAME, Resources.ACTION_TYPE, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof Event) {
                    throw new DataConstrainException(ErrorMessage.Event.DUPLICATED_NAME, Resources.EVENT, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof PriceComponent) {
                    throw new DataConstrainException(ErrorMessage.PriceComponent.DUPLICATED_NAME, Resources.PRICE_COMPONENT, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof Block) {
                    throw new DataConstrainException(ErrorMessage.Block.DUPLICATED_NAME, Resources.BLOCK, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof Offer) {
                    throw new DataConstrainException(ErrorMessage.Offer.DUPLICATED_NAME, Resources.OFFER, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof OfferTemplate) {
                    throw new DataConstrainException(ErrorMessage.OfferTemplate.DUPLICATED_NAME, Resources.OFFER_TEMPLATE, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof BillingCycleType) {
                    throw new DataConstrainException(ErrorMessage.BillingCycle.TYPE_DUPLICATED_NAME, Resources.BILLING_CYCLE_TYPE, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof OfferVersionCommon) {
                    continue;
                }
                if (ocsMoveableEntity instanceof BuilderType) {
                    throw new DataConstrainException(ErrorMessage.BuilderType.DUPLICATED_NAME, Resources.BUILDER_TYPE, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof Verification) {
                    throw new DataConstrainException(
                        ErrorMessage.Verification.DUPLICATED_NAME,
                        Resources.VERIFICATION,
                        ErrorKey.Verification.NAME
                    );
                }
                if (ocsMoveableEntity instanceof Builder) {
                    throw new DataConstrainException(ErrorMessage.Builder.DUPLICATED_NAME, Resources.BUILDER, ErrorKey.Builder.NAME);
                }
                if (ocsMoveableEntity instanceof Notify) {
                    throw new DataConstrainException(ErrorMessage.Notify.DUPLICATED_NAME, Resources.NOTIFY, ErrorKey.Notify.NAME);
                }
                if (ocsMoveableEntity instanceof Condition) {
                    throw new DataConstrainException(ErrorMessage.Condition.DUPLICATED_NAME, Resources.CONDITION, ErrorKey.Condition.NAME);
                }
                if (ocsMoveableEntity instanceof Router) {
                    throw new DataConstrainException(ErrorMessage.Router.DUPLICATED_NAME, Resources.ROUTER, ErrorKey.Router.NAME);
                }
                if (ocsMoveableEntity instanceof com.mycompany.myapp.domain.eventProcessing.Function) {
                    throw new DataConstrainException(ErrorMessage.Function.DUPLICATED_ALIAS, Resources.FUNCTION, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof EventPolicy) {
                    throw new DataConstrainException(ErrorMessage.EventPolicy.DUPLICATED_NAME, Resources.EVENT_POLICY, ErrorKey.NAME);
                }
                if (ocsMoveableEntity instanceof Filter) {
                    throw new DataConstrainException(ErrorMessage.Filter.DUPLICATED_NAME, Resources.FILTER, ErrorKey.NAME);
                }
                throw new DataConstrainException(ErrorMessage.DUPLICATED_NAME, "", ErrorKey.NAME);
            }
        }
    }

    public abstract void setBaseRepository();

    public List<Tree> searchInTree(String name, CategoryType categoryType, List<Long> removeIds, Boolean isPopup) {
        return searchInTree(name, categoryType, removeIds, new HashMap<>(), isPopup, null);
    }

    public List<Tree> searchInTree(String name, CategoryType categoryType, List<Long> removeIds, Boolean isPopup, Long type) {
        return searchInTree(name, categoryType, removeIds, new HashMap<>(), isPopup, type);
    }

    public List<Tree> searchInTree(String name, CategoryType categoryType, Long type) {
        return searchInTree(name, categoryType, null, new HashMap<>(), null, type);
    }

    public List<Tree> searchInTree(String name, CategoryType categoryType, List<Long> removeIds,
                                   Map<String, Object> parameters, Boolean isPopup) {
        return searchInTree(name, categoryType, removeIds, parameters, isPopup, null);
    }

    @SuppressWarnings("unchecked")
    public List<Tree> searchInTree(
        String name,
        CategoryType categoryType,
        List<Long> removeIds,
        Map<String, Object> parameters,
        Boolean isPopup, Long type
    ) {
        Integer domainId = OCSUtils.getDomain();
        Sort posIndexSort = Sort.by(Direction.ASC, "posIndex");
        //        List<Long> removeIds = (List<Long>) parameters.get(Keys.REMOVE_IDS);
        //        Integer ratingFilterType = (Integer) parameters.get(Keys.TYPE);
        parameters.put(Keys.SORT, posIndexSort);
        List<? extends OCSMoveableEntity> foundEntities = findBaseEntity(name, domainId, categoryType, removeIds, parameters, type);

        Set<Tree> leafs = new HashSet<>();

        Map<String, Tree> foundTrees = new HashMap<>();

        List<Category> categories = categoryRepositoryInBase.findByDomainIdAndCategoryTypeOrderByPosIndexAsc(
            domainId,
            getCategoryType() == null ? categoryType : getCategoryType()
        );

        Map<Long, Category> categoryMap = categories.stream().collect(Collectors.toMap(Category::getId, Function.identity()));

        List<Long> ids = foundEntities.stream().map(i -> i.getId()).collect(Collectors.toList());

        Map<Long, Object> mappingOfFoundEntity = getMappingFoundEntities(ids, isPopup);

        Map<Long, Long> map = new HashMap<Long, Long>();

        if (isPopup == null || isPopup != true) {
            if (this instanceof ZoneMapService) {
                map =
                    zoneRepositoryInBase
                        .findByDomainId(domainId)
                        .stream()
                        .collect(Collectors.groupingBy(Zone::getParentId, Collectors.counting()));
            }
            if (this instanceof PepProfileService) {
                map =
                    pepProfilePccRepositoryInBase
                        .findByDomainId(domainId)
                        .stream()
                        .collect(Collectors.groupingBy(PepProfilePcc::getParentId, Collectors.counting()));
            }
            if (this instanceof BalancesService) {
                map =
                    thresholdBaltypeMapRepositoryInBase
                        .findByDomainId(domainId)
                        .stream()
                        .collect(Collectors.groupingBy(ThresholdBaltypeMap::getParentId, Collectors.counting()));
            }
        }

        for (OCSMoveableEntity entity : foundEntities) {
            Tree tree = getTree(entity);

            setTypeForTree(entity, tree);

            String tempTreeType = tree.getTreeType();
            BeanUtils.copyProperties(entity, tree);
            if (tree instanceof CategoryDTO) {
                tree.setType(((Category) entity).getTreeType());
                tree.setTreeType(tempTreeType);
                String key = getKey(tree.getId(), tree.getTreeType());
                foundTrees.put(key, tree);
            }
            if (mappingOfFoundEntity.containsKey(entity.getId())) {
                assert tree instanceof TreeClone;
                ((TreeClone) tree).setHasChild(true);
            }

            if (tree instanceof ZoneMapDTO) {
                ((ZoneMapDTO) tree).setHasChild(map.get(tree.getId()) != null);
            }
            if (tree instanceof PepProfileDTO) {
                ((PepProfileDTO) tree).setHasChild(map.get(tree.getId()) != null);
            }
            if (tree instanceof BalancesDTO) {
                ((BalancesDTO) tree).setHasChild(map.get(tree.getId()) != null);
            }

            leafs.add(tree);
        }
        map.clear();
        for (Tree leaf : leafs) {
            logger.debug("begin building tree ============ ");
            buildTree(leaf, domainId, foundTrees, categoryMap);
            logger.debug("end building tree ============ ");
        }

        List<Tree> results = getTreesResult(foundTrees);
        int parentCategoryLevel = 0;
        this.sortTree(results, parentCategoryLevel);
        setCategoryTreeType(results);
        return results;
    }

    protected Map<Long, Object> getMappingFoundEntities(List<Long> ids, Boolean isPopup) {
        Map<Long, Object> mappingOfFoundEntity = new HashMap<>();
        List list = getMappingFoundEntities(ids);
        if (isPopup == null || (list != null && isPopup != true)) {
            if (!list.isEmpty() && list.get(0) instanceof OCSCloneableMap) {
                mappingOfFoundEntity =
                    (Map<Long, Object>) list.stream().collect(Collectors.groupingBy(OCSCloneableMap::getParentId, Collectors.toList()));
            }

            if (!list.isEmpty() && list.get(0) instanceof OfferVersionCommon) {
                mappingOfFoundEntity =
                    (Map<Long, Object>) list.stream().collect(Collectors.groupingBy(OfferVersionCommon::getParentId, Collectors.toList()));
            }
        }
        return mappingOfFoundEntity;
    }

    protected void setTypeForTree(OCSMoveableEntity entity, Tree tree) {
    }

    private void setCategoryTreeType(Collection<Tree> collection) {
        for (Tree tree : collection) {
            if (tree instanceof CategoryDTO) {
                tree.setTreeType("category");
                if (tree.getTemplates() != null) setCategoryTreeType(tree.getTemplates());
            }
        }
    }

    protected List getMappingFoundEntities(List<Long> ids) {
        return new ArrayList<>();
    }

    protected List<Tree> getTreesResult(Map<String, Tree> foundTrees) {
        List<Tree> results = foundTrees
            .values()
            .stream()
            .filter(foundTree -> foundTree.getParentId() == null)
            .distinct()
            .collect(Collectors.toList());
        return results;
    }

    @SuppressWarnings("unchecked")
    public List<Tree> searchInTree(String name, CategoryType categoryType) {
        return searchInTree(name, categoryType, null, new HashMap<>(), null, null);
    }

    private void sortTree(List<Tree> input, int parentCategoryLevel) {
        Collections.sort(
            input,
            Comparator
                .comparing((Tree tree) -> tree.getTreeType())
                .thenComparing(tree -> tree.getPosIndex())
                .thenComparing(tree -> tree.getId())
        );

        for (Tree tree : input) {
            if (!CollectionUtils.isEmpty(tree.getTemplates())) {
                List<Tree> children = tree.getTemplates().stream().collect(Collectors.toList());
                this.sortTree(children, parentCategoryLevel + 1);
                tree.setTemplates(children);
            }
            tree.setCategoryLevel(parentCategoryLevel);
        }
    }

    public Tree buildTree(Tree leaf, Integer domainId, Map<String, Tree> foundTrees, Map<Long, Category> categoryMap) {
        String key = getKey(leaf.getId(), leaf.getTreeType());
        if (foundTrees.get(key) == null) {
            logger.debug("add to map: {} ", key);
            foundTrees.put(key, leaf);
        }
        logger.debug("leaf id: {}", leaf.getId());
        logger.debug("leaf parent: {}", leaf.getParentId());
        Tree branch = getParent(leaf, domainId, foundTrees, categoryMap);
        if (branch != null) {
            if (branch.getTemplates() == null) {
                branch.setTemplates(new HashSet<>());
            }
//            Set<Long> ids = branch.getTemplates().stream().map(BaseResponseDTO::getId).collect(Collectors.toSet());
//            if (!ids.contains(leaf.getId())) {
//
//            }
            branch.getTemplates().add(leaf);
            if (branch instanceof CategoryDTO) {
                ((CategoryDTO) branch).setHasChild(true);
            }

            return parentService.buildTree(branch, domainId, foundTrees, categoryMap);
        } else {
            return leaf;
        }
    }

    @SuppressWarnings("unchecked")
    private Tree getParent(Tree tree, Integer domainId, Map<String, Tree> foundTrees, Map<Long, Category> categoryMap) {
        String key = getKey(tree.getParentId(), getGroupParent(tree));
        logger.debug("key in map: {}", key);
        if (foundTrees.get(key) != null) {
            logger.debug("found in map");
            return foundTrees.get(key);
        }
        OCSMoveableEntity parent = null;
        if (parentService instanceof CategoryService) {
            if (categoryMap.get(tree.getParentId()) != null || tree.getParentId() == null) {
                parent = categoryMap.get(tree.getParentId());
            } else {
                parent = parentService.findByIdAndDomainId(tree.getParentId(), domainId);
                categoryMap.put(parent.getId(), (Category) parent);
            }
        } else {
            parent = parentService.findByIdAndDomainId(tree.getParentId(), domainId);
        }

        if (parent != null) {
            logger.debug("found in db");
            Tree parentTree = getTree(parent);
            String tempTreeType = parentTree.getTreeType();
            BeanUtils.copyProperties(parent, parentTree);

            if (parent instanceof Category) {
                //                parentTree.setTreeType("category");
                ((CategoryDTO) parentTree).setHasChild(true);
                ((CategoryDTO) parentTree).setType(((Category) parent).getTreeType());
                parentTree.setTreeType(tempTreeType);
            } else {
                setHasChild(parentTree);
            }

            return parentTree;
        } else {
            logger.debug("not found");
            return null;
        }
    }

    public OCSMoveableEntity findByIdAndDomainId(Long id, Integer domainId) {
        return baseRepository.findByIdAndDomainId(id, domainId);
    }

    public OCSMoveableEntity findByIdAndDomainIdForUpdate(Long id, Integer domainId) {
        return baseRepository.findByIdAndDomainIdForUpdate(id, domainId);
    }

    protected Tree getTree(OCSMoveableEntity entity) {
        if (entity instanceof Category) {
            return new CategoryDTO();
        }
        if (entity instanceof UnitType) {
            return new UnitTypeDTO();
        }
        if (entity instanceof Parameter) {
            return new ParameterDTO();
        }
        if (entity instanceof ReserveInfo) {
            return new ReserveInfoDTO();
        }
        if (entity instanceof ZoneMap) {
            return new ZoneMapDTO();
        }
        if (entity instanceof Zone) {
            return new ZoneDTO();
        }
        if (entity instanceof GeoHomeZone) {
            return new GeoHomeZoneDTO();
        }
        if (entity instanceof BalType) {
            return new BalancesDTO();
        }
        if (entity instanceof OCSService) {
            return new OcsServiceDTO();
        }
        if (entity instanceof StatisticItem) {
            return new StatisticItemDTO();
        }
        if (entity instanceof PccRule) {
            return new PccRuleDTO();
        }
        if (entity instanceof StateType) {
            return new StateTypeDTO();
        }
        if (entity instanceof StateGroups) {
            return new StateGroupsDTO();
        }
        if (entity instanceof PepProfile) {
            return new PepProfileDTO();
        }
        if (entity instanceof MapSharebalBal) {
            return new AccountBalanceMappingDTO();
        }
        if (entity instanceof MapAcmBalBal) {
            return new BalanceAcmMappingDTO();
        }
        if (entity instanceof BillingCycleType) {
            return new BillingCycleTypeDTO();
        }
        if (entity instanceof Characteristic) {
            return new CharacteristicDTO();
        }
        if (entity instanceof Normalizer) {
            return new NormalizerDTO();
        }
        if (entity instanceof NestedObject) {
        }
        if (entity instanceof RatingFilter) {
            return new RatingFilterDTO();
        }
        if (entity instanceof RateTable) {
            return new RateTableDTO();
        }
        if (entity instanceof Block) {
            return new BlockDTO();
        }
        if (entity instanceof PriceComponent) {
            return new PriceComponentDTO();
        }
        if (entity instanceof Action) {
            return new ActionDTO();
        }
        if (entity instanceof ActionType) {
            return new ActionTypeDTO();
        }
        if (entity instanceof Event) {
            return new EventDTO();
        }
        if (entity instanceof Offer) {
            return new OfferDTO();
        }
        if (entity instanceof OfferTemplate) {
            return new OfferTemplateDTO();
        }
        if (entity instanceof BuilderType) {
            return new BuilderTypeDTO();
        }
        if (entity instanceof Builder) {
            return new BuilderDTO();
        }
        if (entity instanceof Verification) {
            return new VerificationDTO();
        }
        if (entity instanceof Router) {
            return new RouterDTO();
        }
        if (entity instanceof com.mycompany.myapp.domain.eventProcessing.Function) {
            return new FunctionDTO();
        }
        if (entity instanceof Filter) {
            return new FilterDTO();
        }
        if (entity instanceof Condition) {
            return new ConditionDTO();
        }
        if (entity instanceof EventPolicy) {
            return new EventPolicyDTO();
        }
        throw new RuntimeException();
    }

    private String getKey(Long id, String treeType) {
        return id + treeType;
    }

    @SuppressWarnings("unchecked")
    protected List<? extends OCSMoveableEntity> findBaseEntity(
        String name,
        Integer domainId,
        CategoryType categoryType,
        List<Long> removeIds,
        Map<String, Object> params,
        Long type
    ) {
        Sort sort = (Sort) params.get(Keys.SORT);
        if (!(baseRepository instanceof CategoryRepository)) {
            if (categoryType == CategoryType.BALANCES) {
                if (!StringUtils.isEmpty(name)) {
                    name = name.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
                }
                return ((BalTypeRepository) baseRepository).findByNameAndDomainAndAcm(domainId, name, false, sort)
                    .stream()
                    .map(balType -> (OCSMoveableEntity) balType)
                    .collect(Collectors.toList());
            } else if (categoryType == CategoryType.ACCUMULATE_BALANCES) {
                if (!StringUtils.isEmpty(name)) {
                    name = name.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
                }
                return ((BalTypeRepository) baseRepository).findByNameAndDomainAndAcm(domainId, name, true, sort);
            } else if (categoryType == CategoryType.NESTED_OBJECT) {
                if (!StringUtils.isEmpty(name)) {
                    name = name.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
                }
                return ((BalTypeRepository) baseRepository).findByNameAndDomainAndAcm(domainId, name, true, sort)
                    .stream()
                    .map(balType -> (OCSMoveableEntity) balType)
                    .collect(Collectors.toList());
            } else {
                if (type != null) {
                    if (categoryType == CategoryType.BUILDER) {
                        if (!StringUtils.isEmpty(name)) {
                            return ((BuilderRepository) baseRepository)
                                .findByNameContainingAndDomainIdAndBuilderTypeId(name, domainId, type, sort);
                        }
                        return ((BuilderRepository) baseRepository).findByDomainIdAndBuilderTypeId(domainId, type,
                            sort);
                    }
                    if (categoryType == CategoryType.FUNCTION) {
                        if (!StringUtils.isEmpty(name)) {
                            return ((FunctionRepository) baseRepository)
                                .findByNameContainingAndDomainIdAndFunctionType(name, domainId, type.intValue(), sort);
                        }
                        return ((FunctionRepository) baseRepository).findByDomainIdAndFunctionType(domainId, type.intValue(), sort);
                    }
                }
                if (!StringUtils.isEmpty(name)) {
                    if (removeIds != null && !removeIds.isEmpty()) {
                        return baseRepository.findByNameContainingAndDomainIdAndIdNotIn(name, domainId, removeIds, sort);
                    }
                    return baseRepository.findByNameContainingAndDomainId(name, domainId, sort);
                }
                if (removeIds != null && !removeIds.isEmpty()) {
                    return baseRepository.findByDomainIdAndIdNotIn(domainId, removeIds, sort);
                }
                return baseRepository.findByDomainId(domainId, sort);
            }
        } else {
            return ((CategoryRepository) baseRepository).findByNameContainingAndDomainIdAndCategoryType(name, domainId, categoryType, sort);
        }

    }

    protected Integer getCategoryLevel(Long categoryId) {
        int level = -1;
        if (parentService instanceof CategoryService) {
            Integer domainId = OCSUtils.getDomain();
            Category category = (Category) ((CategoryService) parentService).findByIdAndDomainIdForUpdate(categoryId, domainId);
            while (category.getCategoryParentId() != null) {
                category =
                    (Category) ((CategoryService) parentService).findByIdAndDomainIdForUpdate(category.getCategoryParentId(), domainId);
                level++;
            }
        }
        return level;
    }

    public OCSCloneableEntity cloneEntity(Long id, int levelCount, int deep, String name) {
        Integer domainId = OCSUtils.getDomain();
        if (ocsCloneableRepository == null) {
            throw new DataConstrainException(ErrorMessage.CANNOT_CLONE, "", ErrorKey.ID);
        }
        try {
            OCSCloneableEntity baseToClone = ocsCloneableRepository.findByIdAndDomainId(id, domainId);
            if (levelCount > deep) {
                return baseToClone;
            }

            if (!StringUtils.isEmpty(name)) {
                checkDuplicateName(name, id);
            }

            OCSCloneableEntity clonedEntity = SerializationUtils.clone(baseToClone);
            String newName = "";
            if (name == null || StringUtils.isEmpty(name) || name.equals(clonedEntity.getName())) {
                newName = cloneName(baseToClone.getName(), domainId);
            } else {
                newName = name;
            }

            if(baseToClone instanceof Threshold){
                if (newName.length() > 200) {
                    throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                        StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
                }
            }

            if (newName.length() > 255) {
                throw new CloneNameTooLongException(ErrorMessage.CLONE_NAME_TOO_LONG, getResourceName(), ErrorKey.NAME, id,
                    StringUtils.isEmpty(name) ? clonedEntity.getName() : name);
            }

            if(baseToClone.getName().equalsIgnoreCase(newName)){
                throwDuplicatedNameException();
            }

            clonedEntity.setName(newName);
            setPosIndex(domainId, clonedEntity, clonedEntity.getParentId());

            clonedEntity.setId(null);
            clonedEntity = ocsCloneableRepository.saveAndFlush(clonedEntity);
            if (relationRepository != null) {
                List<OCSCloneableMap> relationMaps = relationRepository.findByDomainIdAndParentId(domainId, id);
                List<OCSCloneableMap> newRelationMaps = new ArrayList<>();
                for (OCSCloneableMap element : relationMaps) {
                    OCSCloneableMap newMap = SerializationUtils.clone(element);
                    if (relationalChildService != null) {
                        Long newChildId = relationalChildService.cloneEntity(element.getChildId(), levelCount + 1, deep, "").getId();
                        newMap.setChildMappingId(newChildId);
                    }
                    newMap.setParentMappingId(clonedEntity.getId());
                    newMap.clearId();
                    newRelationMaps.add(newMap);
                }
                relationRepository.saveAll(newRelationMaps);
            }

            return cloneChildEntities(id, domainId, clonedEntity);
        } catch (ClassCastException e) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_CLONEABLE, Resources.BAL_TYPE, ErrorKey.BalType.ID);
        }
    }

    protected OCSCloneableEntity cloneChildEntities(Long id, Integer domainId, OCSCloneableEntity clonedEntity) {
        for (ChildCloneable ocsBaseService : childServices) {
            List<OCSCloneableEntity> listToClone = ocsBaseService.getOCSCloneableRepository().findByParentIdAndDomainId(id, domainId);
            List<OCSCloneableEntity> clonedList = new ArrayList<>();
            for (OCSCloneableEntity cloneElement : listToClone) {
                OCSCloneableEntity clonedChildEntity = SerializationUtils.clone(cloneElement);
                clonedChildEntity.setId(null);
                clonedChildEntity.setParentId(clonedEntity.getId());
                clonedList.add(clonedChildEntity);
            }
            List<OCSCloneableEntity> cloneableChildEntity = ocsBaseService.getOCSCloneableRepository().saveAll(clonedList);
            cloneChildDependOfChildEntities(domainId, cloneableChildEntity, listToClone, ocsBaseService);
        }
        return clonedEntity;
    }

    protected List<OCSCloneableEntity> cloneListChildEntities(List<Long> newIds, Integer domainId, List<OCSCloneableEntity> clonedEntities, Map<Long, CloneDTO> oldNewIdParentMap) {
        List<Long> oldIds = oldNewIdParentMap.values().stream().map(CloneDTO::getId).collect(Collectors.toList());
        for (ChildCloneable ocsBaseService : childServices) {
            List<OCSCloneableEntity> listToClone = ocsBaseService.getOCSCloneableRepository().findByDomainIdAndParentIdIn(domainId, oldIds);
            List<OCSCloneableEntity> clonedList = new ArrayList<>();
            for (Long newId : newIds) {
                Long oldId = oldNewIdParentMap.get(newId).getId();
                List<OCSCloneableEntity> cloneableEntities = listToClone.stream().filter(item -> Objects.equals(item.getParentId(), oldId)).collect(Collectors.toList());
                for (OCSCloneableEntity cloneableElement : cloneableEntities) {
                    OCSCloneableEntity clonedChildEntity = SerializationUtils.clone(cloneableElement);
                    clonedChildEntity.setId(null);
                    clonedChildEntity.setParentId(newId);
                    clonedList.add(clonedChildEntity);
                }
            }
            List<OCSCloneableEntity> cloneableChildEntity = ocsBaseService.getOCSCloneableRepository().saveAll(clonedList);
            cloneChildDependOfChildEntities(domainId, cloneableChildEntity, listToClone, ocsBaseService);
        }
        return clonedEntities;
    }


    protected void cloneChildDependOfChildEntities(
        Integer domainId,
        List<OCSCloneableEntity> clonedParents,
        List<OCSCloneableEntity> listParentToClone,
        ChildCloneable parentService
    ) {
    }

    protected String cloneName(String name, Integer domainId) {
        List<OCSCloneableEntity> cloneEntities = new ArrayList<>();
        cloneEntities = ocsCloneableRepository.findByNameStartsWithAndDomainId(name + "_clone", domainId);
        Pattern clonedNum = Pattern.compile("_clone(\\([1-9]+\\))?$",Pattern.CASE_INSENSITIVE);
        String newName = "";
        boolean hasBeenCloned = cloneEntities.stream().anyMatch(
            item -> BooleanUtils.isTrue(clonedNum.matcher(item.getName()).find()) &&
                item.getName().toLowerCase().lastIndexOf("_clone") == name.length());
        if (!hasBeenCloned) {
            // Chua duoc clone bao gio
            newName = name + "_clone";
        } else {
            // Da duoc Clone
            // => di tim xem ten clone to nhat la bao nhieu
            int currentCloned = countCloned(name);
            List<OCSCloneableEntity> listToCount = new ArrayList<>();
            for (OCSCloneableEntity cloneEntity : cloneEntities) {
                if (countCloned(cloneEntity.getName()) <= currentCloned + 1) {
                    listToCount.add(cloneEntity);
                }
            }
            if (listToCount.size() == 0) {
                newName = name + "_clone";
            } else {
//                for (int i = 0; i < listToCount.size() + 1; i++) {
//                    String checkName = name + (i == 0 ? "_clone" : ("_clone(" + i + ")"));
//                    if (ocsCloneableRepository.findByNameAndDomainId(checkName, domainId).isEmpty()) {
//                        newName = checkName;
//                        break;
//                    }
//                }
                List<String> listNameClone = listToCount.stream().filter(i -> !i.getName().equals(name + "_clone")).map(i -> i.getName()).collect(Collectors.toList());
//                if (listToCount.size() == listNameClone.size()) {
//                    newName = name + "_clone";
//                } else {
                if (listNameClone.size() == 0) {
                    newName = name + "_clone(1)";
                } else {
                    List<Integer> listIndexOfName = listNameClone.stream().map(i -> {
                        int beginIndex = i.lastIndexOf("(") + 1;
                        try {
                            return Integer.parseInt(i.substring(beginIndex, i.length() - 1));
                        } catch (NumberFormatException e) {
                            return 0;
                        }
                    }).sorted((o1, o2) -> o1 - o2).collect(Collectors.toList());
                    newName = name + "_clone(" + (listIndexOfName.get(listIndexOfName.size() - 1) + 1) + ")";
                }
//                }
            }
        }
        return newName;
    }

    public void moveCategory(Long entityId, Long categoryId) {
        Integer domainId = OCSUtils.getDomain();
        OCSMoveableEntity ocsMoveableEntity = baseRepository.findByIdAndDomainId(entityId, domainId);

        if (!(parentService instanceof CategoryService)) {
            throw new PermissionDeniedException(ErrorMessage.CANT_UPDATE_CATEGORY, Resources.CATEGORY, ErrorKey.CATEGORY_ID);
        }
        if (!ocsMoveableEntity.getParentId().equals(categoryId)) {
            Category category = (Category) parentService.findByIdAndDomainId(categoryId, domainId);

            if (category == null) {
                throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.ENTITY_NOT_FOUND);
            }

            setPosIndex(domainId, ocsMoveableEntity, categoryId);
            ocsMoveableEntity.setCategoryId(categoryId);
        }

        baseRepository.save(ocsMoveableEntity);
    }

    protected int countCloned(String target) {
        Pattern p = Pattern.compile("_clone", Pattern.LITERAL);
        Matcher m = p.matcher(target);
        int count = 0, startIndex = 0;
        while (m.find(startIndex)) {
            count++;
            startIndex = m.start() + 1;
        }
        return count;
    }

    protected void setPosIndex(Integer domainId, OCSMoveableEntity ocsCloneableEntity, Long newParentId) {
        OCSMoveableEntity cloneableEntity = baseRepository.findTopByParentIdAndDomainIdOrderByPosIndexDesc(newParentId, domainId);
        if (cloneableEntity == null || cloneableEntity.getPosIndex() == null) {
            ocsCloneableEntity.setPosIndex(0);
        } else {
            ocsCloneableEntity.setPosIndex(cloneableEntity.getPosIndex() + 1);
        }
    }

    protected void setHasChild(Tree tree) {
        // this method only use to override in Norm -> Offer
    }

    protected Sort sortByValue() {
        return Sort.by(Direction.ASC, "value");
    }

    private String getGroupParent(Tree tree) {
        if (tree instanceof ZoneDTO) {
            return "zone-maps";
        }
        //        if (tree instanceof CategoryDTO) {
        //            return "category";
        //        }
        //        return tree.getTreeType();
        return "category";
    }

    protected abstract CategoryType getCategoryType();

    public Page<OCSMoveableEntity> getListObjectByCategoryId(Long categoryId, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<OCSMoveableEntity> entities = baseRepository.findByParentIdAndDomainId(categoryId, domainId, pageable);
        return entities;
    }

    public void checkCategory(Long categoryId) {
        Integer domainId = OCSUtils.getDomain();
        CategoryType categoryType = getCategoryType();
        Category category = categoryRepositoryInBase.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, "", ErrorKey.CATEGORY_ID);
        }
        if (category.getCategoryType() != categoryType) {
            throw new DataConstrainException(ErrorMessage.CATEGORY_TYPE, "", ErrorKey.Category.CATEGORY_TYPE);
        }
    }

    protected String cloneName(String name, Integer domainId, List<OCSCloneableEntity> clonedEntityNames) {
        List<OCSCloneableEntity> cloneEntities = new ArrayList<>();
        cloneEntities = ocsCloneableRepository.findByNameStartsWithAndDomainId(name + "_clone", domainId);
        Pattern clonedNum = Pattern.compile("_clone(\\([1-9]+\\))?$");
        String newName = "";
        List<OCSCloneableEntity> cloneableEntityList = clonedEntityNames.stream()
            .filter(item -> item.getName().contains(name + "_clone")).collect(Collectors.toList());
        cloneEntities.addAll(cloneableEntityList);
        boolean hasBeenCloned = cloneEntities.stream().anyMatch(
            item -> BooleanUtils.isTrue(clonedNum.matcher(item.getName()).find()));
        if (!hasBeenCloned) {
            // Chua duoc clone bao gio
            newName = name + "_clone";
        } else {
            // Da duoc Clone
            // => di tim xem ten clone to nhat la bao nhieu
            int currentCloned = countCloned(name);
            List<OCSCloneableEntity> listToCount = new ArrayList<>();
            for (OCSCloneableEntity cloneEntity : cloneEntities) {
                if (countCloned(cloneEntity.getName()) <= currentCloned + 1) {
                    listToCount.add(cloneEntity);
                }
            }
            if (listToCount.size() == 0) {
                newName = name + "_clone";
            } else {
//                for (int i = 0; i < listToCount.size() + 1; i++) {
//                    String checkName = name + (i == 0 ? "_clone" : ("_clone(" + i + ")"));
//                    if (ocsCloneableRepository.findByNameAndDomainId(checkName, domainId).isEmpty()) {
//                        newName = checkName;
//                        break;
//                    }
//                }
                List<String> listNameClone = listToCount.stream().filter(i -> !i.getName().equals(name + "_clone")).map(i -> i.getName()).collect(Collectors.toList());
//                if (listToCount.size() == listNameClone.size()) {
//                    newName = name + "_clone";
//                } else {
                if (listNameClone.size() == 0) {
                    newName = name + "_clone(1)";
                } else {
                    List<Integer> listIndexOfName = listNameClone.stream().map(i -> {
                        int beginIndex = i.lastIndexOf("(") + 1;
                        try {
                            return Integer.parseInt(i.substring(beginIndex, i.length() - 1));
                        } catch (NumberFormatException e) {
                            return 0;
                        }
                    }).sorted((o1, o2) -> o1 - o2).collect(Collectors.toList());
                    newName = name + "_clone(" + (listIndexOfName.get(listIndexOfName.size() - 1) + 1) + ")";
                }
//                }
            }
        }
        return newName;
    }

    protected String getResourceName(){
        return "";
    };

    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.DUPLICATED_NAME, "", ErrorKey.NAME);
    }
}
