package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.PepProfile;
import com.mycompany.myapp.domain.Qos;
import com.mycompany.myapp.domain.QosClass;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.QosDTO;
import com.mycompany.myapp.mapper.QosMapper;
import com.mycompany.myapp.repository.PepProfileRepository;
import com.mycompany.myapp.repository.QosClassRepository;
import com.mycompany.myapp.repository.QosRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class QosSerivce {

    @Autowired
    private QosRepository qosRepository;

    @Autowired
    private QosClassRepository qosClassRepository;

    @Autowired
    private PepProfileRepository pepProfileRepository;

    @Autowired
    private QosMapper qosMapper;

    public void deleteQos(Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<PepProfile> listPepProfileUseQos = pepProfileRepository.findByDomainIdAndQosId(domainId, id);
        if(CollectionUtils.isEmpty(listPepProfileUseQos)) {
            qosRepository.deleteByIdAndDomainId(id, domainId);
        }else {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.QOS, ErrorKey.Qos.ID);
        }

    }

    public Page<QosDTO> getAllQos(String name, String qci,
            String mbrul, String mbrdl, String gbrul, String gbrdl,
            String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<QosDTO> lstQos = qosRepository
                .findQos(domainId, name, qci, mbrul, mbrdl, gbrul, gbrdl, description, pageable)
                .map(qosMapper::toDto);
        return lstQos;
    }

    public Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getAllQos(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC, Qos.FieldNames.QOS_ID) : sort);
    }

    public BaseResponseDTO createQos(QosDTO qosDTO) {
        qosDTO.setId(null);
        return saveQos(qosDTO);
    }

    public BaseResponseDTO updateQos(QosDTO qosDTO) {
        Integer domainId = OCSUtils.getDomain();
        Qos qos = qosRepository.findByIdAndDomainId(qosDTO.getId(), domainId);

        if(qos == null) {
            throw new ResourceNotFoundException(ErrorMessage.NOT_FOUND, Resources.QOS, ErrorKey.Qos.ID);
        }

        return saveQos(qosDTO);
    }

    private QosDTO saveQos(QosDTO qosDTO) {
        Integer domainId = OCSUtils.getDomain();
        Qos qos = qosMapper.toEntity(qosDTO);
        qos.setDomainId(domainId);
        checkDuplicateName(qos.getName(), qos.getId());
        String[] qci = qos.getQci().split(":");
        qos.setQci(qci[0]);
        qos = qosRepository.save(qos);
        return qosMapper.toDto(qos);
    }

    public List<QosClass> getListQCI() {
//        List<QosClass> lstQosClass = qosClassRepository.findAll();
//        List<String> lstQci = new ArrayList<>();
//        
//        for (QosClass qosClass : lstQosClass) {
//            String qci = qosClass.getQci() == null ? "" : qosClass.getQci() ;
//            String resourceType = qosClass.getResourceType() == null ? "" : qosClass.getResourceType();
//            String name = qosClass.getName() == null ? "" : qosClass.getName();
//            lstQci.add(qci + ":" + resourceType + ":" + name);
//        }
        return qosClassRepository.findAll();
    }

    @SuppressWarnings("unchecked")
    private void checkDuplicateName(String name, Long id) {
        Integer domainId = OCSUtils.getDomain();
        List<Qos> lstQos = qosRepository.findByNameAndDomainId(name, domainId);
        for (Qos qos : lstQos) {
            if(qos != null && !Objects.equal(id, qos.getId())) {
                throw new DataConstrainException(ErrorMessage.Qos.DUPLICATED_NAME, Resources.QOS, ErrorKey.Qos.NAME);
            }
        }
    }

    public List<Qos> getListAllQos() {
        Integer domainId = OCSUtils.getDomain();
        return qosRepository.findByDomainId(domainId);
    }
}
