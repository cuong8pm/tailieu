package com.mycompany.myapp.service.pricecomponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.swing.text.html.parser.Entity;

import com.mycompany.myapp.dto.actiontype.EventActionTypeMapDTO;
import com.mycompany.myapp.repository.block.BlockRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.domain.pricecomponent.PriceComponentBlockMap;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentBlockMapDTO;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.mapper.pricecomponent.PriceComponentBlockMapMapper;
import com.mycompany.myapp.mapper.pricecomponent.PriceComponentMapper;
import com.mycompany.myapp.repository.action.ActionPriceComponentMapRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentBlockMapRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.action.ActionPriceComponentMapService;
import com.mycompany.myapp.service.clone.AbstractCloneService;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Transactional
@Service
public class PriceComponentService extends AbstractCloneService {

    @Autowired
    private PriceComponentRepository priceComponentRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private List<PriceComponentBlockMapService> priceComponentBlockMapServices;

    @Autowired
    private List<ActionPriceComponentMapService> actionPriceComponentMapServices;

    @Autowired
    private PriceComponentBlockMapRepository priceComponentBlockMapRepository;

    @Autowired
    private PriceComponentMapper priceComponentMapper;

    @Autowired
    private PriceComponentBlockMapMapper priceComponentBlockMapMapper;

    @Autowired
    private ActionPriceComponentMapRepository actionPriceComponentMapReponsitory;

    @Autowired
    private BlockRepository blockRepository;

    @Override
    protected List<CategoryType> dependTypes() {
        return Arrays.asList();
    }

    @Override
    protected List<CategoryType> dependOnTypes() {
        return Arrays.asList(CategoryType.BLOCK);
    }

    @Override
    protected TreeClone getTreeDTO() {
        return new PriceComponentDTO();
    }

    @Override
    public CategoryType getCategoryType() {
        return CategoryType.PRICE_COMPONENT;
    }

    @Override
    protected Integer getCloneNameMaxLength() {
        return PriceComponent.Length.NAME;
    }

    @Override
    protected String getResourceName() {
        return Resources.PRICE_COMPONENT;
    }

    @Override
    protected String getNameCategory() {
        return "Price Components";
    }


    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = priceComponentRepository;
        this.parentService = categoryService;
        this.ocsCloneableRepository = priceComponentRepository;
        this.cloneMappingServices = priceComponentBlockMapServices;
        this.parentDependMappingServices = actionPriceComponentMapServices;
    }

    public Page<PriceComponentDTO> getPriceComponents(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        Page<PriceComponentDTO> page = priceComponentRepository.getPriceComponents(categoryId, name, description, domainId, pageable).map(priceComponentMapper::toDto);
        for (PriceComponentDTO item : page) {
            Integer count = priceComponentBlockMapRepository.countByDomainIdAndParentId(domainId, item.getId());
            item.setHasChild(count > 0);
        }
        return page;
    }

    public PriceComponentDTO getPriceComponentDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        PriceComponent priceComponent = priceComponentRepository.findByIdAndDomainId(id, domainId);
        if (priceComponent == null) {
            throw new DataInvalidException(ErrorMessage.NOT_FOUND, Resources.PRICE_COMPONENT, ErrorKey.ID);
        }
        PriceComponentDTO dto = priceComponentMapper.toDto(priceComponent);
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(dto.getCategoryId(), domainId);
        if (category != null) {
            dto.setCategoryId(category.getId());
            dto.setCategoryName(category.getName());
            dto.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        }
        List<PriceComponentBlockMap> priceComponentBlock = priceComponentBlockMapRepository.findByPriceComponentIdAndDomainIdOrderByPosIndex(id, domainId);
        List<PriceComponentBlockMapDTO> dtos = priceComponentBlockMapMapper.toDto(priceComponentBlock);
        List<Block> blocks = priceComponentBlockMapRepository.getBlocks(id, domainId);
        for (int i = 0; i < dtos.size(); i++) {
            dtos.get(i).setBlockName(blocks.get(i).getName());
            dtos.get(i).setDescription(blocks.get(i).getDescription());
        }
        dto.setBlockMapItems(dtos);
        return dto;
    }

    public PriceComponentDTO createPriceComponent(PriceComponentDTO priceComponentDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkCategoryExist(priceComponentDTO.getCategoryId(), domainId);
        checkDuplicateName(priceComponentDTO.getName(), priceComponentDTO.getId());
        if (!priceComponentDTO.getBlockMapItems().isEmpty()) {
            checkChildBlock(priceComponentDTO.getBlockMapItems());
        }
        PriceComponent entity = priceComponentMapper.toEntity(priceComponentDTO);
        setPosIndex(domainId, entity, entity.getCategoryId());
        entity.setDomainId(domainId);
        entity = priceComponentRepository.save(entity);
        Integer index = 0;

        List<Long> listId = priceComponentDTO.getBlockMapItems().stream().map(PriceComponentBlockMapDTO::getBlockId).collect(Collectors.toList());
        List<PriceComponentBlockMap> blockMaps = new ArrayList<>();
        Boolean result = false;
        for (PriceComponentBlockMapDTO item : priceComponentDTO.getBlockMapItems()) {
            Block block = blockRepository.findByIdAndDomainId(item.getBlockId(), domainId);
            if (block == null) {
                throw new DataConstrainException(ErrorMessage.Block.NOT_FOUND, Resources.BLOCK, ErrorKey.Block.BLOCK_ENTITY);
            }
            PriceComponentBlockMap map = priceComponentBlockMapMapper.toEntity(item);
            map.setPriceComponentId(entity.getId());
            map.setPosIndex(index);
            map.setDomainId(domainId);
            blockMaps.add(map);
            index++;
            if (item.getIsUseForGenerating() == false || (item.getGenneratingType() != null && item.getGenneratingType() == 0)) {
                result = true;
            }
        }
        if (!result) {
            throw new DataInvalidException(ErrorMessage.PriceComponent.GENNERATING_TYPE, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.GENNERATING_TYPE);
        }
        priceComponentBlockMapRepository.saveAll(blockMaps);
        priceComponentDTO = priceComponentMapper.toDto(entity);
        priceComponentDTO.setHasChild(true);
        return priceComponentDTO;
    }

    private void checkChildBlock(List<PriceComponentBlockMapDTO> blockMapItems) {
        Set<Long> BlockIds = new HashSet<>();
        for (PriceComponentBlockMapDTO item : blockMapItems) {
            BlockIds.add(item.getBlockId());
        }
        if (BlockIds.size() != blockMapItems.size()) {
            throw new DataInvalidException(ErrorMessage.PriceComponent.DUPLICATED_BLOCK, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.BLOCK_ID);
        }

    }

    private void checkCategoryExist(Long categoryId, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(categoryId, domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.PRICE_COMPONENT, ErrorKey.CATEGORY_ID);
        }
        if (parentCategory.getCategoryType() != CategoryType.PRICE_COMPONENT) {
            throw new DataConstrainException(ErrorMessage.PriceComponent.CATEGORY_TYPE, Resources.PRICE_COMPONENT, ErrorKey.CATEGORY_ID);
        }

    }

    public PriceComponentDTO updatePriceComponent(PriceComponentDTO priceComponentDTO) {
        Integer domainId = OCSUtils.getDomain();
        checkCategoryExist(priceComponentDTO.getCategoryId(), domainId);
        checkDuplicateName(priceComponentDTO.getName(), priceComponentDTO.getId());
        PriceComponent entity = priceComponentMapper.toEntity(priceComponentDTO);
//        setPosIndex(domainId, entity, entity.getCategoryId());
        entity.setDomainId(domainId);

        List<PriceComponentBlockMapDTO> removes = new ArrayList<>();
        for (PriceComponentBlockMapDTO item : priceComponentDTO.getBlockMapItems()) {
            item.setPriceComponentId(priceComponentDTO.getId());
            item.setDomainId(domainId);
            if (ActionType.DELETE.equals(item.getActionType())) {
                priceComponentBlockMapRepository.deleteByDomainIdAndChildIdAndParentId(domainId, item.getBlockId(), priceComponentDTO.getId());
                removes.add(item);
            }else {
                Block block = blockRepository.findByIdAndDomainId(item.getBlockId(), domainId);
                if (block == null) {
                    throw new DataConstrainException(ErrorMessage.Block.NOT_FOUND, Resources.BLOCK, ErrorKey.Block.BLOCK_ENTITY);
                }
            }
        }
        for (PriceComponentBlockMapDTO item : priceComponentDTO.getBlockMapItems()) {
            item.setPriceComponentId(priceComponentDTO.getId());
            item.setDomainId(domainId);
            if (ActionType.EDIT.equals(item.getActionType())) {
                priceComponentBlockMapRepository.save(priceComponentBlockMapMapper.toEntity(item));
                removes.add(item);
            }
        }
        priceComponentDTO.getBlockMapItems().removeAll(removes);

        Set<Integer> posIndexSet = new HashSet<>();
        Set<Long> blockIdSet = new HashSet<>();
        List<Integer> posIndexList = new ArrayList<>();
        List<Long> blockIdList = new ArrayList<>();
        for (PriceComponentBlockMapDTO item : priceComponentDTO.getBlockMapItems()) {
            posIndexSet.add(item.getPosIndex());
            posIndexList.add(item.getPosIndex());
            blockIdSet.add(item.getBlockId());
            blockIdList.add(item.getBlockId());
        }
        List<Long> blockIdDBs = priceComponentBlockMapRepository.getBlockIds(priceComponentDTO.getId(), domainId);
        blockIdList.addAll(blockIdDBs);
        blockIdSet.addAll(blockIdDBs);
        if (blockIdList.size() != blockIdSet.size()) {
            throw new DataInvalidException(ErrorMessage.PriceComponent.DUPLICATED_BLOCK, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.BLOCK_ID);
        }
        List<Integer> posIndexDBs = priceComponentBlockMapRepository.getPosIndex(priceComponentDTO.getId(), domainId);
        posIndexList.addAll(posIndexDBs);
        posIndexSet.addAll(posIndexDBs);
        if (posIndexList.size() != posIndexSet.size()) {
            throw new DataInvalidException(ErrorMessage.PriceComponent.DUPLICATED_BLOCK_MAP_INDEX, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.INDEX);
        }
        priceComponentBlockMapRepository.saveAll(priceComponentBlockMapMapper.toEntity(priceComponentDTO.getBlockMapItems()));
        entity = priceComponentRepository.save(entity);
        List<PriceComponentBlockMap> componentBlockMaps = priceComponentBlockMapRepository.findByDomainIdAndParentId(domainId, priceComponentDTO.getId());
        if (componentBlockMaps.isEmpty()) {
            throw new DataInvalidException(ErrorMessage.PriceComponent.NOT_EMPTY, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.BLOCK_ITEM);
        }
        Boolean result = false;
        for (PriceComponentBlockMap item : componentBlockMaps) {
            if (item.getIsUseForGenerating() == false || (item.getGenneratingType() != null && item.getGenneratingType() == 0)) {
                result = true;
            }
        }
        if (!result) {
            throw new DataInvalidException(ErrorMessage.PriceComponent.GENNERATING_TYPE, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.GENNERATING_TYPE);
        }
        priceComponentDTO = priceComponentMapper.toDto(entity);
        priceComponentDTO.setHasChild(true);
        return priceComponentDTO;
    }

    public void deletePriceComponent(Long id) {
        Integer domainId = OCSUtils.getDomain();

        Integer count = actionPriceComponentMapReponsitory.countByPriceComponentIdAndDomainId(id, domainId);
        if (count > 0) {
            throw new DataInvalidException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.PRICE_COMPONENT, ErrorKey.PriceComponent.PRICE_COMPONENT_ID);
        }
        priceComponentBlockMapRepository.deleteByParentIdAndDomainId(id, domainId);
        priceComponentRepository.deleteByIdAndDomainId(id, domainId);
    }

    @Override
    public void throwDuplicatedNameException() {
        throw new DataConstrainException(ErrorMessage.PriceComponent.DUPLICATED_NAME, getResourceName(), ErrorKey.NAME);
    }

    @Override
    public void throwResourceNotFoundException() {
        throw new DataConstrainException(ErrorMessage.PriceComponent.NOT_FOUND, getResourceName(), ErrorKey.ID);
    }
}
