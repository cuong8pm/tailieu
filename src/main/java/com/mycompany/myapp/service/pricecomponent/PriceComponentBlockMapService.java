package com.mycompany.myapp.service.pricecomponent;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.mapper.block.BlockMapper;
import com.mycompany.myapp.repository.block.BlockRateTableMapRepository;
import com.mycompany.myapp.repository.pricecomponent.PriceComponentBlockMapRepository;
import com.mycompany.myapp.service.block.BlockService;
import com.mycompany.myapp.service.clone.AbstractCloneMappingService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class PriceComponentBlockMapService extends AbstractCloneMappingService {

    @Autowired
    private PriceComponentBlockMapRepository priceComponentBlockMapRepository;

    @Autowired
    private BlockService blockService;

    @Autowired
    private PriceComponentService priceComponentService;

    @Autowired BlockMapper blockMapper;

    @Autowired
    private BlockRateTableMapRepository blockRateTableMapRepository;

    @Override
    protected String getResource() {
        return Resources.BLOCK;
    }

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.ocsCloneMapRepository = priceComponentBlockMapRepository;
        this.childService = blockService;
        this.parentService = priceComponentService;
        this.parentDependService = priceComponentService;
    }

    public List<BlockDTO> getBlocks(Long priceComponentId) {
        Integer domainId = OCSUtils.getDomain();
        List<Block>  blocks =  priceComponentBlockMapRepository.getBlocks(priceComponentId, domainId);
        List<BlockDTO> blockDtos = blockMapper.toDto(blocks);
        List<Integer> indexs = priceComponentBlockMapRepository.getIndexs(priceComponentId, domainId);
        for(int i =0 ; i < blockDtos.size() ; i ++) {
            blockDtos.get(i).setPosIndex(indexs.get(i));
            blockDtos.get(i).setTreeType(Resources.TreeType.SUB_TEMPLATE);
            blockDtos.get(i).setHasChild(checkHasChild(blockDtos.get(i).getId(), domainId));
        }

        return blockDtos;
    }

    private boolean checkHasChild(Long id, Integer domainId) {
        Integer countReferWithPccRule = blockRateTableMapRepository.countByDomainIdAndParentId(domainId, id);
        if (countReferWithPccRule > 0) {
            return true;
        }
        return false;
    }

    @Override
    protected CategoryType getCategoryType() {
        return priceComponentService.getCategoryType();
    }

    @Override
    public OCSCloneMapRepository getOcsCloneMapRepository() {
        return null;
    }
}
