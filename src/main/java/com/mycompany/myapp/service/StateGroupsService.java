package com.mycompany.myapp.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.StateGroups;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.StateGroupsDTO;
import com.mycompany.myapp.mapper.StateGroupsMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.StateGroupsRepository;
import com.mycompany.myapp.repository.StateTypeRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Transactional
@Service
public class StateGroupsService extends OCSBaseService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private StateGroupsRepository stateGroupsRepository;
    @Autowired
    private StateGroupsMapper stateGroupsMapper;
    @Autowired
    private StateTypeRepository stateTypeRepository;
    @Autowired
    private CategoryService categoryService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = stateGroupsRepository;
        this.parentService = categoryService;
    }
    /**
     * @param stateGroupsDTO
     * @return
     */
    public StateGroupsDTO createStateGroups(StateGroupsDTO stateGroupsDTO) {
        Integer domainId = OCSUtils.getDomain();
//        Convert to Entity
        StateGroups stateGroups = stateGroupsMapper.toEntity(stateGroupsDTO);

//        Check Category
        checkCategoryExist(stateGroupsDTO, domainId);
        checkDuplicateName(stateGroupsDTO.getName(), stateGroupsDTO.getId());

        stateGroups.setDomainId(domainId);
        super.setPosIndex(domainId, stateGroups, stateGroups.getCategoryId());

        stateGroups = stateGroupsRepository.save(stateGroups);

        return stateGroupsMapper.toDto(stateGroups);
    }

    /**
     * @param id
     * @ Delete state_groups
     */
    public void deleteStateGroups(Long id) {
        Integer domainId = OCSUtils.getDomain();
        StateGroups stateGroups = stateGroupsRepository.findByIdAndDomainIdForUpdate(id, domainId);
        checkUsedInStateType(stateGroups.getId());
        stateGroupsRepository.deleteByIdAndDomainId(id, domainId);
    }

    private void checkUsedInStateType(Long id) {
        Integer count = stateTypeRepository.countByStateGroupIdAndDomainId(id, OCSUtils.getDomain());
        if (count > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_ASSOCIATION, Resources.STATEGROUPS, ErrorKey.StateSet.ID);
        }
    }

    /**
     * @param stateGroupsDTO
     * @return function saveEntity
     */
    public StateGroupsDTO updateStateGroups(StateGroupsDTO stateGroupsDTO) {
        Integer domainId = OCSUtils.getDomain();
        StateGroups stateGroupsDB = stateGroupsRepository.findByIdAndDomainIdForUpdate(stateGroupsDTO.getId(), domainId);
        if (stateGroupsDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.StateSet.STATE_GROUP_NOT_FOUND, Resources.STATEGROUPS, ErrorKey.StateSet.ID);
        }
        checkDuplicateName(stateGroupsDTO.getName(), stateGroupsDB.getId());
        checkCategoryExist(stateGroupsDTO, domainId);
        StateGroups stateGroups = stateGroupsMapper.toEntity(stateGroupsDTO);
        stateGroups.setDomainId(domainId);

        if (!Objects.equal(stateGroupsDTO.getCategoryId(), stateGroupsDB.getCategoryId())) {
            setPosIndex(domainId, stateGroups, stateGroupsDTO.getCategoryId());
        }

        stateGroups = stateGroupsRepository.save(stateGroups);
        return stateGroupsMapper.toDto(stateGroups);
    }

    public StateGroupsDTO getStateGroupsDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        StateGroups stateGroups = stateGroupsRepository.findByIdAndDomainId(id, domainId);
        if (stateGroups == null) throw new ResourceNotFoundException(ErrorMessage.StateSet.STATE_GROUP_NOT_FOUND, Resources.STATEGROUPS, ErrorKey.StateSet.STATE_GROUP_ID);
        StateGroupsDTO stateGroupsDTO = stateGroupsMapper.toDto(stateGroups);
        Category category = categoryRepository.findByIdAndDomainId(stateGroupsDTO.getCategoryId(), domainId);
        stateGroupsDTO.setCategoryName(category.getName());
        stateGroupsDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        return stateGroupsDTO;
    }

    /**
     * @param categoryId
     * @return
     */
    public Page<StateGroupsDTO> getStateGroups(Long categoryId, String name, String description, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        StateGroupsDTO stateGroupsDTO = new StateGroupsDTO();
        stateGroupsDTO.setParentId(categoryId);

        checkCategoryExist(stateGroupsDTO, domainId);

        return stateGroupsRepository.findStateGroups(categoryId, domainId, name, description, pageable).map(stateGroupsMapper::toDto);

    }


    /**
     * @param stateGroupsDTO
     * @param domainId
     * @ Check category exist or not
     */
    private void checkCategoryExist(StateGroupsDTO stateGroupsDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(stateGroupsDTO.getParentId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.PARENT_CATEGORY);
        }
        if (parentCategory.getCategoryType() != CategoryType.STATE_GROUP) {
            throw new DataConstrainException(ErrorMessage.StateSet.CATEGORY_GROUP_TYPE, Resources.CATEGORY, ErrorKey.Category.CATEGORY_TYPE);
        }
    }

    public List<StateGroups> getAllStateGroups() {
        return stateGroupsRepository.findByDomainIdOrderByPosIndexAsc(OCSUtils.getDomain());
    }

    public List<StateGroupsDTO> getListStateGroupsByCategoryId(Long categoryId) {
        Integer domainId = OCSUtils.getDomain();
        StateGroupsDTO stateGroupsDTO = new StateGroupsDTO();
        stateGroupsDTO.setParentId(categoryId);
        checkCategoryExist(stateGroupsDTO, domainId);

        return stateGroupsRepository.findByParentIdAndDomainIdOrderByPosIndexAsc(categoryId, domainId).stream().map(stateGroupsMapper::toDto).collect(Collectors.toList());
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.STATE_GROUP;
    }
}
