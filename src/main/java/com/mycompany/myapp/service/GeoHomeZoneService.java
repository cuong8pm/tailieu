package com.mycompany.myapp.service;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Objects;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.GeoHomeZone;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.GeoHomeZoneDTO;
import com.mycompany.myapp.mapper.GeoHomeZoneMapper;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.repository.GeoHomeZoneRepository;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

@Service
@Transactional
public class GeoHomeZoneService extends OCSBaseService {

    @Autowired
    private GeoHomeZoneRepository geoHomeZoneRepository;

    @Autowired
    private GeoHomeZoneMapper geoHomeZoneMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private GeoNetZoneService geoNetZoneService;

    @Autowired
    private CategoryService categoryService;

    @PostConstruct
    @Override
    public void setBaseRepository() {
        this.baseRepository = geoHomeZoneRepository;
        this.parentService = categoryService;
    }

    public Page<GeoHomeZoneDTO> getGeoHomeZones(Long categoryId, String name, String description, String id, Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        int parentCategoryLevel = getCategoryLevel(categoryId) + 1;
        Category category = categoryRepository.findByIdAndDomainId(categoryId, domainId);
        if (category == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
        Page<GeoHomeZoneDTO> geoHomeZoneDTOS =  geoHomeZoneRepository.findGeoHomeZones(categoryId, domainId, name, description, id, pageable).map(geoHomeZoneMapper::toDto);
        for (GeoHomeZoneDTO geoHomeZoneDTO: geoHomeZoneDTOS) {
            geoHomeZoneDTO.setCategoryLevel(parentCategoryLevel);
        }
        return  geoHomeZoneDTOS;
    }

    public GeoHomeZoneDTO getGeoHomeZoneDetail(Long id) {
        Integer domainId = OCSUtils.getDomain();
        GeoHomeZone geoHomeZone = geoHomeZoneRepository.findByIdAndDomainId(id, domainId);
        if (geoHomeZone == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE, ErrorKey.GeoHomeZone.ID);
        }
        Category category = categoryRepository.findByIdAndDomainIdForUpdate(geoHomeZone.getCategoryId(), domainId);
        GeoHomeZoneDTO geoHomeZoneDTO = geoHomeZoneMapper.toDto(geoHomeZone);
        geoHomeZoneDTO.setCategoryName(category.getName());
        geoHomeZoneDTO.setCategoryLevel(getCategoryLevel(category.getId()) + 1);
        Optional<ReferTable> referTable = referTableRepository.findById(Long.valueOf(geoHomeZoneDTO.getGeoHomeZoneType()));
        if(referTable.isPresent()){
            geoHomeZoneDTO.setGeoHomeZoneTypeName(referTable.get().getName());
        }

        return geoHomeZoneDTO;
    }


    public GeoHomeZoneDTO createGeoHomeZone(GeoHomeZoneDTO geoHomeZoneDTO) {
        Integer domainId = OCSUtils.getDomain();

        GeoHomeZone geoHomeZone = geoHomeZoneMapper.toEntity(geoHomeZoneDTO);
        checkCategoryExist(geoHomeZoneDTO, domainId);
        checkDuplicateName(geoHomeZoneDTO.getName(), geoHomeZoneDTO.getId());
        checkDuplicateCode(geoHomeZoneDTO.getGeoHomeZoneCode(), domainId);

        geoHomeZone.setDomainId(domainId);
        super.setPosIndex(domainId, geoHomeZone, geoHomeZoneDTO.getCategoryId());

        geoHomeZone = geoHomeZoneRepository.save(geoHomeZone);

        return geoHomeZoneMapper.toDto(geoHomeZone);
    }

    private void checkDuplicateCode(String geoHomeZoneCode, Integer domainId) {
        List<GeoHomeZone> existGeoHomeZones = null;
        if (geoHomeZoneCode != null) {
            existGeoHomeZones = geoHomeZoneRepository.findByGeoHomeZoneCodeAndDomainId(geoHomeZoneCode, domainId);
        } else {
            existGeoHomeZones = geoHomeZoneRepository.findByGeoHomeZoneCodeNullAndDomainId(domainId);
        }
        if (!CollectionUtils.isEmpty(existGeoHomeZones)) {
            throw new DataConstrainException(ErrorMessage.GeoHomeZone.DUPLICATED_CODE, Resources.GEO_HOME_ZONE,
                    ErrorKey.GeoHomeZone.GEO_CODE);
        }
    }

    // Check category exist or not
    private void checkCategoryExist(GeoHomeZoneDTO geoHomeZoneDTO, Integer domainId) {
        Category parentCategory = categoryRepository.findByIdAndDomainIdForUpdate(geoHomeZoneDTO.getCategoryId(), domainId);
        if (parentCategory == null) {
            throw new ResourceNotFoundException(ErrorMessage.Category.NOT_FOUND, Resources.CATEGORY, ErrorKey.Category.ID);
        }
    }

    public void deleteGeoHomeZone(Long id) {
        Integer domainId = OCSUtils.getDomain();

        if (geoNetZoneService.getGeoNetZones(id, null, null, Pageable.unpaged()).getTotalElements() > 0) {
            throw new DataConstrainException(ErrorMessage.CANNOT_DELETE_HAS_CHILD, "", ErrorKey.ID);
        }
        //check Normalizer use Geo Home Zone
        Long countNormUseGeo = geoHomeZoneRepository.countNormUseGeo(id);
        if (countNormUseGeo > 0) {
            throw new BadRequestAlertException(ErrorMessage.OBJECT_BEING_USED_IN_ANOTHER_OBJECT, Resources.GEO_HOME_ZONE
                , "geoHomeZoneId");
        }
        geoHomeZoneRepository.deleteByIdAndDomainId(id, domainId);
    }

    public GeoHomeZoneDTO updateGeoHomeZone(GeoHomeZoneDTO geoHomeZoneDTO) {
        Integer domainId = OCSUtils.getDomain();
        GeoHomeZone geoHomeZoneDB = geoHomeZoneRepository.findByIdAndDomainIdForUpdate(geoHomeZoneDTO.getId(), domainId);

        if (geoHomeZoneDB == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE, ErrorKey.GeoHomeZone.ID);
        }

        // Lock old category
        checkCategoryExist(geoHomeZoneDTO, domainId);
        checkDuplicateName(geoHomeZoneDTO.getName(), geoHomeZoneDTO.getId());

        // Convert to Entity to save
        GeoHomeZone geoHomeZone = geoHomeZoneMapper.toEntity(geoHomeZoneDTO);
        geoHomeZone.setDomainId(domainId);

        if (!Objects.equal(geoHomeZoneDB.getCategoryId(), geoHomeZoneDTO.getCategoryId())) {
            setPosIndex(domainId, geoHomeZone, geoHomeZoneDTO.getCategoryId());
        }

        geoHomeZone = geoHomeZoneRepository.save(geoHomeZone);
        BaseResponseDTO response = new BaseResponseDTO();
        BeanUtils.copyProperties(geoHomeZone, response);

        return geoHomeZoneDTO;
    }

    public List<ReferTable> getHomeZoneTypes() {
        return referTableRepository.
            getReferTablesByReferType(ReferType.HomeZoneType, sortByValue()) ;
    }

    public void checkGeoHomeZoneExist(Long id) {
        Integer domainId = OCSUtils.getDomain();
        GeoHomeZone geoHomeZone = geoHomeZoneRepository.findByIdAndDomainId(id, domainId);
        if (geoHomeZone == null) {
            throw new ResourceNotFoundException(ErrorMessage.GeoHomeZone.NOT_FOUND, Resources.GEO_HOME_ZONE, ErrorKey.GeoHomeZone.ID);
        }
    }

    public String getNameById(Long geoHomeZoneId){
        checkGeoHomeZoneExist(geoHomeZoneId);
        Integer domainId = OCSUtils.getDomain();
        return geoHomeZoneRepository.findNameByIdAndDomain(geoHomeZoneId,domainId);
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.GEO_HOME_ZONE;
    }
}
