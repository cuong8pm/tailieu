package com.mycompany.myapp.aop.logging;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.SoftException;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.base.Objects;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.utils.IgnoreWildCard;

import io.github.jhipster.config.JHipsterConstants;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Aspect for logging execution of service and repository Spring components.
 *
 * By default, it only runs with the "dev" profile.
 */
@Aspect
public class LoggingAspect {
    private final Environment env;

    public LoggingAspect(Environment env) {
        this.env = env;
    }

    /**
     * Pointcut that matches all repositories, services and Web REST endpoints.
     */
    @Pointcut(
        "within(@org.springframework.stereotype.Repository *)" +
        " || within(@org.springframework.stereotype.Service *)" +
        " || within(@org.springframework.web.bind.annotation.RestController *)"
    )
    public void springBeanPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Pointcut that matches all Spring beans in the application's main packages.
     */
    @Pointcut(
        "within(com.mycompany.myapp.repository..*)" +
        " || within(com.mycompany.myapp.service..*)" +
        " || within(com.mycompany.myapp.web.rest..*)"
    )
    public void applicationPackagePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Retrieves the {@link Logger} associated to the given {@link JoinPoint}.
     *
     * @param joinPoint join point we want the logger for.
     * @return {@link Logger} associated to the given {@link JoinPoint}.
     */
    private Logger logger(JoinPoint joinPoint) {
        return LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
    }

    /**
     * Advice that logs methods throwing exceptions.
     *
     * @param joinPoint join point for advice.
     * @param e exception.
     */
    @AfterThrowing(pointcut = "applicationPackagePointcut() && springBeanPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        if (env.acceptsProfiles(Profiles.of(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT))) {
            logger(joinPoint)
                .error(
                    "Exception in {}() with cause = \'{}\' and exception = \'{}\'",
                    joinPoint.getSignature().getName(),
                    e.getCause() != null ? e.getCause() : "NULL",
                    e.getMessage(),
                    e
                );
        } else {
            logger(joinPoint)
                .error(
                    "Exception in {}() with cause = {}",
                    joinPoint.getSignature().getName(),
                    e.getCause() != null ? e.getCause() : "NULL"
                );
        }
    }

    /**
     * Advice that logs when a method is entered and exited.
     *
     * @param joinPoint join point for advice.
     * @return result.
     * @throws Throwable throws {@link IllegalArgumentException}.
     */
    @Around("applicationPackagePointcut() && springBeanPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger log = logger(joinPoint);
        if (log.isDebugEnabled()) {
            log.debug("Enter: {}() with argument[s] = {}", joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        }
        try {
            Object result = joinPoint.proceed();
            if (log.isDebugEnabled()) {
                log.debug("Exit: {}() with result = {}", joinPoint.getSignature().getName(), result);
            }
            return result;
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument: {} in {}()", Arrays.toString(joinPoint.getArgs()), joinPoint.getSignature().getName());
            throw e;
        }
    }


    @Around("execution(* com.mycompany.myapp.web.rest.*.*(..))")
    public Object replaceWildCards(ProceedingJoinPoint pjp) throws Throwable {
        //get original args
        Object[] args = pjp.getArgs();

        //get all annotations for arguments
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();

        IgnoreWildCard ignoreWildCard = method.getDeclaringClass().getAnnotation(IgnoreWildCard.class);

        String methodName = method.getName();

        JsonView jsonView = method.getAnnotation(JsonView.class);
        boolean containTree = checkContainTree(jsonView);

        Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
        Annotation[][] annotations;
        try {
            annotations = pjp.getTarget().getClass().
                    getMethod(methodName, parameterTypes).getParameterAnnotations();
        } catch (Exception e) {
            throw new SoftException(e);
        }

        Annotation[] methodAnnotations = pjp.getTarget().getClass().
            getMethod(methodName, parameterTypes).getDeclaredAnnotations();
        boolean isPost = Stream.of(methodAnnotations).anyMatch(annotation -> Objects.equal(PostMapping.class, annotation.annotationType()));



        //Find annotated argument
        for (int i = 0; i < args.length; i++) {
            for (Annotation annotation : annotations[i]) {
                if ((annotation.annotationType() == IgnoreWildCard.class || (ignoreWildCard != null && !containTree)) && !isPost) {
                    Object raw = args[i];
                    if (raw instanceof String) {
                        // and replace it with a new value
                        if (!StringUtils.isEmpty(raw.toString())) {
                            args[i] = ((String) raw).replaceAll("%", "\\%").replaceAll("_", "\\_");
                        } else {
                            args[i] = null;
                        }
                    }
                }
            }
        }
        //execute original method with new args
        return pjp.proceed(args);
    }

    private boolean checkContainTree(JsonView jsonView) {
        if (jsonView != null) {
            for (Class<?> object : jsonView.value()) {
                if (Objects.equal(object.getName(), Tree.class.getName())) {
                    return true;
                }
            }
        }
        return false;
    }
}
