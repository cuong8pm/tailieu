package com.mycompany.myapp.aop.logging;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.base.Objects;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.utils.IgnoreWildCard;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.SoftException;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.stream.Stream;

@Aspect
public class OcsAspect {
    private final Environment env;

    public OcsAspect(Environment env) {
        this.env = env;
    }

    @Around("execution(* com.mycompany.myapp.web.rest..*(..))")
    public Object replaceWildCards(ProceedingJoinPoint pjp) throws Throwable {
        //get original args
        Object[] args = pjp.getArgs();

        //get all annotations for arguments
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();

        IgnoreWildCard ignoreWildCard = method.getDeclaringClass().getAnnotation(IgnoreWildCard.class);

        String methodName = method.getName();

        JsonView jsonView = method.getAnnotation(JsonView.class);
        boolean containTree = checkContainTree(jsonView);

        Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
        Annotation[][] annotations;
        try {
            annotations = pjp.getTarget().getClass().
                getMethod(methodName, parameterTypes).getParameterAnnotations();
        } catch (Exception e) {
            throw new SoftException(e);
        }

        Annotation[] methodAnnotations = pjp.getTarget().getClass().
            getMethod(methodName, parameterTypes).getDeclaredAnnotations();
        boolean isPost = Stream.of(methodAnnotations).anyMatch(annotation -> Objects.equal(PostMapping.class, annotation.annotationType()));

        //Find annotated argument
        for (int i = 0; i < args.length; i++) {
            for (Annotation annotation : annotations[i]) {
                if ((annotation.annotationType() == IgnoreWildCard.class || (ignoreWildCard != null && !containTree)) && !isPost) {
                    Object raw = args[i];
                    if (raw instanceof String) {
                        args[i] = ((String) raw).trim();
                        // and replace it with a new value
                        if (!StringUtils.isEmpty(raw.toString())) {
                            args[i] = ((String) raw).replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
                        } else {
                            args[i] = null;
                        }
                    }
                }
            }
        }
        //execute original method with new args
        return pjp.proceed(args);
    }

    private boolean checkContainTree(JsonView jsonView) {
        if (jsonView != null) {
            for (Class<?> object : jsonView.value()) {
                if (Objects.equal(object.getName(), Tree.class.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

}
