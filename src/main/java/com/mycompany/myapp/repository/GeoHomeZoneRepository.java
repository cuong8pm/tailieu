package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.GeoHomeZone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface GeoHomeZoneRepository extends OCSMoveableRepository<GeoHomeZone, Long>, JpaSpecificationExecutor<GeoHomeZone> {
    @Query("SELECT MAX(posIndex) FROM GeoHomeZone WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);


    @Query("SELECT R FROM GeoHomeZone R "
        + "WHERE R.domainId = :domainId "
        + "AND (:categoryId is null or R.categoryId = :categoryId) "
        + "AND (:name is null or R.name LIKE %:name%) "
        + "AND (:id is null or R.tempId LIKE %:id%) "
        + "AND (:description is null or R.remark LIKE %:description%) ")
    Page<GeoHomeZone> findGeoHomeZones(@Param("categoryId") Long categoryId,
                                       @Param("domainId") Integer domainId,
                                       @Param("name") String name,
                                       @Param("description") String description,
                                       @Param("id") String id,
                                       Pageable pageable);

    List<GeoHomeZone> findByGeoHomeZoneCodeAndDomainId(String geoHomeZoneCode, Integer domainId);

    List<GeoHomeZone> findByGeoHomeZoneCodeNullAndDomainId(Integer domainId);

    @Query("SELECT z.name FROM GeoHomeZone z " +
        "WHERE z.id = :geoHomeZoneId " +
        "AND z.domainId = :domainId ")
    String findNameByIdAndDomain(@Param("geoHomeZoneId") Long geoHomeZoneId, @Param("domainId") Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM normalizer_params as np " +
        "WHERE CONFIG_INPUT LIKE CONCAT('zoneValueId:', :geoZoneId, ';dataZoneType:2%') ", nativeQuery = true)
    Long countNormUseGeo(@Param("geoZoneId") Long geoZoneId);
}
