package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.BillingCycleType;
import com.mycompany.myapp.dto.offer.common.DropDownBillingCycleDTO;

public interface BillingCycleTypeRepository extends OCSMoveableRepository<BillingCycleType, Long>, JpaSpecificationExecutor<BillingCycleType> {
    
    @Query("SELECT B FROM BillingCycleType B "
            + "WHERE B.parentId = :parentId "
            + "AND B.domainId = :domainId "
            + "AND (:name IS NULL OR B.name LIKE %:name%) "
            + "AND (:description IS NULL OR B.description LIKE %:description%) ")
    Page<BillingCycleType> findBillingCycles(@Param("parentId") Long parentId, @Param("domainId") Integer domainId,
            @Param("name") String name, @Param("description") String description, Pageable pageable);
    
    @Query("SELECT MAX(posIndex) FROM BillingCycleType WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);
    
    @Query(value = "SELECT BILLING_CYCLE_TYPE_ID as \"id\", BILLING_CYCLE_TYPE_NAME as \"name\" "
            + "FROM billing_cycle_type WHERE DOMAIN_ID = :domainId", nativeQuery = true)
    List<DropDownBillingCycleDTO> findAllBillingCycles(@Param("domainId") Integer domainId);
}
