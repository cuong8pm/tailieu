package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Parameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ParameterRepository extends OCSMoveableRepository<Parameter, Long>, JpaSpecificationExecutor<Parameter> {

    @Query("SELECT MAX(posIndex) FROM Parameter WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

//    Parameter findParameterByIdAndDomainId(Long id, Integer domainId);

    List<Parameter> findParametersByCategoryIdAndDomainId(Long categoryId, Integer domainId);

    @Query("SELECT C FROM Parameter C "
        + "WHERE C.domainId = :domainId "
        + "AND (:parentId is null or C.categoryId = :parentId) "
        + "AND (:name is null or C.name LIKE %:name%) "
        + "AND (:description is null or C.remark LIKE %:description%) ")
    Page<Parameter> findParameters(@Param("parentId") Long parentId,
                                   @Param("domainId") Integer domainId,
                                   @Param("name") String name,
                                   @Param("description") String description,
                                   Pageable pageable);

    @Query("SELECT P from Parameter P where P.name LIKE %:name% and P.domainId = :domainId")
    List<Parameter> findParametersLikeName(@Param("name") String name, @Param(value = "domainId") Integer domainId);

    @Query("SELECT P from Parameter P where P.name LIKE :name% and P.domainId = :domainId")
    List<Parameter> findParametersByName(@Param("name") String name, @Param(value = "domainId") Integer domainId);

    @Query("SELECT P from Parameter P where P.name = :name and P.domainId = :domainId")
    Parameter findParameterByName(@Param(value = "name") String name, @Param(value = "domainId") Integer domainId);

//    Long countByCategoryIdAndDomainId(Long categoryId, Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM normalizer_params as np " +
        "WHERE CONFIG_INPUT LIKE CONCAT('value:', :parameterId, ';isUsingCharacteristic:true;%') " +
        "OR CONFIG_INPUT LIKE CONCAT('start:', :parameterId, ';startIsCharacteristic:true;%') " +
        "OR CONFIG_INPUT LIKE CONCAT('%end:', :parameterId, ';endIsCharacteristic:true;%') " +
        "OR CONFIG_INPUT LIKE CONCAT('parameterID:', :parameterId, ';%')", nativeQuery = true)
    Long countNormUseChar(@Param("parameterId") Long parameterId);

}
