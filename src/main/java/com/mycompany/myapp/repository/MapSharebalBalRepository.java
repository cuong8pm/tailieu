package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.MapSharebalBal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface MapSharebalBalRepository extends OCSMoveableRepository<MapSharebalBal, Long>, JpaSpecificationExecutor<MapSharebalBal> {
    @Query("select ms from MapSharebalBal ms "
        + "where ms.domainId = :domainId "
        + "AND (:parentId is null or ms.categoryId = :parentId) "
        + "AND (:name is null or ms.name LIKE %:name%) "
        + "AND (:description is null or ms.remark LIKE %:description%) ")
    Page<MapSharebalBal> findMapSharebalBals(@Param("parentId") Long parentId,@Param("domainId") Integer domainId,@Param("name") String name,@Param("description") String description, Pageable pageable);

    @Query("SELECT MAX(posIndex) FROM MapSharebalBal WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

    @Query("SELECT CASE WHEN COUNT(ms.id) > 0 THEN true ELSE false END " +
        "FROM MapSharebalBal ms " +
        "WHERE ms.domainId = :domainId " +
        "AND ms.shareBalTypeId = :toBalanceId " +
        "AND ms.balTypeId = :fromBalanceId")
    boolean existsByDomainIdAndFromBalanceIdAndToBalanceId(@Param("domainId") Integer domainId,@Param("fromBalanceId") Long fromBalanceId,@Param("toBalanceId") Long toBalanceId);

    @Query("SELECT CASE WHEN COUNT(ms.id) > 0 THEN true ELSE false END " +
        "FROM MapSharebalBal ms " +
        "WHERE ms.domainId = :domainId " +
        "AND (ms.shareBalTypeId = :balanceId " +
        "OR ms.balTypeId = :balanceId)")
    boolean existsByDomainIdAndBalanceId(@Param("domainId") Integer domainId,@Param("balanceId") Long balanceId);

    @Query("SELECT ms " +
        "FROM MapSharebalBal ms " +
        "WHERE ms.domainId = :domainId " +
        "AND (ms.mappingTypeId = :mappingTypeId " +
        "AND ms.balTypeId = :fromBalanceId)")
    List<MapSharebalBal> findByDomainIdAndFromBalanceIdAndMappingType(@Param("domainId") Integer domainId,
                                                                      @Param("fromBalanceId") Long fromBalanceId, @Param("mappingTypeId") Integer mappingTypeId);
}
