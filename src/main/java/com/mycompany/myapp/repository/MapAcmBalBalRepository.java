package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.MapAcmBalBal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MapAcmBalBalRepository extends OCSMoveableRepository<MapAcmBalBal, Long>, JpaSpecificationExecutor<MapAcmBalBal> {

    @Query("select ms from MapAcmBalBal ms "
        + "where ms.domainId = :domainId "
        + "AND (:parentId is null or ms.categoryId = :parentId) "
        + "AND (:name is null or ms.name LIKE %:name%) "
        + "AND (:description is null or ms.description LIKE %:description%) ")
    Page<MapAcmBalBal> findMapAcmBalBals(@Param("parentId") Long parentId, @Param("domainId") Integer domainId, @Param("name") String name, @Param("description") String description, Pageable pageable);

    @Query("SELECT MAX(posIndex) FROM MapAcmBalBal WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

    @Query("SELECT DISTINCT ms " +
        "FROM MapAcmBalBal ms " +
        "WHERE ms.domainId = :domainId " +
        "AND (ms.toBalanceId = :toBalanceId " +
        "OR ms.fromBalanceId = :fromBalanceId)")
    List<MapAcmBalBal> findByDomainIdAndFromBalanceIdAndToBalanceId(@Param("domainId") Integer domainId, @Param(
        "fromBalanceId") Long fromBalanceId, @Param("toBalanceId") Long toBalanceId);

    @Query("SELECT CASE WHEN COUNT(ms.id) > 0 THEN true ELSE false END " +
        "FROM MapAcmBalBal ms " +
        "WHERE ms.domainId = :domainId " +
        "AND (ms.fromBalanceId = :balanceId " +
        "OR ms.toBalanceId = :balanceId)")
    boolean existsByDomainIdAndBalanceId(@Param("domainId") Integer domainId,@Param("balanceId") Long balanceId);
}
