package com.mycompany.myapp.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.ZoneData;

public interface ZoneDataRepository extends OCSMoveableRepository<ZoneData, Long>, JpaSpecificationExecutor<ZoneData> {
    @Query("SELECT C FROM ZoneData C "
        + "WHERE C.domainId = :domainId "
        + "AND (:zoneDataValue is null or C.zoneDataValue like %:zoneDataValue%) "
        + "AND (:updateDate is null or function('date_format', C.tempUpdateDate,'%d/%m/%Y %H:%i:%s') like %:updateDate%) "
        + "AND (:zoneId is null or C.zoneId = :zoneId) "
        + "AND (:id is null or C.tempId like %:id%) ")
    Page<ZoneData> findZoneDatas(@Param("zoneId") Long zoneId,
                                 @Param("domainId") Integer domainId,
                                 @Param("zoneDataValue") String zoneDataValue,
                                 @Param("updateDate") String updateDate,
                                 @Param("id") String id,
                                 Pageable pageable);

//    ZoneData findByIdAndDomainId(Long id, Integer domainId);

    ZoneData findByZoneDataValueAndDomainId(String zoneDataValue, Integer domainId);
    
    ZoneData findByZoneDataValueAndZoneIdAndDomainId(String zoneDataValue, Long zoneId, Integer domainId);

    void deleteByIdInAndDomainId(List<Long> ids, Integer domainId);

//    @Lock(LockModeType.PESSIMISTIC_WRITE)
//    default ZoneData findByIdAndDomainIdForUpdate(Long id, Integer domainId) {
//        return findByIdAndDomainId(id, domainId);
//    }

//    List<ZoneData> findByDomainId(Integer domainId);
    
    List<ZoneData> findByZoneIdAndZoneDataValueIn(Long zoneId, Collection<String> zoneDataValues);
    
    List<ZoneData> findByZoneIdAndDomainId(Long zoneId, Integer domainId);
}
