package com.mycompany.myapp.repository.pricecomponent;

import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.pricecomponent.PriceComponentBlockMap;
import com.mycompany.myapp.repository.OCSCloneMapRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceComponentBlockMapRepository extends JpaRepository<PriceComponentBlockMap, Long>, OCSCloneMapRepository<PriceComponentBlockMap, Long> {

    @Query(value = " SELECT M.blockId "
            + " FROM PriceComponentBlockMap M "
            + " WHERE M.domainId = :domainId "
            + "   AND M.priceComponentId = :priceComponentId ")
    List<Long> getBlockIds(@Param("priceComponentId") Long priceComponentId, @Param("domainId") Integer domainId);

    @Query(value = " SELECT M.posIndex "
            + " FROM PriceComponentBlockMap M "
            + " WHERE M.domainId = :domainId "
            + "   AND M.priceComponentId = :priceComponentId ")
    List<Integer> getPosIndex(@Param("priceComponentId") Long priceComponentId, @Param("domainId") Integer domainId);

    void deleteByParentIdAndDomainId(Long parentId , Integer DomainId);
    
    @Query(value = "SELECT PM.blockId "
            + " FROM PriceComponentBlockMap PM "
            + " WHERE PM.priceComponentId = :priceComponentId AND PM.domainId= :domainId " )
    List<Long> getRateTableIds(@Param("priceComponentId") Long ratingFilterId, @Param("domainId") Integer domainId);
    
    @Query(value = "select B "
            + " FROM PriceComponentBlockMap PM "
            + "   JOIN Block B on B.id = PM.blockId "
            + " WHERE PM.priceComponentId = :priceComponentId "
            + "   AND  PM.domainId = :domainId "
            + " ORDER BY PM.posIndex ASC ")
    List<Block> getBlocks(@Param("priceComponentId") Long priceComponentId, @Param("domainId") Integer domainId);
    
    @Query(value = "SELECT PM.posIndex "
            + " FROM PriceComponentBlockMap PM "
            + " WHERE PM.priceComponentId = :priceComponentId "
            + "   AND PM.domainId = :domainId "
            + "  ORDER BY PM.posIndex ASC ")
    List<Integer> getIndexs(@Param("priceComponentId") Long priceComponentId, @Param("domainId") Integer domainId);
    
    List<PriceComponentBlockMap> findByPriceComponentIdAndDomainIdOrderByPosIndex(Long priceComponentId , Integer DomainId );
    
    @Override
    <S extends PriceComponentBlockMap> S save(S entity);

}
