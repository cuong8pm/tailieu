package com.mycompany.myapp.repository.pricecomponent;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface PriceComponentRepository extends OCSCloneableRepository<PriceComponent, Long>, JpaSpecificationExecutor<PriceComponent> {

    @Query(value = " SELECT P "
            + " FROM PriceComponent P "
            + " WHERE P.domainId = :domainId "
            + "   AND (:parentId is null or P.parentId = :parentId) "
            + "   AND (:name is null or P.name LIKE %:name%) "
            + "   AND (:description is null or P.description LIKE %:description%)")
    Page<PriceComponent> getPriceComponents(@Param("parentId") Long categoryId, @Param("name") String name, @Param("description") String description, @Param("domainId") Integer domainId, Pageable pageable);

}
