package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.eventProcessing.FunctionParam;
import com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO;
import com.mycompany.myapp.dto.eventProcessing.FunctionParameterDTO;
import com.mycompany.myapp.repository.OCSBaseRepository;
import com.mycompany.myapp.repository.OCSCloneableRepository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FunctionParamRepository extends OCSCloneableRepository<FunctionParam, Long>, JpaSpecificationExecutor<FunctionParam> {
    List<FunctionParam> findAllByFunctionIdAndDomainId(Long functionId, Integer domainId);

    @Query(
        value = "select fp from FunctionParam fp where fp.functionId = :functionId and fp.name like %:name% and fp.domainId = :domainId "
    )
    Page<FunctionParam> findAllByFunctionIdAndNameAndDomainId(
        @Param(value = "functionId") Long functionId,
        @Param(value = "name") String name,
        @Param(value = "domainId") Integer domainId,
        Pageable pageable
    );

    List<FunctionParam> findAllByFunctionIdAndNameAndDomainId(Long functionId, String name, Integer domainId);

    @Query("select max(fp.indexOrder) from FunctionParam fp where fp.functionId = :functionId and fp.domainId = :domainId")
    Integer getMaxFunctionParamIdByFunctionIdAndDomainId(@Param(value = "functionId") Long functionId,
                                                         @Param(value = "domainId") Integer domainId);

    List<FunctionParam> findAllByFunctionIdAndDomainIdAndEnable(Long functionId, Integer domainId, boolean enable);

    @Query(" SELECT FP.id as id,"
        + "   FP.name as name,"
        + "   FP.dataType as dataType,"
        + "   RT.name as dataTypeName, "
        + "   FPI.value as value, "
        + "   FPI.valueType as valueType, "
        + " FP.enable as enable "
        + " FROM FunctionParam FP "
        + "   LEFT JOIN ReferTable RT ON FP.dataType = RT.value AND RT.referType = :referType "
        + "   LEFT JOIN FunctionParamInstance FPI ON FP.id =FPI.functionParameterId AND FPI.ownerType = :ownerType AND  FPI.ownerId = :id "
        + " WHERE FP.domainId = :domainId "
        + "   AND FP.enable is true " +
        "   AND FP.functionId = :functionId "
        + " ORDER BY FP.indexOrder ASC ")
    List<FunctionParameterDTO> getListFunctionParamDetail(@Param("id") Long id,
                                                          @Param("domainId") Integer domainId,
                                                          @Param("referType") ReferType referType,
                                                          @Param("ownerType") Integer ownerType,
                                                          @Param("functionId") Long functionId);

    @Query(value = "select new com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO(" +
        " FP.id, FP.name, FP.functionId, FP.indexOrder, FP.dataType, RT.name, FPI.valueType, FPI.value, FP.enable)"
        + " FROM FunctionParam FP "
        + "   LEFT JOIN ReferTable RT ON FP.dataType = RT.value AND RT.referType = :referType "
        + "   LEFT JOIN FunctionParamInstance FPI ON FP.id =FPI.functionParameterId AND FPI.ownerType = :ownerType AND  FPI.ownerId = :ownerId "
        + " WHERE FP.domainId = :domainId "
        + "   AND FP.functionId = :functionId "
        + " ORDER BY FP.indexOrder ASC ")
    List<FunctionParamDTO> getListFunctionParam(@Param("ownerId") Long id,
                                                @Param("domainId") Integer domainId,
                                                @Param("referType") ReferType referType,
                                                @Param("ownerType") Integer ownerType,
                                                @Param("functionId") Long functionId);

    @Query(value = "select new com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO(" +
        " FP.id, FP.name, FP.functionId, FP.indexOrder, FP.dataType, RT.name, FPI.valueType, FPI.value, FP.enable)"
        + " FROM FunctionParam FP "
        + "   LEFT JOIN ReferTable RT ON FP.dataType = RT.value AND RT.referType = :referType "
        + "   LEFT JOIN FunctionParamInstance FPI ON FP.id =FPI.functionParameterId AND FPI.ownerType = :ownerType AND  FPI.ownerId = :ownerId "
        + " WHERE FP.domainId = :domainId "
        + "   AND FP.functionId = :functionId "
        + "   AND FP.enable = true "
        + " ORDER BY FP.indexOrder ASC ")
    List<FunctionParamDTO> getListFunctionParamEnable(@Param("ownerId") Long id,
                                                      @Param("domainId") Integer domainId,
                                                      @Param("referType") ReferType referType,
                                                      @Param("ownerType") Integer ownerType,
                                                      @Param("functionId") Long functionId);

    List<FunctionParam> findAllByFunctionIdAndDomainIdOrderByIndexOrderAsc(Long functionId, Integer domainId);

    List<FunctionParam> findAllByFunctionIdAndDomainIdOrderByIndexOrder(Long functionId, Integer domainId);
}
