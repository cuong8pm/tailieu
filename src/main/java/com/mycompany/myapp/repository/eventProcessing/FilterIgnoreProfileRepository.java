package com.mycompany.myapp.repository.eventProcessing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.domain.eventProcessing.FilterIgnoreProfile;

import java.util.List;

public interface FilterIgnoreProfileRepository
        extends JpaRepository<FilterIgnoreProfile, Long>, JpaSpecificationExecutor<FilterIgnoreProfile> {

    void deleteByFilterIdAndReferTableId(Long filterId, Long referTableId);

    List<FilterIgnoreProfile> findAllByFilterIdInAndDomainId( List<Long> filterIds, Integer domainId);

    void deleteAllByFilterIdInAndDomainId(List<Long> filterIds, Integer domainId);
}
