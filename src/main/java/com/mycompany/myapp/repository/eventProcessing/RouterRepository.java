package com.mycompany.myapp.repository.eventProcessing;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.eventProcessing.Router;
import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface RouterRepository extends OCSCloneableRepository<Router, Long>, JpaSpecificationExecutor<Router> {
    @Query(
        value = " SELECT R " +
        " FROM Router R " +
        " WHERE R.domainId = :domainId " +
        "   AND (:parentId is null or R.parentId = :parentId) " +
        "   AND (:name is null or R.name LIKE %:name%) " +
        "   AND (:description is null or R.description LIKE %:description%)"
    )
    Page<Router> getRouters(
        @Param("domainId") Integer domainId,
        @Param("parentId") Long categoryId,
        @Param("name") String name,
        @Param("description") String description,
        Pageable pageable
    );

    @Query(
        value = "select r from Router r where r.id in " +
        "(select btrm.routerId from BuilderTypeRouterMap btrm where btrm.builderTypeId = :builderTypeId)"
    )
    Page<Router> getRouterByBuilderTypeId(@Param("builderTypeId") Long builderTypeId, Pageable pageable);

    Integer countRoutersByIdIn(List<Long> ids);

    @Query(value = "select count(f.id) from Filter f where f.routerId = :routerId and f.domainId = :domainId ")
    Integer countFilterByRouterId(@Param(value = "routerId") Long routerId,@Param(value = "domainId") Integer domainId);
}
