package com.mycompany.myapp.repository.eventProcessing;


import com.mycompany.myapp.domain.eventProcessing.Expression;
import com.mycompany.myapp.dto.eventProcessing.ExpressionConditionDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExpressionRepository extends OCSCloneableRepository<Expression, Long>, JpaSpecificationExecutor<Expression> {
    Integer countAllByInputTypeAndLinkedIdAndDomainId(Integer inputType, Long linkedId, Integer domainId);

    @Query(
        value = "select new com.mycompany.myapp.dto.eventProcessing.ExpressionConditionDTO( " +
            " e.id, e.input, e.value , r.value, r.name, e.valueType , e.dataType, r2.name, e.inputType , r1.name,  e.linkedId, f.name) " +
            " from Expression e " +
            " left join ReferTable r on e.operator = r.value and ((r.referType = 48 and e.dataType = 1) or ( e.dataType <> 1 and r.referType = 42))" +
            " left join ReferTable r1 on e.inputType = r1.value and r1.referType = 41" +
            " left join ReferTable r2 on e.dataType = r2.value and r2.referType = 43" +
            " left join Function f on e.linkedId = f.id and e.inputType = 1 " +
            " where e.id in (select efm.expressionId from ExpressionFilterMap efm where efm.filterId = :filterId)"

    )
    Page<ExpressionConditionDTO> getExpressionsByFilterId(@Param(value = "filterId") Long filterId, Pageable pageable);

    @Query(
        value = "select new com.mycompany.myapp.dto.eventProcessing.ExpressionConditionDTO( " +
            " e.id, " +
            " case when e.input is null then f.name else e.input  end  "+
            ", e.value , r.value, r.name, e.valueType , e.dataType, r2.name, e.inputType , r1.name,  e.linkedId, f.name) " +
            " from Expression e " +
            " left join ReferTable r on e.operator = r.value and ((r.referType = 48 and e.dataType = 1) or ( e.dataType <> 1 and r.referType = 42))" +
            " left join ReferTable r1 on e.inputType = r1.value and r1.referType = 41" +
            " left join ReferTable r2 on e.dataType = r2.value and r2.referType = 43" +
            " left join Function f on e.linkedId = f.id and e.inputType = 1 " +
            " inner join ExpressionFilterMap efm on efm.filterId = :filterId and e.id = efm.expressionId " +
            " GROUP BY e.id, e.input, e.value, r.value, r.name, e.valueType, e.dataType, r2.name, e.inputType , r1.name,  e.linkedId, f.name "
    )
    List<ExpressionConditionDTO> getListExpressionsByFilterId(@Param(value = "filterId") Long filterId);
    @Query(
        value = "select new com.mycompany.myapp.dto.eventProcessing.ExpressionConditionDTO( " +
            " e.id, e.input, e.value , r.value, r.name, e.valueType , e.dataType, r2.name, e.inputType , r1.name,  e.linkedId, f.name) " +
            " from Expression e " +
            " left join ReferTable r on e.operator = r.value and ((r.referType = 48 and e.dataType = 1) or ( e.dataType <> 1 and r.referType = 42))" +
            " left join ReferTable r1 on e.inputType = r1.value and r1.referType = 41" +
            " left join ReferTable r2 on e.dataType = r2.value and r2.referType = 43" +
            " left join Function f on e.linkedId = f.id and e.inputType = 1 " +
            " where e.id in (select ecm.expressionId from ExpressionConditionMap ecm where ecm.conditionId = :conditionId)"

    )
    Page<ExpressionConditionDTO> getExpressionByConditionId(@Param(value = "conditionId") Long conditionId, Pageable pageable);

    @Query(
        value = "select new com.mycompany.myapp.dto.eventProcessing.ExpressionConditionDTO( " +
            " e.id," +
            " case when e.input is null then f.name else e.input  end , "+
            " e.value , r.value, r.name, e.valueType , e.dataType, r2.name, e.inputType , r1.name,  e.linkedId, f.name) " +
            " from Expression e " +
            " left join ReferTable r on e.operator = r.value and ((r.referType = 48 and e.dataType = 1) or ( e.dataType <> 1 and r.referType = 42))" +
            " left join ReferTable r1 on e.inputType = r1.value and r1.referType = 41" +
            " left join ReferTable r2 on e.dataType = r2.value and r2.referType = 43" +
            " left join Function f on e.linkedId = f.id and e.inputType = 1 " +
            " inner join ExpressionConditionMap efm on efm.conditionId = :conditionId and e.id = efm.expressionId " +
            " GROUP BY e.id, e.input, e.value, r.value, r.name, e.valueType, e.dataType, r2.name, e.inputType , r1.name,  e.linkedId, f.name "
    )
    List<ExpressionConditionDTO> getListExpressionByConditionId(@Param(value = "conditionId") Long conditionId);

    Expression findFirstByIdAndLinkedIdAndDomainIdAndInputType(Long id, Long linkedId, Integer domainId, Integer inputType);

    Boolean existsExpressionByLinkedIdAndInputTypeAndDomainId(@Param("linkedId") Long linkedId, @Param("inputType") Integer inputType, @Param("domainId") Integer domainId);

}
