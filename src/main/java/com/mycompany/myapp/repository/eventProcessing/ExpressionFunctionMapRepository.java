package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ExpressionFunctionMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

public interface ExpressionFunctionMapRepository extends JpaRepository<ExpressionFunctionMap, Long>, JpaSpecificationExecutor<ExpressionFunctionMap> {

}
