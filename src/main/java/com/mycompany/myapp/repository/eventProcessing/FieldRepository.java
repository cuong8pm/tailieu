package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Field;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FieldRepository extends JpaRepository<Field, Long>, JpaSpecificationExecutor<Field> {

    @Query(" SELECT F "
            + " FROM Field F "
            + " WHERE F.domainId = :domainId "
            + "   AND (:name is null or F.name LIKE %:name%) "
            + "   AND F.id NOT IN ( "
            + "    SELECT FF.fieldId "
            + "    FROM FilterField FF "
            + "    WHERE FF.filterId = :id "
            + "      AND FF.domainId = :domainId ) ")
    List<Field> findByFilterIdAndDomainId(@Param("id") Long id, @Param("name") String name,
            @Param("domainId") Integer domainId);

    Field findByIdAndDomainId(Long fieldId, Integer domainId);

}
