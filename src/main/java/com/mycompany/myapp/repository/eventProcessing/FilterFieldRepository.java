package com.mycompany.myapp.repository.eventProcessing;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.eventProcessing.FilterField;
import com.mycompany.myapp.dto.eventProcessing.FilterFieldDTO;

public interface FilterFieldRepository extends JpaRepository<FilterField, Long>, JpaSpecificationExecutor<FilterField> {
    Integer countFilterFieldByValueTypeAndLinkedIdAndDomainId(Integer valueType, Long linkedId,Integer domainId);

    @Query(" SELECT new com.mycompany.myapp.dto.eventProcessing.FilterFieldDTO( "
            + "   FF.id, FF.filterId, FF.fieldId, F.name, FF.value, FF.valueType, FF.dataType, r.name, FF.inputType, FF.linkedId) "
            + " FROM FilterField FF "
            + "   INNER JOIN Field F ON FF.fieldId = F.id "
            + "   LEFT JOIN ReferTable r on FF.dataType = r.value and r.referType = 43 "
            + " WHERE FF.filterId = :filterId "
            + "   AND (:name is null or F.name like %:name%) "
            + "   AND FF.domainId = :domainId " )
    Page<FilterFieldDTO> getListFilterFields(@Param("filterId") Long filterId, @Param("name") String name,
            Pageable pageable, @Param("domainId") Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    FilterField findByIdAndDomainId(Long id, Integer domainId);

    void deleteAllByDomainIdAndFilterIdIn(Integer domainId, List<Long> filterIds);

    List<FilterField> findAllByDomainIdAndFilterIdIn(Integer domainId, List<Long> filterIds);

    List<FilterField> findAllByDomainIdAndValueTypeAndLinkedId(Integer domainId,Integer valueType, Long linkedId);
    /// với table FilterField lưu functionId =  linkedId với valueType = 3
    Boolean existsFilterFieldByLinkedIdAndValueTypeAndDomainId(@Param("linkedId") Long LinkedId,@Param("valueType") Integer valueType,  @Param("domainId") Integer domainId);

}
