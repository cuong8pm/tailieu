package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.VerificationItem;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VerificationItemRepository extends JpaRepository<VerificationItem, Long>, JpaSpecificationExecutor<VerificationItem> {
    @Query(
        value = "select vi from VerificationItem vi where vi.verificationId = :verificationId and vi.domainId = :domainId " +
            "and (:itemId is null or vi.itemId LIKE %:itemId%)"
    )
    Page<VerificationItem> findVerificationItemWithPage(
        @Param(value = "verificationId") Long verificationId,
        @Param(value = "domainId") Integer domainId,
        @Param(value = "itemId") String itemId,
        Pageable pageable
    );

    VerificationItem findByIdAndDomainId(Long id, Integer domainId);

    List<VerificationItem> findAllByVerificationIdAndDomainId(Long verificationId, Integer domainId);

    @Query(
        value = "select vi from VerificationItem vi where vi.verificationId = :verificationId and (:item is null or vi.itemId like %:item%)"
    )
    List<VerificationItem> findVerificationItem(@Param(value = "verificationId") Long verificationId, @Param("item") String item);

    @Query(
        value = " select vi " +
                " from   VerificationItem vi " +
                " where  vi.verificationId = :verificationId " +
                "   and vi.domainId = :domainId " +
                "   and (:itemId is null or vi.itemId like %:itemId%) "
    )
    List<VerificationItem> findAllByVerificationIdAndDomainIdAndItem(@Param(value = "verificationId") Long verificationId,
                                                                     @Param(value = "domainId") Integer domainId,
                                                                     @Param(value = "itemId") String itemId);


}
