package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.RouterType;
import com.mycompany.myapp.dto.eventProcessing.DropdownDTO;
import com.mycompany.myapp.repository.OCSBaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RouterTypeRepository extends OCSBaseRepository<RouterType, Long>, JpaSpecificationExecutor<RouterType> {

    @Query(value = "select rt.id as id, rt.name as name from RouterType rt where rt.domainId = :domainId")
    List<DropdownDTO> findAllByDomainId(@Param(value = "domainId") Integer domainId);
}
