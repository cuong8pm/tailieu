package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FieldIdOcs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FieldIdOcsRepository extends JpaRepository<FieldIdOcs, Long>, JpaSpecificationExecutor<FieldIdOcs> {

}
