package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventPolicy;
import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EventPolicyRepository extends OCSCloneableRepository<EventPolicy, Long>, JpaSpecificationExecutor<EventPolicy> {

    @Query(value = "select c.eventPolicyId as id, count(c.id) as countChild from Filter c where c.eventPolicyId in (:ids) and c.domainId = :domainId group by c.eventPolicyId")
    List<CountChildrenByParentDTO> countChildrenByParentId(@Param("ids") List<Long> ids, @Param("domainId") Integer domainId);

    @Query(value = "select f from EventPolicy f "
        + "where f.domainId = :domainId "
        + "AND (:categoryId is null or f.categoryId = :categoryId) "
        + "AND (:name is null or f.name LIKE %:name%) "
        + "AND (:description is null or f.description LIKE %:description%) ")
    Page<EventPolicy> getListEventPolicyPaging(@Param(value = "domainId") Integer domainId,
                                     @Param(value = "name") String name,
                                     @Param(value = "description") String description,
                                     @Param(value = "categoryId") Long categoryId,
                                     Pageable pageable);

    @Query(value = "select count(f) from Formula f where f.triggerIds = :eventId and f.domainId = :domainId ")
    Integer countEventUsedInFormula(@Param(value = "eventId")String eventId, @Param(value = "domainId")Integer domainId);

    @Query(value = "select count(f) from ThresholdTriggerMap f where f.triggerOcsId = :eventId and f.domainId = :domainId ")
    Integer countEventUsedInThreshold(@Param(value = "eventId")Long eventId, @Param(value = "domainId")Integer domainId);
}
