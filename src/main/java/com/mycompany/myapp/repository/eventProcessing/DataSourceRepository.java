package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.DataSource;
import com.mycompany.myapp.dto.eventProcessing.DropdownDTO;
import com.mycompany.myapp.repository.OCSBaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DataSourceRepository extends OCSBaseRepository<DataSource, Long>, JpaSpecificationExecutor<DataSource> {

}
