package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Condition;
import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.dto.eventProcessing.ConditionDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.eventProcessing.Condition;
import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface ConditionRepository extends OCSCloneableRepository<Condition, Long>, JpaSpecificationExecutor<Condition> {
    @Query(
        value = " SELECT R " +
        " FROM Condition R " +
        " WHERE R.domainId = :domainId " +
        "   AND (:parentId is null or R.parentId = :parentId) " +
        "   AND (:name is null or R.name LIKE %:name%) " +
        "   AND (:description is null or R.description LIKE %:description%)"
    )
    Page<Condition> getConditions(
        @Param("parentId") Long categoryId,
        @Param("name") String name,
        @Param("description") String description,
        @Param("domainId") Integer domainId,
        Pageable pageable
    );

    @Query(
        value = "select distinct c from Condition c " +
        "join ExpressionConditionMap ecm on c.id = ecm.conditionId " +
        "join Expression e on ecm.expressionId = e.id " +
        "where e.inputType = 1 and e.linkedId = :functionId order by c.id "
    )
    List<Condition> getListConditionsUsingFunction(@Param(value = "functionId") Long functionId);

    Integer countAllByFilterIdAndDomainId(Long filterId, Integer domainId);

    List<Condition> findAllByFilterIdAndDomainId(Long filterId, Integer domainId);

    List<Condition> findAllByFilterIdInAndDomainId(List<Long> filterIds, Integer domainId);

    @Query(" SELECT DISTINCT M.conditionId "
            + " FROM ExpressionConditionMap M "
            + "   INNER JOIN Expression E on M.expressionId = E.id "
            + " WHERE (:input is null OR E.input like :input) "
            + "    AND E.dataType = :dataType "
            + "    AND E.inputType = :inputType "
            + "    AND (:linkedId is null OR E.linkedId = :linkedId) "
            + "    AND (:value is null OR E.value like %:value%) "
            + "    AND M.domainId = :domainId ")
    List<Long> findIdByExpression(@Param("input") String input, @Param("dataType") Integer dataType,
            @Param("inputType") Integer inputType, @Param("linkedId") Long linkedId,
            @Param("value") String value, @Param("domainId") Integer domainId);


    @Query(value = "select c.filterId as id, count(c.id) as countChild from Condition c" +
        " where c.filterId in :filterIds and c.domainId = :domainId group by c.filterId")
    List<CountChildrenByParentDTO> countChildrenByParentId(@Param(value = "filterIds") List<Long> filterIds,
                                                           @Param(value = "domainId")Integer domainId);

    @Query(value = "SELECT new com.mycompany.myapp.dto.eventProcessing.ConditionDTO(c.id, c.name, c.filterId, f.name, c.description, c.posIndex, c.modifyDate)" +
        " from Condition c " +
        " join Filter f on f.id = c.filterId" +
        " where f.eventPolicyId = :eventPolicyId and f.domainId = :domainId " +
        " and c.domainId = :domainId")
    Page<ConditionDTO> findAllByEventPolicyIdAndDomainId(@Param(value = "eventPolicyId") Long eventPolicyId,
                                                         @Param(value = "domainId")Integer domainId, Pageable pageable);

    @Query(value = "select f.id from Filter f where f.id in " +
        " (select c.filterId from Condition c where c.id in :ids) and f.domainId = :domainId")
    List<Long> findFilterIdsByConditionId(@Param(value = "ids")List<Long> ids, @Param(value = "domainId") Integer domainId);
}
