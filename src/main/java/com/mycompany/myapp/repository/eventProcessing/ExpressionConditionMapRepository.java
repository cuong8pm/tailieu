package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Expression;
import com.mycompany.myapp.domain.eventProcessing.ExpressionConditionMap;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpressionConditionMapRepository extends OCSCloneMapRepository<ExpressionConditionMap, Long>, JpaRepository<ExpressionConditionMap,Long> {

    void deleteByDomainIdAndExpressionId(Integer domainId, Long id);

    @Override
    <S extends ExpressionConditionMap> S save(S entity);

    @Query(value = "select e from Expression e where e.id in " +
        " (select ecm.expressionId from ExpressionConditionMap ecm where ecm.conditionId in (:conditionIds) and ecm.domainId = :domainId)" +
        " and e.domainId = :domainId")
    List<Expression> findExpressionByConditionInAndDomainId(@Param(value = "domainId") Integer domainId,
                                                            @Param(value = "conditionIds") List<Long> conditionIds);
}
