package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderTypeItem;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface BuilderTypeItemRepository extends OCSCloneableRepository<BuilderTypeItem,Long>, JpaSpecificationExecutor<BuilderTypeItem> {
    @Query(value = "select bti from BuilderTypeItem bti where bti.builderTypeId = :builderTypeId and bti.domainId = :domainId order by bti.name")
    Page<BuilderTypeItem> findBuilderTypeItemsWithPage(@Param(value = "builderTypeId") Long builderTypeId,
                                                       @Param(value = "domainId") Integer domainId, Pageable pageable);

    @Query(value = "select bti from BuilderTypeItem bti where bti.builderTypeId = :builderTypeId and bti.domainId = :domainId and bti.name like %:name%")
    Page<BuilderTypeItem> findBuilderTypeItemsWithPage(@Param(value = "builderTypeId") Long builderTypeId,
                                                       @Param(value = "domainId") Integer domainId,
                                                       @Param(value = "name")  String name,
                                                       Pageable pageable);
    @Transactional
    @Modifying
    void deleteAllByBuilderTypeIdAndDomainId(Long builderTypeId, Integer domainId);

    @Query(value = "select bti from BuilderTypeItem bti where bti.builderTypeId = :builderTypeId and bti.domainId = :domainId ")
    List<BuilderTypeItem> findAllByBuilderTypeIdAndDomainId(@Param(value = "builderTypeId") Long builderTypeId,
                                                               @Param(value = "domainId") Integer domainId);

    List<BuilderTypeItem> findAllByNameAndBuilderTypeIdAndDomainId(String name, Long builderTypeId, Integer domainId);

    @Query(value = "select bti from BuilderTypeItem bti where bti.builderTypeId = :builderTypeId and bti.id not in " +
        " (select distinct bi.builderTypeItemId from BuilderItem bi where bi.builderId = :builderId and bi.domainId = :domainId ) and bti.domainId = :domainId ")
    List<BuilderTypeItem> findAllItemToAddInstanceValue(@Param(value = "builderTypeId") Long builderTypeId,
                                                        @Param(value = "builderId") Long builderId,
                                                        @Param(value = "domainId") Integer domainId);

}
