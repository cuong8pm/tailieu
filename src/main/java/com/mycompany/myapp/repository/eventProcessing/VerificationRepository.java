package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Verification;
import com.mycompany.myapp.dto.eventProcessing.VerificationDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VerificationRepository extends OCSCloneableRepository<Verification, Long>, JpaSpecificationExecutor<Verification> {

    @Query(value = "SELECT R " +
        "FROM Verification R " +
        "WHERE R.domainId = :domainId " +
        "AND (:parentId is null or R.parentId = :parentId) " +
        "AND (:name is null or R.name LIKE %:name%) " +
        "AND (:description is null or R.description LIKE %:description%)")
    Page<Verification> getVerifications(@Param("parentId") Long categoryId,
                                        @Param("name") String name,
                                        @Param("description") String description,
                                        @Param("domainId") Integer domainId,
                                        Pageable pageable);

    @Query(
        value = "select new com.mycompany.myapp.dto.eventProcessing.VerificationDTO( v.id ,v.name ,rt.name ,rt2.name ) " +
            " from Verification v" +
            " left join ReferTable rt on rt.value = v.verificationType and rt.referType = 58 " +
            " left join ReferTable rt2 on rt2.value = v.listType  and rt2.referType = 59 " +
            " where v.id in (select cvm.verificationId from ConditionVerificationMap cvm where cvm.conditionId = :conditionId) "
    )
    Page<VerificationDTO> getListVerificationByConditionId(@Param(value = "conditionId") Long conditionId,
                                                           Pageable pageable);
}
