package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FunctionParamInstance;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import io.swagger.models.auth.In;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface FunctionParamInstanceRepository
    extends JpaRepository<FunctionParamInstance, Long>, JpaSpecificationExecutor<FunctionParamInstance> {

    void deleteAllByDomainIdAndOwnerIdAndOwnerType(Integer domainId, Long ownerId, Integer ownerType);

    void deleteAllByDomainIdAndOwnerIdInAndOwnerType(Integer domainId, List<Long> ownerIds, Integer ownerType);

    List<FunctionParamInstance> findAllByDomainIdAndFunctionParameterId(Integer domainId, Long functionParameterId);

    FunctionParamInstance findFirstByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(Integer domainId, Long functionParameterId, Long ownerId, Integer ownerType);

    FunctionParamInstance findFirstByDomainIdAndFunctionParameterIdAndOwnerId(Integer domainId, Long functionParameterId, Long ownerId);

    List<FunctionParamInstance> findAllByDomainIdAndOwnerTypeAndOwnerIdIn(Integer domainId, Integer ownerType, List<Long> ownerIds);

    @Query(value = "select count(f.id) from FunctionParamInstance f " +
        "where f.domainId = :domainId and f.functionParameterId = :functionParamId and f.ownerId = :ownerId and f.ownerType = :ownerType")
    Integer countAllByDomainIdAndFunctionParameterIdAndOwnerIdAndOwnerType(@Param(value = "domainId") Integer domainId,
                                                                           @Param(value = "functionParamId") Long functionParameterId,
                                                                           @Param(value = "ownerId") Long ownerId,
                                                                           @Param(value = "ownerType") Integer ownerType);

}
