package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ConditionVerificationMap;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConditionVerificationMapRepository
    extends JpaRepository<ConditionVerificationMap, Long>, OCSCloneMapRepository<ConditionVerificationMap, Long> {
    @Query(value = "SELECT m.verificationId FROM ConditionVerificationMap m WHERE m.domainId = :domainId AND  m.conditionId = :conditionId")
    List<Long> findVerificationIdByConditionId(@Param("domainId") Integer domainId, @Param("conditionId") Long conditionId);

    @Query(value = "SELECT m FROM ConditionVerificationMap m WHERE m.domainId = :domainId and  m.conditionId = :conditionId")
    List<ConditionVerificationMap> findVerificationByConditionId(
        @Param("domainId") Integer domainId,
        @Param("conditionId") Long conditionId
    );

    @Modifying
    @Query(
        "DELETE FROM ConditionVerificationMap m WHERE m.domainId = :domainId and m.verificationId = :verificationId and m.conditionId = :conditionId "
    )
    void deleteByVerificationId(
        @Param("domainId") Integer domainId,
        @Param("verificationId") Long verificationId,
        @Param("conditionId") Long conditionId
    );
    @Modifying
    @Query(
        "DELETE FROM ConditionVerificationMap m WHERE m.domainId = :domainId and m.conditionId = :conditionId "
    )
    void deleteByDomainIdAndConditionId(
        @Param("domainId") Integer domainId,
        @Param("conditionId") Long conditionId
    );

    @Query(
        "SELECT m FROM ConditionVerificationMap m WHERE m.domainId = :domainId and m.verificationId = :verificationId and m.conditionId =:conditionId "
    )
    List<ConditionVerificationMap> findByVerificationIdAndConditionId(
        @Param("domainId") Integer domainId,
        @Param("verificationId") Long verificationId,
        @Param("conditionId") Long conditionId
    );

    Boolean existsByVerificationIdAndDomainId(Long verificationId, Integer domainId);

    Boolean existsByVerificationIdAndConditionIdAndDomainId(Long verificationId, Long conditionId, Integer domainId);

    @Override
    <S extends ConditionVerificationMap> S save(S entity);
}
