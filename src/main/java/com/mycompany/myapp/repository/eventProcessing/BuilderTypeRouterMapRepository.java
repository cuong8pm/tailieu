package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderTypeRouterMap;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BuilderTypeRouterMapRepository
    extends OCSCloneMapRepository<BuilderTypeRouterMap, Long>, JpaRepository<BuilderTypeRouterMap, Long> {
    @Query(value = "select m.routerId from BuilderTypeRouterMap m where m.domainId = :domainId and m.builderTypeId = :builderTypeId")
    List<Long> findRouterIdByBuilderTypeId(@Param("domainId") Integer domainId, @Param("builderTypeId") Long builderTypeId);

    @Query(value = "select m from BuilderTypeRouterMap m where m.domainId = :domainId and m.builderTypeId = :builderTypeId")
    List<BuilderTypeRouterMap> findBuilderTypeRouterMapByBuilderTypeId(
        @Param("domainId") Integer domainId,
        @Param("builderTypeId") Long builderTypeId
    );

    @Query(
        value = "select m from BuilderTypeRouterMap m where m.domainId = :domainId " +
        "and m.builderTypeId = :builderTypeId and m.routerId = :routerId"
    )
    List<BuilderTypeRouterMap> findBuilderTypeRouterMapByBuilderTypeIdAndRouterId(
        @Param("domainId") Integer domainId,
        @Param("builderTypeId") Long builderTypeId,
        @Param("routerId") Long routerId
    );

    @Modifying
    @Query(
        value = "delete from BuilderTypeRouterMap m where m.domainId = :domainId " +
        "and m.builderTypeId = :builderTypeId and m.routerId = :routerId"
    )
    void deleteByRouterId(
        @Param("domainId") Integer domainId,
        @Param("builderTypeId") Long builderTypeId,
        @Param("routerId") Long routerId
    );

    Boolean existsByRouterIdAndDomainId(Long routerId, Integer domainId);

    @Override
    <S extends BuilderTypeRouterMap> S save(S entity);

    void deleteAllByDomainIdAndBuilderTypeId(Integer domainId, Long builderTypeId);
}
