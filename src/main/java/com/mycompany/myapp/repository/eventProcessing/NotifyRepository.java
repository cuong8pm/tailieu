package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Notify;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotifyRepository extends OCSCloneableRepository<Notify, Long>, JpaSpecificationExecutor<Notify> {

    @Query(value = "SELECT N FROM Notify N WHERE N.builderId = :builderId AND N.domainId = :domainId")
    Page<Notify> findNotifyByBuilderId(@Param("builderId") Long builderId, @Param("domainId") Integer domainId, Pageable pageable);

    Notify findByIdAndDomainId(Long id, Integer domainId);

    List<Notify> findAllByBuilderId(Long id);
    List<Notify> findAllByBuilderIdAndLanguageId(Long builderId,Long languageId);
    void deleteAllByDomainIdAndBuilderIdIn(Integer domainId, List<Long> builderIds);
}
