package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.OCSMoveableEntity;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.Condition;
import com.mycompany.myapp.dto.eventProcessing.BuilderConditionDTO;
import com.mycompany.myapp.dto.eventProcessing.BuilderDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BuilderRepository extends JpaSpecificationExecutor<Builder>, OCSCloneableRepository<Builder, Long> {

    @Query(value = " SELECT R "
        + " FROM Builder R "
        + " WHERE R.domainId = :domainId "
        + "   AND (:parentId is null or R.parentId = :parentId) "
        + "   AND (:name is null or R.name LIKE %:name%) "
        + "   AND (:description is null or R.description LIKE %:description%)")
    Page<Builder> getBuilders(@Param("parentId") Long categoryId,
                              @Param("name") String name,
                              @Param("description") String description,
                              @Param("domainId") Integer domainId,
                              Pageable pageable);

    @Query(value = "select b from Builder b " +
        " join BuilderItem bi on b.id = bi.builderId " +
        " where bi.valueType = 2 and bi.linkedId = :functionId and b.name like %:name%")
    List<Builder> getListBuilderUsingFunction(@Param(value = "functionId") Long functionId,
                                              @Param(value = "name") String name);

    Integer countBuildersByBuilderTypeIdAndDomainId(Long builderTypeId, Integer domainId);

    @Query(
        value = "select new com.mycompany.myapp.dto.eventProcessing.BuilderDTO(b.id,b.name,bt.id,bt.name) " +
            " from Builder b " +
            " inner join BuilderType bt on b.builderTypeId = bt.id" +
            " where b.id in (select cbm.builderId from ConditionBuilderMap cbm where cbm.conditionId = :conditionId) "
    )
    Page<BuilderDTO> getListBuilderByConditionId(@Param(value = "conditionId") Long conditionId, Pageable pageable);

    @Query(" SELECT B "
            + " FROM ConditionBuilderMap M "
            + "   INNER JOIN Builder B ON M.builderId = B.id "
            + " WHERE M.conditionId = :id "
            + "   AND M.domainId = :domainId  ")
    List<Builder> findBuilderByCondition(@Param("id") Long id, @Param("domainId") Integer domainId);

    @Query(value = " SELECT new com.mycompany.myapp.dto.eventProcessing.BuilderDTO(b.id, b.name, c.id, c.name, b.posIndex, b.description) " +
        " from Builder b " +
        " join ConditionBuilderMap cbm on b.id = cbm.builderId " +
        " join Condition c on c.id = cbm.conditionId " +
        " join Filter f on f.id = c.filterId " +
        " where f.eventPolicyId = :eventPolicyId and b.domainId = :domainId")
    Page<BuilderDTO> findAllByEventPolicyIdAndDomainId(@Param(value = "eventPolicyId") Long eventPolicyId,
                                                    @Param(value = "domainId")Integer domainId, Pageable pageable);

    List<Builder> findByNameContainingAndDomainIdAndBuilderTypeId(String name, Integer domainId, Long type, Sort sort);

    List<Builder> findByDomainIdAndBuilderTypeId(Integer domainId, Long type, Sort sort);

    @Query(" SELECT new com.mycompany.myapp.dto.eventProcessing.BuilderDTO( "
            + " B.id, B.name, BT.id, BT. name, R.name  ) "
            + " FROM Builder B "
            + "   INNER JOIN BuilderType BT ON B.builderTypeId = BT.id "
            + "   INNER JOIN BuilderTypeRouterMap M ON B.builderTypeId = M.builderTypeId "
            + "   INNER JOIN Router R ON M.routerId = R.id "
            + " WHERE B.id IN :builderIds "
            + "   AND B.domainId = :domainId ")
    List<BuilderDTO> getBuilderRouterByBuilderTypeIds(@Param(value = "builderIds") List<Long> builderIds, @Param(value = "domainId") Integer domainId);
}
