package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Language;
import com.mycompany.myapp.repository.OCSBaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LanguageRepository extends OCSBaseRepository<Language, Long>, JpaSpecificationExecutor<Language> {

}
