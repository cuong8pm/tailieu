package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ConditionBuilderMap;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ConditionBuilderMapRepository
    extends JpaRepository<ConditionBuilderMap, Long>, OCSCloneMapRepository<ConditionBuilderMap, Long> {

    @Query(value = "SELECT m.builderId FROM ConditionBuilderMap m WHERE m.domainId = :domainId and m.conditionId = :conditionId")
    List<Long> findBuilderIdByConditionId(@Param("domainId") Integer domainId, @Param("conditionId") Long conditionId);

    @Query(value = "SELECT m FROM ConditionBuilderMap m WHERE m.domainId = :domainId and m.conditionId = :conditionId")
    List<ConditionBuilderMap> findBuilderByConditionId(@Param("domainId") Integer domainId, @Param("conditionId") Long conditionId);

    @Query("SELECT m FROM ConditionBuilderMap m WHERE m.domainId = :domainId and m.builderId = :builderId and m.conditionId = :conditionId ")
    List<ConditionBuilderMap> findBuilderIdAndConditionId(@Param("domainId") Integer domainId, @Param("builderId") Long builderId, @Param("conditionId") Long conditionId);

    @Modifying
    @Query("DELETE FROM ConditionBuilderMap m WHERE m.domainId = :domainId and m.builderId = :builderId and m.conditionId = :conditionId ")
    void deleteByBuilderId(@Param("domainId") Integer domainId, @Param("builderId") Long builderId, @Param("conditionId") Long conditionId);

    Boolean existsByBuilderIdAndDomainId(Long builderId, Integer domainId);

    @Override
    <S extends ConditionBuilderMap> S save(S entity);

    List<ConditionBuilderMap> findAllByParentIdInAndDomainId(List<Long> parentIds, Integer domainId);

    Integer countByParentIdAndDomainId(Long id, Integer domainId);
    @Query(" select " +
        "  m.conditionId " +
        " FROM  ConditionBuilderMap m " +
        " WHERE m.conditionId IN (:ids ) " +
        "       AND m.domainId = :domainId " +
        " GROUP BY m.conditionId " +
        " ORDER BY m.conditionId ")
    Set<Long> findDistinctAllConditionIdByDomainIdAndParentIdIn(@Param("domainId")Integer domainId,@Param("ids") List<Long> ids );

    void deleteByDomainIdAndBuilderIdIn(Integer domainId, List<Long> ids);

    void deleteByBuilderIdAndDomainId(Long builderId, Integer domainId);

    ConditionBuilderMap findFirstByBuilderIdAndDomainId(Long builderId, Integer domainId);

    Long findFirstConditionIdByBuilderIdAndDomainId(Long builderId, Integer domainId);
}
