package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderItem;
import com.mycompany.myapp.dto.eventProcessing.BuilderItemDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BuilderItemRepository extends OCSCloneableRepository<BuilderItem, Long>, JpaSpecificationExecutor<BuilderItem> {
    @Query("SELECT new com.mycompany.myapp.dto.eventProcessing.BuilderItemDTO( " +
        "BI.id, " +
        "BI.name, " +
        "BI.builderId, " +
        "BI.builderTypeItemId, " +
        "BI.value, " +
        "BI.description, " +
        "BI.valueType, " +
        "BI.dataType, " +
        "BI.linkedId, " +
        "BI.domainId, " +
        "BTI.builderTypeItemName , " +
        "RT.name, " +
        "RT2.name ) " +
        "FROM BuilderTypeItem BTI " +
        "INNER JOIN BuilderItem BI on BI.builderTypeItemId = BTI.id " +
        "INNER JOIN ReferTable RT on BI.valueType = RT.value " +
        "INNER JOIN ReferTable RT2 on BI.dataType = RT2.value " +
        "WHERE BI.builderId = :builderId AND BI.domainId = :domainId " +
        "AND RT.referType = 53 " +
        "AND RT2.referType = 43 ")
    Page<BuilderItemDTO> findBuilderItemByBuilderId(
        @Param("builderId") Long builderId,
        @Param("domainId") Integer domainId,
        Pageable pageable
    );

    BuilderItem findByIdAndDomainId(Long id, Integer domainId);

    BuilderItem findByBuilderIdAndBuilderTypeItemIdAndDomainId(Long builderId,Long builderTypeItemId, Integer domainId);

    Integer countBuilderItemsByBuilderTypeItemIdAndDomainId(Long builderTypeItemId, Integer domainId);

    @Query(
        value = "select count(bi.id) from BuilderItem bi where bi.builderTypeItemId in (" +
        " select bti from BuilderTypeItem  bti where bti.builderTypeId = :builderTypeId) and bi.domainId = :domainId"
    )
    Integer countBuilderItemsByBuilderTypeItemIdInAndDomainId(
        @Param(value = "builderTypeId") Long builderTypeId,
        @Param(value = "domainId") Integer domainId
    );

    List<BuilderItem> findAllByBuilderId(Long id);

    List<BuilderItem> findAllByBuilderTypeItemIdAndDomainId(Long builderTypeItemId, Integer domainId);

    void deleteAllByDomainIdAndBuilderIdIn(Integer domainId, List<Long> builderIds);

    @Query("SELECT new com.mycompany.myapp.dto.eventProcessing.BuilderItemDTO( " +
        "BI.id, " +
        "BI.name, " +
        "BI.builderId, " +
        "BI.builderTypeItemId, " +
        "BI.value, " +
        "BI.description, " +
        "BI.valueType, " +
        "BI.dataType, " +
        "BI.linkedId, " +
        "BI.domainId, " +
        "BTI.name, " +
        "RT.name, " +
        "RT2.name ) " +
        "FROM BuilderItem BI " +
        "INNER JOIN BuilderTypeItem BTI on BI.builderTypeItemId = BTI.id " +
        "INNER JOIN ReferTable RT on BI.valueType = RT.value " +
        "INNER JOIN ReferTable RT2 on BI.dataType = RT2.value " +
        "WHERE BI.builderTypeItemId = :builderTypeItemId AND BI.domainId = :domainId " +
        "AND RT.referType = 53 " +
        "AND RT2.referType = 43")
    List<BuilderItemDTO> findBuilderItemByBuilderTypeItemId(@Param("builderTypeItemId") Long builderTypeItemId,
                                                            @Param("domainId") Integer domainId);
}
