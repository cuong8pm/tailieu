package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.OCSMoveableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BuilderTypeRepository extends OCSCloneableRepository<BuilderType, Long>, JpaSpecificationExecutor<BuilderType> {
    @Query(value = "select f from BuilderType f "
        + "where f.domainId = :domainId "
        + "AND (:categoryId is null or f.categoryId = :categoryId) "
        + "AND (:name is null or f.name LIKE %:name%) "
        + "AND (:description is null or f.description LIKE %:description%) ")
    Page<BuilderType> getListBuilderTypeWithPaging(@Param(value = "domainId") Integer domainId,
                                                @Param(value = "categoryId") Long categoryId,
                                                @Param(value = "name") String name,
                                                @Param(value = "description") String description,
                                                Pageable pageable);

    @Query(value = "select bt from BuilderType bt " +
        " where bt.id in (select b.builderTypeId from Builder b where b.id in :builderIds and b.domainId = :domainId) " +
        " and bt.domainId = :domainId")
    List<BuilderType> getListBuilderTypeByBuilderIds(@Param(value = "builderIds")List<Long> builderIds,
                                                     @Param(value = "domainId") Integer domainId);
}
