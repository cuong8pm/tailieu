package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FunctionSuggestions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FunctionSuggestionsRepository extends JpaRepository<FunctionSuggestions, Long>, JpaSpecificationExecutor<FunctionSuggestions> {

}
