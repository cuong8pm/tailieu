package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventObjectField;
import com.mycompany.myapp.repository.OCSBaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EventObjectFieldRepository extends OCSBaseRepository<EventObjectField, Long>, JpaSpecificationExecutor<EventObjectField> {

    @Query(value = "select eof from EventObjectField eof where eof.eventObjectId = :eventObjectId and eof.parentId is null ")
    List<EventObjectField> findParentEventFieldsByEventObjectId(@Param(value = "eventObjectId") Long eventObjectId);

    @Query(value = "select eof from EventObjectField eof where eof.parentId = :parentId ")
    List<EventObjectField> findChildrenEventFieldsByParentId(@Param(value = "parentId") Long parentId);

    EventObjectField findFirstByDomainIdAndSource(Integer domainId, String source);
}
