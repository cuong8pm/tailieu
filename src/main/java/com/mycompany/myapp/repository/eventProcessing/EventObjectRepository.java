package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventObject;
import com.mycompany.myapp.dto.eventProcessing.DropdownDTO;
import com.mycompany.myapp.repository.OCSBaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EventObjectRepository extends OCSBaseRepository<EventObject, Long>, JpaSpecificationExecutor<EventObject> {

    @Query(value = "select eo.id as id, eo.name as name from EventObject eo where eo.type = :type ")
    List<DropdownDTO> getEventObjectsByType(@Param(value = "type") Integer type);
}
