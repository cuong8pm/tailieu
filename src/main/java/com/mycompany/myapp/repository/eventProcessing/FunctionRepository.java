package com.mycompany.myapp.repository.eventProcessing;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.repository.OCSCloneableRepository;
@Repository
public interface FunctionRepository extends OCSCloneableRepository<Function,Long>, JpaSpecificationExecutor<Function> {

    @Query(value = "select f from Function f "
        + "where f.domainId = :domainId "
        + "AND (:categoryId is null or f.categoryId = :categoryId) "
        + "AND (:name is null or f.name LIKE %:name%) "
        + "AND (:description is null or f.description LIKE %:description%) ")
    Page<Function> getListFunctionsPaging(@Param(value = "domainId") Integer domainId,
                                          @Param(value = "categoryId")Long categoryId,
                                          @Param(value = "name") String name,
                                          @Param(value = "description")String description,
                                          Pageable pageable);

    List<Function> findByNameContainingAndDomainIdAndFunctionType(String name, Integer domainId, Integer type, Sort sort);

    List<Function> findByDomainIdAndFunctionType(Integer domainId, Integer type, Sort sort);

    Function findOneByNameAndDomainId(String name,Integer domainId);
}
