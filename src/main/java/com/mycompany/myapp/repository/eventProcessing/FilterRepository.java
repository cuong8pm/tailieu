package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventPolicy;
import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FilterRepository extends OCSCloneableRepository<Filter, Long>, JpaSpecificationExecutor<Filter> {
    @Query(
        value = "select distinct f from Filter f" +
        " join ExpressionFilterMap efm on f.id = efm.filterId " +
        " join Expression e on efm.expressionId = e.id " +
        " where e.inputType = 1 and e.linkedId = :functionId order by f.id"
    )
    List<Filter> getListFiltersUsingFunction(@Param(value = "functionId") Long functionId);

    @Query(value = "select fip.refer_table_id from filter_ignore_profile fip where fip.filter_id = :filterId ", nativeQuery = true)
    List<Long> getListIgnoreIdReferTable(@Param(value = "filterId") Long filterId);

    @Query(value = "select c.filterId as id, count(c.id) as countChild from Condition c where c.filterId in (:ids) and c.domainId = :domainId group by c.filterId")
    List<CountChildrenByParentDTO> countChildrenByParentId(@Param("ids") List<Long> ids, @Param("domainId") Integer domainId);

    @Query(" SELECT DISTINCT M.filterId "
            + " FROM ExpressionFilterMap M "
            + "   INNER JOIN Expression E on M.expressionId = E.id "
            + " WHERE (:input is null OR E.input like :input) "
            + "    AND E.dataType = :dataType "
            + "    AND E.inputType = :inputType "
            + "    AND (:linkedId is null OR E.linkedId = :linkedId) "
            + "    AND (:value is null OR E.value like %:value%) "
            + "    AND M.domainId = :domainId ")
    List<Long> findIdByExpression(@Param("input") String input, @Param("dataType") Integer dataType,
            @Param("inputType") Integer inputType, @Param("linkedId") Long linkedId,
            @Param("value") String value, @Param("domainId") Integer domainId);

    @Query(value = "select f from Filter f "
        + "where f.domainId = :domainId "
        + "AND (:categoryId is null or f.categoryId = :categoryId) "
        + "AND (:name is null or f.name LIKE %:name%) "
        + "AND (:description is null or f.description LIKE %:description%) ")
    Page<Filter> getListFilterPaging(@Param(value = "domainId") Integer domainId,
                                     @Param(value = "name") String name,
                                     @Param(value = "description") String description,
                                     @Param(value = "categoryId") Long categoryId,
                                     Pageable pageable);

    List<Filter> findAllByDomainIdAndEventPolicyId(Integer domainId, Long eventPolicyId);

    Page<Filter> findAllByDomainIdAndEventPolicyId(Integer domainId, Long eventPolicyId,Pageable pageable);

    List<Filter> findAllByDomainIdAndEventPolicyIdIn(Integer domainId, List<Long> eventPolicyId);

    Integer countAllByEventPolicyIdAndDomainId(Long eventPolicyId, Integer domainId);

    @Query("select e.id from EventPolicy e where e.id in " +
        "(select f.eventPolicyId from Filter f where f.id in :ids) and e.domainId = :domainId")
    List<Long> findAllEventPolicyIdByFilterIdAnd(@Param(value = "ids")List<Long> ids, @Param(value = "domainId") Integer domainId);

}
