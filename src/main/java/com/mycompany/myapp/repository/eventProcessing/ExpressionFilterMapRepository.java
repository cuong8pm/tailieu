package com.mycompany.myapp.repository.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Expression;
import com.mycompany.myapp.domain.eventProcessing.ExpressionFilterMap;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpressionFilterMapRepository extends OCSCloneMapRepository<ExpressionFilterMap, Long>, JpaRepository<ExpressionFilterMap, Long> {

    @Override
    <S extends ExpressionFilterMap> S save(S entity);

    @Query(value = "select e from Expression e where e.id in " +
        " (select efm.expressionId from ExpressionFilterMap efm where efm.parentId in (:parentIds) and efm.domainId = :domainId)" +
        " and e.domainId = :domainId")
    List<Expression> findExpressionByDomainIdAndParentIds(@Param(value = "domainId") Integer domainId,
                                                          @Param(value = "parentIds") List<Long> parentIds);
}
