package com.mycompany.myapp.repository.actiontype;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.domain.actiontype.EventActionTypeMap;
import com.mycompany.myapp.dto.actiontype.EventOfActionTypeDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;

public interface EventActionTypeMapReponsitory extends JpaRepository<EventActionTypeMap, Long>, OCSCloneMapRepository<EventActionTypeMap, Long> {
    Integer countByActionTypeIdAndDomainId(Long actionTypeId, Integer domainId);

    @Query(value = " SELECT "
            + " M.POS_INDEX as \"posIndex\", "
            + " E.EVENT_ID as \"id\", "
            + " E.EVENT_NAME as \"name\", "
            + " E.DESCRIPTION as \"description\" "
            + " FROM event_action_type_map M "
            + " INNER JOIN event E on M.EVENT_ID = E.EVENT_ID "
            + " WHERE M.ACTION_TYPE_ID = :actionTypeId "
            + " ORDER BY M.POS_INDEX ", nativeQuery = true)
    Page<EventOfActionTypeDTO> getListEvent(@Param("actionTypeId") Long actionTypeId, Pageable pageable);

    List<EventActionTypeMap> findByDomainIdAndParentIdOrderByPosIndex(Integer domainId, Long id);

    @Query(value = " SELECT MAX(M.posIndex) "
            + " FROM EventActionTypeMap M "
            + " WHERE M.domainId = :domainId "
            + "   AND M.eventId = :eventId ")
    Integer getMaxPosIndex(@Param("eventId") Long eventId, @Param("domainId") Integer domainId);
    
    List<EventActionTypeMap> findByEventIdAndDomainId(Long id , Integer domainId);

    @Query(value = " SELECT A "
            + " FROM ActionType A "
            + "   INNER JOIN EventActionTypeMap M ON A.id = M.actionTypeId "
            + " WHERE M.domainId = :domainId "
            + "   AND M.eventId = :eventId "
            + " ORDER BY M.posIndex ASC ")
    List<ActionType> getActionTypes(@Param("eventId") Long eventId, @Param("domainId") Integer domainId);
    
    @Override
    <S extends EventActionTypeMap> S save(S entity);
}
