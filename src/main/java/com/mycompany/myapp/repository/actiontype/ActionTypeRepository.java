package com.mycompany.myapp.repository.actiontype;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface ActionTypeRepository extends JpaRepository<ActionType, Long>, JpaSpecificationExecutor<ActionType>, OCSCloneableRepository<ActionType, Long> {

    List<ActionType> findByDomainId(Integer domainId);

    @Query("SELECT A.name FROM ActionType A WHERE A.id = :id AND A.domainId = :domainId")
    String getActionTypeOfFormula(@Param("id") Long id, @Param("domainId") Integer domainId);

    void deleteByIdAndDomainId(Long id , Integer domainId);

    @Query(value = " SELECT A "
            + " FROM ActionType A "
            + "WHERE A.domainId = :domainId "
            + "AND (A.parentId = :parentId ) "
            + "AND (:name IS NULL OR A.name LIKE %:name% ) "
            + "AND (:description IS NULL OR A.description LIKE %:description% ) ")
    Page<ActionType> getActionTypes(@Param(value = "domainId") Integer domainId,
            @Param(value = "parentId") Long parentId,
            @Param(value = "name") String name,
            @Param(value = "description") String Description,
            Pageable pageable);

    ActionType findByIdAndDomainId(Long id , Integer domainId);

    @Query(value = "SELECT COUNT(action_type) "
            + "FROM Action A "
            + "WHERE A.action_type = :id "
            + "AND A.domain_id = :domainId ",nativeQuery = true)
    Integer countActionType(@Param(value = "id") Long id , @Param(value = "domainId") Integer domainId);
    List<ActionType> findAllByDomainIdAndIdIn(Integer domainId, List<Long> listId );
}
