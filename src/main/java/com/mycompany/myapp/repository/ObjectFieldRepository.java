package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ObjectField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


public interface ObjectFieldRepository extends JpaRepository<ObjectField, Long>, JpaSpecificationExecutor<ObjectField> {
    List<ObjectField> findByObjectFieldParent(Long objectFildPatrent);

    ObjectField findByName(String name);
}
