package com.mycompany.myapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.PccRule;

public interface PccRuleRepository extends OCSMoveableRepository<PccRule, Long>{
    @Query("SELECT MAX(posIndex) FROM PccRule WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);


    @Query("SELECT P FROM PccRule P "
        + "WHERE P.domainId = :domainId "
        + "AND (:categoryId is null or P.categoryId = :categoryId) "
        + "AND (:name is null or P.name LIKE %:name%) "
        + "AND (:id is null or P.tempId LIKE %:id%) "
        + "AND (:priority is null or P.tempPriority LIKE %:priority%) "
        + "AND (:description is null or P.remark LIKE %:description%) ")
    Page<PccRule> findPccRules(@Param("categoryId") Long categoryId,
                                       @Param("domainId") Integer domainId,
                                       @Param("name") String name,
                                       @Param("description") String description,
                                       @Param("id") String id,
                                       @Param("priority") String priority,
                                       Pageable pageable);

}
