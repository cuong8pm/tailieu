package com.mycompany.myapp.repository;
import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.Qos;

public interface QosRepository extends JpaRepository<Qos, Long>, JpaSpecificationExecutor<Qos>{
    Qos findByIdAndDomainId(Long id, Integer domainId);
    
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteByIdAndDomainId(Long id, Integer domainId);
    
    @Query("SELECT Q FROM Qos Q "
            + "WHERE Q.domainId = :domainId "
            + "AND (:name is null or Q.name LIKE %:name%) "
            + "AND (:qci is null or Q.qci LIKE %:qci%) "
            + "AND (:mbrul is null or Q.tempMbrul LIKE %:mbrul%) "
            + "AND (:mbrdl is null or Q.tempMbrdl LIKE %:mbrdl%) "
            + "AND (:gbrul is null or Q.tempGbrul LIKE %:gbrul%) "
            + "AND (:gbrdl is null or Q.tempGbrdl LIKE %:gbrdl%) "
            + "AND (:description is null or Q.qosRemark LIKE %:description%) ")
    Page<Qos> findQos(@Param("domainId") Integer domainId,
                      @Param("name") String name,
                      @Param("qci") String qci,
                      @Param("mbrul") String mbrul,
                      @Param("mbrdl") String mbrdl,
                      @Param("gbrul") String gbrul,
                      @Param("gbrdl") String gbrdl,
                      @Param("description") String description,
                      Pageable pageable
                      );    
    
    List<Qos> findByNameAndDomainId(String name, Integer domainId);

    List<Qos> findByDomainId(Integer domainId);
}
