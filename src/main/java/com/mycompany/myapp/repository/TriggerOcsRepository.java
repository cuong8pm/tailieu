package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TriggerOcs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TriggerOcsRepository extends JpaRepository<TriggerOcs, Long>, JpaSpecificationExecutor<TriggerOcs> {

}