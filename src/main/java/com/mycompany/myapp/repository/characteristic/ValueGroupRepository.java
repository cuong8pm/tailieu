package com.mycompany.myapp.repository.characteristic;

import com.mycompany.myapp.domain.characteristic.Relationship;
import com.mycompany.myapp.domain.characteristic.Value;
import com.mycompany.myapp.domain.characteristic.ValueGroup;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the CharSpecValueGroup entity.
 */
@Repository
public interface ValueGroupRepository extends JpaRepository<ValueGroup, Long> {
    List<ValueGroup> findByCharSpecRelationship(Relationship item);

    /**
     * @author LamHV
     */
    @Query(value = "SELECT * FROM char_spec_value_group WHERE char_spec_relationship_id = ?1", nativeQuery = true)
    List<ValueGroup> findByCharSpecRelationshipId(Long charSpecRelationshipId);

    @Query(value = "SELECT id FROM char_spec_value_group WHERE char_spec_relationship_id = ?", nativeQuery = true)
    List<Long> listcharValueGroupByRelationship(Long charSpecRelationshipId);

    @Query(value = "SELECT id FROM char_spec_value_group WHERE parent_char_value_id = ?", nativeQuery = true)
    List<Long> listcharValueGroupByValue(Long parentCharValueId);

    @Modifying
    @Query(value = "DELETE " + " FROM char_spec_value_group "
            + " WHERE char_spec_relationship_id = :charSpecRelationShipId ", nativeQuery = true)
    public void deleteByCharSpecRelationShipId(@Param("charSpecRelationShipId") Long charSpecRelationShipId);

    @Modifying
    @Query(value = "DELETE " + " FROM char_spec_value_group " + " WHERE parent_char_value_id = :id "
            + "  OR child_char_value_id = :id ", nativeQuery = true)
    public void deleteByCharSpecValueId(@Param("id") Long charSpecValueId);

    @Query(value = "SELECT id FROM char_spec_value_group WHERE parent_char_value_id = ?1 and char_spec_relationship_id = ?2",
            nativeQuery = true)
    List<Long> getIdByParentValueIdAndRelationshipId(long parentId, long relationshipId);

    @Modifying
    @Query(value = "UPDATE char_spec_value_group SET is_default = ?1 WHERE char_spec_relationship_id = ?2 ",
            nativeQuery = true)
    int setDefaultByCharSpecRelationshipId(boolean isDefault, long relationshipId);

    @Query(value = "Select count(id) from char_spec_value_group where parent_char_value_id = :charSpecValueId or child_char_value_id = :charSpecValueId",
            nativeQuery = true)
    public int countCharSpecValueGroups(@Param("charSpecValueId") long charSpecValueId);

    @Modifying
    @Query(value = "delete from ValueGroup gr where gr.parentCharValue in :charSpecValues")
    void deleteByParentCharValueIn(@Param(value = "charSpecValues") List<Value> charSpecValues);
}
