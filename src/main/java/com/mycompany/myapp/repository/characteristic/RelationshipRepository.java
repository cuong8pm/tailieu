package com.mycompany.myapp.repository.characteristic;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.characteristic.Relationship;
import com.mycompany.myapp.domain.characteristic.RelationshipPojo;

/**
 * Spring Data repository for the CharSpecRelationship entity.
 */
@Repository
public interface RelationshipRepository extends JpaRepository<Relationship, Long> {

    @Query(value = "SELECT * FROM char_spec_relationship WHERE parent_char_spec_id = :charSpecParentId AND child_char_spec_id = :charSpecChildId LIMIT 1",
            nativeQuery = true)
    Optional<Relationship> findByParentIdAndChildId(@Param("charSpecParentId") Long charSpecParentId,
            @Param("charSpecChildId") Long charSpecChildId);

    @Query(value = "SELECT * FROM char_spec_relationship WHERE parent_char_spec_id = ?1 AND child_char_spec_id IN ?2",
            nativeQuery = true)
    List<Relationship> findByParentIdAndListChildId(Long charSpecParentId, List<Long> charSpecChildIds);

    List<Relationship> findByChildCharSpec(Characteristic childCharSpec);

    List<Relationship> findByParentCharSpec(Characteristic parentCharSpec);

    List<Relationship> findByParentCharSpecId(Long parentCharSpecId);

    @Query(value = "SELECT id FROM char_spec_relationship", nativeQuery = true)
    List<Long> listCharRelationshipID();

    @Query(value = "SELECT R.childCharSpec.id "
            + " FROM Relationship R "
            + " WHERE R.parentCharSpec.id = :id ")
    List<Long> getChildIdByParentId(@Param("id") Long id);

    @Query(value = "SELECT t1.id AS id, t1.parent_char_spec_id AS parentId, t1.child_char_spec_id AS childId, "
            + "            t1.relationship_type_id AS relationshipTypeId, t2.name AS relationshipTypeName"
            + "    FROM char_spec_relationship AS t1 "
            + "           LEFT JOIN relationship_type AS t2 on t1.relationship_type_id = t2.id "
            + "    WHERE t1.parent_char_spec_id = :parentId order by id", nativeQuery = true)
    List<RelationshipPojo> listRelationship(@Param("parentId") Long parentId);

    @Query(value = "select csr.parent_char_spec_id from char_spec_relationship csr " +
        "where csr.child_char_spec_id = :childrenId",nativeQuery = true)
    List<Long> findParentCharSpecIdByChildrenId(@Param(value="childrenId") Long childrenId);

    @Query("SELECT R.childCharSpecId FROM Relationship R where R.parentCharSpecId = :parentCharSpec")
    List<Long> findChildCharspecId(@Param("parentCharSpec") Long parentCharSpec);

    @Query("SELECT R.parentCharSpecId FROM Relationship R where R.childCharSpecId = :childCharSpec ")
    List<Long> findParentCharSpecId(@Param("childCharSpec") Long childCharSpec);
}
