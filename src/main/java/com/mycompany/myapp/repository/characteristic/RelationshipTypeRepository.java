package com.mycompany.myapp.repository.characteristic;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.characteristic.RelationshipType;

/**
 * Spring Data repository for the RelationshipType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RelationshipTypeRepository extends JpaRepository<RelationshipType, Long> {
    @Query(value = "SELECT MAX(id) FROM relationship_type", nativeQuery = true)
    Long getMaxId();
}
