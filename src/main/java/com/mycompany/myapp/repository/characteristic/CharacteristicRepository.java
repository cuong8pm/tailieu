package com.mycompany.myapp.repository.characteristic;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.constant.CharacteristicType;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.OCSMoveableRepository;

/**
 * Spring Data repository for the CharSpec entity.
 */
@Repository
public interface CharacteristicRepository extends OCSMoveableRepository<Characteristic, Long>,
        JpaSpecificationExecutor<Characteristic>, OCSCloneableRepository<Characteristic, Long> {

    @Query("SELECT B FROM Characteristic B " + "WHERE B.parentId = :parentId " + "AND B.domainId = :domainId "
            + "AND (:name IS NULL OR B.name LIKE %:name%) "
            + "AND (:description IS NULL OR B.description LIKE %:description%) ")
    Page<Characteristic> findCharacteristics(@Param("parentId") Long parentId, @Param("domainId") Integer domainId,
            @Param("name") String name, @Param("description") String description, Pageable pageable);

    @Query("SELECT MAX(posIndex) FROM Characteristic WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

    @Query(value = " SELECT * " + " FROM char_spec " + " WHERE name like %:name% " + "   AND category_id = :parentId  ",
            nativeQuery = true)
    public List<Characteristic> searchCharSpec(@Param("parentId") Long parentId, @Param("name") final String name);

    @Query(value = " SELECT * " + " FROM char_spec " + " WHERE id != :id " + "   AND category_id = :categoryId "
            + "   AND char_spec_type_id = :type" + "   AND id NOT IN ( " + "     SELECT parent_char_spec_id "
            + "     FROM char_spec_relationship " + "     WHERE child_char_spec_id = :id )" + "   AND id NOT IN ("
            + "       SELECT child_char_spec_id" + "       FROM char_spec_relationship"
            + "       WHERE parent_char_spec_id = :id) " + "   AND id NOT IN :removeIds ", nativeQuery = true)
    public List<Characteristic> findNoParentCharSpec(@Param("id") final Long id,
            @Param("categoryId") final Long categoryId, @Param("type") final Long typeId,
            @Param("removeIds") List<Long> removeIds);

    /**
     * @author LamHV
     * @param parentId
     * @return
     */
    @Query(value = "SELECT * FROM char_spec WHERE id IN \n"
            + "(SELECT csr.child_char_spec_id FROM char_spec_relationship csr WHERE csr.parent_char_spec_id = ?1)",
            nativeQuery = true)
    List<Characteristic> findByParentId(Long parentId);

    @Query(value = "SELECT * FROM char_spec WHERE id IN \n"
            + "(SELECT csr.child_char_spec_id FROM char_spec_relationship csr WHERE csr.parent_char_spec_id = :id) and name like %:textSearch%",
            nativeQuery = true)
    List<Characteristic> findByParentIdAndTextSearch(@Param("id") Long parentId,
            @Param("textSearch") String textSearch);

    @Query(value = " SELECT * " + " FROM char_spec " + " WHERE name like %:name%", nativeQuery = true)
    public List<Characteristic> searchCharSpec(@Param("name") final String name);

    @Query(value = "SELECT * FROM char_spec WHERE category_id = ?", nativeQuery = true)
    public List<Characteristic> getCharSpec(Long id);

    @Query(value = "SELECT distinct category_id FROM char_spec WHERE category_id is not null", nativeQuery = true)
    public List<Long> getCategoryId();

    @Query(value = "select count(*) from char_spec where category_id = ?1", nativeQuery = true)
    public int getChildCategory(Long id);

    /**
     * @author LamHV
     * @param parentId
     * @return
     */
    @Query(value = "SELECT * FROM char_spec WHERE id IN "
            + "(SELECT csr.child_char_spec_id FROM char_spec_relationship csr WHERE csr.parent_char_spec_id = :id) and name like %:textSearch%",
            countQuery = "SELECT COUNT(*) FROM char_spec WHERE id IN "
                    + "(SELECT csr.child_char_spec_id FROM char_spec_relationship csr WHERE csr.parent_char_spec_id = :id) and name like %:textSearch%",
            nativeQuery = true)
    Page<Characteristic> findByParentId(@Param("id") Long parentId, Pageable pageable,
            @Param("textSearch") String textSearch);

    /**
     * @author LamHV
     * @param childId
     * @return
     */
    @Query(value = "SELECT * FROM char_spec WHERE id IN \n"
            + "		(SELECT csr.parent_char_spec_id FROM char_spec_relationship csr WHERE csr.child_char_spec_id = ?1)",
            nativeQuery = true)
    Optional<Characteristic> findByChildId(Long childId);

    @Query(value = " SELECT * " + " FROM char_spec " + " WHERE id != :id " + "   AND name like %:name% "
            + "   AND category_id = :categoryId " + "   AND char_spec_type_id = :type" + "   AND id NOT IN ( "
            + "     SELECT parent_char_spec_id " + "     FROM char_spec_relationship "
            + "     WHERE child_char_spec_id = :id ) " + "   AND id NOT IN :removeIds ", nativeQuery = true)
    public List<Characteristic> findNoParentCharSpecByName(@Param("name") final String name, @Param("id") final Long id,
            @Param("categoryId") final Long categoryId, @Param("type") final Long typeId,
            @Param("removeIds") List<Long> removeIds);

    /**
     * @author LamHV
     * @param name
     * @return
     */
    @Query(value = "SELECT * FROM char_spec WHERE NAME  = ?1 ", nativeQuery = true)
    public List<Characteristic> findByName(String name);

    @Query(value = "select * from char_spec where name like %:charSpecName%", nativeQuery = true)
    List<Characteristic> findByCharSpecName(@Param("charSpecName") String charSpecName);

    @Query(value = "select * from char_spec where id in (:charSpecIds)", nativeQuery = true)
    List<Characteristic> findByListId(@Param("charSpecIds") List<Long> charSpecIds);

    @Query(value = "select id from char_spec where id in (select char_spec_id from service_desc_char_use where service_desc_id = ?)",
            nativeQuery = true)
    public List<Long> findParentCharSpecUsedByService(Long serviceDescId);

    @Query(value = "select id from char_spec where id in (select child_char_spec_id from char_spec_relationship where parent_char_spec_id in (:charSpecIds))",
            nativeQuery = true)
    public List<Long> findChildInParentCharSpec(@Param("charSpecIds") List<Long> charSpecIds);

    @Query(value = "select * from char_spec where id in (select char_spec_id from service_desc_char_use where service_desc_id = :id) and name like %:name%",
            nativeQuery = true)
    public List<Characteristic> findCharUsedByServiceAndTextSearch(@Param("id") Long categoryId,
            @Param("name") String textSearch);

    @Query(value = "select * from char_spec where id in (select char_spec_id from service_desc_char_use where service_desc_id = ?)",
            nativeQuery = true)
    public List<Characteristic> findCharUsedByService(Long serviceDescId);

    @Query(value = "SELECT * FROM char_spec WHERE id IN (SELECT char_spec_id FROM resource_desc_char_use WHERE resource_desc_id = :id) AND name like %:name% ",
            countQuery = "SELECT COUNT(*) FROM char_spec WHERE id IN (SELECT char_spec_id FROM resource_desc_char_use WHERE resource_desc_id = :id) AND name like %:name% ",
            nativeQuery = true)
    List<Characteristic> searchByResourceDescId(@Param("id") Long categoryId, @Param("name") String textSearch);

    @Query(value = " SELECT * " + " FROM char_spec " + " WHERE name like %:name% " + "   AND category_id = :parentId "
            + "   AND char_spec_type_id = :typeId " + "   AND id NOT IN :removeIds ", nativeQuery = true)
    public List<Characteristic> searchNoParentCharSpec(@Param("parentId") Long parentId,
            @Param("name") final String name, @Param("typeId") Long typeId, @Param("removeIds") List<Long> removeIds);

    @Query(value = "SELECT * FROM char_spec " + " WHERE category_id = :id " + "   AND char_spec_type_id = :typeId "
            + "   AND id NOT IN :removeIds ", nativeQuery = true)
    public List<Characteristic> getNoParentCharSpec(@Param("id") Long id, @Param("typeId") Long typeId,
            @Param("removeIds") List<Long> removeIds);

    @Query(value = "select max(id) from char_spec_relationship", nativeQuery = true)
    public Long getMaxCharSpecRelationshipId();

    @Query(value = " SELECT * FROM char_spec WHERE name like %:name% "
            + "AND category_id = :parentId AND char_spec_type_id = :typeId " + "AND id NOT IN "
            + "(SELECT char_spec_id FROM resource_desc_char_use WHERE resource_desc_id = :resourceDescId)",
            nativeQuery = true)
    public List<Characteristic> findCharSpecNotInResourceDescCharUse(@Param("parentId") Long parentId,
            @Param("name") final String name, @Param("typeId") Long typeId,
            @Param("resourceDescId") Long resourceDescId);

    @Query(value = "select * from char_spec where id in (select parent_char_spec_id from char_spec_relationship where child_char_spec_id = ?)",
            nativeQuery = true)
    public List<Characteristic> findParentByChild(Long childCharSpecId);

    @Query(value = " SELECT * " + " FROM char_spec " + " WHERE name like %:name% " + "   AND category_id = :parentId "
            + "   AND char_spec_type_id = (select id from char_spec_type where name = :type)" + "   AND id NOT IN ( "
            + "     SELECT char_spec_id " + "     FROM service_desc_char_use WHERE service_desc_id = :serviceId) ",
            nativeQuery = true)
    public List<Characteristic> findCharSpecNotInServiceDescCharUse(@Param("parentId") Long categoryId,
            @Param("name") String searchName, @Param("type") String type, @Param("serviceId") Long serviceId);

    @Query(value = "select * from char_spec where id in (SELECT char_spec_id FROM ocs_slice_desc_char_use WHERE ocs_slice_desc_id = ?)",
            nativeQuery = true)
    List<Characteristic> getRootCharOfVersion(Long ocsSliceDescId);

    @Query(value = "select * from char_spec where id in (select DISTINCT child_char_spec_id from char_spec_relationship where parent_char_spec_id = ?)",
            nativeQuery = true)
    List<Characteristic> getChildCharOfVersion(Long ocsSliceDescId);

    @Query(value = "select * from char_spec where id in (SELECT rfs_char_spec_id FROM cfs_rfs_char_mapping where cfs_rfs_mapping_id = ?)",
            nativeQuery = true)
    List<Characteristic> getRootCharOfRfs(Long cfsRfsMappingId);

    @Query(value = "select * from char_spec where id in (select DISTINCT child_char_spec_id from char_spec_relationship where parent_char_spec_id = ?)",
            nativeQuery = true)
    List<Characteristic> getChildCharOfRfs(Long parentCharId);

    @Query(value = " SELECT id FROM char_spec  WHERE  category_id = ?1", nativeQuery = true)
    List<Long> findIdByCategoryId(Long categoryId);

    @Query(value = "SELECT COUNT(id) FROM char_spec WHERE id != ?1 AND name = ?2", nativeQuery = true)
    int existsByIdAndName(Long id, String name);

    boolean existsByName(String name);

    /**
     * @author LongTV
     * @param categoryId
     */
    @Query(value = "SELECT IFNULL(MAX(t1.order_index),0) + 1 FROM char_spec t1 WHERE t1.category_id = ?",
            nativeQuery = true)
    public Integer getMaxOrderIndex(Long categoryId);

    @Query(value = "select * from char_spec where category_id = :categoryId and char_spec_type_id = 1 and name like %:name% ",
            nativeQuery = true)
    public List<Characteristic> findCharSpecByCategory(@Param("categoryId") Long categoryId,
            @Param("name") String name);

    /**
     * @author LongTV
     * @param categoryId
     *            move down
     */
    @Query(value = "SELECT t1.* FROM char_spec t1 INNER JOIN (SELECT" + " MIN(t2.order_index) AS max_order_index"
            + " FROM char_spec t2 WHERE t2.category_id = ?1 AND t2.order_index > ?2"
            + " ) AS A ON A.max_order_index = t1.order_index AND t1.category_id = ?1", nativeQuery = true)
    public Characteristic getOrderIndexMoveDown(Long categoryId, Long orderIndex);

    /**
     * @author LongTV
     * @param categoryId
     *            move up
     */
    @Query(value = "SELECT t1.* FROM char_spec t1 INNER JOIN (SELECT" + " MAX(t2.order_index) AS max_order_index"
            + " FROM char_spec t2 WHERE t2.category_id = ?1 AND t2.order_index < ?2"
            + " ) AS A ON A.max_order_index = t1.order_index AND t1.category_id = ?1", nativeQuery = true)
    public Characteristic getOrderIndexMoveUp(Long categoryId, Long orderIndex);

    @Query(value = "SELECT char_spec.* " + " FROM ocs_slice_desc_char_use "
            + "   inner join char_spec on ocs_slice_desc_char_use.char_spec_id = char_spec.id "
            + " where ocs_slice_desc_char_use.ocs_slice_desc_id = :id " + "   and char_spec.name like %:name% ;",
            nativeQuery = true)
    public List<Characteristic> getCharSpecOfTemplateVersion(@Param("id") Long id, @Param("name") String textSearch);

    /**
     * Hàm này sử dụng để cập nhật lại toàn bộ Characteristic Value Type mỗi khi
     * Characteristic thay đổi Type
     *
     * @param status
     * @param id
     * @return
     */
    @Modifying
    @Query(value = "UPDATE char_spec_value SET value_type_id = ?1 WHERE char_spec_id =?2", nativeQuery = true)
    int updateAllValueTypeIdByCharacteristicId(Long typeId, Long characteristicId);

    Long countByParentIdAndDomainId(Long categoryId, Integer domainId);

    @Query(value = "SELECT C.id "
            + " FROM Characteristic C "
            + " WHERE C.domainId = :domainId "
            + " AND C.valueRefeTable != :typeId ")
    List<Long> getCharspecIdByDomainIdAndTypeNot(@Param("domainId") Integer domainId, @Param("typeId") Integer typeId);

    @Query(value = "SELECT C.name FROM Characteristic C WHERE C.domainId = :domainId AND C.id = :id")
    String getCharName(@Param("domainId") Integer domainId, @Param("id") Long id);

    @Query(value = "SELECT c.name FROM Characteristic c " +
        "WHERE c.id = :charSpecId " +
        "AND c.domainId = :domainId ")
    String findNameByIdAndDomain(@Param("charSpecId") Long charSpecId, @Param("domainId") Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM normalizer_params as np " +
        "WHERE CONFIG_INPUT LIKE CONCAT('value:', :charSpecId, ';isUsingCharacteristic:true;%') " +
        "OR CONFIG_INPUT LIKE CONCAT('start:', :charSpecId, ';startIsCharacteristic:true;%') " +
        "OR CONFIG_INPUT LIKE CONCAT('%end:', :charSpecId, ';endIsCharacteristic:true;%') " +
        "OR CONFIG_INPUT LIKE CONCAT('characteristicID:', :charSpecId, ';%')", nativeQuery = true)
    Long countNormUseChar(@Param("charSpecId") Long charSpecId);

    @Query(value = "SELECT COUNT(*) FROM formula as f " +
        "WHERE (A = :charSpecId AND MOD(TEMPLATE_BITS, 8) >= 4) " +
        "OR (B = :charSpecId AND MOD(TEMPLATE_BITS, 4) >= 2) " +
        "OR (PER = :charSpecId AND MOD(TEMPLATE_BITS, 2) = 1) " +
        "OR (FORMULA_TYPE = :charSpecId AND MOD(TEMPLATE_BITS, 16) >= 8)", nativeQuery = true)
    Long countFormulaUseChar(@Param("charSpecId") Long charSpecId);

    @Query(value = "SELECT * FROM char_spec WHERE name like %:name% AND id IN (:charSpecIds)", nativeQuery = true)
    List<Characteristic> searchByNameAndListId(@Param("name") final String name,
            @Param("charSpecIds") final List<Long> charSpecIds);

    @Query("SELECT C.id FROM Characteristic C WHERE C.valueRefeTable != :type")
    List<Long> findByValue(@Param("type") Integer type);

    @Transactional
    @Modifying
    @Query("UPDATE Characteristic c SET c.defaultValue = :defaultValue WHERE c.id = :id")
    void updateDefaultValue(@Param("defaultValue") String defaultValue, @Param("id") Long id);

    @Transactional
    @Modifying
    @Query("UPDATE Characteristic c SET c.defaultValue = NULL WHERE c.id = :id")
    void deleteDefaultValue(@Param("id") Long id);

    @Query("SELECT valueRefeTable FROM Characteristic where id = :id")
    Integer findCharspectypeId(@Param("id") Long id);

    @Transactional
    @Modifying
    void deleteAllByIdIn(List<Long> ids);
}
