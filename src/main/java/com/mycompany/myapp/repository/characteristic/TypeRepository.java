package com.mycompany.myapp.repository.characteristic;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.characteristic.Type;

/**
 * Spring Data repository for the CharSpecType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {
    Type findByName(String name);
}
