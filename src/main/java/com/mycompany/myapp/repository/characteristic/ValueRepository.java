package com.mycompany.myapp.repository.characteristic;

import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.characteristic.Value;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the CharSpecValue entity.
 */
@Repository
public interface ValueRepository extends JpaRepository<Value, Long> {
    List<Value> findByCharacteristic(Characteristic charSpec);

    /**
     * @author LamHV
     * @param charSpecId
     * @return
     */
    @Query(value = "SELECT * FROM char_spec_value WHERE char_spec_id = :id and name like %:textSearch%",
            countQuery = "SELECT COUNT(*) FROM char_spec_value WHERE char_spec_id = :id and name like %:textSearch%",
            nativeQuery = true)
    Page<Value> findByCharacteristicId(@Param("id") Long charSpecId, Pageable pageable,
            @Param("textSearch") String textSearch);

    @Query(value = "SELECT * FROM char_spec_value WHERE id IN (SELECT char_spec_value_id FROM "
            + "rsc_desc_char_value_use WHERE resource_desc_char_use_id = ? AND is_default = true)", nativeQuery = true)
    Optional<Value> findByResourceDescCharUseId(Long resourceDescCharUseId);

    @Query(value = "SELECT * FROM char_spec_value WHERE char_spec_id = ?1", nativeQuery = true)
    List<Value> findByCharacteristicId(Long charSpecId);

    @Query(value = "SELECT id FROM char_spec_value WHERE char_spec_id = ?", nativeQuery = true)
    List<Long> listcharValueId(Long charSpecId);

    @Query(value = "select value from char_spec_value where char_spec_id =? and is_default = 1 limit 1",
            nativeQuery = true)
    public Optional<String> findCharSpecValueIsSettedDefault(Long charSpecId);

    @Modifying
    @Query(value = "UPDATE char_spec_value SET is_default = ?1 WHERE id = ?2", nativeQuery = true)
    void updateIsDefault(int isDefault, Long id);

    @Query(value = "SELECT id FROM char_spec_value WHERE is_default = 1 and char_spec_id = ?", nativeQuery = true)
    List<Long> getCharValueDefaultId(Long charSpecId);

    @Query(value = "SELECT id FROM char_spec_value_group WHERE is_default = 1 and char_spec_relationship_id = ?",
            nativeQuery = true)
    List<Long> getCharValueGroupDefaultId(Long relationshipId);

    /**
     * @author LamHV
     * @param name
     * @return
     */
    @Query(value = "SELECT * FROM char_spec_value WHERE NAME  = ?1 ", nativeQuery = true)
    public List<Value> findByName(String name);

    @Query(value = "select max(id) from char_spec_relationship", nativeQuery = true)
    Long getCharValueIdMax();

    @Query(value = "SELECT value FROM char_spec_value where is_default = 1 and char_spec_id = ?", nativeQuery = true)
    String getDefaultValueOfChar(Long charSpecId);

    @Query(value = "SELECT * from char_spec_value where char_spec_id = ?", nativeQuery = true)
    List<Value> getListValueOfChar(Long charSpecId);

    @Query(value = "SELECT * FROM char_spec_value WHERE id " + "IN "
            + "(SELECT char_spec_value_id FROM osd_service_char_value_use WHERE ocs_slice_desc_id = ?1 AND service_desc_id = ?2 AND char_spec_id = ?3);",
            nativeQuery = true)
    List<Value> getListUsedChar(Long templateId, Long serviceId, Long charSpecId);

    @Query(value = "SELECT * FROM char_spec_value where char_spec_id = ?", nativeQuery = true)
    List<Value> getValueOfChar(Long charSpecId);

    @Query(value = "SELECT * FROM char_spec_value where char_spec_id in (SELECT rfs_char_value_id FROM cfs_rfs_char_value_mapping where cfs_rfs_char_mapping_id = ?)",
            nativeQuery = true)
    List<Value> getCharValueOfRfs(Long cfsRfsCharMappingId);

    boolean existsByIsDefaultAndCharacteristicId(boolean isDefault, long charSpecId);

    @Query(value = "SELECT id FROM char_spec_value WHERE  is_default =?1 and char_spec_id = ?2", nativeQuery = true)
    List<Long> findIdByIsDefaultAndCharacteristicId(boolean isDefault, long charSpecId);

    @Query(value = "SELECT COUNT(id) FROM char_spec_value WHERE name = ?1 AND char_spec_id = ?2", nativeQuery = true)
    int countByNameAndCharacteristicId(String name, long charSpecId);

    @Query(value = "SELECT COUNT(id) FROM char_spec_value WHERE id != ?1 AND NAME = ?2 AND char_spec_id = ?3",
            nativeQuery = true)
    int countByNameAndIdAndCharacteristicId(long id, String name, long charSpecId);

    @Modifying
    @Query(value = "UPDATE char_spec_value SET is_default = ?1 WHERE char_spec_id = ?2", nativeQuery = true)
    int setDefaultInCharacteristicId(boolean isDefault, long charSpecId);

    @Query(value = "SELECT value FROM char_spec_value where id = ?", nativeQuery = true)
    String getDefaultValue(Long charSpecId);

    /**
     * Hàm này sử dụng để cập nhật lại toàn bộ Characteristic Value Type mỗi khi
     * Characteristic thay đổi Type
     *
     * @param status
     * @param id
     * @return
     */
    @Modifying
    @Query(value = "UPDATE char_spec_value SET value_type_id = ?1 WHERE char_spec_id =?2", nativeQuery = true)
    int updateAllValueTypeIdByCharacteristicId(Long typeId, Long characteristicId);

    @Query(value = "SELECT * FROM char_spec_value WHERE id IN "
            + "(SELECT char_spec_value_id FROM srv_desc_char_value_use WHERE service_desc_id = ?1 AND service_desc_char_use_id = ?2)",
            nativeQuery = true)
    List<Value> getListCharValueOfCfs(Long serviceDescId, Long serviceDescCharUseId);

    @Query(value = "SELECT * FROM char_spec_value where char_spec_id = :id", nativeQuery = true)
    List<Value> getCharSpecValueOfCharSpec(@Param("id") Long id);

    @Query(value = "select parent_char_value_id from char_spec_value_group where char_spec_relationship_id = :relationshipId ", nativeQuery = true)
    List<Long> getCharValueIdByRelationshipId(@Param("relationshipId") Long relationshipId);

    @Query(value = "SELECT * FROM char_spec_value WHERE char_spec_id = ?1", nativeQuery = true)
    List<Value> findByCharSpecId(Long charSpecId);
}
