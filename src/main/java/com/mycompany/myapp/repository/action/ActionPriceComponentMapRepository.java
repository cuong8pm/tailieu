package com.mycompany.myapp.repository.action;


import com.mycompany.myapp.domain.action.ActionPriceComponentMap;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionPriceComponentMapRepository extends JpaRepository<ActionPriceComponentMap, Long>, OCSCloneMapRepository<ActionPriceComponentMap, Long> {
    ActionPriceComponentMap findByIdAndDomainId(Long id, Integer domainId);

    @Override
    <S extends ActionPriceComponentMap> S save(S entity);

    Integer countByPriceComponentIdAndDomainId(Long id, Integer domainId);
}
