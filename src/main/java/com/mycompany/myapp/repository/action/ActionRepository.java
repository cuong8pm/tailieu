package com.mycompany.myapp.repository.action;

import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.repository.OCSCloneableRepository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ActionRepository extends OCSCloneableRepository<Action, Long> {
    @Query("SELECT ac from Action ac " +
        " where ac.domainId = :domainId " +
        " AND (:parentId is null or ac.categoryId = :parentId) " +
        " AND (:name is null or ac.name LIKE %:name%) " +
        " AND (:description is null or ac.description LIKE %:description%) ")
    Page<Action> getActions(@Param("parentId") Long parentId, @Param("domainId") Integer domainId, @Param("name") String name, @Param("description") String description, Pageable pageable);

    Integer countByActionTypeIdAndDomainId(Long actionTypeId, Integer domainId);

    @Query(" select count(ac.id) from Action ac where ac.dynamicReserveFilterId = :ratingFilterId or ac.priorityFilterId = :ratingFilterId or ac.sortingFilterId = :ratingFilterId and ac.domainId = :domainId ")
    Integer checkUsedRatingFilter(@Param("ratingFilterId") Long ratingFilterId, @Param("domainId") Integer domainId);

    Integer countByReserveInfoIdAndDomainId(Long reserveInfoId, Integer domainId);

    List<Action> findByDomainIdAndIdIn(Integer domainId, List<Long> ids);

    @Query("SELECT ac FROM Action ac WHERE ac.priorityFilterId IN (:ids) OR ac.dynamicReserveFilterId IN (:ids) OR ac.sortingFilterId IN (:ids)")
    List<Action> findActionByRatingFilterIds(@Param("ids") List<Long> ids);

    @Query("SELECT ac FROM Action ac LEFT JOIN OfferTemplateVersionAction otva "
            + "ON ac.id = otva.actionId WHERE otva.offerTemplateVersionId = :offerTemplateVersionId "
            + "AND ac.domainId = :domainId ORDER BY otva.posIndex asc")
    Page<Action> findAllActionByOfferTemplateVersionId(@Param("offerTemplateVersionId") Long offerId,
                                                       @Param("domainId") Integer domainId, Pageable pageable);

    @Query("SELECT ac FROM Action ac LEFT JOIN OfferVersionAction ova "
            + "ON ac.id = ova.actionId WHERE ova.offerVersionId = :offerVersionId "
            + "AND ac.domainId = :domainId ORDER BY ova.posIndex asc")
    Page<Action> findAllActionByOfferVersionId(@Param("offerVersionId") Long offerId,
                                                       @Param("domainId") Integer  domainId, Pageable pageable);
}
