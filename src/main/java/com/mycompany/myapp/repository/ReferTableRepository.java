package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.ReferType;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReferTableRepository extends JpaRepository<ReferTable, Long>, JpaSpecificationExecutor<ReferTable> {
    List<ReferTable> getReferTablesByReferType(ReferType referType, Sort sort);

    ReferTable getReferTableByReferTypeAndValue(ReferType referType, Integer value);

    List<ReferTable> findByReferTypeOrderByValueAsc(ReferType referType);

    @Query("select rf.value from ReferTable rf where rf.id = :id")
    Integer findValueById(@Param("id") Long id);

    @Query("SELECT rf.name FROM ReferTable rf where rf.value = :value AND rf.referType = :referType")
    String getSortNameOfFormula(@Param("value") int value, @Param("referType") ReferType referType);

    ReferTable findByReferTypeAndValue(ReferType referType, Integer value);

    List<ReferTable> getReferTablesByReferTypeIn(List<ReferType> referTypes, Sort sort);

    List<ReferTable> findByReferTypeInAndNameContaining(List<ReferType> referTypes, String name, Sort sort);
}
