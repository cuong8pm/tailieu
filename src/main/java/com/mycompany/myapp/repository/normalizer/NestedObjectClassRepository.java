package com.mycompany.myapp.repository.normalizer;

import com.mycompany.myapp.domain.NestedObjectClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface NestedObjectClassRepository extends JpaRepository<NestedObjectClass, Long>, JpaSpecificationExecutor<NestedObjectClass> {

}
