package com.mycompany.myapp.repository.normalizer;

import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.repository.OCSBaseRepository;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.OCSMoveableRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NormParamRepository extends OCSCloneableRepository<NormalizerParam, Long>, OCSBaseRepository<NormalizerParam, Long>, JpaSpecificationExecutor<NormalizerParam> {

    @Query("SELECT NP FROM NormalizerParam NP "
        + "WHERE NP.domainId = :domainId "
        + "AND NP.normalizerId = :normalizerId ")
    List<NormalizerParam> findAllByNormalizerId(@Param("normalizerId") Long normalizerId, @Param("domainId") Integer domainId);

    void deleteByDomainIdAndNormalizerIdAndIdIn(Integer domain, Long normalizerId, List<Long> normalizerIds);

    void deleteByDomainIdAndId(Integer domain, Long normalizerId);

    void deleteByDomainIdAndNormalizerId(Integer domain, Long normalizerId);
}
