package com.mycompany.myapp.repository.normalizer;

import com.mycompany.myapp.domain.normalizer.PreFunction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PreFunctionRepository extends JpaRepository<PreFunction, Integer>, JpaSpecificationExecutor<PreFunction> {
    @Query(value = "select fn from PreFunction fn where fn.functionName like %:name%")
    List<PreFunction> findByFunctionName (@Param(value = "name") String name);
}
