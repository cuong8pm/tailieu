package com.mycompany.myapp.repository.normalizer;

import com.mycompany.myapp.domain.NestedObject;
import com.mycompany.myapp.repository.OCSMoveableRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NestedObjectRepository extends OCSMoveableRepository<NestedObject, Long>, JpaSpecificationExecutor<NestedObject> {
    List<NestedObject> findByName(String name);
    List<NestedObject> findByParentId(Long parentId);

    @Query("select no from NestedObject no where no.parentClassId = :parentId  " +
        "and no.alist = FALSE ")
    List<NestedObject> findByParentIdAndNotHaveChild(@Param("parentId") Long parentId);
    List<NestedObject> findByNameContaining(String name);
    List<NestedObject> findByParentClassId(Long parentClassId);
    List<NestedObject> findByObjClassId(Long objectClassId);
    Optional<NestedObject> findByNameAndParentClassId(String name, Long parentId);
    Optional<NestedObject> findByNameAndParentClassIdAndAlistIsFalse(String name, Long parentId);
}
