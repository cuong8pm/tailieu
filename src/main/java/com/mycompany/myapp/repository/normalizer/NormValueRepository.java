package com.mycompany.myapp.repository.normalizer;

import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NormValueRepository extends OCSCloneableRepository<NormalizerValue, Long>, JpaSpecificationExecutor<NormalizerValue> {

    @Query("SELECT NV FROM NormalizerValue NV "
        + "WHERE NV.domainId = :domainId "
        + "AND NV.normalizerId = :normalizerId ")
    List<NormalizerValue> findAllByNormalizerId(@Param("normalizerId") Long normalizerId, @Param("domainId") Integer domainId);

    void deleteByDomainIdAndIdIn(Integer domainId, List<Long> ids);
    List<NormalizerValue> findByDomainIdAndIdIn(Integer domainId, List<Long> ids);

    void deleteByDomainIdAndNormalizerId(Integer domainId, Long normalizerId);

    @Query("SELECT max (v.valueId) from NormalizerValue v " +
        "where v.domainId =:domainId " +
        "and v.normalizerId =:normalizerId")
    Long getMaxValueIdInNormalizer(@Param("domainId") Integer domainId, @Param("normalizerId") Long normalizerId);

    NormalizerValue findByValueIdAndNormalizerIdAndDomainId(Long valueId, Long normalizerId, Integer domainId);

    List<NormalizerValue> findByValueIdInAndNormalizerIdInAndDomainId(List<Long> valueIds, List<Long> normalizerIds, Integer domainId);
}
