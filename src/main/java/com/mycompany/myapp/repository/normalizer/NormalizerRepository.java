package com.mycompany.myapp.repository.normalizer;

import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.OCSMoveableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NormalizerRepository extends OCSCloneableRepository<Normalizer, Long>, JpaSpecificationExecutor<Normalizer> {

    @Query("SELECT N FROM Normalizer N "
        + "WHERE N.domainId = :domainId "
        + "AND (:parentId is null or N.categoryId = :parentId) "
        + "AND (:name is null or N.name LIKE %:name%) "
        + "AND (:description is null or N.description LIKE %:description%) ")
    Page<Normalizer> findNormalizers(@Param("parentId") Long parentId,
                                     @Param("domainId") Integer domainId,
                                     @Param("name") String name,
                                     @Param("description") String description,
                                     Pageable pageable);

    @Query(value = "SELECT count(rate_table_column_id) FROM rate_table_columns WHERE rate_table_columns.normalizer_id = :normalizerId", nativeQuery = true)
    Long countNormalizerUsed(@Param("normalizerId") Long normalizerId);

    @Query("select n.defaultValue from Normalizer n " +
        "where n.domainId = :domainId " +
        "and n.id = :normalizerId")
    Long findDefaultValueByIdAndDomainId(@Param("normalizerId") Long normalizerId, @Param("domainId") Integer domainId);
}
