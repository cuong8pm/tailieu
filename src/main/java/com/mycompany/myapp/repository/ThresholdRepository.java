
package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Threshold;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.List;


public interface ThresholdRepository extends OCSCloneableRepository<Threshold, Long>, JpaSpecificationExecutor<Threshold> {

    @Query(value = "select th from Threshold th " +
        "where th.id in " +
        "( select mp.thresholdId from ThresholdBaltypeMap mp where mp.baltypeId = :balTypeId and mp.domainId = :domainId) " +
        "AND (:name is null or th.name LIKE %:name%) " +
        "AND th.domainId = :domainId")
    Page<Threshold> findByBalanceIdAndDomainId(@Param("balTypeId") Long balTypeId,
                                               @Param("name") String name,
                                               @Param("domainId") Integer domainId,
                                               Pageable pageable);

    @Query(value = "select th from Threshold th " +
        "where th.id in " +
        "( select mp.thresholdId from ThresholdBaltypeMap mp where mp.baltypeId = :balTypeId and mp.domainId = :domainId) " +
        "and th.domainId = :domainId")
    List<Threshold> findByBalanceIdAndDomainId(@Param("balTypeId") Long balTypeId, @Param("domainId") Integer domainId);

    @Query(value = "select th from Threshold th " +
        "where th.id = :id and th.domainId = :domainId")
    Threshold findByIdAndDomainId(@Param("id") Long id, @Param("domainId") Integer domainId);

    @Query(value = "select th from Threshold th " +
        "where th.id in ( select mp.thresholdId from ThresholdBaltypeMap mp " +
        "   where mp.baltypeId in :balTypeId " +
        "   and mp.domainId = :domainId) ")
    List<Threshold> findThresholdByListBalanceIdAndDomainId(@Param("balTypeId") List<Long> balTypeId, @Param("domainId") Integer domainId);

    @Modifying
    @Query("delete from Threshold t where t.domainId = :domainId and t.id = :thresholdId")
    void deleteByIdAndId(@Param("domainId") Integer domainId, @Param("thresholdId") Long thresholdId);
}
