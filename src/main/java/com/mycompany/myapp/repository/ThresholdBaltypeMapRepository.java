package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ThresholdBaltypeMap;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ThresholdBaltypeMapRepository
		extends JpaRepository<ThresholdBaltypeMap, Long>, OCSCloneMapRepository<ThresholdBaltypeMap, Long> {
    @Query(value = "select m.thresholdId from ThresholdBaltypeMap m where m.domainId = :domainId and  m.baltypeId = :balTypeId")
    List<Long> findThresholdIdByBalTypeId(@Param("domainId") Integer domainId, @Param("balTypeId") Long balTypeId);

    @Query(value = "select m from ThresholdBaltypeMap m where m.domainId = :domainId and  m.baltypeId = :balTypeId")
    List<ThresholdBaltypeMap> findThresholdByBalTypeId(@Param("domainId") Integer domainId, @Param("balTypeId") Long balTypeId);

    @Modifying
    @Query("delete from ThresholdBaltypeMap m where m.domainId = :domainId and m.thresholdId = :thresholdId and m.baltypeId = :balanceId ")
    void deleteByThresholdId(@Param("domainId") Integer domainId, @Param("thresholdId") Long thresholdId, @Param("balanceId") Long balanceId);

    @Query("select m from ThresholdBaltypeMap m where m.domainId = :domainId and m.thresholdId = :thresholdId and m.baltypeId =:balanceId ")
    List<ThresholdBaltypeMap> findByThresholdAndDomainAndBalance(@Param("domainId") Integer domainId, @Param("thresholdId") Long thresholdId, @Param("balanceId") Long balanceId);

    Boolean existsByThresholdIdAndDomainId(Long thresholdId, Integer domainId);

    @Override
    <S extends ThresholdBaltypeMap> S save(S entity);
    
}
