package com.mycompany.myapp.repository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.GeoNetZone;

public interface GeoNetZoneRepository extends JpaRepository<GeoNetZone, Long>, JpaSpecificationExecutor<GeoNetZone> {

    @Query("SELECT C FROM GeoNetZone C "
        + "WHERE C.domainId = :domainId "
        + "AND (:geoHomeZoneId is null or C.geoHomeZoneId = :geoHomeZoneId) "
        + "AND (:cellId is null or C.tempCellId LIKE %:cellId%) "
        + "AND (:updateDate is null or function('date_format', C.tempUpdateDate,'%d/%m/%Y %H:%i:%s') like %:updateDate%) ")

    Page<GeoNetZone> findGeoNetZones(
        @Param(value = "geoHomeZoneId") Long geoHomeZoneId,
        @Param(value = "domainId") Integer domainId,
        @Param(value = "cellId") String cellId,
        @Param(value = "updateDate") String updateDate,
        Pageable pageable);

    GeoNetZone findByIdAndDomainId(Long id, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    GeoNetZone findByCellIdAndAndGeoHomeZoneIdAndDomainId(Long cellId, Long geoHomeZoneId, Integer domainId);

    List<GeoNetZone> findByDomainId(Integer domainId);
    
    List<GeoNetZone> findByDomainIdAndGeoHomeZoneId(Integer domainId, Long geoHomeZoneId);

    List<GeoNetZone> findByGeoHomeZoneIdAndCellIdIn(Long geoHomeZoneId, Collection<Long> cellIds);

    void deleteByIdInAndDomainId(List<Long> ids, Integer domainId);
}
