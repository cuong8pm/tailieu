package com.mycompany.myapp.repository;

import java.util.List;

import javax.persistence.LockModeType;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.dto.Tree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.repository.custom.CategoryCustomRepository;

@Repository
public interface CategoryRepository extends OCSMoveableRepository<Category, Long>, CategoryCustomRepository {

    @Query("SELECT MAX(posIndex) FROM Category WHERE categoryParentId = :parentId AND domainId = :domainId")
    Integer getMaxPosIndex(@Param("parentId") Long parentId, @Param("domainId") Integer domainId);

    Category findByCategoryParentIdAndPosIndexAndDomainId(Long parentId, Integer posIndex, Integer domainId);

    List<Category> findByCategoryParentIdAndDomainId(Long categoryParentId, Integer domainId);

    List<Category> findByDomainIdAndCategoryTypeOrderByPosIndexAsc(Integer domainId, CategoryType categoryType);

    @Query("SELECT C FROM Category C WHERE C.categoryType IN (:categoryTypes) AND C.domainId = :domainId AND C.categoryParentId is null ORDER BY C.posIndex ASC")
    List<Category> findRootCategory(@Param("categoryTypes") List<CategoryType> categoryType, @Param("domainId") Integer domainId);

    @Query("SELECT C FROM Category C "
            + "WHERE C.domainId = :domainId "
            + "AND (:categoryParentId is null or C.categoryParentId = :categoryParentId) "
            + "AND (:name is null or C.name LIKE %:name%) "
            + "AND (:remark is null or C.remark LIKE %:remark%) ")
    Page<Category> findCategory(@Param("categoryParentId") Long categoryParentId,
            @Param("domainId") Integer domainId, @Param("name") String name,
            @Param("remark") String description, Pageable pageable);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT C FROM Category C WHERE C.categoryType = :categoryType AND C.domainId = :domainId AND C.categoryParentId is null")
    Category findRootCategoryForUpdate(@Param("categoryType") CategoryType categoryType, @Param("domainId") Integer domainId);

    List<Category> findByNameContainingAndDomainIdAndCategoryType(String name, Integer domainId, CategoryType categoryType, Sort sort);

    @Query("SELECT C from Category C where C.categoryType = :categoryType and C.domainId = :domainId and C.name = :name")
    List<Category> findAndDomainIdAndCategoryTypeAndName(@Param("domainId") Integer domainId, @Param("categoryType") CategoryType categoryType, @Param("name") String name);

}
