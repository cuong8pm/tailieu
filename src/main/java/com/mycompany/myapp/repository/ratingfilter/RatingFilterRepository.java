package com.mycompany.myapp.repository.ratingfilter;

import com.mycompany.myapp.domain.RatingFilter;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RatingFilterRepository extends OCSCloneableRepository<RatingFilter, Long> {
    RatingFilter findByName(String name);

    List<RatingFilter> findByCategoryId(Integer id);

    @Query("SELECT rt from RatingFilter rt "
        + "where rt.domainId = :domainId "
        + "AND (:parentId is null or rt.categoryId = :parentId) "
        + "AND (:name is null or rt.name LIKE %:name%) "
        + "AND (:description is null or rt.description LIKE %:description%) ")
    Page<RatingFilter> findRatingFilterWithPage(@Param("parentId") Long parentId, @Param("domainId") Integer domainId, @Param("name") String name, @Param("description") String description, Pageable pageable);

    List<RatingFilter> findByNameContainingAndDomainIdAndRatingFilterType(@Param("name") String name, @Param("domainId") Integer domainId, @Param("ratingFilterType") Integer ratingFilterType, Sort sort);
    
    @Query("SELECT rt FROM RatingFilter rt WHERE rt.id IN (SELECT b.blockFilterId FROM Block b WHERE b.id in (:ids))")
    List<RatingFilter> findRatingFiltersByBlockIds(@Param("ids") List<Long> ids);
    
    @Query("SELECT rt FROM RatingFilter rt WHERE rt.id IN (:ids)")
    List<RatingFilter> findRatingFiltersByActionIds(@Param("ids") List<Long> ids);
}
