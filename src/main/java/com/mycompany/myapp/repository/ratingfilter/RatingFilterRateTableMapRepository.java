package com.mycompany.myapp.repository.ratingfilter;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.RatingFilterRateTableMap;
import com.mycompany.myapp.dto.ratingfilter.RateTableProjection;
import com.mycompany.myapp.repository.OCSCloneMapRepository;

public interface RatingFilterRateTableMapRepository extends JpaRepository<RatingFilterRateTableMap, Long>, OCSCloneMapRepository<RatingFilterRateTableMap, Long> {

    Integer countByRateTableIdAndDomainId(Long rateTableId, Integer domainId);

    Integer countByRatingFilterIdAndDomainId(Long id, Integer domainId);

    void deleteByRatingFilterIdAndDomainId(Long id, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    List<RatingFilterRateTableMap> findByRatingFilterIdAndDomainId(Long id, Integer domainId);


    @Query("SELECT RT.id as id, " +
        "          RT.name as name, " +
        "          RT.description as description," +
        "          RM.posIndex as posIndex, " +
        "          RM.id as ratingFilterRateTableMapId "+
        "FROM RatingFilterRateTableMap RM " +
        "INNER JOIN " +
        "RateTable RT " +
        "ON RM.rateTableId = RT.id " +
        "AND RM.domainId = RT.domainId " +
        "WHERE RM.ratingFilterId = :ratingFilterId " +
        "AND RM.domainId = :domainId " +
        "ORDER BY RM.posIndex ASC")
    List<RateTableProjection> getRateTable(@Param("ratingFilterId") Long ratingFilterId, @Param("domainId") Integer domainId);

    @Query("SELECT RM.rateTableId " +
            "FROM RatingFilterRateTableMap RM " +
            "WHERE RM.ratingFilterId = :ratingFilterId " +
            "AND RM.domainId = :domainId ")
        List<Long> getRateTableIds(@Param("ratingFilterId") Long ratingFilterId, @Param("domainId") Integer domainId);

    void deleteByRateTableIdAndRatingFilterIdAndDomainId(Long rateTableId,Long ratingFilterId, Integer domainId);

    RatingFilterRateTableMap findByRateTableIdAndRatingFilterIdAndDomainId(Long rateTableId,Long ratingFilterId, Integer domainId);

    @Override
    <S extends RatingFilterRateTableMap> S save(S entity);

    RatingFilterRateTableMap findByIdAndDomainId(Long id, Integer domainId);
}
