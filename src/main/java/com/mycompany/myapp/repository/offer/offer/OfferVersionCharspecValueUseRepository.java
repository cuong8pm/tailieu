package com.mycompany.myapp.repository.offer.offer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface OfferVersionCharspecValueUseRepository extends OCSCloneableRepository<OfferVersionCharspecValueUse, Long>, JpaSpecificationExecutor<OfferVersionCharspecValueUse> {

    void deleteByCharspecUseIdAndDomainId(Long id, Integer domainId);

    List<OfferVersionCharspecValueUse> deleteByCharspecUseId(Long charspecUseId);
}
