package com.mycompany.myapp.repository.offer.offertemplate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionCharspecUse;
import com.mycompany.myapp.dto.offer.common.CharOfVersionDTO;
import com.mycompany.myapp.repository.offer.common.VersionCharspecUseCommonRepository;

public interface OfferTemplateVersionCharspecUseRepository extends VersionCharspecUseCommonRepository<OfferTemplateVersionCharspecUse>, JpaRepository<OfferTemplateVersionCharspecUse, Long> {

    <S extends OfferTemplateVersionCharspecUse> S save(S entity);

    @Query(" SELECT E.id as id, C.id as charspecId, C.name as name, C.defaultValue as value, C.description as description,C.extensible as extensible"
            + " FROM OfferTemplateVersionCharspecUse E "
            + "  INNER JOIN Characteristic C ON E.charspecId = C.id "
            + " WHERE E.offerTemplateVersionId = :id "
            + "   AND E.domainId = :domainId")
    Page<CharOfVersionDTO> getListChildCharacteristic(@Param("id") Long id, @Param("domainId") Integer domainId, Pageable pageable);

    List<OfferTemplateVersionCharspecUse> deleteByOfferTemplateVersionId(Long offerTemplateVersionId);

    List<OfferTemplateVersionCharspecUse> findByDomainIdAndOfferTemplateVersionId(Integer domainId, Long offerTemplateVersionId);
    
    Integer countByCharspecIdAndDomainId(Long charspectId , Integer domainId);

}
