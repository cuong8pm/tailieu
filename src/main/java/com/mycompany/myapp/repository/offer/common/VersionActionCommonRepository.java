package com.mycompany.myapp.repository.offer.common;

import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface VersionActionCommonRepository<E extends VersionActionCommon> extends JpaRepository<E, Long> {

    <S extends VersionActionCommon> S save(S entity);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    E findTopByParentIdAndDomainIdOrderByPosIndexDesc(Long perentId, Integer domainId);

    E findByChildIdAndDomainId(Long childId, Integer domainId);

    E findByIdAndDomainId(Long id, Integer domainId);

    @Query(" update #{#entityName} e set e.posIndex = e.posIndex + 1 " +
        " where e.parentId = :parentId " +
        " and e.posIndex > :posIndex " +
        " and e.domainId = :domainId " +
        " and e.id > 0 ")
    @Modifying
    void updatePosIndex(@Param("parentId") Long parentId, @Param("posIndex") Integer posIndex, @Param("domainId") Integer domainId);

    E findByIdAndDomainIdAndParentId(Long id, Integer domainId, Long parentId);

    @Query(" update #{#entityName} e set e.posIndex = e.posIndex - 1 " +
        " where e.parentId = :parentId " +
        " and e.posIndex > :posIndex " +
        " and e.domainId = :domainId " +
        " and e.id > 0 ")
    @Modifying
    void updatePosIndexDelete(@Param("parentId") Long parentId, @Param("posIndex") Integer posIndex, @Param("domainId") Integer domainId);
}
