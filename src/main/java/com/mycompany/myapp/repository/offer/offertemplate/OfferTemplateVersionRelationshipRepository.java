package com.mycompany.myapp.repository.offer.offertemplate;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.offer.common.OfferCustomDTO;
import com.mycompany.myapp.repository.offer.common.OfferRelationshipCommonRepository;

public interface OfferTemplateVersionRelationshipRepository extends OfferRelationshipCommonRepository<OfferTemplateVersionRelationship>{

    @Override
    <S extends OfferTemplateVersionRelationship> S save(S entity);

    @Query(value = " SELECT DISTINCT new com.mycompany.myapp.dto.offer.common.OfferCustomDTO( O.id, O.name ) "
            + " FROM Offer O "
            + "  INNER JOIN OfferTemplateVersionRelationship E ON O.id = E.childId  "
            + "  INNER JOIN ReferTable R on E.relationshipType = R.value "
            + " WHERE E.parentId = :id "
            + "  AND E.domainId = :domainId "
            + "  AND (:type is null OR E.relationshipType = :type) "
            + "  AND (:name is null OR O.name like %:name%) "
            + "  AND E.isParent is :isParent "
            + "  AND R.referType = 34 ",
    countQuery = " SELECT COUNT(DISTINCT O.id) "
            + " FROM Offer O "
            + "  INNER JOIN OfferTemplateVersionRelationship E ON O.id = E.childId  "
            + "  INNER JOIN ReferTable R on E.relationshipType = R.value "
            + " WHERE E.parentId = :id "
            + "  AND E.domainId = :domainId "
            + "  AND (:type is null OR E.relationshipType = :type) "
            + "  AND (:name is null OR O.name like %:name%) "
            + "  AND E.isParent is :isParent "
            + "  AND R.referType = 34 ")
    Page<OfferCustomDTO> getListOffer(@Param("id") Long id, @Param("type") Integer type, @Param("isParent") Boolean isParent, @Param("name") String name, @Param("domainId") Integer domainId, Pageable pageable);

    List<OfferTemplateVersionRelationship> findByParentIdAndDomainId(Long parentId, Integer domainId);
}
