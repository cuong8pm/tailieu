package com.mycompany.myapp.repository.offer.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.common.VersionRedirectionCommon;
import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import com.mycompany.myapp.repository.OCSCloneableRepository;

@NoRepositoryBean
public interface VersionRedirectionCommonRepository<E extends VersionRedirectionCommon> extends OCSCloneableRepository<E, Long>, JpaSpecificationExecutor<E> {

    E findByIdAndDomainId(Long id, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    <S extends VersionRedirectionCommon> S save(S entity);

    @Query(" SELECT E.id as id, E.url as url, R.name as redirectionTypeName , R.value as redirectionType "
            + " FROM ReferTable R "
            + "  INNER JOIN #{#entityName} E ON R.value = E.redirectionType "
            + " WHERE R.referType = :referType "
            + "  AND E.parentId = :versionId "
            + "  AND E.domainId = :domainId ")
    Page<OfferOfVersionRedirectionDTO> findOfferVersionRedirections(@Param("referType") ReferType referType, @Param("versionId") Long versionId, @Param("domainId") Integer domainId, Pageable pageable );
}
