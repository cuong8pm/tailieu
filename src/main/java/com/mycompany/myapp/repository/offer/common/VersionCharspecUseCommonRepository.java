package com.mycompany.myapp.repository.offer.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.common.VersionCharspecUseCommon;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.OCSCloneableRepository;

@NoRepositoryBean
public interface VersionCharspecUseCommonRepository<E extends VersionCharspecUseCommon> extends OCSCloneableRepository<E, Long>, JpaSpecificationExecutor<E> {

    <S extends VersionCharspecUseCommon> S save(S entity);
    
    @Query(" SELECT E.charspecId "
            + " FROM #{#entityName} E "
            + " WHERE E.parentId = :parentId "
            + " AND E.domainId = :domainId ")
    List<Long> getListCharspecId(@Param("parentId") Long parentId, @Param("domainId") Integer domainId);

    void deleteByParentIdAndDomainId(Long parentId, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);
}
