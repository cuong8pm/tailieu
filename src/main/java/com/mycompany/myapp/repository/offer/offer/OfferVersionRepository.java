package com.mycompany.myapp.repository.offer.offer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.dto.offer.offer.OfferCharacteristicDTO;
import com.mycompany.myapp.dto.offer.offer.OfferOfCharacteristicDTO;
import com.mycompany.myapp.mapper.offer.template.DropDownOfferTemplateVersionDTO;
import com.mycompany.myapp.mapper.offer.template.TreeDependenciesOfferVersionDTO;
import com.mycompany.myapp.repository.offer.common.OfferVersionCommonRepository;


public interface OfferVersionRepository extends OfferVersionCommonRepository<OfferVersion>{

    @Override
    <S extends OfferVersionCommon> S save(S entity);

    @Query(value = "SELECT C.ID as \"id\", "
            + " C.Name as \"name\" , "
            + " C.description as \"description\", "
            + " OVCVU.VALUE as \"value\" , "
            + " C.extensible as \"extensible\" "
            + " FROM char_spec C "
            + " JOIN offer_version_charspec_use OVCU "
            + " ON C.id = OVCU.CHARSPEC_ID "
            + " JOIN offer_version_charspec_value_use OVCVU "
            + " ON OVCU.ID = OVCVU.CHARSPEC_USE_ID "
            + " WHERE C.ID in :characteristicIds"
            + " AND C.DOMAIN_ID = :domainId " , nativeQuery =  true)
    Page<OfferOfCharacteristicDTO> findChacracteristics(@Param ("characteristicIds") List<Long> characteristicIds, @Param ("domainId") Integer domainId , Pageable pageable);

    List<OfferVersion> findByOfferTemplateVersionIdAndDomainId(Long offerTemplateVersionId, Integer domainId);

    @Query(value = "SELECT C.id as \"id\", "
            + " C.name as \"name\", "
            + " C.description as \"description\", "
            + " C.extensible as \"extensible\", "
            + " OVCVU.VALUE as \"value\", "
            + " OVCVU.ID as \"idVersionCharValueUse\" , "
            + " OVCVU.CHARSPEC_USE_ID as \"idVersionCharcUse\" "
            + " FROM char_spec C JOIN offer_version_charspec_use OVCU "
            + " ON  C.id = OVCU.CHARSPEC_ID "
            + " JOIN offer_version_charspec_value_use OVCVU ON OVCU.OFFER_VERSION_ID = OVCVU.CHARSPEC_USE_ID "
            + " where OVCVU.id = :id "
            + " AND C.DOMAIN_ID = :domainId ", nativeQuery = true)
    OfferCharacteristicDTO findCharacteristicById(@Param ("id") Long id, @Param ("domainId") Integer domainId );

    @Query(value = "select OFFER_VERSION_ID as \"id\", NUMBER as \"number\", OFFER_ID as\"parentId\" from offer_version where OFFER_TEMPLATE_VERSION_ID = ?", nativeQuery = true)
    List<TreeDependenciesOfferVersionDTO> getOfferVersionByOfferTemplateVersion(Long id);

    List<OfferVersion> findByOfferIdInAndDomainId(List<Long> offerIds, Integer domainId);
    
    @Query("SELECT offerId FROM OfferVersion OV "
            + " WHERE id = :id "
            + " AND domainId = :domainId")
    Long findOfferId(@Param("id") Long id , @Param("domainId") Integer domainId);
}
