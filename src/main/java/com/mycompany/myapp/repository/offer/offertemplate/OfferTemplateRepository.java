package com.mycompany.myapp.repository.offer.offertemplate;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.mapper.offer.template.DropDownOfferTemplateVersionDTO;
import com.mycompany.myapp.repository.offer.common.OfferCommonRepository;

public interface OfferTemplateRepository extends OfferCommonRepository<OfferTemplate> {

    @Override
    <S extends OfferCommon> S save(S entity);

    @Query(value = "select OFFER_TEMPLATE_VERSION_ID as \"id\", NUMBER as \"number\" from offer_template_version where OFFER_TEMPLATE_ID = ?", nativeQuery = true)
    List<DropDownOfferTemplateVersionDTO> getListDropDownVersionOfOfferTemplate(Long id);

    @Query(value = "select o from OfferTemplate o where o.id in "
    		+ "(select otv.offerTemplateId from OfferTemplateVersion otv where otv.id in (:ids)) "
    		+ "and o.domainId = :domainId ")
    List<OfferTemplate> findOfferTemplateByOfferTemplateVersionIdAndDomainId(@Param(value="ids") List<Long> ids,
    																		 @Param(value = "domainId") Integer domainId );

    List<OfferTemplate> findByIdInAndDomainId(List<Long> ids, Integer domainId);
    
    Integer countByBillingCycleTypeIdAndDomainId(Long id , Integer domainId);
}
