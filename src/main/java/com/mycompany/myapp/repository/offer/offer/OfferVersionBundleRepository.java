package com.mycompany.myapp.repository.offer.offer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.offer.OfferVersionBundle;
import com.mycompany.myapp.dto.offer.offer.VersionBunndleDTO;
import com.mycompany.myapp.repository.offer.common.VersionBundleCommonRepository;

public interface OfferVersionBundleRepository extends VersionBundleCommonRepository<OfferVersionBundle>{

    @Override
    <S extends OfferVersionBundle> S save(S entity);
    
    @Query(value = " SELECT O.id as id, "
            + " O.name as name , "
            + " O.description as description, "
            + " otvb.id as offerBundleId "
            + " FROM Offer O RIGHT JOIN OfferVersionBundle otvb "
            + " ON otvb.childOfferId = O.id "
            + " WHERE otvb.rootOfferVersionId = :versionId "
            + " AND otvb.domainId = :domainId ")
    Page<VersionBunndleDTO> findBundles(@Param(value = "versionId") Long versionid , @Param(value = "domainId") Integer domainId, Pageable pageable);

    List<OfferVersionBundle> findByDomainIdAndRootOfferVersionId(Integer domainId,  Long parentId);
   
    List<OfferVersionBundle> deleteByDomainIdAndRootOfferVersionId(Integer domainId,  Long parentI);
}
