package com.mycompany.myapp.repository.offer.offer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecUse;
import com.mycompany.myapp.dto.offer.common.CharOfVersionDTO;
import com.mycompany.myapp.repository.offer.common.VersionCharspecUseCommonRepository;

public interface OfferVersionCharspecUseRepository extends VersionCharspecUseCommonRepository<OfferVersionCharspecUse>, JpaRepository<OfferVersionCharspecUse, Long> {

    <S extends OfferVersionCharspecUse> S save(S entity);

    List<OfferVersionCharspecUse> deleteByOfferVersionId(Long offerVersionId);

    @Query(" SELECT E.id as id, C.id as charspecId, C.name as name, V.value as value, C.description as description, C.extensible as extensible, V.id as charspecValueUseId"
        + " FROM OfferVersionCharspecUse E "
        + "  INNER JOIN Characteristic C ON E.charspecId = C.id "
        + "  INNER JOIN OfferVersionCharspecValueUse V ON E.id = V.charspecUseId "
        + " WHERE E.offerVersionId = :id "
        + "  AND E.domainId = :domainId ")
    Page<CharOfVersionDTO> getListChildCharacteristic(@Param("id") Long id, @Param("domainId") Integer domainId, Pageable pageable);

    List<OfferVersionCharspecUse> findByDomainIdAndOfferVersionId(Integer domainId, Long offerVersionId);

    Integer countByCharspecIdAndDomainId(Long charspecId, Integer domainId);
}
