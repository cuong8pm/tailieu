package com.mycompany.myapp.repository.offer.offertemplate;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionAction;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.offer.common.VersionActionCommonRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OfferTemplateVersionActionRepository extends VersionActionCommonRepository<OfferTemplateVersionAction>, OCSCloneMapRepository<OfferTemplateVersionAction, Long> {
    Integer countByActionIdAndDomainId(Long actionId, Integer domainId);

    @Query(" select a.id as actionId, a.name as name, a.description as description, ova.allowMobileMoney as allowMobileMoney, ova.id as id, ova.posIndex as posIndex " +
        " from OfferTemplateVersionAction as ova " +
        " left join Action as a " +
        " on ova.actionId = a.id " +
        " where ova.offerTemplateVersionId = :offerTemplateVersionId " +
        " and ova.domainId =:domainId " )
    Page<OfferActionDTO> getListOfferTemplateVersionAction(@Param("offerTemplateVersionId") Long offerTemplateVersionId, @Param("domainId") Integer domainId, Pageable pageable);

    @Query(" select oft.actionId from OfferTemplateVersionAction oft where oft.offerTemplateVersionId =:offerTemplateVersionId and oft.domainId =:domainId ")
    List<Long> getListActionId(@Param("offerTemplateVersionId") Long offerTemplateVersionId, @Param("domainId") Integer domainId );

    OfferTemplateVersionAction findByOfferTemplateVersionIdAndActionIdAndDomainId(Long parentId, Long actionId, Integer domainId);

    List<OfferTemplateVersionAction> findByDomainIdAndParentId(Integer domainId, Long parentId);
}
