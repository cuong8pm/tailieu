package com.mycompany.myapp.repository.offer.offer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.offer.OfferRelationship;
import com.mycompany.myapp.dto.offer.common.OfferCustomDTO;
import com.mycompany.myapp.dto.offer.common.RelationshipMappingOfferCommonDTO;
import com.mycompany.myapp.repository.offer.common.OfferRelationshipCommonRepository;

public interface OfferRelationshipRepository extends OfferRelationshipCommonRepository<OfferRelationship> {

    @Query(" SELECT  new com.mycompany.myapp.dto.offer.common.OfferCustomDTO( O.id, O.name ) "
            + " FROM Offer O "
            + "  INNER JOIN OfferRelationship E ON O.id = E.childId "
            + "  INNER JOIN ReferTable R on E.relationshipType = R.value "
            + " WHERE E.parentId = :id "
            + "  AND E.domainId = :domainId "
            + "  AND (:type is null OR E.relationshipType = :type) "
            + "  AND (:name is null OR O.name like %:name%) "
            + "  AND R.referType = 34 "
            + " GROUP BY O.id, O.name  ")
    Page<OfferCustomDTO> getListChildOffer(@Param("id") Long id, @Param("type") Integer type, @Param("name") String name, @Param("domainId") Integer domainId, Pageable pageable);

    @Query(" SELECT  new com.mycompany.myapp.dto.offer.common.OfferCustomDTO( O.id, O.name ) "
            + " FROM Offer O "
            + "  INNER JOIN OfferRelationship E ON O.id = E.parentId  "
            + "  INNER JOIN ReferTable R on E.relationshipType = R.value "
            + " WHERE E.childId = :id "
            + "  AND E.domainId = :domainId "
            + "  AND (:type is null OR E.relationshipType = :type) "
            + "  AND (:name is null OR O.name like %:name%) "
            + "  AND R.referType = 34 "
            + " GROUP BY O.id, O.name  ")
    Page<OfferCustomDTO> getListParentOffer(@Param("id") Long id, @Param("type") Integer type, @Param("name") String name, @Param("domainId") Integer domainId, Pageable pageable);

    @Query(" SELECT E.relationshipType as relationshipType, E.childId as offerId "
            + " FROM OfferRelationship E "
            + " WHERE E.parentId = :parentId "
            + "   AND E.domainId = :domainId ")
    List<RelationshipMappingOfferCommonDTO> getChildRelationShip(@Param("parentId") Long parentId, @Param("domainId") Integer domainId);

    @Query(" SELECT E.relationshipType as relationshipType, E.parentId as offerId "
            + " FROM OfferRelationship E "
            + " WHERE E.childId = :childId "
            + "   AND E.domainId = :domainId ")
    List<RelationshipMappingOfferCommonDTO> getParentRelationShip(@Param("childId") Long childId, @Param("domainId") Integer domainId);

    @Override
    <S extends OfferRelationship> S save(S entity);

    List<OfferRelationship> deleteByParentIdAndDomainId(Long parentId, Integer domainId);

    List<OfferRelationship> deleteByChildIdAndDomainId(Long childId, Integer domainId);

    List<OfferRelationship> findByParentIdAndDomainId(Long parentId, Integer domainId);
}
