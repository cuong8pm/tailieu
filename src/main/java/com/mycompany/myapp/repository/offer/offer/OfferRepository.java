package com.mycompany.myapp.repository.offer.offer;

import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.repository.offer.common.OfferCommonRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OfferRepository extends OfferCommonRepository<Offer> {

    @Override
    <S extends OfferCommon> S save(S entity);

    List<Offer> findAllByExternalIdAndDomainId(Long externalId, Integer domainId);

    Offer findTopByOrderByExternalIdDesc();

    @Query("SELECT o FROM Offer o WHERE o.id IN (SELECT DISTINCT v.parentId FROM OfferVersion v WHERE v.id IN :ids)")
    List<Offer> getOffersByOfferVersions(@Param("ids") List<Long> offerVersionIds);

    Offer findByDomainIdAndExternalId(Integer domainId, Long externalId);

    Integer countByBillingCycleTypeIdAndAndDomainId(Long billingCycleTypeId, Integer domainId);

    @Query(" select o from Offer o where o.name =:name and o.domainId =:domainId")
    Offer findByNameAndDomainIdCheck(@Param("name") String name, @Param("domainId") Integer domainId);

    @Query("select o.externalId from Offer o " +
        "where o.id = (select v.offerId from OfferVersion v where v.id = :versionId and v.domainId = :domainId ) " +
        "and o.domainId = :domainId")
    Long getExternalIdByOfferVersionId(@Param(value = "versionId") Long versionId,@Param("domainId") Integer domainId);

    Optional<Offer> findFirstByExternalIdAndDomainId(Long externalId,Integer domainId);

    @Query(value = "select o from Offer o where o.id in "
        + "(select otv.offerId from OfferVersion otv where otv.id in (:ids)) "
        + "and o.domainId = :domainId ")
    List<Offer> findOfferByOfferVersionIdAndDomainId(@Param(value="ids") List<Long> ids,
                                                                             @Param(value = "domainId") Integer domainId );
    Offer getOfferById( Long id);
    List<Offer> findAllByIdIn(List<Long> ids);
}
