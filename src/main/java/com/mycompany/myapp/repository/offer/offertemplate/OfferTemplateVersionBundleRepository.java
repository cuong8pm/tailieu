package com.mycompany.myapp.repository.offer.offertemplate;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionBundle;
import com.mycompany.myapp.dto.offer.offer.VersionBunndleDTO;
import com.mycompany.myapp.repository.offer.common.VersionBundleCommonRepository;

public interface OfferTemplateVersionBundleRepository extends VersionBundleCommonRepository<OfferTemplateVersionBundle> {

    @Query(value  = " SELECT O.id as id , "
            + " O.name as name , "
            + " OTVB.id as offerBundleId , "
            + " O.description as description "
            + " FROM Offer O "
            + " RIGHT JOIN OfferTemplateVersionBundle OTVB on O.id = OTVB.childOfferId  "
            + " WHERE OTVB.offerTemplateVersionId = :versionId "
            + " AND OTVB.domainId = :domainId ")
    Page<VersionBunndleDTO> findBundles(@Param("versionId") Long versionId , @Param("domainId") Integer domainId, Pageable pageable);
    
    @Override
    <S extends OfferTemplateVersionBundle> S save(S entity);
  
    List<OfferTemplateVersionBundle> deleteByOfferTemplateVersionIdAndDomainId(Long offerTemplateVersionId, Integer domainId);

}
