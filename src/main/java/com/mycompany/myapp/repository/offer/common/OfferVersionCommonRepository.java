package com.mycompany.myapp.repository.offer.common;

import java.util.List;

import javax.transaction.Transactional;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface OfferVersionCommonRepository<T extends OfferVersionCommon> extends OCSCloneableRepository<T, Long>  {

    <S extends OfferVersionCommon> S save(S entity);

    @Query("SELECT max(v.number) FROM #{#entityName} v WHERE v.parentId = ?1")
    Long getMaxNumber(Long parentId);

    @Query("SELECT v.id FROM #{#entityName} v WHERE v.parentId = ?1 AND v.state = 1")
    List<Long> getListActiveVersion(Long parentId);

    @Transactional
    @Modifying
    @Query("UPDATE #{#entityName} v SET v.state = 0 WHERE v.id = :versionId")
    void updateStateOfVersion(@Param("versionId") Long versionId);

    @Query("SELECT max(v.number) FROM #{#entityName} v WHERE v.parentId = ?2 AND v.domainId = ?1")
    Integer getMaxNumberByDomainIdAndParentId(Integer domainId, Long parentId);

    List<OCSCloneableEntity> findByDomainIdAndParentId(Integer domainId, Long parentId);

    @Query(value = "select otv "
            + "from #{#entityName} otv join ReferTable rf ON otv.state = rf.value "
            + "where otv.parentId = :offerId and otv.domainId = :domainId and rf.referType = 36")
    Page<T> findAllVersionByOfferId(@Param("offerId") Long offerId, @Param("domainId") Integer domainId,
                                  Pageable pageable);

    List<T> findByDomainIdAndParentIdIn(Integer domainId, List<Long> parentIds);
}

