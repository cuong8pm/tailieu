package com.mycompany.myapp.repository.offer.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.offer.common.OfferRelationshipCommon;
import com.mycompany.myapp.repository.OCSCloneMapRepository;

@NoRepositoryBean
public interface OfferRelationshipCommonRepository<T extends OfferRelationshipCommon> extends JpaRepository<T, Long>, OCSCloneMapRepository<T, Long> {

    void deleteByIdAndDomainId(Long id, Integer domainId);

    @Query(" SELECT E.relationshipType "
            + " FROM #{#entityName} E " 
            + " WHERE E.domainId = :domainId "
            + "  AND E.parentId = :parentId "
            + "  AND E.childId = :childId "
            + "  AND (:isParent is null or E.isParent = :isParent)")
    List<Integer> getListRelationshipUse(@Param("parentId") Long parentId, @Param("childId") Long childId, @Param("isParent") Boolean isParent, @Param("domainId") Integer domainId);
    
    @Query(" SELECT R "
            + " FROM #{#entityName} E "
            + "  INNER JOIN ReferTable R on E.relationshipType = R.value " 
            + " WHERE E.domainId = :domainId "
            + "  AND E.parentId = :parentId "
            + "  AND E.childId = :childId "
            + "  AND R.referType = 34 "
            + "  AND (:type is null OR R.value = :type) "
            + "  AND (:isParent is null or E.isParent = :isParent) ")
    List<ReferTable> getListRelationshipType(@Param("parentId") Long parentId, @Param("childId") Long childId, @Param("type") Integer type, @Param("isParent") Boolean isParent, @Param("domainId") Integer domainId);

    T findByParentIdAndChildIdAndRelationshipTypeAndDomainId(Long parentId, Long childId, Integer relationshipType, Integer domainId);

    void deleteByParentIdAndChildIdAndRelationshipTypeAndDomainId(Long parentId, Long childId, Integer relationshipType, Integer domainId);
    
    void deleteByParentIdAndChildIdAndRelationshipTypeAndIsParentAndDomainId(Long parentId, Long childId, Integer relationshipType, Boolean isParent, Integer domainId);

    <S extends T> S save(S entity);
}
