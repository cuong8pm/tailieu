package com.mycompany.myapp.repository.offer.offer;

import com.mycompany.myapp.domain.offer.offer.OfferVersionAction;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import com.mycompany.myapp.repository.offer.common.VersionActionCommonRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OfferVersionActionRepository extends VersionActionCommonRepository<OfferVersionAction>, OCSCloneMapRepository<OfferVersionAction, Long> {
    Integer countByActionIdAndDomainId(Long actionId, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    @Query(" select a.id as actionId, a.name as name, a.description as description, ova.allowMobileMoney as allowMobileMoney, ova.id as id, ova.posIndex as posIndex " +
        " from OfferVersionAction as ova " +
        " left join Action as a " +
        " on ova.actionId = a.id " +
        " where ova.offerVersionId = :offerVersionId " +
        " and ova.domainId =:domainId " )
    Page<OfferActionDTO> getListOfferVersionAction(@Param("offerVersionId") Long offerVersionId, @Param("domainId") Integer domainId, Pageable pageable);

    OfferVersionAction findTopByParentIdAndDomainIdOrderByPosIndexDesc(Long perentId, Integer domainId);

    OfferVersionAction findByActionIdAndOfferVersionIdAndDomainId(Long actionId, Long offerVersionId, Integer domainId);

    List<OfferVersionAction> findByDomainIdAndParentId(Integer domainId, Long parentId);

    @Query(" select ofv.actionId from OfferVersionAction ofv where ofv.offerVersionId " +
        "=:offerVersionId and ofv.domainId =:domainId ")
    List<Long> getListActionId(@Param("offerVersionId") Long offerVersionId, @Param("domainId") Integer domainId );

    OfferVersionAction findByDomainIdAndOfferVersionIdAndActionId(Integer domainId, Long offerVersionId, Long actionId);
}
