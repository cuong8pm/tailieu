package com.mycompany.myapp.repository.offer.offertemplate;


import java.util.List;

import com.mycompany.myapp.domain.constant.ReferType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRedirection;
import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import com.mycompany.myapp.repository.offer.common.VersionRedirectionCommonRepository;

public interface OfferTemplateVersionRedirectionRepository extends VersionRedirectionCommonRepository<OfferTemplateVersionRedirection>{

    <S extends OfferTemplateVersionRedirection> S save(S entity);

    List<OfferTemplateVersionRedirection> deleteByOfferTemplateVersionId(Long offerTemplateVersionId);

    @Query(" select o.id as id, o.url as url, o.name as redirectionTypeName , r.value as redirectionType " +
        "from OfferTemplateVersionRedirection o left join ReferTable r" +
        " on o.redirectionType = r.value " +
        " where o.offerTemplateVersionId =:offerTemplateVersionId" +
        " and o.domainId =:domainId " +
        " and r.referType =:referType ")
    List<OfferOfVersionRedirectionDTO> getOfferTemplateVersionRedirection(@Param("offerTemplateVersionId") Long offerTemplateVersionId, @Param("domainId") Integer domainId, @Param("referType") ReferType referType);
}
