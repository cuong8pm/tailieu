package com.mycompany.myapp.repository.offer.offer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.offer.OfferVersionRedirection;
import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionRedirectionDTO;
import com.mycompany.myapp.repository.offer.common.VersionRedirectionCommonRepository;

public interface OfferVersionRedirectionRepository extends VersionRedirectionCommonRepository<OfferVersionRedirection> {

    <S extends OfferVersionRedirection> S save(S entity);

    List<OfferVersionRedirection> deleteByOfferVersionId(Long offerVersionId);
}
