package com.mycompany.myapp.repository.offer.common;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.repository.OCSCloneableRepository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface OfferCommonRepository<T extends OfferCommon> extends OCSCloneableRepository<T, Long> {

    <S extends OfferCommon> S save(S entity);

    @Query("SELECT o from #{#entityName} o " +
        " where o.domainId = :domainId " +
        " AND (:parentId is null or o.categoryId = :parentId) " +
        " AND (:name is null or o.name LIKE %:name%) " +
        " AND (:description is null or o.description LIKE %:description%) ")
    Page<T> findOfferCommons(@Param("parentId") Long parentId, @Param("domainId") Integer domainId, @Param("name") String name,
                                         @Param("description") String description, Pageable pageable);

}
