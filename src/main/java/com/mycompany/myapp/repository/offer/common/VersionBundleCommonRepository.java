package com.mycompany.myapp.repository.offer.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;
import com.mycompany.myapp.repository.OCSCloneableRepository;

@NoRepositoryBean
public interface VersionBundleCommonRepository<E extends VersionBundleCommon> extends OCSCloneableRepository<E, Long>, JpaRepository<E, Long> {

    List<E> findByDomainIdAndChildOfferIdInAndVersionId(Integer domainId, List<Long> childs, Long versionId);

    <S extends VersionBundleCommon> S save(S entity);

    List<E> findByDomainIdAndChildOfferId(Integer domainId, Long child);

    void deleteByIdAndDomainId(Long id , Integer domainId);

    Integer countByDomainIdAndChildOfferIdAndVersionId(Integer domainId, Long childId, Long versionId);

    List<E> findByDomainIdAndParentId(Integer domainId , Long versionId);
}
