package com.mycompany.myapp.repository.offer.offertemplate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.repository.offer.common.OfferVersionCommonRepository;

import java.util.List;

public interface OfferTemplateVersionRepository extends OfferVersionCommonRepository<OfferTemplateVersion> {

    @Override
    <S extends OfferVersionCommon> S save(S entity);

    List<OfferTemplateVersion> findByOfferTemplateIdInAndDomainId(List<Long> offerIds, Integer domainId);

    List<OfferTemplateVersion> findByIdInAndDomainId(List<Long> ids, Integer domainId);
}
