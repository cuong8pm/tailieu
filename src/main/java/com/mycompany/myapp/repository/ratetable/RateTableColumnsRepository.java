package com.mycompany.myapp.repository.ratetable;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.ratetable.RateTableColumn;
import com.mycompany.myapp.repository.OCSCloneMapRepository;

public interface RateTableColumnsRepository extends JpaRepository<RateTableColumn, Long>, OCSCloneMapRepository<RateTableColumn, Long> {

    void deleteByRateTableIdAndDomainId(Long rateTableId, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);

    List<RateTableColumn> findByRateTableIdAndDomainIdOrderByPosIndexAsc(Long rateTableId, Integer domainId);

    @Query(value = " SELECT N.name "
            + " FROM RateTableColumn R "
            + "   INNER JOIN Normalizer N ON R.normalizerId = N.id "
            + " WHERE R.domainId = :domainId "
            + "   AND R.rateTableId = :rateTableId "
            + " ORDER BY R.posIndex ASC ")
    List<String> getNormalizeNames(@Param("rateTableId") Long rateTableId, @Param("domainId") Integer domainId);

    @Query(value = " SELECT R.posIndex "
            + " FROM RateTableColumn R "
            + " WHERE R.domainId = :domainId "
            + "   AND R.rateTableId = :rateTableId ")
    List<Integer> getColumnIndex(@Param("rateTableId") Long rateTableId, @Param("domainId") Integer domainId);

    @Query(value = " SELECT R.normalizerId "
            + " FROM RateTableColumn R "
            + " WHERE R.domainId = :domainId "
            + "   AND R.rateTableId = :rateTableId ")
    List<Long> getNormalizerIds(@Param("rateTableId") Long rateTableId, @Param("domainId") Integer domainId);

    @Query(value = " SELECT N "
            + " FROM RateTableColumn R "
            + "   INNER JOIN Normalizer N ON R.normalizerId = N.id "
            + " WHERE R.domainId = :domainId "
            + "   AND R.rateTableId = :rateTableId "
            + " ORDER BY R.posIndex ASC ")
    List<Normalizer> getNormalizers(@Param("rateTableId") Long rateTableId, @Param("domainId") Integer domainId);

    Integer countByRateTableIdAndDomainId(Long id, Integer domainId);

    List<RateTableColumn> findByRateTableIdAndDomainIdAndIdNotIn(Long rateTableId, Integer domainId, List<Long> lstColumnUpdateId);

    List<RateTableColumn> findByRateTableIdAndDisplayNameAndDomainId(Long rateTableId, String displayName, Integer domainId);

    @Override
    <S extends RateTableColumn> S save(S entity);
}
