package com.mycompany.myapp.repository.ratetable;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import com.mycompany.myapp.repository.OCSMoveableRepository;

public interface RateTableRepository extends OCSCloneableRepository<RateTable, Long>, JpaSpecificationExecutor<RateTable> {

    @Query(value = " SELECT R "
            + " FROM RateTable R "
            + " WHERE R.domainId = :domainId "
            + "   AND (:parentId is null or R.parentId = :parentId) "
            + "   AND (:name is null or R.name LIKE %:name%) "
            + "   AND (:description is null or R.description LIKE %:description%)")
    Page<RateTable> getRateTables(@Param("parentId") Long categoryId,
            @Param("name") String name,
            @Param("description") String description,
            @Param("domainId") Integer domainId,
            Pageable pageable);

    List<RateTable> findByDomainIdAndUnitTypeId(Integer domainId, Long unitTypeId);

}
