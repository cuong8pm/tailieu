package com.mycompany.myapp.repository.ratetable;

import com.mycompany.myapp.repository.OCSCloneMapRepository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.ratetable.RateTableColumn;
import com.mycompany.myapp.domain.ratetable.RateTableRow;

public interface RateTableRowsRepository extends JpaRepository<RateTableRow, Long>, JpaSpecificationExecutor<RateTableRow>, OCSCloneMapRepository<RateTableRow, Long> {

    void deleteByRateTableIdAndDomainId(Long rateTableId, Integer domainId);

    void deleteByIdAndDomainId(Long id, Integer domainId);
    
    List<RateTableRow> findByRateTableIdAndDomainId(Long rateTableId, Integer domainId);
    
    List<RateTableRow> findByRateTableIdAndDomainIdAndIdNotIn(Long rateTableId, Integer domainId, List<Long> lstId);
    
    RateTableRow findByIdAndDomainId(Long id, Integer domainId);
    
    @Query("SELECT R.formulaId FROM RateTableRow R WHERE R.domainId = :domainId AND R.rateTableId IN (:rateTableIds)")
    List<Long> findFormulaIdByDomainIdAndRateTableIdIn(@Param("domainId") Integer domainId, @Param("rateTableIds") List<Long> rateTableIds);
    
    @Override
    <S extends RateTableRow> S save(S entity);
}
