package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface OCSCloneableRepository<T extends OCSCloneableEntity, ID extends Serializable>
    extends OCSMoveableRepository<T, ID> {

    @Override
    T findByIdAndDomainId(ID id, Integer domainId);

    @Override
    List<T> findByNameContainingAndDomainId(String name, Integer domainId);


    List<T> findByNameStartsWithAndDomainId(String name, Integer domainId);

    @Override
    <S extends T> S save(S entity);

    @Override
    <S extends T> S saveAndFlush(S entity);


    @Query(value = "select o.parentId as id, max(o.posIndex) as countChild " +
        "from #{#entityName} o " +
        "where o.parentId in (:ids) and o.domainId = :domainId group by o.parentId")
    List<CountChildrenByParentDTO> getMaxPosIndexChildByParentId(@Param("ids") List<Long> ids, @Param("domainId") Integer domainId);
}
