package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.dto.ratetable.DropDownCustomDTO;

public interface UnitTypeRepository extends OCSMoveableRepository<UnitType, Long> {
    @Query("SELECT MAX(unitOrder) FROM UnitType WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxUnitOrder(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

    @Query("SELECT C FROM UnitType C "
        + "WHERE C.domainId = :domainId "
        + "AND (:parentId is null or C.categoryId = :parentId) "
        + "AND (:name is null or C.name LIKE %:name%) "
        + "AND (:description is null or C.remark LIKE %:description%) ")
    Page<UnitType> findUnitTypes(@Param("parentId") Long parentId,
                                 @Param("domainId") Integer domainId,
                                 @Param("name") String name,
                                 @Param("description") String description,
                                 Pageable pageable);

    @Query(value = "SELECT UNIT_TYPE_ID as \"id\", "
            + "NAME as \"name\", "
            + "ABBREVIATION as \"abbreviation\", "
            + "BASE_RATE as \"baseRate\" FROM unit_type", nativeQuery = true)
    List<DropDownCustomDTO> getLstUnitType();

    boolean existsByIdAndDomainId(Long unitTypeId, Integer domainId);

    @Query("SELECT C.baseRate FROM UnitType C "
        + "WHERE C.domainId = :domainId "
        + "AND C.id = :unitTypeId ")
    Integer findBaseRateByIdAndDomain(@Param("unitTypeId") Long unitTypeId,@Param("domainId") Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM rate_table as rt " +
        "WHERE UNIT_TYPE_ID = :unitTypeId ", nativeQuery = true)
    Long countUnitTypeUseByRateTable(@Param("unitTypeId") Long unitTypeId);
}
