package com.mycompany.myapp.repository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.domain.constant.BillingCycleStatus;

public interface BillingCycleRepository extends JpaRepository<BillingCycle, Long>, JpaSpecificationExecutor<BillingCycle> {

    List<BillingCycle> findByParentIdAndDomainId(Long parentId, Integer domainId);

    @Query("SELECT BC FROM BillingCycle BC "
            + "WHERE BC.domainId = :domainId "
            + "AND BC.billingCycleTypeId = :billingCycleTypeId "
            + "AND (:beginDate is null or function('date_format', BC.beginDateString,'%d/%m/%Y') like %:beginDate%) "
            + "AND (:endDate is null or function('date_format', BC.endDateString,'%d/%m/%Y') like %:endDate%) " )
    Page<BillingCycle> findBillingCycle(
            @Param("billingCycleTypeId") Long billingCycleTypeId,
            @Param("domainId") Integer domainId,
            @Param("beginDate") String beginDate,
            @Param("endDate") String endDate,
            Pageable pageable);

    void deleteByIdInAndDomainIdAndBillingCycleTypeId(List<Long> ids, Integer domainId, Long billingCycleTypeId);

    Long countByParentIdAndDomainId(Long parentId, Integer domainId);

    @Modifying
    @Query("UPDATE BillingCycle BC "
            + "SET BC.state = :status "
            + "WHERE BC.domainId = :domainId "
            + "AND BC.billingCycleTypeId = :billingCycleTypeId ")
    void updateBillingCycleStatus(@Param("domainId") Integer domainId, @Param("billingCycleTypeId") Long billingCycleTypeId, @Param("status") BillingCycleStatus status);

    @Query("SELECT COUNT(BC.id) FROM BillingCycle BC "
            + "WHERE BC.id >= :minId "
            + "AND BC.id NOT IN (:deleteIds) "
            + "AND BC.billingCycleTypeId = :billingCycleTypeId "
            + "AND BC.domainId = :domainId ")
    Long countUpIdToDelete(@Param("minId") Long minId, @Param("deleteIds") List<Long> deleteIds,
            @Param("billingCycleTypeId") Long billingCycleTypeId, @Param("domainId") Integer domainId);

    @Query("SELECT COUNT(BC.id) FROM BillingCycle BC "
            + "WHERE BC.id <= :maxId "
            + "AND BC.id NOT IN (:deleteIds) "
            + "AND BC.billingCycleTypeId = :billingCycleTypeId "
            + "AND BC.domainId = :domainId ")
    Long countDownIdToDelete(@Param("maxId") Long maxId, @Param("deleteIds") List<Long> deleteIds,
            @Param("billingCycleTypeId") Long billingCycleTypeId, @Param("domainId") Integer domainId);

    BillingCycle findByIdAndDomainId(Long id, Integer domainId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    default BillingCycle findByIdAndDomainIdForUpdate(Long id, Integer domainId) {
        return findByIdAndDomainId(id, domainId);
    }
}
