package com.mycompany.myapp.repository.formula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.domain.formula.Formula;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FormulaRepository extends OCSCloneableRepository<Formula, Long>, JpaSpecificationExecutor<Formula> {

    void deleteByIdAndDomainId(Long id, Integer domainId);

    Formula findByIdAndDomainId(Long id, Integer domainId);

    List<Formula> findByDomainIdAndIdIn(Integer domainId, List<Long> formulaIds);

    List<Formula> findByDomainIdAndFormulaType(Integer domainId, Long formulaType);

    List<Formula> findByIdInAndDomainId(List<Long> ids, Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM formula as f " +
        "WHERE (A = :parameterId AND MOD(TEMPLATE_BITS, 8) >= 4) " +
        "OR (B = :parameterId AND MOD(TEMPLATE_BITS, 4) >= 2) " +
        "OR (PER = :parameterId AND MOD(TEMPLATE_BITS, 2) = 1) " +
        "OR (FORMULA_TYPE = :parameterId AND MOD(TEMPLATE_BITS, 16) >= 8)", nativeQuery = true)
    Long countFormulaUseChar(@Param("parameterId") Long parameterId);

}
