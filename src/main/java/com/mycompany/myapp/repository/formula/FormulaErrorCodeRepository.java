package com.mycompany.myapp.repository.formula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.formula.FormulaErrorCode;

public interface FormulaErrorCodeRepository extends JpaRepository<FormulaErrorCode, Long>, JpaSpecificationExecutor<FormulaErrorCode> {
    
    @Query("SELECT F.errorCode FROM FormulaErrorCode F WHERE F.id = :id AND F.domainId = :domainId")
    Long getErrorCodeOfFormula(@Param("id") Long id, @Param("domainId") Integer domainId);
    
    List<FormulaErrorCode> findByDomainIdOrderByErrorCodeAsc(Integer domainId);

    FormulaErrorCode findByNameAndDomainId(String name, Integer domainId);
    
    FormulaErrorCode findByErrorCodeAndDomainId(Long errorCode, Integer domainId);
}
