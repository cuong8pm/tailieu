package com.mycompany.myapp.repository.formula;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.mycompany.myapp.domain.formula.FormulaUnitConvert;

import org.springframework.data.repository.query.Param;

public interface FormulaUnitConvertRepository extends JpaRepository<FormulaUnitConvert, Long>, JpaSpecificationExecutor<FormulaUnitConvert> {
    
    @Query("SELECT F.name FROM FormulaUnitConvert F WHERE F.id = :id AND F.domainId = :domainId")
    String getConvertTypeOfFormula(@Param("id") Long id, @Param("domainId") Integer domainId);
    
    List<FormulaUnitConvert> findByDomainId(Integer domainId);
}
