package com.mycompany.myapp.repository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.NoRepositoryBean;

import com.mycompany.myapp.domain.OCSBaseEntity;

@NoRepositoryBean
public interface OCSBaseRepository<T extends OCSBaseEntity, ID> extends JpaRepository<T, ID> {

    T findByIdAndDomainId(ID id, Integer domainId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteByIdAndDomainId(ID id, Integer domainId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    default T findByIdAndDomainIdForUpdate(ID id, Integer domainId) {
        return findByIdAndDomainId(id, domainId);
    }

    List<T> findByNameContainingAndDomainId(String name, Integer domainId);

    List<T> findByNameContainingAndDomainId(String name, Integer domainId, Sort sort);

//    Long countByParentIdAndDomainId(Long categoryId, Integer domainId);

    List<T> findByDomainId(Integer domainId);

    List<T> findByDomainId(Integer domainId, Sort sort);

    List<T> findByDomainIdAndIdNotIn(Integer domainId, List<Long> removeIds, Sort sort);

    List<T> findByNameContainingAndDomainIdAndIdNotIn(String name, Integer domainId, List<Long> removeIds, Sort sort);

//    default T findByNameAndDomainId(String name, Integer domainId) {
//        return findByNameIgnoreCaseAndDomainId(name, domainId);
//    }

    T findByNameIgnoreCaseAndDomainId(String name, Integer domainId);

    List<T> findByNameAndDomainId(String name, Integer domainId);

}
