package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ZoneMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ZoneMapRepository extends OCSMoveableRepository<ZoneMap, Long>{
    @Query("SELECT MAX(posIndex) FROM ZoneMap WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);


    @Query("SELECT C FROM ZoneMap C "
        + "WHERE C.domainId = :domainId "
        + "AND (:parentId is null or C.categoryId = :parentId) "
        + "AND (:name is null or C.name LIKE %:name%) "
        + "AND (:description is null or C.remark LIKE %:description%) ")
    Page<ZoneMap> findZoneMaps(@Param("parentId") Long parentId,
                                 @Param("domainId") Integer domainId,
                                 @Param("name") String name,
                                 @Param("description") String description,
                                 Pageable pageable);

    @Query("SELECT zm.name FROM ZoneMap zm " +
        "WHERE zm.domainId =:domainId " +
        "AND zm.id =:zoneMapId ")
    String findNameByIdAndDomain(@Param("zoneMapId") Long zoneMapId, @Param("domainId") Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM normalizer_params as np " +
        "WHERE CONFIG_INPUT LIKE CONCAT('zoneValueId:', :zoneMapId, ';dataZoneType:1%') ", nativeQuery = true)
    Long countNormUseZoneMap(@Param("zoneMapId") Long zoneMapId);
}
