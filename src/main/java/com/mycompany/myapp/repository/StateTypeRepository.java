package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.StateType;

public interface StateTypeRepository extends OCSMoveableRepository<StateType, Long>, JpaSpecificationExecutor<StateType> {

    @Query("SELECT MAX(S.posIndex) FROM StateType S WHERE S.parentId = :parentId AND S.domainId = :domainId")
    Integer getMaxPosIndex(@Param("parentId") Long parentId, @Param("domainId") Integer domainId);

    @Query("SELECT S FROM StateType S "
        + "WHERE S.domainId = :domainId "
        + "AND (:parentId is null or S.parentId = :parentId) "
        + "AND (:name is null or S.name LIKE %:name%) "
        + "AND (:description is null or S.stateDesc LIKE %:description%) ")
    Page<StateType> findStateType(@Param("parentId") Long parentId,
                                  @Param("domainId") Integer domainId,
                                  @Param("name") String name,
                                  @Param("description") String description,
                                  Pageable pageable);

    List<StateType> findByStateGroupIdAndDomainId(Long stateGroupId, Integer domainId);
    
    List<StateType> findByParentIdAndDomainIdOrderByPosIndexAsc(Long categoryId, Integer domainId);

    Integer countByStateGroupIdAndDomainId(Long stateGroupId, Integer domainId);
}
