package com.mycompany.myapp.repository.event;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.event.Event;
import com.mycompany.myapp.repository.OCSCloneableRepository;

public interface EventRepository extends OCSCloneableRepository<Event, Long>, JpaSpecificationExecutor<Event> {

    @Query(value = " SELECT E "
            + " FROM Event E "
            + " WHERE E.domainId = :domainId "
            + "   AND (:parentId is null or E.parentId = :parentId) "
            + "   AND (:name is null or E.name LIKE %:name%) "
            + "   AND (:description is null or E.description LIKE %:description%)")
    Page<Event> getListChildEvents(@Param("parentId") Long categoryId, @Param("name") String name, @Param("description") String description, @Param("domainId") Integer domainId, Pageable pageable);

    @Query(" select count(e.id) from Event e where e.eventAnalysisFilter = :ratingFilterId and e.domainId = :domainId ")
    Integer checkUsedRatingFilter(@Param("ratingFilterId") Long ratingFilterId, @Param("domainId") Integer domainId);

    List<Event> findByDomainIdAndEventAnalysisFilterIn(Integer domainId, List<Long> ratingFilterIds);
}
