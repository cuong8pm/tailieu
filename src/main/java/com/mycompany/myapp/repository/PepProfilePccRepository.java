package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.PepProfilePcc;
import com.mycompany.myapp.repository.custom.PepProfilePccCustomRepository;

public interface PepProfilePccRepository extends OCSMoveableRepository<PepProfilePcc, Long>, JpaSpecificationExecutor<PepProfilePcc>, PepProfilePccCustomRepository {

    void deleteByPccRuleIdAndPepProfileIdAndDomainId(Long pccRuleId, Long pepProfileId, Integer domainId);

    Integer countByPepProfileIdAndDomainId(Long id, Integer domainId);

    Integer countByPccRuleIdAndPepProfileIdAndDomainId(Long pccRuleId, Long pepProfileId, Integer domainId);

    @Query(value = "SELECT P.pccRuleId "
            + " FROM PepProfilePcc P "
            + " WHERE P.pepProfileId = :id ")
    List<Long> getPccRuleIdByPepProfileId(@Param("id") Long id);

    PepProfilePcc findTopByParentIdAndDomainIdOrderByPosIndexDesc(Long parentId, Integer domainId);

    PepProfilePcc findByPccRuleIdAndDomainIdAndPepProfileId(Long pccRuleId, Integer domainId, Long pepProfileId);

    Integer countByPccRuleIdAndDomainId(Long id, Integer domainId);
}
