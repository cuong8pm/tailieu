package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Zone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ZoneRepository extends OCSMoveableRepository<Zone, Long>, JpaSpecificationExecutor<Zone> {

    @Query("SELECT C FROM Zone C "
        + "WHERE C.domainId = :domainId "
        + "AND (:zoneMapId is null or C.zoneMapId = :zoneMapId) "
        + "AND (:name is null or C.name LIKE %:name%) "
        + "AND (:description is null or C.remark LIKE %:description%) "
        + "AND (:zoneCode is null or C.zoneCode LIKE %:zoneCode%) "
        + "AND (:id is null or C.tempId like %:id%) ")
    Page<Zone> findZones(@Param("zoneMapId") Long zoneMapId,
                         @Param("domainId") Integer domainId,
                         @Param("name") String name,
                         @Param("description") String description,
                         @Param("zoneCode") String zoneCode,
                         @Param("id") String id,
                         Pageable pageable);

    Zone findZoneByNameAndDomainIdAndZoneMapId(String name, Integer domainId, Long zoneMapId);

    @Query("SELECT z.name FROM Zone z " +
        "WHERE z.id = :zoneId " +
        "AND z.domainId = :domainId ")
    String findNameByIdAndDomain(@Param("zoneId") Long zoneId, @Param("domainId") Integer domainId);

    @Query(value = "SELECT COUNT(*) FROM normalizer_params as np " +
        "WHERE CONFIG_INPUT LIKE CONCAT('zoneValueId:', :zoneId, ';dataZoneType:0%') ", nativeQuery = true)
    Long countNormUseZone(@Param("zoneId") Long zoneId);
}
