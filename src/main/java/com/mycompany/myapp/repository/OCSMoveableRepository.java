package com.mycompany.myapp.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.NoRepositoryBean;

import com.mycompany.myapp.domain.OCSMoveableEntity;
import com.mycompany.myapp.domain.PepProfilePcc;

@NoRepositoryBean
public interface OCSMoveableRepository<T extends OCSMoveableEntity, ID extends Serializable>
        extends OCSBaseRepository<T, ID> {

    @Override
    T findByIdAndDomainId(ID id, Integer domainId);

    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteByIdAndDomainId(ID id, Integer domainId);

    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout",value = "0")})
    default T findByIdAndDomainIdForUpdate(ID id, Integer domainId) {
        return findByIdAndDomainId(id, domainId);
    }

    List<T> findByParentIdAndDomainId(Long parentId, Integer domainId);

    Long countByParentIdAndDomainId(Long categoryId, Integer domainId);

    List<T> findByNameContainingAndDomainId(String name, Integer domainId);

    List<T> findByNameContainingAndDomainId(String name, Integer domainId, Sort sort);

    T findTopByParentIdAndDomainIdOrderByPosIndexDesc(Long categoryId, Integer domainId);

    List<T> findByIdInAndDomainId(List<Long> ids, Integer domainId);

    List<T> findByDomainIdAndParentIdIn(Integer domainId, List<Long> parentIds);

    List<T> findByDomainId(Integer domainId);

    List<T> findByDomainIdAndIdIn(Integer domainId, List<Long> ids);

    Page<T> findByParentIdAndDomainId(Long parentId, Integer domainId,Pageable pageable);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteAllByDomainIdAndIdIn(Integer domainId, List<Long> ids);

}
