package com.mycompany.myapp.repository;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.ReserveInfo;

public interface ReserveInfoRepository extends OCSMoveableRepository<ReserveInfo, Long>, JpaSpecificationExecutor<ReserveInfo> {
    
    @Query("SELECT R FROM ReserveInfo R "
            + "WHERE R.domainId = :domainId "
            + "AND (:categoryId is null or R.categoryId = :categoryId) "
            + "AND (:name is null or R.resvName LIKE %:name%) "
            + "AND (:id is null or R.tempId LIKE %:id%) "
            + "AND (:description is null or R.resvDesc LIKE %:description%) ")
    Page<ReserveInfo> findReserveInfos(@Param("categoryId") Long categoryId,
                                 @Param("domainId") Integer domainId,
                                 @Param("name") String name,
                                 @Param("description") String description,
                                 @Param("id") String id,
                                 Pageable pageable);
    
    ReserveInfo findByIdAndDomainId(@Param("id") Long id, @Param("domainId") Integer domainId);
    
//    @Lock(LockModeType.PESSIMISTIC_WRITE)
//    @Query("SELECT R FROM ReserveInfo R WHERE R.id = :id AND R.domainId = :domainId")
//    ReserveInfo findByIdAndDomainIdForUpdate(@Param("id") Long id, @Param("domainId") Integer domainId);
    
    @Query("SELECT MAX(posIndex) FROM ReserveInfo R WHERE R.categoryId = :categoryId AND R.domainId = :domainId")
    Integer getMaxPosIndex(@Param("categoryId") Long categoryId, @Param("domainId") Integer domainId);
    
//    Long countByCategoryIdAndDomainId(Long categoryId, Integer domainId);
}
