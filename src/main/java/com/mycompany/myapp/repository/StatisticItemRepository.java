package com.mycompany.myapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.StatisticItem;

public interface StatisticItemRepository extends OCSMoveableRepository<StatisticItem, Long> {

    @Query("SELECT S FROM StatisticItem S "
            + "WHERE S.domainId = :domainId "
            + "AND (:parentId is null or S.parentId = :parentId) "
            + "AND (:name is null or S.name LIKE %:name%) "
            + "AND (:id is null or S.tempId LIKE %:id%) "
            + "AND (:description is null or S.description LIKE %:description%) ")
    Page<StatisticItem> findStatistics(@Param("parentId") Long parentId,
            @Param("domainId") Integer domainId,
            @Param("name") String name,
            @Param("description") String description,
            @Param("id") String id,
            Pageable pageable);

    @Query("SELECT MAX(posIndex) FROM StatisticItem WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

    @Query(value = "SELECT COUNT(*) FROM formula as f " +
        "WHERE STATISTIC_ITEMS LIKE CONCAT('%;', :staticId, ';%') " +
        "OR STATISTIC_ITEMS LIKE CONCAT(:staticId, ';%') " +
        "OR STATISTIC_ITEMS LIKE CONCAT('%;', :staticId) " +
        "OR STATISTIC_ITEMS LIKE :staticId ", nativeQuery = true)
    Long countStaticUseByFormula(@Param("staticId") Long staticId);
}
