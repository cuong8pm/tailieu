package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.MonitorKey;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MonitorKeyRepository extends JpaRepository<MonitorKey, Long>, JpaSpecificationExecutor<MonitorKey>{
    
    MonitorKey findByIdAndDomainId(Long id, Integer domainId);
    
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteByIdAndDomainId(Long id, Integer domainId);
    
    @Query("SELECT M FROM MonitorKey M "
            + "WHERE M.domainId = :domainId "
            + "AND (:monitorKey is null or M.tempMonitorKey LIKE %:monitorKey%) "
            + "AND (:description is null or M.remark LIKE %:description%) ")
    Page<MonitorKey> findMonitorKeys(@Param("domainId") Integer domainId,
                                     @Param("monitorKey") String monitorKey,
                                     @Param("description") String description,
                                     Pageable pageable);

    MonitorKey findByMonitorKeyAndDomainId(Long monitorKey, Integer domainId);
    
    List<MonitorKey> findByDomainId(Integer domainId);
}
