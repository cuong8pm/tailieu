package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.mycompany.myapp.domain.QosClass;

public interface QosClassRepository extends JpaRepository<QosClass, String>, JpaSpecificationExecutor<QosClass>{
}
