package com.mycompany.myapp.repository;

import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.OCSService;


public interface OcsServiceRepository extends OCSMoveableRepository<OCSService, Long>, JpaSpecificationExecutor<OCSService>{
    @Query("SELECT S FROM OCSService S "
         + "WHERE S.domainId = :domainId "
         + "AND (:parentId is null or S.categoryId = :parentId) "
         + "AND (:name is null or S.name LIKE %:name%) "
         + "AND (:description is null or S.serviceDesc LIKE %:description%) ")
    Page<OCSService> findServices(@Param("parentId") Long parentId,
                                  @Param("domainId") Integer domainId,
                                  @Param("name") String name,
                                  @Param("description") String description,
                                  Pageable pageable);
    
    @Query("SELECT MAX(S.posIndex) FROM OCSService S WHERE S.parentId = :parentId AND S.domainId = :domainId")
    Integer getMaxPosIndex(@Param("parentId") Long parentId, @Param("domainId") Integer domainId);
}
