package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.domain.PepProfile;

public interface PepProfileRepository extends OCSMoveableRepository<PepProfile, Long>, JpaSpecificationExecutor<PepProfile> {

    @Query("SELECT MAX(P.posIndex) FROM PepProfile P WHERE P.parentId = :parentId AND P.domainId = :domainId")
    Integer getMaxPosIndex(@Param("parentId") Long parentId, @Param("domainId") Integer domainId);

    @Query("SELECT P FROM PepProfile P "
            + "WHERE P.domainId = :domainId "
            + "AND (:parentId is null or P.parentId = :parentId) "
            + "AND (:name is null or P.name LIKE %:name%) "
            + "AND (:description is null or P.remark LIKE %:description%) "
            + "AND (:monitorKey is null or P.tempMonitorKey LIKE %:monitorKey%) "
            + "AND (:priority is null or P.tempPriority LIKE %:priority%) ")
        Page<PepProfile> findPepProfiles(@Param("parentId") Long parentId,
                                      @Param("domainId") Integer domainId,
                                      @Param("name") String name,
                                      @Param("monitorKey") String monitorKey,
                                      @Param("priority") String priority,
                                      @Param("description") String description,
                                      Pageable pageable);

    List<PepProfile> findByDomainIdAndQosId(Integer domainId, Long qosId);
    
    List<PepProfile> findByDomainIdAndMonitorKey(Integer domainId, Long monitorKeyId);
}
