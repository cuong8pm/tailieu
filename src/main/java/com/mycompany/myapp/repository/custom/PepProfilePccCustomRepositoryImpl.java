package com.mycompany.myapp.repository.custom;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import com.mycompany.myapp.domain.PccRule;
import com.mycompany.myapp.dto.PccRuleDTO;
import com.mycompany.myapp.mapper.PccRuleMapper;
import com.mycompany.myapp.utils.OCSUtils;

@Component
public class PepProfilePccCustomRepositoryImpl implements PepProfilePccCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PccRuleMapper pccRuleMapper;

    public List<PccRuleDTO> getListPccRuleMapping(Long id, String name, String priority, String description,
                                                  Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        StringBuilder query = new StringBuilder();
        query.append(" SELECT PR ")
            .append(" FROM PccRule PR ")
            .append(" INNER JOIN PepProfilePcc PPP ON PR.id = PPP.pccRuleId ")
            .append(" WHERE PPP.pepProfileId = :id ")
            .append(" AND PR.domainId = :domainId ")
            .append(" AND (:name is null or PR.name LIKE :nameLike) ")
            .append(" AND (:priority is null or PR.tempPriority LIKE :priorityLike) ")
            .append(" AND (:description is null or PR.remark LIKE :descriptionLike) ");
        setOrderBy(query, pageable);
        List<PccRule> list = entityManager.createQuery(query.toString(), PccRule.class)
            .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
            .setMaxResults(pageable.getPageSize())
            .setParameter("id", id)
            .setParameter("name", name)
            .setParameter("priority", priority)
            .setParameter("description", description)
            .setParameter("nameLike", "%" + name + "%")
            .setParameter("priorityLike", "%" + priority + "%")
            .setParameter("descriptionLike", "%" + description + "%")
            .setParameter("domainId", domainId)
            .getResultList();
        List<PccRuleDTO> dtos = pccRuleMapper.toDto(list);

        return dtos;
    }

    private void setOrderBy(StringBuilder query, Pageable pageable) {
        if (pageable.getSort() == null || pageable.getSort().isEmpty()) {
            query.append(" ORDER BY PPP.posIndex ASC ");
        }
        if (pageable != null && !pageable.getSort().isEmpty()) {
            query.append("ORDER BY ");
            Iterator<Order> iterator = pageable.getSort().iterator();
            while (iterator.hasNext()) {
                Order order = iterator.next();
                query.append(getSearchParam(order.getProperty())).append(" ").append(order.getDirection());
            }
        }
    }

    private String getSearchParam(String property) {
        switch (property) {
            case "name":
                return "PR.name";
            case "description":
                return "PR.remark";
            case "priority":
                return "PR.priority";
            default:
                return "PPP.posIndex";
        }
    }

    public Integer getTotalPccRuleMapping(Long id, String name, String priority, String description) {
        Integer domainId = OCSUtils.getDomain();
        StringBuilder countQuery = new StringBuilder();
        countQuery.append(" SELECT COUNT(PR.id) ")
            .append(" FROM PccRule PR ")
            .append(" INNER JOIN PepProfilePcc PPP ON PR.id = PPP.pccRuleId ")
            .append(" WHERE PPP.pepProfileId = :id ")
            .append(" AND PR.domainId = :domainId ")
            .append(" AND (:name is null or PR.name LIKE :nameLike) ")
            .append(" AND (:priority is null or PR.tempPriority LIKE :priorityLike) ")
            .append(" AND (:description is null or PR.remark LIKE :descriptionLike) ");

        Integer total = entityManager.createQuery(countQuery.toString(), Long.class)
            .setParameter("id", id)
            .setParameter("name", name)
            .setParameter("priority", priority)
            .setParameter("description", description)
            .setParameter("nameLike", "%" + name + "%")
            .setParameter("priorityLike", "%" + priority + "%")
            .setParameter("descriptionLike", "%" + description + "%")
            .setParameter("domainId", domainId)
            .getSingleResult().intValue();

        return total;
    }

    public List<Integer> getListIndexPccRuleMapping(Long id, String name, String priority, String description,
                                                    Pageable pageable) {
        Integer domainId = OCSUtils.getDomain();
        StringBuilder query = new StringBuilder();
        query.append(" SELECT PPP.posIndex ")
            .append(" FROM PccRule PR ")
            .append(" INNER JOIN PepProfilePcc PPP ON PR.id = PPP.pccRuleId ")
            .append(" WHERE PPP.pepProfileId = :id ")
            .append(" AND PR.domainId = :domainId ")
            .append(" AND (:name is null or PR.name LIKE :nameLike) ")
            .append(" AND (:priority is null or PR.tempPriority LIKE :priorityLike) ")
            .append(" AND (:description is null or PR.remark LIKE :descriptionLike) ");
        setOrderBy(query, pageable);

        List<Integer> list = entityManager.createQuery(query.toString(), Integer.class)
            .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
            .setMaxResults(pageable.getPageSize())
            .setParameter("id", id)
            .setParameter("name", name)
            .setParameter("priority", priority)
            .setParameter("description", description)
            .setParameter("nameLike", "%" + name + "%")
            .setParameter("priorityLike", "%" + priority + "%")
            .setParameter("descriptionLike", "%" + description + "%")
            .setParameter("domainId", domainId)
            .getResultList();
        return list;
    }

}
