package com.mycompany.myapp.repository.custom;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.dto.PccRuleDTO;

public interface PepProfilePccCustomRepository {
    List<PccRuleDTO> getListPccRuleMapping(Long id, String name, String priority, String description,
            Pageable pageable);
    
    Integer getTotalPccRuleMapping(Long id, String name, String priority, String description);
    
    List<Integer> getListIndexPccRuleMapping(Long id, String name, String priority, String description,
            Pageable pageable);
}
