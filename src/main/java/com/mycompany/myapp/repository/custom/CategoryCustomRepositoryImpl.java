package com.mycompany.myapp.repository.custom;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CategoryCustomRepositoryImpl implements CategoryCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;
//
//    @Override
//    public Page<Category> search(SearchCriteria searchCriteria, Pageable pageable) {
//        // Count
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("SELECT C FROM Category C \n");
//        Query query = getQuery(searchCriteria, stringBuilder, pageable);
//
//        long totalRows = count(searchCriteria);
//
//        return new PageImpl<>(query.getResultList(), pageable, totalRows);
//    }
//
//    private int count(SearchCriteria searchCriteria) {
//        // Count
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("SELECT COUNT(*) FROM Category C \n");
//        Query query = getQuery(searchCriteria, stringBuilder, null);
//
//        Long totalRows = (Long) query.getResultList().get(0);
//
//        return totalRows.intValue();
//    }
//
//    private Query getQuery(SearchCriteria searchCriteria, StringBuilder stringBuilder, Pageable pageable) {
//        stringBuilder.append("WHERE 1 = 1 ");
//
//        if (StringUtils.isNotEmpty(searchCriteria.getName())) {
//            stringBuilder.append("AND C.name like :name ");
//        }
//        if (StringUtils.isNotEmpty(searchCriteria.getDescription())) {
//            stringBuilder.append("AND C.remark like :remark ");
//        }
//        stringBuilder.append("AND C.domainId = :domainId ");
//        stringBuilder.append("AND C.categoryType = :categoryType ");
//        stringBuilder.append("AND C.categoryParentId = :categoryParentId ");
//        if (pageable != null && !pageable.getSort().isEmpty()) {
//            boolean hasOrder = false;
//            stringBuilder.append("ORDER BY ");
//            Iterator<Order> iterator = pageable.getSort().iterator();
//            while (iterator.hasNext()) {
//                Order order = iterator.next();
//                if (StringUtils.equals(order.getProperty(), "name")
//                        || StringUtils.equals(order.getProperty(), "description")) {
//                    stringBuilder.append(getSearchParam(order.getProperty())).append(" ").append(order.getDirection()).append(", ");
//                    hasOrder = true;
//                }
//            }
//            if (hasOrder) {
//                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(", "));
//            } else {
//                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf("ORDER BY "));
//            }
//        }
//        // Create Query
//        Query query = entityManager.createQuery(stringBuilder.toString());
//        if (StringUtils.isNotEmpty(searchCriteria.getName())) {
//            query.setParameter("name", "%" + searchCriteria.getName() + "%");
//        }
//        if (StringUtils.isNotEmpty(searchCriteria.getDescription())) {
//            query.setParameter(CategoryDTO.FieldNames.REMARK, "%" + searchCriteria.getDescription() + "%");
//        }
//        if (StringUtils.isNotEmpty(searchCriteria.getDescription())) {
//            query.setParameter(CategoryDTO.FieldNames.REMARK, "%" + searchCriteria.getDescription() + "%");
//        }
//        query.setParameter("domainId", searchCriteria.getDomainId());
//        query.setParameter("categoryType", searchCriteria.getCategoryType());
//        query.setParameter("categoryParentId", searchCriteria.getParentId());
//        
//        if (pageable != null) {
//            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
//            query.setMaxResults(pageable.getPageSize());
//        }
//        return query;
//    }
//    
//    private String getSearchParam(String property) {
//        switch (property) {
//        case "name":
//            return "name";
//        case "description":
//            return CategoryDTO.FieldNames.REMARK;
//        default:
//            return null;
//        }
//    }
}

