package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.domain.CalcUnit;

public interface CalcUnitRepository extends JpaRepository<CalcUnit, Long>, JpaSpecificationExecutor<CalcUnit> {
    List<CalcUnit> findByDomainId(Integer domainId);
    
    CalcUnit findByIdAndDomainId(Long id, Integer domainId);
}
