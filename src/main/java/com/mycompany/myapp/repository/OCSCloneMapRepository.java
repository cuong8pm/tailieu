package com.mycompany.myapp.repository;

import java.util.List;

import javax.persistence.LockModeType;
import javax.persistence.MappedSuperclass;

import com.mycompany.myapp.domain.eventProcessing.ConditionBuilderMap;
import com.mycompany.myapp.dto.common.CountChildrenByParentDTO;
import org.springframework.data.domain.Pageable;

import com.mycompany.myapp.domain.OCSCloneableMap;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@MappedSuperclass
public interface OCSCloneMapRepository<T extends OCSCloneableMap, ID> {

    List<T> findByDomainIdAndParentId(Integer domainId, Long parentId);

    List<T> findByDomainIdAndChildId(Integer domainId, Long childId);

    List<T> findByDomainIdAndParentId(Integer domainId, Long parentId, Pageable pageable);

    List<T> findByDomainIdAndChildId(Integer domainId, Long childId, Pageable pageable);

    List<T> findByDomainIdAndParentIdIn(Integer domainId, List<Long> parentIds);

    List<T> findByDomainIdAndChildIdIn(Integer domainId, List<Long> childIds);

    Integer countByDomainIdAndParentId(Integer domainId, Long parentId);

    <S extends T> List<S> saveAll(Iterable<S> entities);

    T findByDomainIdAndChildIdAndParentId(Integer domainId, Long childId, Long parentId);

    void deleteByDomainIdAndChildIdAndParentId(Integer domainId, Long childId, Long parentId);

    <S extends T> S save(S entity);

    Integer countByDomainIdAndChildId(Integer domainId, Long childId);

    void deleteByDomainIdAndParentId(Integer domainId, Long parentId);

    List<T> findByDomainId(Integer domainId);

    @Query(value = "select o.parentId as id, count(o.childId) as countChild " +
        "from #{#entityName} o " +
        "where o.parentId in (:ids) and o.domainId = :domainId group by o.parentId")
    List<CountChildrenByParentDTO> countChildrenByParentIdInAndDomainId(@Param("ids") List<Long> ids, @Param("domainId") Integer domainId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void deleteAllByDomainIdAndParentIdIn(Integer domainId, List<Long> parentIds);
}
