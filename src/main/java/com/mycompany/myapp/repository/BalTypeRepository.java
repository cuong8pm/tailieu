package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.BalType;
import com.mycompany.myapp.domain.constant.Block;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BalTypeRepository extends OCSMoveableRepository<BalType, Long>, JpaSpecificationExecutor<BalType>, OCSCloneableRepository<BalType, Long> {

    @Query("SELECT bt from BalType bt "
        + "where bt.domainId = :domainId "
        + "AND (:parentId is null or bt.categoryId = :parentId) "
        + "AND (:name is null or bt.name LIKE %:name%) "
        + "AND (:description is null or bt.description LIKE %:description%) ")
    Page<BalType> findBalTypes(@Param("parentId") Long parentId, @Param("domainId") Integer domainId, @Param("name") String name, @Param("description") String description, Pageable pageable);

    @Query("SELECT MAX(posIndex) FROM BalType WHERE domainId = :domainId and categoryId = :categoryId")
    Integer getMaxPosIndex(@Param("domainId") Integer domainId, @Param("categoryId") Long categoryId);

    @Query(value = "select count(*) " +
        "from block b where b.AFFECTED_OBJECT_TYPE = :affectedObjectType "+
        "and b.AFFECTED_VALUE = :balanceId and b.DOMAIN_ID = :domainId", nativeQuery = true)
    Integer isUsingByBlockTable(@Param("affectedObjectType") Integer affectedObjectType ,@Param("balanceId") Long balanceId, @Param("domainId") Integer domainId);

    @Query("SELECT bt from BalType bt "
        + "where bt.domainId = :domainId "
        + "AND bt.acm = :acm "
        + "AND (:name is null or bt.name LIKE %:name%) ")
    List<BalType> findByNameAndDomainAndAcm(@Param("domainId") Integer domainId, @Param("name") String name, @Param("acm") boolean acm, Sort sort);

    List<BalType> findByDomainIdAndUnitTypeId(Integer domainId, Long unitTypeId);

    @Query("select count(b.id) " +
        "from BalType b where b.billingCycleTypeId = :billingCycleTypeId ")
    Integer isUsingBillingCycle(@Param("billingCycleTypeId") Integer billingCycleTypeId);

    @Query("SELECT bt.id from BalType bt "
        + "where bt.domainId = :domainId "
        + "AND bt.unitTypeId = :unitTypeId ")
    List<Long> findIdByUnitTypeId(@Param("unitTypeId") Long unitTypeId, @Param("domainId") Integer domainId);
}
