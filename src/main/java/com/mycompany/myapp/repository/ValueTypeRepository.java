package com.mycompany.myapp.repository;


import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.domain.characteristic.ValueType;

/**
 * Spring Data  repository for the ValueType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ValueTypeRepository extends JpaRepository<ValueType, Long> {
}
