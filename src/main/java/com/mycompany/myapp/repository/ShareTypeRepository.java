package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ShareType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShareTypeRepository extends JpaRepository<ShareType, Integer>, JpaSpecificationExecutor<ShareType> {
    List<ShareType> findByDomainId(Integer domainId);
}
