package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ThresholdTriggerMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ThresholdTriggerMapRepository
		extends JpaRepository<ThresholdTriggerMap, Long>, JpaSpecificationExecutor<ThresholdTriggerMap> {

}