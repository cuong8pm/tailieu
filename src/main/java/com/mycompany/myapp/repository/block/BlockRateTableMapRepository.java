package com.mycompany.myapp.repository.block;

import com.mycompany.myapp.domain.block.BlockRateTableMap;
import com.mycompany.myapp.dto.block.RateTableProjection;
import com.mycompany.myapp.repository.OCSCloneMapRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BlockRateTableMapRepository extends JpaRepository<BlockRateTableMap, Long>, OCSCloneMapRepository<BlockRateTableMap, Long> {
    Integer countByRateTableIdAndDomainId(Long rateTableId, Integer domainId);

    Integer countByComponentTypeAndBlockIdAndDomainId(Integer componentType, Long blockId, Integer domainId);

    List<BlockRateTableMap> findByDomainIdAndBlockIdAndComponentTypeOrderByPosIndexAsc (Integer domainId, Long blockId, Integer componentType);

    List<BlockRateTableMap> findByDomainIdAndParentIdOrderByComponentTypeAscPosIndexAsc(Integer domainId, Long parentId);

    @Query("SELECT RT.id as id, " +
        "          RT.name as name, " +
        "          RT.description as description," +
        "          BM.posIndex as posIndex, " +
        "          BM.componentType as componentType "+
        "FROM BlockRateTableMap BM " +
        "INNER JOIN " +
        "RateTable RT " +
        "ON BM.rateTableId = RT.id " +
        "AND BM.domainId = RT.domainId " +
        "WHERE BM.blockId = :blockId " +
        "AND BM.domainId = :domainId " +
        "ORDER BY BM.posIndex ASC")
    List<RateTableProjection> getRateTable(@Param("blockId") Long blockId, @Param("domainId") Integer domainId);

    BlockRateTableMap findByDomainIdAndBlockIdAndRateTableIdAndComponentType(Integer domainId, Long blockId, Long rateTableId, Integer componentType);

    @Query("SELECT B.rateTableId FROM BlockRateTableMap B WHERE B.blockId = :blockId AND B.domainId = :domainId AND B.componentType = :componentType")
    List<Long> getRateTableIds(@Param("blockId") Long blockId, @Param("domainId") Integer domainId, @Param("componentType") Integer componentType);
    @Override
    <S extends BlockRateTableMap> S save(S entity);

    @Query(value = "select max(b.posIndex) from BlockRateTableMap b " +
        "where b.domainId = :domainId and b.blockId = :blockId and b.componentType = :type ")
    Integer getMaxPosIndexByBlockId(@Param("blockId")Long blockId,
                                    @Param("domainId")Integer domainId,
                                    @Param("type") Integer type);
}
