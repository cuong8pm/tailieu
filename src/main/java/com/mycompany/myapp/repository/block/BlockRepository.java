package com.mycompany.myapp.repository.block;

import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.repository.OCSCloneableRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface BlockRepository extends OCSCloneableRepository<Block, Long> {

    @Query("SELECT bl from Block bl " +
        " where bl.domainId = :domainId " +
        " AND (:parentId is null or bl.categoryId = :parentId) " +
        " AND (:name is null or bl.name LIKE %:name%) " +
        " AND (:description is null or bl.description LIKE %:description%) ")
    Page<Block> findBlocks(@Param("parentId") Long parentId, @Param("domainId") Integer domainId, @Param("name") String name, @Param("description") String description, Pageable pageable);

    Integer countByBlockFilterIdAndDomainId(Long blockFilterId, Integer domainId);

    List<Block> findAllByBlockFilterIdInAndDomainId(List<Long> blockFilterIds, Integer domainId);

    List<Block> findAllByAffectedValueAndAffectedObjectTypeAndDomainId(Long value, Integer type,Integer domainId);

    @Query("SELECT bl FROM Block bl where bl.affectedObjectType = 5 and bl.affectedValue = :paramId")
    List<Block> findBlockByParameterId(@Param("paramId") Long id);
    List<Block> findAllByDomainIdAndIdIn(Integer domainId, List<Long> listId );

}
