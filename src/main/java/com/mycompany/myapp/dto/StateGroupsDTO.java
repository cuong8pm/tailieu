package com.mycompany.myapp.dto;

import java.util.Collection;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class StateGroupsDTO extends Tree implements Moveable {

//    private Long id;

    @NotNull(groups = {StateGroupsDTO.Create.class, StateGroupsDTO.Update.class})
    @Positive(groups = {StateGroupsDTO.Create.class, StateGroupsDTO.Update.class})
    private Long categoryId;

    private Integer domainId;

    @Size(max = 255, groups = {StateGroupsDTO.Create.class, StateGroupsDTO.Update.class})
    private String description;

    private String categoryName;
    
    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.STATEGROUPS;
    
    @JsonView(Tree.class)
    private String icon = Resources.STATEGROUPS;
    
    @JsonView(Tree.class)
    private Integer categoryLevel;

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
