package com.mycompany.myapp.dto;

import java.util.Collection;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class OcsServiceDTO extends Tree {

    @NotNull
    private Long categoryId;

    private Integer domainId;

    private String description;

    private String categoryName;

    @NotNull(groups = { Move.class, Update.class })
    @Min(value = 0, groups = { Move.class, Update.class })
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.SERVICE;

    @JsonView(Tree.class)
    private String icon = Resources.SERVICE;
    
    private Integer categoryLevel;

    public Long getParentId() {
        return categoryId;
    }

    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public Collection<Tree> getTemplates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
        // TODO Auto-generated method stub

    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
