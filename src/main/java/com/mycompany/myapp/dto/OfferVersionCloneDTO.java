package com.mycompany.myapp.dto;

import com.mycompany.myapp.domain.constant.State;

import lombok.Builder;
import lombok.Data;

@Data
public class OfferVersionCloneDTO extends CloneDTO{
    private State state;
    private String description;
    private Boolean isCloneRelationship;
}
