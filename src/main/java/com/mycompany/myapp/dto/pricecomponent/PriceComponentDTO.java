package com.mycompany.myapp.dto.pricecomponent;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = { "name", "description" })
public class PriceComponentDTO extends TreeClone implements Moveable {

    @NotNull(groups = { PriceComponentDTO.Create.class, PriceComponentDTO.Update.class })
    private Long categoryId;

    private String categoryName;

    @JsonView(Tree.class)
    private String description;

    private Integer domainId;

    @Valid
    @NotEmpty(groups = { PriceComponentDTO.Create.class })
    private List<PriceComponentBlockMapDTO> blockMapItems;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.PRICE_COMPONENT;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

}
