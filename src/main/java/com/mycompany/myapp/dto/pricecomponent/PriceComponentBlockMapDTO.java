package com.mycompany.myapp.dto.pricecomponent;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.utils.PriceComponentGenerating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TrimString(fieldNames = { "description" })
@PriceComponentGenerating(fieldName = "isUseForGenerating", dependFieldName = "genneratingType", groups = { PriceComponentDTO.Create.class, PriceComponentDTO.Update.class } )
public class PriceComponentBlockMapDTO {
    @JsonProperty("priceComponentBlockMapId")
    private Long id;

    private Long priceComponentId;

    @JsonProperty("id")
    @NotNull(groups = { PriceComponentDTO.Create.class, PriceComponentDTO.Update.class })
    private Long blockId;

    @JsonProperty("name")
    private String blockName;

    private String Description;

    private Boolean isUseForGenerating;

    private Integer genneratingType;

    private String genneratingTypeName;

    private Boolean isReplace;

    private Integer posIndex;

    private Integer domainId;

    private ActionType actionType;
}
