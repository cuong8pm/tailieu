package com.mycompany.myapp.dto;

import com.mycompany.myapp.domain.constant.ReferType;

public class OwnerLevelDTO extends BaseResponseDTO {
    private Integer referType = ReferType.OwnerLevel.getValue();
    private Integer value;

    public Integer getReferType() {
        return referType;
    }

    public void setReferType(Integer referType) {
        this.referType = referType;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
