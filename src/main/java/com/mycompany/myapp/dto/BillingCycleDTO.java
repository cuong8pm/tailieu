package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.BillingCycleStatus;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class BillingCycleDTO {
    private Long id;

    private Long billingCycleTypeId;

    private Timestamp cycleBeginDate;

    private Timestamp cycleEndDate;

    private BillingCycleStatus state;

    private Integer domainId;

    private String treeType = Resources.TreeType.TEMPLTE;

    private String type = Resources.BILLING_CYCLE_TYPE;
    
    @JsonView(Tree.class)
    private String icon = Resources.BILLING_CYCLE;

    private String categoryName;
    private Integer categoryLevel;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

}
