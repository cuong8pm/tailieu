package com.mycompany.myapp.dto.eventProcessing;

import java.util.List;

import javax.validation.Valid;
import javax.validation.groups.ConvertGroup;

import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.eventProcessing.CreateEventPolicyDTO.Validator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OutputDTO {

    private List<ReferTableDTO> profile;
    
    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Validator.class, to = FilterFieldDTO.Create.class),
    })
    private List<FilterFieldDTO> listField;
}
