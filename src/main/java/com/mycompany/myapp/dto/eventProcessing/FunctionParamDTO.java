package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
public class FunctionParamDTO extends BaseResponseDTO {
    @NotNull
    private Long functionId;

    @Min(value = 0, groups = { Create.class, Update.class })
    private Integer indexOrder;

    @NotNull(groups = { Create.class, Update.class })
    private Integer dataType;

    @NotNull(groups = { Create.class, Update.class })
    private boolean enable;

    @Length(max = 255, groups = { Create.class, Update.class })
    private String description;

    private String value;

    private Integer valueType;

    private String dataTypeName;

    private String valueTypeName;

    private Boolean edit = true;

    public FunctionParamDTO(Long id, String name, Long functionId, Integer indexOrder, Integer dataType,
                            String dataTypeName, Integer valueType, String value, Boolean enable){
        super.setId(id);
        super.setName(name);
        this.functionId = functionId;
        this.indexOrder = indexOrder;
        this.dataType = dataType;
        this.dataTypeName = dataTypeName;
        this.valueType = valueType;
        this.value = value;
        this.enable = enable;
    }

    public interface Create {
    }

    public interface Update {}
}
