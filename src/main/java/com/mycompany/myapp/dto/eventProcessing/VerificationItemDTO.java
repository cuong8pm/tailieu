package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class VerificationItemDTO extends BaseResponseDTO {
    private Boolean isMultiple;

    private Long verificationId;

    @NotBlank(groups = { Create.class, Update.class })
    private String itemId;

    private int itemIdNumber;

    public  void setItemIdNumber(){
        this.itemIdNumber = Integer.parseInt(this.itemId);
    }
    public  void setItemIdNumber(int itemIdNumber){
        this.itemIdNumber = itemIdNumber;
    }
    public  int getItemIdNumber(){
       return this.itemIdNumber ;
    }

    public  void setItemId(String itemId){
        this.itemId = itemId;
    }
    public  void setItemId(){
        this.itemId = String.valueOf(this.itemIdNumber);
    }
    public  String getItemId(){
        return this.itemId ;
    }
    public interface Create {}
    public interface Update {}
}
