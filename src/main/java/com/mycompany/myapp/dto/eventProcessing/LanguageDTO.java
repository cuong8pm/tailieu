package com.mycompany.myapp.dto.eventProcessing;


import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class LanguageDTO extends BaseResponseDTO {

    @NotBlank(groups = {Create.class, Update.class})
    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;


    public interface Create {
    }

    public interface Update {
    }


}
