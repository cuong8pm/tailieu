package com.mycompany.myapp.dto.eventProcessing;

public interface VerificationConditionDTO {

    Long getId();

    String getVerificationType();

    String getListType();

    String getName();
}
