package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.web.rest.errors.Resources;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EventObjectFieldDTO extends BaseResponseDTO {
    private Long eventObjectId;

    private Integer modelType;

    private Integer dataType;

    private boolean isPrimitive;

    private Long parentId;

    private String source;

    private String treeType = Resources.TreeType.TEMPLTE;

    private String type = Resources.EVENT_OBJECT_FIELD;

    private String icon = Resources.EVENT_OBJECT_FIELD;

    private List<EventObjectFieldDTO> templates;

    private Boolean alist = false;

    private Boolean hasChild = false;

    public interface Create {}

    public interface Update {}
}
