package com.mycompany.myapp.dto.eventProcessing;


import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FunctionSuggestionsDTO extends BaseResponseDTO {

    private Long fieldId;

    private Long functionId;

    public interface Create {
    }

    public interface Update {
    }

}
