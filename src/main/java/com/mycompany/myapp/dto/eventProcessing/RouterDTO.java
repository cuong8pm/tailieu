package com.mycompany.myapp.dto.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class RouterDTO extends TreeClone implements Moveable {

    private String properties;

    private List<PropertiesDTO> propertiesDTOS;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    @NotNull(groups = {Create.class, Update.class})
    private Long routerTypeId;

    private Long categoryId;

    private String categoryName;

    private Timestamp modifyDate;

    @NotNull(groups = {RouterDTO.Move.class })
    @Min(value = 0, groups = {RouterDTO.Move.class})
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private String icon = Resources.ROUTER;

    @JsonView(Tree.class)
    private String type = Resources.ROUTER;

    @JsonView(Tree.class)
    private boolean hasChild = false;

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
