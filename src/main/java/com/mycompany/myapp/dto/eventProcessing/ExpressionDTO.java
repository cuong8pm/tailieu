package com.mycompany.myapp.dto.eventProcessing;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.CreateEventPolicyDTO.Validator;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExpressionDTO extends BaseResponseDTO {

    @NotNull(groups = { Create.class, Update.class, Search.class })
    @PositiveOrZero(groups = { Create.class, Update.class, Validator.class, Search.class })
    private Integer inputType;

    @NotNull(groups = { Create.class, Update.class, Validator.class })
    @PositiveOrZero(groups = { Create.class, Update.class, Validator.class })
    private Integer dataType;

    @Length(max = 255, groups = { Create.class, Update.class, Validator.class })
    private String input;

    @NotNull(groups = { Create.class, Update.class, Validator.class })
    private Integer operator;

    @NotBlank(groups = { Create.class, Update.class, Validator.class })
    private String value;

    @NotNull(groups = { Create.class, Update.class, Validator.class })
    private Integer valueType;

    private Long eventObjectFieldId;

    private List<FunctionParamDTO> functionParams;

    @NotNull(groups = { Search.class })
    private Long linkedId;

    private String functionName;

    private String functionAlias;


    public interface Create {
    }

    public interface Update {
    }

    public interface Search {
    }

}
