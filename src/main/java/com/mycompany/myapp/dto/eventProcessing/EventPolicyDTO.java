package com.mycompany.myapp.dto.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EventPolicyDTO extends TreeClone implements Moveable {

    private Long categoryId;

    @Length(max = 255, groups = {Create.class,Update.class})
    @JsonView(Tree.class)
    private String description;

    @NotNull(groups = {Move.class })
    @Min(value = 0, groups = {Move.class})
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private String icon = Resources.EVENT_POLICY;

    @JsonView(Tree.class)
    private String type = Resources.EVENT_POLICY;

    @JsonView(Tree.class)
    private Boolean hasChild = false;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    private String categoryName;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }


    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
