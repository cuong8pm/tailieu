package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
public class ConditionBuilderMapDTO extends BaseResponseDTO {

    private Long conditionId;

    private Long builderId;

    public interface Create {
    }

    public interface Update {
    }

}
