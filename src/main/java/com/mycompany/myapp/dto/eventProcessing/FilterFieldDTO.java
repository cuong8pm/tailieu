package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FunctionParam;
import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

import org.hibernate.validator.constraints.Length;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class FilterFieldDTO extends BaseResponseDTO {

    private Long filterId;

    private Long fieldId;

    private String value;

    private Integer valueType;

    private Integer dataType;

    private String dataTypeName;

    @Length(max = 255, groups = {Update.class,Create.class})
    private String inputType;

    private Long linkedId;

    private List<FunctionParamInstanceDTO> instanceDTOs;

    private List<FunctionParamDTO> functionParams;

    private String fieldName;

    private List<Long> paramOrder;

    private String functionName;

    public FilterFieldDTO(Long id, Long filterId, Long fieldId, String fieldName, String value, Integer valueType,
            Integer dataType, String dataTypeName, String inputType, Long linkedId) {
        super(id, null);
        this.filterId = filterId;
        this.fieldId = fieldId;
        this.value = value;
        this.valueType = valueType;
        this.dataType = dataType;
        this.dataTypeName = dataTypeName;
        this.inputType = inputType;
        this.linkedId = linkedId;
        this.fieldName = fieldName;
    }

    public interface Create {
    }

    public interface Update {
    }
}
