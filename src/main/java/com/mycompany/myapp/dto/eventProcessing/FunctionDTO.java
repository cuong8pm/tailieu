package com.mycompany.myapp.dto.eventProcessing;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TrimString(fieldNames = {"name", "description", "alias"})
public class FunctionDTO extends TreeClone implements Moveable {

    private Long categoryId;

    @NotNull(groups = {Move.class})
    private Integer posIndex;

    @Length(max = 255, groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private String description;

    @JsonView(Tree.class)
    @NotNull(groups = {Create.class, Update.class})
    @PositiveOrZero
    private Integer dataType;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.FUNCTION;

    @JsonView(Tree.class)
    private String icon = Resources.FUNCTION;

    private Integer categoryLevel;

    private String categoryName;

    private Boolean edit = true;

    private List<FunctionParamDTO> functionParamDTOS;

    // dùng để lấy thông tin cho popup edit cấu hình value cho filter-field theo function
    private List<FunctionParameterDTO> functionParameterDTOs;

    @JsonView(Tree.class)
    private boolean hasChild = false;

    @JsonView(Tree.class)
    @Length(max = 255, groups = {Create.class, Update.class})
    @NotBlank(groups = {Create.class, Update.class})
    private String functionName;

    private Timestamp modifyDate;

    @NotNull(groups = {Create.class, Update.class})
    @PositiveOrZero
    private Integer functionType;

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }


    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

}
