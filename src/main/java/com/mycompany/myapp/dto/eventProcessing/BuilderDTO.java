package com.mycompany.myapp.dto.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Collection;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BuilderDTO extends TreeClone implements Moveable {

    private Long categoryId;

    private String categoryName;

    private String preProcess;

    private String postProcess;

    private String reProduce;

    @NotNull(groups = { Create.class, Update.class })
    @Positive(groups = {Create.class, Update.class})
    private Long builderTypeId;

    @JsonView(Tree.class)
    private String builderTypeName;

    private String router;

    private Long conditionId;

    private String conditionName;

    @Length(max = 255, groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private String description;

    @NotNull(groups = {Move.class })
    @Min(value = 0, groups = {Move.class})
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private String icon = Resources.BUILDER;

    @JsonView(Tree.class)
    private String type = Resources.BUILDER;

    @JsonView(Tree.class)
    private Boolean hasChild = false;

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public BuilderDTO(Long id, String name, Long builderTypeId, String builderTypeName, String router) {
        this.setId(id);
        this.setName(name);
        this.builderTypeId = builderTypeId;
        this.builderTypeName = builderTypeName;
        this.router = router;
    }

    public BuilderDTO(Long id, String name, Long builderTypeId, String builderTypeName) {
        this.setId(id);
        this.setName(name);
        this.builderTypeId = builderTypeId;
        this.builderTypeName = builderTypeName;
    }

    public BuilderDTO(Long id, String name, Long conditionId, String conditionName, Integer posIndex, String description){
        this.setId(id);
        this.setName(name);
        this.conditionName = conditionName;
        this.conditionId = conditionId;
        this.posIndex = posIndex;
        this.description = description;
    }

}
