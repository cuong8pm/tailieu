package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@NoArgsConstructor
@Data
public class BuilderTypeRouterMapDTO extends BaseResponseDTO {

    private Long builderTypeId;

    private Long routerId;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String validation;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    private Integer priority;

    public interface Create {
    }

    public interface Update {
    }
}
