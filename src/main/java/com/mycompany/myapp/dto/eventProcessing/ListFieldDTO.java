package com.mycompany.myapp.dto.eventProcessing;

public interface ListFieldDTO {

    Long getId();

    Long getFilterId();

    Long getFieldId();

    String getFieldName();

    String getValue();

    Integer getValueType();

    Integer getDataType();

    String getInputType();

    Long getLinkedId();

}
