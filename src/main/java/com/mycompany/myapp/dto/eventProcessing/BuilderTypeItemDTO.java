package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;


@Data
@NoArgsConstructor
public class BuilderTypeItemDTO extends BaseResponseDTO {

    private Long builderTypeId;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    public interface Create {
    }

    public interface Update {
    }
}
