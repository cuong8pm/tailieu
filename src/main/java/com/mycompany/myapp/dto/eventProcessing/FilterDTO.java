package com.mycompany.myapp.dto.eventProcessing;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.groups.ConvertGroup;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.CreateEventPolicyDTO.Validator;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO.Create;
import com.mycompany.myapp.dto.ratetable.RateTableDTO.Update;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FilterDTO extends TreeClone implements Moveable {
    private Long categoryId;

    @JsonView(Tree.class)
    @NotNull(groups = { Move.class })
    private Integer posIndex;

    @Length(max = 255, groups = { Create.class, Update.class, Validator.class })
    @JsonView(Tree.class)
    private String description;

    @NotNull(groups = { Create.class, Update.class, Validator.class })
    @Positive
    private Long routerId;

    @Positive
    @NotNull(groups = { Create.class, Update.class, Validator.class })
    private Long moduleId;

    @Positive
    @NotNull(groups = { Create.class, Update.class, Validator.class })
    private Long eventPolicyId;

    private Timestamp modifyDate;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.FILTER;

    @JsonView(Tree.class)
    private String icon = Resources.FILTER;

    private Integer categoryLevel;

    private String categoryName;

    private String eventPolicyName;

    private String routerName;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    @JsonView(Tree.class)
    private Boolean hasChild = false;

    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Validator.class, to = ExpressionDTO.Create.class),
    })
    private List<ExpressionDTO> expressions;

    @Valid
    private OutputDTO outputs;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {}

    public interface Update {}

    public interface Move {}

}
