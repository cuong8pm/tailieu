package com.mycompany.myapp.dto.eventProcessing;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpressionConditionDTO {

    private Long id;

    private String input;

    private String value;

    private Integer operator;

    private String operatorName;

    private Integer valueType;

    private Integer dataType;

    private String dataTypeName;

    private Integer inputType;

    private String inputTypeName;

    private Long linkedId;

    private String functionAlias;

    private List<FunctionParamDTO> functionParams;

    public ExpressionConditionDTO(Long id, String input, String value, Integer operator, String operatorName, Integer valueType,
                                  Integer dataType, String dataTypeName, Integer inputType, String inputTypeName, Long linkedId, String functionAlias){
        this.id = id;
        this.input = input;
        this. value = value;
        this.operator = operator;
        this.operatorName = operatorName;
        this.valueType = valueType;
        this.dataType = dataType;
        this.dataTypeName = dataTypeName;
        this.inputType = inputType;
        this.inputTypeName = inputTypeName;
        this.linkedId = linkedId;
        this.functionAlias = functionAlias;
    }
}
