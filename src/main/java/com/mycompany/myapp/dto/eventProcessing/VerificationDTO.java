package com.mycompany.myapp.dto.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Collection;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class VerificationDTO extends TreeClone implements Moveable {

    private Long categoryId;

    private String categoryName;

    @NotNull(groups = {Move.class})
    private Integer posIndex;

    @Length(max = 255, groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private String description;

    @JsonView(Tree.class)
    private Long listType;

    @JsonView(Tree.class)
    private Long verificationType;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.VERIFICATION;

    @JsonView(Tree.class)
    private String icon = Resources.VERIFICATION;

    private Integer categoryLevel;

    @JsonView(Tree.class)
    private boolean hasChild = false;

    private String verificationTypeName;

    private String listTypeName;

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }


    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public VerificationDTO(Long id, String name, String verificationTypeName, String listTypeName){
        this.setId(id);
        this.setName(name);
        this.verificationTypeName = verificationTypeName;
        this.listTypeName = listTypeName;
    }
}
