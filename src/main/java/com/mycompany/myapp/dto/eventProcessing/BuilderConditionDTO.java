package com.mycompany.myapp.dto.eventProcessing;

public interface BuilderConditionDTO {

    Long getId();

    String getType();

    String getName();

    Long getBuilderTypeId();
}
