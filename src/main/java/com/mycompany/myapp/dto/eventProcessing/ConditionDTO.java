package com.mycompany.myapp.dto.eventProcessing;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.eventProcessing.CreateEventPolicyDTO.Validator;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class ConditionDTO extends TreeClone implements Moveable {
    private Long categoryId;

    private String categoryName;

    private String preProcess;

    /**
     * 1: là builder
     * 0: là filter
     */

    @NotNull(groups = { ConditionDTO.Create.class, ConditionDTO.Update.class, Validator.class })
    private Long filterId;

    @NotNull(groups = { ConditionDTO.Create.class })
    private String filterName;

    private Timestamp modifyDate;

    @Length(max = 255, groups = { ConditionDTO.Create.class, ConditionDTO.Update.class, Validator.class })
    @JsonView(Tree.class)
    private String description;

    @Valid
    @ConvertGroup.List({ @ConvertGroup(from = Validator.class, to = ExpressionDTO.Create.class) })
    private List<ExpressionDTO> expressions;

    private List<VerificationDTO> verifications;

    private List<BuilderDTO> builders;

    @NotNull(groups = { ConditionDTO.Move.class })
    @Min(value = 0, groups = { ConditionDTO.Move.class })
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private String icon = Resources.CONDITION;

    @JsonView(Tree.class)
    private String type = Resources.CONDITION;

    @JsonView(Tree.class)
    private Boolean hasChild = false;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {}

    public interface Update {}

    public interface Move {}

    public ConditionDTO(Long id, String name, Long filterId, String filterName, String description, Integer posIndex, Date modifyDate){
        this.setId(id);
        this.setName(name);
        this.filterId = filterId;
        this.filterName = filterName;
        this.description = description;
        this.posIndex = posIndex;
        this.modifyDate = new Timestamp(modifyDate.getTime());
    }
}
