package com.mycompany.myapp.dto.eventProcessing;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateEventPolicyDTO {

    @NotNull(groups = { Validator.class })
    private FilterDTO filter;

    @NotNull(groups = { Validator.class })
    private ConditionDTO condition;

    public interface Validator {
    }
}
