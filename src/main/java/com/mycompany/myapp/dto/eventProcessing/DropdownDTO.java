package com.mycompany.myapp.dto.eventProcessing;

public interface DropdownDTO {

    Long getId();

    String getName();
}
