package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@Data
@NoArgsConstructor
public class ExpressionConditionMapDTO extends BaseResponseDTO {

    @NotNull(groups = {Create.class,Update.class})
    @Positive
    private Long expressionId;

    @NotNull(groups = {Create.class,Update.class})
    @Positive
    private Long conditionId;

    public interface Create {
    }

    public interface Update {
    }
}
