package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.CloneDTO;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class FunctionCloneDTO extends CloneDTO {

    @NotBlank
    @Length(max = 255)
    private String functionName;

}
