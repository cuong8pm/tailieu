package com.mycompany.myapp.dto.eventProcessing;

public interface FunctionParameterDTO {

    Long getId();

    String getName();

    Integer getDataType();

    String getDataTypeName();

    String getValue();

    Integer getValueType();

    Boolean getEnable();
}
