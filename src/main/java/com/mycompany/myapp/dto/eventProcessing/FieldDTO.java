package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
public class FieldDTO extends BaseResponseDTO {

    @Length(max = 255, groups = {Create.class, Update.class})
    private String dataType;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    private String treeType = Resources.TreeType.TEMPLTE;

    private String type = Resources.FIELD;

    private String icon = Resources.FIELD;

    public interface Create {
    }

    public interface Update {
    }
}
