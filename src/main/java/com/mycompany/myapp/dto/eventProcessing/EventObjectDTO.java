package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EventObjectDTO extends BaseResponseDTO {

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    private Integer type;

    private List<EventObjectFieldDTO> templates;

    public interface Create {
    }

    public interface Update {
    }
}
