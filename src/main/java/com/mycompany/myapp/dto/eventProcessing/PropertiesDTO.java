package com.mycompany.myapp.dto.eventProcessing;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PropertiesDTO {
    private Long id;

    private String name;

    private String value;
}
