package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExpressionFunctionMapDTO extends BaseResponseDTO {

    private Long functionId;

    private Long expressionId;

    public interface Create {
    }

    public interface Update {
    }
}
