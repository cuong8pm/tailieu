package com.mycompany.myapp.dto.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Data
@NoArgsConstructor
public class BuilderTypeDTO extends TreeClone implements Moveable {

    private Long categoryId;

    @Length(max = 255, groups = {BuilderTypeDTO.Create.class, BuilderTypeDTO.Update.class})
    @JsonView(Tree.class)
    private String description;

    @NotNull(groups = {BuilderTypeDTO.Move.class })
    @Min(value = 0, groups = {BuilderTypeDTO.Move.class})
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    private String categoryName;

    @JsonView(Tree.class)
    private String icon = Resources.BUILDER_TYPE;

    @JsonView(Tree.class)
    private String type = Resources.BUILDER_TYPE;

    @JsonView(Tree.class)
    private boolean hasChild = false;

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
