 package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

 @EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class BuilderItemDTO extends BaseResponseDTO {
    @NotNull(groups = { Create.class, Update.class })
    @Positive
    private Long builderId;

    @NotNull(groups = { Create.class, Update.class })
    @Positive
    private Long builderTypeItemId;

    @NotBlank(groups = { Create.class, Update.class })
    private String value;

    private String description;

    @NotNull(groups = { Create.class, Update.class })
    @PositiveOrZero
    private Integer valueType;

    @NotNull(groups = { Create.class, Update.class })
    @PositiveOrZero
    private Integer dataType;

    @Positive
    private Long linkedId;

    private Integer domainId;

    private String builderTypeItemName;

    private String valueTypeName;

    private String dataTypeName;

    public interface Create {}

    public interface Update {}

    public BuilderItemDTO(Long id, String name, Long builderId, Long builderTypeItemId, String value, String description, Integer valueType, Integer dataType, Long linkedId,Integer domainId, String builderTypeItemName, String valueTypeName, String dataTypeName) {
        super();
        setId(id);
        setName(name);
        this.builderId = builderId;
        this.builderTypeItemId = builderTypeItemId;
        this.value = value;
        this.description = description;
        this.valueType = valueType;
        this.dataType = dataType;
        this.linkedId = linkedId;
        this.domainId = domainId;
        this.builderTypeItemName = builderTypeItemName;
        this.valueTypeName = valueTypeName;
        this.dataTypeName = dataTypeName;
    }
}
