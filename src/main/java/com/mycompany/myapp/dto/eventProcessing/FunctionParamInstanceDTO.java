package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class FunctionParamInstanceDTO extends BaseResponseDTO {

    @NotNull(groups = {Create.class,Update.class})
    private Long functionParameterId;

    private Long ownerId;

    private Integer ownerType;

    private String value;

    private Integer valueType;

    public interface Create {
    }

    public interface Update {
    }

}
