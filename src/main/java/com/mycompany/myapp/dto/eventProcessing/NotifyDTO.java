package com.mycompany.myapp.dto.eventProcessing;

import com.mycompany.myapp.dto.BaseResponseDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
public class NotifyDTO extends BaseResponseDTO {
    @NotBlank(groups = { Create.class, Update.class })
    private String content;

    @NotNull(groups = { Create.class, Update.class })
    private Long languageId;

    private String languageName;

    @NotNull(groups = { Create.class, Update.class })
    private Long builderId;

    public interface Create {}

    public interface Update {}
}
