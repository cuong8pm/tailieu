package com.mycompany.myapp.dto.offer.offer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfferVersionCharspecValueUseDTO {

    private Long id;

    private Long charspecId;

    @NotBlank
    private String value;

    private Integer domainId;

    @NotNull
    private Long charspecValueUseId;
}
