package com.mycompany.myapp.dto.offer.template;

import com.mycompany.myapp.dto.offer.common.VersionBundleCommonDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferTemplateVersionBundleDTO extends VersionBundleCommonDTO {

    private Long versionId;
}
