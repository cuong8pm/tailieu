package com.mycompany.myapp.dto.offer.offer;


import com.mycompany.myapp.dto.offer.common.VersionActionCommonDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferVersionActionDTO extends VersionActionCommonDTO {


    private Long parentId;


}
