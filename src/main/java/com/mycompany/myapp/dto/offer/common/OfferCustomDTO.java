package com.mycompany.myapp.dto.offer.common;

import java.util.List;

import com.mycompany.myapp.domain.ReferTable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferCustomDTO {

    private Long id;

    private String name;

    private List<ReferTable> relationshipTypes;

    public OfferCustomDTO(Long id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

}
