package com.mycompany.myapp.dto.offer.template;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.common.OfferCommonDTO;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OfferTemplateDTO extends OfferCommonDTO {
    @JsonView(Tree.class)
    private String type = Resources.OFFER_TEMPLATE;
}
