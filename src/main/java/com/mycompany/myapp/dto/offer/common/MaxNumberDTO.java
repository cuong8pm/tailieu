package com.mycompany.myapp.dto.offer.common;

import lombok.Data;

@Data
public class MaxNumberDTO {
    private Long maxNumber;
}
