package com.mycompany.myapp.dto.offer.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CharValueUseMapDTO {

    private Long charspecId;

    @NotBlank
    @Length(max = 255)
    private String value;

}
