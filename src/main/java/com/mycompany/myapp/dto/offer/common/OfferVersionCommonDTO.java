package com.mycompany.myapp.dto.offer.common;

import java.sql.Timestamp;
import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.State;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = {"description"}, groups = { OfferVersionCommonDTO.Create.class, OfferVersionCommonDTO.Update.class })
public class OfferVersionCommonDTO extends TreeClone implements Moveable {

    @NotNull(groups = { Create.class, Update.class })
    private Long number;

    @NotNull(groups = { Create.class, Update.class, Validate.class})
    private State state;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Timestamp effDate;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Timestamp expDate;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Integer specialMethod;

    @Length(max = 255, groups = { Create.class, Update.class })
    private String description;

    private Integer domainId;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    private String parentName;
    
    private String stateName;

    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Validate {
    }


    @Override
    public Long getParentId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setParentId(Long parentId) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getType() {
        // TODO Auto-generated method stub
        return null;
    }
}
