package com.mycompany.myapp.dto.offer.common;

import com.mycompany.myapp.dto.TreeClone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class OfferRelationshipCommonDTO extends TreeClone {

    private Integer relationshipType;
    
    private Integer domainId;

    private Integer posIndex;

    public interface Create {
    }

    public interface Update {
    }

    @Override
    public Long getParentId() {
        return null;
    }

    @Override
    public void setParentId(Long parentId) {
    }

    @Override
    public String getTreeType() {
        return null;
    }

    @Override
    public void setTreeType(String treeType) {
    }

    @Override
    public Integer getCategoryLevel() {
        return null;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {
        
    }

    @Override
    public String getType() {
        return null;
    }
}
