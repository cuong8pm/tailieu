package com.mycompany.myapp.dto.offer.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.dto.ReferTableDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferRelationshipCustomDTO {

    @JsonProperty("id")
    @NotNull(groups = {OfferCommonDTO.Validate.class})
    private Long offerId;

    @NotNull(groups = {OfferCommonDTO.Validate.class})
    private List<ReferTableDTO> relationshipTypes;
}
