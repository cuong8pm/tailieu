package com.mycompany.myapp.dto.offer.template;

import com.mycompany.myapp.dto.offer.common.VersionCharspecUseCommonDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class OfferTemplateVersionCharspecUseDTO extends VersionCharspecUseCommonDTO {

    private Long offerTemplateVersionId;
}
