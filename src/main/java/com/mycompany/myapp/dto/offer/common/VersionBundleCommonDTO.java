package com.mycompany.myapp.dto.offer.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VersionBundleCommonDTO  {

    private Long id;

    private Long childOfferId;

    private Integer domainId;

}
