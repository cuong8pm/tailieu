package com.mycompany.myapp.dto.offer.common;

public interface CharOfVersionDTO {

    Long getId();

    Long getCharspecId();

    String getName();

    String getValue();

    String getDescription();

    Boolean getExtensible();

    Long getCharspecValueUseId();
}
