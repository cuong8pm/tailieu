package com.mycompany.myapp.dto.offer.offer;

public interface OfferOfCharacteristicDTO {

    Long getId();
    
    String getName();
    
    String getDescription();
    
    String  getValue();
    
    Boolean  getExtensible();
}
