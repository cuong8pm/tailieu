package com.mycompany.myapp.dto.offer.common;

public interface RelationshipMappingOfferCommonDTO {

    Integer getRelationshipType();

    Long getOfferId();
}
