package com.mycompany.myapp.dto.offer.common;

public interface OfferOfVersionRedirectionDTO {
    Long getId();
    
    String getUrl();
    
    Integer getRedirectionType();
    
    String getRedirectionTypeName();
}
