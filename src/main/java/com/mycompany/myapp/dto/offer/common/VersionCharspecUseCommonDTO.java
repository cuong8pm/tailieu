package com.mycompany.myapp.dto.offer.common;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VersionCharspecUseCommonDTO {

    private Long id;

    @NotNull
    private Long charspecId;

    private Integer domainId;
}
