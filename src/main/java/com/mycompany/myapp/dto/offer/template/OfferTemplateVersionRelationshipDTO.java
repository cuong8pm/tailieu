package com.mycompany.myapp.dto.offer.template;

import com.mycompany.myapp.dto.offer.common.OfferRelationshipCommonDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class OfferTemplateVersionRelationshipDTO extends OfferRelationshipCommonDTO {

    private Long offerTemplateVersionId;

    private Long offerId;

    private Boolean isParent;
}
