package com.mycompany.myapp.dto.offer.offer;

public interface OfferCharacteristicDTO {

    Long getId();
    
    String getName();
    
    String getDescription();
    
    Boolean getExtensible();
    
    String getValue();
    
    Long getIdVersionCharValueUse();
    
    Long getIdVersionCharcUse();
    
}
