package com.mycompany.myapp.dto.offer.offer;

public interface OfferActionDTO {

    Long getActionId();

    String getName();

    String getDescription();

    Boolean getAllowMobileMoney();

    Long getId();

    Integer getPosIndex();
}
