package com.mycompany.myapp.dto.offer.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = { "name", "description" }, groups = { OfferCommonDTO.Create.class, OfferCommonDTO.Update.class })
public class OfferCommonDTO extends TreeClone implements Moveable {

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    @JsonView(Tree.class)
    private Long categoryId;

    private String categoryName;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    @Positive(groups = { Create.class, Update.class, Validate.class })
    private Long priority;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Timestamp effDate;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Timestamp expDate;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Integer offerType;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Long billingCycleTypeId;
    @JsonView(Tree.class)
    @Size(max = 255, groups = { Create.class, Update.class, Validate.class })
    private String description;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Integer paymentType;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Boolean expriable;

    private Integer domainId;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Boolean suspendable;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Boolean isBundle;

    @NotNull(groups = { Create.class, Update.class, Validate.class })
    private Boolean isMultiCycle;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    private Timestamp updateDate;

    private Long parentId;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public Integer getPosIndex() {
        return super.getId() == null ? null : super.getId().intValue();
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Validate {
    }
}
