package com.mycompany.myapp.dto.offer.offer;

import com.mycompany.myapp.dto.offer.common.VersionBundleCommonDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferVersionBundleDTO extends VersionBundleCommonDTO {

    private Long versionId;  
}
