package com.mycompany.myapp.dto.offer.common;

import com.mycompany.myapp.domain.constant.State;
import com.mycompany.myapp.dto.CloneDTO;
import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class OfferCloneDTO extends CloneDTO {
    @Positive
    private Long externalId;

    private String description;

    private Boolean isCloneRelationship;
    
    private State state;
}
