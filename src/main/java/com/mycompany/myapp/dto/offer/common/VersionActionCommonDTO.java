package com.mycompany.myapp.dto.offer.common;


import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.action.ActionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class VersionActionCommonDTO extends TreeClone implements Moveable {

//    private Long id;
    @NotNull(groups = {VersionActionCommonDTO.Create.class})
    private Long actionId;

    private Integer posIndex;

    private Boolean allowMobileMoney;

    private Integer domainId;

    private Long childId;


    @Override
    public Long getParentId() {
        return null;
    }

    @Override
    public void setParentId(Long parentId) {

    }

    @Override
    public String getTreeType() {
        return null;
    }

    @Override
    public void setTreeType(String treeType) {

    }

    @Override
    public Integer getCategoryLevel() {
        return null;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {

    }

    @Override
    public String getType() {
        return null;
    }

    public interface Create {
    }

    public interface Update {
    }
}
