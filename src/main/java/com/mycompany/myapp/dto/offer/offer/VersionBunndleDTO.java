package com.mycompany.myapp.dto.offer.offer;

public interface VersionBunndleDTO {

    public Long getId();
    
    public String getName();
    
    public String getDescription();
      
    public Long getOfferBundleId();
}
