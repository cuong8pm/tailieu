package com.mycompany.myapp.dto.offer.template;

import javax.validation.constraints.NotNull;

import com.mycompany.myapp.dto.offer.common.VersionRedirectionCommonDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class OfferTemplateVersionRedirectionDTO extends VersionRedirectionCommonDTO {

    @NotNull
    private Long versionId;

    private String name;
}
