package com.mycompany.myapp.dto.offer.template;

import com.mycompany.myapp.dto.offer.common.VersionActionCommonDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OfferTemplateVersionActionDTO extends VersionActionCommonDTO {

    private Long parentId;
}
