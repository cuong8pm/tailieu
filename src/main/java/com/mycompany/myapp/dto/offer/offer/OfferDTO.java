package com.mycompany.myapp.dto.offer.offer;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.common.OfferCommonDTO;
import com.mycompany.myapp.dto.offer.common.OfferRelationshipCustomDTO;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OfferDTO extends OfferCommonDTO {

    @NotNull(groups = {OfferCommonDTO.Create.class, OfferCommonDTO.Update.class, OfferCommonDTO.Validate.class})
    private Long externalId;

    @JsonView(Tree.class)
    private String type = Resources.OFFER;

    private List<OfferRelationshipCustomDTO> listChilds;

    private List<OfferRelationshipCustomDTO> listParents;

    private OfferVersionDTO version;
}
