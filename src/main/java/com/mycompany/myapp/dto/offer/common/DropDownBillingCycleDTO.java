package com.mycompany.myapp.dto.offer.common;

public interface DropDownBillingCycleDTO {
    Long getId();
    String getName();
}
