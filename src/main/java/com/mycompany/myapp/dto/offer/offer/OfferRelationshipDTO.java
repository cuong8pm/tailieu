package com.mycompany.myapp.dto.offer.offer;

import com.mycompany.myapp.dto.offer.common.OfferRelationshipCommonDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class OfferRelationshipDTO extends OfferRelationshipCommonDTO {

    private Long parentOfferId;

    private Long ChildOfferId;
}
