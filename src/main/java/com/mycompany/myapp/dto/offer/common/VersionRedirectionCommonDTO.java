package com.mycompany.myapp.dto.offer.common;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.mycompany.myapp.dto.normailizer.validator.TrimString;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TrimString(fieldNames = { "url" })
public class VersionRedirectionCommonDTO {

    private Long id;

    @NotBlank
    private String url;

    @NotNull
    private Integer redirectionType;

    private Integer domainId;

}
