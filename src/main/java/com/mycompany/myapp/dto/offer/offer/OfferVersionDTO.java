package com.mycompany.myapp.dto.offer.offer;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.offer.common.CharValueUseMapDTO;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OfferVersionDTO extends OfferVersionCommonDTO{

    @NotNull(groups = {Create.class, Update.class})
    private Long offerId;

    private Long offerTemplateVersionId;

    @NotNull(groups = {Create.class, Update.class})
    private Long parentId;

    private List<OfferVersionRedirectionDTO> redirections;

    private List<CharValueUseMapDTO> characteristics;

    private List<CloneDTO> actions;

    private List<Long> bunndles;

    @JsonView(Tree.class)
    private String type = Resources.OFFER_VERSION;

    private String offerTemplate;

    private Long externalId;

    public interface Create {
    }

    public interface Update {
    }

    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;

    }

    @Override
    public String getType() {
        return this.type;
    }

    private List<Long> actionIds;
}
