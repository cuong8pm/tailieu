package com.mycompany.myapp.dto.offer.template;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO.Create;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO.Update;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OfferTemplateVersionDTO extends OfferVersionCommonDTO{

    @NotNull(groups = {Create.class, Update.class})
    private Long offerTemplateId;

    @NotNull(groups = {Create.class, Update.class})
    private Long parentId;

    @JsonView(Tree.class)
    private String type = Resources.OFFER_TEMPLATE_VERSION;

    public interface Create {

    }

    public interface Update {

    }

    public Long getOfferTemplateId() {
        return offerTemplateId;
    }

    public void setOfferTemplateId(Long offerTemplateId) {
        this.offerTemplateId = offerTemplateId;
    }

    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;

    }

    @Override
    public String getType() {
        return this.type;
    }
}
