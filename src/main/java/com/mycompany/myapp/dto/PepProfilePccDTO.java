package com.mycompany.myapp.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PepProfilePccDTO {

    private Long id;

    @NotNull(groups = {PepProfilePccDTO.Create.class, PepProfilePccDTO.Update.class})
    private Long pccRuleId;

    @NotNull(groups = {PepProfilePccDTO.Create.class, PepProfilePccDTO.Update.class})
    private Long pepProfileId;

    private Integer domainId;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
