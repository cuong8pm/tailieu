package com.mycompany.myapp.dto.ratetable;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = {"name", "description"})
public class RateTableDTO extends TreeClone implements Moveable {

    @NotNull(groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private Long categoryId;

    private String categoryName;

    private Long defaultFormulaId;

    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Create.class, to = FormulaDTO.Create.class),
        @ConvertGroup(from = Update.class, to = FormulaDTO.Update.class)
    })
    private FormulaDTO defaultFormula;

    @NotNull(groups = {Create.class, Update.class})
    private Long unitTypeId;

    private String defaultCombinedValue;

    @JsonView(Tree.class)
    private String description;

    private Integer domainId;

    private String defaultFormulaString;

    private String unitType;

    private Integer baseRate;

    private ActionType actionTypeDefaultFormula;

    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Create.class, to = RateTableRowDTO.Create.class),
        @ConvertGroup(from = Update.class, to = RateTableRowDTO.Update.class)
    })
    private List<RateTableRowDTO> rows;

    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Create.class, to = RateTableColumnDTO.Create.class),
        @ConvertGroup(from = Update.class, to = RateTableColumnDTO.Update.class)
    })
    private List<RateTableColumnDTO> columns;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.RATE_TABLE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private Integer componentType;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;

    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String RATE_TABLE_NAME = "rate_table_name";
    }

}
