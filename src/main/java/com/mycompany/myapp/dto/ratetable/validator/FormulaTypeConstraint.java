package com.mycompany.myapp.dto.ratetable.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = FormulaTypeValidator.class)
@Target({ TYPE, ANNOTATION_TYPE })
@Repeatable(FormulaTypeConstraint.List.class)
@Retention(RUNTIME)
public @interface FormulaTypeConstraint {

    String fieldName();
    String dependFieldName();
    String tempFieldName();

    String message() default "formula_invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        FormulaTypeConstraint[] value();
    }
}
