package com.mycompany.myapp.dto.ratetable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = "displayName")
public class RateTableColumnDTO {

    private Long id;

    @NotNull(groups = {Create.class, Update.class})
    private Long normalizerId;

    private Long rateTableId;

    @NotNull(groups = {Create.class, Update.class})
    @Positive
    private Integer columnIndex;

    @NotNull(groups = {Create.class, Update.class})
    @Length(max = 255)
    private String displayName;

    private Integer domainId;

    private Long valueId;

    private String valueName;

    private String normalizerName;

    private ActionType actionType;

    private String colorText;

    private String colorBG;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
