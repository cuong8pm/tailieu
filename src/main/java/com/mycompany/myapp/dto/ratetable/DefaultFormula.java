package com.mycompany.myapp.dto.ratetable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DefaultFormula {
    private Long id;
    private String defaultFormula;
}
