package com.mycompany.myapp.dto.ratetable.validator;

import com.mycompany.myapp.domain.constant.FormulaType;
import com.mycompany.myapp.service.exception.ErrorMessage;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FormulaTypeValidator implements ConstraintValidator<FormulaTypeConstraint, Object> {

    private String fieldName;
    private String dependFieldName;
    private String tempFieldName;
    private static final String A = "a";
    private static final String B = "b";
    private static final String PER = "per";
    private static final String INPUT_A = "inputA";
    private static final String INPUT_B = "inputB";
    private static final String IS_PERCENTAGE = "percentage";
    private static final String CONVERT_TYPE = "converterType";
    private static final String ERROR_CODE = "formulaErrorCode";
    private static final String STATIC_ITEM = "statisticItems";
    private static final String IS_SEND_RAR = "sendRar";
    private static final String IS_MONITOR = "monitor";
    private static final String RESERVATION_TIME = "reservationTime";
    private static final String TEMPLATE_BITS_STRING = "templateBitString";
    private static final char UNCHECK = '0';
    private static final char CHECK = '1';

    public void initialize(FormulaTypeConstraint constraint) {
        this.fieldName = constraint.fieldName();
        this.dependFieldName = constraint.dependFieldName();
        this.tempFieldName = constraint.tempFieldName();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (value == null) {
            return true;
        }
        BeanWrapperImpl obj = new BeanWrapperImpl(value);
        Object fieldValue = obj.getPropertyValue(this.dependFieldName);
        Object tempValue = obj.getPropertyValue(this.tempFieldName);
        if (tempValue == null) {
            return false;
        }
        String tempValueString = tempValue.toString();
        Object dependFieldValue = obj.getPropertyValue(fieldName);
        boolean isValid = true;
        if (tempValueString.length() != 6 || (tempValueString.charAt(2) != CHECK && tempValueString.charAt(2) != UNCHECK)) {
            return false;
        }
        if (tempValueString.charAt(2) == CHECK) {
            isValid = validateType(fieldName, dependFieldValue, context, tempValueString);
        }
        if (tempValueString.charAt(2) == UNCHECK) {
            context.disableDefaultConstraintViolation();
            Long typeLong = (Long) fieldValue;
            int typeInt = Math.toIntExact(typeLong);
            switch (FormulaType.of(typeInt)) {
                case SORT_PC:
                case BLOCK_FILTER:
                case EVENT_ANALYSIS:
                case ACTION_PRIORITY:
                    isValid = validateTypeSort(fieldName, dependFieldValue, context, tempValueString, typeInt);
                    break;
                case DYNAMIC_RESERVE:
                    isValid = validateTypeDynamic(fieldName, dependFieldValue, context, tempValueString);
                    break;
                case DENY:
                case EXIT:
                case SKIP:
                    isValid = validateTypeSkip(fieldName, dependFieldValue, context);
                    break;
                case NORMAL:
                    isValid = validateType(fieldName, dependFieldValue, context, tempValueString);
                    break;
            }
        }
        return isValid;
    }

    private void customMessageForValidation(ConstraintValidatorContext constraintContext, String message) {
        // build new violation message and add it
        constraintContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }

    private boolean validateTypeSort(String fieldName, Object value, ConstraintValidatorContext context,
                                     String tempValueString, int typeInt) {
        boolean isValid = true;
        switch (fieldName) {
            case B:
                if (FormulaType.of(typeInt).equals(FormulaType.ACTION_PRIORITY) && ((Long) value) < -1L) {
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_GREATER_THAN + "-1");
                    isValid = false;
                    break;
                }
            case TEMPLATE_BITS_STRING:
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
            case INPUT_B:
                if (tempValueString.charAt(0) == UNCHECK) {
                    break;
                }
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
            case A:
            case PER:
            case INPUT_A:
            case IS_PERCENTAGE:
            case ERROR_CODE:
            case STATIC_ITEM:
            case IS_SEND_RAR:
            case IS_MONITOR:
            case RESERVATION_TIME:
                if (value != null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_BE_NULL);
                }
                break;
        }
        return isValid;
    }

    private boolean validateTypeDynamic(String fieldName, Object value, ConstraintValidatorContext context, String tempValueString) {
        boolean isValid = true;
        switch (fieldName) {
            case B:
            case A:
            case PER:
            case TEMPLATE_BITS_STRING:
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;

            case INPUT_A:
                if (tempValueString.charAt(1) == UNCHECK) {
                    break;
                }
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
            case INPUT_B:
                if (tempValueString.charAt(0) == UNCHECK) {
                    break;
                }
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
            case IS_PERCENTAGE:
            case ERROR_CODE:
            case STATIC_ITEM:
            case IS_SEND_RAR:
            case IS_MONITOR:
            case RESERVATION_TIME:
                if (value != null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_BE_NULL);
                }
                break;
        }
        return isValid;
    }

    private boolean validateTypeSkip(String fieldName, Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        switch (fieldName) {
            case B:
            case A:
            case PER:
            case INPUT_A:
            case INPUT_B:
            case IS_PERCENTAGE:
                if (value != null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_BE_NULL);
                }
                break;
            case TEMPLATE_BITS_STRING:
            case IS_SEND_RAR:
            case IS_MONITOR:
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
        }
        return isValid;
    }

    private boolean validateType(String fieldName, Object value, ConstraintValidatorContext context, String tempValueString) {
        boolean isValid = true;
        switch (fieldName) {
            case B:
            case A:
            case PER:
            case IS_PERCENTAGE:
            case IS_SEND_RAR:
            case IS_MONITOR:
            case TEMPLATE_BITS_STRING:
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
            case INPUT_A:
                if (tempValueString.charAt(1) == UNCHECK) {
                    break;
                }
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
            case INPUT_B:
                if (tempValueString.charAt(0) == UNCHECK) {
                    break;
                }
                if (value == null) {
                    isValid = false;
                    customMessageForValidation(context, ErrorMessage.Formula.MUST_NOT_BE_NULL);
                }
                break;
        }
        return isValid;
    }
}
