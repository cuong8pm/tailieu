package com.mycompany.myapp.dto.ratetable;

public interface DropDownCustomDTO {
    Long getId();
    String getName();
    String getAbbreviation();
    Integer getBaseRate();
}
