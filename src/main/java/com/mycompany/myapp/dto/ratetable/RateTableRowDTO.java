package com.mycompany.myapp.dto.ratetable;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.groups.ConvertGroup;

import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.dto.normailizer.ActionType;

import com.mycompany.myapp.service.exception.ErrorMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RateTableRowDTO {

    private Long id;

    private Long formulaId;

//    @NotNull(groups = {Create.class, Update.class})
    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Create.class, to = FormulaDTO.Create.class),
            // @ConvertGroup(from = Update.class, to = FormulaDTO.Update.class)
    })
    private FormulaDTO formula;

    private Long rateTableId;

    @NotNull(groups = {Create.class, Update.class})
    @NotBlank(groups = {Create.class, Update.class})
    @Pattern(regexp = "^\\d(/\\d)*$", groups = {Create.class, Update.class} , message = ErrorMessage.RateTable.INVALID_COMBINED_VALUE)
    private String combinedValue;

    private Integer domainId;

    private String formulaString;

    private boolean isDefault;

    private List<RateTableColumnDTO> columns;

    private ActionType actionType;

    private ActionType actionTypeFormula;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
