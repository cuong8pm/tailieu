package com.mycompany.myapp.dto.event;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.actiontype.EventActionTypeMapDTO;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@TrimString(fieldNames = { "name", "description" })
public class EventDTO extends TreeClone implements Moveable {

    @PositiveOrZero
    @NotNull(groups = { EventDTO.Create.class, EventDTO.Update.class })
    private Integer eventType;

    @Length(max = 255)
    private String description;

    @NotNull(groups = { EventDTO.Create.class, EventDTO.Update.class })
    private Boolean isRequireReserveInfo;

    @Positive
    @NotNull(groups = { EventDTO.Create.class, EventDTO.Update.class })
    private Long categoryId;

    private String categoryName;

    @NotNull(groups = { EventDTO.Create.class, EventDTO.Update.class })
    private Boolean isRequireRatingConfig;

    @JsonProperty("ratingFilterId")
    private Long eventAnalysisFilter;

    private String ratingFilterName;

    private Integer domainId;

    @Valid
    @NotEmpty(groups = { EventDTO.Create.class })
    private List<EventActionTypeMapDTO> actionTypeMapDTOs;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.EVENT;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Long getParentId() {
        return categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @JsonView(Tree.class)
    private Boolean hasChild = true;
}
