package com.mycompany.myapp.dto.formula;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.mycompany.myapp.dto.normailizer.validator.TrimString;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = "name")
public class FormulaErrorCodeDTO {

    private Long id;

    @NotNull
    @Positive
    private Long errorCode;

    @NotNull
    private Integer type;

    @NotBlank
    @Length(max = 255)
    private String name;
}
