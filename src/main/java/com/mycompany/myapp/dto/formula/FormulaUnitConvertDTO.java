package com.mycompany.myapp.dto.formula;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class FormulaUnitConvertDTO {

    private Long id;

    private String name;

    private Long description;
}
