package com.mycompany.myapp.dto.formula;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mycompany.myapp.dto.StatisticItemDTO;
import com.mycompany.myapp.dto.ratetable.validator.FormulaTypeConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@FormulaTypeConstraint(fieldName = "a", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "b", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "per", dependFieldName = "formulaType",tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "inputA", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "inputB", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "templateBitString", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "isPercentage", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "statisticItems", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "isSendRar", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "isMonitor", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
@FormulaTypeConstraint(fieldName = "reservationTime", dependFieldName = "formulaType", tempFieldName = "templateBitString", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
public class FormulaDTO {

    private Long id;

    @NotNull(groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    private Long formulaType;

    private Long a;

    private Long b;

    @Positive(groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    private Long per;

    @JsonIgnore
    private Long triggerOcsId;

    private boolean isPercentage;

    @JsonIgnore
    private Long templateBits;

    @NotNull(groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    @Length(max = 6, groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    @Pattern(regexp = "^[0,1]{6}$", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    private String templateBitString;

    @Min(1)
    private Long formulaErrorCode;

    private Long converterType;

    private boolean isSendRar;

    private boolean isMonitor;

    @JsonIgnore
    private String triggerIds;

    @Pattern(regexp = "^\\d*(;\\d+)*$", groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    private String statisticItemString;

    private Integer domainId;

    @Positive(groups = {FormulaDTO.Create.class, FormulaDTO.Update.class})
    private Long reservationTime;

    private String inputA;

    private String inputB;

    private List<StatisticItemDTO> statisticItems;

    private String charFormulaType;

    private String charA;

    private String charB;

    private String charPer;

    private Long charFormulaTypeId;

    private Long charAId;

    private Long  charBId;

    private Long charPerId;

    public boolean getIsPercentage() {
        return isPercentage;
    }

    public boolean isPercentage() {
        return isPercentage;
    }

    public void setIsPercentage(boolean percentage) {
        isPercentage = percentage;
    }

    public void setPercentage(boolean percentage) {
        isPercentage = percentage;
    }

    public boolean getIsSendRar() {
        return isSendRar;
    }

    public boolean isSendRar() {
        return isSendRar;
    }

    public void setIsSendRar(boolean sendRar) {
        isSendRar = sendRar;
    }

    public void setSendRar(boolean sendRar) {
        isSendRar = sendRar;
    }

    public boolean getIsMonitor() {
        return isMonitor;
    }

    public boolean isMonitor() {
        return isMonitor;
    }

    public void setIsMonitor(boolean monitor) {
        isMonitor = monitor;
    }

    public void setMonitor(boolean monitor) {
        isMonitor = monitor;
    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String FORMULA_TYPE = "formula_type";
        public static final String INPUT_B = "input_B";
        public static final String INPUT_A = "input_A";
        public static final String A = "A";
        public static final String B = "B";
        public static final String PER = "per";
    }

    public interface Create {
    }

    public interface Update {
    }
}
