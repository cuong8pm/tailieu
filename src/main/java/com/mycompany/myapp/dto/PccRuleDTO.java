package com.mycompany.myapp.dto;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PccRuleDTO extends Tree {

    @NotNull(groups = { Create.class, Update.class })
    @PositiveOrZero(groups = { Create.class, Update.class })
    private Integer priority;

    @NotNull(groups = { Create.class, Update.class })
    @PositiveOrZero(groups = { Create.class, Update.class })
    private Integer precedence;

    @NotNull(groups = { Create.class, Update.class })
    private Timestamp effectDate;

    private Timestamp expireDate;

    @NotNull(groups = { Create.class, Update.class })
    @JsonView(Tree.class)
    private Long categoryId;

    private String categoryName;

    private Integer categoryLevel;

    @Length(max = 255, groups = { Create.class, Update.class })
    private String description;

    @NotNull(groups = { Create.class, Update.class })
    @PositiveOrZero(groups = { Create.class, Update.class })
    private Integer sendType;

    private String typeName;

    private Integer domainId;

    @NotNull(groups = { Move.class, Update.class })
    @Min(value = 0, groups = { Move.class, Update.class })
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;
    
    @JsonView(Tree.class)
    private String icon = Resources.PCC_RULE;

    @JsonView(Tree.class)
    private String type = Resources.PCC_RULE;

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = categoryId;

    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

}
