package com.mycompany.myapp.dto.balance;

import lombok.Data;

@Data
public class BalanceCommonDTO {
    private Long id;
    private String name;
    private String description;
}
