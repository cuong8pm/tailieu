package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Collection;
import java.util.Collections;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class TriggerOcsDTO extends Tree {
    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long id;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long categoryId;

    @NotNull(groups = {Create.class, Update.class})
    private String name;

    @NotNull(groups = {Create.class, Update.class})
    private Long triggerType;

    private String description;

    private Integer status;

    private Integer module;

    @NotNull(groups = {Create.class, Update.class})
    private Integer domainId;

    private String extProperty;

    private Integer smsTemplateCode;

    @NotNull(groups = {Create.class, Update.class})
    private Integer posIndex;
    private String treeType = Resources.TreeType.TEMPLTE;
    
    private Integer categoryLevel;

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getTreeType() {
        return this.treeType;
    }

    @Override
    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    @Override
    public Integer getCategoryLevel() {
        return this.categoryLevel;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    @Override
    public Integer getPosIndex() {
        return this.posIndex;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

    public interface Create {
    }

    public interface Update {
    }

    @Override
    public String getType() {
        return "trigger";
    }
}
