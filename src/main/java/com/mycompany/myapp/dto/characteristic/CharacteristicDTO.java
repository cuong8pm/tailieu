package com.mycompany.myapp.dto.characteristic;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Collection;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CharacteristicDTO extends TreeClone {
    private Long characteristicId;

    @NotBlank
    @Size(max = 255)
    private String characteristicName;

    @JsonView(Tree.class)
    @Size(max = 255)
    private String description;

    private Boolean unique;

    private Integer minCardinality;

    private Integer maxCardinality;

    private Boolean configurable;

    @JsonView(Tree.class)
    private Boolean extensible;

    @Size(max = 255)
    private String formula;

    @Size(max = 255)
    private String regex;

    @NotNull
    private Timestamp startDate;

    @NotNull
    private Timestamp endDate;

    @NotNull
    @Min(value = 1)
    private Long categoryId;

    private String categoryName;

    @NotNull
    private Integer characteristicTypeId;

    @JsonView(Tree.class )
    private String characteristicTypeName;

    @NotNull
    private Long valueTypeId;

    private String defaultValue;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.CHARACTERISTIC;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public Collection<Tree> getTemplates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
        // TODO Auto-generated method stub

    }

}
