package com.mycompany.myapp.dto.characteristic;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ValuesDTO {

    private Boolean isDelete = false;

    private Long valueId;

    @NotNull
    @Size(max = 255)
    @NotBlank
    private String valueName;

    private Boolean isDefault;

    @NotNull
    @Size(max = 255)
    @NotBlank
    private String value;

    @Size(max = 255)
    private String unitOfMeasure;

    @NotNull
    @Size(max = 255)
    @NotBlank
    private String unitType;

    @NotNull
    @Size(max = 255)
    @NotBlank
    private String valueFrom;

    @NotNull
    @Size(max = 255)
    @NotBlank
    private String valueTo;

    @Size(max = 255)
    private String rangeInterval;

    private Integer rangeStep;

    @Size(max = 255)
    private String description;

    private Long characteristicId;

    private Long valueTypeId;

    private String actionType;

    private Long tempId;
    
    private Long id;
    
    private String name;
}
