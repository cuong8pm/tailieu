package com.mycompany.myapp.dto.characteristic;

import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MappingValueDTO {

    private Boolean isDelete = false;

    private Long id;

    private Boolean isDefault;

    private Long parentCharacteristicId;

    private Long childCharacteristicId;

    @Size(max = 255)
    private String parentCharacteristicName;

    @Size(max = 255)
    private String childCharacteristicName;

    private Long relationshipId;
    @NotNull
    private Long relationshipTypeId;

    private String valueOfParent;

    private String valueOfChild;

    private Long valueOfParentId;

    private Long valueOfChildId;

    private String actionType;
}
