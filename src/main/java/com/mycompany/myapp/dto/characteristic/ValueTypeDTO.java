package com.mycompany.myapp.dto.characteristic;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.viettel.ocs.slice.domain.ValueType} entity.
 */
public class ValueTypeDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    private String name;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ValueTypeDTO)) {
            return false;
        }

        return id != null && id.equals(((ValueTypeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ValueTypeDTO{" + "id=" + getId() + ", name='" + getName() + "'" + ", description='" + getDescription()
                + "'" + "}";
    }
}
