package com.mycompany.myapp.dto.characteristic;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RelationshipDTO {

    // relationship id
    private Long id;

    private Long parentCharacteristicId;

    private Long childCharacteristicId;

    @NotNull
    private Long relationshipTypeId;
    
    private String relationshipTypeName;

    private String actionType;

    @Size(max = 255)
    private String childCharacteristicName;

    @Size(max = 255)
    private String description;

    private List<MappingValueDTO> mappingValue;
    private String index;
    private int level;
}
