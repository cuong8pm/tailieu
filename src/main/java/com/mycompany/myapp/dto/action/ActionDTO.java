package com.mycompany.myapp.dto.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@TrimString(fieldNames = {"name", "description"}, groups = {ActionDTO.Create.class, ActionDTO.Update.class})
public class ActionDTO extends TreeClone implements Moveable {

    private Long actionTypeId;

    @NotNull(groups = {ActionDTO.Create.class, ActionDTO.Update.class})
    private Long categoryId;

    @Size(max = 255, groups = {ActionDTO.Create.class, ActionDTO.Update.class})
    private String description;

    private Integer domainId;

    private Long reserveInfoId;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    private Timestamp effDate;

//    @JsonFormat(pattern="dd/MM/yyyy'T'HH:mm:ss")
    private Timestamp expDate;

    @JsonProperty("generated")
    private Boolean state;

    private Integer posIndex;

//    private Boolean generated;

    private Long priorityFilterId;

    private Long dynamicReserveFilterId;

    private Long sortingFilterId;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.ACTION;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    private String categoryName;

    private String reserveInfoName;

    private String dynamicReserveFilterName;

    private String sortingFilterName;

    private String priorityFilterName;

    @NotNull(groups = {ActionDTO.Create.class, ActionDTO.Update.class})
    private String actionTypeName;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    private List<ActionPriceComponentMapDTO> actionPriceComponentMaps = new ArrayList<>();
}
