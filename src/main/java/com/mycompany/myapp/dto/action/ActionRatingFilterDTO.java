package com.mycompany.myapp.dto.action;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ActionRatingFilterDTO extends TreeClone {

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.ACTION;

    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

    @Override
    public Long getParentId() {
        return null;
    }

    @Override
    public void setParentId(Long parentId) {

    }


    @Override
    public Integer getCategoryLevel() {
        return null;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {

    }
}
