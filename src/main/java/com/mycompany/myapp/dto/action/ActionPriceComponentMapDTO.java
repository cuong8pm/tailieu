package com.mycompany.myapp.dto.action;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.dto.normailizer.ActionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActionPriceComponentMapDTO {

    @JsonProperty("actionPriceComponentMapId")
    private Long id;

    @NotNull
    private Long actionId;

    @NotNull
    @JsonProperty("id")
    private Long priceComponentId;

    private Integer posIndex;

    private Integer domainId;

    private Long parentId;

    private Long childId;

    private ActionType actionType;


    private String name;

    private String description;
}
