package com.mycompany.myapp.dto;

import java.util.Collection;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
@JsonInclude(Include.NON_NULL)
@Data
@EqualsAndHashCode(callSuper = true)
public class CategoryDTO extends Tree implements Moveable {

    @JsonView(Tree.class)
    private CategoryType categoryType;

//    @NotBlank(groups = { Create.class, Update.class })
//    @JsonView(Tree.class)
//    private String categoryName;

    @NotNull(groups = {Create.class})
    @Positive(groups = {Create.class})
    private Long parentCategoryId;

    private String parentCategoryName;

//    @Length(max = 255, groups = { Create.class, Update.class })
    private String description;
    @JsonView(Tree.class)
    private Collection<Tree> templates;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.CATEGORY;

    @NotNull(groups = { Move.class })
    @Min(value = 0, groups = {Move.class})
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private boolean hasChild = false;

    @JsonView(Tree.class)
    private Integer parentCategoryLevel;

    @JsonView(Tree.class)
    private String type;

    @JsonView(Tree.class)
    private String icon = Resources.CATEGORY;

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    public boolean isHasChild() {
        return hasChild;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }

    public String getTreeType() {
        return treeType;
    }

    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public void setParentCategoryLevel(Integer parentCategoryLevel) {
        this.parentCategoryLevel = parentCategoryLevel;
    }

    public Integer getParentCategoryLevel() {
        return parentCategoryLevel;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public static final class FieldNames {

        public static final String REMARK = "remark";
        public static final String NAME = "name";

        private FieldNames() {};
    }

    public Collection<Tree> getTemplates() {
        return templates;
    }

    public void setTemplates(Collection<Tree> templates) {
        this.templates = templates;
    }

    @Override
    public Long getParentId() {
        return this.parentCategoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentCategoryId = parentId;
    }

    @Override
    public Integer getCategoryLevel() {
        return this.parentCategoryLevel;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {
        this.parentCategoryLevel = categoryLevel;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof CategoryDTO)) {
            return false;
        }
        CategoryDTO other = (CategoryDTO) obj;
        if (categoryType != other.categoryType) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (hasChild != other.hasChild) {
            return false;
        }
        if (icon == null) {
            if (other.icon != null) {
                return false;
            }
        } else if (!icon.equals(other.icon)) {
            return false;
        }
        if (parentCategoryId == null) {
            if (other.parentCategoryId != null) {
                return false;
            }
        } else if (!parentCategoryId.equals(other.parentCategoryId)) {
            return false;
        }
        if (parentCategoryLevel == null) {
            if (other.parentCategoryLevel != null) {
                return false;
            }
        } else if (!parentCategoryLevel.equals(other.parentCategoryLevel)) {
            return false;
        }
        if (parentCategoryName == null) {
            if (other.parentCategoryName != null) {
                return false;
            }
        } else if (!parentCategoryName.equals(other.parentCategoryName)) {
            return false;
        }
        if (posIndex == null) {
            if (other.posIndex != null) {
                return false;
            }
        } else if (!posIndex.equals(other.posIndex)) {
            return false;
        }
        if (treeType == null) {
            if (other.treeType != null) {
                return false;
            }
        } else if (!treeType.equals(other.treeType)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!type.equals(other.type)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((categoryType == null) ? 0 : categoryType.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
//        result = prime * result + (hasChild ? 1231 : 1237);
        result = prime * result + ((icon == null) ? 0 : icon.hashCode());
        result = prime * result + ((parentCategoryId == null) ? 0 : parentCategoryId.hashCode());
        result = prime * result + ((parentCategoryLevel == null) ? 0 : parentCategoryLevel.hashCode());
        result = prime * result + ((parentCategoryName == null) ? 0 : parentCategoryName.hashCode());
        result = prime * result + ((posIndex == null) ? 0 : posIndex.hashCode());
//        result = prime * result + ((templates == null) ? 0 : templates.hashCode());
        result = prime * result + ((treeType == null) ? 0 : treeType.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }


}
