package com.mycompany.myapp.dto;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class GenerateBillingCycleDTO extends BaseResponseDTO {
    @Positive
    @NotNull
    private Integer quantity;
    private Timestamp beginDate;
    private Timestamp endDate;

    private Integer fromOfDay;

}
