package com.mycompany.myapp.dto;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GeoNetZoneDTO extends BaseResponseDTO {

    @NotNull(groups = {Create.class, Update.class})
    @PositiveOrZero(groups = {Create.class, Update.class})
    private Long cellId;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long geoHomeZoneId;

    private Timestamp updateDate;

    private Integer domainId;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.GEO_NET_ZONE;
    
    @JsonView(Tree.class)
    private String icon = Resources.GEO_NET_ZONE;

    private String geoHomeZoneName;

    private String homeZoneCode;

    private Calendar magic;

    private String tempUpdateDate;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
