package com.mycompany.myapp.dto.normailizer.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StringTrimConverter implements ConstraintValidator<TrimString, Object> {

    private String[] fieldNames;

    @Override
    public void initialize(TrimString constraintAnnotation) {
        this.fieldNames = constraintAnnotation.fieldNames();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        for (String fieldName : fieldNames) {
            Object fieldValue = new BeanWrapperImpl(value)
                .getPropertyValue(fieldName);
            if (fieldValue != null) {
                new BeanWrapperImpl(value).setPropertyValue(fieldName, fieldValue.toString().trim());
            }
        }

        return true;
    }
}
