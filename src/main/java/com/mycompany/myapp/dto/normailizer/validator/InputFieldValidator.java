package com.mycompany.myapp.dto.normailizer.validator;

import com.mycompany.myapp.domain.constant.NormalizerType;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InputFieldValidator implements ConstraintValidator<InputFieldConstraint, Object> {

    public static final String REGEX = "^(?<f>\\w+(\\{((\\w+\\=\\w+)(\\&\\w+\\=\\w+)*)*\\;(\\w+\\(((\\d+)(\\,\\d+)*)?\\))(\\:\\w+\\(((\\d+)(\\,\\d+)*)?\\))*\\})?)(\\.\\w+(\\{((\\w+\\=\\w+)(\\&\\w+\\=\\w+)*)*\\;(\\w+\\(((\\d+)(\\,\\d+)*)?\\))(\\:\\w+\\(((\\d+)(\\,\\d+)*)?\\))*\\})?)*$";

    private String fieldName;
    private String dependFieldName;

    @Override
    public void initialize(InputFieldConstraint constraint) {
        this.fieldName = constraint.fieldName();
        this.dependFieldName = constraint.dependFieldName();
    }


    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Object fieldValue = new BeanWrapperImpl(value)
            .getPropertyValue(this.fieldName);
        Object dependFieldValue = new BeanWrapperImpl(value)
            .getPropertyValue(this.dependFieldName);


        if (dependFieldValue == NormalizerType.StringMatchNormalizer || dependFieldValue == NormalizerType.CheckInListNormalizer) {
            if (!StringUtils.isEmpty(fieldValue)) {
//                Pattern pattern = Pattern.compile(REGEX);
//                assert dependFieldValue != null;
//                Matcher matcher = pattern.matcher((String) dependFieldValue);
//                return matcher.find();
                return true;
            }
            return false;
        }
        return true;
    }
}
