package com.mycompany.myapp.dto.normailizer;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class Color {

    @NotBlank(groups = {Create.class, Update.class})
    @Length(max = 80, groups = {Create.class, Update.class})
    @Pattern(regexp = "^#[0-9A-F]{6}$", groups = {Create.class, Update.class}, message = "wrong_color_code_format")
    private String text;

    @NotBlank(groups = {Create.class, Update.class})
    @Length(max = 80, groups = {Create.class, Update.class})
    @Pattern(regexp = "^#[0-9A-F]{6}$", groups = {Create.class, Update.class}, message = "wrong_color_code_format")
    private String background;

    public interface Create{}
    public interface Update{}
}
