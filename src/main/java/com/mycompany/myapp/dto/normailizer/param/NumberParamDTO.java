//package com.mycompany.myapp.dto.normailizer.param;
//
//import com.mycompany.myapp.domain.normalizer.NormalizerParam;
//import com.mycompany.myapp.dto.normailizer.NormParamDTO;
//import lombok.Data;
//import org.hibernate.validator.constraints.Range;
//
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Positive;
//
//@Data
//public class NumberParamDTO implements NormParamDTO {
//    private Long id;
//
//    @NotNull
//    private Integer valueId;
//
//    @NotNull
//    private Long valueName;
//
//    @NotNull
//    private Boolean isCharacteristic;
//
//    @NotNull
//    @Range(min = 0, max = Integer.MAX_VALUE)
//    private Long parameterValue;
//
//    @NotNull
//    @Range(min = 0, max = Integer.MAX_VALUE)
//    private Long priority;
//
//    @NotNull
//    @Positive
//    @Range(min = 1, max = 5)
//    private Long compareType;
//
//    public String getConfigInput() {
//        return String.format("value:%d;isUsingCharacteristic:%s;comparisionType:%d;priority:%d", this.valueId, this.isCharacteristic.toString(), this.compareType, this.priority);
//    }
//
//    public NormalizerParam toEntity() {
//        NormalizerParam entity = new NormalizerParam();
//        entity.setId(this.id);
//        entity.setConfigInput(this.getConfigInput());
//        entity.setValueId(this.getValueId());
//        return entity;
//    }
//}
