package com.mycompany.myapp.dto.normailizer.validator;

import com.mycompany.myapp.domain.constant.StartType;
import com.mycompany.myapp.service.exception.ErrorMessage;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateNormParamValidator implements ConstraintValidator<DateNormParamConstraint, Object> {

    private String fieldName1;
    private String fieldName2;
    private String dependFieldName1;
    private String dependFieldName2;
    private static final String END_VALUE = "endValue";
    private static final String START_VALUE = "startValue";
    private static final String START_TYPE = "startType";
    private static final String END_TYPE = "endType";

    public void initialize(DateNormParamConstraint constraint) {
        this.fieldName1 = constraint.fieldName1();
        this.fieldName2 = constraint.fieldName2();
        this.dependFieldName1 = constraint.dependFieldName1();
        this.dependFieldName2 = constraint.dependFieldName2();
    }

    private void customMessageForValidation(ConstraintValidatorContext constraintContext, String fieldName,
                                            String message) {
        // build new violation message and add it
        constraintContext.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldName).addConstraintViolation();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (value == null) {
            return true;
        }
        Object fieldValue1 = new BeanWrapperImpl(value).getPropertyValue(this.fieldName1);

        Object fieldValue2 = null;
        if (!fieldName2.equals("")) {
            fieldValue2 = new BeanWrapperImpl(value).getPropertyValue(this.fieldName2);
        }

        Object dependFieldValue1 = new BeanWrapperImpl(value).getPropertyValue(this.dependFieldName1);

        Object dependFieldValue2 = null;
        if (!dependFieldName2.equals("")) {
            dependFieldValue2 = new BeanWrapperImpl(value).getPropertyValue(this.dependFieldName2);
        }

        assert fieldValue1 != null;
        if (fieldValue1 == StartType.NONE.getValue()) {
            Pattern pattern = Pattern.compile(
                    "^\\d\\d\\d\\d/([0]?[1-9]|1[012])/([1-9]|([012][0-9])|(3[01]))/([01]?[0-9]|2[0-3])/[0-5][0-9]/[0-5][0-9]$");
            assert dependFieldValue1 != null;
            Matcher matcher = pattern.matcher((String) dependFieldValue1);
            if (!matcher.find()) {
                customMessageForValidation(context, dependFieldName1, ErrorMessage.NormalizerParam.PARAMETER_VALUE_INVALID);
                return false;
            }
        }
        if (fieldValue1 == StartType.DELTA.getValue()) {
            Pattern pattern = Pattern.compile("^-?\\d{1,10}$");
            assert dependFieldValue1 != null;
            Matcher matcher = pattern.matcher((String) dependFieldValue1);
            if (!matcher.find()) {
                customMessageForValidation(context, dependFieldName1,ErrorMessage.NormalizerParam.PARAMETER_VALUE_NOT_A_DIGIT);
                return false;
            }
        }
        if (fieldValue1 == StartType.CURRENT_TIME.getValue()) {
            if (!"none".equals((String) dependFieldValue1)) {
                customMessageForValidation(context, dependFieldName1,ErrorMessage.NormalizerParam.MUST_BE_NONE);
                return false;
            }
        }

        assert fieldValue2 != null && dependFieldValue2 != null;
        if (fieldValue1 == StartType.DELTA.getValue() && fieldValue2 == StartType.DELTA.getValue()) {
            if (START_VALUE.equals(dependFieldName2)) {
                if (Integer.parseInt((String) dependFieldValue1) < Integer.parseInt((String) dependFieldValue2)) {
                    customMessageForValidation(context, dependFieldName1,ErrorMessage.NormalizerParam.START_VALUE_AND_END_VALUE);
                    return false;
                }
            }
            if (END_VALUE.equals(dependFieldName2)) {
                if (Integer.parseInt((String) dependFieldValue1) > Integer.parseInt((String) dependFieldValue2)) {
                    customMessageForValidation(context, dependFieldName1,ErrorMessage.NormalizerParam.START_VALUE_AND_END_VALUE);
                    return false;
                }
            }
        }
        if (fieldValue1 == StartType.NONE.getValue() && fieldValue2 == StartType.NONE.getValue()) {
            LocalDateTime dateTime1 = convertToDate((String) dependFieldValue1);
            LocalDateTime dateTime2 = convertToDate((String) dependFieldValue2);
            if (START_VALUE.equals(dependFieldName2)) {
                if (dateTime1.isBefore(dateTime2)) {
                    customMessageForValidation(context, dependFieldName2,ErrorMessage.NormalizerParam.START_VALUE_AND_END_VALUE);
                    return false;
                }
            }
            if (END_VALUE.equals(dependFieldName2)) {
                if (dateTime2.isBefore(dateTime1)) {
                    customMessageForValidation(context, dependFieldName2,ErrorMessage.NormalizerParam.START_VALUE_AND_END_VALUE);
                    return false;
                }
            }
        }

        if (fieldValue1 == StartType.CURRENT_TIME.getValue() && fieldValue2 == StartType.CURRENT_TIME.getValue()) {
            customMessageForValidation(context, fieldName1,ErrorMessage.NormalizerParam.CAN_NOT_SAME_CURRENT_TIME);
            return false;
        }

        if ((START_TYPE.equals(fieldName1) && fieldValue1 == StartType.DELTA.getValue() && (fieldValue2 != StartType.DELTA.getValue() && fieldValue2 != null))
                || (START_TYPE.equals(fieldName2) && fieldValue2 == StartType.DELTA && fieldValue1 != StartType.DELTA)) {
            if (START_VALUE.equals(dependFieldName2)) {
                if (Integer.parseInt((String) dependFieldValue2) <= 0) {
                    customMessageForValidation(context, dependFieldName2,ErrorMessage.NormalizerParam.MUST_BE_GREATER_THAN_0);
                    return false;
                }
            }
            if (START_VALUE.equals(dependFieldName1)) {
                if (Integer.parseInt((String) dependFieldValue1) <= 0) {
                    customMessageForValidation(context, dependFieldName1,ErrorMessage.NormalizerParam.MUST_BE_GREATER_THAN_0);
                    return false;
                }
            }
        }

        if ((END_TYPE.equals(fieldName1) && fieldValue1 == StartType.DELTA.getValue()
                && fieldValue2 != StartType.DELTA.getValue() && fieldValue2 != null)
                || (END_TYPE.equals(fieldName2) && fieldValue2 == StartType.DELTA && fieldValue1 != StartType.DELTA)) {
            if (END_VALUE.equals(dependFieldName2)) {
                if (Integer.parseInt((String) dependFieldValue2) <= 0) {
                    customMessageForValidation(context, dependFieldName2,ErrorMessage.NormalizerParam.MUST_BE_GREATER_THAN_0);
                    return false;
                }
            }
            if (END_VALUE.equals(dependFieldName1)) {
                if (Integer.parseInt((String) dependFieldValue1) <= 0) {
                    customMessageForValidation(context, dependFieldName1,ErrorMessage.NormalizerParam.MUST_BE_GREATER_THAN_0);
                    return false;
                }
            }
        }

        return true;
    }

    private LocalDateTime convertToDate(String value) {
        String[] timeArr = value.split("/");
        String time = timeArr[0] + "-" + timeArr[1] + "-" + timeArr[2] + " " + timeArr[3] + ":" + timeArr[4] +
            ":" + timeArr[5];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(time, formatter);
    }
}
