package com.mycompany.myapp.dto.normailizer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.validation.groups.ConvertGroup;

@Data
@TrimString(fieldNames = {"name","description","tempId"},
    groups = {NormValueDTO.Create.class, NormValueDTO.Update.class})
public class NormValueDTO {

    @NotNull(groups = {Create.class, Update.class })
    private ActionType actionType;

    @Null(groups = {Create.class})
    private Long id;

    @NotNull(groups = {Create.class, Update.class })
    private boolean isDefault;

    private boolean hasParams;

    @NotNull(groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    @Max(value = Long.MAX_VALUE, groups = {Create.class, Update.class})
    private Long valueId;

    @NotBlank(groups = {Create.class, Update.class})
    @Length(max = 255, groups = {Create.class, Update.class})
    @JsonProperty("valueName")
    private String name;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    @Valid
    @ConvertGroup.List({
        @ConvertGroup(from = Create.class, to = Color.Create.class),
        @ConvertGroup(from = Update.class, to = Color.Update.class)
    })
    private Color color;

    private Integer domainId;

    @NotBlank(groups = {Create.class, Update.class})
    private String tempId;

    public interface Create {}
    public interface Update {}
}
