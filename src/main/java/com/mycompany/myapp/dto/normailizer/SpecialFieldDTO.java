package com.mycompany.myapp.dto.normailizer;

import com.mycompany.myapp.domain.constant.NormalizerType;
import lombok.Data;
import org.bouncycastle.util.Strings;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Data
public class SpecialFieldDTO {

    private Boolean availableAmount;

    @NotNull(groups = Balance.class)
    @Range(min = 0, max = Integer.MAX_VALUE, groups = Balance.class)
    private Long period;

    @Range(min = 1, max = 4, groups = {AllType.class, Balance.class}, message = "compare_mode_invalid")
    @NotNull(groups = {AllType.class, Balance.class})
    private Long compareMode;

    @Range(min = 0, max = Integer.MAX_VALUE, groups = StringType.class)
    private Long startCharacter;

    @Range(min = 0, max = Integer.MAX_VALUE, groups = StringType.class)
    private Long endCharacter;

    public String toSpecialString(NormalizerType normalizerType) {
        String result = "";
        switch (normalizerType) {
            case StringMatchNormalizer:
                String endCharacterStr = "";
                if (this.getEndCharacter() == Integer.MAX_VALUE) {
                    endCharacterStr = "Integer.MaxValue";
                } else endCharacterStr = this.getEndCharacter().toString();
                result = String.format("startCharacter:%d;endCharacter:%s",
                    this.getStartCharacter(), endCharacterStr);
                break;
            case DateNormalizer:
            case QuantityNormalizer:
                result = String.format("compareType:%d",
                    this.compareMode);
                break;
            case BalanceNormalizer:
                result = String.format("isAvailableAmount:%s;period:%d;compareType:%d",
                    this.getAvailableAmount().toString(),
                    this.period,
                    this.compareMode);
                break;
            case AcmBalanceNormalizer:
                result = String.format("period:%d;compareType:%d",
                    this.period,
                    this.compareMode);
                break;
        }
        return result;
    }

    public Long getStartCharacter() {
        return Objects.requireNonNullElse(this.startCharacter, 0L);
    }

    public Long getEndCharacter() {
        return Objects.requireNonNullElse(this.endCharacter, Long.parseLong(Integer.MAX_VALUE + ""));
    }

    public Long getPeriod() {
        return Objects.requireNonNullElse(this.period, 0L);
    }

    public Boolean getAvailableAmount() {
        return Objects.requireNonNullElse(this.availableAmount, false);
    }

    public interface Balance {
    }

    public interface AllType {
    }

    public interface StringType {
    }
}
