package com.mycompany.myapp.dto.normailizer.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = InputFieldValidator.class)
@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
public @interface InputFieldConstraint {
    String fieldName();
    String dependFieldName();

    String message() default "must_not_be_blank";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
