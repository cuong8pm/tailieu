//package com.mycompany.myapp.dto.normailizer.param;
//
//import com.mycompany.myapp.domain.normalizer.NormalizerParam;
//import com.mycompany.myapp.dto.normailizer.NormParamDTO;
//import lombok.Data;
//import org.hibernate.validator.constraints.Range;
//
//import javax.validation.constraints.NotNull;
//
//@Data
//public class AcmBalanceParamDTO implements NormParamDTO {
//    private Long id;
//
//    @NotNull
//    private Integer valueId;
//
//    @NotNull
//    private Long valueName;
//
//    @NotNull
//    private Boolean isCharSpecStartValue;
//
//    @NotNull
//    private Boolean isCharSpecEndValue;
//
//    @NotNull
//    @Range(min = 0, max = Integer.MAX_VALUE)
//    private Long startValue;
//
//    @NotNull
//    @Range(min = 0, max = Integer.MAX_VALUE)
//    private Long endValue;
//
//    @NotNull
//    @Range(min = 0, max = Integer.MAX_VALUE)
//    private Long priority;
//
//    public String getConfigInput() {
//        return String.format("start:%d;startIsParameter:%s;end:%d;endIsParameter:%s;priority:%d",this.startValue, this.isCharSpecStartValue.toString(), this.endValue, this.isCharSpecEndValue.toString(), this.priority);
//    }
//
//    public NormalizerParam toEntity() {
//        NormalizerParam entity = new NormalizerParam();
//        entity.setId(this.id);
//        entity.setConfigInput(this.getConfigInput());
//        entity.setValueId(this.getValueId());
//        return entity;
//    }
//}
