package com.mycompany.myapp.dto.normailizer.param;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mycompany.myapp.domain.constant.DateEnum;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeNormParamDTO implements NormParamDTO {
    private Long id;

    @NotNull
    private Long valueId;

    private Long valueName;

    private ActionType actionType;

    @NotNull
    private DateEnum date;

    @JsonIgnore
    private Integer parseDate;

    @NotNull
    @Pattern(regexp = "^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$", message = "time_format_invalid")
    private String time;

    public TimeNormParamDTO(@NotNull DateEnum date,
        @NotNull @Pattern(regexp = "^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$", message = "time_format_invalid") String time) {
        this.date = date;
        this.time = time;
    }

    public TimeNormParamDTO(NormalizerParam param) {
        if (param != null) {
            this.id = param.getId();
            this.valueId = param.getValueId();
            Map<String, Object> configInput = OCSUtils.stringToObjectMap(param.getConfigInput());
            if(configInput.containsKey("day")){
                this.date = parseNumberToDate(Integer.parseInt(configInput.get("day").toString()));
            }
            this.time = configInput.get("hour").toString() + ":" + configInput.get("min").toString() + ":" + configInput.get("sec").toString();
        }
    }

    @Override
    public String getConfigInput(NormalizerType normalizerType) {
        parseDateToNumber();
        String[] times = time.split(":");
        return String.format("day:%d;hour:%s;min:%s;sec:%s", this.parseDate, times[0], times[1], times[2]);
    }

    private void parseDateToNumber() {
        switch (this.getDate()) {
        case Sunday:
            parseDate = 1;
            break;
        case Monday:
            parseDate = 2;
            break;
        case Tuesday:
            parseDate = 3;
            break;
        case Wednesday:
            parseDate = 4;
            break;
        case Thursday:
            parseDate = 5;
            break;
        case Friday:
            parseDate = 6;
            break;
        case Saturday:
            parseDate = 7;
            break;
        default:
            throw new ResourceNotFoundException("date_invalid", "time params", "date");
        }
    }

    private DateEnum parseNumberToDate(int value) {
        switch (value) {
            case 1:
                return DateEnum.Sunday;
            case 2:
                return DateEnum.Monday;
            case 3:
                return DateEnum.Tuesday;
            case 4:
                return DateEnum.Wednesday;
            case 5:
                return DateEnum.Thursday;
            case 6:
                return DateEnum.Friday;
            case 7:
                return DateEnum.Saturday;
            default:
                throw new ResourceNotFoundException("date_invalid", "time params", "date");
        }
    }

    @Override
    public NormalizerParam toEntity(NormalizerType normalizerType) {
        NormalizerParam entity = new NormalizerParam();
        entity.setId(this.id);
        entity.setConfigInput(this.getConfigInput(normalizerType));
        entity.setValueId(this.getValueId());
        return entity;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeNormParamDTO timeNormParamDTO = (TimeNormParamDTO) o;
        return timeNormParamDTO.getDate() == this.getDate() && timeNormParamDTO.getTime().equals(this.getTime());
    }
}
