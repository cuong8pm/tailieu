package com.mycompany.myapp.dto.normailizer;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;

public interface NormParamDTO {
    String getConfigInput(NormalizerType normalizerType);
    NormalizerParam toEntity(NormalizerType normalizerType);
    ActionType getActionType();
    Long getId();
    void setId(Long id);
    Long getValueId();
}
