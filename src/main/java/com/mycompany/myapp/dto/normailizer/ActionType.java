package com.mycompany.myapp.dto.normailizer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum ActionType {
    //old: được gán khi object lấy từ trong db ra ko có actionType
    ADD("add"), EDIT("edit"), DELETE("delete"), OLD("old");
    @JsonValue
    private String value;

    public String getValue() {
        return this.value;
    }

    ActionType(String actionType) {
        this.value = actionType;
    }

    @JsonCreator
    public static ActionType of(String value) {
        return Stream.of(ActionType.values()).filter(c -> c.getValue().equals(value)).findFirst()
            .orElse(OLD);
    }
}
