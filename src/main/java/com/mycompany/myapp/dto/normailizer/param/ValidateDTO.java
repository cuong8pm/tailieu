package com.mycompany.myapp.dto.normailizer.param;

import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

/**
 * Class phục vụ cho việc validate list object normalizer parameter
 */
@Data
public class ValidateDTO {
    @Valid
    private List<? extends NormParamDTO> listOfParameters;
}
