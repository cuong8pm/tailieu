package com.mycompany.myapp.dto.normailizer.validator;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class NormFilterValidator {
    @Valid
    private List<PreFunctionDTO> preFunctions;
    @Valid
    private List<FilterDTO> filters;
}
