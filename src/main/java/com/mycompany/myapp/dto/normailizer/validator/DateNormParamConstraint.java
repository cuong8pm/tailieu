package com.mycompany.myapp.dto.normailizer.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = DateNormParamValidator.class)
@Target({ TYPE, ANNOTATION_TYPE })
@Repeatable(DateNormParamConstraint.List.class)
@Retention(RUNTIME)
public @interface DateNormParamConstraint {

    String fieldName1();
    String fieldName2();
    String dependFieldName1();
    String dependFieldName2();

    String message() default "value_for_type_invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        DateNormParamConstraint[] value();
    }
}
