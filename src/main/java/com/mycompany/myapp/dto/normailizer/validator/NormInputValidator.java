package com.mycompany.myapp.dto.normailizer.validator;

import com.mycompany.myapp.dto.normailizer.validator.NormFilterValidator;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class NormInputValidator {
    @NotBlank
    private String paths;
    @Valid
    private List<NormFilterValidator> filterAndFunctions;
}
