package com.mycompany.myapp.dto.normailizer.param;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.utils.OCSUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.util.Map;

@Data
@NoArgsConstructor
@TrimString(fieldNames = {"valueName", "parameterValue"}, groups = {NormalParamDTO.StringType.class, NormalParamDTO.NumberType.class, NormalParamDTO.NumberParamType.class})
public class NormalParamDTO implements NormParamDTO {

    private ActionType actionType;

    private Long id;

    @NotNull(groups = {StringType.class, NumberType.class, NumberParamType.class})
    private Long valueId;

    private String valueName;

    @NotNull(groups = {StringType.class, NumberType.class})
    private Boolean isCharacteristic;

    @NotBlank(groups = {StringType.class, NumberType.class, NumberParamType.class, NumberParamType.class})
    @Pattern(regexp = "^[^;:]*$", message = "can't_contain_(:)_and_(;)_characters", groups = StringType.class)
    @Pattern(regexp = "^(\\d{1,10})*$", message = "field_value_must_be_in_a_period_of_0_to_" + Integer.MAX_VALUE, groups = NumberType.class)
    private String parameterValue;

    private String parameterName;

    @NotNull(groups = {StringType.class, NumberType.class, NumberParamType.class})
    @Range(min = 0, max = Integer.MAX_VALUE, groups = {StringType.class, NumberType.class})
    private Long priority;

    @NotNull(groups = {StringType.class, NumberType.class, NumberParamType.class})
    @Positive(groups = {StringType.class, NumberType.class, NumberParamType.class})
    @Range(min = 1, max = 4, message = "compare_type_invalid", groups = {StringType.class})
    @Range(min = 1, max = 5, message = "compare_type_invalid", groups = {NumberType.class, NumberParamType.class})
    private Long compareType;

    public String getConfigInput(NormalizerType normalizerType) {
        switch (normalizerType) {
            case StringNormalizer:
            case NumberNormalizer:
                return String.format("value:%s;isUsingCharacteristic:%s;comparisionType:%d;priority:%d",
                    this.parameterValue.trim(),
                    this.isCharacteristic.toString(),
                    this.compareType,
                    this.priority
                );
            case NumberParameterNormalizer:
                return String.format("characteristicID:%s;comparisionType:%d;priority:%d",
                    this.parameterValue.trim(),
                    this.compareType,
                    this.priority
                );
            default:
                return null;
        }
    }

    @Override
    public NormalizerParam toEntity(NormalizerType normalizerType) {
        NormalizerParam entity = new NormalizerParam();
        entity.setId(this.id);
        entity.setConfigInput(this.getConfigInput(normalizerType));
        entity.setValueId(this.getValueId());
        return entity;
    }

    public NormalParamDTO(NormalizerParam param) {
        if (param != null) {
            this.id = param.getId();
            this.valueId = param.getValueId();
            Map<String, Object> configInput = OCSUtils.stringToObjectMap(param.getConfigInput());
            if (configInput.containsKey("value")) {
                this.parameterValue = configInput.get("value").toString();
            }
            if (configInput.containsKey("isUsingCharacteristic")) {
                this.isCharacteristic = Boolean.parseBoolean(configInput.get("isUsingCharacteristic").toString());
            }
            if (configInput.containsKey("characteristicID")) {
                this.parameterValue = configInput.get("characteristicID").toString();
                this.setIsCharacteristic(true);
            }
            this.compareType = Long.parseLong(configInput.get("comparisionType").toString());
            this.priority = Long.parseLong(configInput.get("priority").toString());
            this.actionType = ActionType.OLD;
        }
    }

    public interface StringType {

    }

    public interface NumberType {

    }

    public interface NumberParamType {

    }

    public String getParameterValue(){
        return this.parameterValue.trim();
    }
}
