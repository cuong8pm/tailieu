package com.mycompany.myapp.dto.normailizer.param;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.NormValueDTO;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.utils.OCSUtils;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@TrimString(fieldNames = {"valueName"})
public class StartEndParamDTO implements NormParamDTO {

    private ActionType actionType;

    private Long id;

    @NotNull
    private Long valueId;

    private String valueName;

    @NotNull
    private Boolean startIsCharacteristic;

    @NotNull
    private Boolean endIsCharacteristic;

    @NotNull
    @Range(min = 0, max = Integer.MAX_VALUE)
    private Long startValue;

    private String startName;

    @NotNull
    @Range(min = 0, max = Integer.MAX_VALUE)
    private Long endValue;

    private String endName;

    @NotNull
    @Range(min = 0, max = Integer.MAX_VALUE)
    private Long priority;

    @Override
    public String getConfigInput(NormalizerType normalizerType) {
        return String.format("start:%d;startIsCharacteristic:%s;end:%d;endIsCharacteristic:%s;priority:%d",
            this.startValue,
            this.startIsCharacteristic.toString(),
            this.endValue,
            this.endIsCharacteristic.toString(),
            this.priority);
    }

    @Override
    public NormalizerParam toEntity(NormalizerType normalizerType) {
        NormalizerParam entity = new NormalizerParam();
        entity.setId(this.id);
        entity.setConfigInput(this.getConfigInput(normalizerType));
        entity.setValueId(this.getValueId());
        return entity;
    }

    public StartEndParamDTO(NormalizerParam param) {
        if (param != null) {
            this.id = param.getId();
            this.valueId = param.getValueId();
            Map<String, Object> configInput = OCSUtils.stringToObjectMap(param.getConfigInput());
            this.startValue = Long.parseLong(configInput.get("start").toString());
            this.startIsCharacteristic = Boolean.parseBoolean(configInput.get("startIsCharacteristic").toString());
            this.endValue = Long.parseLong(configInput.get("end").toString());
            this.endIsCharacteristic = Boolean.parseBoolean(configInput.get("endIsCharacteristic").toString());
            this.priority = Long.parseLong(configInput.get("priority").toString());
            this.actionType = ActionType.OLD;
        }
    }
    public StartEndParamDTO(){}
}
