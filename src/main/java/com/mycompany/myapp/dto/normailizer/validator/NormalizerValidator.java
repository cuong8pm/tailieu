package com.mycompany.myapp.dto.normailizer.validator;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class NormalizerValidator {
    @NotNull
    private NormInputValidator inputField1;

    private NormInputValidator inputField2;

}
