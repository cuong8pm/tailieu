package com.mycompany.myapp.dto.normailizer.param;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.constant.StartType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.validator.DateNormParamConstraint;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.utils.OCSUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

@DateNormParamConstraint(fieldName1 = "startType", fieldName2 = "", dependFieldName1 = "startValue", dependFieldName2 = "")
@DateNormParamConstraint(fieldName1 = "endType", dependFieldName1 = "endValue", message = "end_value_for_end_type_invalid", dependFieldName2 = "", fieldName2 = "")
@DateNormParamConstraint(fieldName1 = "startType", fieldName2 = "endType", dependFieldName1 = "startValue", dependFieldName2 = "endValue")
@TrimString(fieldNames = {"startValue", "endValue"})
@Data
@NoArgsConstructor
public class DateNormParamDTO implements NormParamDTO {
    private ActionType actionType;

    private Long id;

    @NotNull
    private Long valueId;

    private Long valueName;

    @NotNull
    @Range(min = 1, max = 3, message = "start_type_invalid")
    private Integer startType;

    @NotNull
    @NotBlank
    private String startValue;

    @NotNull
    @Range(min = 1, max = 3, message = "start_type_invalid")
    private Integer endType;

    @NotNull
    @NotBlank
    private String endValue;

    @Override
    public String getConfigInput(NormalizerType normalizerType) {
        String startValue = convertDataForDB(this.startValue, startType);
        String endValue = convertDataForDB(this.endValue, endType);
        return String.format("startValue:%s;startType:%d;endValue:%s;endType:%d", startValue,
            this.startType, endValue, this.endType);
    }

    @Override
    public NormalizerParam toEntity(NormalizerType normalizerType) {
        NormalizerParam entity = new NormalizerParam();
        entity.setId(this.id);
        entity.setConfigInput(this.getConfigInput(normalizerType));
        entity.setValueId(this.getValueId());
        return entity;
    }

    private String convertDataForDB(String data, Integer startType) {
        switch (startType) {
            case 1:
                return data.replace(" ", "/").replace(":", "/");
            case 2:
                return "none";
            case 3:
                return data;
            default:
                throw new ResourceNotFoundException("normalizer_type_not_found", "date params", "start_type");
        }
    }

    public DateNormParamDTO(NormalizerParam param) {
        if (param != null) {
            this.id = param.getId();
            this.valueId = param.getValueId();
            Map<String, Object> configInput = OCSUtils.stringToObjectMap(param.getConfigInput());
            this.startType = Integer.parseInt(configInput.get("startType").toString());
            this.startValue = configInput.get("startValue").toString();
            this.endValue = configInput.get("endValue").toString();
            this.endType = Integer.parseInt(configInput.get("endType").toString());
            this.actionType = ActionType.OLD;
        }
    }
}
