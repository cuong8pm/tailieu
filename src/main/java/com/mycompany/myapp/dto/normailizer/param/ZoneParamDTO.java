package com.mycompany.myapp.dto.normailizer.param;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.normailizer.NormParamDTO;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.utils.OCSUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@TrimString(fieldNames = {"valueName", "zoneDataName"})
@NoArgsConstructor
public class ZoneParamDTO implements NormParamDTO {
    private ActionType actionType;

    private Long id;

    @NotNull
    private Long valueId;

    private String valueName;

    @NotNull
    @Range(min = 0, max = 2, message = "zone_value_type_id_invalid")
    private Integer zoneValueTypeId;

    @NotNull
    private Long zoneValueId;

    private String zoneDataName;

    @Override
    public String getConfigInput(NormalizerType normalizerType) {
        return String.format("zoneValueId:%d;dataZoneType:%d", this.zoneValueId, this.zoneValueTypeId);
    }

    @Override
    public NormalizerParam toEntity(NormalizerType normalizerType) {
        NormalizerParam entity = new NormalizerParam();
        entity.setId(this.id);
        entity.setConfigInput(this.getConfigInput(normalizerType));
        entity.setValueId(this.getValueId());
        return entity;
    }

    public ZoneParamDTO(NormalizerParam param) {
        if (param != null) {
            this.id = param.getId();
            this.valueId = param.getValueId();
            Map<String, Object> configInput = OCSUtils.stringToObjectMap(param.getConfigInput());
            if (configInput.containsKey("dataZoneType")) {
                this.zoneValueTypeId = Integer.parseInt(configInput.get("dataZoneType").toString());
            }
            if (configInput.containsKey("zoneValueId")) {
                this.zoneValueId = Long.parseLong(configInput.get("zoneValueId").toString());
            }
            this.actionType = ActionType.OLD;
        }
    }
}
