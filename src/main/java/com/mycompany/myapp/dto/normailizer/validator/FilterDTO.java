package com.mycompany.myapp.dto.normailizer.validator;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class FilterDTO {
    @NotBlank
    private String key;

    @NotBlank
    @Length(max = 64)
    private String value;

    public FilterDTO(@NotBlank String key, @NotBlank  String value) {
        this.key = key;
        this.value = value;
    }
}
