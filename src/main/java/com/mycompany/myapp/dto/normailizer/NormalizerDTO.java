package com.mycompany.myapp.dto.normailizer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.InputFieldConstraint;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@TrimString(fieldNames = {"name", "description", "inputField", "inputField2", "specialField"}, groups = {NormalizerDTO.Create.class, NormalizerDTO.Update.class})
@InputFieldConstraint(fieldName = "inputField2", dependFieldName = "typeId", groups = {NormalizerDTO.Create.class, NormalizerDTO.Update.class})
public class NormalizerDTO extends TreeClone {

    @NotNull(groups = {Create.class, Update.class})
    private NormalizerType typeId;

    @JsonView(Tree.class)
    @NotNull(groups = {Create.class, Update.class})
    private Long categoryId;

    @NotNull(groups = {Create.class, Update.class})
    private Long stateId;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    @NotBlank(groups = {Create.class, Update.class})
//    @Pattern(regexp = InputFieldValidator.REGEX, message = "input_field_invalid", groups = {Create.class, Update.class})
    private String inputField;

    private String inputField2;

    private SpecialFieldDTO specialFieldDTO;

    private String specialField;

    private Long defaultValue;

    @Valid
    @JsonProperty("listOfValues")
    @ConvertGroup.List({
        @ConvertGroup(from = Create.class, to = NormValueDTO.Create.class),
        @ConvertGroup(from = Update.class, to = NormValueDTO.Update.class)
    })
    private List<NormValueDTO> values;

    @JsonProperty("listOfParameters")
    private List<Map<String, Object>> parameters;

    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.NORMALIZER;

    private Integer categoryLevel;

    private String categoryName;

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NORMALIZER_TYPE = "normalizer_type";
    }
}
