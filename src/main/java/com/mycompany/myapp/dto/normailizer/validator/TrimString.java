package com.mycompany.myapp.dto.normailizer.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = StringTrimConverter.class)
@Target({ TYPE, ANNOTATION_TYPE,FIELD })
@Retention(RUNTIME)
public @interface TrimString {
    String[] fieldNames();

    String message() default "cannot_trim";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
