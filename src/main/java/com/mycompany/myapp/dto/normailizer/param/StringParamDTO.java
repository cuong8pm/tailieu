//package com.mycompany.myapp.dto.normailizer.param;
//
//import com.mycompany.myapp.domain.normalizer.NormalizerParam;
//import com.mycompany.myapp.dto.normailizer.NormParamDTO;
//import lombok.Data;
//import org.hibernate.validator.constraints.Range;
//
//import javax.annotation.RegEx;
//import javax.validation.constraints.*;
//
//@Data
//public class StringParamDTO implements NormParamDTO {
//    private Long id;
//
//    @NotNull
//    private Integer valueId;
//
//    @NotNull
//    private Long valueName;
//
//    @NotNull
//    private Boolean isCharacteristic;
//
//    @NotNull
//    @Pattern(regexp = "^[^;:]*$", message = "can't_contain_(:)_and_(;)_characters")
//    private String parameterValue;
//
//    @NotNull
//    @Range(min = 0, max = Integer.MAX_VALUE)
//    private Long priority;
//
//    @NotNull
//    @Positive
//    @Range(min = 1, max = 4)
//    private Long compareType;
//
//    public String getConfigInput() {
//        return String.format("value:%d;isUsingCharacteristic:%s;comparisionType:%d;priority:%d", this.valueId, this.isCharacteristic.toString(), this.compareType, this.priority);
//    }
//
//    public NormalizerParam toEntity() {
//        NormalizerParam entity = new NormalizerParam();
//        entity.setId(this.id);
//        entity.setConfigInput(this.getConfigInput());
//        entity.setValueId(this.getValueId());
//        return entity;
//    }
//}
