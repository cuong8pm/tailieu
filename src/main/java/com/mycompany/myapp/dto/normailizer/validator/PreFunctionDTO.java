package com.mycompany.myapp.dto.normailizer.validator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreFunctionDTO {
    @JsonView(Dropdown.class)
    private Integer id;

    @NotBlank
    @JsonView(Dropdown.class)
    @JsonProperty("name")
    private String functionName;

    private List<String> parameters;

    @JsonView(Dropdown.class)
    private Integer numberParam;

    public interface Dropdown{}

    public PreFunctionDTO(@NotBlank String functionName, List<String> parameters,
        Integer numberParam) {
        this.functionName = functionName;
        this.parameters = parameters;
        this.numberParam = numberParam;
    }
}
