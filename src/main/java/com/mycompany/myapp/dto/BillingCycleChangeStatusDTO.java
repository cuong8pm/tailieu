package com.mycompany.myapp.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.mycompany.myapp.domain.constant.BillingCycleStatus;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BillingCycleChangeStatusDTO {

}
