package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Collection;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ZoneDTO extends Tree {

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long zoneMapId;

    private String zoneCode;

    private String description;

    private Integer domainId;

    private boolean isMatching;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.SUB_TEMPLATE;

    @JsonView(Tree.class)
    private String type = Resources.ZONE;
    
    @JsonView(Tree.class)
    private String icon = Resources.ZONE;

    private String zoneMapName;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Integer getPosIndex() {
        //vi zone khong co posIndex nen dung id de sap xep thu tu trong tree
        return zoneMapId.intValue();
    }

    @Override
    public void setPosIndex(Integer posIndex) {
    }

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.zoneMapId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.zoneMapId = parentId;
    }

    @Override
    public Integer getCategoryLevel() {
        return null;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {
    }

}
