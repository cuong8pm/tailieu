package com.mycompany.myapp.dto;

import com.mycompany.myapp.domain.constant.CalcUnitType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CalcUnitDTO extends BaseResponseDTO {
    private CalcUnitType calcUnitType;

    private String description;
}
