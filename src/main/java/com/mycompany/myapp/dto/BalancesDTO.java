package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
public class BalancesDTO extends Tree {

    @JsonView(Tree.class)
    private String description;

    @NotBlank(groups = {Create.class, Update.class})
    private String externalId;

    //Multi Bal Type
    @NotNull(groups = {Create.class, Update.class})
    private Integer multiBalTypeId;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private Long categoryId;

    // BALANCE : 0
    // ACCUMULATE BALANCES : 1
    private boolean acm;

    @NotNull(groups = {Create.class, Update.class})
    private Long paymentTypeId;

    @Size(max = 20, groups = {Create.class, Update.class})
    private String currency;

    private Integer effDateTypeId;

    @Range(min = 0, max = Integer.MAX_VALUE, groups = {Create.class, Update.class})
    private Long startDateData;

    private Timestamp startDate;

    private Integer expDateTypeId;

    @Range(min = 0, max = Integer.MAX_VALUE, groups = {Create.class, Update.class})
    private Long expDateData;

    private Timestamp expDate;

    private Integer recuringType;

    private Integer recuringPeriod;

    @NotNull(groups = {Create.class, Update.class})
    private Long unitTypeId;

    //Them display Rate cho FrontEnd su dung
    private Integer baseRate;

    private Integer periodicTypeId;

    @Min(value = 0, groups = {Create.class, Update.class})
    @Max(value = 999999999, groups = {Create.class, Update.class})
    private Integer periodicPeriod;

    @Range(min = 0, max = Integer.MAX_VALUE, groups = {Create.class, Update.class})
    private Integer windowSize;

    @Min(value = 0, groups = {Create.class, Update.class})
    @Max(value = 999999999, groups = {Create.class, Update.class})
    private Integer lowLevel;

    @Min(value = 0, groups = {Create.class, Update.class})
    @Max(value = 999999999, groups = {Create.class, Update.class})
    private Integer highLevel;

    private Integer billingCycleTypeId;

    @NotNull(groups = {Create.class, Update.class})
    private Integer ownerLevelId;

    private Long precision;

    private Boolean cleanable;

    private Integer domainId;

    private Long balTypePercision;

    //Can be clean when recurring
    private Boolean clearable;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.BAL_TYPE;
    
    @JsonView(Tree.class)
    private String icon = Resources.BAL_TYPE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    private List<ThresholdsDTO> thresholds;

    private String categoryName;

    @JsonView(Tree.class)
    private boolean hasChild;

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String WINDOW_SIZE = "windowSize";
        public static final String LOW_LEVEL = "lowLevel";
        public static final String HIGH_LEVEL = "highLevel";
        public static final String START_DATE = "startDate";
        public static final String START_DATE_DATA = "startDateData";
        public static final String EXP_DATE = "expDate";
        public static final String EXP_DATE_DATA = "expDateData";
        public static final String ID = "id";
        public static final String OWNER_LEVEL = "ownerLevelId";
        public static final String EFF_DATE_TYPE_ID = "effDateTypeId";
        public static final String EXP_DATE_TYPE_ID = "expDateTypeId";
    }
}
