package com.mycompany.myapp.dto.common;

import com.mycompany.myapp.dto.Moveable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MoveableDTO implements Moveable {
    private Long id;
    private Integer posIndex;
}
