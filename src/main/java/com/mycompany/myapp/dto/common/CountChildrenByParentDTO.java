package com.mycompany.myapp.dto.common;


public interface CountChildrenByParentDTO {
    Long getId();

    int getCountChild();
}

