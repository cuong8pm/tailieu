package com.mycompany.myapp.dto;

import javax.validation.constraints.NotNull;

public class ThresholdBaltypeMapDTO {
    @NotNull
    private Long id;
    @NotNull
    private Long thresholdId;
    @NotNull
    private Long baltypeId;
    @NotNull
    private Integer domainId;
}
