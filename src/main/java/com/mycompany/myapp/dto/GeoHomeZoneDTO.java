package com.mycompany.myapp.dto;


import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import java.util.Collection;
import java.util.Collections;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class GeoHomeZoneDTO extends Tree {
    @Length(max = 200, groups = {Create.class, Update.class})
    private String geoHomeZoneCode;
    @Length(max = 500, groups = {Create.class, Update.class})
    private String description;

    @NotNull(groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private Integer geoHomeZoneType;

    private String geoHomeZoneTypeName;

    @NotNull(groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private Long categoryId;

    private Integer domainId;

    @NotNull(groups = {Move.class, Update.class})
    @Min(value = 0, groups = {Move.class, Update.class})
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.GEO_HOME_ZONE;
    
    @JsonView(Tree.class)
    private String icon = Resources.GEO_HOME_ZONE;

    private String categoryName;
    private Integer categoryLevel;

    public String getGeoHomeZoneCode() {
        return geoHomeZoneCode;
    }

    public void setGeoHomeZoneCode(String geoHomeZoneCode) {
        this.geoHomeZoneCode = geoHomeZoneCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGeoHomeZoneType() {
        return geoHomeZoneType;
    }

    public void setGeoHomeZoneType(Integer geoHomeZoneType) {
        this.geoHomeZoneType = geoHomeZoneType;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public String getType() {
        return type;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getCategoryLevel() {
        return categoryLevel;
    }

    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getTreeType() {
        return this.treeType;
    }

    @Override
    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    @Override
    public Integer getPosIndex() {
        return posIndex;
    }

    @Override
    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    public void setType(String type) {
        this.type = type;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
