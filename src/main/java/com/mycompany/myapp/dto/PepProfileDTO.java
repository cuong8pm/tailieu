package com.mycompany.myapp.dto;

import java.util.Collection;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class PepProfileDTO extends Tree implements Moveable {
    
    @NotNull(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    @Positive(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    private Long qosId;

    @NotNull(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    @Positive(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    private Long monitorKey;
    
    @NotNull(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    @Min(value = 0, groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    @Max(value = 999999999, groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    private Integer priority;
    
    private Integer domainId;
    
    @NotNull(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    @Positive(groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    private Long categoryId;
    
    @Length(max = 100, groups = {PepProfileDTO.Create.class, PepProfileDTO.Update.class})
    private String description;
    
    private String qosName;
    
    private String categoryName;
    
    private String monitorKeyName;
    
    @JsonView(Tree.class)
    private boolean hasChild;
    
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.PEP_PROFILE;
    
    @JsonView(Tree.class)
    private String icon = Resources.PEP_PROFILE;
    
    @JsonView(Tree.class)
    private Integer categoryLevel;
    
    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Collection<Tree> getTemplates() {
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
