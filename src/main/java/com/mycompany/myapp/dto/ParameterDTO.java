package com.mycompany.myapp.dto;

import java.util.Collection;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

public class ParameterDTO extends Tree {

    @NotNull(groups = { Create.class, Update.class })
    private Integer ownerLevel;

    private String parameterValue;

    @NotNull(groups = { Create.class, Update.class })
    private boolean forTemplate;

    @NotNull(groups = { Create.class, Update.class })
    private Long categoryId;
    @Length(max = 180, groups = {Create.class, Update.class})
    private String description;

    @NotNull(groups = { Move.class, Update.class })
    @Min(value = 0, groups = { Move.class, Update.class })
    @JsonView(Tree.class)
    private Integer posIndex;
    
    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;
    
    @JsonView(Tree.class)
    private String type = Resources.PARAMETER;
    
    @JsonView(Tree.class)
    private String icon = Resources.PARAMETER;
    
    private String categoryName;

    private String ownerLevelName;
    private Integer categoryLevel;

    public Integer getOwnerLevel() {
        return ownerLevel;
    }

    public void setOwnerLevel(Integer ownerLevel) {
        this.ownerLevel = ownerLevel;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public boolean getForTemplate() {
        return forTemplate;
    }

    public void setForTemplate(boolean forTemplate) {
        this.forTemplate = forTemplate;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getOwnerLevelName() {
        return ownerLevelName;
    }

    public void setOwnerLevelName(String ownerLevelName) {
        this.ownerLevelName = ownerLevelName;
    }

    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    public Integer getCategoryLevel() {
        return categoryLevel;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Collection<Tree> getTemplates() {
//        throw new UnsupportedOperationException();
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
//        throw new UnsupportedOperationException();
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getTreeType() {
        return this.treeType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }
    
}
