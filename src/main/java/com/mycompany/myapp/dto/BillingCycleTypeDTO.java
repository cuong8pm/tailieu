package com.mycompany.myapp.dto;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BillingCycleTypeDTO extends Tree implements Moveable{

    private Long categoryId;
    private String categoryName;

    @Length(max = 250, groups = {Create.class, Update.class})
    private String description;

    private Timestamp beginDate;

    private Long unitId;
    private String unitName;
    @Positive(groups = {Create.class, Update.class})
    @NotNull(groups = {Create.class, Update.class})
    @Max(value = 999999999, groups = {Create.class, Update.class})
    private Long quantity;

    @NotNull(groups = { Move.class })
    @Min(value = 0, groups = {Move.class})
    @JsonView(Tree.class)
    private Integer posIndex;
    private Integer fromOfDay;

    private Timestamp lastestDate;
    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.BILLING_CYCLE_TYPE;
    
    @JsonView(Tree.class)
    private String icon = Resources.BILLING_CYCLE_TYPE;

    private Integer categoryLevel;

    @JsonView(Tree.class)
    private boolean hasChild = false;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Collection<Tree> getTemplates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
        // TODO Auto-generated method stub

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

}
