package com.mycompany.myapp.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

import lombok.Data;

@Data
public class UnitTypeDTO extends Tree {

    @NotNull(groups = {Create.class, Update.class})
    @Min(value = 1, groups = {Create.class, Update.class})
    @JsonView(BalTypeView.class)
    private Integer baseRate;

    @NotNull(groups = {Create.class, Update.class})
    @Min(groups = {Create.class, Update.class}, value = 1)
    private Integer displayRate;

    private Integer precision;

    @NotNull(groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    @JsonView(BalTypeView.class)
    private Integer unitPrecision;

    @NotNull(groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private Long categoryId;

    private String abbreviation;

    private String description;

    @NotNull(groups = {Move.class, Update.class})
    @Min(value = 0, groups = {Move.class, Update.class})
    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.UNIT_TYPE;
    
    @JsonView(Tree.class)
    private String icon = Resources.UNIT_TYPE;

    private String categoryName;
    private Integer categoryLevel;

    public Integer getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(Integer baseRate) {
        this.baseRate = baseRate;
    }

    public Integer getDisplayRate() {
        return displayRate;
    }

    public void setDisplayRate(Integer displayRate) {
        this.displayRate = displayRate;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    public Integer getUnitPrecision() {
        return unitPrecision;
    }

    public void setUnitPrecision(Integer unitPrecision) {
        this.unitPrecision = unitPrecision;
    }

    public String getType() {
        return type;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    public Integer getCategoryLevel() {
        return categoryLevel;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
    public interface BalTypeView {
    }

    @Override
    public Collection<Tree> getTemplates() {
        return new ArrayList<Tree>();
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getTreeType() {
        return this.treeType;
    }

    public void setType(String type) {
        this.type = type;
    }
    @Override
    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

}
