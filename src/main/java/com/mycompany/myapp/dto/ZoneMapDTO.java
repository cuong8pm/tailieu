package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ZoneMapDTO extends Tree {

    private String description;
    private Integer domainId;
    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long categoryId;
    @NotNull(groups = {Move.class, Update.class})
    @Min(value = 0, groups = {Move.class, Update.class})
    private Integer posIndex;
    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.ZONE_MAP;
    
    @JsonView(Tree.class)
    private String icon = Resources.ZONE_MAP;
    
    private String categoryName;
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private boolean hasChild;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof ZoneMapDTO)) {
            return false;
        }
        ZoneMapDTO other = (ZoneMapDTO) obj;
        if (categoryId == null) {
            if (other.categoryId != null) {
                return false;
            }
        } else if (!categoryId.equals(other.categoryId)) {
            return false;
        }
        if (categoryLevel == null) {
            if (other.categoryLevel != null) {
                return false;
            }
        } else if (!categoryLevel.equals(other.categoryLevel)) {
            return false;
        }
        if (categoryName == null) {
            if (other.categoryName != null) {
                return false;
            }
        } else if (!categoryName.equals(other.categoryName)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (domainId == null) {
            if (other.domainId != null) {
                return false;
            }
        } else if (!domainId.equals(other.domainId)) {
            return false;
        }
        if (hasChild != other.hasChild) {
            return false;
        }
        if (icon == null) {
            if (other.icon != null) {
                return false;
            }
        } else if (!icon.equals(other.icon)) {
            return false;
        }
        if (posIndex == null) {
            if (other.posIndex != null) {
                return false;
            }
        } else if (!posIndex.equals(other.posIndex)) {
            return false;
        }
        if (treeType == null) {
            if (other.treeType != null) {
                return false;
            }
        } else if (!treeType.equals(other.treeType)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!type.equals(other.type)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((categoryId == null) ? 0 : categoryId.hashCode());
        result = prime * result + ((categoryLevel == null) ? 0 : categoryLevel.hashCode());
        result = prime * result + ((categoryName == null) ? 0 : categoryName.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((domainId == null) ? 0 : domainId.hashCode());
        result = prime * result + (hasChild ? 1231 : 1237);
        result = prime * result + ((icon == null) ? 0 : icon.hashCode());
        result = prime * result + ((posIndex == null) ? 0 : posIndex.hashCode());
        result = prime * result + ((treeType == null) ? 0 : treeType.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }
    
    

}
