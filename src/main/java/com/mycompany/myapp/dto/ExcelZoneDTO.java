package com.mycompany.myapp.dto;

import com.sun.istack.NotNull;

public class ExcelZoneDTO {
    
    @NotNull
    private String value;
    
    private Long id;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ExcelZoneDTO(String value, Long id) {
        super();
        this.value = value;
        this.id = id;
    }

    public ExcelZoneDTO() {
        // TODO Auto-generated constructor stub
    }
    
}
