package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
@Data
@NoArgsConstructor
public class ThresholdsDTO extends BaseResponseDTO {
    @Min(value = 0)
    @Max(value = 1)
    private Boolean percentage;

    //Applies to an increase : 0
    //Applies to an descrease : 1
    //Applies to an increase ,Applies to an descrease : 2
    @Min(value = 0)
    @Max(value = 2)
    private Integer thresholdType;

    private String thresholdTypeName;

    @NotNull(groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    @Max(value = Long.MAX_VALUE, groups = {Create.class, Update.class})
    private Long value;

    @Size(max = 80)
    @NotBlank(groups = {Create.class, Update.class})
    private String externalId;

    private Integer domainId;

    private Boolean forBreakRating;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.THRESHOLD;

    @JsonView(Tree.class)
    private String icon = Resources.THRESHOLD;

    private Integer baseRate;

    private Integer unitPrecision;

    public interface Create {
    }

    public interface Update {
    }
}
