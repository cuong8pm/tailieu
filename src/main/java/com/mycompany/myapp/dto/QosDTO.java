package com.mycompany.myapp.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.MonitorKeyDTO.Create;
import com.mycompany.myapp.dto.MonitorKeyDTO.Update;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QosDTO extends BaseResponseDTO{
    private static final long MAX_VALUE = 999_999_999;

    @NotNull(groups = {Create.class, Update.class})
    private String qci;

    @NotNull(groups = {Create.class, Update.class})
    @Max(value = MAX_VALUE, groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    private Integer mbrul;

    @NotNull(groups = {Create.class, Update.class})
    @Max(value = MAX_VALUE, groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    private Integer mbrdl;

    @NotNull(groups = {Create.class, Update.class})
    @Max(value = MAX_VALUE, groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    private Integer gbrul;

    @Max(value = MAX_VALUE, groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    @NotNull(groups = {Create.class, Update.class})
    private Integer gbrdl;

    private Integer domainId;
    
    @JsonView(Tree.class)
    private String icon = Resources.QOS;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
