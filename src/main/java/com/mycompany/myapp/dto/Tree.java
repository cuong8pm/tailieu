package com.mycompany.myapp.dto;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;

import com.fasterxml.jackson.annotation.JsonView;

@JsonInclude(Include.NON_NULL)
@EqualsAndHashCode(callSuper = true)
public abstract class Tree extends BaseResponseDTO implements Moveable {

    public abstract Collection<Tree> getTemplates();

    public abstract void setTemplates(Collection<Tree> templates);

    @JsonIgnore
    public abstract Long getParentId();

    public abstract void setParentId(Long parentId);

    public abstract String getTreeType();

    public abstract void setTreeType(String treeType);

    public abstract Integer getCategoryLevel();

    public abstract void setCategoryLevel(Integer categoryLevel);

    public abstract String getType();

    public void setType(String type) {

    }

    @JsonView(Tree.class)
    public Integer getComponentType() {
        return null;
    }

    public void setComponentType(Integer componentType) {

    }
}
