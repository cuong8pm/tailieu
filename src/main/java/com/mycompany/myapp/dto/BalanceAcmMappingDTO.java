package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Collections;

@Data
public class BalanceAcmMappingDTO extends Tree {

    private Integer domainId;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long categoryId;

    @Size(max = 255, groups = {Create.class, Update.class})
    private String description;

    private Integer posIndex;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long fromBalanceId;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long toBalanceId;

    private BalancesDTO toBalance;

    private BalancesDTO fromBalance;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.BALANCE_ACM_MAPPINGS;

    @JsonView(Tree.class)
    private String icon = Resources.BALANCE_ACM_MAPPINGS;
    
    private Integer categoryLevel;
    private String categoryName;

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getTreeType() {
        return this.treeType;
    }

    @Override
    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    @Override
    public Integer getCategoryLevel() {
        return this.categoryLevel;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
