package com.mycompany.myapp.dto.block;

public interface RateTableProjection {
    Long getId();

    String getName();

    Integer getPosIndex();

    String getDescription();

    Integer getComponentType();
}
