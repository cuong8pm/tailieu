package com.mycompany.myapp.dto.block;

import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlockResponse {
    private Integer componentType;
    private List<RateTableDTO> rateTable;
}
