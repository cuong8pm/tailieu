package com.mycompany.myapp.dto.block;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.block.validator.AffectedObjectTypeConstraint;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@AffectedObjectTypeConstraint(fieldName = "affectedObjectType", dependFieldName = "affectedObject", groups = {BlockDTO.Create.class, BlockDTO.Update.class})
@AffectedObjectTypeConstraint(fieldName = "affectedObjectType", dependFieldName = "isCreateNewObject", groups = {BlockDTO.Create.class, BlockDTO.Update.class})
@AffectedObjectTypeConstraint(fieldName = "affectedObjectType", dependFieldName = "isConditionalMultibalCreating", groups = {BlockDTO.Create.class, BlockDTO.Update.class})
@AffectedObjectTypeConstraint(fieldName = "affectedObjectType", dependFieldName = "isSetBalIfNotExist", groups = {BlockDTO.Create.class, BlockDTO.Update.class})
@AffectedObjectTypeConstraint(fieldName = "affectedObjectType", dependFieldName = "affectedBalMode", groups = {BlockDTO.Create.class, BlockDTO.Update.class})
public class BlockDTO extends TreeClone implements Moveable {

    @NotNull(groups = {Create.class, Update.class})
    private Long blockType;

    @NotNull(groups = {Create.class, Update.class})
    private Long categoryId;

    @NotNull(groups = {Create.class, Update.class})
    private Boolean isConditionalMultibalCreating;

    @NotNull(groups = {Create.class, Update.class})
    private Boolean isCreateNewObject;

    @NotNull(groups = {Create.class, Update.class})
    private Integer affectedObjectType;

    private Long affectedObject;

    private String affectedField;

    @JsonView(Tree.class)
    @Length(max = 255, groups = {BlockDTO.Create.class, BlockDTO.Update.class})
    private String description;

    private Long blockFilterId;

    @NotNull(groups = {Create.class, Update.class})
    private Boolean isSetBalIfNotExist;

    private Integer affectedBalMode;

    private Integer posIndex;

    private Integer domainId;

    private List<BlockRateTableMapDTO> rateTableItems;

    private List<BlockRateTableMapDTO> oldRateTableItems;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.BLOCK;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    private String categoryName;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    private Set<Integer> typee;

    private Integer componentType;

    private String affectedObjectName;

    private String blockFilterName;

    @NotNull(groups = {Create.class, Update.class})
    private Long affectedObjectFieldNameId;

}
