package com.mycompany.myapp.dto.block;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BlockRateTableMapDTO extends TreeClone {

    @NotNull(groups = {BlockRateTableMapDTO.Create.class, BlockRateTableMapDTO.Update.class})
    private Long blockId;

    @NotNull(groups = {BlockRateTableMapDTO.Create.class, BlockRateTableMapDTO.Update.class})
    private Long rateTableId;

    @NotNull(groups = {BlockRateTableMapDTO.Create.class, BlockRateTableMapDTO.Update.class})
    @Positive(groups = {BlockRateTableMapDTO.Create.class, BlockRateTableMapDTO.Update.class})
    private Integer componentType;

    @NotNull(groups = {BlockRateTableMapDTO.Create.class, BlockRateTableMapDTO.Update.class})
    @Positive(groups = {BlockRateTableMapDTO.Create.class, BlockRateTableMapDTO.Update.class})
    private Integer posIndex;

    private Integer domainId;

    private Long parentId;

    private Long childId;

    private ActionType actionType;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.BLOCK;
    
    public interface Create {
    }

    public interface Update {
    }

    private String description;

    @Override
    public Integer getCategoryLevel() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCategoryLevel(Integer categoryLevel) {
        // TODO Auto-generated method stub
        
    }

}
