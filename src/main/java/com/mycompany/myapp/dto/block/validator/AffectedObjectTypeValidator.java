package com.mycompany.myapp.dto.block.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.mycompany.myapp.domain.constant.AffectedObjectType;


public class AffectedObjectTypeValidator implements ConstraintValidator<AffectedObjectTypeConstraint, Object>{

    private String fieldName;
    private String dependFieldName;
    private static final String AFFECTED_OBJECT_TYPE = "affectedObjectType";
    private static final String AFFECTED_VALUE = "affectedObject";
    private static final String IS_CREATE_NEW_OBJECT = "isCreateNewObject";
    private static final String IS_CONDITIONAL_MULTIBAL_CREATING = "isConditionalMultibalCreating";
    private static final String IS_SET_BAL_IF_NOT_EXIST = "isSetBalIfNotExist";
    private static final String BLOCK_MODE = "affectedBalMode";
    private static final String MUST_BE_NOT_NULL = "must_be_not_null";
    private static final String MUST_BE_NULL = "must_be_null";
    private static final String AFFECTED_OBJECT_NAME = "affectedObjectName";
    private static final String MUST_BE_UNCHECK = "must_be_uncheck";

    public void initialize(AffectedObjectTypeConstraint constraint) {
        this.fieldName = constraint.fieldName();
        this.dependFieldName = constraint.dependFieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if(value == null) {
            return true;
        }

        BeanWrapperImpl obj = new BeanWrapperImpl(value);
        Object fieldValue = obj.getPropertyValue(this.fieldName);
        boolean isValid = true;
        if( fieldValue != null) {
            Object dependFieldValue = obj.getPropertyValue(dependFieldName);
            Long typeLong = Long.valueOf(fieldValue.toString());
            int typeInt = Math.toIntExact(typeLong);
            switch (AffectedObjectType.of(typeInt)) {
            case CUST:
                isValid = validateOther(dependFieldName, dependFieldValue, context);
                break;
            case SUB:
                isValid = validateOther(dependFieldName, dependFieldValue, context);
                break;
            case BALANCE:
                isValid = validateBalance(dependFieldName, dependFieldValue, context);
                break;
            case ACM_BALANCE:
                isValid = validateAcmBalance(dependFieldName, dependFieldValue, context);
                break;
            case CHARACTERISTIC:
                isValid = validateCharacteristic(dependFieldName, dependFieldValue, context);
                break;
            case GROUP:
                isValid = validateOther(dependFieldName, dependFieldValue, context);
                break;
            case OCSMSG:
                isValid = validateOther(dependFieldName, dependFieldValue, context);
                break;
            case OFFER:
                isValid = validateOffer(dependFieldName, dependFieldValue, context);
                break;
            default:
                break;
            }
        }
        return isValid;
    }

    private void customMessageForValidation(ConstraintValidatorContext constraintContext, String message, String fieldName) {
        // build new violation message and add it
        constraintContext.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldName).addConstraintViolation();
    }

    private boolean validateBalance(String fieldName, Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        switch(fieldName) {
            case AFFECTED_VALUE:
                if(value == null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NOT_NULL, AFFECTED_OBJECT_NAME);
                }
                break;
            case BLOCK_MODE:
                if(value == null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NOT_NULL, fieldName);
                }
                break;
        }
        return isValid;
    }

    private boolean validateAcmBalance(String fieldName, Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        switch(fieldName) {
            case AFFECTED_VALUE:
                if(value == null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NOT_NULL, AFFECTED_OBJECT_NAME);
                }
                break;
            case BLOCK_MODE:
                if(value != null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NULL, fieldName);
                }
                break;
        }
        return isValid;
    }

    private boolean validateCharacteristic(String fieldName, Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        switch(fieldName) {
            case AFFECTED_VALUE:
                if(value == null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NOT_NULL, AFFECTED_OBJECT_NAME);
                }
                break;
            case BLOCK_MODE:
                if(value != null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NULL, fieldName);
                }
                break;
            case IS_SET_BAL_IF_NOT_EXIST:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
            case IS_CONDITIONAL_MULTIBAL_CREATING:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
        }
        return isValid;
    }

    private boolean validateOffer(String fieldName, Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        switch(fieldName) {
            case AFFECTED_VALUE:
                if(value == null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NOT_NULL, AFFECTED_OBJECT_NAME);
                }
                break;
            case IS_CREATE_NEW_OBJECT:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
            case BLOCK_MODE:
                if(value != null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NULL, fieldName);
                }
                break;
            case IS_SET_BAL_IF_NOT_EXIST:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
            case IS_CONDITIONAL_MULTIBAL_CREATING:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
        }
        return isValid;
    }

    private boolean validateOther(String fieldName, Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        switch(fieldName) {
            case AFFECTED_VALUE:
                if(value != null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NULL, AFFECTED_OBJECT_NAME);
                }
                break;
            case IS_CREATE_NEW_OBJECT:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
            case BLOCK_MODE:
                if(value != null) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_NULL, fieldName);
                }
                break;
            case IS_SET_BAL_IF_NOT_EXIST:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
            case IS_CONDITIONAL_MULTIBAL_CREATING:
                if((boolean) value != false) {
                    isValid = false;
                    customMessageForValidation(context, MUST_BE_UNCHECK, fieldName);
                }
                break;
        }
        return isValid;
    }
}
