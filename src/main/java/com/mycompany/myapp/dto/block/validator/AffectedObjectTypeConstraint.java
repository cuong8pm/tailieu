package com.mycompany.myapp.dto.block.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = AffectedObjectTypeValidator.class)
@Target({ TYPE, ANNOTATION_TYPE })
@Repeatable(AffectedObjectTypeConstraint.List.class)
@Retention(RUNTIME)
public @interface AffectedObjectTypeConstraint {

    String fieldName();
    String dependFieldName();

    String message() default "block_invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        AffectedObjectTypeConstraint[] value();
    }
}
