package com.mycompany.myapp.dto;

import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.EqualsAndHashCode;

@JsonInclude(Include.NON_NULL)
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class TreeClone extends Tree {

    @JsonView(Tree.class)
    private Boolean hasChild;

    @JsonView(Tree.class)
    private Collection<Tree> templates;
}
