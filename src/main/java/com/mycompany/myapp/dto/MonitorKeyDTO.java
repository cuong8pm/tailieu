package com.mycompany.myapp.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class MonitorKeyDTO extends BaseResponseDTO{
    @Max(value = 999_999_999, message = "length_must_be_between_0_and_9",groups = {Create.class, Update.class})
    @Min(value = 0, groups = {Create.class, Update.class})
    @NotNull(groups = {Create.class, Update.class})
    private Long monitorKey;

    @Length(max = 255, groups = {Create.class, Update.class})
    private String description;
    
    @JsonView(Tree.class)
    private String icon = Resources.MONITOR_KEY;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
