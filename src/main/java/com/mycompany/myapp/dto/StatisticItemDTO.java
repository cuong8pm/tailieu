package com.mycompany.myapp.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import org.hibernate.validator.constraints.Length;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.UnitTypeDTO.Create;
import com.mycompany.myapp.dto.UnitTypeDTO.Move;
import com.mycompany.myapp.dto.UnitTypeDTO.Update;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Collection;
import java.util.Collections;

@Data
@EqualsAndHashCode(callSuper = true)
public class StatisticItemDTO extends Tree {

    @JsonView(Tree.class)
    @Length(max = 512)
    private String description;

    private Integer domainId;

    @NotNull(groups = {Create.class, Update.class})
    @JsonView(Tree.class)
    private Long categoryId;

    @NotNull(groups = { Move.class, Update.class })
    @Min(value = 0, groups = { Move.class, Update.class })
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;

    private String categoryName;

    private Integer categoryLevel;

    @JsonView(Tree.class)
    private String type = Resources.STATISTIC;
    
    @JsonView(Tree.class)
    private String icon = Resources.STATISTIC;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {

    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public String getTreeType() {
        return treeType;
    }

    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getCategoryLevel() {
        return categoryLevel;
    }

    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

}
