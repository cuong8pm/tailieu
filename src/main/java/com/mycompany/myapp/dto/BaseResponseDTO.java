package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.dto.event.EventDTO;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.dto.offer.common.OfferCommonDTO;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

@EqualsAndHashCode
public class BaseResponseDTO {
    @JsonView({ Tree.class, UnitTypeDTO.BalTypeView.class })
    @NotNull(groups = { EventDTO.Create.class })
    @Positive(groups = { EventDTO.Create.class })
    private Long id;

    @NotBlank(
        groups = {
            EventPolicyDTO.Create.class,
            EventPolicyDTO.Update.class,
            FilterDTO.Create.class,
            FilterDTO.Update.class,
            FunctionParamDTO.Create.class,
            FunctionParamDTO.Update.class,
            FunctionDTO.Create.class,
            FunctionDTO.Update.class,
            BuilderTypeItemDTO.Create.class,
            BuilderTypeItemDTO.Update.class,
            BuilderTypeDTO.Create.class,
            BuilderTypeDTO.Update.class,
            FunctionParamDTO.Create.class,
            FunctionParamDTO.Update.class,
            LanguageDTO.Create.class,
            LanguageDTO.Update.class,
            RouterTypeDTO.Create.class,
            RouterTypeDTO.Update.class,
            UnitTypeDTO.Create.class,
            UnitTypeDTO.Update.class,
            CategoryDTO.Create.class,
            CategoryDTO.Update.class,
            ParameterDTO.Create.class,
            ParameterDTO.Update.class,
            ReserveInfoDTO.Create.class,
            ReserveInfoDTO.Update.class,
            ZoneMapDTO.Create.class,
            ZoneMapDTO.Update.class,
            ZoneDTO.Create.class,
            ZoneDTO.Update.class,
            GeoHomeZoneDTO.Create.class,
            GeoHomeZoneDTO.Update.class,
            ThresholdsDTO.Create.class,
            ThresholdsDTO.Update.class,
            OcsServiceDTO.Create.class,
            OcsServiceDTO.Update.class,
            StatisticItemDTO.Create.class,
            StatisticItemDTO.Update.class,
            PccRuleDTO.Create.class,
            PccRuleDTO.Update.class,
            StateTypeDTO.Create.class,
            StateTypeDTO.Update.class,
            StateGroupsDTO.Create.class,
            StateGroupsDTO.Update.class,
            MonitorKeyDTO.Create.class,
            MonitorKeyDTO.Update.class,
            BalancesDTO.Create.class,
            BalancesDTO.Update.class,
            BalanceAcmMappingDTO.Create.class,
            BalanceAcmMappingDTO.Update.class,
            AccountBalanceMappingDTO.Create.class,
            AccountBalanceMappingDTO.Update.class,
            BillingCycleTypeDTO.Create.class,
            BillingCycleTypeDTO.Update.class,
            QosDTO.Create.class,
            QosDTO.Update.class,
            NormalizerDTO.Create.class,
            NormalizerDTO.Update.class,
            RateTableDTO.Create.class,
            RateTableDTO.Update.class,
            PepProfileDTO.Create.class,
            PepProfileDTO.Update.class,
            RatingFilterDTO.Create.class,
            RatingFilterDTO.Update.class,
            PriceComponentDTO.Create.class,
            PriceComponentDTO.Update.class,
            BlockDTO.Create.class,
            BlockDTO.Update.class,
            ActionTypeDTO.Create.class,
            ActionTypeDTO.Update.class,
            EventDTO.Create.class,
            EventDTO.Update.class,
            ActionDTO.Create.class,
            ActionDTO.Update.class,
            OfferCommonDTO.Create.class,
            OfferCommonDTO.Update.class,
            OfferCommonDTO.Validate.class,
            VerificationDTO.Create.class,
            VerificationDTO.Update.class,
            ConditionDTO.Create.class,
            ConditionDTO.Update.class,
            RouterDTO.Create.class,
            RouterDTO.Update.class,
            BuilderDTO.Create.class,
            BuilderDTO.Update.class,
            CreateEventPolicyDTO.Validator.class
        },
        message = "must_not_be_blank"
    )
    @Length(
        max = 200,
        groups = {
            ParameterDTO.Create.class,
            ParameterDTO.Update.class,
            GeoHomeZoneDTO.Create.class,
            GeoHomeZoneDTO.Update.class,
            ZoneMapDTO.Create.class,
            ZoneMapDTO.Update.class,
            ParameterDTO.Create.class,
            ParameterDTO.Update.class,
            StateTypeDTO.Create.class,
            StateTypeDTO.Update.class,
            MonitorKeyDTO.Create.class,
            MonitorKeyDTO.Update.class,
            ThresholdsDTO.Create.class,
            ThresholdsDTO.Update.class,
        }
    )
    @Length(
        max = 60,
        groups = { PccRuleDTO.Create.class, PccRuleDTO.Update.class, BillingCycleTypeDTO.Create.class, BillingCycleTypeDTO.Update.class }
    )
    @Length(max = 128, groups = { StatisticItemDTO.Create.class, StatisticItemDTO.Update.class })
    @Length(
        max = 255,
        groups = {
            EventPolicyDTO.Create.class,
            EventPolicyDTO.Update.class,
            FilterDTO.Create.class,
            FilterDTO.Update.class,
            FunctionParamDTO.Create.class,
            FunctionParamDTO.Update.class,
            FunctionDTO.Create.class,
            FunctionDTO.Update.class,
            DataSourceDTO.Create.class,
            DataSourceDTO.Update.class,
            BuilderTypeItemDTO.Create.class,
            BuilderTypeItemDTO.Update.class,
            FieldIdOcsDTO.Create.class,
            FieldIdOcsDTO.Update.class,
            FieldDTO.Create.class,
            FieldDTO.Update.class,
            EventPolicyDTO.Create.class,
            EventPolicyDTO.Update.class,
            EventObjectDTO.Create.class,
            EventObjectDTO.Update.class,
            ConditionDTO.Create.class,
            ConditionDTO.Update.class,
            BuilderTypeDTO.Create.class,
            BuilderTypeDTO.Update.class,
            BuilderDTO.Create.class,
            BuilderDTO.Update.class,
            FilterDTO.Create.class,
            FilterDTO.Update.class,
            FunctionDTO.Create.class,
            FunctionDTO.Update.class,
            FunctionParamDTO.Create.class,
            FunctionParamDTO.Update.class,
            LanguageDTO.Create.class,
            LanguageDTO.Update.class,
            NotifyDTO.Create.class,
            NotifyDTO.Update.class,
            RouterDTO.Create.class,
            RouterDTO.Update.class,
            RouterTypeDTO.Create.class,
            RouterTypeDTO.Update.class,
            StateGroupsDTO.Create.class,
            StateGroupsDTO.Update.class,
            NormalizerDTO.Create.class,
            NormalizerDTO.Update.class,
            BalanceAcmMappingDTO.Create.class,
            BalanceAcmMappingDTO.Update.class,
            AccountBalanceMappingDTO.Create.class,
            AccountBalanceMappingDTO.Update.class,
            RatingFilterDTO.Create.class,
            RatingFilterDTO.Update.class,
            BlockDTO.Create.class,
            BlockDTO.Update.class,
            PriceComponentDTO.Create.class,
            PriceComponentDTO.Update.class,
            ActionTypeDTO.Create.class,
            ActionTypeDTO.Update.class,
            PriceComponentDTO.Create.class,
            PriceComponentDTO.Update.class,
            ActionDTO.Create.class,
            ActionDTO.Update.class,
            OfferCommonDTO.Create.class,
            OfferCommonDTO.Update.class,
            EventDTO.Create.class,
            EventDTO.Update.class,
            ActionDTO.Create.class,
            ActionDTO.Update.class,
            QosDTO.Create.class,
            QosDTO.Update.class,
            VerificationDTO.Create.class,
            VerificationDTO.Update.class,
            CreateEventPolicyDTO.Validator.class
        }
    )
    @Length(max = 30, groups = { PepProfileDTO.Create.class, PepProfileDTO.Update.class })
    @JsonView({ Tree.class, UnitTypeDTO.BalTypeView.class })
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseResponseDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public BaseResponseDTO() {}
}
