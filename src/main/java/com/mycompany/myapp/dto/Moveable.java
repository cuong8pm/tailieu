package com.mycompany.myapp.dto;

public interface Moveable {
    
    Long getId();
    void setId(Long id);
    
    Integer getPosIndex();
    void setPosIndex(Integer posIndex);
}
