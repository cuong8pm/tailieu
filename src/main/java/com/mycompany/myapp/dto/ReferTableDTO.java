package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.normailizer.ActionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReferTableDTO {

    @JsonProperty("referId")
    private Long id;

    @JsonIgnore
    private ReferType referType;

    private String name;

    @JsonProperty("id")
    private Integer value;

    @JsonIgnore
    private String remark;

    private Boolean isUsed;
    
    private ActionType actionType;
}
