package com.mycompany.myapp.dto;

import java.util.Collection;
import java.util.Collections;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.Constants;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class StateTypeDTO extends Tree implements Moveable {

    @NotNull(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    private Integer stateType;

    @NotNull(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    @Positive(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    private Long stateGroupId;

    @Size(max = 200, groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    private String description;

    @Pattern(regexp = Constants.KEY_REGEX, message = Constants.KEY_REGEX_MESSAGE_FAILER, groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    @NotNull(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    private String stateMask;

    @Pattern(regexp = Constants.KEY_REGEX, message = Constants.KEY_REGEX_MESSAGE_FAILER, groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    @NotNull(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    private String filterValue;

    private Boolean status;

    @NotNull(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    @Positive(groups = {StateTypeDTO.Create.class, StateTypeDTO.Update.class})
    private Long categoryId;

    private Integer stateIndex;

    private Boolean stateStatus;

    private Integer domainId;

    private String categoryName;

    private String stateGroupName;

    private String stateTypeName;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.STATETYPE;
    
    @JsonView(Tree.class)
    private String icon = Resources.STATETYPE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    @Override
    public Collection<Tree> getTemplates() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
        // Do nothing
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long categoryId) {
        this.categoryId = categoryId;

    }

}
