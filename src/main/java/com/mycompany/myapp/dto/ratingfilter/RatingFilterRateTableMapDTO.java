package com.mycompany.myapp.dto.ratingfilter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class RatingFilterRateTableMapDTO {
    private Long id;

    @NotNull(groups = {RatingFilterRateTableMapDTO.Create.class, RatingFilterRateTableMapDTO.Update.class})
    @Positive(groups = {RatingFilterRateTableMapDTO.Create.class, RatingFilterRateTableMapDTO.Update.class})
    private Long ratingFilterId;

    @NotNull(groups = {RatingFilterRateTableMapDTO.Create.class, RatingFilterRateTableMapDTO.Update.class})
    @Positive(groups = {RatingFilterRateTableMapDTO.Create.class, RatingFilterRateTableMapDTO.Update.class})
    private Long rateTableId;

    @Positive(groups = {RatingFilterRateTableMapDTO.Create.class, RatingFilterRateTableMapDTO.Update.class})
    private Integer index;

    private Integer domainId;

    public interface Create {
    }

    public interface Update {
    }
}
