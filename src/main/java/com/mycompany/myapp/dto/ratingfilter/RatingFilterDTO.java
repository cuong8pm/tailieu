package com.mycompany.myapp.dto.ratingfilter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.StateGroupsDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;

import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RatingFilterDTO extends TreeClone implements Moveable {

    @NotNull(groups = {RatingFilterDTO.Create.class, RatingFilterDTO.Update.class}, message = "must_not_be_blank")
    @Positive(groups = {RatingFilterDTO.Create.class, RatingFilterDTO.Update.class})
    private Integer ratingFilterType;

    @Size(max = 255, groups = {StateGroupsDTO.Create.class, StateGroupsDTO.Update.class})
    private String description;

    private Integer posIndex;

    private Integer domainId;

    private String categoryName;

    @NotNull(groups = {RatingFilterDTO.Create.class, RatingFilterDTO.Update.class})
    @Positive(groups = {RatingFilterDTO.Create.class, RatingFilterDTO.Update.class})
    @JsonView(Tree.class)
    private Long categoryId;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    private List<RateTableProjection> rateTables;

    private List<RateTableRequest> rateTableInRatingFilter= new ArrayList<>();

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.RATING_FILTER;


    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
