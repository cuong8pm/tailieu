package com.mycompany.myapp.dto.ratingfilter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.dto.normailizer.ActionType;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class RateTableRequest {

    @NotNull(groups = {RateTableDTO.Create.class, RateTableDTO.Update.class})
    @Positive(groups = {RateTableDTO.Create.class, RateTableDTO.Update.class})
    private Long id;

    @NotNull(groups = {RateTableDTO.Create.class, RateTableDTO.Update.class})
    @Positive(groups = {RateTableDTO.Create.class, RateTableDTO.Update.class})
    @JsonProperty("posIndex")
    private Integer index;

    private ActionType actionType;

    private Long ratingFilterRateTableMapId;

    public interface Create {
    }

    public interface Update {
    }
}
