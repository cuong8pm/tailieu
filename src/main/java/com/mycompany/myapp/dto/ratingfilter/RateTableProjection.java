package com.mycompany.myapp.dto.ratingfilter;

public interface RateTableProjection {

    Long getId();

    String getName();

    Integer getPosIndex();

    String getDescription();

    Long getRatingFilterRateTableMapId();
}
