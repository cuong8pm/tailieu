package com.mycompany.myapp.dto;

import org.apache.commons.lang3.StringUtils;


public class SearchCriteria {

    private String name;
    private String description;
    private Integer domainId;
    private Long parentId;
    private boolean searchAll;

    public SearchCriteria(String name, String description, Long parentId) {
        this.name = StringUtils.isEmpty(name) ? null : name;
        this.description = StringUtils.isEmpty(description) ? null : description;
        this.parentId = parentId;
    }

    public SearchCriteria(String name, String description) {
        this.name = StringUtils.isEmpty(name) ? null : name;
        this.description = StringUtils.isEmpty(description) ? null : description;
    }

    public SearchCriteria() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public boolean isSearchAll() {
        return searchAll;
    }

    public void setSearchAll(boolean searchAll) {
        this.searchAll = searchAll;
    }

}
