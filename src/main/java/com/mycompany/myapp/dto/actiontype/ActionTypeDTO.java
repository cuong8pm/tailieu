package com.mycompany.myapp.dto.actiontype;

import java.util.Collection;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Moveable;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import com.mycompany.myapp.web.rest.errors.Resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@TrimString(fieldNames = { "name", "description" })
public class ActionTypeDTO extends TreeClone implements Moveable {
    private String icon;

    @JsonView(Tree.class)
    @Size(max = 255, groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private String description;

    @NotNull(groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private boolean forProvisioning;

    @NotNull(groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private Long categoryId;

    private String categoryName;

    private Integer domainId;

    private List<EventActionTypeMapDTO> eventActionTypeMapDTOs;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.ACTION_TYPE;

    @JsonView(Tree.class)
    private Integer categoryLevel;

    @JsonView(Tree.class)
    private Collection<Tree> templates;

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }
}
