package com.mycompany.myapp.dto.actiontype;

public interface EventOfActionTypeDTO {

    Integer getPosIndex();

    Long getId();

    String getName();

    String getDescription();
}
