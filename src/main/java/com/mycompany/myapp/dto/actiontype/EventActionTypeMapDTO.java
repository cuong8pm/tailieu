package com.mycompany.myapp.dto.actiontype;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.dto.normailizer.ActionType;

import lombok.Data;

@Data
public class EventActionTypeMapDTO {

    @JsonProperty("mappingId")
    private Long id;

    @JsonProperty("id")
    private Long actionTypeId;

    @JsonProperty("name")
    private String actionTypeName;

    private Long eventId;

    private String eventName;

    private String Description;

    private Integer posIndex;

    private Integer domainId;

    private ActionType actionType;
}
