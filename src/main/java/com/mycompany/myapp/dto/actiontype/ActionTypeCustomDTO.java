package com.mycompany.myapp.dto.actiontype;

import java.math.BigInteger;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.mycompany.myapp.utils.MaxLong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActionTypeCustomDTO {

    @Positive(groups = { ActionTypeDTO.Create.class })
    @NotNull(groups = { ActionTypeDTO.Create.class })
    @MaxLong(groups = { ActionTypeDTO.Create.class })
    private BigInteger id;

    @NotBlank(groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    @Length(max = 255, groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private String name;

    private String icon;

    @Size(max = 255, groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private String description;

    @NotNull(groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private boolean forProvisioning;

    @NotNull(groups = { ActionTypeDTO.Create.class, ActionTypeDTO.Update.class })
    private Long categoryId;

    private Integer domainId;

    private Integer posIndex;
}
