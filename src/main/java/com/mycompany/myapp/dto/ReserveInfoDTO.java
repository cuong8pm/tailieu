package com.mycompany.myapp.dto;

import java.util.Collection;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.web.rest.errors.Resources;
import com.mycompany.myapp.web.rest.errors.Resources.TreeType;

public class ReserveInfoDTO extends Tree implements Moveable {

    @NotNull(groups = {Create.class, Update.class})
    @Min(groups = {Create.class, Update.class}, value = 0)
    private Integer maxReserveValue;

    @NotNull(groups = {Create.class, Update.class})
    @Min(groups = {Create.class, Update.class}, value = 0)
    private Integer minReserveValue;

    @Min(groups = {Create.class, Update.class}, value = 0)
    private Integer usageQuotaThreshold;
    private Boolean canRollback;

    private String description;

    @NotNull(groups = {Create.class, Update.class})
    @Positive(groups = {Create.class, Update.class})
    private Long categoryId;

    @JsonView(Tree.class)
    private Integer posIndex;

    @JsonView(Tree.class)
    private String treeType = TreeType.TEMPLTE;

    @JsonView(Tree.class)
    private String type = Resources.RESERVE_INFO;
    
    @JsonView(Tree.class)
    private String icon = Resources.RESERVE_INFO;
    
    private String categoryName;
    private Integer categoryLevel;

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryLevel(Integer categoryLevel) {
        this.categoryLevel = categoryLevel;
    }

    public Integer getCategoryLevel() {
        return categoryLevel;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public Integer getMaxReserveValue() {
        return maxReserveValue;
    }

    public void setMaxReserveValue(Integer maxReserveValue) {
        this.maxReserveValue = maxReserveValue;
    }

    public Integer getMinReserveValue() {
        return minReserveValue;
    }

    public void setMinReserveValue(Integer minReserveValue) {
        this.minReserveValue = minReserveValue;
    }

    public Integer getUsageQuotaThreshold() {
        return usageQuotaThreshold;
    }

    public void setUsageQuotaThreshold(Integer usageQuotaThreshold) {
        this.usageQuotaThreshold = usageQuotaThreshold;
    }

    public Boolean getCanRollback() {
        return canRollback;
    }

    public void setCanRollback(Boolean canRollback) {
        this.canRollback = canRollback;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    public Collection<Tree> getTemplates() {
//        throw new UnsupportedOperationException();
        return null;
    }

    @Override
    public void setTemplates(Collection<Tree> templates) {
//        throw new UnsupportedOperationException();
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public String getTreeType() {
        return this.treeType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    @Override
    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }
}
