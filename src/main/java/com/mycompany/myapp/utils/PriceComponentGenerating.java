package com.mycompany.myapp.utils;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PriceComponentGeneratingValidator.class)
@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
public @interface PriceComponentGenerating {

    String fieldName();

    String dependFieldName();

    String message() default "gennerating_type_must_be_not_null";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
