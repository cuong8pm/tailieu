package com.mycompany.myapp.utils;

import java.math.BigInteger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MaxLongValidator implements ConstraintValidator<MaxLong, BigInteger> {

    @Override
    public boolean isValid(BigInteger id, ConstraintValidatorContext context) {
        if (id == null) {
            return true;
        }
        BigInteger maxLong = BigInteger.valueOf(Long.MAX_VALUE);
        if (maxLong.compareTo(id) == 1) {
            return true;
        }
        return false;
    }

}
