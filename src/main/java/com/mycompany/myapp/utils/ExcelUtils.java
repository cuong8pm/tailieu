package com.mycompany.myapp.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javatuples.Quartet;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mycompany.myapp.domain.GeoNetZone;
import com.mycompany.myapp.domain.ZoneData;

public final class ExcelUtils {
    // i = 3 vi trong file dung de import data bat dau tu row thu 3
    public static final int ROW_INDEX = 3;

    public static final String SUCCESS_STATUS = "Pass";

    public static final String FAIL_STATUS = "Fail";

    public static final String STATUS = "Status";

    public static final String FORMAT_DATE_TIME = "uuuu-MM-dd-HHmmss";

    public static <T> Set<T> findDuplicates(Collection<T> collection) {

        Set<T> duplicates = new LinkedHashSet<>();
        Set<T> uniques = new HashSet<>();

        for (T t : collection) {
            if (!uniques.add(t)) {
                duplicates.add(t);
            }
        }

        return duplicates;
    }

    public static CellStyle setStyleDate(XSSFWorkbook workbook) {
        CellStyle styleDate = workbook.createCellStyle();
        CreationHelper creationHelper = workbook.getCreationHelper();
        styleDate.setDataFormat(creationHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm:ss"));
        styleDate.setAlignment(CellStyle.ALIGN_CENTER);
        return styleDate;
    }

    public static Sheet createSheetForm(String sheetName, String[] headerNames, String titleName,
            HSSFWorkbook workbook) {
        Sheet sheet = workbook.createSheet(sheetName);
        int indexTitle = sheet.addMergedRegion(CellRangeAddress.valueOf("A1:C1"));
        sheet.setColumnWidth(2, 25 * 200);
        sheet.setColumnWidth(1, 25 * 200);
        Row titleRow = sheet.createRow(0);
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.BROWN.getIndex());
        styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue(titleName);
        titleCell.setCellStyle(styleHeader);
        Row headerRow = sheet.createRow(2);
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        for (int i = 0; i < headerNames.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headerNames[i]);
            cell.setCellStyle(headerCellStyle);
        }
        return sheet;
    }

    public static CellStyle setAlignMiddle(XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        return cellStyle;
    }

    public static Boolean isTemplateFormXlsx(MultipartFile readExcelDataFile, String[] HEADERs, String TITLE)
            throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        XSSFRow headerRow = worksheet.getRow(2);
        XSSFRow titleRow = worksheet.getRow(0);
        Boolean isTemplateForm = false;
        headerRow.getPhysicalNumberOfCells();
        if (TITLE.equals(titleRow.getCell(0).getStringCellValue())) {
            for (int i = 0; i < HEADERs.length; i++) {
                if (!HEADERs[i].equals(headerRow.getCell(i).getStringCellValue())) {
                    return false;
                }
            }
            isTemplateForm = true;
        }
        workbook.close();
        return isTemplateForm;
    }

    public static Boolean isTemplateFormXls(MultipartFile readExcelDataFile, String[] HEADERs, String TITLE)
            throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(readExcelDataFile.getInputStream());
        HSSFSheet worksheet = workbook.getSheetAt(0);
        HSSFRow headerRow = worksheet.getRow(2);
        HSSFRow titleRow = worksheet.getRow(0);
        Boolean isTemplateForm = false;
        if (TITLE.equals(titleRow.getCell(0).getStringCellValue())) {
            for (int i = 0; i < HEADERs.length; i++) {
                if (!HEADERs[i].equals(headerRow.getCell(i).getStringCellValue())) {
                    return false;
                }
            }
            isTemplateForm = true;
        }
        workbook.close();
        return isTemplateForm;
    }

    public static class ExcelUtilsZoneData {
        // Zone Data
        public static ByteArrayInputStream exportZoneData(List<ZoneData> lstZoneData, String SHEET, String[] HEADERs,
                String TITLE) throws IOException {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Sheet sheet = ExcelUtils.createSheetForm(SHEET, HEADERs, TITLE, workbook);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            int rowIdx = ExcelUtils.ROW_INDEX;
            for (ZoneData zoneData : lstZoneData) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(zoneData.getId());
                row.getCell(0).setCellStyle(headerCellStyle);
                row.createCell(1).setCellValue(zoneData.getZoneDataValue());
                row.getCell(1).setCellStyle(headerCellStyle);

                DateTimeFormatter dateSqlFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime time = LocalDateTime.parse(zoneData.getTempUpdateDate(), dateSqlFormatter);

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/uuuu HH:mm:ss");
                String updateTime = dtf.format(time);
                row.createCell(2).setCellValue(updateTime);
                row.getCell(2).setCellStyle(headerCellStyle);
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }

        // import zonedata tu file XLSX
        public static Quartet<Boolean, InputStream, Integer, Integer> exportDataOfImportFileXlsx(
                MultipartFile readExcelDataFile, List<String> lstFailRecord, List<String> lstSuccessRecord,
                List<String> lstZoneDataValueDuplicateOfFile, List<String> lstRecordOfFile) throws IOException {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Boolean isSuccess = false;
            Integer numberOfValidRecord = 0;
            XSSFWorkbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
            XSSFSheet worksheet = workbook.getSheetAt(0);
            XSSFRow headerRow = worksheet.getRow(2);
            Cell statusCell = headerRow.createCell(3);
            statusCell.setCellValue(STATUS);
            statusCell.setCellStyle(setAlignMiddle(workbook));
            List<Integer> lstRowDulicateValue = new ArrayList<>();
            List<String> lstZoneDataValueDuplicateOfFileCopy = new ArrayList<>();
            // Truong hop import tat ca ban ghi dung
            if (CollectionUtils.isEmpty(lstFailRecord) && CollectionUtils.isEmpty(lstZoneDataValueDuplicateOfFile)) {
                isSuccess = true;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim())) {
                            String valueOfCell = row.getCell(1).getStringCellValue().trim();
                            row.getCell(1).setCellValue(valueOfCell);
                            row.createCell(3).setCellValue(SUCCESS_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                            numberOfValidRecord++;
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            row.createCell(3).setCellValue(SUCCESS_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                            numberOfValidRecord++;
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import tat ca ban ghi sai
            } else if (CollectionUtils.isEmpty(lstSuccessRecord) && !CollectionUtils.isEmpty(lstFailRecord)) {
                isSuccess = false;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim())) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import co ban ghi dung va ban ghi sai
            } else {
                isSuccess = true;

                lstZoneDataValueDuplicateOfFileCopy.addAll(lstZoneDataValueDuplicateOfFile);
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                            String valueOfCell = row.getCell(1).getStringCellValue().trim();
                            if (lstSuccessRecord.contains(valueOfCell)
                                    && lstZoneDataValueDuplicateOfFile.contains(valueOfCell)) {
                                lstRowDulicateValue.add(i);
                                lstZoneDataValueDuplicateOfFile.remove(valueOfCell);
                            } else {
                                if (lstFailRecord.contains(valueOfCell)
                                        || lstZoneDataValueDuplicateOfFileCopy
                                                .contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                }
                                if (lstSuccessRecord.contains(valueOfCell)
                                        && !lstZoneDataValueDuplicateOfFileCopy
                                                .contains(valueOfCell)) {
                                    row.getCell(1).setCellValue(valueOfCell);

                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                    numberOfValidRecord++;
                                }
                            }
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            if (lstSuccessRecord.contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))
                                    && lstZoneDataValueDuplicateOfFile
                                            .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))) {
                                lstRowDulicateValue.add(i);
                                lstZoneDataValueDuplicateOfFile
                                        .remove(String.valueOf((int) row.getCell(1).getNumericCellValue()));
                            } else {
                                if (lstFailRecord.contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))
                                        || lstZoneDataValueDuplicateOfFileCopy
                                                .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                }
                                if (lstSuccessRecord
                                        .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))
                                        && !lstZoneDataValueDuplicateOfFileCopy
                                                .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))) {
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                    numberOfValidRecord++;
                                }
                            }
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                for (int j = 0; j < lstRowDulicateValue.size(); j++) {
                    XSSFRow rowDulicateValue = worksheet.getRow(lstRowDulicateValue.get(j));
                    rowDulicateValue.createCell(3).setCellValue(SUCCESS_STATUS);
                    rowDulicateValue.getCell(3).setCellStyle(setAlignMiddle(workbook));
                    numberOfValidRecord++;
                }
            }
            workbook.write(out);

            return new Quartet<Boolean, InputStream, Integer, Integer>(isSuccess,
                    new ByteArrayInputStream(out.toByteArray()), lstRecordOfFile.size(), numberOfValidRecord);
        }

        // import zonedata tu file XLS
        public static Quartet<Boolean, InputStream, Integer, Integer> exportDataOfImportFileXls(
                MultipartFile readExcelDataFile, List<String> lstFailRecord, List<String> lstSuccessRecord,
                List<String> lstZoneDataValueDuplicateOfFile, List<String> lstRecordOfFile) throws IOException {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Boolean isSuccess = false;
            Integer numberOfValidRecord = 0;
            HSSFWorkbook workbook = new HSSFWorkbook(readExcelDataFile.getInputStream());
            HSSFSheet worksheet = workbook.getSheetAt(0);
            HSSFRow headerRow = worksheet.getRow(2);
            Cell statusCell = headerRow.createCell(3);
            statusCell.setCellValue(STATUS);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            statusCell.setCellStyle(headerCellStyle);
            List<Integer> lstRowDulicateValue = new ArrayList<>();
            List<String> lstZoneDataValueDuplicateOfFileCopy = new ArrayList<>();
            // Truong hop import tat ca ban ghi dung
            if (CollectionUtils.isEmpty(lstFailRecord) && CollectionUtils.isEmpty(lstZoneDataValueDuplicateOfFile)) {
                isSuccess = true;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    HSSFRow row = worksheet.getRow(i);
                    try {
                        String valueOfCell = row.getCell(1).toString().trim();
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim())) {
                            row.getCell(1).setCellValue(valueOfCell);
                            row.createCell(3).setCellValue(SUCCESS_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                            numberOfValidRecord++;
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            row.createCell(3).setCellValue(SUCCESS_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                            numberOfValidRecord++;
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import tat ca ban ghi sai
            } else if (CollectionUtils.isEmpty(lstSuccessRecord) && !CollectionUtils.isEmpty(lstFailRecord)) {
                isSuccess = false;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    HSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim())) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import co ban ghi dung va ban ghi sai
            } else {
                isSuccess = true;

                lstZoneDataValueDuplicateOfFileCopy.addAll(lstZoneDataValueDuplicateOfFile);
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    HSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                            String valueOfCell = row.getCell(1).getStringCellValue().trim();
                            if (lstSuccessRecord.contains(valueOfCell)
                                    && lstZoneDataValueDuplicateOfFile.contains(valueOfCell)) {
                                lstRowDulicateValue.add(i);
                                lstZoneDataValueDuplicateOfFile.remove(valueOfCell);
                            } else {
                                if (lstFailRecord.contains(valueOfCell)
                                        || lstZoneDataValueDuplicateOfFileCopy
                                                .contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                }
                                if (lstSuccessRecord.contains(valueOfCell)
                                        && !lstZoneDataValueDuplicateOfFileCopy
                                                .contains(valueOfCell)) {
                                    row.getCell(1).setCellValue(valueOfCell);
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                    numberOfValidRecord++;
                                }
                            }
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            if (lstSuccessRecord.contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))
                                    && lstZoneDataValueDuplicateOfFile
                                            .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))) {
                                lstRowDulicateValue.add(i);
                                lstZoneDataValueDuplicateOfFile
                                        .remove(String.valueOf((int) row.getCell(1).getNumericCellValue()));
                            } else {
                                if (lstFailRecord.contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))
                                        || lstZoneDataValueDuplicateOfFileCopy
                                                .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                }
                                if (lstSuccessRecord
                                        .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))
                                        && !lstZoneDataValueDuplicateOfFileCopy
                                                .contains(String.valueOf((int) row.getCell(1).getNumericCellValue()))) {
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                    numberOfValidRecord++;
                                }
                            }
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                for (int j = 0; j < lstRowDulicateValue.size(); j++) {
                    HSSFRow rowDulicateValue = worksheet.getRow(lstRowDulicateValue.get(j));
                    rowDulicateValue.createCell(3).setCellValue(SUCCESS_STATUS);
                    rowDulicateValue.getCell(3).setCellStyle(headerCellStyle);
                    numberOfValidRecord++;
                }
            }
            workbook.write(out);
            workbook.close();

            return new Quartet<Boolean, InputStream, Integer, Integer>(isSuccess,
                    new ByteArrayInputStream(out.toByteArray()), lstRecordOfFile.size(), numberOfValidRecord);
        }

        public static Quartet<Boolean, InputStream, Integer, Integer> exportDataOfImportFileTxt(
                MultipartFile readDataFile, List<String> lstRecordOfFile, List<String> lstFailRecord,
                List<String> lstSuccessRecord, List<String> lstZoneDataValueDuplicateOfFile, String storageFile)
                throws IOException {
            Boolean isSuccess = true;
            Integer numberOfValidRecord = 0;
            LocalDate now = LocalDate.now();
            FileWriter myWriter = new FileWriter(storageFile + readDataFile.getName() + now.toString() + ".txt");
            myWriter.close();
            File fileTemp = new File(storageFile + readDataFile.getName() + now.toString() + ".txt");
            try (FileWriter writer = new FileWriter(fileTemp)) {

                List<Integer> lstRowDulicateValue = new ArrayList<>();
                // Truong hop import tat ca ban ghi dung
                if (CollectionUtils.isEmpty(lstFailRecord)
                        && CollectionUtils.isEmpty(lstZoneDataValueDuplicateOfFile)) {
                    isSuccess = true;
                    for (int i = 0; i < lstRecordOfFile.size(); i++) {
                        if (i == lstRecordOfFile.size() - 1) {
                            writer.write(lstRecordOfFile.get(i) + ", " + SUCCESS_STATUS);
                            numberOfValidRecord++;
                        } else {
                            writer.write(lstRecordOfFile.get(i) + ", " + SUCCESS_STATUS + "\n");
                            numberOfValidRecord++;
                        }
                    }
                }
                // Truong hop import tat ca ban ghi sai
                else if (CollectionUtils.isEmpty(lstSuccessRecord) && !CollectionUtils.isEmpty(lstFailRecord)) {
                    isSuccess = false;
                    for (int i = 0; i < lstRecordOfFile.size(); i++) {
                        if (i == lstRecordOfFile.size() - 1) {
                            writer.write(lstRecordOfFile.get(i) + ", " + FAIL_STATUS);
                        } else {
                            writer.write(lstRecordOfFile.get(i) + ", " + FAIL_STATUS + "\n");
                        }
                    }
                }
                // Truong hop import co ban ghi dung va ban ghi sai
                else {
                    isSuccess = true;

                    for (String item : lstZoneDataValueDuplicateOfFile) {
                        for (int j = 0; j < lstRecordOfFile.size(); j++) {
                            if (lstSuccessRecord.contains(lstRecordOfFile.get(j))
                                    && item.equals(lstRecordOfFile.get(j))) {
                                lstRowDulicateValue.add(j);
                                break;
                            }
                        }
                    }

                    for (int k = 0; k < lstRecordOfFile.size(); k++) {
                        if (lstRowDulicateValue.contains(k)) {
                            writer.write(lstRecordOfFile.get(k) + ", " + SUCCESS_STATUS + "\n");
                            numberOfValidRecord++;
                        } else {
                            if (lstFailRecord.contains(lstRecordOfFile.get(k))
                                    || lstZoneDataValueDuplicateOfFile.contains(lstRecordOfFile.get(k))) {
                                writer.write(lstRecordOfFile.get(k) + ", " + FAIL_STATUS + "\n");
                            }
                            if (lstSuccessRecord.contains(lstRecordOfFile.get(k))
                                    && !lstZoneDataValueDuplicateOfFile.contains(lstRecordOfFile.get(k))) {
                                writer.write(lstRecordOfFile.get(k) + ", " + SUCCESS_STATUS + "\n");
                                numberOfValidRecord++;
                            }
                        }
                    }
                }
            }

            return new Quartet<Boolean, InputStream, Integer, Integer>(isSuccess,
                    new ByteArrayInputStream(FileUtils.readFileToByteArray(fileTemp)), lstRecordOfFile.size(),
                    numberOfValidRecord);
        }
    }

    public static class ExcelUltilsGeoNetZone {
        // Geo Net Zone
        public static ByteArrayInputStream exportGeoNetZoneData(List<GeoNetZone> lstGeoNetZoneData, String SHEET,
                String[] HEADERs, String TITLE) throws IOException {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Sheet sheet = ExcelUtils.createSheetForm(SHEET, HEADERs, TITLE, workbook);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            int rowIdx = ExcelUtils.ROW_INDEX;
            for (GeoNetZone geoNetZoneData : lstGeoNetZoneData) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(geoNetZoneData.getId());
                row.getCell(0).setCellStyle(headerCellStyle);
                row.createCell(1).setCellValue(geoNetZoneData.getCellId().toString());
                row.getCell(1).setCellStyle(headerCellStyle);

                DateTimeFormatter dateSqlFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime time = LocalDateTime.parse(geoNetZoneData.getTempUpdateDate(), dateSqlFormatter);

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/uuuu HH:mm:ss");
                String updateTime = dtf.format(time);
                row.createCell(2).setCellValue(updateTime);
                row.getCell(2).setCellStyle(headerCellStyle);
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }

        // import geo net zone tu file XLSX
        public static Quartet<Boolean, ByteArrayInputStream, Integer, Integer> exportDataOfImportFileXlsx(
                MultipartFile readExcelDataFile, List<Long> lstFailRecord, List<Long> lstSuccessRecord,
                List<Long> lstCellIdDuplicateOfFile, List<Long> lstRecordOfFile) throws IOException {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Boolean isSuccess = false;
            Integer numberOfValidRecord = 0;
            List<Integer> lstRowDulicateValue = new ArrayList<>();
            XSSFWorkbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
            XSSFSheet worksheet = workbook.getSheetAt(0);
            XSSFRow headerRow = worksheet.getRow(2);
            Cell statusCell = headerRow.createCell(3);
            statusCell.setCellValue(STATUS);
            statusCell.setCellStyle(setAlignMiddle(workbook));
            List<Long> lstCellIdDuplicateOfFileCopy = new ArrayList<>();
            int countStringCell = 0;
            for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                XSSFRow row = worksheet.getRow(i);
                try {
                    if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING
                            && !lstRecordOfFile.contains(Long.parseLong(row.getCell(1).getStringCellValue().trim()))) {
                        row.createCell(3).setCellValue(FAIL_STATUS);
                        row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                        countStringCell++;
                    }
                } catch (Exception e) {
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim())) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                            countStringCell++;
                        }
                    } catch (Exception e2) {
                        continue;
                    }
                    continue;
                }
            }
            // Truong hop import tat ca ban ghi dung
            if (CollectionUtils.isEmpty(lstFailRecord) && CollectionUtils.isEmpty(lstCellIdDuplicateOfFile)
                    && countStringCell == 0) {
                isSuccess = true;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC
                                || (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim()))) {
                            row.createCell(3).setCellValue(SUCCESS_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                            numberOfValidRecord++;
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import tat ca ban ghi sai
            } else if (CollectionUtils.isEmpty(lstSuccessRecord) && !CollectionUtils.isEmpty(lstFailRecord)) {
                isSuccess = false;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC
                                || (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim()))) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import co ban ghi dung va ban ghi sai
            } else {
                isSuccess = true;

                lstCellIdDuplicateOfFileCopy.addAll(lstCellIdDuplicateOfFile);
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            Long valueOfCell = (long) row.getCell(1).getNumericCellValue();
                            if (lstSuccessRecord.contains(valueOfCell)
                                    && lstCellIdDuplicateOfFile.contains(valueOfCell)) {
                                lstRowDulicateValue.add(i);
                                lstCellIdDuplicateOfFile.remove(valueOfCell);
                            } else {
                                if (lstFailRecord.contains(valueOfCell)
                                        || lstCellIdDuplicateOfFileCopy
                                                .contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                }
                                if (lstSuccessRecord.contains(valueOfCell)
                                        && !lstCellIdDuplicateOfFileCopy
                                                .contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                    numberOfValidRecord++;
                                }
                            }
                        }

                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                            Long valueOfCell = Long.parseLong(row.getCell(1).getStringCellValue().trim());
                            if (lstSuccessRecord.contains(valueOfCell)
                                    && lstCellIdDuplicateOfFile.contains(valueOfCell)) {
                                lstRowDulicateValue.add(i);
                                lstCellIdDuplicateOfFile.remove(valueOfCell);
                            } else {
                                if (lstFailRecord.contains(valueOfCell)
                                        || lstCellIdDuplicateOfFileCopy.contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                }
                                if (lstSuccessRecord.contains(valueOfCell)
                                        && !lstCellIdDuplicateOfFileCopy.contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(setAlignMiddle(workbook));
                                    numberOfValidRecord++;
                                }
                            }
                        }

                    } catch (Exception e) {
                        continue;
                    }
                }
                for (int j = 0; j < lstRowDulicateValue.size(); j++) {
                    XSSFRow rowDulicateValue = worksheet.getRow(lstRowDulicateValue.get(j));
                    rowDulicateValue.createCell(3).setCellValue(SUCCESS_STATUS);
                    rowDulicateValue.getCell(3).setCellStyle(setAlignMiddle(workbook));
                    numberOfValidRecord++;
                }
            }
            workbook.write(out);

            return new Quartet<Boolean, ByteArrayInputStream, Integer, Integer>(isSuccess,
                    new ByteArrayInputStream(out.toByteArray()), lstRecordOfFile.size() + countStringCell,
                    numberOfValidRecord);
        }

        // import geo net zone tu file XLS
        public static Quartet<Boolean, ByteArrayInputStream, Integer, Integer> exportDataOfImportFileXls(
                MultipartFile readExcelDataFile, List<Long> lstFailRecord, List<Long> lstSuccessRecord,
                List<Long> lstCellIdDuplicateOfFile, List<Long> lstRecordOfFile) throws IOException {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            List<Integer> lstRowDulicateValue = new ArrayList<>();
            Boolean isSuccess = false;
            List<Long> lstCellIdDuplicateOfFileCopy = new ArrayList<>();
            HSSFWorkbook workbook = new HSSFWorkbook(readExcelDataFile.getInputStream());
            HSSFSheet worksheet = workbook.getSheetAt(0);
            HSSFRow headerRow = worksheet.getRow(2);
            Cell statusCell = headerRow.createCell(3);
            statusCell.setCellValue(STATUS);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            statusCell.setCellStyle(headerCellStyle);
            int countStringCell = 0;
            Integer numberOfValidRecord = 0;
            for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                HSSFRow row = worksheet.getRow(i);
                try {
                    if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING
                            && !lstRecordOfFile.contains(Long.parseLong(row.getCell(1).getStringCellValue().trim()))) {
                        row.createCell(3).setCellValue(FAIL_STATUS);
                        row.getCell(3).setCellStyle(headerCellStyle);
                        countStringCell++;
                    }
                } catch (Exception e) {
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim())) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                            countStringCell++;
                        }
                    } catch (Exception e2) {
                        continue;
                    }
                    continue;
                }
            }
            // Truong hop import tat ca ban ghi dung
            if (CollectionUtils.isEmpty(lstFailRecord) && CollectionUtils.isEmpty(lstCellIdDuplicateOfFile)
                    && countStringCell == 0) {
                isSuccess = true;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    HSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC
                                || (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim()))) {
                            row.createCell(3).setCellValue(SUCCESS_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                            numberOfValidRecord++;
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import tat ca ban ghi sai
            } else if (CollectionUtils.isEmpty(lstSuccessRecord) && !CollectionUtils.isEmpty(lstFailRecord)) {
                isSuccess = false;
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    HSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC
                                || (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING && !StringUtils.isEmpty(row.getCell(1).getStringCellValue().trim()))) {
                            row.createCell(3).setCellValue(FAIL_STATUS);
                            row.getCell(3).setCellStyle(headerCellStyle);
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                // Truong hop import co ban ghi dung va ban ghi sai
            } else {
                isSuccess = true;

                lstCellIdDuplicateOfFileCopy.addAll(lstCellIdDuplicateOfFile);
                for (int i = ExcelUtils.ROW_INDEX; i < worksheet.getLastRowNum() + 1; i++) {
                    HSSFRow row = worksheet.getRow(i);
                    try {
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            Long valueOfCell = (long) row.getCell(1).getNumericCellValue();
                            if (lstSuccessRecord.contains(valueOfCell)
                                    && lstCellIdDuplicateOfFile.contains(valueOfCell)) {
                                lstRowDulicateValue.add(i);
                                lstCellIdDuplicateOfFile.remove(valueOfCell);
                            } else {
                                if (lstFailRecord.contains(valueOfCell)
                                        || lstCellIdDuplicateOfFileCopy.contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                }
                                if (lstSuccessRecord.contains(valueOfCell)
                                        && !lstCellIdDuplicateOfFileCopy.contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                    numberOfValidRecord++;
                                }
                            }
                        }
                        if (row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
                            Long valueOfCell = Long.parseLong(row.getCell(1).getStringCellValue().trim());
                            if (lstSuccessRecord.contains(valueOfCell)
                                    && lstCellIdDuplicateOfFile.contains(valueOfCell)) {
                                lstRowDulicateValue.add(i);
                                lstCellIdDuplicateOfFile.remove(valueOfCell);
                            } else {
                                if (lstFailRecord.contains(valueOfCell)
                                        || lstCellIdDuplicateOfFileCopy.contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(FAIL_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                }
                                if (lstSuccessRecord.contains(valueOfCell)
                                        && !lstCellIdDuplicateOfFileCopy.contains(valueOfCell)) {
                                    row.createCell(3).setCellValue(SUCCESS_STATUS);
                                    row.getCell(3).setCellStyle(headerCellStyle);
                                    numberOfValidRecord++;
                                }
                            }
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
                for (int j = 0; j < lstRowDulicateValue.size(); j++) {
                    HSSFRow rowDulicateValue = worksheet.getRow(lstRowDulicateValue.get(j));
                    rowDulicateValue.createCell(3).setCellValue(SUCCESS_STATUS);
                    rowDulicateValue.getCell(3).setCellStyle(headerCellStyle);
                    numberOfValidRecord++;
                }
            }
            workbook.write(out);
            return new Quartet<Boolean, ByteArrayInputStream, Integer, Integer>(isSuccess,
                    new ByteArrayInputStream(out.toByteArray()), lstRecordOfFile.size() + countStringCell,
                    numberOfValidRecord);
        }

        // import geo net zone tu file TXT
        public static Quartet<Boolean, ByteArrayInputStream, Integer, Integer> exportDataOfImportFileTxt(
                MultipartFile readDataFile, List<Long> lstRecordOfFile, List<Long> lstFailRecord,
                List<Long> lstSuccessRecord, List<Long> lstCellIdDuplicateOfFile, List<String> lstInvalidTypeCellId,
                String storageFile) throws IOException {
            Boolean isSuccess = true;
            Integer numberOfValidRecord = 0;
            List<Integer> lstRowDulicateValue = new ArrayList<>();
            FileWriter myWriter = new FileWriter(storageFile + readDataFile.getOriginalFilename());
            myWriter.close();
            File fileTemp = new File(storageFile + readDataFile.getOriginalFilename());
            try (FileWriter writer = new FileWriter(fileTemp)) {

                // Truong hop import tat ca ban ghi dung
                if (CollectionUtils.isEmpty(lstFailRecord) && CollectionUtils.isEmpty(lstCellIdDuplicateOfFile)
                        && CollectionUtils.isEmpty(lstInvalidTypeCellId)) {
                    isSuccess = true;
                    for (int i = 0; i < lstRecordOfFile.size(); i++) {
                        writer.write(lstRecordOfFile.get(i) + ", " + SUCCESS_STATUS + "\n");
                        numberOfValidRecord++;
                    }
                }
                // Truong hop import tat ca ban ghi sai
                else if (CollectionUtils.isEmpty(lstSuccessRecord) && !CollectionUtils.isEmpty(lstFailRecord)) {
                    isSuccess = false;
                    for (int i = 0; i < lstRecordOfFile.size(); i++) {
                        writer.write(lstRecordOfFile.get(i) + ", " + FAIL_STATUS + "\n");
                    }
                    for (String stringCellId : lstInvalidTypeCellId) {
                        writer.write(stringCellId + ", " + FAIL_STATUS + "\n");
                    }
                }
                // Truong hop import co ban ghi dung va ban ghi sai
                else {
                    isSuccess = true;

                    for (Long item : lstCellIdDuplicateOfFile) {
                        for (int j = 0; j < lstRecordOfFile.size(); j++) {
                            if (lstSuccessRecord.contains(lstRecordOfFile.get(j))
                                    && item.equals(lstRecordOfFile.get(j))) {
                                lstRowDulicateValue.add(j);
                                break;
                            }
                        }
                    }

                    for (int k = 0; k < lstRecordOfFile.size(); k++) {
                        if (lstRowDulicateValue.contains(k)) {
                            writer.write(lstRecordOfFile.get(k) + ", " + SUCCESS_STATUS + "\n");
                            numberOfValidRecord++;
                        } else {
                            if (lstFailRecord.contains(lstRecordOfFile.get(k))
                                    || lstCellIdDuplicateOfFile.contains(lstRecordOfFile.get(k))) {
                                writer.write(lstRecordOfFile.get(k) + ", " + FAIL_STATUS + "\n");
                            }
                            if (lstSuccessRecord.contains(lstRecordOfFile.get(k))
                                    && !lstCellIdDuplicateOfFile.contains(lstRecordOfFile.get(k))) {
                                writer.write(lstRecordOfFile.get(k) + ", " + SUCCESS_STATUS + "\n");
                                numberOfValidRecord++;
                            }
                        }

                    }
                    for (String stringCellId : lstInvalidTypeCellId) {
                        writer.write(stringCellId + ", " + FAIL_STATUS + "\n");
                    }
                }
            }
            return new Quartet<Boolean, ByteArrayInputStream, Integer, Integer>(isSuccess,
                    new ByteArrayInputStream(FileUtils.readFileToByteArray(fileTemp)),
                    lstRecordOfFile.size() + lstInvalidTypeCellId.size(), numberOfValidRecord);
        }
    }
}
