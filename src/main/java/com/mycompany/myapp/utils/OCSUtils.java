package com.mycompany.myapp.utils;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.constant.NormalizerConstants;
import com.mycompany.myapp.domain.event.Event;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.Verification;
import com.mycompany.myapp.domain.eventProcessing.VerificationItem;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.domain.ratetable.RateTable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.math.NumberUtils;

public final class OCSUtils {

    private OCSUtils() {}

    public static Integer getDomain() {
        return 1;
    }

    public static String getCategorySearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "remark";
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getStatisticSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getZoneSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "remark";
            case "id":
                return "id";
            case "zoneCode":
                return "zoneCode";
            default:
                return null;
        }
    }

    public static String getPccRuleSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "remark";
            case "priority":
                return "priority";
            default:
                return null;
        }
    }

    public static String getGeoNetZoneSearchParam(String property) {
        switch (property) {
            case "id":
                return "id";
            case "cellId":
                return "cellId";
            case "updateDate":
                return "updateDate";
            default:
                return null;
        }
    }

    public static String getZoneDataSearchParam(String property) {
        switch (property) {
            case "id":
                return "id";
            case "zoneDataValue":
                return "zoneDataValue";
            case "updateDate":
                return "updateDate";
            default:
                return null;
        }
    }

    public static String getReserveInfoSearchParam(String property) {
        switch (property) {
            case "name":
                return ReserveInfo.FieldNames.NAME;
            case "description":
                return ReserveInfo.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getBalTypeSearchParam(String property) {
        switch (property) {
            case "name":
                return BalType.FieldNames.NAME;
            case "description":
                return BalType.FieldNames.DESCRIPTION;
            default:
                return null;
        }
    }

    public static String getThresholdSearchParam(String property) {
        switch (property) {
            case "name":
                return Threshold.FieldNames.NAME;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getMapAcmBalBalSearchParam(String property) {
        switch (property) {
            case "name":
                return Threshold.FieldNames.NAME;
            case "description":
                return "remark";
            default:
                return null;
        }
    }

    public static String getMapSharebalBalSearchParam(String property) {
        switch (property) {
            case "name":
                return Threshold.FieldNames.NAME;
            case "description":
                return "remark";
            default:
                return null;
        }
    }

    public static String getStateTypeSearchParam(String property) {
        switch (property) {
            case "name":
                return StateType.FieldNames.NAME;
            case "description":
                return StateType.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getStateGroupSearchParam(String property) {
        switch (property) {
            case "name":
                return StateGroups.FieldNames.NAME;
            case "description":
                return StateGroups.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getPepProfileSearchParam(String property) {
        switch (property) {
            case "name":
                return PepProfile.FieldNames.NAME;
            case "description":
                return PepProfile.FieldNames.DESCRIPTION;
            case "monitorKey":
                return PepProfile.FieldNames.MONITORKEY;
            case "priority":
                return PepProfile.FieldNames.PRIORITY;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getCharacteristicSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getBillingCyclesParams(String property) {
        switch (property) {
            case BillingCycle.FieldNames.END_DATE:
                return BillingCycle.FieldNames.END_DATE;
            case BillingCycle.FieldNames.BEGIN_DATE:
                return BillingCycle.FieldNames.BEGIN_DATE;
            default:
                return null;
        }
    }

    public static String getNormalizerSearchParams(String property) {
        switch (property) {
            case Normalizer.FieldNames.NAME:
                return Normalizer.FieldNames.NAME;
            case Normalizer.FieldNames.DESCRIPTION:
                return Normalizer.FieldNames.DESCRIPTION;
            default:
                return null;
        }
    }

    public static String getAllQos(String property) {
        switch (property) {
            case Qos.FieldNames.QOS_NAME:
                return Qos.FieldNames.QOS_NAME;
            case Qos.FieldNames.QOS_GBRDL:
                return Qos.FieldNames.QOS_GBRDL;
            case Qos.FieldNames.QOS_GBRUL:
                return Qos.FieldNames.QOS_GBRUL;
            case Qos.FieldNames.QOS_MBRDL:
                return Qos.FieldNames.QOS_MBRDL;
            case Qos.FieldNames.QOS_MBRUL:
                return Qos.FieldNames.QOS_MBRUL;
            case "description":
                return Qos.FieldNames.QOS_DESCRIPTION;
            case Qos.FieldNames.QOS_QCI:
                return Qos.FieldNames.QOS_QCI;
            default:
                return null;
        }
    }

    public static String getMonitorKeys(String property) {
        switch (property) {
            case MonitorKey.FieldNames.MONITOR_KEY:
                return MonitorKey.FieldNames.MONITOR_KEY;
            case "description":
                return MonitorKey.FieldNames.MONITOR_DESCRIPTION;
            default:
                return null;
        }
    }

    public static String getVerificationItemKeys(String property) {
        switch (property) {
            case VerificationItem.FieldNames.ITEM_ID:
                return VerificationItem.FieldNames.ITEM_ID;
            default:
                return null;
        }
    }

    public static Map<String, Object> stringToObjectMap(String input) {
        Map<String, Object> results = new HashMap<>();
        String[] entries = input.split(";");
        for (String entry : entries) {
            String[] keyValue = entry.split(":");
            String key = getKeyCount(keyValue[0], results);
            String value = keyValue[1];
            if (Objects.equals(value, NormalizerConstants.NONE)) {
                results.put(key, value);
                continue;
            }
            //            LocalDateTime valueDate = OCSDateTimeUtils.stringToDate(keyValue[1]);
            //            if (valueDate != null) {
            //                results.put(key, valueDate);
            //                continue;
            //            }
            try {
                Long valueLong = NumberUtils.createNumber(value).longValue();
                results.put(key, valueLong);
            } catch (Exception e) {
                results.put(key, value);
                // Ignore
            }
            results.put(key, value);
        }
        return results;
    }

    private static String getKeyCount(String key, Map<String, Object> map) {
        int count = 1;
        String keyResult = key;
        while (map.get(keyResult) != null) {
            keyResult = key + count;
            count++;
        }
        return keyResult;
    }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String getBillingCycles(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getRatingFilterSearchParam(String property) {
        switch (property) {
            case "name":
                return RatingFilter.FieldNames.NAME;
            case "description":
                return RatingFilter.FieldNames.DESCRIPTION;
            case "input":
                return RatingFilter.FieldNames.INPUT;
            default:
                return null;
        }
    }

    public static String getPriceComponentSearchParam(String property) {
        switch (property) {
            case "name":
                return PriceComponent.FieldNames.NAME;
            case "description":
                return PriceComponent.FieldNames.DESCRIPTION;
            default:
                return null;
        }
    }

    public static String getBlockSearchParam(String property) {
        switch (property) {
            case "name":
                return Block.FieldNames.NAME;
            case "description":
                return Block.FieldNames.DESCRIPTION;
            default:
                return null;
        }
    }

    public static String getRateTableSearchParam(String property) {
        switch (property) {
            case "name":
                return RateTable.FieldNames.NAME;
            case "description":
                return RateTable.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getBuilderSearchParam(String property) {
        switch (property) {
            case "name":
                return Builder.FieldNames.NAME;
            case "description":
                return Builder.FieldNames.DESCRIPTION;
            case "id":
                return Builder.FieldNames.ID;
            case "builderTypeItemName":
                return Builder.FieldNames.BUILDER_TYPE_ITEM_NAME;
            case "conditionName":
                return "c.name";
            default:
                return null;
        }
    }

    public static String getRouterSearchParam(String property) {
        switch (property) {
            case "name":
                return Builder.FieldNames.NAME;
            case "description":
                return Builder.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getVerificationSearchParam(String property) {
        switch (property) {
            case "name":
                return Verification.FieldNames.NAME;
            case "description":
                return Verification.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getActionTypeParam(String property) {
        switch (property) {
            case "name":
                return ActionType.FieldNames.NAME;
            case "description":
                return ActionType.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getActionSearchParam(String property) {
        switch (property) {
            case "name":
                return Action.FieldNames.NAME;
            case "description":
                return Action.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getEventSearchParam(String property) {
        switch (property) {
            case "name":
                return Event.FieldNames.NAME;
            case "description":
                return Event.FieldNames.DESCRIPTION;
            case "id":
                return "id";
            default:
                return null;
        }
    }

    public static String getOfferSearchParam(String property) {
        switch (property) {
            case "name":
                return Offer.FieldNames.NAME;
            case "description":
                return Offer.FieldNames.DESCRIPTION;
            case "id":
                return Offer.FieldNames.ID;
            default:
                return null;
        }
    }

    public static String getOfferVersionSearchParam(String property) {
        switch (property) {
            case "state":
                return "rf.name";
            case "id":
                return OfferVersion.FieldNames.ID;
            case "number":
                return OfferVersion.FieldNames.NUMBER;
            default:
                return null;
        }
    }

    public static String getOfferRedirectionParam(String property) {
        switch (property) {
            case "redirectionType":
                return "redirectionTypeName";
            default:
                return null;
        }
    }

    public static String getOfferTemplateVersionActionSearchParam(String property) {
        switch (property) {
            case "actionId":
                return "actionId";
            case "name":
                return "name";
            case "description":
                return "description";
            case "posIndex":
                return "posIndex";
            default:
                return null;
        }
    }

    public static String getOfferTemplateVersionBundleSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            case "id":
                return "id";
            case "offerBundleId":
                return "offerBundleId";
            default:
                return null;
        }
    }

    public static String getOfferTemplateVersionCharspecSearchParam(String property) {
        switch (property) {
            case "charspecId":
                return "charspecId";
            case "description":
                return "description";
            case "id":
                return "id";
            case "extensible":
                return "extensible";
            case "value":
                return "value";
            case "name":
                return "name";
            default:
                return null;
        }
    }

    public static String getOfferVersionCharspecSearchParam(String property) {
        switch (property) {
            case "charspecId":
                return "charspecId";
            case "id":
                return "id";
            case "value":
                return "value";
            case "name":
                return "name";
            default:
                return null;
        }
    }

    public static String getOfferVersionActionSearchParam(String property) {
        switch (property) {
            case "actionId":
                return "actionId";
            case "name":
                return "name";
            case "posIndex":
                return "posIndex";
            default:
                return null;
        }
    }

    public static String getOfferVersionBundleSearchParam(String property) {
        switch (property) {
            case "id":
                return "id";
            case "name":
                return "name";
            case "posIndex":
                return "posIndex";
            default:
                return null;
        }
    }

    public static String getBuilderTypeItemSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            default:
                return null;
        }
    }

    public static String getFunctionParamSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            default:
                return null;
        }
    }

    public static String getFilterSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            case "id":
                return "id";
            case "fieldName":
                return "F.name";
            default:
                return null;
        }
    }

    public static String getConditionSearchParam(String property) {
        switch (property) {
            case "name":
                return "name";
            case "description":
                return "description";
            case "id":
                return "id";
            case "input":
                return "input";
            case "posIndex":
                return "posIndex";

            case "filterName":
                return "f.name";
            default:
                return null;
        }
    }

}
