package com.mycompany.myapp.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OCSDateTimeUtils {
    private OCSDateTimeUtils() {}
    public static final String DATE_TIME_NORM_PARAM_PATTERN = "yyyy/MM/dd/HH/mm/ss";


    public static LocalDateTime stringToDate(String input) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_NORM_PARAM_PATTERN);
            LocalDateTime dateTime = LocalDateTime.parse(input, formatter);
            return dateTime;
        } catch (Exception exception) {
            return null;
        }
    }
}
