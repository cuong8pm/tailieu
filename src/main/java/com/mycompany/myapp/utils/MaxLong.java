package com.mycompany.myapp.utils;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MaxLongValidator.class)
public @interface MaxLong {

    String message() default "id_must_be_less_than_max_long_value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
