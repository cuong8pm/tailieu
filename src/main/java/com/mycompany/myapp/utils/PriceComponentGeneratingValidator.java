package com.mycompany.myapp.utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;

public class PriceComponentGeneratingValidator implements ConstraintValidator<PriceComponentGenerating, Object> {

    private String fieldName;
    private String dependFieldName;

    public void initialize(PriceComponentGenerating PriceComponentGenerating) {
        this.fieldName = PriceComponentGenerating.fieldName();
        this.dependFieldName = PriceComponentGenerating.dependFieldName();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        BeanWrapperImpl obj = new BeanWrapperImpl(value);
        Integer genneratingType = (Integer) obj.getPropertyValue(dependFieldName);
        Boolean isUseForGenerating = (Boolean) obj.getPropertyValue(fieldName);
        if (isUseForGenerating && genneratingType == null) {
            return false;
        } else if (!isUseForGenerating && genneratingType != null) {
            return false;
        }
        return true;
    }

}
