package com.mycompany.myapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "reserve_info")
public class ReserveInfo implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RESERVE_INFO_ID", nullable = false)
    private Long id;

    @Column(name = "RESERVE_INFO_ID", insertable = false, updatable = false)
    private String tempId;
    
    @Column(name = "MAX_RESERVE", nullable = false)
    private Integer maxReserve;

    @Column(name = "MIN_RESERVE", nullable = false)
    private Integer minReserve;

    @Column(name = "USAGE_QUOTA_THRESHOLD")
    private Integer usageQuotaThreshold;

    @Column(name = "CAN_ROLLBACK")
    private Boolean canRollback;

    @Column(name = "RESV_NAME", nullable = false)
    private String resvName;

    @Column(name = "RESV_DESC")
    private String resvDesc;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;
    
    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "RESV_NAME", insertable = false, updatable = false)
    private String name;
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "resvName";
        public static final String DESCRIPTION = "resvDesc";
        public static final String POS_INDEX = "posIndex";
    }

    @Transient
    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
