package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "nested_object_class")
public class NestedObjectClass implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "OBJ_CLASS_ID", nullable = false)
    private Long id;

    @Column(name = "OBJ_CLASS_NAME", nullable = false)
    private String name;

}
