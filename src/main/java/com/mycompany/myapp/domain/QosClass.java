package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "qos_class")
public class QosClass implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "QCI", nullable = false, length = 30)
    private String qci;
    
    @Column(name = "RESOURCE_TYPE", nullable = true)
    private String resourceType;
    
    @Column(name = "PRIORITY", nullable = true, scale = 2)
    private Double priority;
    
    @Column(name = "DELAY_BUDGET", nullable = true)
    private Integer delayBudget;
    
    @Column(name = "ERROR_LOST", nullable = true)
    private Integer errorLost;
    
    @Column(name = "NAME", nullable = false)
    private String name;
    
    @JsonProperty(value = "value", access = Access.READ_ONLY)
    public String getValue() {
        return (qci + ":" + resourceType + ":" + name);
    }
}
