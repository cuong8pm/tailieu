package com.mycompany.myapp.domain.action;

import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "action_price_component_map")
@Data
public class ActionPriceComponentMap implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ACTION_PRICE_COMPONENT_MAP_ID", nullable = false)
    private Long id;

    @Column(name = "ACTION_ID", nullable = false)
    private Long actionId;

    @Column(name = "PRICE_COMPONENT_ID", nullable = false)
    private Long priceComponentId;

    @Column(name = "PC_INDEX")
    private Integer posIndex;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "ACTION_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "PRICE_COMPONENT_ID", insertable = false, updatable = false)
    private Long childId;


    @Override
    public void setParentMappingId(Long parentId) {
        this.actionId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.priceComponentId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }

    public static class FieldNames {
        private FieldNames() {}

        public static final String POS_INDEX = "posIndex";
    }
}
