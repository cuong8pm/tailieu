package com.mycompany.myapp.domain.action;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "action")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Action implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACTION_ID", nullable = false)
    private Long id;

    @Column(name = "ACTION_NAME", nullable = false)
    private String name;

    @Column(name = "ACTION_TYPE", nullable = false)
    private Long actionTypeId;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "EFF_DATE")
    private Timestamp effDate;

    @Column(name = "EXP_DATE")
    private Timestamp expDate;

    @Column(name = "STATE")
    private Boolean state;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "RESERVE_INFO_ID")
    private Long reserveInfoId;

//    @Column(name = "IS_GENERATED")
//    private Boolean generated;

    @Column(name = "PRIORITY_FILTER_ID")
    private Long priorityFilterId;

    @Column(name = "DYNAMIC_RESERVE_FILTER_ID")
    private Long dynamicReserveFilterId;

    @Column(name = "SORTING_FILTER_ID")
    private Long sortingFilterId;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;


    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }
}
