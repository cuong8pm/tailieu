package com.mycompany.myapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "state_type")
@Data
public class StateType implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "STATE_TYPE_ID", nullable = false )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "STATE_NAME", nullable = false)
    private String name;

    @Column(name = "STATE_TYPE", nullable = false)
    private Integer stateType;

    @Column(name = "STATE_GROUP_ID", nullable = false)
    private Long stateGroupId;

    @Column(name = "STATE_DESC")
    private String stateDesc;

    @Column(name = "STATE_MASK", nullable = false)
    private String stateMask;

    @Column(name = "FILTER_VALUE", nullable = false)
    private String filterValue;

    @Column(name = "STATUS")
    private Boolean status;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "STATE_INDEX")
    private Integer stateIndex;

    @Column(name = "STATE_STATUS")
    private Boolean stateStatus;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX")
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;
    
    @Column(name = "STATE_TYPE_ID", insertable = false, updatable = false)
    private Long tempId;
    
    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "stateDesc";
        public static final String POS_INDEX = "posIndex";
    }

}
