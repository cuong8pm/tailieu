
package com.mycompany.myapp.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.constant.CategoryType;

@Entity
@Table(name = "category")
public class Category implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "CATEGORY_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CATEGORY_TYPE", updatable = false)
//    @Enumerated(EnumType.ORDINAL)
    private CategoryType categoryType;

    @Column(name = "CATEGORY_NAME")
    private String name;

    @Column(name = "CATEGORY_PARENT_ID")
    private Long categoryParentId;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "treeType")
    private String treeType;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "path")
    private String path;

    @Column(name = "pos_index")
    private Integer posIndex;

    @Column(name = "CATEGORY_PARENT_ID", insertable = false, updatable = false)
    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long categoryId) {
        this.id = categoryId;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public String getName() {
        return name;
    }

    public void setName(String categoryName) {
        this.name = categoryName;
    }

    public Long getCategoryParentId() {
        return categoryParentId;
    }

    public void setCategoryParentId(Long categoryParentId) {
        this.categoryParentId = categoryParentId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTreeType() {
        return treeType;
    }

    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Category that = (Category) o;

        if (id != that.id)
            return false;
        if (categoryType != that.categoryType)
            return false;
        if (!Objects.equals(name, that.name))
            return false;
        if (!Objects.equals(categoryParentId, that.categoryParentId))
            return false;
        if (!Objects.equals(remark, that.remark))
            return false;
        if (!Objects.equals(treeType, that.treeType))
            return false;
        if (!Objects.equals(domainId, that.domainId))
            return false;
        if (!Objects.equals(path, that.path))
            return false;
        if (!Objects.equals(posIndex, that.posIndex))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + categoryType.getValue();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (categoryParentId != null ? categoryParentId.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (treeType != null ? treeType.hashCode() : 0);
        result = 31 * result + (domainId != null ? domainId.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (posIndex != null ? posIndex.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" + "categoryId=" + id + ", categoryType=" + categoryType + ", categoryName='" + name + '\''
            + ", categoryParentId=" + categoryParentId + ", remark='" + remark + '\'' + ", treeType='" + treeType
            + '\'' + ", domainId=" + domainId + ", path='" + path + '\'' + ", posIndex=" + posIndex + '}';
    }

    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public void setCategoryId(Long categoryId) {
        // not implement
    }

    public static final class FieldNames {
        private FieldNames() {}

        public static final String POS_INDEX = "posIndex";
    }
}
