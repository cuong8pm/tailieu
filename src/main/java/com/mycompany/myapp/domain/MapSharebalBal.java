package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "map_sharebal_bal")
@Data
public class MapSharebalBal implements  Serializable, OCSMoveableEntity  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MAP_SHAREBAL_BAL_ID", nullable = false)
    private Long id;

    @Column(name = "SHARE_BALTYPE_ID")
    private Long shareBalTypeId;

    @Column(name = "BALTYPE_ID")
    private Long balTypeId;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "SHARE_TYPE")
    private Integer mappingTypeId;

    @Column(name = "SHARE_TYPE_ID")
    private Integer shareTypeId;

    @Column(name = "SHARE_BAL_TYPE")
    private Integer shareBalType;

    @Column(name = "POS_INDEX")
    private Integer posIndex;
    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;
    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }
}
