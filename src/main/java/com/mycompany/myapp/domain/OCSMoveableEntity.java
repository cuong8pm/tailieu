package com.mycompany.myapp.domain;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface OCSMoveableEntity extends OCSBaseEntity {

    Integer getPosIndex();

    void setPosIndex(Integer posIndex);

    Long getParentId();

    void setParentId(Long parentId);

    void setCategoryId(Long categoryId);
}
