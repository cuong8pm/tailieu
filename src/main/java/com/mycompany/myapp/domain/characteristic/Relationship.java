package com.mycompany.myapp.domain.characteristic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CharSpecRelationship.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "char_spec_relationship")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Relationship implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "char_spec_seq")
    private Integer charSpecSeq;

    @OneToMany(mappedBy = "charSpecRelationship")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ValueGroup> charSpecValueGroups = new HashSet<>();

    @ManyToOne(optional = false)
    @JoinColumn(name = "parent_char_spec_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecRelationshipLists", allowSetters = true)
    private Characteristic parentCharSpec;

    @ManyToOne(optional = false)
    @JoinColumn(name = "child_char_spec_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecRelationshipList2s", allowSetters = true)
    private Characteristic childCharSpec;

    @ManyToOne(optional = false)
    @JoinColumn(name = "relationship_type_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecRelationships", allowSetters = true)
    private RelationshipType relationshipType;
    
    @Column(name = "child_char_spec_id" , insertable = false, updatable = false)
    private Long childCharSpecId;
    
    @Column(name ="parent_char_spec_id", insertable = false, updatable = false)
    private Long parentCharSpecId;
}
