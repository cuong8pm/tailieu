package com.mycompany.myapp.domain.characteristic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "char_spec_value")
@Data
public class CharSpecValue {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "is_default")
    private Boolean isDefault;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "range_interval")
    private String rangeInterval;

    @Column(name = "range_step")
    private Integer rangeStep;

    @Column(name = "unit_of_measure")
    private String unitOfMeasure;

    @Column(name = "value", nullable = false)
    private String value;

    @Column(name = "value_from", nullable = false)
    private String valueFrom;

    @Column(name = "value_to", nullable = false)
    private String valueTo;

    @Column(name = "char_spec_id", nullable = false)
    private Long charSpecId;

    @Column(name = "value_type_id", nullable = false)
    private Long valueTypeId;
}
