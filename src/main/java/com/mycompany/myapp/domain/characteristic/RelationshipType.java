package com.mycompany.myapp.domain.characteristic;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A RelationshipType.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "relationship_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RelationshipType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "relationshipType")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Relationship> charSpecRelationships = new HashSet<>();

    @OneToMany(mappedBy = "relationshipType")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ValueGroup> charSpecValueGroups = new HashSet<>();
}
