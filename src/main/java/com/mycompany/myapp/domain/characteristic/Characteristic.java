package com.mycompany.myapp.domain.characteristic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;

/**
 * A CharSpec.
 */
@Entity
@Table(name = "char_spec")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Characteristic implements Serializable, OCSMoveableEntity, OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    public Characteristic() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Size(max = 255)
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "value_unique")
    private Boolean valueUnique;

    @Column(name = "min_cardinality")
    private Integer minCardinality;

    @Column(name = "max_cardinality")
    private Integer maxCardinality;

    @Column(name = "configurable")
    private Boolean configurable;

    @Column(name = "extensible")
    private Boolean extensible;

    @Column(name = "derivation_formula")
    private String formula;

    @Column(name = "regex")
    private String regex;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private Timestamp startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private Timestamp endDate;

    @OneToMany(mappedBy = "characteristic")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Value> charSpecValues = new HashSet<>();

    @OneToMany(mappedBy = "parentCharSpec")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Relationship> charSpecRelationshipLists = new HashSet<>();

    @OneToMany(mappedBy = "childCharSpec")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Relationship> charSpecRelationshipList2s = new HashSet<>();

    @Column(name = "char_spec_type_id")
    private Integer valueRefeTable;

    @Column(name = "value_type_id")
    private Long valueType;
    
    @Column(name = "default_value")
    private String defaultValue;
    
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public Long getValueType() {
        return valueType;
    }

    public void setValueType(Long valueType) {
        this.valueType = valueType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Characteristic name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Characteristic description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isValueUnique() {
        return valueUnique;
    }

    public Characteristic valueUnique(Boolean valueUnique) {
        this.valueUnique = valueUnique;
        return this;
    }

    public void setValueUnique(Boolean valueUnique) {
        this.valueUnique = valueUnique;
    }

    public Integer getMinCardinality() {
        return minCardinality;
    }

    public Characteristic minCardinality(Integer minCardinality) {
        this.minCardinality = minCardinality;
        return this;
    }

    public void setMinCardinality(Integer minCardinality) {
        this.minCardinality = minCardinality;
    }

    public Integer getMaxCardinality() {
        return maxCardinality;
    }

    public Characteristic maxCardinality(Integer maxCardinality) {
        this.maxCardinality = maxCardinality;
        return this;
    }

    public void setMaxCardinality(Integer maxCardinality) {
        this.maxCardinality = maxCardinality;
    }

    public Boolean isConfigurable() {
        return configurable;
    }

    public Characteristic configurable(Boolean configurable) {
        this.configurable = configurable;
        return this;
    }

    public void setConfigurable(Boolean configurable) {
        this.configurable = configurable;
    }

    public Boolean isExtensible() {
        return extensible;
    }

    public Boolean getExtensible() {
        return extensible;
    }

    public Characteristic extensible(Boolean extensible) {
        this.extensible = extensible;
        return this;
    }

    public void setExtensible(Boolean extensible) {
        this.extensible = extensible;
    }

    public String getFormula() {
        return formula;
    }

    public Characteristic formula(String formula) {
        this.formula = formula;
        return this;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getRegex() {
        return regex;
    }

    public Characteristic regex(String regex) {
        this.regex = regex;
        return this;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public Characteristic startDate(Timestamp startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public Characteristic endDate(Timestamp endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Set<Value> getCharSpecValues() {
        return charSpecValues;
    }

    public Characteristic charSpecValues(Set<Value> charSpecValues) {
        this.charSpecValues = charSpecValues;
        return this;
    }

    public Characteristic addCharSpecValue(Value charSpecValue) {
        this.charSpecValues.add(charSpecValue);
        charSpecValue.setCharacteristic(this);
        return this;
    }

    public Characteristic removeCharacteristicValue(Value charSpecValue) {
        this.charSpecValues.remove(charSpecValue);
        charSpecValue.setCharacteristic(null);
        return this;
    }

    public void setCharacteristicValues(Set<Value> charSpecValues) {
        this.charSpecValues = charSpecValues;
    }

    public Set<Relationship> getCharSpecRelationshipLists() {
        return charSpecRelationshipLists;
    }

    public Characteristic charSpecRelationshipLists(Set<Relationship> charSpecRelationships) {
        this.charSpecRelationshipLists = charSpecRelationships;
        return this;
    }

    public Characteristic addCharSpecRelationshipList(Relationship charSpecRelationship) {
        this.charSpecRelationshipLists.add(charSpecRelationship);
        charSpecRelationship.setParentCharSpec(this);
        return this;
    }

    public Characteristic removeCharSpecRelationshipList(Relationship charSpecRelationship) {
        this.charSpecRelationshipLists.remove(charSpecRelationship);
        charSpecRelationship.setParentCharSpec(null);
        return this;
    }

    public void setCharacteristicRelationshipLists(Set<Relationship> charSpecRelationships) {
        this.charSpecRelationshipLists = charSpecRelationships;
    }

    public void setCharSpecRelationshipList2s(Set<Relationship> charSpecRelationships) {
        this.charSpecRelationshipList2s = charSpecRelationships;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Characteristic)) {
            return false;
        }
        return id != null && id.equals(((Characteristic) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CharSpec{" + "id=" + getId() + ", name='" + getName() + "'" + ", description='" + getDescription() + "'"
                + ", valueUnique='" + isValueUnique() + "'" + ", minCardinality=" + getMinCardinality()
                + ", maxCardinality=" + getMaxCardinality() + ", configurable='" + isConfigurable() + "'"
                + ", extensible='" + isExtensible() + "'" + ", derivationFormula='" + getFormula() + "'" + ", regex='"
                + getRegex() + "'" + ", startDate='" + getStartDate() + "'" + ", endDate='" + getEndDate() + "'"
                + ", orderIndex=" + getPosIndex() + "}";
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    @Transient
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getValueRefeTable() {
        return valueRefeTable;
    }

    public void setValueRefeTable(Integer valueRefeTable) {
        this.valueRefeTable = valueRefeTable;
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Move {
    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
        public static final String NAME = "name";
    }

    public static class Length {
        private Length() {
        }

        public static final Integer NAME = 255;
    }
}
