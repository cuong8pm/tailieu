package com.mycompany.myapp.domain.characteristic;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A CharSpecValueGroup.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "char_spec_value_group")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ValueGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "is_default")
    private Boolean isDefault;

    @ManyToOne(optional = false)
    @JoinColumn(name = "parent_char_value_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecValueGroupLists", allowSetters = true)
    private Value parentCharValue;

    @ManyToOne(optional = false)
    @JoinColumn(name = "child_char_value_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecValueGroupList2s", allowSetters = true)
    private Value childCharValue;

    @ManyToOne(optional = false)
    @JoinColumn(name = "char_spec_relationship_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecValueGroups", allowSetters = true)
    private Relationship charSpecRelationship;

    @ManyToOne(optional = false)
    @JoinColumn(name = "relationship_type_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecValueGroups", allowSetters = true)
    private RelationshipType relationshipType;
}
