package com.mycompany.myapp.domain.characteristic;

public interface RelationshipPojo extends Comparable<RelationshipPojo> {

    public Long getId();

    public Long getParentId();

    public Long getChildId();

    public Long getRelationshipTypeId();

    public String getRelationshipTypeName();

    default int compareTo(RelationshipPojo relationshipPojo) {
        // sort student's name by ASC
        return this.getId().compareTo(relationshipPojo.getId());
    }
}
