package com.mycompany.myapp.domain.characteristic;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "char_spec_value")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Value implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "is_default")
    private Boolean isDefault;

    @NotNull
    @Column(name = "value", nullable = false)
    private String value;

    @Column(name = "unit_of_measure")
    private String unitOfMeasure;

    @NotNull
    @Column(name = "value_from", nullable = false)
    private String valueFrom;

    @NotNull
    @Column(name = "value_to", nullable = false)
    private String valueTo;

    @Column(name = "range_interval")
    private String rangeInterval;

    @Column(name = "range_step")
    private Integer rangeStep;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "parentCharValue")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ValueGroup> charSpecValueGroupLists = new HashSet<>();

    @OneToMany(mappedBy = "childCharValue")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ValueGroup> charSpecValueGroupList2s = new HashSet<>();

    @ManyToOne(optional = false)
    @JoinColumn(name = "char_spec_id")
    @NotNull
    @JsonIgnoreProperties(value = "charSpecValues", allowSetters = true)
    private Characteristic characteristic;

    @Column(name = "value_type_id")
    private Long valueType;
}
