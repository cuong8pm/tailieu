package com.mycompany.myapp.domain.characteristic;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ValueType.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "value_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ValueType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "valueType")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Characteristic> charSpecs = new HashSet<>();

    @OneToMany(mappedBy = "valueType")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Value> charSpecValues = new HashSet<>();
}
