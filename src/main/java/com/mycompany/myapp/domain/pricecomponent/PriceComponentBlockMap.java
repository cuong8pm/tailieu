package com.mycompany.myapp.domain.pricecomponent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.mycompany.myapp.domain.OCSCloneableMap;

import lombok.Data;

@Entity
@Data
@Table(name = "price_component_block_map")
public class PriceComponentBlockMap implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "PRICE_COMPONENT_BLOCK_MAP_ID", nullable = false)
    private Long id;

    @Column(name = "PRICE_COMPONENT_ID", nullable = false)
    private Long priceComponentId;

    @Column(name = "BLOCK_ID", nullable = false)
    private Long blockId;

    @Column(name = "IS_USE_FOR_GENERATING", nullable = false)
    private Boolean isUseForGenerating;

    @Column(name = "GENERATING_TYPE")
    private Integer genneratingType;

    @Column(name = "IS_REPLACE")
    private Boolean isReplace;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "PRICE_COMPONENT_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "BLOCK_ID", insertable = false, updatable = false)
    private Long childId;

    public static class FieldNames {
        private FieldNames() {};
        
        public static final String POS_INDEX = "posIndex";
    }

    @Override
    public void clearId() {
        this.id = null;

    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.priceComponentId = parentId;
        
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.blockId = childId;
    }

}
