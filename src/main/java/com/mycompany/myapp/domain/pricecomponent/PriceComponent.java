package com.mycompany.myapp.domain.pricecomponent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.OCSCloneableEntity;

import lombok.Data;

@Entity
@Table(name = "price_component")
@Data
public class PriceComponent implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRICE_COMPONENT_ID", nullable = false)
    private Long id;

    @Column(name = "PRICE_COMPONENT_NAME", nullable = false)
    private String name;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }

}
