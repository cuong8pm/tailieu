package com.mycompany.myapp.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "unit_type")
public class UnitType implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UNIT_TYPE_ID", nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "BASE_RATE", nullable = false)
    private Integer baseRate;

    @Column(name = "DISPLAY_RATE", nullable = false)
    private Integer displayRate;

    @Column(name = "[PRECISION]")
    private Integer precision;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "UNIT_ORDER")
    private Integer unitOrder;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "UNIT_PRECISION", nullable = false)
    private Integer unitPrecision;

    @Column(name = "[ABBREVIATION]")
    private String abbreviation;
    
    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;
    
    @Column(name = "UNIT_ORDER", insertable = false, updatable = false)
    private Long posIndex;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(Integer baseRate) {
        this.baseRate = baseRate;
    }

    public Integer getDisplayRate() {
        return displayRate;
    }

    public void setDisplayRate(Integer displayRate) {
        this.displayRate = displayRate;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUnitOrder() {
        return unitOrder;
    }

    public void setUnitOrder(Integer unitOrder) {
        this.unitOrder = unitOrder;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Integer getUnitPrecision() {
        return unitPrecision;
    }

    public void setUnitPrecision(Integer unitPrecision) {
        this.unitPrecision = unitPrecision;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UnitType)) return false;
        UnitType unitType = (UnitType) o;
        return Objects.equals(getId(), unitType.getId()) &&
            Objects.equals(getName(), unitType.getName()) &&
            Objects.equals(getBaseRate(), unitType.getBaseRate()) &&
            Objects.equals(getDisplayRate(), unitType.getDisplayRate()) &&
            Objects.equals(getPrecision(), unitType.getPrecision()) &&
            Objects.equals(getRemark(), unitType.getRemark()) &&
            Objects.equals(getCategoryId(), unitType.getCategoryId()) &&
            Objects.equals(getUnitOrder(), unitType.getUnitOrder()) &&
            Objects.equals(getDomainId(), unitType.getDomainId()) &&
            Objects.equals(getUnitPrecision(), unitType.getUnitPrecision()) &&
            Objects.equals(getAbbreviation(), unitType.getAbbreviation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getBaseRate(), getDisplayRate(), getPrecision(), getRemark(), getCategoryId(), getUnitOrder(), getDomainId(), getUnitPrecision(), getAbbreviation());
    }

    @Override
    public String toString() {
        return "UnitType{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", baseRate=" + baseRate +
            ", displayRate=" + displayRate +
            ", precision=" + precision +
            ", remark='" + remark + '\'' +
            ", categoryId=" + categoryId +
            ", unitOrder=" + unitOrder +
            ", domainId=" + domainId +
            ", unitPrecision=" + unitPrecision +
            ", abbreviation='" + abbreviation + '\'' +
            '}';
    }

    @Override
    public void setPosIndex(Integer posIndex) {
        setUnitOrder(posIndex);
    }

    @Override
    public Integer getPosIndex() {
        return getUnitOrder();
    }

    @Transient
    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public void setPosIndex(Long posIndex) {
        this.posIndex = posIndex;
    }


    public static final class FieldNames {

        private FieldNames () {}
        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
        public static final String POS_INDEX = "unitOrder";
    }
}
