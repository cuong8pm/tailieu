package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "map_acmbal_bal")
public class MapAcmBalBal implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "ACM_BALTYPE_ID")
    private Long toBalanceId;

    @Column(name = "BALTYPE_ID")
    private Long fromBalanceId;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MAP_ACM_BALTYPE_ID", nullable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "POS_INDEX")
    private Integer posIndex;

    @Column(name = "REMARK")
    private String description;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }
}
