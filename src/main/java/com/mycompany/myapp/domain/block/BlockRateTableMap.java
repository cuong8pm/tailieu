package com.mycompany.myapp.domain.block;

import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "block_rate_table_map")
public class BlockRateTableMap implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "BLOCK_RATE_TABLE_MAP_ID")
    private Long id;

    @Column(name = "BLOCK_ID", nullable = false)
    private Long blockId;

    @Column(name = "RATE_TABLE_ID", nullable = false)
    private Long rateTableId;

    @Column(name = "COMPONENT_TYPE", nullable = false)
    private Integer componentType;

    @Column(name = "RATE_TABLE_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "BLOCK_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "RATE_TABLE_ID", insertable = false, updatable = false)
    private Long childId;

    @Override
    public void clearId() {
        this.id = null;
    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.blockId = parentId;

    }

    @Override
    public void setChildMappingId(Long childId) {
        this.rateTableId = childId;
    }

}
