package com.mycompany.myapp.domain.block;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "block")
public class Block implements OCSCloneableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BLOCK_ID")
    private Long id;

    @Column(name = "BLOCK_TYPE", nullable = false)
    private Long blockType;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "BLOCK_NAME", nullable = false)
    private String name;

    @Column(name = "IS_CONDITIONAL_MULTIBAL_CREATING", nullable = false)
    private Boolean isConditionalMultibalCreating;

    @Column(name = "IS_CREATE_NEW_OBJECT", nullable = false)
    private Boolean isCreateNewObject;

    @Column(name = "AFFECTED_OBJECT_TYPE", nullable = false)
    private Integer affectedObjectType;

    @Column(name = "AFFECTED_VALUE")
    private Long affectedValue;

    @Column(name = "AFFECTED_FIELD")
    private String affectedField;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "BLOCK_FILTER_ID")
    private Long blockFilterId;

    @Column(name = "IS_SET_BAL_IF_NOT_EXIST", nullable = false)
    private Boolean isSetBalIfNotExist;

    @Column(name = "BLOCK_MODE")
    private Integer blockMode;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }
}
