package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "qos")
public class Qos implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "QOS_ID", nullable = false)
    private Long id;
    
    @Column(name = "QOS_NAME", nullable = false, length = 100)
    private String name;
    
    @Column(name = "QCI", nullable = false, length = 30)
    private String qci;
    
    @Column(name = "MBRUL", nullable = false)
    private Integer mbrul;
    
    @Column(name = "MBRDL", nullable = false)
    private Integer mbrdl;
    
    @Column(name = "GBRUL", nullable = false)
    private Integer gbrul;
    
    @Column(name = "GBRDL", nullable = false)
    private Integer gbrdl;
    
    @Column(name = "DOMAIN_ID", nullable = true)
    private Integer domainId;
    
    @Column(name = "QOS_REMARK", nullable = true, length = 100)
    private String qosRemark;
    
    @Column(name = "MBRUL", insertable = false, updatable = false)
    private String tempMbrul;
    
    @Column(name = "MBRDL", insertable = false, updatable = false)
    private String tempMbrdl;
    
    @Column(name = "GBRUL", insertable = false, updatable = false)
    private String tempGbrul;
    
    @Column(name = "GBRDL", insertable = false, updatable = false)
    private String tempGbrdl;
    
    public static final class FieldNames {
        private FieldNames() {}
        public static final String QOS_ID = "id";
        public static final String QOS_NAME = "name";
        public static final String QOS_QCI = "qci";
        public static final String QOS_MBRUL = "mbrul";
        public static final String QOS_MBRDL = "mbrdl";
        public static final String QOS_GBRUL = "gbrul";
        public static final String QOS_GBRDL = "gbrdl";
        public static final String QOS_DESCRIPTION = "qosRemark";
    }
}
