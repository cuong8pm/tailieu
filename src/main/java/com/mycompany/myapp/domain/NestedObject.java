package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.web.rest.errors.Resources;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Table(name = "nested_object")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NestedObject extends Tree implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "OBJ_ID", nullable = false)
    @JsonView(Dropdown.class)
    private Long id;

    @Column(name = "OBJ_NAME", nullable = false)
    @JsonView(Dropdown.class)
    private String name;

    @Column(name = "OBJ_CLASS_ID", nullable = false)
    @JsonView({Tree.class, Dropdown.class})
    private Long objClassId;

    @Column(name = "IS_ALIST", nullable = false)
    @JsonView(value = {Dropdown.class, Tree.class})
    private Boolean alist;

    @Column(name = "PARENT_CLASS_ID", nullable = false)
    @JsonView(Tree.class)
    private Long parentClassId;

    // Not use properties
    @Column(name = "OBJ_ID", insertable = false, updatable = false)
    private Integer domainId;

    @Column(name = "PARENT_CLASS_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "OBJ_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @JsonView(Tree.class)
    @Transient
    private Collection<Tree> templates;

    @JsonView(Tree.class)
    @Transient
    private String treeType = Resources.TreeType.TEMPLTE;

    @JsonView(Tree.class)
    @Transient
    private String type = Resources.NESTED_OBJECT;

    @JsonView(Tree.class)
    @Transient
    private Integer categoryLevel;

    @JsonView(Tree.class)
    @Transient
    private Boolean hasChild;

    @Override
    public void setCategoryId(Long categoryId) {

    }

    public interface Dropdown{}
}
