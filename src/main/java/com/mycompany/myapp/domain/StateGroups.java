package com.mycompany.myapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "state_groups")
@Data
public class StateGroups implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "STATE_GROUP_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "state_group_desc")
    private String stateGroupDesc;

    @Column(name = "state_group_name", nullable = false)
    private String name;

    @Column(name = "POS_INDEX")
    private Integer posIndex;
    
    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;
    
    @Column(name = "state_group_id", insertable = false, updatable = false)
    private Long tempId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "stateGroupDesc";
        public static final String POS_INDEX = "posIndex";
    }
}
