package com.mycompany.myapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "profile_pep")
@Data
public class PepProfile implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PROFILE_PEP_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PROFILE_PEP_NAME", nullable = false)
    private String name;

    @Column(name = "QOS_ID", nullable = false)
    private Long qosId;

    @Column(name = "MONITOR_KEY", nullable = false)
    private Long monitorKey;

    @Column(name = "PRIORITY", nullable = false)
    private Integer priority;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "POS_INDEX")
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "PROFILE_PEP_ID", insertable = false, updatable = false)
    private Long tempId;
    
    @Column(name = "MONITOR_KEY", insertable = false, updatable = false)
    private String tempMonitorKey;

    @Column(name = "PRIORITY", insertable = false, updatable = false)
    private String tempPriority;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
        public static final String MONITORKEY = "monitorKey";
        public static final String PRIORITY = "priority";
        public static final String POS_INDEX = "posIndex";
    }

}
