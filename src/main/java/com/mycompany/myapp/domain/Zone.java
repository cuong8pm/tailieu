package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "zone")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Zone implements OCSMoveableEntity, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ZONE_ID", nullable = false)
    private Long id;

    @Column(name = "ZONE_MAP_ID", nullable = false)
    private Long zoneMapId;

    @Column(name = "ZONE_NAME", nullable = false)
    private String name;

    @Column(name = "ZONE_CODE")
    private String zoneCode;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "ZONE_MAP_ID", updatable = false, insertable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "IS_MATCHING")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isMatching;

    @Column(name = "ZONE_ID", insertable = false, updatable = false)
    private String tempId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public void setCategoryId(Long categoryId) {

    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
    }

}
