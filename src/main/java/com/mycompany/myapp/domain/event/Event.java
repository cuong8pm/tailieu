package com.mycompany.myapp.domain.event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.OCSCloneableEntity;

import lombok.Data;

@Entity
@Table(name = "event")
@Data
public class Event implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "EVENT_ID", nullable = false)
    private Long id;

    @Column(name = "EVENT_NAME", nullable = false)
    private String name;

    @Column(name = "EVENT_TYPE", nullable = false)
    private Integer eventType;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IS_REQUIRE_RESERVE_INFO", nullable = false)
    private Boolean isRequireReserveInfo;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "IS_REQUIRE_RATING_CONFIG", nullable = false)
    private Boolean isRequireRatingConfig;

    @Column(name = "EVENT_ANALYSIS_FILTER", nullable = false)
    private Long eventAnalysisFilter;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static final class Length {

        private Length() {
        }

        public static final Integer NAME = null;
    }
}
