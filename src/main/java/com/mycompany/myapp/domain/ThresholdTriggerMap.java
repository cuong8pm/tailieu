package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "threshold_event_policy_map")
@Data
public class ThresholdTriggerMap implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "THRESHOLD_TRIGGER_MAP_ID", nullable = false)
    private Long id;

    @Column(name = "THRESHOLD_ID", nullable = false)
    private Long thresholdId;

    @Column(name = "TRIGGER_OCS_ID", nullable = false)
    private Long triggerOcsId;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

}
