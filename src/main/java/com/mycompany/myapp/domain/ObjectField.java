package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "object_field")
public class ObjectField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OBJECT_FIELD_ID", nullable = false)
    private Long id;

    @Column(name = "OBJECT_FIELD_NAME", nullable = false)
    private String name;

    @Column(name = "IS_LIST", nullable = false)
    private Long list;

    @Column(name = "OBJECT_FIELD_PARENT")
    private Long objectFieldParent;

}
