package com.mycompany.myapp.domain.offer.offer;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import liquibase.pro.packaged.cl;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "offer")
public class Offer extends OfferCommon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_ID", nullable = false)
    private Long id;

    @Column(name = "EXTERNAL_ID", nullable = false)
    private Long externalId;

    @Column(name = "OFFER_ID", insertable = false, updatable = false)
    private Long posIndex;

    @Column(name = "OFFER_NAME", nullable = false)
    private String name;

    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

    public static class Length {
        public Length() {
        }
        public static final Integer NAME = 255;
    }
}
