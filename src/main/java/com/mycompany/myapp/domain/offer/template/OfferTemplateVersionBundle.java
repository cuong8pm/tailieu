package com.mycompany.myapp.domain.offer.template;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "offer_template_version_bundle")
public class OfferTemplateVersionBundle extends VersionBundleCommon {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = " OFFER_TEMPLATE_VERSION_BUNDLE_ID", nullable = false)
    private Long id;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", nullable = false)
    private Long offerTemplateVersionId;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "OFFER_TEMPLATE_VERSION_BUNDLE_ID", insertable = false, updatable = false)
    private String name;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", insertable = false, updatable = false)
    private Long versionId;

    public static final class FieldNames {

        private FieldNames() {
        }
        public static final String ID = "bundleId";
    }
    @Override
    public Long getId() {

        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.offerTemplateVersionId = parentId;
    }
}
