package com.mycompany.myapp.domain.offer.offer;

import com.mycompany.myapp.domain.OCSCloneableMap;
import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "offer_version_action")
public class OfferVersionAction extends VersionActionCommon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "OFFER_VERSION_ACTION_ID")
    private Long id;

    @Column(name = "offer_version_id", nullable = false)
    private Long offerVersionId;

    @Column(name = "offer_version_id", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public void setParentMappingId(Long parentId) {
        this.offerVersionId = parentId;
    }

    @Override
    public Long getParentId() {
        return this.offerVersionId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.actionId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }

}
