package com.mycompany.myapp.domain.offer.template;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "offer_template")
public class OfferTemplate extends OfferCommon implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_TEMPLATE_ID", nullable = false)
    private Long id;

    @Column(name = "OFFER_TEMPLATE_ID", insertable = false, updatable = false)
    private Long posIndex;

    @Column(name = "OFFER_TEMPLATE_NAME", nullable = false)
    private String name;

    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }
}
