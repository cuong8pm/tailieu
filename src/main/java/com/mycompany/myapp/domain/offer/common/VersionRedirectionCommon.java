package com.mycompany.myapp.domain.offer.common;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.mycompany.myapp.domain.OCSCloneableEntity;

import lombok.Data;

@Data
@MappedSuperclass
public class VersionRedirectionCommon implements OCSCloneableEntity {

    @Column(name = "URL", nullable = false)
    private String url;

    @Column(name = "REDIRECTION_TYPE", nullable = false)
    private Integer redirectionType;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private String name;

    @Override
    public Long getParentId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setParentId(Long parentId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub

    }

    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void setId(Long id) {

    }

}
