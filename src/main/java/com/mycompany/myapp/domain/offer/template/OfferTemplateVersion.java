package com.mycompany.myapp.domain.offer.template;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;

import lombok.Data;

@Entity
@Data
@Table(name = "offer_template_version")
public class OfferTemplateVersion extends OfferVersionCommon implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_TEMPLATE_VERSION_ID", nullable = false)
    private Long id;
    
    @Column(name = "OFFER_TEMPLATE_ID", nullable = false)
    private Long offerTemplateId;
    
    @Column(name = "OFFER_TEMPLATE_ID", insertable = false, updatable = false)
    private Long parentId;
    
    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.offerTemplateId = parentId;
    }  

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub
    }
    
    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
