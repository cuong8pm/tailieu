package com.mycompany.myapp.domain.offer.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.mycompany.myapp.domain.OCSCloneableEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public class VersionBundleCommon implements OCSCloneableEntity {

    
    private static final long serialVersionUID = 1L;

    @Column(name = "CHILD_OFFER_ID", nullable = false)
    private Long childOfferId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;
    
    
    
    @Override
    public Long getParentId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setParentId(Long parentId) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Integer getPosIndex() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setName(String name) {
        // TODO Auto-generated method stub
        
    }
}
