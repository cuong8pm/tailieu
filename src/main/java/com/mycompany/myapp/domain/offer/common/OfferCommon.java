package com.mycompany.myapp.domain.offer.common;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public class OfferCommon implements OCSCloneableEntity {
    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "PRIORITY", nullable = false)
    private Long priority;

    @Column(name = "EFF_DATE", nullable = false)
    private Timestamp effDate;

    @Column(name = "EXP_DATE", nullable = false)
    private Timestamp expDate;

    @Column(name = "OFFER_TYPE", nullable = false)
    private Integer offerType;

    @Column(name = "BILLING_CYCLE_TYPE_ID", nullable = false)
    private Long billingCycleTypeId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PAYMENT_TYPE", nullable = false)
    private Integer paymentType;

    @Column(name = "EXPIRABLE", nullable = false)
    private Boolean expriable;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "SUSPENDABLE", nullable = false)
    private Boolean suspendable;

    @Column(name = "IS_BUNDLE", nullable = false)
    private Boolean isBundle;

    @Column(name = "IS_MULTI_CYCLE", nullable = false)
    private Boolean isMultiCycle;

    @Column(name = "UPDATE_DATE", nullable = false)
    private Timestamp updateDate;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;


    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    public static final class FieldNames {

        private FieldNames () {}
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }
}
