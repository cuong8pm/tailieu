package com.mycompany.myapp.domain.offer.common;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSCloneableMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public class VersionCharspecUseCommon implements OCSCloneableEntity {

    @Column(name = "CHARSPEC_ID", nullable = false)
    private Long charspecId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private String name;

    @Column(name = "CHARSPEC_ID", insertable = false, updatable = false)
    private Long childId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
    }

    @Override
    public Long getParentId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setParentId(Long parentId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub

    }

    public Long getId(){
        return null;
    }

    @Override
    public void setId(Long id) {

    }

}
