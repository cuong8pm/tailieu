package com.mycompany.myapp.domain.offer.offer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "offer_version_bundle")
public class OfferVersionBundle extends VersionBundleCommon {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_VERSION_BUNDLE_ID", nullable = false)
    private Long id;

    @Column(name = "ROOT_OFFER_VERSION_ID", nullable = false)
    private Long rootOfferVersionId;

    @Column(name = "ROOT_OFFER_VERSION_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "OFFER_VERSION_BUNDLE_ID", insertable = false, updatable = false)
    private String name;

    @Column(name = "ROOT_OFFER_VERSION_ID", insertable = false, updatable = false)
    private Long versionId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = null;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.rootOfferVersionId = parentId;
    }

}
