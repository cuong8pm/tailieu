package com.mycompany.myapp.domain.offer.template;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.offer.common.OfferRelationshipCommon;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "OFFER_TEMPLATE_VERSION_RELATIONSHIP")
public class OfferTemplateVersionRelationship extends OfferRelationshipCommon{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_TEMPLATE_VERSION_RELATIONSHIP_ID", nullable = false)
    private Long id;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", nullable = false)
    private Long offerTemplateVersionId;

    @Column(name = "IS_PARENT", nullable = false)
    private Boolean isParent;

    @Column(name = "OFFER_ID", nullable = false)
    private Long offerId;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "OFFER_ID", insertable = false, updatable = false)
    private Long childId;

    @Override
    public Long getParentId() {
        return this.offerTemplateVersionId;
    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.offerTemplateVersionId = parentId;
    }

    @Override
    public Long getChildId() {
        return this.offerId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.offerId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }

}
