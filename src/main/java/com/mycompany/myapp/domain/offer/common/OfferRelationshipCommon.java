package com.mycompany.myapp.domain.offer.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.mycompany.myapp.domain.OCSCloneableMap;

import lombok.Data;

@Data
@MappedSuperclass
public class OfferRelationshipCommon implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

 
    
    @Column(name = "RELATIONSHIP_TYPE", nullable = false)
    private Integer relationshipType;
    
    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private String name;

    @Override
    public Long getParentId() {
        return null;
    }

    @Override
    public void setParentMappingId(Long parentId) {
    }

    @Override
    public Long getChildId() {
        return null;
    }

    @Override
    public void setChildMappingId(Long childId) {
        
    }

    @Override
    public void clearId() {
    }

}
