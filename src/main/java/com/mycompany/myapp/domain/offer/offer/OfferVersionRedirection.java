package com.mycompany.myapp.domain.offer.offer;

import java.io.Serializable;

import javax.persistence.*;

import com.mycompany.myapp.domain.offer.common.VersionRedirectionCommon;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "offer_version_redirection")
public class OfferVersionRedirection extends VersionRedirectionCommon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_version_redirection_id", nullable = false)
    private Long id;

    @Column(name = "OFFER_VERSION_ID", nullable = false)
    private Long offerVersionId;

    @Column(name = "OFFER_VERSION_ID", insertable = false, updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
        public static final String REDIRECTION_TYPE = "name";
    }

    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.offerVersionId = parentId;
    }
}
