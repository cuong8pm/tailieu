package com.mycompany.myapp.domain.offer.offer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.offer.common.OfferRelationshipCommon;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "OFFER_RELATIONSHIP")
public class OfferRelationship extends OfferRelationshipCommon {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_RELATIONSHIP_ID", nullable = false)
    private Long id;
    
    @Column(name = "PARENT_OFFER_ID", nullable = false)
    private Long parentOfferId;

    @Column(name = "CHILD_OFFER_ID", nullable = false)
    private Long childOfferId;

    @Column(name = "PARENT_OFFER_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "CHILD_OFFER_ID", insertable = false, updatable = false)
    private Long childId;

    @Column(name = "CHILD_OFFER_ID", insertable = false, updatable = false)
    private Long isParent;
    
    @Override
    public Long getParentId() {
        return this.parentOfferId;
    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.parentOfferId = parentId;
    }

    @Override
    public Long getChildId() {
        return this.childOfferId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.childOfferId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }
}
