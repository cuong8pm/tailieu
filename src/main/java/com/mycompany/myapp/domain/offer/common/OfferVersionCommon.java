package com.mycompany.myapp.domain.offer.common;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.State;

import lombok.Data;

@Data
@MappedSuperclass
public class OfferVersionCommon implements OCSCloneableEntity{

    @Column(name = "NUMBER", nullable = false)
    private Long number;

    @Column(name = "STATE", nullable = false)
    private State state;

    @Column(name = "EFF_DATE", nullable = false)
    private Timestamp effDate;

    @Column(name = "EXP_DATE", nullable = false)
    private Timestamp expDate;

    @Column(name = "SPECIAL_METHOD", nullable = true)
    private Integer specialMethod;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "OFFER_VERSION_ID", insertable = false, updatable = false)
    private Long posIndex;

    //ignore
    @Column(name = "OFFER_VERSION_ID", insertable = false, updatable = false)
    private String name;

    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {
    }

    @Override
    public Long getParentId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setParentId(Long parentId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub

    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setId(Long id) {
        // TODO Auto-generated method stub

    }

    public static final class FieldNames {

        private FieldNames () {}
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String STATE = "state";
        public static final String NUMBER = "number";
    }
}
