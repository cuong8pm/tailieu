package com.mycompany.myapp.domain.offer.template;

import javax.persistence.*;

import com.mycompany.myapp.domain.offer.common.VersionCharspecUseCommon;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "offer_template_version_charspec_use")
public class OfferTemplateVersionCharspecUse extends VersionCharspecUseCommon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_template_version_charspec_use_id", nullable = false)
    private Long id;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", nullable = false)
    private Long offerTemplateVersionId;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.offerTemplateVersionId = parentId;
    }

    @Override
    public Long getParentId() {
        return this.parentId;
    }
}
