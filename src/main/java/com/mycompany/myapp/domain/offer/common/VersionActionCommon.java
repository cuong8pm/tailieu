package com.mycompany.myapp.domain.offer.common;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public class VersionActionCommon implements OCSCloneableMap {


    @Column(name = "action_id", nullable = false)
    protected Long actionId;

    @Column(name = "pos_index", nullable = false)
    protected Integer posIndex;

    @Column(name = "allow_mobile_money", nullable = false)
    protected Boolean allowMobileMoney;

    @Column(name = "domain_id", nullable = false)
    protected Integer domainId;

    @Column(name = "action_id", insertable = false, updatable = false)
    protected Long childId;

    @Override
    public Long getParentId() {
        return null;
    }

    @Override
    public void setParentMappingId(Long parentId) {

    }

    @Override
    public void setChildMappingId(Long childId) {

    }

    @Override
    public void clearId() {
    }
}
