package com.mycompany.myapp.domain.offer.offer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;

import lombok.Data;

@Entity
@Data
@Table(name = "offer_version")
public class OfferVersion extends OfferVersionCommon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OFFER_VERSION_ID", nullable = false)
    private Long id;

    @Column(name = "OFFER_ID", nullable = false)
    private Long offerId;

    @Column(name = "OFFER_TEMPLATE_VERSION_ID", nullable = true)
    private Long offerTemplateVersionId;

    @Column(name = "OFFER_ID", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub
    }

    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.offerId = parentId;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
