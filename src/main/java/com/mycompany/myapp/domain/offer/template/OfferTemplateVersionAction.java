package com.mycompany.myapp.domain.offer.template;

import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "offer_template_version_action")
public class OfferTemplateVersionAction extends VersionActionCommon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "OFFER_TEMPLATE_VERSION_ACTION_ID")
    private Long id;

    @Column(name = "offer_template_version_id", nullable = false)
    private Long offerTemplateVersionId;

    @Column(name = "offer_template_version_id", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public void setParentMappingId(Long parentId) {
        this.offerTemplateVersionId = parentId;
    }

    @Override
    public Long getParentId() {
        return this.offerTemplateVersionId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.actionId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }
}
