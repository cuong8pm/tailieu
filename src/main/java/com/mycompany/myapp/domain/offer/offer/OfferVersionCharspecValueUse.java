package com.mycompany.myapp.domain.offer.offer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.OCSCloneableEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "offer_version_charspec_value_use")
public class OfferVersionCharspecValueUse implements OCSCloneableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_version_charspec_value_use_id", nullable = false)
    private Long id;

    @Column(name = "CHARSPEC_USE_ID", nullable = false)
    private Long charspecUseId;

    @Column(name = "VALUE", nullable = false)
    private String value;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "CHARSPEC_USE_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "VALUE", insertable = false, updatable = false)
    private String name;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
    }

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.charspecUseId = parentId;
    }

}
