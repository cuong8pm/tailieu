package com.mycompany.myapp.domain.constant;

public final class Constants {
    public static final String SYSTEM_ACCOUNT = "system";

    // Regex for acceptable name fields
    public static final String NAME_REGEX =
        "^([a-zA-Z0-9-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]*)$";
    public static final String REGEX_MESSAGE_FAILED = "name_cannot_contain_special_characters";

    public static final String KEY_REGEX = "^([0-9*#]{4}-){3}[0-9*#]{4}$";
    public static final String KEY_REGEX_MESSAGE_FAILER = "wrong_format";
    public static final String ACTION_TYPE ="actionType";
    private Constants() {}
}

