package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;
import java.util.stream.Stream;

public enum NormalizerType {

    StringNormalizer(0),
    StringMatchNormalizer(1),
    NumberNormalizer(2),
    CheckRegisterListNormalizer(3),
    TimeNormalizer(4),
    DateNormalizer(5),
    QuantityNormalizer(6),
    BalanceNormalizer(7),
    ZoneNormalizer(8),
    CheckInListNormalizer(9),
    AcmBalanceNormalizer(10),
    NumberParameterNormalizer(11);

    @JsonValue
    private int value;

    NormalizerType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @JsonCreator
    public static NormalizerType of(int value) {
        return Stream.of(NormalizerType.values()).filter(c -> Objects.equals(c.getValue(), value)).findFirst()
            .orElse(CheckRegisterListNormalizer);
    }
}
