package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.mycompany.myapp.dto.BalancesDTO;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.Objects;
import java.util.stream.Stream;

public final class DropdownType {
    public static final class MultiBalType {
        public static final int PERIODIC = 2;
    }

    public enum EffectDateType {
        None(null),
        StartDateField(0),
        FromProvisioning(1),
        FirstEvent(2),
        BillingCycle(3),
        PurcharseDate(4),
        StartOfHour(5),
        StartOfDay(6),
        StartOfWeek(7),
        StartOfMonth(8),
        StartOfYear(9),
        HoursFromStartOfDay(10),
        DaysFromStartOfWeek(11),
        WeeksFromStartOfMonth(12),
        MonthsFromStartOfYear(13);
        @JsonValue
        private final Integer value;

        EffectDateType(Integer value) {
            this.value = value;
        }

        @JsonValue
        public Integer getValue() {
            return this.value;
        }

        @JsonCreator
        public static EffectDateType of(Integer value) {
            return Stream.of(EffectDateType.values()).filter(c -> Objects.equals(c.getValue(), value)).findFirst()
                .orElseThrow(()->new DataInvalidException(ErrorMessage.BalType.EFFECT_DATE_TYPE_NOT_FOUND, Resources.BAL_TYPE, BalancesDTO.FieldNames.EFF_DATE_TYPE_ID));
        }
    }
    public enum ExpireDateType {
        None(null),
        ExpireDateField(0),
        FromProvisioning(1),
        BillingCycle(2),
        EndOfHour(3),
        EndOfDay(4),
        EndOfWeek(5),
        EndOfMonth(6),
        EndOfYear(7),
        Hours(8),
        Days(9),
        Weeks(10),
        Months(11),
        Years(12);
        @JsonValue
        private final Integer value;

        ExpireDateType(Integer value) {
            this.value = value;
        }

        @JsonValue
        public Integer getValue() {
            return this.value;
        }

        @JsonCreator
        public static ExpireDateType of(Integer value) {
            return Stream.of(ExpireDateType.values()).filter(c -> Objects.equals(c.getValue(), value)).findFirst()
                .orElseThrow(()->new DataInvalidException(ErrorMessage.BalType.EXP_DATE_TYPE_ID_NOT_FOUND, Resources.BAL_TYPE, BalancesDTO.FieldNames.EXP_DATE_TYPE_ID));
        }
    }
    public static final class OwnerLevel {
        public static final int MEMBERSHIP = 4;
    }
    public enum ZoneDataType {
        ZONE(0),
        ZONE_MAP(1),
        GEO_HOME_ZONE(2);

        @JsonValue
        private final Integer value;

        ZoneDataType(Integer value) {
            this.value = value;
        }

        @JsonValue
        public Integer getValue() {
            return this.value;
        }

        @JsonCreator
        public static ZoneDataType of(Integer value) {
            return Stream.of(ZoneDataType.values()).filter(c -> Objects.equals(c.getValue(), value)).findFirst()
                .orElseThrow(()->new DataInvalidException(ErrorMessage.Zone.NOT_FOUND, Resources.NORMALIZER, "zone_data_type"));
        }
    }
}
