package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.mycompany.myapp.dto.BalancesDTO;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.web.rest.errors.Resources;

import java.util.Objects;
import java.util.stream.Stream;

public final class Block {
    public enum AffectedObjectType{
        Balance(3);

        @JsonValue
        private final Integer value;

        AffectedObjectType(Integer value) {
            this.value = value;
        }

        @JsonValue
        public Integer getValue() {
            return this.value;
        }

        @JsonCreator
        public static AffectedObjectType of(Integer value) {
            return Stream.of(AffectedObjectType.values()).filter(c -> Objects.equals(c.getValue(), value)).findFirst()
                .orElseThrow(()->new DataInvalidException(ErrorMessage.Block.AFFECTED_OBJECT_TYPE_NOT_FOUND, Resources.BLOCK, "afffected_object_type"));
        }
    }
}
