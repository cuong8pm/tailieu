package com.mycompany.myapp.domain.constant;

import java.util.Objects;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.mycompany.myapp.service.exception.DataInvalidException;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.web.rest.errors.Resources;

public enum BlockComponentType {
    
    BASIC(1), DISCOUNT(2), ACCUMULATE(3);
    
    @JsonValue
    private final Integer value;
    
    BlockComponentType(Integer value) {
        this.value = value;
    }
    public Integer getValue() {
        return this.value;
    }
    
    @JsonCreator
    public static BlockComponentType of(Integer value) {
        return Stream.of(BlockComponentType.values()).filter(c -> Objects.equals(c.getValue(), value)).findFirst()
            .orElseThrow(()->new DataInvalidException(ErrorMessage.Block.BLOCK_COMPONENT_TYPE_NOT_FOUND, Resources.BLOCK, "block_component_type"));
    }
}
