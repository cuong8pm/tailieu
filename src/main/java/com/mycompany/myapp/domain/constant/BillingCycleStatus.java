package com.mycompany.myapp.domain.constant;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum BillingCycleStatus {
    NON_ACTIVE(0), ACTIVE(1); 
    
    BillingCycleStatus(int value) {
        this.value = value;
    }
    
    @JsonValue
    private int value;

    public int getValue() {
        return this.value;
    }
    
    @JsonCreator
    public static BillingCycleStatus of(int value) {
        return Stream.of(BillingCycleStatus.values()).filter(c -> c.getValue() == value).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
