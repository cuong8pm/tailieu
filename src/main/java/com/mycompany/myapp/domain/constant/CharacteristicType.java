package com.mycompany.myapp.domain.constant;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CharacteristicType {
    CUST(0), SUB(1) , GROUP(3), OFFER(6), SYSTEM(7);
    
    @JsonValue
    private int value;
    
     CharacteristicType(Integer value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @JsonCreator
    public static CharacteristicType of(int value) {
        return Stream.of(CharacteristicType.values()).filter(c -> c.getValue() == value).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
    
}

