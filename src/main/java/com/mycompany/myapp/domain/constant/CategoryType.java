package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.stream.Stream;

public enum CategoryType {
    UNIT_TYPES(1),
    PARAMETER(2),
    RESERVE_INFO(3),
    ZONE(4),
    GEO_HOME_ZONE(5),
    BILLING_CYCLE(6),
    STATE_TYPE(7),
    STATE_GROUP(8),
    STATISTIC(9),
    SERVICE(10),
    BALANCES(11),
    ACCUMULATE_BALANCES(12),
    ACCOUNT_BALANCE_MAPPING(13),
    ACM_BALANCE_MAPPING(14),
    PCC_RULE(15),
    PEP_PROFILE(16),
    CHARACTERISTIC(18),
    NORMALIZER(19),
    RATE_TABLE(20),
    RATING_FILTER(21),
    BLOCK(22),
    PRICE_COMPONENT(23),
    ACTION_TYPE(24),
    EVENT(25),
    ACTION(26),
    OFFER_TEMPlATE(27),
    OFFER(28),
    BUILDER_TYPE(29),
    BUILDER(30),
    NESTED_OBJECT(-1),
    OFFER_VERSION(-2),
    OFFER_TEMPlATE_VERSION(-3),
    VERIFICATION(31),
    FUNCTION(32),
    ROUTER(35),
    CONDITION(33),
    FILTER(34),
    EXPRESSION(36),
    EVENT_POLICY(37);

    @JsonValue
    private final int value;

    CategoryType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @JsonCreator
    public static CategoryType of(int value) {
        return Stream.of(CategoryType.values()).filter(c -> c.getValue() == value).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    @JsonCreator
    public static CategoryType of(String input) {
        int value = Integer.parseInt(input);
        return Stream.of(CategoryType.values()).filter(c -> c.getValue() == value).findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
