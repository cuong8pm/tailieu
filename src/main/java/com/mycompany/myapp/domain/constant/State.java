package com.mycompany.myapp.domain.constant;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum State {
    IN_ACTIVE(0), ACTIVE(1), TESTING(2), SUSPEND(3), UNDEPLOY(4);
    
    @JsonValue
    private final int value;
    
    State(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
    
    @JsonCreator
    public static State of(int value) {
        return Stream.of(State.values()).filter(c -> c.getValue() == value).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }    
}
