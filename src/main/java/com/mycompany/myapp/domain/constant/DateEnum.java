package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum DateEnum {
    Sunday(1), Monday(2), Tuesday(3), Wednesday(4), Thursday(5), Friday(6), Saturday(7);

    @JsonValue
    private final Integer day;

    DateEnum(Integer day) {
        this.day = day;
    }

    public Integer getDay() {
        return this.day;
    }

    @JsonCreator
    public static DateEnum of(Integer value) {
        return Stream.of(DateEnum.values()).filter(c -> c.getDay().equals(value)).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
