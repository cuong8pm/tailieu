package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum StartType {
    NONE(1), CURRENT_TIME(2), DELTA(3);

    @JsonValue
    private final Integer value;

    StartType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static StartType of(Integer value) {
        return Stream.of(StartType.values()).filter(c -> c.getValue().equals(value)).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
    
//    @JsonCreator
    public static StartType of(Object value) {
        if (value instanceof String) {
            Integer intValue = Integer.parseInt(value.toString());
            return of(intValue);
        } else if (value instanceof Integer) {
            return of(((Integer)value).intValue());
        }
        return null;
            
    }
}
