package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.stream.Stream;

public enum ReferType {
    OwnerLevel(1),
    HomeZoneType(2),
    StateType(3),
    CALC_UNIT(4),
    PaymentType(5),
    BalTypeOwnerLevel(6),
    EffectDataType(7),
    ExpireDataType(8),
    MultiBalType(9),
    PeriodicType(10),
    IsPercentage(11),
    ThresholdType(12),
    MappingType(13),
    SendType(14),
    NormalizerType(15),
    NormalizerState(16),
    NormalizerStringCompareType(17),
    NormalizerNumberCompareType(18),
    NormalizerZoneType(19),
    NormalizerStartType(20),
    NormalizerCompareMode(22),
    FormulaType(23),
    SortingType(24),
    BlockFilterDecision(25),
    FormulaErrorCodeType(26),
    RatingFilter(27),
    BlockType(28),
    AffectedObjectType(29),
    BlockMode(30),
    PriceComponent(31),
    Event(32),
    OfferType(33),
    OfferRelationshipType(34),
    RedirectionType(35),
    State(36),
    SpecialMethod(37),
    CharSpecType(38),
    ValueType(39),
    VerificationType(58),
    ListType(59),
    FunctionType(56),
    DataType(43),
    RouterType(60),
    ExpressionInputType(41),
    OperatorNumber(42),
    ModuleName(40),
    EventObjectType(55),
    ExpressionValueType(50),
    ParameterValueType(51),
    ProfileCustomer(44),
    ProfileBalance(45),
    ProfileOffer(46),
    ProfileOther(47),
    BuilderValueType(53),
    BuilderDataType(54),
    OperatorString(48),
    FieldValueType(57);

    @JsonValue
    private int value;

    ReferType(int value) {
        this.value = value;
    }

    @JsonValue
    public int getValue() {
        return this.value;
    }

    @JsonCreator
    public static ReferType of(int value) {
        return Stream.of(ReferType.values()).filter(c -> c.getValue() == value).findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
