package com.mycompany.myapp.domain.constant;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AffectedObjectType {
    CUST(1), SUB(2), BALANCE(3), ACM_BALANCE(4), CHARACTERISTIC(5), OFFER(6), GROUP(7), OCSMSG(9);
    
    @JsonValue
    private final int value;
    
    AffectedObjectType(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
    
    @JsonCreator
    public static AffectedObjectType of(int value) {
        return Stream.of(AffectedObjectType.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
