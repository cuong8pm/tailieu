package com.mycompany.myapp.domain.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum BalancePercentageType {
    ABSOLUTE(1), PERCENTAGE(0);

    BalancePercentageType(int value) {
        this.value = value;
    }

    @JsonValue
    private int value;

    public int getValue() {
        return value;
    }

    @JsonCreator
    public static BalancePercentageType of(int value) {
        return Stream.of(BalancePercentageType.values()).filter(balance -> balance.getValue() == value).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
