package com.mycompany.myapp.domain.constant;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FormulaType {
    NORMAL(0), SKIP(1), DENY(2), EXIT(3), SORT_PC(4), DYNAMIC_RESERVE(5),
    BLOCK_FILTER(6), ACTION_PRIORITY(7), EVENT_ANALYSIS(8);

    @JsonValue
    private final int value;

    FormulaType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @JsonCreator
    public static FormulaType of(int value) {
        return Stream.of(FormulaType.values()).filter(c -> c.getValue() == value).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
