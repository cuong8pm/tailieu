package com.mycompany.myapp.domain.constant;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CalcUnitType {
    MINUTES(1), HOUR(2), DAY(3), DAY_OF_WEEK(4), WEEK(5), DAY_OF_MONTH(6), MONTH(7), YEAR(8), MONTH_FOR_MAIN_PP(9), HOUR_OF_DAY(10);

    CalcUnitType(int value) {
        this.value = value;
    }
    @JsonValue
    private int value;

    public int getValue() {
        return this.value;
    }
    
    @JsonCreator
    public static CalcUnitType of(int value) {
        return Stream.of(CalcUnitType.values()).filter(c -> c.getValue() == value).findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
