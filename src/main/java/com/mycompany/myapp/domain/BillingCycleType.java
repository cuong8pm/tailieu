package com.mycompany.myapp.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@Entity
@Table(name = "billing_cycle_type")
public class BillingCycleType implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BILLING_CYCLE_TYPE_ID", nullable = false)
    private Long id;

    @Column(name = "CALC_UNIT_ID")
    private Long calcUnitId;

    @Column(name = "BILLING_CYCLE_TYPE_NAME", nullable = false)
    private String name;

    @Column(name = "QUANTITY", nullable = false)
    private Long quantity;

    @Column(name = "BEGIN_DATE", nullable = false)
    private Timestamp beginDate;

    @Column(name = "REMARK")
    private String description;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "FROM_OF_DAY")
    private Integer fromOfDay;

    @Column(name = "POS_INDEX")
    private Integer posIndex;

    @Column(name = "CYCLE_MASK")
    private Long cycleMask;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static final class FieldNames {
        private FieldNames() {}

        public static final String POS_INDEX = "posIndex";
    }

}
