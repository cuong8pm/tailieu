package com.mycompany.myapp.domain;

import javax.persistence.*;

@Entity
@Table(name = "geo_home_zone")
public class GeoHomeZone implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GEO_HOME_ZONE_ID", nullable = false)
    private Long id;

    @Column(name = "GEO_HOME_ZONE_NAME", nullable = false)
    private String name;

    @Column(name = "GEO_HOME_ZONE_CODE")
    private String geoHomeZoneCode;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "GEO_HOME_ZONE_TYPE")
    private Integer geoHomeZoneType;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "GEO_HOME_ZONE_ID", insertable = false, updatable = false)
    private String tempId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeoHomeZoneCode() {
        return geoHomeZoneCode;
    }

    public void setGeoHomeZoneCode(String geoHomeZoneCode) {
        this.geoHomeZoneCode = geoHomeZoneCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getGeoHomeZoneType() {
        return geoHomeZoneType;
    }

    public void setGeoHomeZoneType(Integer geoHomeZoneType) {
        this.geoHomeZoneType = geoHomeZoneType;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    @Transient
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "GeoHomeZone{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", geoHomeZoneCode='" + geoHomeZoneCode + '\'' +
            ", remark='" + remark + '\'' +
            ", geoHomeZoneType=" + geoHomeZoneType +
            ", categoryId=" + categoryId +
            ", domainId=" + domainId +
            ", posIndex=" + posIndex +
            '}';
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public static final class FieldNames {
        private FieldNames() {}

        public static final String POS_INDEX = "posIndex";
    }
}
