package com.mycompany.myapp.domain;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "geo_net_zone")
@Data
public class GeoNetZone {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GEO_NET_ZONE_ID", nullable = false)
    private Long id;

    @Column(name = "CELL_ID", nullable = false)
    private Long cellId;

    @Column(name = "CELL_ID", insertable = false, updatable = false)
    private String tempCellId;

    @Column(name = "GEO_HOME_ZONE_ID", nullable = false)
    private Long geoHomeZoneId;

    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @CreatedDate
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp updateDate;

    @Column(name = "UPDATE_DATE", insertable = false, updatable = false)
    private String tempUpdateDate;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE", insertable = false, updatable = false)
    private Calendar magic;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "GeoNetZone{" +
            "id=" + id +
            ", cellId=" + cellId +
            ", geoHomeZoneId=" + geoHomeZoneId +
            ", updateDate=" + updateDate +
            ", domainId=" + domainId +
            '}';
    }

    public static final class FieldNames {
        private FieldNames() {}

        public static final String CELL_ID = "cellId";
    }
}
