package com.mycompany.myapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "profile_pep_pcc")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PepProfilePcc implements OCSMoveableEntity {

    @Id
    @Column(name = "PROFILE_PEP_PCC_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PCC_RULE_ID", nullable = false)
    private Long pccRuleId;

    @Column(name = "PROFILE_PEP_ID", nullable = false)
    private Long pepProfileId;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "POS_INDEX")
    private Integer posIndex;

    @Column(name = "PROFILE_PEP_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "PCC_RULE_ID", insertable = false, updatable = false)
    private Long childId;

    // not use
    @Column(name = "PROFILE_PEP_PCC_ID", insertable = false, updatable = false)
    private String name;

    public PepProfilePcc(Long id, Long pccRuleId, Long pepProfileId, Integer domainId) {
        this.id = id;
        this.pccRuleId = pccRuleId;
        this.pepProfileId = pepProfileId;
        this.domainId = domainId;
    }

    @Override
    public void setCategoryId(Long categoryId) {

    }
}
