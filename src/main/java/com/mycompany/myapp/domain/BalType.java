package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "bal_type")
public class BalType implements Serializable, OCSMoveableEntity, OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BAL_TYPE_ID", nullable = false)
    private Long id;

    @Column(name = "BAL_TYPE_NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "EXTERNAL_ID", nullable = false)
    private String externalId;

    @Column(name = "BAL_TYPE_TYPE", nullable = false)
    private Integer balTypeType;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "IS_ACM")
    private boolean acm;

    @Column(name = "PAYMENT_TYPE", nullable = false)
    private Long paymentType;

    @Column(name = "IS_CURRENCY")
    private String currency;

    @Column(name = "EFF_DATE_TYPE")
    private Integer effDateType;

    @Column(name = "EFF_DATE_DATA")
    private Integer effDateData;

    @Column(name = "EFF_DATE")
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp effDate;

    @Column(name = "EXP_DATE_TYPE")
    private Integer expDateType;

    @Column(name = "EXP_DATE_DATA")
    private Integer expDateData;

    @Column(name = "EXP_DATE")
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp expDate;

    @Column(name = "RECURING_TYPE")
    private Integer recuringType;

    @Column(name = "RECURING_PERIOD")
    private Integer recuringPeriod;

    @Column(name = "UNIT_TYPE_ID", nullable = false)
    private Long unitTypeId;

    @Column(name = "PERIODIC_PERIOD_TYPE")
    private Integer periodicPeriodType;

    @Column(name = "PERIODIC_PERIOD")
    private Integer periodicPeriod;

    @Column(name = "WINDOW_SIZE")
    private Integer windowSize;

    @Column(name = "LOW_WATER_MARK_LEVEL")
    private Integer lowWaterMarkLevel;

    @Column(name = "HIGH_WATER_MARK_LEVEL")
    private Integer highWaterMarkLevel;

    @Column(name = "BILLING_CYCLE_TYPE_ID")
    private Integer billingCycleTypeId;

    @Column(name = "BAL_LEVEL")
    private Integer balLevel;

    @Column(name = "PERCISION")
    private Long percision;

    @Column(name = "IS_CLEANABLE")
    private Boolean cleanable;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "BAL_TYPE_PERCISION")
    private Long balTypePercision;

    @Column(name = "IS_CLEARABLE")
    private Boolean clearable;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Override
    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }
    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }
}
