package com.mycompany.myapp.domain.normalizer;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import lombok.Data;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "normalizer")
@Data
public class Normalizer implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "NORMALIZER_ID", nullable = false)
    private Long id;

    @Column(name = "NORMALIZER_NAME", nullable = false)
    private String name;

    @Column(name = "NORMALIZER_TYPE", nullable = false)
    private Integer normalizerType;

    @Column(name = "STATE", nullable = false)
    private Integer state;

    @Column(name = "DEFAULT_VALUE")
    private Long defaultValue;

    @Column(name = "INPUT_FIELDS")
    private String inputField;

    @Column(name = "SPECIAL_FIELDS")
    private String specialField;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "DESCRIPTIONS")
    private String description;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;
    public static final class FieldNames {
        private FieldNames() {}

        public static final String ID = "id";
        public static final String POS_INDEX = "posIndex";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }
}
