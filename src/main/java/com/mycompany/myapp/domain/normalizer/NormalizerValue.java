package com.mycompany.myapp.domain.normalizer;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import lombok.Data;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "normalizer_values")
@Data
public class NormalizerValue implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "NORMALIZER_VALUE_ID", nullable = false)
    private Long id;

    @JsonView(NormalizerValue.class)
    @Column(name = "VALUE_NAME")
    private String name;

    @JsonView(NormalizerValue.class)
    @Column(name = "VALUE_ID")
    private Long valueId;

    @Column(name = "DESCRIPTION")
    private String description;

    @JsonView(NormalizerValue.class)
    @Column(name = "COLOR")
    private String colorText;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @JsonView(NormalizerValue.class)
    @Column(name = "COLORBG")
    private String colorBG;

    @Column(name = "NORMALIZER_ID")
    private Long normalizerId;

    @Column(name = "NORMALIZER_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Transient
    private String tempId;

    @Transient
    private boolean isDefault;

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.normalizerId = parentId;
    }

    @Override
    public void setCategoryId(Long categoryId) {

    }
}
