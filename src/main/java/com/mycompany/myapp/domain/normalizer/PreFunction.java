package com.mycompany.myapp.domain.normalizer;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "pre_function")
@Data
public class PreFunction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "pre_function_ID", nullable = false)
    private Integer id;

    @Column(name = "FUNCTION_NAME", nullable = false)
    private String functionName;

    @Column(name = "NUMBER_PARAM", nullable = false)
    private Integer numberParam;

}
