package com.mycompany.myapp.domain.normalizer;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import lombok.Data;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "normalizer_params")
@Data
public class NormalizerParam implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "NORMALIZER_PARAM_ID", nullable = false)
    private Long id;

    @Column(name = "CONFIG_INPUT")
    private String configInput;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "NORMALIZER_ID")
    private Long normalizerId;

    @Column(name = "VALUE_ID")
    private Long valueId;

    @Column(name = "CONFIG_INPUT", insertable = false, updatable = false)
    private String name;

    @Column(name = "NORMALIZER_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "NORMALIZER_PARAM_ID", insertable = false, updatable = false)
    private Integer posIndex;
    @Transient
    private String tempId;


    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
        this.normalizerId = parentId;
    }

    @Override
    public void setCategoryId(Long categoryId) {

    }
}
