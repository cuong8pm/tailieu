package com.mycompany.myapp.domain;

import com.mycompany.myapp.domain.ratetable.RateTable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rating_filter_rate_table_map")
@Data
@NoArgsConstructor
public class RatingFilterRateTableMap implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "RATING_FILTER_ID", nullable = false)
    private Long ratingFilterId;

    @Column(name = "RATE_TABLE_ID", nullable = false)
    private Long rateTableId;

    @Column(name = "`INDEX`", nullable = false)
    private Integer posIndex;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "RATING_FILTER_ID", insertable = false, updatable = false)
//    private RatingFilter ratingFilter;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "RATE_TABLE_ID", insertable = false, updatable = false)
//    private RateTable rateTable;

    @Column(name = "RATING_FILTER_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "RATE_TABLE_ID", insertable = false, updatable = false)
    private Long childId;

    @Override
    public void clearId() {
        this.id = null;
    }

    // ignore
    @Column(name = "RATING_FILTER_ID", insertable = false, updatable = false)
    private String name;

    public RatingFilterRateTableMap(Long id, Long ratingFilterId, Long rateTableId, Integer posIndex,
                                    Integer domainId) {
        this.id = id;
        this.ratingFilterId = ratingFilterId;
        this.rateTableId = rateTableId;
        this.posIndex = posIndex;
        this.domainId = domainId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.rateTableId = childId;
    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.ratingFilterId = parentId;
    }

    public static class FieldNames {
        private FieldNames() {};

        public static final String POS_INDEX = "posIndex";
    }
}
