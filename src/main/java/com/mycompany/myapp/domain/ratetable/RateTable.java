package com.mycompany.myapp.domain.ratetable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;

import com.mycompany.myapp.domain.RatingFilterRateTableMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "rate_table")
public class RateTable implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "RATE_TABLE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "RATE_TABLE_NAME")
    private String name;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "DEFAULT_FORMULA_ID")
    private Long defaultFormulaId;

    @Column(name = "UNIT_TYPE_ID", nullable = false)
    private Long unitTypeId;

    @Column(name = "DEFAULT_COMBINED_VALUE", nullable = false)
    private String defaultCombinedValue;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }


    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }
}
