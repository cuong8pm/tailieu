package com.mycompany.myapp.domain.ratetable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "rate_table_rows")
public class RateTableRow implements OCSCloneableMap {

    @Id
    @Column(name = "RATE_TABLE_ROW_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "FORMULA_ID")
    private Long formulaId;

    @Column(name = "RATE_TABLE_ID", nullable = false)
    private Long rateTableId;

    @Column(name = "COMBINED_VALUE", nullable = false)
    private String combinedValue;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "FORMULA_ID", insertable = false, updatable = false)
    private Long childId;

    @Column(name = "RATE_TABLE_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Override
    public void clearId() {
        this.id = null;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.formulaId = childId;
    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.rateTableId = parentId;
    }
}
