package com.mycompany.myapp.domain.ratetable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

import com.mycompany.myapp.domain.OCSCloneableMap;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "rate_table_columns")
public class RateTableColumn implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "RATE_TABLE_COLUMN_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @Column(name = "NORMALIZER_ID", nullable = false)
    private Long normalizerId;

    @Column(name = "RATE_TABLE_ID", nullable = false)
    private Long rateTableId;

    @Column(name = "COLUMN_INDEX", nullable = false)
    private Integer columnIndex;

    @Column(name = "COLUMN_INDEX", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "DISPLAY_NAME", nullable = false)
    private String displayName;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "RATE_TABLE_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "NORMALIZER_ID", insertable = false, updatable = false)
    private Long childId;
    // not use
    @Column(name = "RATE_TABLE_COLUMN_ID", insertable = false, updatable = false)
    private String name;

    @Override
    public void clearId() {
        this.id = null;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.normalizerId = childId;
    }

    @Override
    public void setParentMappingId(Long parentId) {
        this.rateTableId = parentId;
    }
}
