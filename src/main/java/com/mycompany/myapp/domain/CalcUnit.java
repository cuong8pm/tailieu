package com.mycompany.myapp.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.constant.CalcUnitType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "calc_unit")
public class CalcUnit implements OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CALC_UNIT_ID", nullable = false)
    private Long id;

    @Column(name = "CALC_UNIT_NAME", nullable = false)
    private String name;

    @Column(name = "CALC_UNIT_TYPE", nullable = false)
    private CalcUnitType calcUnitType;

    @Column(name = "REMARK")
    private String description;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

}
