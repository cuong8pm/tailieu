package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mycompany.myapp.domain.constant.ReferType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "refer_table")
public class ReferTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "refer_table_id", nullable = false)
    @JsonProperty("referId")
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @Column(name = "refer_type")
    private ReferType referType;

    @Column(name = "name")
    private String name;

    @JsonProperty("id")
    @Column(name = "value")
    private Integer value;

    @JsonIgnore
    @Column(name = "remark")
    private String remark;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    public ReferTable(Integer value, String name){
        this.value =value;
        this.name =name;
    }
}
