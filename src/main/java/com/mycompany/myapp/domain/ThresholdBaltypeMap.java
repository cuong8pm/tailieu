package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "threshold_baltype_map")
@Data
public class ThresholdBaltypeMap implements Serializable, OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "THRESHOLD_BALTYPE_MAP_ID", nullable = false)
    private Long id;

    @Column(name = "THRESHOLD_ID", nullable = false)
    private Long thresholdId;

    @Column(name = "BALTYPE_ID", nullable = false)
    private Long baltypeId;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "BALTYPE_ID", insertable = false, updatable = false)
    private Long parentId;
    
    @Column(name = "THRESHOLD_ID", insertable = false, updatable = false)
    private Long childId;

    @Override
    public void clearId() {
        this.id = null;
    }
    
    @Override
    public void setParentMappingId(Long parentId) {
        this.baltypeId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.thresholdId = childId;
    }

    @Override
    public Integer getPosIndex() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {
        // TODO Auto-generated method stub

    }
    // @ManyToOne(fetch = FetchType.LAZY)
    // @NotNull
    // @JsonIgnoreProperties(value = "thresholdList", allowSetters = true)
    // private Threshold threshold;
    //
    // @ManyToOne(fetch = FetchType.LAZY)
    // @NotNull
    // @JsonIgnoreProperties(value = "balTypeList", allowSetters = true)
    // private BalType balType;
}
