package com.mycompany.myapp.domain.actiontype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.mycompany.myapp.domain.OCSCloneableMap;

import lombok.Data;

@Entity
@Table(name = "event_action_type_map")
@Data
public class EventActionTypeMap implements OCSCloneableMap {

    @Id
    @Column(name = "EVENT_ACTION_TYPE_MAP_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;
    
    @Column(name = "ACTION_TYPE_ID", nullable = false)
    private Long actionTypeId;
    
    @Column(name = "EVENT_ID", nullable = false)
    private Long eventId;
    
    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;
    
    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "EVENT_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "ACTION_TYPE_ID", insertable = false, updatable = false)
    private Long childId;

    @Override
    public void setParentMappingId(Long parentId) {
        this.eventId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.actionTypeId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }
}
