package com.mycompany.myapp.domain.actiontype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.OCSCloneableEntity;

import lombok.Data;

@Entity
@Table(name = "action_type")
@Data
public class ActionType implements OCSCloneableEntity{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ACTION_TYPE_ID", nullable = false)
    private Long id;
    
    @Column(name = "ICON", nullable = false)
    private String icon;
    
    @Column(name = "ACTION_TYPE_NAME", nullable = false)
    private String name;
    
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;
    
    @Column(name = "FOR_PROVISIONING ", nullable = false)
    private boolean forProvisioning;
    
    @Column(name = "CATEGORY_ID ", nullable = false)
    private Long categoryId;
    
    @Column(name = "DOMAIN_ID")
    private Integer domainId;
    
    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;
    
    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = null;
    }
}
