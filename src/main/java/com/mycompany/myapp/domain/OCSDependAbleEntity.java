package com.mycompany.myapp.domain;

public interface OCSDependAbleEntity extends OCSCloneableEntity {
    String getObjectType();
}
