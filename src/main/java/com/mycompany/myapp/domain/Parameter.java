package com.mycompany.myapp.domain;

import org.hibernate.annotations.Type;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "parameter")
public class Parameter implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PARAMETER_ID", nullable = false)
    private Long id;

    @Column(name = "OWNER_LEVEL", nullable = false)
    private Integer ownerLevel;

    @Column(name = "PARAMETER_VALUE")
    private String parameterValue;

    @Column(name = "PARAMETER_NAME", nullable = false)
    private String name;

    @Column(name = "FOR_TEMPLATE", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean forTemplate;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;
    
    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOwnerLevel() {
        return ownerLevel;
    }

    public void setOwnerLevel(Integer ownerLevel) {
        this.ownerLevel = ownerLevel;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getForTemplate() {
        return forTemplate;
    }

    public void setForTemplate(boolean forTemplate) {
        this.forTemplate = forTemplate;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDomainId() {
        return domainId;
    }

    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Parameter)) return false;
        Parameter parameter = (Parameter) o;
        return Objects.equals(getId(), parameter.getId()) &&
            Objects.equals(getOwnerLevel(), parameter.getOwnerLevel()) &&
            Objects.equals(getParameterValue(), parameter.getParameterValue()) &&
            Objects.equals(getName(), parameter.getName()) &&
            Objects.equals(getForTemplate(), parameter.getForTemplate()) &&
            Objects.equals(getCategoryId(), parameter.getCategoryId()) &&
            Objects.equals(getRemark(), parameter.getRemark()) &&
            Objects.equals(getDomainId(), parameter.getDomainId()) &&
            Objects.equals(getPosIndex(), parameter.getPosIndex());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getOwnerLevel(), getParameterValue(), getName(), getForTemplate(), getCategoryId(), getRemark(), getDomainId(), getPosIndex());
    }

    @Override
    public String toString() {
        return "Parameter{" +
            "id=" + id +
            ", ownerLevel=" + ownerLevel +
            ", parameterValue='" + parameterValue + '\'' +
            ", name='" + name + '\'' +
            ", forTemplate=" + forTemplate +
            ", categoryId=" + categoryId +
            ", remark='" + remark + '\'' +
            ", domainId=" + domainId +
            ", posIndex=" + posIndex +
            '}';
    }

    @Transient
    @Override
    public Long getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public static final class FieldNames {

        private FieldNames () {}
        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
        public static final String POS_INDEX = "posIndex";
    }
    
    public static final class Length {
        private Length() {}
        public static final int NAME = 200;
    }
}
