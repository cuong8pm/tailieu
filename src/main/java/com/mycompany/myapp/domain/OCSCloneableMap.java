package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface OCSCloneableMap extends Serializable {

    Long getParentId();
    void setParentMappingId(Long parentId);

    Long getChildId();
    void setChildMappingId(Long childId);

    void clearId();

    Integer getPosIndex();
    void setPosIndex(Integer posIndex);

    default Integer getComponentType() {
        return null;
    }

    default void setComponentType(Integer componentType) {

    }
}
