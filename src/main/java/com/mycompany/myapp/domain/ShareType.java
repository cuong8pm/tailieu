package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
public class ShareType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SHARE_TYPE_ID", nullable = false)
    private Integer id;

    @Column(name = "SHARE_TYPE_NAME", nullable = false)
    private String name;
    @JsonIgnore
    @Column(name = "DOMAIN_ID")
    private Integer domainId;

}
