package com.mycompany.myapp.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "zone_data")
public class ZoneData implements OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ZONE_DATA_ID", nullable = false)
    private Long id;

    @Column(name = "ZONE_DATA_VALUE")
    private String zoneDataValue;

    @Column(name = "ZONE_ID")
    private Long zoneId;

    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @CreatedDate
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp updateDate;

    @Column(name = "UPDATE_DATE", insertable = false, updatable = false)
    private String tempUpdateDate;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "ZONE_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    // This properties is used to avoid PropertyReferenceException
    @Column(name = "ZONE_DATA_VALUE", insertable = false, updatable = false)
    private String name;

    @Column(name = "ZONE_DATA_ID", insertable = false, updatable = false)
    private String tempId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public void setCategoryId(Long categoryId) {

    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
    }

}
