package com.mycompany.myapp.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pcc_rule")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PccRule implements Serializable, OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PCC_RULE_ID", nullable = false)
    private Long id;

    @Column(name = "PCC_RULE_ID", insertable = false, updatable = false)
    private String tempId;

    @Column(name = "PCC_BASE_NAME", nullable = false)
    private String name;

    @Column(name = "PRECEDENCE", nullable = false)
    private Integer precedence;

    @Column(name = "ACTIVE_TIME")
    private Timestamp activeTime;

    @Column(name = "DEACTIVE_TIME")
    private Timestamp deactiveTime;

    @Column(name = "TYPE", nullable = false)
    private Integer sendType;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "PRIORITY", nullable = false)
    private Integer priority;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "PRIORITY", insertable = false, updatable = false)
    private String tempPriority;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
        public static final String POS_INDEX = "posIndex";
    }
}
