package com.mycompany.myapp.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "zone_map")
public class ZoneMap implements Serializable, OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ZONE_MAP_ID", nullable = false)
    private Long id;

    @Column(name = "ZONE_MAP_NAME")
    private String name;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;

    @Override
    @Transient
    public Long getParentId() {
        return parentId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public Integer getDomainId() {
        return domainId;
    }

    @Override
    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    public String toString() {
        return "ZoneMap{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", remark='" + remark + '\'' +
            ", domainId=" + domainId +
            ", categoryId=" + categoryId +
            ", posIndex=" + posIndex +
            '}';
    }

    public static final class FieldNames {

        private FieldNames () {}
        public static final String NAME = "name";
        public static final String DESCRIPTION = "remark";
        public static final String POS_INDEX = "posIndex";
    }
}
