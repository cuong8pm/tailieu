package com.mycompany.myapp.domain;

public interface OCSCloneableEntity extends OCSMoveableEntity {
    String getName();

    void setName(String name);
}
