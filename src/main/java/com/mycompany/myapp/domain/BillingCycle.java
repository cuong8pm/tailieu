package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.constant.BillingCycleStatus;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@Entity
@Table(name = "billing_cycle")
public class BillingCycle implements OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BILLING_CYCLE_ID", nullable = false)
    private Long id;

    @Column(name = "BILLING_CYCLE_TYPE_ID", nullable = false)
    private Long billingCycleTypeId;

    @Column(name = "CYCLE_BEGIN_DATE")
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp cycleBeginDate;

    @Column(name = "CYCLE_END_DATE")
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp cycleEndDate;

    @Column(name = "STATE")
    private BillingCycleStatus state;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "BILLING_CYCLE_TYPE_ID", insertable = false, updatable = false)
    private Long parentId;
    
    @Column(name = "CYCLE_BEGIN_DATE", insertable = false, updatable = false)
    private String beginDateString;
    
    @Column(name = "CYCLE_END_DATE", insertable = false, updatable = false)
    private String endDateString;
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    public static final class FieldNames {
        private FieldNames() {}
        
        public static final String ID = "id";
        public static final String POS_INDEX = "posIndex";
        public static final String END_DATE = "cycleEndDate";
        public static final String BEGIN_DATE = "cycleBeginDate";
    }
}
