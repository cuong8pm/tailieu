package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface OCSBaseEntity extends Serializable {
    Long getId();

    void setId(Long id);

    Integer getDomainId();

    void setDomainId(Integer domainId);
}
