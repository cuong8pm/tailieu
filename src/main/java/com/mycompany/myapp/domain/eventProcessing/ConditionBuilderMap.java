package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "condition_builder_map")
@Data
public class ConditionBuilderMap implements OCSCloneableMap {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "condition_builder_map_id", nullable = false)
    private Long id;

    @Column(name = "condition_id")
    private Long conditionId;

    @Column(name = "builder_id")
    private Long builderId;

    @Column(name = "condition_ID", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "builder_id", insertable = false, updatable = false)
    private Long childId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    // ignore this property
    @Column(name = "domain_id",insertable = false, updatable = false)
    private Integer posIndex;

    @Override
    public void setParentMappingId(Long parentId) {
        this.conditionId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.builderId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }
}
