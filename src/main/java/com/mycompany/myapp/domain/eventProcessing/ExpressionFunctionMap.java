package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "expression_function_map")
public class ExpressionFunctionMap implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expression_function_map_id", nullable = false)
    private Long id;

    @Column(name = "function_id")
    private Long functionId;

    @Column(name = "expression_id")
    private Long expressionId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

}
