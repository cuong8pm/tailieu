package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "builder_type")
public class BuilderType implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "builder_type_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "builder_type_name")
    private String name;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "category_id", insertable = false,updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }

}
