package com.mycompany.myapp.domain.eventProcessing;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "filter_ignore_profile")
@Data
public class FilterIgnoreProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "filter_ignore_profile_id", nullable = false)
    private Long id;

    @Column(name = "filter_id", nullable = false)
    private Long filterId;

    @Column(name = "refer_table_id", nullable = false)
    private Long referTableId;

    @Column(name = "domain_id",nullable = false)
    private Integer domainId;

}
