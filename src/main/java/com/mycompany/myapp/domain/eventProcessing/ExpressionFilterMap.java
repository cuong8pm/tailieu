package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "expression_filter_map")
public class ExpressionFilterMap implements OCSCloneableMap {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expression_filter_map_id", nullable = false)
    private Long id;

    @Column(name = "expression_id", nullable = false)
    private Long expressionId;

    @Column(name = "filter_id", nullable = false)
    private Long filterId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "filter_id", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "expression_id", insertable = false, updatable = false)
    private Long childId;

    // ignore this property
    @Column(name = "domain_id", insertable = false, updatable = false)
    private Integer posIndex;

    @Override
    public void setParentMappingId(Long parentId) {
        this.filterId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.expressionId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }
}
