package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "builder")
@Data
@NoArgsConstructor
@TrimString(fieldNames = {"name", "description"})
public class Builder implements EventPoliciesEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "builder_id", nullable = false)
    private Long id;

    @Column(name = "builder_name")
    private String name;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "pre_process")
    private String preProcess;

    @Column(name = "post_process")
    private String postProcess;

    @Column(name = "reproduce")
    private String reProduce;

    @Column(name = "builder_type_id")
    private Long builderTypeId;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentId;

    //ignore this property
    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentObjectId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
        public static final String ID = "id";
        public static final String BUILDER_TYPE_ITEM_NAME = "builderTypeItemName";
    }

    public static class Length {
        private Length() {
        }

        public static final Integer NAME = 255;
    }

}
