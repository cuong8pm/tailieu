package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "expression_condition_map")
public class ExpressionConditionMap implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expression_condition_map_id", nullable = false)
    private Long id;

    @Column(name = "expression_id", nullable = false)
    private Long expressionId;

    @Column(name = "condition_id", nullable = false)
    private Long conditionId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "condition_id", insertable = false,updatable = false)
    private Long parentId;

    @Column(name = "expression_id", insertable = false,updatable = false)
    private Long childId;

    // ignore this property
    @Column(name = "domain_id", insertable = false,updatable = false)
    private Integer posIndex;

    @Override
    public void setParentMappingId(Long parentId) {
        this.conditionId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.expressionId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }

}
