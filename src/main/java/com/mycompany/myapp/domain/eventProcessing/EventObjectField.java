package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "event_object_field")
@Data
public class EventObjectField implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_object_field_id", nullable = false)
    private Long id;

    @Column(name = "event_object_field_name")
    private String name;

    @Column(name = "event_object_id")
    private Long eventObjectId;

    @Column(name = "model_type")
    private Integer modelType;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "is_primitive")
    private boolean isPrimitive;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "source")
    private String source;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

}
