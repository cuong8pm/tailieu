package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "router")
@Data
@TrimString(fieldNames = { "name", "description" })
public class Router implements EventPoliciesEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "router_id", nullable = false)
    private Long id;

    @Column(name = "router_name")
    private String name;

    @Column(name = "properties")
    private String properties;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "router_type_id")
    private Long routerTypeId;

    @Column(name = "modify_date")
    private Timestamp modifyDate;

    //ignore these properties
    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "category_id", nullable = false)
    private Long categoryId;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentObjectId;

    public static final class FieldNames {

        private FieldNames() {}

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {

        private Length() {}

        public static final Integer NAME = 255;
    }
}
