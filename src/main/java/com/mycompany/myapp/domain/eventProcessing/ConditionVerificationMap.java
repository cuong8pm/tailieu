package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableMap;
import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "condition_verification_map")
@Data
public class ConditionVerificationMap implements Serializable, OCSCloneableMap {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "condition_verification_map_id", nullable = false)
    private Long id;

    @Column(name = "condition_id", nullable = false)
    private Long conditionId;

    @Column(name = "verification_id", nullable = false)
    private Long verificationId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "condition_id", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "verification_id", insertable = false, updatable = false)
    private Long childId;

    @Override
    public void setParentMappingId(Long parentId) {
        this.conditionId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.verificationId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }

    @Override
    public Integer getPosIndex() {
        // TODO Auto-generated method stub

        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {
        // TODO Auto-generated method stub

    }
}
