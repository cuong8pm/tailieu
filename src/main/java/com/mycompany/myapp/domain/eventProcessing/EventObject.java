package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@Table(name = "event_object")
public class EventObject implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_object_id", nullable = false)
    private Long id;

    @Column(name = "event_object_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "type")
    private Integer type;

}
