package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.OCSCloneableMap;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "builder_type_router_map")
public class BuilderTypeRouterMap implements OCSCloneableMap {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "builder_type_router_map_id", nullable = false)
    private Long id;

    @Column(name = "builder_type_id")
    private Long builderTypeId;

    @Column(name = "router_id")
    private Long routerId;

    @Column(name = "validation")
    private String validation;

    @Column(name = "description")
    private String description;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "builder_type_id", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "router_id", insertable = false, updatable = false)
    private Long childId;

    @Override
    public void setParentMappingId(Long parentId) {
        this.builderTypeId = parentId;
    }

    @Override
    public void setChildMappingId(Long childId) {
        this.routerId = childId;
    }

    @Override
    public void clearId() {
        this.id = null;
    }

    @Override
    public Integer getPosIndex() {
        return null;
    }

    @Override
    public void setPosIndex(Integer posIndex) {

    }

}
