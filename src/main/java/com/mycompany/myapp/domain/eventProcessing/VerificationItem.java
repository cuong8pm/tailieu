package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Entity
@Table(name = "verification_item")
@Data
@TrimString(fieldNames = { "itemId" })
public class VerificationItem implements Serializable, OCSBaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "verification_item_id", nullable = false)
    private Long id;

    @Column(name = "verification_id")
    private Long verificationId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "item_id")
    private String itemId;

    public static final class FieldNames {

        private FieldNames() {}

        public static final String ID = "id";
        public static final String ITEM_ID = "itemId";
    }
}
