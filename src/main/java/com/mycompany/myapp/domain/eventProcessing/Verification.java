package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "verification")
@TrimString(fieldNames = {"name", "description"})
public class Verification implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "verification_id", nullable = false)
    private Long id;

    @Column(name = "verification_name")
    private String name;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "description")
    private String description;

    @Column(name = "list_type")
    private Long listType;

    @Column(name = "verification_type")
    private Long verificationType;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
        public static final String ITEM_ID_NUMBER = "itemIdNumber";
    }
}
