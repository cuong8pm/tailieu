package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "expression")
@Data
public class Expression implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expression_id", nullable = false)
    private Long id;

    @Column(name = "value")
    private String value;

    @Column(name = "input_type")
    private Integer inputType;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "input")
    private String input;

    @Column(name = "operator")
    private Integer operator;

    @Column(name = "linked_id")
    private Long linkedId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "value_type")
    private Integer valueType;

    //ignore these properties
    @Column(name = "linked_id",insertable = false,updatable = false)
    private Long categoryId;

    @Column(name = "data_type", insertable = false,updatable = false)
    private Integer posIndex;

    @Column(name = "linked_id",insertable = false,updatable = false)
    private Long parentId;

    @Column(name = "operator",insertable = false,updatable = false)
    private String name;

    public static class InputType {
        private InputType(){}

        public static final Integer FUNCTION = 1;
        public static final Integer OBJECT = 0;
    }
    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }
}
