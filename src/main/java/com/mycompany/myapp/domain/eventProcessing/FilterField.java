package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "filter_field")
@Data
public class FilterField implements OCSBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "filter_field_id")
    private Long id;

    @Column(name = "filter_id")
    private Long filterId;

    @Column(name = "field_id")
    private Long fieldId;

    @Column(name = "value")
    private String value;

    @Column(name = "value_type")
    private Integer valueType;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "input_type")
    private String inputType;

    @Column(name = "linked_id")
    private Long linkedId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;
}
