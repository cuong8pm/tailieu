package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "`filter`")
@Data
public class Filter implements EventPoliciesEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "filter_id", nullable = false)
    private Long id;

    @Column(name = "filter_name")
    private String name;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "description")
    private String description;

    @Column(name = "router_id")
    private Long routerId;

    @Column(name = "module_id")
    private Long moduleId;

    @Column(name = "event_policy_id")
    private Long eventPolicyId;

    @Column(name = "modify_date")
    private Timestamp modifyDate;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public Long getParentObjectId() {
        return this.eventPolicyId;
    }

    @Override
    public void setParentObjectId(Long parentObjectId) {
        this.eventPolicyId = parentObjectId;
    }


    public static final class FieldNames {

        private FieldNames() {
        }
        public static final String FIELD_ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }

}
