package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;

public interface EventPoliciesEntity  extends OCSCloneableEntity {
    String getDescription();

    void setDescription(String description);

    Long getParentObjectId();

    void setParentObjectId(Long parentObjectId);
}
