package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "function_param_instance")
public class FunctionParamInstance implements OCSBaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "function_param_instance_id", nullable = false)
    private Long id;

    @Column(name = "function_parameter_id", nullable = false)
    private Long functionParameterId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "owner_id")
    private Long ownerId;

    @Column(name = "owner_type")
    private Integer ownerType;

    @Column(name = "value")
    private String value;

    @Column(name = "value_type")
    private Integer valueType;
}
