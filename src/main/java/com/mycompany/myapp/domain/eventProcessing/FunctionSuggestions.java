package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "function_suggestions")
public class FunctionSuggestions implements Serializable , OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "function_suggestions_id", nullable = false)
    private Long id;

    @Column(name = "field_id")
    private Long fieldId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "function_id")
    private Long functionId;

}
