package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name = "field_id_ocs")
public class FieldIdOcs implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "field_id", nullable = false)
    private Long id;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "filed_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

}
