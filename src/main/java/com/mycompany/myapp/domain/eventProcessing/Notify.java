package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.OCSMoveableEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "notify")
@Data
public class Notify implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "notify_id", nullable = false)
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "language_id", nullable = false)
    private Long languageId;

    @Column(name = "builder_id", nullable = false)
    private Long builderId;

    //ignore this property
    @Column(name = "content", insertable = false, updatable = false)
    private String name;

    @Column(name = "builder_id", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "domain_id", insertable = false, updatable = false)
    private Integer posIndex;

    @Override
    public void setCategoryId(Long categoryId) {

    }

    public void setParentId(Long parentId) {
        this.builderId = parentId;
    }

    public Long getParentId() {
        return this.builderId;
    }
}
