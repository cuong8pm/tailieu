package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "function_param")
public class FunctionParam implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "function_param_id", nullable = false)
    private Long id;

    @Column(name = "function_param_name", nullable = false)
    private String name;

    @Column(name = "function_id", nullable = false)
    private Long functionId;

    @Column(name = "index_order")
    private Integer indexOrder;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "description")
    private String description;

    @Column(name = "enable")
    private boolean enable;

    @Column(name = "function_id", insertable = false, updatable = false)
    private Long parentId;

    //ignore this property
    @Column(name = "function_id", insertable = false, updatable = false)
    private Integer posIndex;

    public void setParentId(Long parentId){
        this.functionId = parentId;
    }

    public Long getParentId(){
        return this.functionId;
    }

    @Override
    public void setCategoryId(Long categoryId) {
    }

    public static final class FieldNames{
        private FieldNames(){};

        public static final String ORDER_INDEX = "indexOrder";
        public static final String NAME = "name";
    }

}
