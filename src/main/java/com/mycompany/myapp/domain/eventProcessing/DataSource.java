package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "data_source")
@Data
public class DataSource implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "data_source_id", nullable = false)
    private Long id;

    @Column(name = "data_source_name")
    private String name;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

}
