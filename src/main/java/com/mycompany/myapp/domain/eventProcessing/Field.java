package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "field")
@Data
public class Field implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "field_id", nullable = false)
    private Long id;

    @Column(name = "field_name")
    private String name;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "field_name", insertable = false, updatable = false)
    private String posIndex;
}
