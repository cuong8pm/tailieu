package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "router_type")
@Data
public class RouterType implements Serializable, OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "router_type_id", nullable = false)
    private Long id;

    @Column(name = "router_type_name", nullable = false)
    private String name;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "description")
    private String description;

}
