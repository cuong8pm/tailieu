package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "builder_item")
@Data
@NoArgsConstructor
public class BuilderItem implements OCSCloneableEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "builder_item_id", nullable = false)
    private Long id;

    @Column(name = "builder_id", nullable = false)
    private Long builderId;

    @Column(name = "builder_type_item_id", nullable = false)
    private Long builderTypeItemId;

    @Column(name = "value")
    private String value;

    @Column(name = "description")
    private String description;

    @Column(name = "value_type")
    private Integer valueType;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "linked_id")
    private Long linkedId;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    //ignore this property
    @Column(name = "value", insertable = false, updatable = false)
    private String name;

    @Column(name = "builder_id", insertable = false, updatable = false)
    private Long parentId;

    @Column(name = "domain_id", insertable = false, updatable = false)
    private Integer posIndex;

    @Override
    public void setCategoryId(Long categoryId) {}

    public void setParentId(Long parentId) {
        this.builderId = parentId;
    }

    public Long getParentId() {
        return this.builderId;
    }

    public static final class ValueType {

        private ValueType() {}

        public static final Integer MANUAL = 0;
        public static final Integer OBJECT = 1;
        public static final Integer SOURCE_CODE = 2;
        public static final Integer EXITS_CONFIGURATION = 3;
    }

    public static final class DataType {

        private DataType() {}

        public static final Integer INT = 0;
        public static final Integer STRING = 1;
    }
}
