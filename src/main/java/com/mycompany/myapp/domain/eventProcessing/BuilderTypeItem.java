package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "builder_type_item")
@Data
public class BuilderTypeItem implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "builder_type_item_id", nullable = false)
    private Long id;

    @Column(name = "builder_type_id")
    private Long builderTypeId;

    @Column(name = "item_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    //ignore this property
    @Column(name = "domain_id", insertable = false,updatable = false)
    private Integer posIndex;
    //ignore this property
    @Column(name = "item_name", insertable = false, updatable = false)
    private String builderTypeItemName;

    @Column(name = "builder_type_id", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public Long getParentId() {
        return this.builderTypeId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.builderTypeId = parentId;
    }

    @Override
    public void setCategoryId(Long categoryId) {

    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }
}
