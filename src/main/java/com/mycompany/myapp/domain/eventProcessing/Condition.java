package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "`condition`")
@Data
@NoArgsConstructor
@TrimString(fieldNames = { "name", "description" })
public class Condition implements EventPoliciesEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "condition_id", nullable = false)
    private Long id;

    @Column(name = "condition_name")
    private String name;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "pre_process")
    private String preProcess;

    @Column(name = "modify_date")
    private Timestamp modifyDate;

    /**
     * 1: là builder
     * 0: là filter
     */
    @Column(name = "filter_id", nullable = false)
    private Long filterId;

    @Column(name = "description")
    private String description;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public Long getParentObjectId() {
        return this.filterId;
    }

    @Override
    public void setParentObjectId(Long parentObjectId) {
        this.filterId = parentObjectId;
    }

    public static final class FieldNames {

        private FieldNames() {}

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
    }

    public static class Length {

        private Length() {}

        public static final Integer NAME = 255;
    }
}
