package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "`function`")
@Data
public class Function implements EventPoliciesEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "function_id", nullable = false)
    private Long id;

    @Column(name = "function_name", nullable = false)
    private String functionName;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "pos_index", nullable = false)
    private Integer posIndex;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "description")
    private String description;

    @Column(name = "data_type")
    private Integer dataType;

    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentId;

    // alias ở function có chức năng như name ở các đối tượng khác
    @Column(name = "alias", nullable = false)
    private String name;

    @Column(name = "modify_date")
    private Timestamp modifyDate;

    @Column(name = "function_type")
    private Integer functionType;

    //ignore this property
    @Column(name = "category_id", insertable = false, updatable = false)
    private Long parentObjectId;

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
        public static final String ALIAS = "alias";
    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }

    public static final class FunctionType{
        private FunctionType(){}

        public static final Integer FILTER_TYPE = 0;
        public static final Integer CONDITION_TYPE = 1;
        public static final Integer FIELD_TYPE = 2;
    }

}
