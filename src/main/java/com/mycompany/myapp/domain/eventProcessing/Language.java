package com.mycompany.myapp.domain.eventProcessing;

import com.mycompany.myapp.domain.OCSBaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "language")
public class Language implements Serializable , OCSBaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "language_id", nullable = false)
    private Long id;

    @Column(name = "language_name", nullable = false)
    private String name;

    @Column(name = "domain_id", nullable = false)
    private Integer domainId;

    @Column(name = "description")
    private String description;

}
