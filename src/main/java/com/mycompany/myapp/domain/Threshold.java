package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "threshold")
public class Threshold implements Serializable, OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "THRESHOLD_ID", nullable = false)
    private Long id;

    @Column(name = "IS_PERCENTAGE")
    private Boolean percentage;

    @Column(name = "THRESHOLD_TYPE")
    private Integer thresholdType;

    @Column(name = "VALUE")
    private Long value;

    @Column(name = "THRESHOLD_NAME", nullable = false)
    private String name;

    @Column(name = "EXTERNAL_ID")
    private String externalId;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "FOR_BREAK_RATING")
    private Boolean forBreakRating;

    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;

    @Column(name = "THRESHOLD_ID", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public void setCategoryId(Long categoryId) {

    }

    public static final class FieldNames {

        private FieldNames () {}
        public static final String NAME = "name";
        public static final String ID = "id";
    }

}
