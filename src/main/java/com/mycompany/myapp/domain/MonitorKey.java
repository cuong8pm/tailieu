package com.mycompany.myapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "monitor_key")
public class MonitorKey implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MONITOR_KEY_ID", nullable = false)
    private Long id;
    
    @Column(name = "REMARK", nullable = true, length = 100)
    private String remark;
    
    @Column(name = "CATEGORY_ID", nullable = true)
    private Long categoryId;
    
    @Column(name = "MONITOR_KEY", nullable = false, unique = true)
    private Long monitorKey;
    
    @Column(name = "MONITOR_KEY", insertable = false, updatable = false)
    private String tempMonitorKey;
    
    @Column(name = "DOMAIN_ID", nullable = true)
    private Integer domainId;
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    
    public static final class FieldNames {
        private FieldNames () {}
        public static final String MONITOR_KEY_ID = "id";
        public static final String MONITOR_KEY = "monitorKey";
        public static final String MONITOR_DESCRIPTION = "remark";
    }
}
