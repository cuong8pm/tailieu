package com.mycompany.myapp.domain.formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "formula_unit_converter")
public class FormulaUnitConvert {

    @Id
    @Column(name = "FORMULA_UNIT_CONVERTER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FORMULA_UNIT_CONVERTER_NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;
}
