package com.mycompany.myapp.domain.formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.FormulaType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "formula")
public class Formula implements OCSCloneableEntity {

    @Id
    @Column(name = "FORMULA_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FORMULA_TYPE", nullable = false)
    private Long formulaType;

    @Column(name = "A", nullable = false)
    private Long a;

    @Column(name = "B", nullable = false)
    private Long b;

    @Column(name = "PER", nullable = false)
    private Long per;

    @Column(name = "IS_PERCENTAGE")
    private boolean isPercentage;

    @Column(name = "TEMPLATE_BITS")
    private Long templateBits;

    @Column(name = "NORMALIZING_VALUE_TYPE")
    private Long converterType;

    @Column(name = "FORMULA_ERROR_CODE")
    private Long formulaErrorCode;

    @Column(name = "IS_SEND_RAR")
    private boolean isSendRar;

    @Column(name = "IS_MONITOR")
    private boolean isMonitor;

    @Column(name = "TRIGGER_IDS")
    private String triggerIds;

    @Column(name = "STATISTIC_ITEMS")
    private String statisticItems;

    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;

    @Column(name = "RESERVATION_TIME")
    private Long reservationTime;

    @Column(name = "INPUT_A")
    private String inputA;

    @Column(name = "INPUT_B")
    private String inputB;

    // ignore these properties
    @Column(name = "DOMAIN_ID", insertable = false, updatable = false)
    private Integer posIndex;
    @Column(name = "FORMULA_ID", insertable = false, updatable = false)
    private String name;
    @Column(name = "FORMULA_ID", insertable = false, updatable = false)
    private Long parentId;

    @Override
    public void setCategoryId(Long categoryId) {
        // TODO Auto-generated method stub

    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = null;
    }
}
