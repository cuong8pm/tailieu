package com.mycompany.myapp.domain.formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "formula_error_code")
public class FormulaErrorCode {

    @Id
    @Column(name = "FORMULA_ERROR_CODE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "ERROR_CODE", nullable = false)
    private Long errorCode;
    
    @Column(name = "TYPE", nullable = false)
    private Integer type;
    
    @Column(name = "ERROR_NAME", nullable = false)
    private String name;
    
    @Column(name = "DOMAIN_ID", nullable = false)
    private Integer domainId;
}
