package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class TriggerOcs implements Serializable, OCSMoveableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRIGGER_OCS_ID", nullable = false)
    private Long id;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "TRIGGER_NAME", nullable = false)
    private String name;

    @Column(name = "TRIGGER_TYPE", nullable = false)
    private Long triggerType;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "MODULE")
    private Integer module;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "EXT_PROPERTY")
    private String extProperty;

    @Column(name = "SMS_TEMPLATE_CODE")
    private Integer smsTemplateCode;

    @Column(name = "POS_INDEX", nullable = false)
    private Integer posIndex;

    @Override
    public Integer getPosIndex() {
        return this.posIndex;
    }

    @Override
    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }

    @Override
    public Long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long parentId) {
        this.categoryId = parentId;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getDomainId() {
        return this.domainId;
    }

    @Override
    public void setDomainId(Integer domainId) {
        this.domainId = domainId;
    }
}
