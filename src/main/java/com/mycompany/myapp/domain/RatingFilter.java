package com.mycompany.myapp.domain;

import com.mycompany.myapp.dto.normailizer.validator.TrimString;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "rating_filter")
@Data
@NoArgsConstructor
@TrimString(fieldNames = {"name", "description"})
public class RatingFilter implements OCSCloneableEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "RATING_FILTER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "RATING_FILTER_NAME", nullable = false)
    private String name;

    @Column(name = "RATING_FILTER_TYPE", nullable = false)
    private Integer ratingFilterType;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "POS_INDEX")
    private Integer posIndex;

    @Column(name = "DOMAIN_ID")
    private Integer domainId;

    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @Column(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Long parentId;


    public RatingFilter(Long id, String name, Integer ratingFilterType, String description, Integer posIndex, Integer domainId, Long categoryId) {
        this.id = id;
        this.name = name;
        this.ratingFilterType = ratingFilterType;
        this.description = description;
        this.posIndex = posIndex;
        this.domainId = domainId;
        this.categoryId = categoryId;
    }

    public static final class FieldNames {

        private FieldNames() {
        }

        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String POS_INDEX = "posIndex";
        public static final String INPUT = "input";

    }

    public static class Length {
        private Length() {}
        public static final Integer NAME = 255;
    }

}
