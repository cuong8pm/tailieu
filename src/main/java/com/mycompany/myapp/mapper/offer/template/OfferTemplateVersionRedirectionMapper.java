package com.mycompany.myapp.mapper.offer.template;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRedirection;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionRedirectionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionRedirectionCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferTemplateVersionRedirectionMapper
        extends EntityMapper<OfferTemplateVersionRedirectionDTO, OfferTemplateVersionRedirection>, 
        VersionRedirectionCommonMapper<OfferTemplateVersionRedirectionDTO, OfferTemplateVersionRedirection> {

    @Mapping(source = "versionId", target = "offerTemplateVersionId")
    OfferTemplateVersionRedirection toEntity(OfferTemplateVersionRedirectionDTO offerTemplateVersionRedirectionDTO);

    @Mapping(source = "offerTemplateVersionId", target = "versionId")
    OfferTemplateVersionRedirectionDTO toDto(OfferTemplateVersionRedirection offerTemplateVersionRedirection);
}
