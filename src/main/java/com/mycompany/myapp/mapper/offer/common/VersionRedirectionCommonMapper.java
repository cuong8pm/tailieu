package com.mycompany.myapp.mapper.offer.common;

import com.mycompany.myapp.domain.offer.common.VersionRedirectionCommon;
import com.mycompany.myapp.dto.offer.common.VersionRedirectionCommonDTO;

public interface VersionRedirectionCommonMapper<D extends VersionRedirectionCommonDTO, E extends VersionRedirectionCommon> {

    E toEntity(D dto);

    D toDto(E entity);
}
