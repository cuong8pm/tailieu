package com.mycompany.myapp.mapper.offer.common;

import com.mycompany.myapp.domain.offer.common.VersionCharspecUseCommon;
import com.mycompany.myapp.dto.offer.common.VersionCharspecUseCommonDTO;

public interface VersionCharspecUseCommonMapper<D extends VersionCharspecUseCommonDTO, E extends VersionCharspecUseCommon> {

    E toEntity(D dto);

    D toDto(E entity);
}
