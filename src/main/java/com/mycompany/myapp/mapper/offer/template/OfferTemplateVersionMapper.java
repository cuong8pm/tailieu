package com.mycompany.myapp.mapper.offer.template;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.OfferVersionCommonMapper;

@Mapper(componentModel = "spring")
public interface OfferTemplateVersionMapper extends EntityMapper<OfferTemplateVersionDTO, OfferTemplateVersion>, OfferVersionCommonMapper<OfferTemplateVersionDTO, OfferTemplateVersion> {

    OfferTemplateVersion toEntity(OfferTemplateVersionDTO offerTemplateVersionDTO);
    
    OfferTemplateVersionDTO toDto(OfferTemplateVersion offerTemplateVersion);
}
