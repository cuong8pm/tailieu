package com.mycompany.myapp.mapper.offer.common;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.dto.offer.common.OfferCommonDTO;

public interface OfferCommonMapper<D extends OfferCommonDTO, E extends OfferCommon> {

    E toEntity(D offerCommonDTO);

    D toDto(E offerCommon);
}
