package com.mycompany.myapp.mapper.offer.template;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionRelationshipDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.OfferRelationshipCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferTemplateVersionRelationshipMapper
        extends OfferRelationshipCommonMapper<OfferTemplateVersionRelationshipDTO, OfferTemplateVersionRelationship>,
        EntityMapper<OfferTemplateVersionRelationshipDTO, OfferTemplateVersionRelationship> {

    OfferTemplateVersionRelationship toEntity(OfferTemplateVersionRelationshipDTO dto);

    OfferTemplateVersionRelationshipDTO toDto(OfferTemplateVersionRelationship entity);
}
