package com.mycompany.myapp.mapper.offer.offer;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.dto.offer.offer.OfferVersionCharspecValueUseDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {})
public interface OfferVersionCharspecValueUseMapper
    extends EntityMapper<OfferVersionCharspecValueUseDTO, OfferVersionCharspecValueUse> {

    @Mapping(target = "charspecUseId", source = "charspecId")
    @Mapping(target = "id", source = "charspecValueUseId")
    OfferVersionCharspecValueUse toEntity(OfferVersionCharspecValueUseDTO dto);

    @Mapping(target = "charspecId", source = "charspecUseId")
    @Mapping(target = "charspecValueUseId", source = "id")
    OfferVersionCharspecValueUseDTO toDto(OfferVersionCharspecValueUse entity);
}
