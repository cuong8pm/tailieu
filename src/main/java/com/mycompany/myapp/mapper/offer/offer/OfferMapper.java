package com.mycompany.myapp.mapper.offer.offer;

import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.OfferCommonMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OfferMapper extends EntityMapper<OfferDTO, Offer>, OfferCommonMapper<OfferDTO, Offer> {

    @Mapping(target = "posIndex", ignore = true)
    Offer toEntity(OfferDTO offerDTO);

    OfferDTO toDto(Offer offer);
}
