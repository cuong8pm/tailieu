package com.mycompany.myapp.mapper.offer.template;

import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.dto.offer.template.OfferTemplateDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.OfferCommonMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OfferTemplateMapper extends EntityMapper<OfferTemplateDTO, OfferTemplate>, OfferCommonMapper<OfferTemplateDTO, OfferTemplate> {
    OfferTemplate toEntity(OfferTemplateDTO offerTemplateDTO);

    OfferTemplateDTO toDto(OfferTemplate offerTemplate);
}
