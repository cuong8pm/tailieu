package com.mycompany.myapp.mapper.offer.template;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionCharspecUse;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionCharspecUseDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionCharspecUseCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferTemplateVersionCharspecUseMapper
        extends EntityMapper<OfferTemplateVersionCharspecUseDTO, OfferTemplateVersionCharspecUse>,
        VersionCharspecUseCommonMapper<OfferTemplateVersionCharspecUseDTO, OfferTemplateVersionCharspecUse> {

    OfferTemplateVersionCharspecUse toEntity(OfferTemplateVersionCharspecUseDTO dto);

    OfferTemplateVersionCharspecUseDTO toDto(OfferTemplateVersionCharspecUse entity);
}
