package com.mycompany.myapp.mapper.offer.common;

import com.mycompany.myapp.domain.offer.common.OfferRelationshipCommon;
import com.mycompany.myapp.dto.offer.common.OfferRelationshipCommonDTO;

public interface OfferRelationshipCommonMapper<D extends OfferRelationshipCommonDTO, E extends OfferRelationshipCommon> {

    E toEntity(D dto);

    D toDto(E entity);
}
