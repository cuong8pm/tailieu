package com.mycompany.myapp.mapper.offer.template;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionBundle;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionBundleDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionBundleCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferTemplateVersionBundleMapper
        extends EntityMapper<OfferTemplateVersionBundleDTO, OfferTemplateVersionBundle>,
        VersionBundleCommonMapper<OfferTemplateVersionBundleDTO, OfferTemplateVersionBundle> {

    @Mapping(source = "versionId", target = "offerTemplateVersionId")
    OfferTemplateVersionBundle toEntity(OfferTemplateVersionBundleDTO dto);

    @Mapping(source = "offerTemplateVersionId", target = "versionId")
    OfferTemplateVersionBundleDTO toDto(OfferTemplateVersionBundle entity);
}
