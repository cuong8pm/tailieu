package com.mycompany.myapp.mapper.offer.common;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;

public interface OfferVersionCommonMapper<D extends OfferVersionCommonDTO, E extends OfferVersionCommon> {
    
    E toEntity(D offerVersionCommonDTO);
    
    D toDto(E offerVersionCommon);

}
