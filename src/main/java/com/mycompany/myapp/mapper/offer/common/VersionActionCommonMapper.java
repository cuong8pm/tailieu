package com.mycompany.myapp.mapper.offer.common;


import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import com.mycompany.myapp.dto.offer.common.VersionActionCommonDTO;

import java.util.List;

public interface VersionActionCommonMapper<D extends VersionActionCommonDTO, E extends VersionActionCommon> {
    E toEntity(D dto);

    D toDto(E entity);
}
