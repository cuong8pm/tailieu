package com.mycompany.myapp.mapper.offer.common;

import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;
import com.mycompany.myapp.dto.offer.common.VersionBundleCommonDTO;

public interface VersionBundleCommonMapper<D extends VersionBundleCommonDTO, E extends VersionBundleCommon> {

    E toEntity(D dto);

    D toDto(E entity);
}
