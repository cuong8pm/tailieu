package com.mycompany.myapp.mapper.offer.offer;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.offer.offer.OfferVersionBundle;
import com.mycompany.myapp.dto.offer.offer.OfferVersionBundleDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionBundleCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferVersionBundleMapper extends EntityMapper<OfferVersionBundleDTO, OfferVersionBundle>,
        VersionBundleCommonMapper<OfferVersionBundleDTO, OfferVersionBundle> {

    @Mapping(source = "versionId", target = "rootOfferVersionId")
    OfferVersionBundle toEntity(OfferVersionBundleDTO dto);

    @Mapping(source = "rootOfferVersionId", target = "versionId")
    OfferVersionBundleDTO toDto(OfferVersionBundle entity);
}
