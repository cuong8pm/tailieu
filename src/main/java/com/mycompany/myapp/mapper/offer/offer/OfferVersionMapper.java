package com.mycompany.myapp.mapper.offer.offer;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.OfferVersionCommonMapper;

@Mapper(componentModel = "spring")
public interface OfferVersionMapper extends EntityMapper<OfferVersionDTO, OfferVersion>, OfferVersionCommonMapper<OfferVersionDTO, OfferVersion> {
    
    OfferVersion toEntity(OfferVersionDTO offerVersionDTO);
    
    OfferVersionDTO toDto(OfferVersion offerVersion);
}
