package com.mycompany.myapp.mapper.offer.template;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionAction;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionActionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionActionCommonMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {})
public interface OfferTemplateVersionActionMapper extends EntityMapper<OfferTemplateVersionActionDTO, OfferTemplateVersionAction>,
    VersionActionCommonMapper<OfferTemplateVersionActionDTO, OfferTemplateVersionAction> {

    @Mappings({
        @Mapping(target = "offerTemplateVersionId", source = "parentId")
    })
    OfferTemplateVersionAction toEntity(OfferTemplateVersionActionDTO offerTemplateVersionActionDTO);


    @Mappings({
        @Mapping(target = "parentId", source = "offerTemplateVersionId")
    })
    OfferTemplateVersionActionDTO toDto(OfferTemplateVersionAction offerTemplateVersionAction);
}
