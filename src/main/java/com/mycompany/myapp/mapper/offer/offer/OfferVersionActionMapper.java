package com.mycompany.myapp.mapper.offer.offer;

import com.mycompany.myapp.domain.offer.offer.OfferVersionAction;
import com.mycompany.myapp.dto.offer.offer.OfferVersionActionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionActionCommonMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {})
public interface OfferVersionActionMapper extends EntityMapper<OfferVersionActionDTO, OfferVersionAction>,
    VersionActionCommonMapper<OfferVersionActionDTO, OfferVersionAction> {

    @Mappings({
        @Mapping(target = "offerVersionId", source = "parentId")
    })
    OfferVersionAction toEntity(OfferVersionActionDTO offerVersionActionDTO);


    @Mappings({
        @Mapping(target = "parentId", source = "offerVersionId")
    })
    OfferVersionActionDTO toDto(OfferVersionAction offerVersionAction);

}
