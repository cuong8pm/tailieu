package com.mycompany.myapp.mapper.offer.template;

public interface DropDownOfferTemplateVersionDTO {
    Long getId();
    Long getNumber(); 
}
