package com.mycompany.myapp.mapper.offer.offer;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.offer.OfferRelationship;
import com.mycompany.myapp.dto.offer.offer.OfferRelationshipDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.OfferRelationshipCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferRelationshipMapper
        extends OfferRelationshipCommonMapper<OfferRelationshipDTO, OfferRelationship>,
        EntityMapper<OfferRelationshipDTO, OfferRelationship> {

    OfferRelationship toEntity(OfferRelationshipDTO dto);

    OfferRelationshipDTO toDto(OfferRelationship entity);

}
