package com.mycompany.myapp.mapper.offer.template;

public interface TreeDependenciesOfferVersionDTO {
    Long getId();
    Long getNumber(); 
    Long getParentId();
}
