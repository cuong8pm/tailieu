package com.mycompany.myapp.mapper.offer.offer;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecUse;
import com.mycompany.myapp.dto.offer.offer.OfferVersionCharspecUseDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionCharspecUseCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferVersionCharspecUseMapper
        extends EntityMapper<OfferVersionCharspecUseDTO, OfferVersionCharspecUse>,
        VersionCharspecUseCommonMapper<OfferVersionCharspecUseDTO, OfferVersionCharspecUse> {

    OfferVersionCharspecUse toEntity(OfferVersionCharspecUseDTO dto);

    OfferVersionCharspecUseDTO toDto(OfferVersionCharspecUse entity);
}
