package com.mycompany.myapp.mapper.offer.offer;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.offer.offer.OfferVersionRedirection;
import com.mycompany.myapp.dto.offer.offer.OfferVersionRedirectionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import com.mycompany.myapp.mapper.offer.common.VersionRedirectionCommonMapper;

@Mapper(componentModel = "spring", uses = {})
public interface OfferVersionRedirectionMapper
        extends EntityMapper<OfferVersionRedirectionDTO, OfferVersionRedirection>,
        VersionRedirectionCommonMapper<OfferVersionRedirectionDTO, OfferVersionRedirection> {

    @Mapping(source = "versionId", target = "offerVersionId")
    OfferVersionRedirection toEntity(OfferVersionRedirectionDTO dto);

    @Mapping(source = "offerVersionId", target = "versionId")
    OfferVersionRedirectionDTO toDto(OfferVersionRedirection entity);

}
