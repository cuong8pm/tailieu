package com.mycompany.myapp.mapper.ratingfilter;

import com.mycompany.myapp.domain.RatingFilter;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {})
public interface RatingFilterMapper extends EntityMapper<RatingFilterDTO, RatingFilter> {
}
