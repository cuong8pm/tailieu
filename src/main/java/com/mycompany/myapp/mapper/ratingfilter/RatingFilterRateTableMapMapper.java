package com.mycompany.myapp.mapper.ratingfilter;

import com.mycompany.myapp.domain.RatingFilterRateTableMap;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterRateTableMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {})
public interface RatingFilterRateTableMapMapper extends EntityMapper<RatingFilterRateTableMapDTO, RatingFilterRateTableMap> {
    @Mappings({
        @Mapping(target = "index", source = "posIndex")
    })
    RatingFilterRateTableMapDTO toDto(RatingFilterRateTableMap ratingFilterRateTableMap);

    @Mappings({
        @Mapping(target = "posIndex", source = "index")
    })
    RatingFilterRateTableMap toEntity(RatingFilterRateTableMapDTO ratingFilterRateTableMapDTO);
}
