package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import com.mycompany.myapp.domain.PccRule;
import com.mycompany.myapp.dto.PccRuleDTO;

@Mapper(componentModel = "spring", uses = {})
public interface PccRuleMapper extends EntityMapper<PccRuleDTO, PccRule> {
    @Mapping(source = "description", target = "remark")
    @Mapping(source = "effectDate", target = "activeTime")
    @Mapping(source = "expireDate", target = "deactiveTime")
    @Mapping(source = "sendType", target = "sendType")
    PccRule toEntity(PccRuleDTO dto);

    @Mapping(source = "remark", target = "description")
    @Mapping(source = "activeTime", target = "effectDate")
    @Mapping(source = "deactiveTime", target = "expireDate")
    @Mapping(source = "sendType", target = "sendType")
    PccRuleDTO toDto(PccRule entity);
}
