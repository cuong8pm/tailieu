package com.mycompany.myapp.mapper.actiontype;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface ActiontypeMapper extends EntityMapper<ActionTypeDTO, ActionType> {

    ActionType toEntity(ActionTypeDTO actionTypeDTO);

    ActionTypeDTO toDto(ActionType actionType);
}
