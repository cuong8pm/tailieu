package com.mycompany.myapp.mapper.actiontype;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.dto.actiontype.ActionTypeCustomDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface ActionTypeCustomMapper extends EntityMapper<ActionTypeCustomDTO, ActionType> {

    ActionType toEntity(ActionTypeCustomDTO dto);

    ActionTypeCustomDTO toDto(ActionType actionType);
}
