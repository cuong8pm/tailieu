package com.mycompany.myapp.mapper.actiontype;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.actiontype.EventActionTypeMap;
import com.mycompany.myapp.dto.actiontype.EventActionTypeMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface EventActionTypeMapMapper extends EntityMapper<EventActionTypeMapDTO, EventActionTypeMap> {

    EventActionTypeMap toEntity(EventActionTypeMapDTO eventActionTypeMapDTO);

    EventActionTypeMapDTO toDto(EventActionTypeMap eventActionTypeMap);
}
