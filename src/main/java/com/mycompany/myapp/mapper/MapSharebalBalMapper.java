package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.MapSharebalBal;
import com.mycompany.myapp.dto.AccountBalanceMappingDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface MapSharebalBalMapper extends EntityMapper<AccountBalanceMappingDTO, MapSharebalBal> {
    @Mapping(source = "description", target = "remark")
    @Mapping(source = "toBalanceId", target = "shareBalTypeId")
    @Mapping(source = "fromBalanceId", target = "balTypeId")
    @Mapping(source = "mappingTypeId", target = "mappingTypeId")
    MapSharebalBal toEntity(AccountBalanceMappingDTO dto);

    @Mapping(source = "remark", target = "description")
    @Mapping(source = "shareBalTypeId", target = "toBalanceId")
    @Mapping(source = "balTypeId", target = "fromBalanceId")
    @Mapping(source = "mappingTypeId", target = "mappingTypeId")
    AccountBalanceMappingDTO toDto(MapSharebalBal entity);
}
