package com.mycompany.myapp.mapper.action;

import com.mycompany.myapp.domain.action.ActionPriceComponentMap;
import com.mycompany.myapp.dto.action.ActionPriceComponentMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ActionPriceComponentMapMapper extends EntityMapper<ActionPriceComponentMapDTO, ActionPriceComponentMap>{
}
