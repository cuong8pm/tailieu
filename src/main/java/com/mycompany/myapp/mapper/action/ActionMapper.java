package com.mycompany.myapp.mapper.action;

import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.mapper.EntityMapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ActionMapper extends EntityMapper<ActionDTO, Action> {
    Action toEntity(ActionDTO blockDTO);

    @Mappings({
        @Mapping(target = "actionPriceComponentMaps", ignore = true)
    })
    ActionDTO toDto(Action action);
    
    @IterableMapping
    default Timestamp toTimestamp(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Timestamp.valueOf(localDateTime);
    }
    
    @IterableMapping
    default LocalDateTime toLocalDateTime(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.toLocalDateTime();
    }
}
