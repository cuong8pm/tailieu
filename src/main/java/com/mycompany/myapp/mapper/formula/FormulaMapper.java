package com.mycompany.myapp.mapper.formula;

import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.formula.Formula;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface FormulaMapper extends EntityMapper<FormulaDTO, Formula> {

    @Mapping(source = "statisticItemString", target = "statisticItems")
    Formula toEntity(FormulaDTO formulaDTO);

    @Mapping(source = "statisticItems", target = "statisticItemString")
    @Mapping(target = "statisticItems", ignore = true)
    FormulaDTO toDto(Formula formula);
}
