package com.mycompany.myapp.mapper.formula;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.formula.FormulaErrorCode;
import com.mycompany.myapp.dto.formula.FormulaErrorCodeDTO;

@Mapper(componentModel = "spring", uses = {})
public interface FormulaErrorCodeMapper {

    FormulaErrorCode toEntity(FormulaErrorCodeDTO formulaErrorCodeDTO);

    FormulaErrorCodeDTO toDto(FormulaErrorCode formulaErrorCode);
}
