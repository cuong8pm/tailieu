package com.mycompany.myapp.mapper.formula;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.formula.FormulaUnitConvert;
import com.mycompany.myapp.dto.formula.FormulaUnitConvertDTO;

@Mapper(componentModel = "spring", uses = {})
public interface FormulaUnitConvertMapping {

    FormulaUnitConvert toEntity(FormulaUnitConvertDTO formulaUnitConvertDTO);

    FormulaUnitConvertDTO toDto(FormulaUnitConvert formulaUnitConvert);
}
