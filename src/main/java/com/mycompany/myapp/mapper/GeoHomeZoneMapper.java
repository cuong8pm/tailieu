package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.GeoHomeZone;
import com.mycompany.myapp.dto.GeoHomeZoneDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface GeoHomeZoneMapper extends EntityMapper<GeoHomeZoneDTO, GeoHomeZone> {
    @Mapping(source = "description", target = "remark")
    GeoHomeZone toEntity(GeoHomeZoneDTO dto);

    @Mapping(source = "remark", target = "description")
    GeoHomeZoneDTO toDto(GeoHomeZone entity);
}
