package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.StateType;
import com.mycompany.myapp.dto.StateTypeDTO;

@Mapper(componentModel = "spring", uses = {})
public interface StateTypeMapper extends EntityMapper<StateTypeDTO, StateType> {
    @Mapping(source = "description", target = "stateDesc")
    StateType toEntity(StateTypeDTO stateTypeDTO);

    @Mapping(source = "stateDesc", target = "description")
    StateTypeDTO toDto(StateType stateType);

}
