package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.dto.CategoryDTO;

@Mapper(componentModel = "spring", uses = {})
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "remark", target = "description")
    @Mapping(source = "categoryParentId", target = "parentCategoryId")
    @Mapping(source = "treeType", target = "type")
    CategoryDTO toDto(Category category);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "remark")
    @Mapping(source = "parentCategoryId", target = "categoryParentId")
    Category toEntity(CategoryDTO categoryDTO);

    default Category fromId(Long id) {
        if (id == null) {
            return null;
        }
        Category category = new Category();
        category.setId(id);
        return category;
    }
}
