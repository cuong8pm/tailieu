package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.Threshold;
import com.mycompany.myapp.dto.ThresholdsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ThresholdMapper extends EntityMapper<ThresholdsDTO, Threshold> {
    Threshold toEntity(ThresholdsDTO dto);

    ThresholdsDTO toDto(Threshold entity);
}
