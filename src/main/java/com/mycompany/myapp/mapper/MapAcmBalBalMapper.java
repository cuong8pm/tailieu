package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.MapAcmBalBal;
import com.mycompany.myapp.dto.BalanceAcmMappingDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface MapAcmBalBalMapper extends EntityMapper<BalanceAcmMappingDTO, MapAcmBalBal> {
    MapAcmBalBal toEntity(BalanceAcmMappingDTO dto);

    BalanceAcmMappingDTO toDto(MapAcmBalBal entity);
}
