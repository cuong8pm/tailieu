package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.PepProfilePcc;
import com.mycompany.myapp.dto.PepProfilePccDTO;

@Mapper(componentModel = "spring", uses = {})
public interface PepProfilePccMapper extends EntityMapper<PepProfilePccDTO, PepProfilePcc> {

    PepProfilePcc toEntity(PepProfilePccDTO pepProfilePccDTO);
    
    PepProfilePccDTO toDto(PepProfilePcc pepProfilePcc);
}
