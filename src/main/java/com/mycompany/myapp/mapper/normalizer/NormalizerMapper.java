package com.mycompany.myapp.mapper.normalizer;

import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface NormalizerMapper extends EntityMapper<NormalizerDTO, Normalizer> {
    @Mapping(source = "typeId", target = "normalizerType")
    @Mapping(source = "stateId", target = "state")
    @Mapping(source = "defaultValue", target = "defaultValue")
    Normalizer toEntity(NormalizerDTO dto);

    @Mapping(source = "normalizerType", target = "typeId")
    @Mapping(source = "state", target = "stateId")
    @Mapping(source = "defaultValue", target = "defaultValue")
    NormalizerDTO toDto(Normalizer entity);

    default Integer toTypeId(NormalizerType normalizerType) {
        return normalizerType.getValue();
    }

    default NormalizerType toNormalizerType(Integer value) {
        return NormalizerType.of(value);
    }
}
