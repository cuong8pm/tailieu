package com.mycompany.myapp.mapper.normalizer;

import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.dto.normailizer.Color;
import com.mycompany.myapp.dto.normailizer.NormValueDTO;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {Color.class})
public interface NormValueMapper extends EntityMapper<NormValueDTO, NormalizerValue> {
    @Mapping(source = "color.text", target = "colorText")
    @Mapping(source = "color.background", target = "colorBG")
    NormalizerValue toEntity(NormValueDTO dto);

    @Mapping(source = "colorText", target = "color.text")
    @Mapping(source = "colorBG", target = "color.background")
    @Mapping(target = "hasParams", ignore = true)
    NormValueDTO toDto(NormalizerValue entity);
}
