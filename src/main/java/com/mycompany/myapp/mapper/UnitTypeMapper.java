package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.dto.UnitTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface UnitTypeMapper extends EntityMapper<UnitTypeDTO, UnitType> {

    @Mapping(source = "description", target = "remark")
    @Mapping(source = "posIndex", target = "unitOrder")
    UnitType toEntity(UnitTypeDTO dto);

    @Mapping(source = "remark", target = "description")
    @Mapping(source = "unitOrder", target = "posIndex")
    UnitTypeDTO toDto(UnitType entity);
}
