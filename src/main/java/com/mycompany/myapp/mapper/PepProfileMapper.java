package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.PepProfile;
import com.mycompany.myapp.dto.PepProfileDTO;

@Mapper(componentModel = "spring", uses = {})
public interface PepProfileMapper extends EntityMapper<PepProfileDTO, PepProfile> {
    @Mapping(source = "description", target = "remark")
    PepProfile toEntity(PepProfileDTO profilePepDTO);

    @Mapping(source = "remark", target = "description")
    PepProfileDTO toDto(PepProfile profilePep);
}
