package com.mycompany.myapp.mapper.ratetable;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface RateTableMapper extends EntityMapper<RateTableDTO, RateTable> {

    RateTable toEntity(RateTableDTO rateTableDTO);

    RateTableDTO toDto(RateTable rateTable);
}
