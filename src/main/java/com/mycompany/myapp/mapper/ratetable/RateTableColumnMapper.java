package com.mycompany.myapp.mapper.ratetable;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.ratetable.RateTableColumn;
import com.mycompany.myapp.dto.ratetable.RateTableColumnDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface RateTableColumnMapper extends EntityMapper<RateTableColumnDTO, RateTableColumn> {

    RateTableColumn toEntity(RateTableColumnDTO rateTableColumnDTO);

    RateTableColumnDTO toDto(RateTableColumn rateTableColumn);
}
