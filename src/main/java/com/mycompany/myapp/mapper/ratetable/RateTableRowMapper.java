package com.mycompany.myapp.mapper.ratetable;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.ratetable.RateTableRow;
import com.mycompany.myapp.dto.ratetable.RateTableRowDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface RateTableRowMapper extends EntityMapper<RateTableRowDTO, RateTableRow> {

    RateTableRow toEntity(RateTableRowDTO rateTableRowDTO);

    RateTableRowDTO toDto(RateTableRow rateTableRow);
}
