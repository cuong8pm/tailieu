package com.mycompany.myapp.mapper.block;

import com.mycompany.myapp.domain.block.BlockRateTableMap;
import com.mycompany.myapp.dto.block.BlockRateTableMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface BlockRateTableMapMapper extends EntityMapper<BlockRateTableMapDTO, BlockRateTableMap> {
}
