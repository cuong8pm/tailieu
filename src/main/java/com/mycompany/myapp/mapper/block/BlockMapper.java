package com.mycompany.myapp.mapper.block;

import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {})
public interface BlockMapper extends EntityMapper<BlockDTO, Block> {
    @Mappings({
        @Mapping(target = "blockMode", source = "affectedBalMode"),
        @Mapping(target = "affectedValue", source = "affectedObject"),
        @Mapping(target = "affectedObjectType", source = "affectedObjectType")
    })
    Block toEntity(BlockDTO blockDTO);

    @Mappings({
        @Mapping(target = "affectedBalMode", source = "blockMode"),
        @Mapping(target = "affectedObject", source = "affectedValue"),
        @Mapping(target = "affectedObjectType", source = "affectedObjectType"),
        @Mapping(target = "rateTableItems", ignore = true)
    })
    BlockDTO toDto(Block block);
}
