package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.ThresholdBaltypeMap;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.ThresholdBaltypeMapDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface ThresholdBalTypeMapMapper extends EntityMapper<ThresholdBaltypeMapDTO, ThresholdBaltypeMap> {

    ThresholdBaltypeMapDTO toDto(ThresholdBaltypeMap thresholdBaltypeMap);

    ThresholdBaltypeMap toEntity(ThresholdBaltypeMapDTO thresholdBaltypeMapDTO);

    default ThresholdBaltypeMap fromId(Long id) {
        if (id == null) {
            return null;
        }
        ThresholdBaltypeMap thresholdBaltypeMap = new ThresholdBaltypeMap();
        thresholdBaltypeMap.setId(id);
        return thresholdBaltypeMap;
    }
}
