package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.Qos;
import com.mycompany.myapp.dto.QosDTO;

@Mapper(componentModel = "spring", uses = {})
public interface QosMapper extends EntityMapper<QosDTO, Qos>{
    @Mapping(source = "description", target = "qosRemark")
    Qos toEntity(QosDTO dto);
    
    @Mapping(source = "qosRemark", target = "description")
    QosDTO toDto(Qos entity);
}
