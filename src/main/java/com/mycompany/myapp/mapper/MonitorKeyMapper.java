package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.MonitorKey;
import com.mycompany.myapp.dto.MonitorKeyDTO;

@Mapper(componentModel = "spring", uses = {})
public interface MonitorKeyMapper extends EntityMapper<MonitorKeyDTO, MonitorKey>{
    @Mapping(source = "description", target = "remark")
    MonitorKey toEntity(MonitorKeyDTO dto);
    
    @Mapping(source = "remark", target = "description")
    @Mapping(target = "name", expression = "java(entity.getMonitorKey() + \"(\" + entity.getRemark() + \")\")")
    MonitorKeyDTO toDto(MonitorKey entity);
}
