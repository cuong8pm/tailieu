package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.dto.BillingCycleDTO;
@Mapper(componentModel = "spring", uses = {})
public interface BillingCycleMapper extends EntityMapper<BillingCycleDTO, BillingCycle> {

}
