package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.normalizer.PreFunction;
import com.mycompany.myapp.dto.normailizer.validator.PreFunctionDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PreFunctionMapper extends EntityMapper<PreFunctionDTO, PreFunction> {
    PreFunction toEntity(PreFunctionDTO dto);

    PreFunctionDTO toDto(PreFunction entity);
}
