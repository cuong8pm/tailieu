package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.dto.ReferTableDTO;

@Mapper(componentModel = "spring", uses = {})
public interface ReferTableMapper extends EntityMapper<ReferTableDTO, ReferTable> {

    ReferTable toEntity(ReferTableDTO dto);

    ReferTableDTO toDto(ReferTable entity);
}
