package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.ReserveInfo;
import com.mycompany.myapp.dto.ReserveInfoDTO;

@Mapper(componentModel = "spring", uses = {})
public interface ReserveInfoMapper extends EntityMapper<ReserveInfoDTO, ReserveInfo> {
    @Mapping(source = "maxReserveValue", target = "maxReserve")
    @Mapping(source = "minReserveValue", target = "minReserve")
    @Mapping(source = "name", target = "resvName")
    @Mapping(source = "description", target = "resvDesc")
    ReserveInfo toEntity(ReserveInfoDTO dto);

    @Mapping(source = "maxReserve", target = "maxReserveValue")
    @Mapping(source = "minReserve", target = "minReserveValue")
    @Mapping(source = "resvName", target = "name")
    @Mapping(source = "resvDesc", target = "description")
    ReserveInfoDTO toDto(ReserveInfo entity);
}
