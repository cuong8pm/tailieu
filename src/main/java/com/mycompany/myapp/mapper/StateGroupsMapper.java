package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.StateGroups;
import com.mycompany.myapp.dto.StateGroupsDTO;

@Mapper(componentModel = "spring", uses = {})
public interface StateGroupsMapper extends EntityMapper<StateGroupsDTO, StateGroups> {
    @Mapping(source = "description", target = "stateGroupDesc")
    StateGroups toEntity(StateGroupsDTO stateGroupsDTO);

    @Mapping(source = "stateGroupDesc", target = "description")
    StateGroupsDTO toDto(StateGroups stateGroups);
}
