package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.ZoneMap;
import com.mycompany.myapp.dto.ZoneMapDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface ZoneMapMapper extends EntityMapper<ZoneMapDTO, ZoneMap> {
    @Mapping(source = "description", target = "remark")
    ZoneMap toEntity(ZoneMapDTO dto);

    @Mapping(source = "remark", target = "description")
    ZoneMapDTO toDto(ZoneMap entity);
}
