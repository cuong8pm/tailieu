package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.CalcUnit;
import com.mycompany.myapp.dto.CalcUnitDTO;

@Mapper(componentModel = "spring", uses = {})
public interface CalcUnitMapper extends EntityMapper<CalcUnitDTO, CalcUnit>{

}
