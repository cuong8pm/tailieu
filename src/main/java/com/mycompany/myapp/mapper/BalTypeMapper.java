package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.BalType;
import com.mycompany.myapp.dto.BalancesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface BalTypeMapper extends EntityMapper<BalancesDTO, BalType> {
    @Mapping(source = "multiBalTypeId", target = "balTypeType")
    @Mapping(source = "effDateTypeId", target = "effDateType")
    @Mapping(source = "expDateTypeId", target = "expDateType")
    @Mapping(source = "paymentTypeId", target = "paymentType")
    @Mapping(source = "ownerLevelId", target = "balLevel")
    @Mapping(source = "startDateData", target = "effDateData")
    @Mapping(source = "startDate", target = "effDate")
    @Mapping(source = "lowLevel", target = "lowWaterMarkLevel")
    @Mapping(source = "highLevel", target = "highWaterMarkLevel")
    @Mapping(source = "precision", target = "percision")
    @Mapping(source = "periodicTypeId", target = "periodicPeriodType")
    BalType toEntity(BalancesDTO dto);

    @Mapping(source = "balTypeType", target = "multiBalTypeId")
    @Mapping(source = "effDateType", target = "effDateTypeId")
    @Mapping(source = "expDateType", target = "expDateTypeId")
    @Mapping(source = "paymentType", target = "paymentTypeId")
    @Mapping(source = "balLevel", target = "ownerLevelId")
    @Mapping(source = "effDateData", target = "startDateData")
    @Mapping(source = "effDate", target = "startDate")
    @Mapping(source = "lowWaterMarkLevel", target = "lowLevel")
    @Mapping(source = "highWaterMarkLevel", target = "highLevel")
    @Mapping(source = "percision", target = "precision")
    @Mapping(source = "periodicPeriodType", target = "periodicTypeId")
    BalancesDTO toDto(BalType entity);
}
