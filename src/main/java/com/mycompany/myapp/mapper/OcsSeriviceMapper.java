package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.OCSService;
import com.mycompany.myapp.dto.OcsServiceDTO;

@Mapper(componentModel = "spring", uses = {})
public interface OcsSeriviceMapper extends EntityMapper<OcsServiceDTO, OCSService> {
    @Mapping(source = "description", target = "serviceDesc")
    OCSService toEntity(OcsServiceDTO dto);

    @Mapping(source = "serviceDesc", target = "description")
    OcsServiceDTO toDto(OCSService entity);
}
