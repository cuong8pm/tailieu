package com.mycompany.myapp.mapper.event;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.event.Event;
import com.mycompany.myapp.dto.event.EventDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface EventMapper extends EntityMapper<EventDTO, Event> {

    Event toEntity(EventDTO eventDTO);

    EventDTO toDto(Event event);
}
