package com.mycompany.myapp.mapper.pricecomponent;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface PriceComponentMapper extends EntityMapper<PriceComponentDTO, PriceComponent> {

    PriceComponent toEntity(PriceComponentDTO priceComponentDTO);

    PriceComponentDTO toDto(PriceComponent priceComponent);
}
