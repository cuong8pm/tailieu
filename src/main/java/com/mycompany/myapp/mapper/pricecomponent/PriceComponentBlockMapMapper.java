package com.mycompany.myapp.mapper.pricecomponent;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.pricecomponent.PriceComponentBlockMap;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentBlockMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface PriceComponentBlockMapMapper extends EntityMapper<PriceComponentBlockMapDTO, PriceComponentBlockMap> {

    PriceComponentBlockMap toEntity(PriceComponentBlockMapDTO priceComponentBlockMapDTO);

    PriceComponentBlockMapDTO toDto(PriceComponentBlockMap priceComponentBlockMap);
}
