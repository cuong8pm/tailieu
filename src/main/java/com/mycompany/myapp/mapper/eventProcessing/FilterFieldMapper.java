package com.mycompany.myapp.mapper.eventProcessing;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.eventProcessing.FilterField;
import com.mycompany.myapp.dto.eventProcessing.FilterFieldDTO;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = {})
public interface FilterFieldMapper extends EntityMapper<FilterFieldDTO, FilterField> {

    FilterField toEntity(FilterFieldDTO dto);

    FilterFieldDTO toDto(FilterField entity);
}
