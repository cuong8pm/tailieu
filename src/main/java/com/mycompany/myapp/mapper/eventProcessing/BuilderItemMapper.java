package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderItem;
import com.mycompany.myapp.dto.eventProcessing.BuilderItemDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface BuilderItemMapper extends EntityMapper<BuilderItemDTO, BuilderItem> {
}
