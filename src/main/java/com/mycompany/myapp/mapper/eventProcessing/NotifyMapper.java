package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Notify;
import com.mycompany.myapp.dto.eventProcessing.NotifyDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {})
public interface NotifyMapper extends EntityMapper<NotifyDTO, Notify> {

    Notify toEntity(NotifyDTO notifyDTO);

    NotifyDTO toDto(Notify notify);

}
