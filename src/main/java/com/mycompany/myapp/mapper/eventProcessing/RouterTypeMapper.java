package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.RouterType;
import com.mycompany.myapp.dto.eventProcessing.RouterTypeDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {})
public interface RouterTypeMapper extends EntityMapper<RouterTypeDTO, RouterType> {

}
