package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.dto.eventProcessing.FilterDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface FilterMapper extends EntityMapper<FilterDTO, Filter> {

}
