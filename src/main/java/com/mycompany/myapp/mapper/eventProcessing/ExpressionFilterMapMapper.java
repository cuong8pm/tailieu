package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ExpressionFilterMap;
import com.mycompany.myapp.dto.eventProcessing.ExpressionFilterMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ExpressionFilterMapMapper extends EntityMapper<ExpressionFilterMapDTO, ExpressionFilterMap> {
}
