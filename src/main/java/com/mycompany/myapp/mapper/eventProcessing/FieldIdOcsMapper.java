package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FieldIdOcs;
import com.mycompany.myapp.dto.eventProcessing.FieldIdOcsDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface FieldIdOcsMapper extends EntityMapper<FieldIdOcsDTO, FieldIdOcs> {
}
