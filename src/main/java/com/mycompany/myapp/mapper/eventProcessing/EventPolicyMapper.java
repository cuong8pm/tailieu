package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventPolicy;
import com.mycompany.myapp.dto.eventProcessing.EventPolicyDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface EventPolicyMapper extends EntityMapper<EventPolicyDTO, EventPolicy> {
}
