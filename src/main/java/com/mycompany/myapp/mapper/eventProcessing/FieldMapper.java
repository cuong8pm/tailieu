package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Field;
import com.mycompany.myapp.dto.eventProcessing.FieldDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface FieldMapper extends EntityMapper<FieldDTO, Field> {
}
