package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.dto.eventProcessing.BuilderDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface BuilderMapper extends EntityMapper<BuilderDTO, Builder> {

    Builder toEntity(BuilderDTO builderDTO);

    BuilderDTO toDto(Builder builder);

}
