package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FunctionParamInstance;
import com.mycompany.myapp.dto.eventProcessing.FunctionParamInstanceDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {})
public interface FunctionParamInstanceMapper extends EntityMapper<FunctionParamInstanceDTO, FunctionParamInstance> {

}
