package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Expression;
import com.mycompany.myapp.dto.eventProcessing.ExpressionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface ExpressionMapper extends EntityMapper<ExpressionDTO, Expression> {

}
