package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderTypeItem;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeItemDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface BuilderTypeItemMapper extends EntityMapper<BuilderTypeItemDTO, BuilderTypeItem> {
}
