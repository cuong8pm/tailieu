package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ConditionVerificationMap;
import com.mycompany.myapp.dto.eventProcessing.ConditionVerificationMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ConditionVerificationMapMapper extends EntityMapper<ConditionVerificationMapDTO, ConditionVerificationMap> {
}
