package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ExpressionFunctionMap;
import com.mycompany.myapp.dto.eventProcessing.ExpressionFunctionMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ExpressionFunctionMapMapper extends EntityMapper<ExpressionFunctionMapDTO, ExpressionFunctionMap> {
}
