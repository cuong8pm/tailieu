package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderTypeRouterMap;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeRouterMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface BuilderTypeRouterMapMapper extends EntityMapper<BuilderTypeRouterMapDTO, BuilderTypeRouterMap> {
}
