package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Verification;
import com.mycompany.myapp.dto.eventProcessing.VerificationDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface VerificationMapper extends EntityMapper<VerificationDTO, Verification> {

}
