package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventObjectField;
import com.mycompany.myapp.dto.eventProcessing.EventObjectFieldDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface EventObjectFieldMapper extends EntityMapper<EventObjectFieldDTO, EventObjectField> {}
