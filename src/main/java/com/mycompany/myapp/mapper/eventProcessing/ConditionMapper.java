package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Condition;
import com.mycompany.myapp.dto.eventProcessing.ConditionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface ConditionMapper extends EntityMapper<ConditionDTO, Condition> {
    Condition toEntity(ConditionDTO conditionDTO);

    ConditionDTO toDto(Condition condition);
}
