package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface BuilderTypeMapper extends EntityMapper<BuilderTypeDTO, BuilderType> {
}
