package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ConditionBuilderMap;
import com.mycompany.myapp.dto.eventProcessing.ConditionBuilderMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ConditionBuilderMapMapper extends EntityMapper<ConditionBuilderMapDTO, ConditionBuilderMap> {
}
