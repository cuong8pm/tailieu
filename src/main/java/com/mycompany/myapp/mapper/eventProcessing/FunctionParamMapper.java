package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FunctionParam;
import com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;


@Mapper(componentModel = "spring", uses = {})
@Component
public interface FunctionParamMapper extends EntityMapper<FunctionParamDTO, FunctionParam> {

}
