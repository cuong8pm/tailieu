package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.VerificationItem;
import com.mycompany.myapp.dto.eventProcessing.VerificationItemDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface VerificationItemMapper extends EntityMapper<VerificationItemDTO, VerificationItem> {

}
