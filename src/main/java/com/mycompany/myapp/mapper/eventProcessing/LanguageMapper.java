package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Language;
import com.mycompany.myapp.dto.eventProcessing.LanguageDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {})
public interface LanguageMapper extends EntityMapper<LanguageDTO, Language> {

    Language toEntity(LanguageDTO languageDTO);

    LanguageDTO toDto(Language language);

}
