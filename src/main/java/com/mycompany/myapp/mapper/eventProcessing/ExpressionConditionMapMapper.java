package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.ExpressionConditionMap;
import com.mycompany.myapp.dto.eventProcessing.ExpressionConditionMapDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ExpressionConditionMapMapper extends EntityMapper<ExpressionConditionMapDTO, ExpressionConditionMap> {
}
