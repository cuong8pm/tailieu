package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.dto.eventProcessing.FunctionDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {})
public interface FunctionMapper extends EntityMapper<FunctionDTO, Function> {

}
