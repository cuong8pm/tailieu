package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.EventObject;
import com.mycompany.myapp.dto.eventProcessing.EventObjectDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface EventObjectMapper extends EntityMapper<EventObjectDTO, EventObject> {
}
