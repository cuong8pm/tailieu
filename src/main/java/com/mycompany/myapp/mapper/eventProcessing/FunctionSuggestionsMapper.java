package com.mycompany.myapp.mapper.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.FunctionSuggestions;
import com.mycompany.myapp.dto.eventProcessing.FunctionSuggestionsDTO;
import com.mycompany.myapp.mapper.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface FunctionSuggestionsMapper extends EntityMapper<FunctionSuggestionsDTO, FunctionSuggestions> {

}
