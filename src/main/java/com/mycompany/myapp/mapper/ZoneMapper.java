package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.dto.ZoneDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface ZoneMapper extends EntityMapper<ZoneDTO, Zone> {
    @Mapping(source = "description", target = "remark")
    Zone toEntity(ZoneDTO dto);

    @Mapping(source = "remark", target = "description")
    ZoneDTO toDto(Zone entity);
}
