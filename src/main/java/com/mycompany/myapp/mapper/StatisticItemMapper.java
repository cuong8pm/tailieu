package com.mycompany.myapp.mapper;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.StatisticItem;
import com.mycompany.myapp.dto.StatisticItemDTO;

@Mapper(componentModel = "spring", uses = {})
public interface StatisticItemMapper extends EntityMapper<StatisticItemDTO, StatisticItem> {
    StatisticItem toEntity(StatisticItemDTO dto);

    StatisticItemDTO toDto(StatisticItem entity);
}
