package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.ZoneData;
import com.mycompany.myapp.dto.ZoneDataDTO;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ZoneDataMapper extends EntityMapper<ZoneDataDTO, ZoneData> {


    @IterableMapping
    default Timestamp toTimestamp(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Timestamp.valueOf(localDateTime);
    }
    
    @IterableMapping
    default LocalDateTime toLocalDateTime(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.toLocalDateTime();
    }
}
