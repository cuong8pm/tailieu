package com.mycompany.myapp.mapper.characteristic;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.characteristic.RelationshipType;
import com.mycompany.myapp.dto.characteristic.RelationshipTypeDTO;
import com.mycompany.myapp.mapper.EntityMapper;

/**
 * Mapper for the entity {@link RelationshipType} and its DTO
 * {@link RelationshipTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RelationshipTypeMapper extends EntityMapper<RelationshipTypeDTO, RelationshipType> {

    RelationshipType toEntity(RelationshipTypeDTO relationshipTypeDTO);

    default RelationshipType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RelationshipType relationshipType = new RelationshipType();
        relationshipType.setId(id);
        return relationshipType;
    }
}