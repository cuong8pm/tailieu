package com.mycompany.myapp.mapper.characteristic;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.characteristic.Relationship;
import com.mycompany.myapp.dto.characteristic.RelationshipDTO;
import com.mycompany.myapp.mapper.EntityMapper;

/**
 * Mapper for the entity {@link CharSpecRelationship} and its DTO
 * {@link CharSpecRelationshipDTO}.
 */
@Mapper(componentModel = "spring", uses = { CharacteristicMapper.class, RelationshipTypeMapper.class })
public interface RelationshipMapper extends EntityMapper<RelationshipDTO, Relationship> {

    @Mapping(source = "parentCharSpec.id", target = "parentCharacteristicId")
    @Mapping(source = "childCharSpec.id", target = "childCharacteristicId")
    @Mapping(source = "relationshipType.id", target = "relationshipTypeId")
    @Mapping(source = "relationshipType.name", target = "relationshipTypeName")
    @Mapping(source = "childCharSpec.name", target = "childCharacteristicName")
    @Mapping(source = "childCharSpec.description", target = "description")
    RelationshipDTO toDto(Relationship charSpecRelationship);

    @Mapping(source = "parentCharacteristicId", target = "parentCharSpec")
    @Mapping(source = "childCharacteristicId", target = "childCharSpec")
    @Mapping(source = "relationshipTypeId", target = "relationshipType")
    Relationship toEntity(RelationshipDTO relationshipDTO);

    default Relationship fromId(Long id) {
        if (id == null) {
            return null;
        }
        Relationship charSpecRelationship = new Relationship();
        charSpecRelationship.setId(id);
        return charSpecRelationship;
    }
}
