package com.mycompany.myapp.mapper.characteristic;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.dto.characteristic.CharacteristicDTO;
import com.mycompany.myapp.mapper.CategoryMapper;
import com.mycompany.myapp.mapper.EntityMapper;

@Mapper(componentModel = "spring", uses = { CategoryMapper.class, TypeMapper.class, ValueTypeMapper.class })
public interface CharacteristicMapper extends EntityMapper<CharacteristicDTO, Characteristic> {

    @Mapping(source = "valueRefeTable", target = "characteristicTypeId")
    @Mapping(source = "valueType", target = "valueTypeId")
    @Mapping(source = "id", target = "characteristicId")
    @Mapping(source = "name", target = "characteristicName")
    @Mapping(source = "valueUnique", target = "unique")
    CharacteristicDTO toDto(Characteristic characteristic);

    @Mapping(source = "characteristicTypeId", target = "valueRefeTable")
    @Mapping(source = "valueTypeId", target = "valueType")
    @Mapping(source = "characteristicName", target = "name")
    @Mapping(source = "characteristicId", target = "id")
    @Mapping(source = "unique", target = "valueUnique")
    Characteristic toEntity(CharacteristicDTO characteristicDTO);

    default Characteristic fromId(Long id) {
        if (id == null) {
            return null;
        }
        Characteristic charSpec = new Characteristic();
        charSpec.setId(id);
        return charSpec;
    }
}
