package com.mycompany.myapp.mapper.characteristic;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.characteristic.Type;
import com.mycompany.myapp.dto.characteristic.TypeDTO;
import com.mycompany.myapp.mapper.EntityMapper;


/**
 * Mapper for the entity {@link Type} and its DTO {@link TypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TypeMapper extends EntityMapper<TypeDTO, Type> {

    Type toEntity(TypeDTO charSpecTypeDTO);

    default Type fromId(Long id) {
        if (id == null) {
            return null;
        }
        Type charSpecType = new Type();
        charSpecType.setId(id);
        return charSpecType;
    }
}
