package com.mycompany.myapp.mapper.characteristic;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.characteristic.Value;
import com.mycompany.myapp.dto.characteristic.ValuesDTO;
import com.mycompany.myapp.mapper.EntityMapper;

/**
 * Mapper for the entity {@link CharSpecValue} and its DTO
 * {@link CharSpecValueDTO}.
 */
@Mapper(componentModel = "spring", uses = { CharacteristicMapper.class, ValueTypeMapper.class })
public interface ValueMapper extends EntityMapper<ValuesDTO, Value> {

    @Mapping(source = "characteristic.id", target = "characteristicId")
    @Mapping(source = "valueType", target = "valueTypeId")
    @Mapping(source = "name", target = "valueName")
    @Mapping(source = "id", target = "valueId")
    @Mapping(source = "unitOfMeasure", target = "unitType")
    ValuesDTO toDto(Value charSpecValue);

    @Mapping(source = "characteristicId", target = "characteristic")
    @Mapping(source = "valueTypeId", target = "valueType")
    @Mapping(source = "valueName", target = "name")
    @Mapping(source = "valueId", target = "id")
    @Mapping(source = "unitType", target = "unitOfMeasure")
    Value toEntity(ValuesDTO valuesDTO);

    default Value fromId(Long id) {
        if (id == null) {
            return null;
        }
        Value charSpecValue = new Value();
        charSpecValue.setId(id);
        return charSpecValue;
    }
}
