package com.mycompany.myapp.mapper.characteristic;


import org.mapstruct.*;

import com.mycompany.myapp.domain.characteristic.ValueGroup;
import com.mycompany.myapp.dto.characteristic.MappingValueDTO;
import com.mycompany.myapp.mapper.EntityMapper;

/**
 * Mapper for the entity {@link CharSpecValueGroup} and its DTO
 * {@link CharSpecValueGroupDTO}.
 */
@Mapper(componentModel = "spring",
        uses = { CharacteristicMapper.class, RelationshipMapper.class, RelationshipTypeMapper.class })
public interface MappingValueMapper extends EntityMapper<MappingValueDTO, ValueGroup> {

    @Mapping(source = "parentCharValue.id", target = "valueOfParentId")
    @Mapping(source = "childCharValue.id", target = "valueOfChildId")
    @Mapping(source = "parentCharValue.name", target = "valueOfParent")
    @Mapping(source = "childCharValue.name", target = "valueOfChild")
    @Mapping(source = "charSpecRelationship.id", target = "relationshipId")
    @Mapping(source = "relationshipType.id", target = "relationshipTypeId")
    MappingValueDTO toDto(ValueGroup charSpecValueGroup);

    @Mapping(source = "valueOfParentId", target = "parentCharValue.id")
    @Mapping(source = "valueOfChildId", target = "childCharValue.id")
    @Mapping(source = "valueOfParent", target = "parentCharValue.name")
    @Mapping(source = "valueOfChild", target = "childCharValue.name")
    @Mapping(source = "relationshipId", target = "charSpecRelationship")
    @Mapping(source = "relationshipTypeId", target = "relationshipType")
    ValueGroup toEntity(MappingValueDTO mappingValueDTO);

    default ValueGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        ValueGroup charSpecValueGroup = new ValueGroup();
        charSpecValueGroup.setId(id);
        return charSpecValueGroup;
    }
}
