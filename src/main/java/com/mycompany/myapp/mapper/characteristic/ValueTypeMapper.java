package com.mycompany.myapp.mapper.characteristic;

import org.mapstruct.Mapper;

import com.mycompany.myapp.domain.characteristic.ValueType;
import com.mycompany.myapp.dto.characteristic.ValueTypeDTO;
import com.mycompany.myapp.mapper.EntityMapper;

/**
 * Mapper for the entity {@link ValueType} and its DTO {@link ValueTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ValueTypeMapper extends EntityMapper<ValueTypeDTO, ValueType> {

    ValueType toEntity(ValueTypeDTO valueTypeDTO);

    default ValueType fromId(Long id) {
        if (id == null) {
            return null;
        }
        ValueType valueType = new ValueType();
        valueType.setId(id);
        return valueType;
    }
}
