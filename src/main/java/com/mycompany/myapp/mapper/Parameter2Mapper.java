package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.Parameter;
import com.mycompany.myapp.dto.ParameterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface Parameter2Mapper extends EntityMapper<ParameterDTO, Parameter>{

    @Mapping(source = "description", target = "remark")
    Parameter toEntity(ParameterDTO dto);

    @Mapping(source = "remark", target = "description")
    ParameterDTO toDto(Parameter entity);
}
