package com.mycompany.myapp.mapper;

import com.mycompany.myapp.domain.GeoNetZone;
import com.mycompany.myapp.dto.GeoNetZoneDTO;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface GeoNetZoneMapper extends EntityMapper<GeoNetZoneDTO, GeoNetZone> {
    
    @IterableMapping
    default Timestamp toTimestamp(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Timestamp.valueOf(localDateTime);
    }
    
    @IterableMapping
    default LocalDateTime toLocalDateTime(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.toLocalDateTime();
    }
}
