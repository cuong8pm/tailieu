package com.mycompany.myapp.mapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mycompany.myapp.domain.BillingCycleType;
import com.mycompany.myapp.dto.BillingCycleTypeDTO;

@Mapper(componentModel = "spring", uses = {})
public interface BillingCycleTypeMapper extends EntityMapper<BillingCycleTypeDTO, BillingCycleType>{
    @Mapping(source = "unitId", target = "calcUnitId")
    BillingCycleType toEntity(BillingCycleTypeDTO dto);

    @Mapping(source = "calcUnitId", target = "unitId")
    BillingCycleTypeDTO toDto(BillingCycleType entity);
    
    @IterableMapping
    default Timestamp toTimestamp(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Timestamp.valueOf(localDateTime);
    }
    
    @IterableMapping
    default LocalDateTime toLocalDateTime(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.toLocalDateTime();
    }
}
