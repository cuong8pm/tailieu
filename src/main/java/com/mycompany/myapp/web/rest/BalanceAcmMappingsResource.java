package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.MapAcmBalBal;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BalanceAcmMappingDTO;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.BalancesService;
import com.mycompany.myapp.service.BalanceAcmMappingService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Iterator;
import java.util.List;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/balance-acm-mappings")
public class BalanceAcmMappingsResource {
    private final Logger log = LoggerFactory.getLogger(BalanceAcmMappingsResource.class);
    @Autowired
    private BalanceAcmMappingService balanceAcmMappingService;
    @Autowired
    private BalancesService balancesService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<BalanceAcmMappingDTO>> getMapAcmBalBals(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, MapAcmBalBal.FieldNames.POS_INDEX));

        List<BalanceAcmMappingDTO> list = balanceAcmMappingService.getMapAcmBalBals(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-balance-acm-mappings/{categoryId}")
    public ResponseEntity<Page<BalanceAcmMappingDTO>> getMapAcmBalBals(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        Page<BalanceAcmMappingDTO> page = balanceAcmMappingService.getMapAcmBalBals(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<BalanceAcmMappingDTO> getMapAcmBalBalDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        BalanceAcmMappingDTO balTypeDTO= balanceAcmMappingService.getMapAcmBalBalDetail(id);
        return ResponseEntity.ok().body(balTypeDTO);
    }

    // Them
    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> addMapAcmBalBal(
        @Validated(BalanceAcmMappingDTO.Create.class) @RequestBody BalanceAcmMappingDTO balType) {
        return ResponseEntity.status(HttpStatus.CREATED).body(balanceAcmMappingService.saveMapAcmBalBal(balType, true));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveMapAcmBalBal(
        @Validated(BalanceAcmMappingDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<BalanceAcmMappingDTO> balType) {
        if (balType.size() != 2) {
            throw new BadRequestAlertException("Bad request", "BalanceType's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(balanceAcmMappingService.move(balType));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteMapAcmBalBal(@PathVariable(name = "id") Long id) {
        balanceAcmMappingService.deleteMapAcmBalBal(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateMapAcmBalBal(
        @Validated(BalanceAcmMappingDTO.Update.class) @RequestBody BalanceAcmMappingDTO balTypeDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        balTypeDTO.setId(id);
        return ResponseEntity.ok().body(balanceAcmMappingService.updateMapAcmBalBal(balTypeDTO));
    }

    @GetMapping("/search-bal-types")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getMapAcmBalBals(
        @RequestParam(value = "name") String name) {
        List<Tree> list = balanceAcmMappingService.searchInTree(name.trim(), CategoryType.GEO_HOME_ZONE);
        return ResponseEntity.ok().body(list);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = OCSUtils.getMapAcmBalBalSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, MapAcmBalBal.FieldNames.POS_INDEX) : sort);
    }
    // tree balance acm trong popup
    @GetMapping("/balance-acm-tree")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBalanceTree(@RequestParam("name") String name) {
        name = StringUtils.isEmpty(name) ? null : name;
        List<Tree> list = balancesService.searchInTree(name, CategoryType.ACCUMULATE_BALANCES);
        return ResponseEntity.ok().body(list);
    }
}
