package com.mycompany.myapp.web.rest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.javatuples.Quartet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mycompany.myapp.config.ApplicationProperties;
import com.mycompany.myapp.domain.GeoNetZone;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.GeoNetZoneDTO;
import com.mycompany.myapp.service.GeoNetZoneService;
import com.mycompany.myapp.utils.ExcelUtils;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/geo-net-zones")
public class GeoNetZoneResource {
    private final Logger log = LoggerFactory.getLogger(GeoNetZone.class);
    @Autowired
    private GeoNetZoneService geoNetZoneService;

    @Autowired
    private ApplicationProperties applicationProperties;

    private static final String EXTENSION = ".xls";
    private static final String FILE_EXPORT = "GeoNetZone";

    @GetMapping("/list-child/{geoHomeZoneId}")
    public ResponseEntity<List<GeoNetZoneDTO>> getGeoNetZones(
        @PathVariable(value = "geoHomeZoneId") @NotNull @Positive Long geoHomeZoneId) {

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, GeoNetZone.FieldNames.CELL_ID));

        List<GeoNetZoneDTO> list = geoNetZoneService.getGeoNetZones(geoHomeZoneId,null,null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-geo-net-zones/{geoHomeZoneId}")
    public ResponseEntity<Page<GeoNetZoneDTO>> getGeoNetZones(
        @PathVariable(value = "geoHomeZoneId") @NotNull @Positive Long geoHomeZoneId,
        @RequestParam(value = "cellId", required = false) String cellId,
        @RequestParam(value = "updateDate", required = false) String updateDate,
        Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<GeoNetZoneDTO> page = geoNetZoneService.getGeoNetZones(geoHomeZoneId, cellId, updateDate, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<GeoNetZoneDTO> getGeoNetZoneDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        GeoNetZoneDTO geoNetZoneDTO= geoNetZoneService.getGeoNetZoneDetail(id);
        return ResponseEntity.ok().body(geoNetZoneDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addGeoNetZone(
        @Validated(GeoNetZoneDTO.Create.class) @RequestBody GeoNetZoneDTO geoNetZone) {
        geoNetZone.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(geoNetZoneService.createGeoNetZone(geoNetZone));
    }

    // Xoa
    @DeleteMapping("/{ids}")
    public ResponseEntity<BaseResponseDTO> deleteGeoNetZone(@PathVariable(name = "ids") List<Long> ids) {
        geoNetZoneService.deleteGeoNetZone(ids);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateGeoNetZone(
        @Validated(GeoNetZoneDTO.Update.class) @RequestBody GeoNetZoneDTO geoNetZoneDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        geoNetZoneDTO.setId(id);
        return ResponseEntity.ok().body(geoNetZoneService.updateGeoNetZone(geoNetZoneDTO));
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getGeoNetZoneSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(new ArrayList<>()) : sort);
    }

    @GetMapping(path = "/download")
    public ResponseEntity<Resource> downloadTemplate() throws IOException {

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + applicationProperties.getGeoNetZoneTemplateFileName());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
        ByteArrayResource resource = null;
        File file = null;
        try {
            String filePath = applicationProperties.getExcelTemplateRootDirectoryPath() + applicationProperties.getGeoNetZoneTemplateFileName();
            Path path = Paths.get(filePath);
            file = new File(filePath);
            if(file.length() == 0) {
                throw new Exception();
            }
            resource = new ByteArrayResource(Files.readAllBytes(path));
        } catch (Exception e) {
            log.error("Request download template : {}", e.getMessage());
            throw new BadRequestAlertException("excel_file_does_not_exist", Resources.GEO_HOME_ZONE, "file");
        }

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(resource);
    }

    @PostMapping("/{geoHomeZoneId}/import")
    public ResponseEntity<?> importDataFromFile(@RequestParam("file") MultipartFile readExcelDataFile, @PathVariable Long geoHomeZoneId) throws IOException {
        Quartet<Boolean, ByteArrayInputStream, Integer, Integer> quartet = geoNetZoneService
                .importDataFromFile(readExcelDataFile, geoHomeZoneId);
        String[] fileName = readExcelDataFile.getOriginalFilename().split("\\.");
        HttpHeaders header = new HttpHeaders();
        LocalDateTime now = LocalDateTime.now().plusHours(7);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy_hhmmss");
        String nameOfFile = fileName[0] + "_" + dtf.format(now) + "." + fileName[fileName.length - 1];
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + nameOfFile);
        header.add("Number-Of-Records", "" + quartet.getValue2());
        header.add("Number-Of-Valid-Records", "" + quartet.getValue3());
        header.add("Is-Success", "" + quartet.getValue0());
        InputStreamResource file = new InputStreamResource(quartet.getValue1());
        if ("txt".equals(fileName[fileName.length - 1])) {
            File fileTemp = new File(applicationProperties.getExcelTemplateRootDirectoryPath()
                    + readExcelDataFile.getOriginalFilename() + dtf.format(now));
            if (fileTemp.delete()) {
                System.out.println("Deleted the file: " + fileTemp.getName());
            }
            return ResponseEntity.ok()
                    .headers(header)
                    .contentType(MediaType.TEXT_XML)
                    .body(file);
        }
        return ResponseEntity.ok()
                .headers(header)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/{geoHomeZoneId}/export")
    public ResponseEntity<Resource> exportGeoNetZoneData(@PathVariable Long geoHomeZoneId) throws IOException {
        String filename = FILE_EXPORT + EXTENSION;
        InputStreamResource file = new InputStreamResource(geoNetZoneService.exportGeoNetZoneData(geoHomeZoneId));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
}
