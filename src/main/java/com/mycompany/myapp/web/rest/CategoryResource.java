package com.mycompany.myapp.web.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.Category;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;
import liquibase.pro.packaged.T;

@RestController
@IgnoreWildCard
@RequestMapping("/api/categories")
public class CategoryResource {

    private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/list-child/{id}")
    public ResponseEntity<List<CategoryDTO>> listChild(
//        @RequestParam(value = "type") Integer type,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        @RequestParam(value = "searchAll", required = false) boolean searchAll,
        @PathVariable(value = "id") Long parentId) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        SearchCriteria searchCriteria = new SearchCriteria(name, description);
        searchCriteria.setSearchAll(searchAll);
        searchCriteria.setParentId(parentId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Category.FieldNames.POS_INDEX));
        Page<CategoryDTO> page = categoryService.getCategory(searchCriteria, unpaged);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/list-child-category/{id}")
    public ResponseEntity<Page<CategoryDTO>> listChildCategory(
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        @RequestParam(value = "searchAll", required = false) boolean searchAll,
        @PathVariable(value = "id") Long parentId,
        Pageable pageable) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        pageable = validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description);
        searchCriteria.setSearchAll(searchAll);
        searchCriteria.setParentId(parentId);
        Page<CategoryDTO> page = categoryService.getCategory(searchCriteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), Pageable.unpaged());
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/list-parent/{type}")
    public ResponseEntity<List<CategoryDTO>> getRootCategory(
        @PathVariable(value = "type") List<Integer> type) {
        List<CategoryType> categoryTypes = type.stream().map(value -> CategoryType.of(value)).collect(Collectors.toList());
        List<CategoryDTO> page = categoryService.getRootCategory(categoryTypes, false);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategoryDTO> getDetails(
        @PathVariable(value = "id") Long categoryParentId,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        pageable = validatePageable(pageable);
        return ResponseEntity.ok().body(categoryService.getCategoryDetail(categoryParentId, description, pageable));
    }

    @GetMapping("/list-all-category/{type}")
    public ResponseEntity<List<Tree>> getAllCategory(@PathVariable(name = "type") Integer type) {
        CategoryType categoryType = CategoryType.of(type);
        List<Tree> list = categoryService.getAllCategory(categoryType);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addCategory(
        @Validated(CategoryDTO.Create.class) @RequestBody CategoryDTO categoryDTO) {
        categoryDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.createCategory(categoryDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateCategory(
        @Validated(CategoryDTO.Update.class) @RequestBody CategoryDTO categoryDTO,
        @PathVariable(name = "id", required = true) Long id) {
        validateId(id);
        categoryDTO.setId(id);
        return ResponseEntity.ok().body(categoryService.updateCategory(categoryDTO));
    }

    @PutMapping("/move")
    public ResponseEntity<BaseResponseDTO> moveCategory(
            @Validated(CategoryDTO.Move.class) @RequestBody List<CategoryDTO> categoryDTOs) {
        if (categoryDTOs.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Category", "");
        }
        return ResponseEntity.ok().body(categoryService.move(categoryDTOs));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteCategory(@PathVariable(name = "id", required = true) Long id) {
        validateId(id);
        categoryService.deleteCategories(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
    private void validateId(Long id) {
        if (id == null || id < 1) {
            throw new BadRequestAlertException("Id must be greater than 1", "Category", "");
        }
    }

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/test/{id}")
    public ResponseEntity<Category> test(@PathVariable Long id) {
        categoryService.move(new ArrayList<>());
        return ResponseEntity.ok().body(null);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Order order = iterator.next();
            String propery = OCSUtils.getCategorySearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC, Category.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/search-categories")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchCategories (
        @RequestParam(value = "name") String name, @RequestParam(value = "type") Integer type) {
        CategoryType categoryType = CategoryType.of(type);
        List<Tree> list = categoryService.searchInTree(name.trim(), categoryType);
        return ResponseEntity.ok().body(list);
    }

    @PutMapping("/check-parent-category")
    public ResponseEntity<T> checkCategories(
        @RequestBody CategoryDTO categoryDTO) {
        categoryService.checkInfinityLoop(categoryDTO);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/list-child")
    public ResponseEntity<List<Tree>> getListObjectByCategoryId(
        @PathVariable(value = "id") Long categoryId){
        return null;
    }

}
