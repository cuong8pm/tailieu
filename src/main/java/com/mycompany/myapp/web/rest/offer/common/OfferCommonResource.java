package com.mycompany.myapp.web.rest.offer.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.utils.IgnoreWildCard;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.common.DropDownBillingCycleDTO;
import com.mycompany.myapp.dto.offer.common.OfferCloneDTO;
import com.mycompany.myapp.dto.offer.common.OfferCommonDTO;
import com.mycompany.myapp.dto.offer.common.VersionBundleCommonDTO;
import com.mycompany.myapp.service.ReferTableService;
import com.mycompany.myapp.service.offer.common.OfferCommonService;
import com.mycompany.myapp.service.offer.common.VersionBundleCommonService;
import com.mycompany.myapp.service.offer.offer.OfferService;
import com.mycompany.myapp.service.offer.offer.OfferVersionBundleService;
import com.mycompany.myapp.service.offer.template.OfferTemplateService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;

import io.github.jhipster.web.util.PaginationUtil;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api")
public abstract class OfferCommonResource<D extends OfferCommonDTO, E extends OfferCommon > extends AbstractResource<OfferCloneDTO> {

    protected OfferCommonService<D, E> offerCommonService;

    @Autowired
    private ReferTableService referTableService;

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferTemplateService offerTemplateService;

    protected VersionBundleCommonService<? extends VersionBundleCommonDTO, ? extends VersionBundleCommon> versionBundleCommonService;

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> create(@NotNull @RequestBody @Validated(OfferCommonDTO.Create.class) D commonDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(offerCommonService.create(commonDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> update(@NotNull @RequestBody @Validated(OfferCommonDTO.Create.class) D commonDTO, @PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(offerCommonService.update(commonDTO, id));
    }

    // Get popup tree with no paging
    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<D>> getTreeOffer(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
            Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.NAME));
        List<D> list = offerCommonService.getOfferCommons(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get tree with paging and search
    @GetMapping("/list-child-offers/{categoryId}")
    public ResponseEntity<Page<D>> getOffers(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "id", required = false) String id,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description, categoryId);

        Page<D> page = offerCommonService.getOfferCommons(searchCriteria, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.NAME) : sort);
    }

    //Get Detail
    @GetMapping("/{id}")
    public ResponseEntity<D> getOfferDetail(@PathVariable(value = "id") @NotNull @Positive Long id) {
        D offerCommonDTO = offerCommonService.getOfferDetail(id);

        return ResponseEntity.ok().body(offerCommonDTO);
    }

    //Delete
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteAction(@Positive @PathVariable Long id) {
        offerCommonService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    protected abstract CategoryType getCategoryType();

    @GetMapping("/payment-type")
    public ResponseEntity<List<ReferTable>> getPaymentTypes() {
        return ResponseEntity.ok().body(referTableService.getReferTables(ReferType.PaymentType));
    }

    @GetMapping("/offer-type")
    public ResponseEntity<List<ReferTable>> getOfferTypes() {
        return ResponseEntity.ok().body(referTableService.getReferTables(ReferType.OfferType));
    }

    @GetMapping("/units")
    public ResponseEntity<List<ReferTable>> getCalcUnits() {
        return ResponseEntity.ok(referTableService.getReferTables(ReferType.CALC_UNIT.getValue()));
    }

    @PostMapping("/{id}/clone")
    public ResponseEntity<OCSCloneableEntity> cloneRatingFilter(@PathVariable("id") @Positive Long id,
                                                                @RequestBody @Valid OfferCloneDTO cloneDTO) {
        cloneDTO.setId(id);
        List<CloneDTO> cloneDTOs = new ArrayList<>();
        cloneDTOs.add(cloneDTO);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<OCSCloneableEntity> results = ocsCloneService.cloneEntities(cloneDTOs, oldNewChildMap);
        ocsCloneService.clearCache();

        return ResponseEntity.status(HttpStatus.CREATED).body(results.get(0));
    }

    @GetMapping("/list-billing-cycle-types")
    public ResponseEntity<List<DropDownBillingCycleDTO>> getLstDropDownBillingCycle() {
        return ResponseEntity.ok(offerCommonService.getListBillingCycles());
    }
}
