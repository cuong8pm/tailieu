package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.Constants;
import com.mycompany.myapp.domain.constant.NormalizerType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.domain.normalizer.NormalizerParam;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.normailizer.NormValueDTO;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.domain.normalizer.Normalizer;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.normailizer.validator.PreFunctionDTO;
import com.mycompany.myapp.repository.normalizer.NestedObjectClassRepository;
import com.mycompany.myapp.service.normalizer.*;
import com.mycompany.myapp.service.characteristic.CharacteristicService;
import com.mycompany.myapp.service.CategoryService;
import com.mycompany.myapp.service.ReferTableService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@IgnoreWildCard
@RequestMapping("/api/normalizers")
public class NormalizerResource extends AbstractResource {

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private ReferTableService referTableService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private NestedObjectService nestedObjectService;

    @Autowired
    private PreFunctionService preFunctionService;

    @Autowired
    private CharacteristicService characteristicService;

    @Autowired
    private NestedObjectClassRepository nestedObjectClassRepository;

    @Autowired
    private NormValueService normValueService;

    private NormalizerService normalizerService;

    private NormalizerService getNormalizerService(NormalizerType normalizerType) {
        return beanFactory.getBean(getNormalizerServiceName(normalizerType), NormalizerService.class);
    }

    private String getNormalizerServiceName(NormalizerType normalizerType) {
        switch (normalizerType) {
        case TimeNormalizer:
            return "timeNormalizerService";
        case DateNormalizer:
            return "dateNormalizerService";
        case ZoneNormalizer:
            return "zoneNormalizerService";
        default:
            return "normalNormalizerServiceImp";
        }
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> addNormalizer(
            @Validated(NormalizerDTO.Create.class) @RequestBody NormalizerDTO normalizerDTO) {
        normalizerDTO.setId(null);
        BaseResponseDTO base = getNormalizerService(normalizerDTO.getTypeId()).createNormalizer(normalizerDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(base);
    }

    // Get tree with no paging
    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<NormalizerDTO>> getNormalizers(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, ReserveInfo.FieldNames.POS_INDEX));
        normalizerService = getNormalizerService(NormalizerType.StringNormalizer);
        List<NormalizerDTO> list = normalizerService.getNormalizerForm(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Search in tree
    @GetMapping("/search-normalizers")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchNormalizers(@RequestParam(value = "name") String name) {
        normalizerService = getNormalizerService(NormalizerType.StringNormalizer);
        List<Tree> list = normalizerService.searchInTree(name, CategoryType.NORMALIZER);
        return ResponseEntity.ok().body(list);
    }

    // Get tree with paging and search
    @GetMapping("/list-child-normalizers/{categoryId}")
    public ResponseEntity<Page<NormalizerDTO>> getNormalizers(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
            @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description, categoryId);
        normalizerService = getNormalizerService(NormalizerType.StringNormalizer);
        Page<NormalizerDTO> page = normalizerService.getNormalizerForm(searchCriteria, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Get detail
    @GetMapping("/{id}")
    public ResponseEntity<NormalizerDTO> getNormalizerDetail(@PathVariable(value = "id") @NotNull @Positive Long id) {
        // normalizerService = getNormalizerService(null);
        NormalizerDTO normalizerDTO = getNormalizerService(NormalizerType.StringNormalizer).getNormalizerDetail(id);
        if (!CollectionUtils.isEmpty(normalizerDTO.getParameters())) {
            normalizerDTO.getParameters().forEach(param -> {
                if (param.containsKey(Constants.ACTION_TYPE)) {
                    param.put(Constants.ACTION_TYPE, null);
                }
            });
        }
        return ResponseEntity.ok().body(normalizerDTO);
    }

    //Get list value of normalizer no paging
    @GetMapping("/{normalizerId}/list-values")
    public ResponseEntity<List<NormValueDTO>> getValueByNormalizer(@PathVariable("normalizerId") Long normalizeId) {
        List<NormValueDTO> normValues = normValueService.getAllByNormalizerId(normalizeId);
        return ResponseEntity.ok().body(normValues);
    }

    //delete
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteNormalizer(
            @PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        getNormalizerService(NormalizerType.StringNormalizer).deleteNormalizer(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateNormalizer(
            @Validated(NormalizerDTO.Update.class) @RequestBody NormalizerDTO normalizer,
            @PathVariable(name = "id") @NotNull @Positive Long id) {
        normalizer.setId(id);
        BaseResponseDTO base = getNormalizerService(normalizer.getTypeId()).updateNormalizer(normalizer);
        return ResponseEntity.ok().body(base);
    }

    // Search in tree
    @GetMapping("/nested-objects")
    public ResponseEntity<Collection<NestedObject>> searchNestedObject() {
        NestedObjectClass nestedObjectClass = nestedObjectClassRepository.findById(100L).orElse(new NestedObjectClass());
        NestedObject root = new NestedObject();
        BeanUtils.copyProperties(nestedObjectClass, root);
        root.setAlist(true);
        root.setHasChild(true);
        // nestedObjectService.findByParentClassId(100L);
        Collection<NestedObject> list = nestedObjectService.searchTree(100L);
        root.setTemplates(list.stream().map(nestedObject1 -> (Tree) nestedObject1).collect(Collectors.toList()));
        return ResponseEntity.ok().body(Collections.singletonList(root));
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getNormalizerSearchParams(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, Normalizer.FieldNames.POS_INDEX) : sort);
    }

    // <editor-fold desc="Dropdown list and popup">
    // dropdown list
    @GetMapping("/normalizer-types")
    public ResponseEntity<List<ReferTable>> getNormalizerTypes() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerType);
        return ResponseEntity.ok().body(referTables);
    }

    @GetMapping("/categories")
    @JsonView(Tree.class)
    public ResponseEntity<List<CategoryDTO>> getDDCategories() {
        List<CategoryDTO> categories = categoryService.getRootCategory(CategoryType.NORMALIZER, true);
        return ResponseEntity.ok().body(categories);
    }

    @GetMapping("/normalizer-states")
    public ResponseEntity<List<ReferTable>> getDDNormalizerStates() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerState);
        return ResponseEntity.ok().body(referTables);
    }

    @GetMapping("/filter-field-names/{parentId}")
    @JsonView(NestedObject.Dropdown.class)
    public ResponseEntity<List<NestedObject>> getDDNestedObjectByParent(
            @PathVariable(name = "parentId") Long parentId) {
        List<NestedObject> result = nestedObjectService.findByParentClassIdNotHaveChild(parentId);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/pre-function")
    @JsonView(PreFunctionDTO.Dropdown.class)
    public ResponseEntity<List<PreFunctionDTO>> getDDPreFunctions() {
        List<PreFunctionDTO> result = preFunctionService.getAll();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/characteristics-tree")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getTreeCharSpec(@RequestParam("name") String name) {
        name = StringUtils.isEmpty(name) ? null : name;
        List<Tree> charSpecs = characteristicService.searchInTree(name, CategoryType.CHARACTERISTIC);
        return ResponseEntity.ok().body(charSpecs);
    }

    @GetMapping("/string-normalizer-compare-types")
    public ResponseEntity<List<ReferTable>> getDDStringNormalizerCompareTypes() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerStringCompareType);
        return ResponseEntity.ok().body(referTables);
    }

    @GetMapping("/number-normalizer-compare-types")
    public ResponseEntity<List<ReferTable>> getDDNumberNormalizerCompareTypes() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerNumberCompareType);
        return ResponseEntity.ok().body(referTables);
    }

    @GetMapping("/compare-modes")
    public ResponseEntity<List<ReferTable>> getDDCompareMode() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerCompareMode);
        return ResponseEntity.ok().body(referTables);
    }

    @GetMapping("/start-types")
    public ResponseEntity<List<ReferTable>> getDDStartType() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerStartType);
        return ResponseEntity.ok().body(referTables);
    }

    @GetMapping("/zone-data-types")
    public ResponseEntity<List<ReferTable>> getDDZoneDataType() {
        List<ReferTable> referTables = referTableService.getReferTables(ReferType.NormalizerZoneType);
        return ResponseEntity.ok().body(referTables);
    }

    @PostConstruct
    @Override
    protected void setBaseService() {
       this.ocsCloneService = getNormalizerService(NormalizerType.StringNormalizer);
    }

}
