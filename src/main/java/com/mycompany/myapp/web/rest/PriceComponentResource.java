package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.pricecomponent.PriceComponent;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.service.pricecomponent.PriceComponentBlockMapService;
import com.mycompany.myapp.service.pricecomponent.PriceComponentService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

@RestController
@IgnoreWildCard
@RequestMapping("/api/price-components")
public class PriceComponentResource extends AbstractResource {
    @Autowired
    private PriceComponentService priceComponentService;

    @Autowired
    private PriceComponentBlockMapService priceComponentBlockMapService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = priceComponentService;
        this.childMappingService = priceComponentBlockMapService;

    }

    @GetMapping("/list-child-price-components/{categoryId}")
    public ResponseEntity<Page<PriceComponentDTO>> getRateTables(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<PriceComponentDTO> dtos = priceComponentService.getPriceComponents(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getPriceComponentSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, PriceComponent.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<PriceComponentDTO>> getPriceComponents(@PathVariable Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, PriceComponent.FieldNames.POS_INDEX));
        List<PriceComponentDTO> dtos = priceComponentService.getPriceComponents(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PriceComponentDTO> getPriceComponentDetail(@PathVariable Long id) {
        PriceComponentDTO dto = priceComponentService.getPriceComponentDetail(id);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/search-price-components")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getPriceComponentTree(@RequestParam(required = false) String name) {
        List<Tree> list = priceComponentService.searchInTree(name, CategoryType.PRICE_COMPONENT);
        return ResponseEntity.ok().body(list);
    }
    
    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> createPriceComponent(@RequestBody @Validated(PriceComponentDTO.Create.class) PriceComponentDTO priceComponentDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(priceComponentService.createPriceComponent(priceComponentDTO));
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updatePriceComponent(@PathVariable Long id, @RequestBody @Validated(PriceComponentDTO.Update.class) PriceComponentDTO priceComponentDTO) {
        priceComponentDTO.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(priceComponentService.updatePriceComponent(priceComponentDTO));
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePriceComponent(@PathVariable Long id ) {
    	priceComponentService.deletePriceComponent(id);
    	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @GetMapping("/list-child-blocks/{priceComponentId}")
    public ResponseEntity<List<BlockDTO>> getBlocks(@PathVariable Long priceComponentId) {
        List<BlockDTO> dtos = priceComponentBlockMapService.getBlocks(priceComponentId);
        return ResponseEntity.ok().body(dtos);
    }
}
