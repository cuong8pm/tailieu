package com.mycompany.myapp.web.rest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.PositiveOrZero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.normalizer.NormalizerValue;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ratetable.RateTableColumnDTO;
import com.mycompany.myapp.service.ratetable.RateTableColumnService;
import com.mycompany.myapp.utils.IgnoreWildCard;

@RestController
@IgnoreWildCard
@RequestMapping("/api/rate-table-columns")
public class RateTableColumnResource {

    @Autowired
    private RateTableColumnService rateTableColumnService;

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteRateTable(@PathVariable @PositiveOrZero Long id) {
        rateTableColumnService.deleteColumn(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @JsonView(NormalizerValue.class)
    @GetMapping("/list-normalizer-value/{normalizerId}")
    public ResponseEntity<List<NormalizerValue>> getReferTables(@PathVariable Long normalizerId) {
        return ResponseEntity.ok().body(rateTableColumnService.getNormalizeValue(normalizerId));
    }

    @JsonView(NormalizerValue.class)
    @GetMapping("/list-normalizer-value")
    public ResponseEntity<List<List<NormalizerValue>>> getReferTables(@RequestParam Long[] normalizerIds) {
        List<List<NormalizerValue>> allNormValueId = new ArrayList<>();
        for (Long normalizerId : normalizerIds) {
            allNormValueId.add(rateTableColumnService.getNormalizeValue(normalizerId));
        }
        return ResponseEntity.ok().body(allNormValueId);
    }

    @GetMapping("/list-column/{rateTableId}")
    public ResponseEntity<List<RateTableColumnDTO>> getListColumns(@PathVariable Long rateTableId) {
        return ResponseEntity.ok().body(rateTableColumnService.getListColumns(rateTableId));
    }
}
