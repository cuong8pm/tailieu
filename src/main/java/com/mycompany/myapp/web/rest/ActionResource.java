package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.pricecomponent.PriceComponentDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.service.action.ActionPriceComponentMapService;
import com.mycompany.myapp.service.action.ActionService;
import com.mycompany.myapp.service.offer.offer.OfferVersionService;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import io.github.jhipster.web.util.PaginationUtil;
import liquibase.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/actions")
public class ActionResource extends AbstractResource {
    @Autowired
    private ActionService actionService;
    @Autowired
    private ActionPriceComponentMapService actionPriceComponentMapService;
    @Autowired
    private OfferTemplateVersionService offerTemplateVersionService;
    @Autowired
    private OfferVersionService offerVersionService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = actionService;
        this.childMappingService = actionPriceComponentMapService;
    }

    @GetMapping("/search-actions")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getAction(@RequestParam(value = "name", defaultValue = "") String name,
                                                @RequestParam(required = false) Long offerVersionId, @RequestParam(required = false) Long offerTemplateVersionId, @RequestParam(required = false) Boolean isPopup) {
        List<Long> removeIds = new ArrayList<>();
        if (offerTemplateVersionId != null) {
            removeIds = offerTemplateVersionService.removeId(offerTemplateVersionId);
        }
        if (offerVersionId != null) {
            removeIds = offerVersionService.removeId(offerVersionId);
        }
        List<Tree> list = actionService.searchInTree(name.trim(), CategoryType.ACTION, removeIds, isPopup);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity<BaseResponseDTO> createAction(@Validated(ActionDTO.Create.class) @NotNull @RequestBody ActionDTO actionDTO) throws ParseException {
        return ResponseEntity.status(HttpStatus.CREATED).body(actionService.createAction(actionDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateAction(@Validated(ActionDTO.Update.class) @NotNull @RequestBody ActionDTO actionDTO,
                                                        @PathVariable Long id) {
        actionDTO.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(actionService.updateAction(actionDTO));
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<ActionDTO>> getByCategoryId(@PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Block.FieldNames.POS_INDEX));
        List<ActionDTO> list = actionService.getActions(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-actions/{categoryId}")
    public ResponseEntity<Page<ActionDTO>> getActionWithPage(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        pageable = validatePageable(pageable);

        Page<ActionDTO> page = actionService.getActionWithPage(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = OCSUtils.getActionSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, Action.FieldNames.POS_INDEX) : sort);
    }

    @DeleteMapping("/{actionId}")
    public ResponseEntity<BaseResponseDTO> deleteAction(@Positive @PathVariable Long actionId) {
        actionService.deleteAction(actionId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ActionDTO> getBlockDetail(@NotNull @Positive @PathVariable Long id) {
        return ResponseEntity.ok().body(actionService.getActionDetail(id));
    }

    @GetMapping("/list-child-price-components/{actionId}")
    public ResponseEntity<List<PriceComponentDTO>> getPriceComponentByAction(@NotNull @Positive @PathVariable Long actionId) {
        return ResponseEntity.ok().body(actionService.getPriceComponentByActionId(actionId));
    }

    @GetMapping("/list-child-sub-actions/{actionId}")
    public ResponseEntity<List<TreeClone>> getActionInTree(@NotNull @Positive @PathVariable Long actionId) {
        return ResponseEntity.ok().body(actionService.getActionInTree(actionId));
    }

    @GetMapping("/list-child-rating-filters/{id}")
    public ResponseEntity<List<RatingFilterDTO>> getRatingFilter(@NotNull @Positive @PathVariable Long id) {
        return ResponseEntity.ok().body(actionService.getRatingFilter(id));
    }
}
