package com.mycompany.myapp.web.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.utils.IgnoreWildCard;

@RestController
@IgnoreWildCard
@RequestMapping("/api/rate-table-rows")
public class RateTableRowResource {

}
