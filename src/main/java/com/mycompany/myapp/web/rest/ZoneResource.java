package com.mycompany.myapp.web.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.ZoneDTO;
import com.mycompany.myapp.service.ZoneService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/zones")
public class ZoneResource {
    private final Logger log = LoggerFactory.getLogger(ZoneResource.class);
    @Autowired
    private ZoneService zoneService;

    @GetMapping("/list-child/{zoneMapId}")
    public ResponseEntity<List<ZoneDTO>> getZones(
        @PathVariable(value = "zoneMapId") @NotNull @Positive Long zoneMapId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Zone.FieldNames.ID));
        List<ZoneDTO> list = zoneService.getZones(zoneMapId, null, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-zones/{zoneMapId}")
    public ResponseEntity<Page<ZoneDTO>> getZones(
        @PathVariable(value = "zoneMapId") @NotNull @Positive Long zoneMapId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        @RequestParam(value = "zoneCode", required = false) String zoneCode,
        @RequestParam(value = "id", required = false) String id,
        Pageable pageable) {
        pageable = validatePageable(pageable);
        description = StringUtils.isEmpty(description) ? null : description;
        name = StringUtils.isEmpty(name) ? null : name;
        zoneCode = StringUtils.isEmpty(zoneCode) ? null : zoneCode;
        Page<ZoneDTO> page = zoneService.getZones(zoneMapId, name, description, zoneCode, id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<ZoneDTO> getZoneDetail(@PathVariable(value = "id") @NotNull @Positive Long id) {
        ZoneDTO zoneDTO = zoneService.getZoneDetail(id);
        return ResponseEntity.ok().body(zoneDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addZone(
        @Validated(ZoneDTO.Create.class) @RequestBody ZoneDTO zone) {
        zone.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(zoneService.createZone(zone));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteZone(@PathVariable(name = "id") Long id) {
        zoneService.deleteZone(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateZone(
        @Validated(ZoneDTO.Update.class) @RequestBody ZoneDTO zoneDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        zoneDTO.setId(id);
        return ResponseEntity.ok().body(zoneService.updateZone(zoneDTO));
    }
    
    @GetMapping("/search-zones")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getZoneMaps(
        @RequestParam(value = "name") String name) {
        List<Tree> list = zoneService.searchInTree(name, CategoryType.ZONE);
        return ResponseEntity.ok().body(list);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getZoneSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, Zone.FieldNames.ID) : sort);
    }

}
