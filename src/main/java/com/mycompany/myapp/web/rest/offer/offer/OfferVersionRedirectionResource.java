package com.mycompany.myapp.web.rest.offer.offer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.offer.OfferVersionRedirection;
import com.mycompany.myapp.dto.offer.offer.OfferVersionRedirectionDTO;
import com.mycompany.myapp.service.offer.offer.OfferVersionRedirectionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.offer.common.VersionRedirectionCommonResource;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-version-redirection")
public class OfferVersionRedirectionResource extends VersionRedirectionCommonResource<OfferVersionRedirectionDTO, OfferVersionRedirection> {

    @Autowired
    private OfferVersionRedirectionService offerVersionRedirectionService;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerVersionRedirectionService;
    }

    
}
