package com.mycompany.myapp.web.rest.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.BuilderTypeItem;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeItemDTO;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeItemService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;

@RestController
@IgnoreWildCard
@RequestMapping("api/builder-type-items")
public class BuilderTypeItemResource extends AbstractResource {


    @Autowired
    BuilderTypeItemService builderTypeItemService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = builderTypeItemService;
    }

    public ResponseEntity<BuilderTypeItemDTO> createBuilderTypeItem( Long builderTypeId, BuilderTypeItemDTO builderTypeItemDTO){
        builderTypeItemDTO.setBuilderTypeId(builderTypeId);
        return ResponseEntity.status(HttpStatus.CREATED).body(builderTypeItemService.createBuilderTypeItem(builderTypeItemDTO));
    }

    public ResponseEntity<Page<BuilderTypeItemDTO>> getBuilderTypeItemsByBuilderTypeId(String name, Long builderTypeId, Pageable pageable){
        pageable = pageableValidation(pageable);
        if (name == null) {
            name = "";
        }
        Page<BuilderTypeItemDTO> page = builderTypeItemService.getBuilderTypeItemsByBuilderTypeId(builderTypeId, name, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    public ResponseEntity<BuilderTypeItemDTO> updateBuilderTypeItem(Long builderTypeId,Long builderTypeItemId,BuilderTypeItemDTO builderTypeItemDTO) {
        builderTypeItemDTO.setBuilderTypeId(builderTypeId);
        builderTypeItemDTO.setId(builderTypeItemId);
        return ResponseEntity.status(HttpStatus.CREATED).body(builderTypeItemService.updateBuilderTypeItem(builderTypeItemDTO));
    }

    public ResponseEntity<?> deleteBuilderTypeItem(Long builderTypeId,Long builderTypeItemId) {
        builderTypeItemService.deleteBuilderTypeItem(builderTypeId, builderTypeItemId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public String getSortFieldName() {
        return BuilderTypeItem.FieldNames.NAME;
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getBuilderTypeItemSearchParam(order.getProperty());
    }
}
