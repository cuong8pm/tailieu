package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.domain.ZoneMap;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.repository.ZoneRepository;
import com.mycompany.myapp.service.ZoneDataService;
import com.mycompany.myapp.service.ZoneMapService;
import com.mycompany.myapp.service.ZoneService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/api/zone-maps")
@IgnoreWildCard
public class ZoneMapResource {
    private final Logger log = LoggerFactory.getLogger(ZoneMapResource.class);

    @Autowired
    private ZoneMapService zoneMapService;
    
    @Autowired
    private ZoneRepository zoneRepository;
    
    @Autowired
    private ZoneService zoneService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<ZoneMapDTO>> getZoneMaps(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, ZoneMap.FieldNames.POS_INDEX));

        List<ZoneMapDTO> list = zoneMapService.getZoneMaps(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-zone-maps/{categoryId}")
    public ResponseEntity<Page<ZoneMapDTO>> getZoneMaps(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        pageable = validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description, categoryId);
        Page<ZoneMapDTO> page = zoneMapService.getZoneMaps(searchCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<ZoneMapDTO> getZoneMapDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        ZoneMapDTO zoneMapDTO= zoneMapService.getZoneMapDetail(id);
        return ResponseEntity.ok().body(zoneMapDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addZoneMap(
        @Validated(ZoneMapDTO.Create.class) @RequestBody ZoneMapDTO zoneMap) {
        zoneMap.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(zoneMapService.createZoneMap(zoneMap));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveZoneMap(
        @Validated(ZoneMapDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<ZoneMapDTO> zoneMap) {
        if (zoneMap.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Zone Map's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(zoneMapService.move(zoneMap));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteZoneMap(@PathVariable(name = "id") Long id) {
        zoneMapService.deleteZoneMap(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateZoneMap(
        @Validated(ZoneMapDTO.Update.class) @RequestBody ZoneMapDTO zoneMapDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        zoneMapDTO.setId(id);
        return ResponseEntity.ok().body(zoneMapService.updateZoneMap(zoneMapDTO));
    }

    @GetMapping("/search-zone-maps")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getZoneMaps(
        @RequestParam(value = "name") String name) {
        List<Tree> list = zoneMapService.searchInTree(name, CategoryType.ZONE);
        return ResponseEntity.ok().body(list);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCategorySearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, ZoneMap.FieldNames.POS_INDEX) : sort);
    }
    
    @GetMapping("/{zoneMapId}/zones")
    public ResponseEntity<List<ZoneDTO>> getZones(
        @PathVariable(value = "zoneMapId") @NotNull @Positive Long zoneMapId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Zone.FieldNames.ID));
        List<ZoneDTO> list = zoneService.getZones(zoneMapId, null, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

}
