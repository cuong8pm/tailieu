package com.mycompany.myapp.web.rest.offer.offer;

import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferVersionAction;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionActionDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.service.offer.offer.OfferVersionActionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.offer.common.VersionActionCommonResource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Iterator;
import java.util.List;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-version-actions")
public class OfferVersionActionResource extends VersionActionCommonResource<OfferVersionActionDTO, OfferVersionAction> {
    @Autowired
    private OfferVersionActionService offerVersionActionService;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerVersionActionService;
    }

    @GetMapping("/{offerVersionId}")
    public ResponseEntity<Page<OfferActionDTO>> getListOfferVersionAction(@NotNull @Positive @PathVariable Long offerVersionId, Pageable pageable) {
        pageable = validatePageable(pageable);
        return ResponseEntity.ok().body(offerVersionActionService.getListOfferVersionAction(offerVersionId, pageable));
    }

    @PostMapping("/{offerVersionId}")
    public ResponseEntity createOfferVersionAction(@NotNull @Positive @PathVariable Long offerVersionId,
                                                   @Validated(OfferVersionActionDTO.Create.class) @RequestBody List<OfferVersionActionDTO> offerVersionActionDTOs) {
        offerVersionActionService.create(offerVersionActionDTOs, offerVersionId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{offerVersionActionId}")
    public ResponseEntity updateOfferVersionAction(@NotNull @Positive @PathVariable Long offerVersionActionId,
                                                   @Validated(OfferVersionActionDTO.Update.class) @RequestBody OfferVersionActionDTO offerVersionActionDTO) {
        offerVersionActionDTO.setId(offerVersionActionId);
        offerVersionActionService.update(offerVersionActionDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/move")
    public ResponseEntity<Void> move(@RequestBody @Size(min = 2, max = 2) List<OfferVersionActionDTO> offerVersionActionDTOS, @NotNull @RequestParam Long parentId) {
        if (offerVersionActionDTOS.size() != 2) {
            throw new BadRequestAlertException("Bad request", " quantity must be 2.", "");
        }
        offerVersionActionService.moveVersionAction(offerVersionActionDTOS, parentId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/add-child/{offerVersionId}")
    public ResponseEntity<BaseResponseDTO> add(@NotNull @Positive @PathVariable Long offerVersionId,
                                               @Validated(OfferVersionActionDTO.Create.class) @RequestBody OfferVersionActionDTO offerVersionActionDTO,
                                               @RequestParam Integer posIndex) {
        return ResponseEntity.status(HttpStatus.CREATED).body(offerVersionActionService.save(offerVersionActionDTO, offerVersionId, posIndex));
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferVersionActionSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, "posIndex") : sort);
    }
}
