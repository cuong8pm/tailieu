package com.mycompany.myapp.web.rest.offer.offer;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.offer.OfferService;
import com.mycompany.myapp.service.offer.offer.OfferVersionService;
import com.mycompany.myapp.service.offer.template.OfferTemplateService;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.offer.common.OfferCommonResource;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/offers")
public class OfferResource extends OfferCommonResource<OfferDTO, Offer> {
    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferTemplateService offerTemplateService;
    
    @Autowired
    private OfferVersionService offerVersionService;
    
    private final Long MAX_VALUE = 9007199254740991L;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.offerCommonService = offerService;
        this.ocsCloneService = offerService;
        this.childMappingService = null;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.OFFER;
    }

    @GetMapping("/get-data-offer/{offerTemplateId}")
    public ResponseEntity<OfferDTO> getDataOffer(@NotNull @PathVariable Long offerTemplateId) {
        return ResponseEntity.ok().body(offerService.getDataCreate(offerTemplateId));
    }

    @GetMapping("/search-offers")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchInTree(@RequestParam(value = "name", defaultValue = "") String name,
            @RequestParam(required = false) Long offerId, @RequestParam(required = false) Long offerTemplateVersionId,
            @RequestParam(required = false) Boolean isPopup,
            @RequestParam(required = false) Long offerVersionId,
            @RequestParam(value = "tab", defaultValue = "") String tab) {
        List<Long> removeIds = new ArrayList<>();
        List<Long> removeIDs = new ArrayList<>();
        if (StringUtils.equals(tab, "bundleOffer") && offerVersionId != null) {
            removeIDs = offerService.removeId(offerVersionId);
            Long id = offerVersionService.getOffferId(offerVersionId);
            removeIds.addAll(removeIDs);
            removeIds.add(id);
        }
        if (StringUtils.equals(tab, "bundleOffer") && offerTemplateVersionId != null) {
            removeIds = offerTemplateService.removeId(offerTemplateVersionId);
        }
        if (StringUtils.equals(tab, "relationship") && offerTemplateVersionId != null) {
            removeIds = offerTemplateService.findIds(offerTemplateVersionId);
        }
        if (StringUtils.equals(tab, "relationship") && offerId != null) {
            removeIDs = offerService.findIds(offerId);
            removeIds.add(offerId);
            removeIds.addAll(removeIDs);
        }
        List<Tree> list = offerService.searchInTree(name, getCategoryType(), removeIds, isPopup);

        return ResponseEntity.ok().body(list);
    }
    
    @GetMapping("/get-max-external-id")
    public ResponseEntity<Long> getMaxExternalId() {
        if(Long.compare(offerService.getMaxExternalId() + 1L, MAX_VALUE) > 0) {
            return ResponseEntity.ok().body(-1L);
        }
        return ResponseEntity.ok().body(offerService.getMaxExternalId() + 1L);
    }
}
