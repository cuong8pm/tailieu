package com.mycompany.myapp.web.rest.offer.template;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.exception.ResourceNotFoundException;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionBundle;
import com.mycompany.myapp.dto.offer.offer.VersionBunndleDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionBundleDTO;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionBundleService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.offer.common.VersionBundleCommonResource;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-template-version-bundles")
public class OfferTemplateVersionBundleResource extends VersionBundleCommonResource<OfferTemplateVersionBundleDTO, OfferTemplateVersionBundle> {

    @Autowired
    private OfferTemplateVersionBundleService offerTemplateVersionBundleService;

    @Autowired
    private OfferRepository offerRepository;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerTemplateVersionBundleService;
    }

    @GetMapping("/list-bundles/{versionId}")
    public ResponseEntity<List<VersionBunndleDTO>> getAllBundles(@PathVariable @Positive Long versionId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Offer.FieldNames.ID));
        List<VersionBunndleDTO> list = offerTemplateVersionBundleService.findVersionBunndleDTOs(versionId, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @Override
    @PostMapping("/{versionId}")
    public ResponseEntity<BaseResponseDTO> addBundles(@RequestBody List<Long> offerIds, @PathVariable("versionId") Long versionId) {

        List<Offer> offers = offerRepository.findAllByIdIn(offerIds);
        if (offers.size() != offerIds.size()) {
            throw new ResourceNotFoundException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER_VERSION, ErrorKey.Offer.ENTITY_NOT_FOUND);
        }
        return super.addBundles(offerIds, versionId);
    }

    @Override
    @PutMapping("/{id}/{versionId}")
    public ResponseEntity<BaseResponseDTO> updateBundle(@PathVariable("id") Long id ,@PathVariable("versionId") Long versionId ,@RequestParam(value = "offerId") Long childId) {
        Offer offer = offerRepository.getOfferById(childId);
        if (offer == null) {
            throw new ResourceNotFoundException(ErrorMessage.Offer.NOT_FOUND, Resources.OFFER_VERSION, ErrorKey.Offer.ENTITY_NOT_FOUND);
        }
        return super.updateBundle(id, versionId, childId);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferTemplateVersionBundleSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, Offer.FieldNames.ID) : sort);
    }
    @GetMapping("/{versionId}")
    public ResponseEntity<Page<VersionBunndleDTO>> getAllBundle(@PathVariable @Positive Long versionId, Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<VersionBunndleDTO> list = offerTemplateVersionBundleService.findVersionBunndleDTOs(versionId, pageable);
        return ResponseEntity.ok().body(list);
    }

}
