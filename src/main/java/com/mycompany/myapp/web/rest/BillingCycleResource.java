package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.BillingCycle;
import com.mycompany.myapp.domain.BillingCycleType;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.BillingCycleChangeStatusDTO;
import com.mycompany.myapp.dto.BillingCycleTypeDTO;
import com.mycompany.myapp.dto.CalcUnitDTO;
import com.mycompany.myapp.dto.GenerateBillingCycleDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.BillingCycleDTO;
import com.mycompany.myapp.service.BillingCycleService;
import com.mycompany.myapp.service.ReferTableService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/billing-cycle-types")
public class BillingCycleResource {

    @Autowired
    private BillingCycleService billingCycleService;

    @Autowired
    private ReferTableService referTableService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<BillingCycleTypeDTO>> getBillingCycleTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, BillingCycleType.FieldNames.POS_INDEX));

        List<BillingCycleTypeDTO> list = billingCycleService.searchBillingCycleType(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }
    // Get
    @GetMapping("/list-child-billing-cycle-types/{categoryId}")
    public ResponseEntity<Page<BillingCycleTypeDTO>> getBillingCycleTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        pageable = validatePageable(pageable);
        Page<BillingCycleTypeDTO> page = billingCycleService.searchBillingCycleType(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<BillingCycleTypeDTO> getBillingCycleTypeDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        BillingCycleTypeDTO billingCycleTypeDTO = billingCycleService.getBillingCycleTypeDetails(id);
        return ResponseEntity.ok().body(billingCycleTypeDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addBillingCycleType (
        @Validated(BillingCycleTypeDTO.Create.class) @RequestBody BillingCycleTypeDTO billingCycleTypeDTO) {
        billingCycleTypeDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(billingCycleService.createBillingCycleType(billingCycleTypeDTO));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveBillingCycleTypes(
        @Validated(BillingCycleTypeDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<BillingCycleTypeDTO> billingCycles) {
        if (billingCycles.size() != 2) {
            throw new BadRequestAlertException("Bad request", "BillingCycle's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(billingCycleService.move(billingCycles));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteBillingCycleType (@PathVariable(name = "id", required = true) Long id) {
        billingCycleService.deleteBillingCycleType(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateBillingCycleType (
        @Validated(BillingCycleTypeDTO.Update.class) @RequestBody BillingCycleTypeDTO billingCycleTypeDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        billingCycleTypeDTO.setId(id);
        return ResponseEntity.ok().body(billingCycleService.updateBillingCycleType(billingCycleTypeDTO));
    }


    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getBillingCycles(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.ASC, BillingCycleType.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/search-billing-cycle-types")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBillingCycleTypes (
        @RequestParam(value = "name") String name) {
        List<Tree> list = billingCycleService.searchInTree(name.trim(), CategoryType.BILLING_CYCLE);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/units")
    public ResponseEntity<List<ReferTable>> getCalcUnits() {
        return ResponseEntity.ok(referTableService.getReferTables(ReferType.CALC_UNIT.getValue()));
    }

    @GetMapping("/{billingCycleTypeId}/billing-cycles")
    public ResponseEntity<Page<BillingCycleDTO>> getBillingCycles(
            @PathVariable("billingCycleTypeId") Long billingCycleTypeId,
            @RequestParam(value = "beginDate", required = false) String beginDate,
            @RequestParam(value = "endDate", required = false) String endDate,
            Pageable pageable) {
        pageable = validateBillingCyclesPageable(pageable);

        return ResponseEntity.ok().body(billingCycleService.findBillingCycles(billingCycleTypeId, beginDate, endDate, pageable));
    }

    private Pageable validateBillingCyclesPageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getBillingCyclesParams(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.DESC, BillingCycle.FieldNames.END_DATE) : sort);
    }

    @PutMapping("/{billingCycleTypeId}/billing-cycles")
    public ResponseEntity<Void> deleteBillingCycles(@PathVariable("billingCycleTypeId") Long billingCycleTypeId, @RequestBody List<Long> ids) {
        billingCycleService.deleteBillingCycles(ids, billingCycleTypeId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{billingCycleTypeId}/billing-cycles/{id}/change-status")
    public ResponseEntity<Void> updateBillingCyclesStatus(@PathVariable("billingCycleTypeId") Long billingCycleTypeId,
            @PathVariable("id") Long billingCycleId) {
        billingCycleService.changeStatus(billingCycleTypeId, billingCycleId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{billingCycleTypeId}/billing-cycles")
    public ResponseEntity<Void> generateBillingCycles(@PathVariable("billingCycleTypeId") Long billingCycleTypeId,
            @RequestBody @Valid GenerateBillingCycleDTO billingCycleDTO) {
        billingCycleService.generateBillingCycle(billingCycleDTO, billingCycleTypeId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{billingCycleTypeId}/billing-cycles/deleteable")
    public ResponseEntity<Void> checkDeleteBillingCycles(@PathVariable("billingCycleTypeId") Long billingCycleTypeId, @RequestBody List<Long> ids) {
        billingCycleService.checkDeleteBillingCycles(ids, billingCycleTypeId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/dd-billing-cycles-type")
    public ResponseEntity<List<BaseResponseDTO>> getBillingCycleTypeForDropdown(){
        return ResponseEntity.ok(billingCycleService.getForDropdown());
    }
}
