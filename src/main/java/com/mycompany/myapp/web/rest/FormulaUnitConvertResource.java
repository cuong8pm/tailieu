package com.mycompany.myapp.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.formula.FormulaUnitConvert;
import com.mycompany.myapp.service.formula.FormulaUnitConvertService;

@RestController
@RequestMapping("/api/formula-unit-convert")
public class FormulaUnitConvertResource {

    @Autowired
    private FormulaUnitConvertService formulaUnitConvertService;

    @GetMapping("")
    public ResponseEntity<List<FormulaUnitConvert>> getAll() {
        return ResponseEntity.ok().body(formulaUnitConvertService.getAll());
    }
}
