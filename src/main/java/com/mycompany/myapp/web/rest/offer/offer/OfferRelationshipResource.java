package com.mycompany.myapp.web.rest.offer.offer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.offer.OfferRelationship;
import com.mycompany.myapp.dto.offer.offer.OfferRelationshipDTO;
import com.mycompany.myapp.service.offer.offer.OfferRelationshipService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.offer.common.OfferRelationshipCommonResource;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-relationships")
public class OfferRelationshipResource extends OfferRelationshipCommonResource<OfferRelationshipDTO, OfferRelationship> {

    @Autowired
    private OfferRelationshipService offerRelationshipService;
    @PostConstruct
    @Override
    public void setCommonService() {
        this.commonService = offerRelationshipService;
    }
}
