package com.mycompany.myapp.web.rest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.domain.constant.AffectedObjectType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mycompany.myapp.dto.BalancesDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.errors.Resources;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/accumulate-balances")
public class BalancesAcmResource extends BalancesResource {


    // Get
    @GetMapping("/list-child-accumulate-balances/{categoryId}")
    public ResponseEntity<Page<BalancesDTO>> getBalTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);

        Page<BalancesDTO> page = balancesService.getBalTypes(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @Override
    public ResponseEntity<BalancesDTO> cloneBalType(Long balanceId, int deep, @Valid CloneDTO cloneDTO) {
        ResponseEntity<BalancesDTO> result = super.cloneBalType(balanceId, deep, cloneDTO);
        result.getBody().setType(Resources.ACM_BAL_TYPE);
        return result;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteBalType(@PathVariable(name = "id") Long id) {
        balancesService.deleteBalType(id, AffectedObjectType.ACM_BALANCE.getValue());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


}
