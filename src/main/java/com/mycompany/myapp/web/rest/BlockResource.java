package com.mycompany.myapp.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.ObjectField;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.block.Block;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.block.BlockDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.service.block.BlockRateTableMapService;
import com.mycompany.myapp.service.block.BlockService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import io.github.jhipster.web.util.PaginationUtil;
import liquibase.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.util.SetUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Iterator;


@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/blocks")
public class BlockResource extends AbstractResource {
    @Autowired
    private BlockService blockService;
    @Autowired
    private BlockRateTableMapService blockRateTableMapService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = blockService;
        this.childMappingService = blockRateTableMapService;
    }

    @GetMapping("/search-blocks")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBlock(@RequestParam(value = "name", defaultValue = "") String name, @RequestParam(required = false) Long priceComponentId, @RequestParam(required = false) Boolean isPopup) {
        List<Long> removeIds = new ArrayList<>();
        if (priceComponentId != null) {
            removeIds = blockService.getRemoveIds(priceComponentId);
        }
        List<Tree> list = blockService.searchInTree(name.trim(), CategoryType.BLOCK, removeIds, isPopup);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> addBlock(@Validated(BlockDTO.Create.class) @RequestBody BlockDTO blockDTO) {
        blockDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(blockService.createBlock(blockDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateBLock(@Validated(BlockDTO.Update.class) @RequestBody BlockDTO blockDTO,
                                                       @PathVariable Long id) {
        blockDTO.setId(id);
        return ResponseEntity.ok().body(blockService.updateBlock(blockDTO));
    }

    @GetMapping("/block-types")
    public ResponseEntity<List<ReferTable>> getBlockType() {
        return ResponseEntity.ok().body(blockService.getBlockType());
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<BlockDTO>> getByCategoryId(@PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Block.FieldNames.POS_INDEX));
        List<BlockDTO> list = blockService.getBlocks(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-blocks/{categoryId}")
    public ResponseEntity<Page<BlockDTO>> getBlockWithPage(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        pageable = validatePageable(pageable);

        Page<BlockDTO> page = blockService.getBlockWithPage(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = OCSUtils.getBlockSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, Block.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/list-child-sub-blocks/{blockId}")
    public ResponseEntity<List<TreeClone>> getBlockInTree(@NotNull @Positive @PathVariable Long blockId) {
        return ResponseEntity.ok().body(blockService.getBlockInTree(blockId));
    }

    @GetMapping("/list-child-rate-tables/{blockId}")
    public ResponseEntity<List<RateTableDTO>> getRateTableByComponentType(@NotNull @Positive @PathVariable Long blockId, @NotNull @Positive @RequestParam Integer componentType) {
        return ResponseEntity.ok().body(blockService.getRateTableByComponentType(componentType, blockId));
    }

    @GetMapping("/list-child-rating-filters/{id}")
    public ResponseEntity<List<RatingFilterDTO>> getRatingFilter(@NotNull @Positive @PathVariable Long id) {
        return ResponseEntity.ok().body(blockService.getRatingFilter(id));
    }

    @GetMapping("/affected-object-types")
    public ResponseEntity<List<ReferTable>> getAffectedObjectType() {
        return ResponseEntity.ok().body(blockService.getAffectedObjectType());
    }

    @GetMapping("/affected-bal-modes")
    public ResponseEntity<List<ReferTable>> getBlockMode() {
        return ResponseEntity.ok().body(blockService.getBlockMode());
    }

    @GetMapping("/{id}")
    public ResponseEntity<BlockDTO> getBlockDetail(@PathVariable Long id) {
        return ResponseEntity.ok().body(blockService.getBlockDetail(id));
    }

    @DeleteMapping("/{blockId}")
    public ResponseEntity<BaseResponseDTO> deleteBlock(@PathVariable Long blockId) {
        blockService.deleteBlock(blockId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/affected-object-field-name/{id}")
    public ResponseEntity<List<ObjectField>> getAffectedObjectFieldName(@NotNull @PathVariable Long id) {
        return ResponseEntity.ok().body(blockService.getAffectedObjectFieldName(id));
    }

    @PutMapping("/{blockId}/move-child-rate-table")
    public ResponseEntity<Void> moveChild(@PathVariable("blockId") @Positive Long blockId, @RequestBody List<RateTableDTO> moveables, @NotNull @Positive @RequestParam Integer componentType) {
        blockService.moveChild(moveables, blockId, componentType);
        return ResponseEntity.noContent().build();
    }

    @JsonView(Tree.class)
    @GetMapping("/{id}/dependencies")
    @Override
    public ResponseEntity<Map<String, Set<CategoryDTO>>> checkDependencies(@PathVariable Long id) {
        ResponseEntity<Map<String, Set<CategoryDTO>>> result = super.checkDependencies(id);
        Set<CategoryDTO> dependOn = result.getBody().get("dependOn");
        if(!SetUtils.isEmpty(dependOn)) {
            for (CategoryDTO categoryDTO : dependOn) {
                if (categoryDTO.getName().equals("Rating Filters")) {
                    categoryDTO.setName("Rating Filter");
                }
            }
        }
        return result;

    }



}
