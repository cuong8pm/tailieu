package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.StatisticItem;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.service.StatisticItemService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@IgnoreWildCard
@RequestMapping("/api/statistics")
public class StatisticItemResource {

    private final Logger log = LoggerFactory.getLogger(StatisticItemResource.class);

    @Autowired
    private StatisticItemService statisticItemService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<StatisticItemDTO>> getStatisticItems(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, StatisticItem.FieldNames.POS_INDEX));

        List<StatisticItemDTO> list = statisticItemService.getStatistics(categoryId, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-statistics/{categoryId}")
    public ResponseEntity<Page<StatisticItemDTO>> getStatisticItems(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
            @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        Page<StatisticItemDTO> page = statisticItemService.getStatistics(categoryId, name, description, id, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StatisticItemDTO> getStatisticItemDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        StatisticItemDTO statisticItemDTO = statisticItemService.getStatisticItemDetail(id);
        return ResponseEntity.ok().body(statisticItemDTO);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addStatistic(
        @Validated(StatisticItemDTO.Create.class) @RequestBody StatisticItemDTO statisticDTO) {
        statisticDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(statisticItemService.createStatistic(statisticDTO));
    }

    @PutMapping("/move")
    public ResponseEntity<Object> moveStatistic(@Validated(StatisticItemDTO.Move.class) @RequestBody @Size(min = 2,
            max = 2) List<StatisticItemDTO> statisticItemDTOS) {
        if (statisticItemDTOS.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Statistic's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(statisticItemService.move(statisticItemDTOS));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteStatistic(@PathVariable(name = "id") Long id) {
        statisticItemService.deleteStatistic(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateStatistic(
            @Validated(StatisticItemDTO.Update.class) @RequestBody StatisticItemDTO statisticItemDTO,
            @PathVariable(name = "id") @NotNull @Positive Long id) {
        statisticItemDTO.setId(id);
        return ResponseEntity.ok().body(statisticItemService.updateStatistic(statisticItemDTO));
    }

    @GetMapping("/search-statistics")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getStatistic(@RequestParam(value = "name") String name) {
        List<Tree> list = statisticItemService.searchInTree(name, CategoryType.STATISTIC);
        return ResponseEntity.ok().body(list);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getStatisticSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC,StatisticItem.FieldNames.POS_INDEX) : sort);
    }

}
