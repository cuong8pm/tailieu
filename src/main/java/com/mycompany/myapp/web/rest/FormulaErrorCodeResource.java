package com.mycompany.myapp.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.formula.FormulaErrorCode;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.formula.FormulaErrorCodeDTO;
import com.mycompany.myapp.service.formula.FormulaErrorCodeService;

@RestController
@RequestMapping("/api/formula-error-code")
public class FormulaErrorCodeResource {

    @Autowired
    private FormulaErrorCodeService formulaErrorCodeService;
    
    @GetMapping("")
    public ResponseEntity<List<FormulaErrorCode>> getAll() {
        return ResponseEntity.ok().body(formulaErrorCodeService.getAll());
    }
    
    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> createNewFormulaErrorCode(@RequestBody @Validated FormulaErrorCodeDTO dto) {
        return ResponseEntity.ok().body(formulaErrorCodeService.createNewFormulaErrorCode(dto));
    }
}
