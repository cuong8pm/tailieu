package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Filter;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.service.eventProcessing.FilterService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.*;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("api/filters")
public class FilterResource extends AbstractResource {
    @Autowired
    private FilterService filterService;

    @Autowired
    private ExpressionResource expressionResource;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = filterService;
        this.ocsCloneService = filterService;
    }

    @PostMapping("")
    public ResponseEntity<FilterDTO> createFilter(@Validated(FilterDTO.Create.class) @RequestBody @NotNull FilterDTO filterDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(filterService.createFilter(filterDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<FilterDTO> updateFilter(
        @PathVariable(value = "id") @NotNull @Positive Long filterId,
        @Validated(FilterDTO.Create.class) @RequestBody @NotNull FilterDTO filterDTO
    ) {
        filterDTO.setId(filterId);
        return ResponseEntity.status(HttpStatus.CREATED).body(filterService.updateFilter(filterDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFilter(@PathVariable(value = "id") @NotNull @Positive Long filterId) {
        filterService.deleteFilter(Arrays.asList(filterId));
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<FilterDTO> getDetailFilter(@PathVariable(value = "id") @NotNull @Positive Long filterId) {
        return ResponseEntity.ok().body(filterService.getDetailFilter(filterId));
    }

    @GetMapping("/search-filters")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchFilter(@RequestParam(value = "name", defaultValue = "") String name) {
        return ResponseEntity.ok().body(filterService.searchInTree(name.trim(), CategoryType.FILTER));
    }

    @GetMapping("list-child/{categoryId}")
    public ResponseEntity<List<FilterDTO>> getListObjectByCategory(@PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<FilterDTO> dtos = filterService.getListFiltersByCategoryId(categoryId, unpaged);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child-filters/{categoryId}")
    public ResponseEntity<Page<FilterDTO>> getListObjectByCategoryPaging(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<FilterDTO> page = filterService.getFiltersWithPaging(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}/expressions")
    public ResponseEntity<Page<ExpressionConditionDTO>> getListExpressionByFilterId(
        @PathVariable(value = "id") @NotNull @Positive Long filterId,
        Pageable pageable
    ) {
        return expressionResource.getListExpressionByFilterId(filterId, pageable);
    }

    @PostMapping("/{id}/expressions")
    public ResponseEntity<ExpressionDTO> createExpression(
        @PathVariable(value = "id") @NotNull @Positive Long filterId,
        @RequestBody @Validated @NotNull(groups = {ExpressionDTO.Create.class}) ExpressionDTO expressionDTO
    ) {
        return expressionResource.createExpression(filterId, expressionDTO);
    }

    @GetMapping("/{id}/expressions/{expressionId}")
    public ResponseEntity<ExpressionDTO> getDetailExpression(
        @PathVariable(value = "id") @NotNull @Positive Long filterId,
        @PathVariable(value = "expressionId") Long expressionId
    ) {
        return expressionResource.getDetailExpression(filterId, expressionId);
    }

    @DeleteMapping("/{id}/expressions/{expressionId}")
    public ResponseEntity<?> deleteExpression(
        @PathVariable(value = "id") @NotNull @Positive Long filterId,
        @PathVariable(value = "expressionId") @NotNull @Positive Long expressionId
    ) {
        return expressionResource.deleteExpression(filterId, expressionId);
    }

    @PutMapping("/{id}/expressions/{expressionId}")
    public ResponseEntity<?> updateExpression(
        @PathVariable(value = "id") @NotNull @Positive Long filterId,
        @PathVariable(value = "expressionId") @NotNull @Positive Long expressionId,
        @RequestBody @Validated @NotNull(groups = {ExpressionDTO.Create.class}) ExpressionDTO expressionDTO
    ) {
        return expressionResource.updateExpression(filterId, expressionId, expressionDTO);
    }

    @GetMapping("/dropdown/modules")
    public ResponseEntity<List<ReferTable>> getDropdownModuleName() {
        return ResponseEntity.ok(filterService.getDropdownModule());
    }

    @GetMapping("/dropdown/input-types")
    public ResponseEntity<List<ReferTable>> getDropdownInputType() {
        return ResponseEntity.ok(filterService.getDropdownInputType());
    }

    @GetMapping("/dropdown/object-sources")
    public ResponseEntity<List<DropdownDTO>> getDropdownObjectSource(@RequestParam Integer type) {

        return ResponseEntity.ok(filterService.getDropdownObjectSource(type));
    }

    @GetMapping("/event-objects/{eventObjectId}/event-fields")
    public ResponseEntity<List<EventObjectFieldDTO>> getTreeEventField(
        @PathVariable(value = "eventObjectId") @NotNull @Positive Long eventObjectId,
        @RequestParam(required = false) String name
    ) {
        return ResponseEntity.ok(filterService.getListEventFields(eventObjectId, name));
    }

    @GetMapping("/expressions/{expressionId}/functions/{functionId}")
    public ResponseEntity<FunctionDTO> getDetailFunction(
        @PathVariable(value = "expressionId") @NotNull @Positive Long expressionId,
        @PathVariable(value = "functionId") @NotNull @Positive Long functionId
    ) {
        return expressionResource.getDetailFunction(expressionId, functionId);
    }

    @GetMapping("/dropdown/expression-value-types")
    public ResponseEntity<List<ReferTable>> getDropdownExpressionValueType() {
        return ResponseEntity.ok(filterService.getDropdownExpressionValueType());
    }

    @GetMapping("/dropdown/parameter-value-types")
    public ResponseEntity<List<ReferTable>> getDropdownParameterValue() {
        return ResponseEntity.ok(filterService.getDropdownParameterValue());
    }

    @GetMapping("/dropdown/param-instance-values/{paramId}")
    public ResponseEntity<List<Map<String, Object>>> getDropdownParamInstanceValue(
        @PathVariable(value = "paramId") @NotNull @Positive Long paramId
    ) {
        return ResponseEntity.ok(filterService.getDropdownParamInstanceValue(paramId));
    }

    @GetMapping("/dropdown/operators")
    public ResponseEntity<List<ReferTable>> getDropdownOperator(@RequestParam(value = "type") @NotNull @PositiveOrZero Integer type) {
        return ResponseEntity.ok(filterService.getDropdownOperator(type));
    }

    @GetMapping("/list-child-conditions/{filterId}")
    public ResponseEntity<List<ConditionDTO>> getDetailPopupEnterParameterParam(
        @PathVariable(value = "filterId") @NotNull @Positive Long filterId
    ) {
        return ResponseEntity.ok(filterService.getListConditionByFilterId(filterId));
    }

    @GetMapping("/{id}/profiles")
    public ResponseEntity<Map<String, Object>> getListProfiles(@PathVariable(value = "id") @NotNull @PositiveOrZero Long filterId) {
        return ResponseEntity.ok(filterService.getListProfile(filterId));
    }

    @GetMapping("/{id}/popup-profiles")
    public ResponseEntity<Map<String, Object>> getListPopupProfiles(@PathVariable @NotNull @PositiveOrZero Long id,
                                                                    @RequestParam(required = false) String name) {
        return ResponseEntity.ok(filterService.getListPopupProfile(id, name));
    }

    @GetMapping("/{id}/popup-fields")
    public ResponseEntity<List<FieldDTO>> getListFields(@PathVariable @NotNull @PositiveOrZero Long id,
                                                        @RequestParam(required = false) String name) {
        return ResponseEntity.ok(filterService.getListFields(id, name));
    }

    @PostMapping("/{filterId}/filter-fields")
    public ResponseEntity<BaseResponseDTO> addField(@PathVariable @NotNull @Positive Long filterId,
                                                    @RequestBody @Valid FilterFieldDTO dto) {
        dto.setFilterId(filterId);
        filterService.addField(dto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{filterId}/filter-fields")
    public ResponseEntity<Page<FilterFieldDTO>> getListFilterFields(@PathVariable @NotNull @Positive Long filterId,
                                                                    @RequestParam(required = false) String name, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<FilterFieldDTO> page = filterService.getListFilterFields(filterId, name, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @DeleteMapping("/filter-fields/{id}")
    public ResponseEntity<BaseResponseDTO> deleteFilterField(@PathVariable @NotNull @Positive Long id) {
        filterService.deleteFilterField(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/filter-fields/{id}")
    public ResponseEntity<BaseResponseDTO> updateFilterField(@PathVariable @NotNull @Positive Long id,
                                                             @RequestBody @Valid FilterFieldDTO dto) {
        filterService.updateFilterField(id, dto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}/functions/{functionId}/function-params")
    public ResponseEntity<List<FunctionParamDTO>> getListFunctionParam(@PathVariable @NotNull Long id,
                                                                           @PathVariable @NotNull @Positive Long functionId,
                                                                           @RequestParam(value = "ownerType") Integer ownerType) {
        return ResponseEntity.ok(filterService.getListFunctionParam(id, functionId, ownerType));
    }

    @GetMapping("/filter-fields/{id}/value-detail")
    public ResponseEntity<FunctionDTO> getValueDetail(@PathVariable @NotNull @Positive Long id) {
        return ResponseEntity.ok(filterService.getValueDetail(id));
    }

    @PutMapping("/{id}/change-profiles")
    public ResponseEntity<BaseResponseDTO> updateProfiles(@PathVariable @NotNull @Positive Long id,
                                                          @RequestBody @Valid List<ReferTableDTO> dtos) {
        filterService.updateProfiles(id, dtos);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public String getSortFieldName() {
        return Filter.FieldNames.POS_INDEX;
    }

    @GetMapping("/{id}/list-expressions")
    public ResponseEntity<List<ExpressionConditionDTO>> getListExpressionNotPaging(@PathVariable @NotNull @Positive Long id) {
        return expressionResource.getListExpressionByFilterIdNoPaging(id);
    }

    @GetMapping("/{id}/list-outputs")
    public ResponseEntity<List<FilterFieldDTO>> getListFilterFieldNotPaging(@PathVariable(value = "id") @NotNull @Positive Long filterId,
                                                                            @RequestParam(required = false) String name) {
        Pageable unpage = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<FilterFieldDTO> page = filterService.getListFilterFields(filterId, name, unpage).getContent();
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/expressions/{id}")
    public ResponseEntity<ExpressionDTO> getExpressionFunctionDetail(@PathVariable @NotNull @Positive Long id,
                                                                     @RequestParam(value = "type") Integer type) {
        return expressionResource.getExpressionFunctionDetail(id, type);
    }

    @PostMapping("/search-by-expression")
    public ResponseEntity<List<FilterDTO>> searchFilterByExpression(@RequestBody @Validated(ExpressionDTO.Search.class) List<ExpressionDTO> dtos) {
        return ResponseEntity.ok(filterService.searchFilterByExpression(dtos));
    }

    public ResponseEntity<Page<FilterDTO>> getListFilterByEventPolicyIdPaging(Long eventPolicyId, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<FilterDTO> page = filterService.getFiltersByEventPolicyId(eventPolicyId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getFilterSearchParam(order.getProperty());
    }

    @GetMapping("/{filterId}/list-fields")
    public ResponseEntity<List<FilterFieldDTO>> getListFilterFields(@PathVariable @NotNull @Positive Long filterId,
                                                                    @RequestParam(required = false) String name) {
        Pageable unpage = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<FilterFieldDTO> list = filterService.getListFilterFields(filterId, name, unpage).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/filter-fields/{fieldId}/functions/{functionId}/function-params")
    public ResponseEntity<FunctionDTO> getListAllFunctionParams(@PathVariable(name = "fieldId") @NotNull @PositiveOrZero Long fieldId,
                                                                @PathVariable(name = "functionId") @NotNull @Positive Long functionId) {
        return ResponseEntity.ok(filterService.getListAllFunctionParam(fieldId,functionId));
    }

    @PostMapping("/check-double")
    public ResponseEntity<Map<String, Boolean>> checkDouble(@RequestBody List<String> listDouble) {

        Map<String, Boolean> map = new HashMap<String, Boolean>();
        BigDecimal maxDouble = BigDecimal.valueOf(Double.MAX_VALUE);
        for (String item : listDouble
        ) {
            BigDecimal itemDouble = new BigDecimal(item);
            if (itemDouble.compareTo(maxDouble) > 0 || itemDouble.compareTo(BigDecimal.valueOf(0)) < 0) {
                map.put("value", false);
                return ResponseEntity.ok(map);
            }
        }
        map.put("value", true);
        return ResponseEntity.ok(map);
    }
}
