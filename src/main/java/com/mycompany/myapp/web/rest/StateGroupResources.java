package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.StateGroups;
import com.mycompany.myapp.domain.StateType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.StateGroupsDTO;
import com.mycompany.myapp.dto.StateTypeDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.StateGroupsService;
import com.mycompany.myapp.service.StateTypeService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/state-groups")
public class StateGroupResources {
    
    @Autowired
    private StateGroupsService stateGroupsService;
    @Autowired
    private StateTypeService stateTypeService;
    
    @GetMapping("/list-child/{parentId}")
    public ResponseEntity<List<StateGroupsDTO>> getListStateGroupsByCategoryId(@PathVariable Long parentId) {
        return ResponseEntity.ok().body(stateGroupsService.getListStateGroupsByCategoryId(parentId));
    }
    

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> createStateGroup(
            @RequestBody @Validated(StateGroupsDTO.Create.class) StateGroupsDTO stateGroupsDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(stateGroupsService.createStateGroups(stateGroupsDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateStateGroup(@PathVariable(name = "id", required = true) Long id,
            @RequestBody @Validated(StateGroupsDTO.Update.class) StateGroupsDTO stateGroupsDTO) {
        stateGroupsDTO.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(stateGroupsService.updateStateGroups(stateGroupsDTO));
    }

    @PutMapping("/move")
    public ResponseEntity<Object> moveStateGroup(
        @Validated(StateGroupsDTO.Move.class) @RequestBody @NotNull @Size(min = 2, max = 2) List<StateGroupsDTO> stateGroupsDTOs) {
        return ResponseEntity.ok().body(stateGroupsService.move(stateGroupsDTOs));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StateGroupsDTO> getStateGroupDetail(@PathVariable("id") @NotNull @Positive Long id) {
        StateGroupsDTO stateGroupsDTO = stateGroupsService.getStateGroupsDetail(id);
        return ResponseEntity.ok().body(stateGroupsDTO);
    }

    @GetMapping("/list-child-state-groups/{parentId}")
    public ResponseEntity<Page<StateGroupsDTO>> getStateGroups(@PathVariable(value = "parentId") @NotNull @Positive Long parentId,
                                                            @RequestParam(value = "name", required = false) String name,
                                                            @RequestParam(value = "description", required = false) String description,
                                                            Pageable pageable) {
        pageable = validatePageable(pageable, Resources.STATEGROUPS);
        Page<StateGroupsDTO> page = stateGroupsService.getStateGroups(parentId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteStateGroup(@PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        stateGroupsService.deleteStateGroups(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-all-state-groups")
    public ResponseEntity<List<StateGroups>> getAllStateGroups() {
        return ResponseEntity.ok().body(stateGroupsService.getAllStateGroups());
    }
    
    @GetMapping("/search-state-sets")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getUnitTypes(@RequestParam(value = "name", defaultValue = "") String name) {
        List<Tree> list = stateTypeService.searchInTree(name.trim(), CategoryType.STATE_TYPE);
        list.addAll(stateGroupsService.searchInTree(name.trim(), CategoryType.STATE_GROUP));
        return ResponseEntity.ok().body(list);
    }
    
    private Pageable validatePageable(Pageable pageable, String type) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getStateTypeSearchParam(order.getProperty());
            if (type.equals(Resources.STATEGROUPS)) {
                propery = OCSUtils.getStateGroupSearchParam(order.getProperty());
            }
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.ASC, StateType.FieldNames.POS_INDEX) : sort);
    }

}
