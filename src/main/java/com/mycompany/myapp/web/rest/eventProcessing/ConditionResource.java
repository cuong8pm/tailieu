package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.ConditionBuilderMap;
import com.mycompany.myapp.domain.eventProcessing.ConditionVerificationMap;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.mapper.eventProcessing.BuilderMapper;
import com.mycompany.myapp.service.eventProcessing.*;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/conditions")
public class ConditionResource extends AbstractResource {
    @Autowired
    private ConditionService conditionService;

    @Autowired
    private BuilderService builderService;

    @Autowired
    private ConditionVerificationMapService conditionVerificationMapService;

    @Autowired
    private ConditionBuilderMapService conditionBuilderMapService;

    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private VerificationService verificationService;

    @Autowired
    private BuilderMapper builderMapper;

    @Autowired
    private BuilderTypeService builderTypeService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = conditionService;
        this.ocsCloneService = conditionService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ConditionDTO> getCondition(@PathVariable(name = "id") @NotNull @Positive Long id) {
        ConditionDTO conditionDTO = conditionService.getConditionDetail(id);
        return ResponseEntity.ok().body(conditionDTO);
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> addCondition(@Validated(ConditionDTO.Create.class) @RequestBody ConditionDTO conditionDTO) {
        conditionDTO.setId(null);
        BaseResponseDTO baseResponseDTO = conditionService.createCondition(conditionDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(baseResponseDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ConditionDTO> updateCondition(
        @Validated(ConditionDTO.Update.class) @RequestBody ConditionDTO conditionDTO,
        @PathVariable(name = "id") Long id
    ) {
        conditionDTO.setId(id);
        ConditionDTO conditionDTORes = conditionService.updateCondition(conditionDTO);
        return ResponseEntity.ok().body(conditionDTORes);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteCondition(@PathVariable(name = "id") Long id) {
        conditionService.deleteCondition(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-child-conditions/{categoryId}")
    public ResponseEntity<Page<ConditionDTO>> getConditions(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<ConditionDTO> dtos = conditionService.getConditions(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<ConditionDTO>> getConditions(@PathVariable Long categoryId) {
        Pageable uppaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Builder.FieldNames.POS_INDEX));
        List<ConditionDTO> dtos = conditionService.getConditions(categoryId, null, null, uppaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/search-conditions")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getConditionTree(@RequestParam(required = false, defaultValue = "") String name) {
        List<Tree> list = conditionService.searchInTree(name.trim(), CategoryType.CONDITION);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping("/{conditionId}/verifications")
    public ResponseEntity<List<ConditionVerificationMapDTO>> createConditionVerificationMap(
        @PathVariable(name = "conditionId") Long conditionId,
        @RequestBody List<VerificationDTO> verificationDTOList
    ) {
        List<ConditionVerificationMapDTO> dto = conditionVerificationMapService.createConditionVerificationMap(
            verificationDTOList,
            conditionId
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @DeleteMapping("/{conditionId}/verifications/{verificationId}")
    public ResponseEntity<BaseResponseDTO> deleteConditionVerificationMap(
        @PathVariable(name = "conditionId") Long conditionId,
        @PathVariable(name = "verificationId") Long verificationId
    ) {
        conditionVerificationMapService.deleteByConditionAndVerification(conditionId, verificationId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/{conditionId}/builders-clone")
    public ResponseEntity<List<BuilderDTO>> createConditionBuilderMapByBuilderClone(
        @PathVariable(name = "conditionId") Long conditionId,
        @RequestBody List<CloneDTO> cloneDTO
    ) {
        List<CloneDTO> cloneDTOs = new ArrayList<>();
        cloneDTOs.addAll(cloneDTO);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<OCSCloneableEntity> results = builderService.cloneEntities(cloneDTOs, oldNewChildMap);
        List<BuilderDTO> dtos = new ArrayList<>();
        List<Long> builderIds = results.stream().map(OCSCloneableEntity::getId).collect(Collectors.toList());
        Map<Long, String> builderTypeMap = builderTypeService.getListBuilderTypesByBuilderIds(builderIds);
        for (OCSCloneableEntity item : results) {
            BuilderDTO builderDTO = builderMapper.toDto((Builder) item);
            if (conditionId != 0) {
                conditionBuilderMapService.createConditionBuilderMapByAdd(builderDTO, conditionId);
            }
            builderDTO.setBuilderTypeName(builderTypeMap.get(builderDTO.getBuilderTypeId()));
            dtos.add(builderDTO);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(dtos);
    }

    @PostMapping("/{conditionId}/builders")
    public ResponseEntity<ConditionBuilderMapDTO> createConditionBuilderMap(
        @PathVariable(name = "conditionId") Long conditionId,
        @RequestBody BuilderDTO builderDTO
    ) {
        ConditionBuilderMapDTO dto = conditionBuilderMapService.createConditionBuilderMapByAdd(builderDTO, conditionId);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @DeleteMapping("/builders/{builderId}")
    public ResponseEntity<BaseResponseDTO> deleteConditionBuilderMap(@PathVariable(name = "builderId") Long builderId) {
        conditionService.deleteBuilders(builderId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}/expressions-condition")
    public ResponseEntity<Page<ExpressionConditionDTO>> getListExpressionByConditionId(
            @PathVariable @NotNull @Positive Long id, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<ExpressionConditionDTO> page = expressionService.getListExpressionByConditionId(id, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }


    @PostMapping("/{id}/expressions-condition")
    public ResponseEntity<BaseResponseDTO> addExpressions(@PathVariable @NotNull @Positive Long id,
            @RequestBody ExpressionDTO dto) {

        conditionService.addExpressions(id, dto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/expressions-condition/{id}")
    public ResponseEntity<BaseResponseDTO> deleteExpressions(@PathVariable @NotNull @Positive Long id) {
        conditionService.deleteExpressions(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/expressions-condition/{id}")
    public ResponseEntity<BaseResponseDTO> updateExpressions(@PathVariable @NotNull @Positive Long id,
            @RequestBody ExpressionDTO dto) {
        conditionService.updateExpressionCondition(id, dto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}/verifications-condition")
    public ResponseEntity<Page<VerificationDTO>> getListVerificationByConditionId(
        @PathVariable @NotNull @Positive Long id, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<VerificationDTO> page = verificationService.getListVerificationByConditionId(id, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}/builders-condition")
    public ResponseEntity<Page<BuilderDTO>> getListBuilderByConditionId(
            @PathVariable @NotNull @Positive Long id, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<BuilderDTO> page = builderService.getListBuilderByConditionId(id, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}/builders-condition-clone")
    public ResponseEntity<List<BuilderDTO>> getListBuilderCloneByConditionId(
        @PathVariable @NotNull @Positive Long id) {
        List<BuilderDTO> list = builderService.getListBuilderCloneByConditionId(id);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/expressions-condition/{id}/value-detail")
    public ResponseEntity<FunctionDTO> getValueDetail(@PathVariable @NotNull @Positive Long id) {
        return ResponseEntity.ok(conditionService.getValueDetail(id));
    }

    @GetMapping("/{id}/list-child-builders")
    public ResponseEntity<List<BuilderDTO>> getlistChildBuilder(@PathVariable @NotNull @Positive Long id) {
        return ResponseEntity.ok(conditionService.getlistChildBuilder(id));
    }

    @PostMapping("/search-by-expression")
    public ResponseEntity<List<ConditionDTO>> searchConditionByExpression(
            @RequestBody @Validated(ExpressionDTO.Search.class) List<ExpressionDTO> dtos) {
        return ResponseEntity.ok(conditionService.searchConditionByExpression(dtos));
    }


    public ResponseEntity<Page<ConditionDTO>> getListConditionByEventPolicyIdPaging(Long eventPolicyId,Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<ConditionDTO> page = conditionService.getConditionByEventPolicyId(eventPolicyId,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getConditionSearchParam(order.getProperty());
    }

    @GetMapping("/expressions/{id}")
    public ResponseEntity<ExpressionDTO> getExpressionFunctionDetail(@PathVariable @NotNull @Positive Long id,
                                                                     @RequestParam(value = "type") Integer type) {
        return ResponseEntity.ok(expressionService.getExpressionFunctionDetail(id, type));
    }

    @GetMapping("/{id}/search-verifications")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getVerification(@PathVariable(value = "id") Long conditionId,
                                                      @RequestParam(required = false,defaultValue = "") String name) {
        List<Long> removeIds = new ArrayList<>();

        List<ConditionVerificationMap> conditionVerificationMaps = conditionVerificationMapService.getVerificationByConditionId(conditionId);

        removeIds =  conditionVerificationMaps.stream().map(x -> x.getVerificationId()).collect(Collectors.toList());

        List<Tree> list = verificationService.searchInTree(name.trim(), CategoryType.VERIFICATION, removeIds, true);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/{id}/search-builders")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBuilders(@PathVariable(value = "id") Long conditionId,
                                                      @RequestParam(required = false,defaultValue = "") String name,
                                                  @RequestParam(required = false,value = "type") Long type) {
        List<Long> removeIds = new ArrayList<>();

        List<ConditionBuilderMap> conditionBuilderMaps = conditionBuilderMapService.getBuilderByConditionId(conditionId);

        removeIds =  conditionBuilderMaps.stream().map(x -> x.getBuilderId()).collect(Collectors.toList());
        List<Tree> list = new ArrayList<Tree>();
        if(type == null){
             list = builderService.searchInTree(name.trim(), CategoryType.BUILDER, removeIds, true);
        }else {
             list = builderService.searchInTree(name.trim(), CategoryType.BUILDER, removeIds, true, type);
        }

        return ResponseEntity.ok().body(list);
    }
}
