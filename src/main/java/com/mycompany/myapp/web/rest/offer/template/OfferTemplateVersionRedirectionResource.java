package com.mycompany.myapp.web.rest.offer.template;

import javax.annotation.PostConstruct;

import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRedirection;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionRedirectionDTO;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionRedirectionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.offer.common.VersionRedirectionCommonResource;

import java.util.List;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-template-version-redirection")
public class OfferTemplateVersionRedirectionResource extends VersionRedirectionCommonResource<OfferTemplateVersionRedirectionDTO, OfferTemplateVersionRedirection>{

    @Autowired
    private OfferTemplateVersionRedirectionService offerTemplateVersionRedirectionService;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerTemplateVersionRedirectionService;

    }

    @GetMapping("/list-child-redirections-all/{offerTemplateVersionId}")
    public ResponseEntity<List<OfferOfVersionRedirectionDTO>> getOfferVersionRedirectionsAll(@PathVariable Long offerTemplateVersionId){
        return ResponseEntity.ok().body(offerTemplateVersionRedirectionService.getOfferTemplateVersionRedirection(offerTemplateVersionId));
    }

}
