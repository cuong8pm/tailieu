package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.common.MoveableDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.service.OCSBaseService;
import com.mycompany.myapp.service.clone.OCSCloneMappingService;
import com.mycompany.myapp.service.clone.OCSCloneService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.*;

@RestController
@RequestMapping("/api")
@Validated
public abstract class AbstractResource<T extends CloneDTO> {
    protected OCSCloneService ocsCloneService;
    protected OCSCloneMappingService childMappingService;
    protected OCSBaseService ocsBaseService;

    protected abstract void setBaseService();

    @PutMapping("/{id}/move-category")
    public ResponseEntity<Void> moveCategory(@PathVariable(value = "id") Long id,
                                             @Positive @RequestParam("categoryId") Long categoryId) {
        ocsCloneService.moveCategory(id, categoryId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/move-child")
    public ResponseEntity<Void> moveChild(@PathVariable("id") @Positive Long ratingFilterId, @RequestBody List<MoveableDTO> moveables) {
        childMappingService.move(ratingFilterId, moveables);
        return ResponseEntity.noContent().build();
    }

    @JsonView(Tree.class)
    @GetMapping("/{id}/cloneable")
    public ResponseEntity<? extends Tree> checkCloneable(@PathVariable Long id) {
        List<Long> ids = new ArrayList();
        ids.add(id);
        List<TreeClone> result = new ArrayList(ocsCloneService.getCloneableTree(ids).values());
        return ResponseEntity.ok(result.get(0));
    }

    @PostMapping("/{id}/clone")
    public ResponseEntity<OCSCloneableEntity> cloneRatingFilter(@PathVariable("id") @Positive Long id,
                                                                @RequestBody @Valid T cloneDTO) {
        cloneDTO.setId(id);
        List<CloneDTO> cloneDTOs = new ArrayList<>();
        cloneDTOs.add(cloneDTO);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<OCSCloneableEntity> results = ocsCloneService.cloneEntities(cloneDTOs, oldNewChildMap);
        ocsCloneService.clearCache();
        return ResponseEntity.status(HttpStatus.CREATED).body(results.get(0));
    }

    @JsonView(Tree.class)
    @GetMapping("/{id}/dependencies")
    public ResponseEntity<Map<String, Set<CategoryDTO>>> checkDependencies(@PathVariable Long id) {
        return ResponseEntity.ok(ocsCloneService.checkDependencies(id));
    }

    @PutMapping("/move")
    public ResponseEntity<Object> move(
        @Validated(RatingFilterDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<MoveableDTO> ratingFilterDTO) {
        if (ratingFilterDTO.size() != 2) {
            throw new BadRequestAlertException("Bad request", " quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(ocsCloneService.move(ratingFilterDTO));
    }

    public String getSortProperties(){
        return "posIndex";
    }

    public Pageable pageableValidation(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = getSearchParam(order);
            if (liquibase.util.StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, getSortFieldName()) : sort);
    }

    public String getSortFieldName() {
        return "id";
    }

    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getRatingFilterSearchParam(order.getProperty());
    }

}
