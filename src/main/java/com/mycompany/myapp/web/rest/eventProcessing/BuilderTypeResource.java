package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeDTO;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeItemDTO;
import com.mycompany.myapp.dto.eventProcessing.RouterDTO;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeItemService;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeRouterMapService;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeService;
import com.mycompany.myapp.service.eventProcessing.RouterService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("api/builder-types")
public class BuilderTypeResource extends AbstractResource {
    @Autowired
    private BuilderTypeService builderTypeService;

    @Autowired
    private BuilderTypeItemService builderTypeItemService;

    @Autowired
    private BuilderTypeItemResource builderTypeItemResource;

    @Autowired
    private RouterService routerService;

    @Autowired
    private BuilderTypeRouterMapService builderTypeRouterMapService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = builderTypeService;
        this.ocsCloneService = builderTypeService;
    }

    @PostMapping("")
    public ResponseEntity<BuilderTypeDTO> createBuilderType(
        @Validated(BuilderTypeDTO.Create.class) @RequestBody @NotNull BuilderTypeDTO builderTypeDTO
    ) {
        return ResponseEntity.status(HttpStatus.CREATED).body(builderTypeService.createBuilderType(builderTypeDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BuilderTypeDTO> updateBuilderType(
        @PathVariable(value = "id") @NotNull @Positive Long builderTypeId,
        @Validated(BuilderTypeDTO.Update.class) @RequestBody @NotNull BuilderTypeDTO builderTypeDTO
    ) {
        builderTypeDTO.setId(builderTypeId);
        return ResponseEntity.status(HttpStatus.CREATED).body(builderTypeService.updateBuilderType(builderTypeDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBuilderType(@PathVariable(value = "id") @NotNull @Positive Long builderTypeId) {
        builderTypeService.deleteBuilderType(builderTypeId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BuilderTypeDTO> getDetailBuilderType(@PathVariable(value = "id") @NotNull @Positive Long builderTypeId) {
        return ResponseEntity.ok().body(builderTypeService.getDetailBuilderTypeById(builderTypeId));
    }

    @GetMapping("/search-builder-types")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchBuilderTypes(@RequestParam(value = "name", defaultValue = "") String name) {
        return ResponseEntity.ok().body(builderTypeService.searchInTree(name.trim(), CategoryType.BUILDER_TYPE));
    }

    //builder-type-items
    @GetMapping("/{id}/builder-type-items")
    public ResponseEntity<Page<BuilderTypeItemDTO>> getBuilderTypeItemsByBuilderTypeId(
        @RequestParam(value = "name", required = false) String name,
        @PathVariable(value = "id") @NotNull @Positive Long builderTypeId,
        Pageable pageable
    ) {
        return builderTypeItemResource.getBuilderTypeItemsByBuilderTypeId(name, builderTypeId, pageable);
    }

    @PostMapping("/{builderTypeId}/builder-type-items")
    public ResponseEntity<BuilderTypeItemDTO> createBuilderTypeItem(
        @PathVariable(value = "builderTypeId") @NotNull @Positive Long builderTypeId,
        @Validated(BuilderTypeItemDTO.Create.class) @RequestBody @NotNull BuilderTypeItemDTO builderTypeItemDTO
    ) {
        return builderTypeItemResource.createBuilderTypeItem(builderTypeId, builderTypeItemDTO);
    }

    @PutMapping("/{builderTypeId}/builder-type-items/{builderTypeItemId}")
    public ResponseEntity<BuilderTypeItemDTO> updateBuilderTypeItem(
        @PathVariable(value = "builderTypeId") @NotNull @Positive Long builderTypeId,
        @PathVariable(value = "builderTypeItemId") @NotNull @Positive Long builderTypeItemId,
        @Validated(BuilderTypeItemDTO.Create.class) @RequestBody @NotNull BuilderTypeItemDTO builderTypeItemDTO
    ) {
        return builderTypeItemResource.updateBuilderTypeItem(builderTypeId, builderTypeItemId, builderTypeItemDTO);
    }

    @DeleteMapping("/{builderTypeId}/builder-type-items/{builderTypeItemId}")
    public ResponseEntity<?> deleteBuilderTypeItem(
        @PathVariable(value = "builderTypeId") @NotNull @Positive Long builderTypeId,
        @PathVariable(value = "builderTypeItemId") @NotNull @Positive Long builderTypeItemId
    ) {
        return builderTypeItemResource.deleteBuilderTypeItem(builderTypeId, builderTypeItemId);
    }

    @GetMapping("list-child/{categoryId}")
    public ResponseEntity<List<BuilderTypeDTO>> getListObjectByCategory(@PathVariable(value = "categoryId") Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<BuilderTypeDTO> dtos = builderTypeService.getListChildByCategoryId(categoryId, unpaged);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/{builderTypeId}/builder-type-items/{builderTypeItemId}")
    public ResponseEntity<BuilderTypeItemDTO> getDetailBuilderTypeItem(
        @PathVariable(value = "builderTypeId") @NotNull @Positive Long builderTypeId,
        @PathVariable(value = "builderTypeItemId") @NotNull @Positive Long builderTypeItemId
    ) {
        return ResponseEntity.ok(builderTypeItemService.getDetailBuilderTypeItem(builderTypeId, builderTypeItemId));
    }

    @GetMapping("list-child-builder-types/{categoryId}")
    public ResponseEntity<Page<BuilderTypeDTO>> getListObjectByCategoryPaging(
        @PathVariable(value = "categoryId") Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<BuilderTypeDTO> page = builderTypeService.getListBuilderTypesPaging(categoryId,name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @Override
    public String getSortFieldName() {
        return BuilderType.FieldNames.POS_INDEX;
    }

    //routers
    @GetMapping("/{builderTypeId}/routers")
    public ResponseEntity<Page<RouterDTO>> getListRouterDTO(@PathVariable("builderTypeId") Long builderTypeId, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<RouterDTO> routerDTOS = routerService.getListRouterDTOByBuilderTypeId(builderTypeId, pageable);
        return ResponseEntity.ok().body(routerDTOS);
    }

    @PostMapping("/{builderTypeId}/routers")
    public ResponseEntity<BaseResponseDTO> createRouterInBuilderType(
        @RequestBody ArrayList<Long> routerIds,
        @PathVariable("builderTypeId") @NotNull @Positive Long builderTypeId) {
        builderTypeRouterMapService.createBuilderTypeRouterMap(routerIds, builderTypeId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{builderTypeId}/routers/{routerId}")
    public ResponseEntity<BaseResponseDTO> deleteRouterInBuilderType(
        @PathVariable("builderTypeId") @NotNull @Positive Long builderTypeId,
        @PathVariable("routerId") @NotNull @Positive Long routerId) {
        builderTypeRouterMapService.deleteBuilderTypeRouterMap(routerId, builderTypeId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{builderTypeId}/list-routers")
    public ResponseEntity<List<RouterDTO>> getListRouterNotPaging(@PathVariable("builderTypeId") Long builderTypeId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<RouterDTO> routerDTOS = routerService.getListRouterNotPaging(builderTypeId, unpaged);
        return ResponseEntity.ok().body(routerDTOS);
    }
}
