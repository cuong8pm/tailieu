package com.mycompany.myapp.web.rest.offer.template;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.common.CharValueUseMapDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.mycompany.myapp.domain.offer.offer.OfferVersionCharspecValueUse;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionCharspecUse;
import com.mycompany.myapp.dto.offer.common.CharOfVersionDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionCharspecUseDTO;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionCharspecUseService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.offer.common.VersionCharspecUseCommonResource;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-template-version-charspec-uses")
public class OfferTemplateVersionCharspecUseResource extends VersionCharspecUseCommonResource<OfferTemplateVersionCharspecUseDTO, OfferTemplateVersionCharspecUse> {

    @Autowired
    private
    OfferTemplateVersionCharspecUseService offerTemplateVersionCharspecUseService;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerTemplateVersionCharspecUseService;
    }

    @GetMapping("/list-child-characteristics/{id}")
    public ResponseEntity<Page<CharOfVersionDTO>> listChildCharacteristic( @PathVariable @Positive Long id,Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<CharOfVersionDTO> page = offerTemplateVersionCharspecUseService.listChildCharacteristic(id, pageable);
        return ResponseEntity.ok().body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferTemplateVersionCharspecSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, OfferVersionCharspecValueUse.FieldNames.ID) : sort);
    }

    @GetMapping("/list-all-characteristics/{id}")
    public ResponseEntity<List<CharOfVersionDTO>> listAllCharacteristic(@PathVariable @Positive Long id) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, OfferVersionCharspecValueUse.FieldNames.ID));
        List<CharOfVersionDTO> list = offerTemplateVersionCharspecUseService.listChildCharacteristic(id, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @PostMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> update(@PathVariable Long id, @RequestBody List<CharValueUseMapDTO> dtos) {
        commonService.addCharacteristic(id, dtos);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
