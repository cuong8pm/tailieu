package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.service.eventProcessing.ConditionService;
import com.mycompany.myapp.service.eventProcessing.EventPolicyService;
import com.mycompany.myapp.service.eventProcessing.FilterService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("api/event-policies")
public class EventPolicyResource extends AbstractResource {

    @Autowired
    private EventPolicyService eventPolicyService;

    @Autowired
    private FilterResource filterResource;

    @Autowired
    private ConditionResource conditionResource;

    @Autowired
    private BuilderResource builderResource;

    @Autowired
    private FilterService filterService;

    @Autowired
    private ConditionService conditionService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = eventPolicyService;
        this.ocsCloneService = eventPolicyService;
    }

    @PostMapping("")
    public ResponseEntity<EventPolicyDTO> createEventPolicy(
        @Validated(EventPolicyDTO.Create.class) @RequestBody @NotNull EventPolicyDTO eventPolicyDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(eventPolicyService.createEventPolicy(eventPolicyDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventPolicyDTO> updateEventPolicy(
        @PathVariable(value = "id") @NotNull @Positive Long eventPolicyId,
        @Validated(EventPolicyDTO.Create.class) @RequestBody @NotNull EventPolicyDTO eventPolicyDTO
    ) {
        eventPolicyDTO.setId(eventPolicyId);
        return ResponseEntity.status(HttpStatus.CREATED).body(eventPolicyService.updateEventPolicy(eventPolicyDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEventPolicy(
        @PathVariable(value = "id") @NotNull @Positive Long eventPolicyId) {
        eventPolicyService.deleteEventPolicy(eventPolicyId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<EventPolicyDTO> getDetailEventPolicy(
        @PathVariable(value = "id") @NotNull @Positive Long eventPolicyId) {
        return ResponseEntity.ok().body(eventPolicyService.getDetailEventPolicy(eventPolicyId));
    }

    @GetMapping("/search-event-policies")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchEventPolicy(@RequestParam(value = "name", defaultValue = "") String name) {
        return ResponseEntity.ok().body(eventPolicyService.searchInTree(name.trim(), CategoryType.EVENT_POLICY));
    }

    @GetMapping("list-child/{categoryId}")
    public ResponseEntity<List<EventPolicyDTO>> getListObjectByCategory(@PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<EventPolicyDTO> dtos = eventPolicyService.getListEventPolicyNotPaging(categoryId, unpaged);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child-event-policies/{categoryId}")
    public ResponseEntity<Page<EventPolicyDTO>> getListObjectByCategoryPaging(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<EventPolicyDTO> page = eventPolicyService.getListEventPolicyByCategoryId(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/list-child-filters/{eventPolicyId}")
    public ResponseEntity<List<FilterDTO>> getDetailPopupEnterParameterParam(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId) {
        return ResponseEntity.ok(eventPolicyService.getListFilterByEventPolicyId(eventPolicyId));
    }

    @GetMapping("/{eventPolicyId}/filters")
    public ResponseEntity<Page<FilterDTO>> getPageFilterByEventPolicyId(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId,
        Pageable pageable){
        return filterResource.getListFilterByEventPolicyIdPaging(eventPolicyId,pageable);
    }

    @GetMapping("/{eventPolicyId}/conditions")
    public ResponseEntity<Page<ConditionDTO>> getListConditionByEventPolicyIdPaging(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId,
        Pageable pageable){
        return conditionResource.getListConditionByEventPolicyIdPaging(eventPolicyId,pageable);
    }

    @GetMapping("/{eventPolicyId}/builders")
    public ResponseEntity<Page<BuilderDTO>> getListBuilderByEventPolicyIdPaging(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId,
        Pageable pageable){
        return builderResource.getListBuilderByEventPolicyIdPaging(eventPolicyId,pageable);
    }

    @GetMapping("/data-clone-filters/{id}")
    public ResponseEntity<FilterDTO> getDetailFilterClone(
            @PathVariable @NotNull @Positive Long id) {
        return ResponseEntity.ok().body(filterService.getDetailFilterClone(id));
    }

    @GetMapping("/data-clone-conditions/{id}")
    public ResponseEntity<ConditionDTO> getDetailConditionClone(@PathVariable @NotNull @Positive Long id) {
        return ResponseEntity.ok().body(conditionService.getDetailConditionClone(id));
    }

    @PostMapping("/validator-filters")
    public ResponseEntity<BaseResponseDTO> validatorFilters(@Validated(CreateEventPolicyDTO.Validator.class) @RequestBody @NotNull FilterDTO dto) {
        filterService.validatorFilters(dto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/validator-conditions")
    public ResponseEntity<List<BuilderDTO>> validatorConditions(@Validated(CreateEventPolicyDTO.Validator.class) @RequestBody @NotNull ConditionDTO dto) {
        return ResponseEntity.ok().body(conditionService.validatorConditions(dto));
    }

    @DeleteMapping("/{eventPolicyId}/filters/{filterId}")
    public ResponseEntity<?> deleteFilter(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId,
        @PathVariable(value = "filterId") @NotNull @Positive Long filterId){
        eventPolicyService.deleteFilter(eventPolicyId,filterId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/{eventPolicyId}/conditions/{conditionId}")
    public ResponseEntity<?> deleteCondition(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId,
        @PathVariable(value = "conditionId") @NotNull @Positive Long conditionId){
        eventPolicyService.deleteCondition(eventPolicyId,conditionId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/{eventPolicyId}/builders/{builderId}")
    public ResponseEntity<?> deleteBuilder(
        @PathVariable(value = "eventPolicyId") @NotNull @Positive Long eventPolicyId,
        @PathVariable(value = "builderId") @NotNull @Positive Long builderId){
        eventPolicyService.deleteBuilder(eventPolicyId,builderId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/{id}")
    public ResponseEntity<CreateEventPolicyDTO> createEventPolicy(@PathVariable @NotNull @Positive Long id,
            @Validated(CreateEventPolicyDTO.Validator.class) @RequestBody @NotNull CreateEventPolicyDTO dto) {
        CreateEventPolicyDTO createEventPolicyDTO = eventPolicyService.createEventPolicy(id, dto);
        return ResponseEntity.ok().body(createEventPolicyDTO);
    }
}
