package com.mycompany.myapp.web.rest;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.MonitorKeyDTO;
import com.mycompany.myapp.service.MonitorKeyService;
import com.mycompany.myapp.utils.IgnoreWildCard;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/monitor-key")
public class MonitorKeyResource {
    private final Logger log = LoggerFactory.getLogger(MonitorKeyResource.class);

    @Autowired
    private MonitorKeyService monitorKeyService;

    @DeleteMapping("/{monitorKeyId}")
    public ResponseEntity<MonitorKeyDTO> deleteMonitorKey(
            @PathVariable(name = "monitorKeyId", required = true) @NotNull @Positive Long monitorKeyId) {
        monitorKeyService.deleteMonitorKey(monitorKeyId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/")
    public ResponseEntity<Page<MonitorKeyDTO>> getMonitorKeys(
            @RequestParam(value = "monitorKey", required = false) String monitorKey,
            @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        pageable = monitorKeyService.validatePageable(pageable);
        Page<MonitorKeyDTO> page = monitorKeyService.getMonitoKeys(monitorKey, description, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addMonitorKey(@Validated(MonitorKeyDTO.Create.class) @RequestBody MonitorKeyDTO monitorKeyDTO) {
        monitorKeyDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(monitorKeyService.createMonitorKey(monitorKeyDTO));
    }

    @PutMapping("/{monitorKeyId}")
    public ResponseEntity<BaseResponseDTO> updateMonitorKey(
            @Validated(MonitorKeyDTO.Update.class) @RequestBody MonitorKeyDTO monitorKeyDTO,
            @PathVariable(name = "monitorKeyId") @NotNull @Positive Long monitorKeyId) {
        monitorKeyDTO.setId(monitorKeyId);
        return ResponseEntity.ok().body(monitorKeyService.updateMonitorKey(monitorKeyDTO, monitorKeyId));
    }

    @GetMapping("/{monitorKeyId}")
    public ResponseEntity<MonitorKeyDTO> getMonitorKeyDetail(@PathVariable(value = "monitorKeyId") Long monitorKeyId) {
        MonitorKeyDTO monitorKeyDTO = monitorKeyService.getMonitorDetail(monitorKeyId);
        return ResponseEntity.ok().body(monitorKeyDTO);
    }

    @GetMapping("/list-all-monitorkeys")
    public ResponseEntity<List<MonitorKeyDTO>> getAllMonitorKey() {
        return ResponseEntity.ok().body(monitorKeyService.getAllMonitorKey());
    }
}
