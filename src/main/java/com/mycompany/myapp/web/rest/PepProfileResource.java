package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.PccRule;
import com.mycompany.myapp.domain.PepProfile;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.PccRuleDTO;
import com.mycompany.myapp.dto.PepProfileDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.PepProfileService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/pep-profiles")
public class PepProfileResource {

    @Autowired
    private PepProfileService pepProfileService;

    @GetMapping("/search-pep-profiles")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getPepProfiles(@RequestParam(value = "name", defaultValue = "") String name) {
        List<Tree> list = pepProfileService.searchInTree(name.trim(), CategoryType.PEP_PROFILE);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<PepProfileDTO>> getPepProfiles(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
            Sort.by(Sort.Direction.ASC, PepProfile.FieldNames.POS_INDEX));
        List<PepProfileDTO> list = pepProfileService.getPepProfiles(categoryId, null, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-pep-profiles/{categoryId}")
    public ResponseEntity<Page<PepProfileDTO>> getPepProfiles(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        @RequestParam(value = "monitorKey", required = false) String monitorKey,
        @RequestParam(value = "priority", required = false) String priority,
        Pageable pageable) {

        pageable = validatePageable(pageable);
        Page<PepProfileDTO> page = pepProfileService.getPepProfiles(categoryId, name, monitorKey, priority, description, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }



    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> createPepProfile(@RequestBody @Validated(PepProfileDTO.Create.class) PepProfileDTO profilePepDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(pepProfileService.createProfilePep(profilePepDTO));
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getPepProfileSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, PepProfile.FieldNames.POS_INDEX) : sort);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updatePepProfile(@PathVariable Long id, @RequestBody @Validated(PepProfileDTO.Create.class) PepProfileDTO profilePepDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(pepProfileService.updateProfilePep(id,profilePepDTO));
    }

    @PutMapping("/move")
    public ResponseEntity<BaseResponseDTO> move(@Validated(PepProfileDTO.Move.class) @RequestBody @NotNull @Size(min = 2, max = 2) List<PepProfileDTO> profilePepDTOs) {
        return ResponseEntity.status(HttpStatus.OK).body(pepProfileService.move(profilePepDTOs));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PepProfileDTO> getPepProfileDetail(@PathVariable("id") @NotNull @Positive Long id) {
        PepProfileDTO pepProfileDTO = pepProfileService.getPepProfileDetail(id);
        return ResponseEntity.ok().body(pepProfileDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deletePepProfile(@PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        pepProfileService.deletePepProfile(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-pcc-rule-mapping/{id}")
    public ResponseEntity<Page<PccRuleDTO>> getPccRules(
        @PathVariable(value = "id") @NotNull @Positive Long id,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        @RequestParam(value = "priority", required = false) String priority,
        Pageable pageable) {

        Page<PccRuleDTO> page = pepProfileService.getPccRules(id, name, priority, description, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }
    
    @GetMapping("/{pepProfileId}/pcc-rules")
    public ResponseEntity<List<PccRuleDTO>> getPccRulesByPepProfiles(
        @PathVariable(value = "pepProfileId") @NotNull @Positive Long pepProfileId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        @RequestParam(value = "priority", required = false) String priority,
        Pageable pageable) {

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, PccRule.FieldNames.POS_INDEX));
        List<PccRuleDTO> list = pepProfileService.getPccRules(pepProfileId, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @DeleteMapping("/pcc-rule-mapping/{pepProfileId}")
    public ResponseEntity<BaseResponseDTO> deletePccRuleMapping(
            @PathVariable(name = "pepProfileId", required = true) @NotNull @Positive Long pepProfileId,
            @RequestParam(value = "pccRuleId", required = true) Long pccRuleId ) {
        pepProfileService.deletePccRuleMapping(pepProfileId, pccRuleId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/{id}/pcc-rule-mapping")
    public ResponseEntity<BaseResponseDTO> createPccRuleMapping(@PathVariable Long id, @RequestBody List<Long> pccRuleIds) {
        pepProfileService.createProfilePepPcc(id, pccRuleIds);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/list-pcc-rule/pep-profile/{id}")
    public ResponseEntity<List<PccRuleDTO>> getListPccRules(@PathVariable(value = "id") @NotNull @Positive Long id) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, PccRule.FieldNames.POS_INDEX));
        List<PccRuleDTO> list = pepProfileService.getPccRules(id, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @PutMapping("/{id}/pcc-rule-mapping/move")
    public ResponseEntity<BaseResponseDTO> movePccRuleMapping(@PathVariable Long id, @RequestBody @NotNull @Size(min = 2, max = 2) List<PccRuleDTO> pccRuleDTOs) {
        return ResponseEntity.status(HttpStatus.OK).body(pepProfileService.moveMapping(pccRuleDTOs, id));
    }
}
