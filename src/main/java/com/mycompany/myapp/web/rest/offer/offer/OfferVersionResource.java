package com.mycompany.myapp.web.rest.offer.offer;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.repository.offer.common.OfferVersionCommonRepository;
import com.mycompany.myapp.service.offer.offer.OfferService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.offer.offer.OfferVersion;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.common.MaxNumberDTO;
import com.mycompany.myapp.dto.offer.offer.OfferCharacteristicDTO;
import com.mycompany.myapp.dto.offer.offer.OfferOfCharacteristicDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.service.offer.offer.OfferVersionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.offer.common.OfferVersionCommonResource;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/offer-versions")
public class OfferVersionResource extends OfferVersionCommonResource<OfferVersionDTO, OfferVersion>{

    @Autowired
    private OfferVersionService offerVersionService;

    @Autowired
    private OfferService offerService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.offerVersionCommonService = offerVersionService;
        this.ocsCloneService = offerVersionService;
        this.childMappingService = null;
        this.offerCommonService = offerService;
    }

    @Override
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> createVersion(@NotNull @RequestBody @Validated(OfferVersionCommonDTO.Create.class) OfferVersionDTO offerVersionDTO) {
        offerVersionDTO.setNumber(offerVersionService.getMaxNumberOfVersions(offerVersionDTO.getNumber(),offerVersionDTO.getOfferId()));
        return super.createVersion(offerVersionDTO);
    }

    @GetMapping("/{offerId}/max-number")
    public ResponseEntity<MaxNumberDTO> getMaxNumber(@NotNull @PathVariable Long offerId) {
        return ResponseEntity.ok().body(offerVersionService.getMaxNumberOfVersion(offerId));
    }

    @GetMapping("/list-child-chacracteristics")
    public ResponseEntity<List<OfferOfCharacteristicDTO>> findChacracteristics(@RequestParam("charspecIds") List<Long> charspecIds , Pageable pageable){
        pageable = validatePageable(pageable);
        List<OfferOfCharacteristicDTO> dtos = offerVersionService.findChacracteristics(charspecIds, pageable).getContent();
        return ResponseEntity.ok().body(dtos);
    }


    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCharacteristicSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(0, Integer.MAX_VALUE,
                sort == null ? Sort.by(Sort.Direction.ASC, Characteristic.FieldNames.ID) : sort);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteOfferTemplateVersion(@NotNull @PathVariable Long id) {
        offerVersionService.deleteOfferVersion(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    @GetMapping("/chacracteristics/{VersionCharValueUseId}")
    public ResponseEntity<OfferCharacteristicDTO> findValueChacracteristics(@PathVariable("VersionCharValueUseId") Long id ){
        OfferCharacteristicDTO  dtos = offerVersionService.getcharacteristic(id);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/get-data/{offerTemplateVersionId}")
    public ResponseEntity<OfferVersionDTO> getDataOfferTemplateVersion(@PathVariable @NotNull Long offerTemplateVersionId) {
        return ResponseEntity.ok().body(offerVersionService.getDataOfferVersion(offerTemplateVersionId));
    }
}
