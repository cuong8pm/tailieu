package com.mycompany.myapp.web.rest;

import javax.validation.constraints.PositiveOrZero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.service.formula.FormulaService;
import com.mycompany.myapp.service.ratetable.RateTableService;
import com.mycompany.myapp.utils.IgnoreWildCard;

@RestController
@IgnoreWildCard
@RequestMapping("/api/formula")
public class FormulaResource {
    
    @Autowired
    private FormulaService formulaService;
    
    @Autowired
    private RateTableService rateTableService;
    
    
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteformula(@PathVariable @PositiveOrZero Long id) {
        formulaService.deleteFormula(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
    
    @GetMapping("/{formulaId}")
    public ResponseEntity<FormulaDTO> getDetailFormula(@PathVariable Long formulaId) {
        return ResponseEntity.ok().body(rateTableService.getDetailFormula(formulaId));
    }
}
