package com.mycompany.myapp.web.rest;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSService;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.OcsServiceDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.OcsServiceService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/services")
public class OcsServiceResource {
    private final Logger log = LoggerFactory.getLogger(OcsServiceResource.class);

    @Autowired
    private OcsServiceService ocsServiceSerivce;

    @DeleteMapping("/{serviceId}")
    public ResponseEntity<BaseResponseDTO> deleteService(
            @PathVariable(name = "serviceId", required = true) @NotNull @Positive Long serviceId) {
        ocsServiceSerivce.deleteService(serviceId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/move")
    public ResponseEntity<Object> moveService(
            @Validated(OcsServiceDTO.Move.class) @RequestBody List<OcsServiceDTO> services) {
        if (services.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Service's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(ocsServiceSerivce.move(services));
    }

    @GetMapping("/list-child-services/{categoryId}")
    public ResponseEntity<Page<OcsServiceDTO>> getServices(@PathVariable(value = "categoryId") Long categoryId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "description", required = false) String description, Pageable pageable) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        pageable = ocsServiceSerivce.validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description, categoryId);
        Page<OcsServiceDTO> page = ocsServiceSerivce.getServices(searchCriteria, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addService(
            @Validated(OcsServiceDTO.Create.class) @RequestBody OcsServiceDTO service) {
        service.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(ocsServiceSerivce.createService(service));
    }

    @PutMapping("/{serviceId}")
    public ResponseEntity<BaseResponseDTO> updateService(
            @Validated(OcsServiceDTO.Update.class) @RequestBody OcsServiceDTO service,
            @PathVariable(name = "serviceId") @NotNull @Positive Long serviceId) {
        service.setId(serviceId);
        return ResponseEntity.ok().body(ocsServiceSerivce.updateService(service));
    }

    @GetMapping("/{serviceId}")
    public ResponseEntity<OcsServiceDTO> getServiceDetail(@PathVariable(value = "serviceId") Long serviceId) {
        OcsServiceDTO ocsServiceDTO = ocsServiceSerivce.getServiceDetail(serviceId);
        return ResponseEntity.ok().body(ocsServiceDTO);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<OcsServiceDTO>> getServices(@PathVariable(value = "categoryId") Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, OCSService.FieldNames.POS_INDEX));

        List<OcsServiceDTO> list = ocsServiceSerivce.getServices(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/search-services")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getServices(@RequestParam(value = "name") String name) {
        List<Tree> list = ocsServiceSerivce.searchInTree(name, CategoryType.SERVICE);
        return ResponseEntity.ok().body(list);
    }
}
