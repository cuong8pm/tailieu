package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.event.Event;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.event.EventDTO;
import com.mycompany.myapp.service.event.EventActionTypeMapService;
import com.mycompany.myapp.service.event.EventService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

@RestController
@IgnoreWildCard
@RequestMapping("/api/events")
public class EventResource extends AbstractResource {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventActionTypeMapService eventActionTypeMapService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = eventService;
        this.childMappingService = eventActionTypeMapService;
    }

    @GetMapping("/list-child-events/{categoryId}")
    public ResponseEntity<Page<EventDTO>> listChildEvents(
            @PathVariable @Positive Long categoryId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description,
            Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<EventDTO> page = eventService.listChildEvents(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getEventSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, Event.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<EventDTO>> listChildEvents(@PathVariable @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Event.FieldNames.POS_INDEX));
        List<EventDTO> dtos = eventService.listChildEvents(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EventDTO> getPriceComponentDetail(@PathVariable Long id) {
        EventDTO dto = eventService.getEventDetail(id);
        return ResponseEntity.ok().body(dto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateEvent(@PathVariable Long id,
            @RequestBody @Validated(EventDTO.Update.class) EventDTO eventDTO) {
        eventDTO.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(eventService.updateEvent(eventDTO));
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> createEvent(@RequestBody @Validated(EventDTO.Create.class) EventDTO eventDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(eventService.createEvent(eventDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEvent(@PathVariable Long id ) {
        eventService.deleteEvent(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/search-events")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getEventTree(@RequestParam(required = false) String name) {
        List<Tree> list = eventService.searchInTree(name, CategoryType.EVENT);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-action-types/{eventId}")
    public ResponseEntity<List<ActionTypeDTO>> getListActionTypes(@PathVariable @Positive Long eventId) {
        List<ActionTypeDTO> list = eventService.getListActionTypes(eventId);
        return ResponseEntity.ok().body(list);
    }
}
