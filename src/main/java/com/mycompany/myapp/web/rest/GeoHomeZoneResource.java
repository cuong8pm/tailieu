package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.GeoHomeZone;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.service.GeoHomeZoneService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@IgnoreWildCard
@RequestMapping("/api/geo-home-zones")
public class GeoHomeZoneResource {
    private final Logger log = LoggerFactory.getLogger(GeoHomeZone.class);
    @Autowired
    private GeoHomeZoneService geoHomeZoneService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<GeoHomeZoneDTO>> getGeoHomeZones(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, GeoHomeZone.FieldNames.POS_INDEX));

        List<GeoHomeZoneDTO> list = geoHomeZoneService.getGeoHomeZones(categoryId, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-geo-home-zones/{categoryId}")
    public ResponseEntity<Page<GeoHomeZoneDTO>> getGeoHomeZones(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "id", required = false) String id,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        Page<GeoHomeZoneDTO> page = geoHomeZoneService.getGeoHomeZones(categoryId, name, description, id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<GeoHomeZoneDTO> getGeoHomeZoneDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        GeoHomeZoneDTO geoHomeZoneDTO= geoHomeZoneService.getGeoHomeZoneDetail(id);
        return ResponseEntity.ok().body(geoHomeZoneDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addGeoHomeZone(
        @Validated(GeoHomeZoneDTO.Create.class) @RequestBody GeoHomeZoneDTO geoHomeZone) {
        geoHomeZone.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(geoHomeZoneService.createGeoHomeZone(geoHomeZone));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveGeoHomeZone(
        @Validated(GeoHomeZoneDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<GeoHomeZoneDTO> geoHomeZone) {
        if (geoHomeZone.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Geo Home Zone's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(geoHomeZoneService.move(geoHomeZone));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteGeoHomeZone(@PathVariable(name = "id") Long id) {
        geoHomeZoneService.deleteGeoHomeZone(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateGeoHomeZone(
        @Validated(GeoHomeZoneDTO.Update.class) @RequestBody GeoHomeZoneDTO geoHomeZoneDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        geoHomeZoneDTO.setId(id);
        return ResponseEntity.ok().body(geoHomeZoneService.updateGeoHomeZone(geoHomeZoneDTO));
    }

    @GetMapping("/search-geo-home-zones")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getGeoHomeZones(
        @RequestParam(value = "name") String name) {
        List<Tree> list = geoHomeZoneService.searchInTree(name, CategoryType.GEO_HOME_ZONE);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/home-zone-types")
    public ResponseEntity<List<ReferTable>>getHomeZoneTypes() {
        return ResponseEntity.ok().body(geoHomeZoneService.getHomeZoneTypes());
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCategorySearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, GeoHomeZone.FieldNames.POS_INDEX) : sort);
    }

}
