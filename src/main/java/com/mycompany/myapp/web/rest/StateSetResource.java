package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.StateGroups;
import com.mycompany.myapp.domain.StateType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.StateGroupsDTO;
import com.mycompany.myapp.dto.StateTypeDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.StateGroupsService;
import com.mycompany.myapp.service.StateTypeService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/state-types")
public class StateSetResource {

    @Autowired
    private StateGroupsService stateGroupsService;
    @Autowired
    private StateTypeService stateTypeService;

    @GetMapping("/list-child-state-types/{parentId}")
    public ResponseEntity<Page<StateTypeDTO>> getStateTypes(@PathVariable(value = "parentId") @NotNull @Positive Long parentId,
                                                            @RequestParam(value = "name", required = false) String name,
                                                            @RequestParam(value = "description", required = false) String description,
                                                            Pageable pageable) {
        pageable = validatePageable(pageable, Resources.STATETYPE);
        Page<StateTypeDTO> page = stateTypeService.getStateType(parentId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StateTypeDTO> getStateTypeDetail(@PathVariable("id") @NotNull @Positive Long id) {
        StateTypeDTO stateTypeDTO = stateTypeService.getStateTypeDetail(id);
        return ResponseEntity.ok().body(stateTypeDTO);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> createStateType(@RequestBody @Validated(StateTypeDTO.Create.class) StateTypeDTO stateTypeDTO) {
        stateTypeDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(stateTypeService.createStateType(stateTypeDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateStateType(@PathVariable(name = "id", required = true) Long id, @Validated(StateTypeDTO.Update.class) @RequestBody StateTypeDTO stateTypeDTO) {
        stateTypeDTO.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(stateTypeService.updateStateType(stateTypeDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteStateType(@PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        stateTypeService.deleteStateType(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/move")
    public ResponseEntity<Object> moveStateType(
        @Validated(StateTypeDTO.Move.class) @RequestBody @NotNull @Size(min = 2, max = 2) List<StateTypeDTO> stateTypeDTOS) {
        return ResponseEntity.ok().body(stateTypeService.move(stateTypeDTOS));
    }

    @GetMapping("/search-state-sets")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getUnitTypes(@RequestParam(value = "name", defaultValue = "") String name) {
        List<Tree> list = stateTypeService.searchInTree(name.trim(), CategoryType.STATE_TYPE);
        list.addAll(stateGroupsService.searchInTree(name.trim(), CategoryType.STATE_GROUP));
        return ResponseEntity.ok().body(list);
    }

    private Pageable validatePageable(Pageable pageable, String type) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getStateTypeSearchParam(order.getProperty());
            if (type.equals(Resources.STATEGROUPS)) {
                propery = OCSUtils.getStateGroupSearchParam(order.getProperty());
            }
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.ASC, StateType.FieldNames.POS_INDEX) : sort);
    }
    @GetMapping("/list-child/{parentId}")
    public ResponseEntity<List<StateTypeDTO>> getListStateTypeByCategoryId(@PathVariable Long parentId) {
        return ResponseEntity.ok().body(stateTypeService.getListStateTypeByCategoryId(parentId));
    }
}
