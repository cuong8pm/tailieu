package com.mycompany.myapp.web.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.service.ReferTableService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.PccRule;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.PccRuleDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.PccRuleService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/pcc-rules")
public class PccRuleResource {
    private final Logger log = LoggerFactory.getLogger(PccRuleResource.class);

    @Autowired
    private PccRuleService pccRuleService;

    @Autowired
    private ReferTableService referTableService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<PccRuleDTO>> getPccRules(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, PccRule.FieldNames.POS_INDEX));

        List<PccRuleDTO> list = pccRuleService.getPccRules(categoryId, null, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-pcc-rules/{categoryId}")
    public ResponseEntity<Page<PccRuleDTO>> getPccRules(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
            @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "priority", required = false) String priority,
            @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        Page<PccRuleDTO> page = pccRuleService.getPccRules(categoryId, name, description, id, priority, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PccRuleDTO> getPccRuleDetail(@PathVariable(value = "id") @NotNull @Positive Long id) {
        PccRuleDTO PccRuleDTO = pccRuleService.getPccRuleDetail(id);
        return ResponseEntity.ok().body(PccRuleDTO);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addPccRule(
            @Validated(PccRuleDTO.Create.class) @RequestBody PccRuleDTO pccRuleDTO) {
        pccRuleDTO.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(pccRuleService.createPccRule(pccRuleDTO));
    }

    @PutMapping("/move")
    public ResponseEntity<Object> movePccRule(
            @Validated(PccRuleDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<PccRuleDTO> PccRuleDTOS) {
        if (PccRuleDTOS.size() != 2) {
            throw new BadRequestAlertException("Bad request", "PccRule's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(pccRuleService.move(PccRuleDTOS));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deletePccRule(@PathVariable(name = "id") Long id) {
        pccRuleService.deletePccRule(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> update(
            @Validated(PccRuleDTO.Update.class) @RequestBody PccRuleDTO PccRuleDTO,
            @PathVariable(name = "id") @NotNull @Positive Long id) {
        PccRuleDTO.setId(id);
        return ResponseEntity.ok().body(pccRuleService.updatePccRule(PccRuleDTO));
    }

    @GetMapping("/search-pcc-rules")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getPccRules(@RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "pepProfileId", required = false) Long pepProfileId,
            @RequestParam(required = false) Boolean isPopup) {
        List<Long> removeIds = new ArrayList<>();
        if (pepProfileId != null) {
            removeIds = pccRuleService.getRemoveIds(pepProfileId);
        }
        List<Tree> list = pccRuleService.searchInTree(name, CategoryType.PCC_RULE, removeIds, isPopup);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-all-send-type")
    public List<ReferTable> getListSendType() {
        return referTableService.getReferTables(ReferType.SendType.getValue());
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getPccRuleSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC, PccRule.FieldNames.POS_INDEX) : sort);
    }
}
