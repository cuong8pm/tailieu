package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Router;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.DropdownDTO;
import com.mycompany.myapp.dto.eventProcessing.RouterDTO;
import com.mycompany.myapp.service.eventProcessing.RouterService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@IgnoreWildCard
@RequestMapping("/api/routers")
public class RouterResource extends AbstractResource {

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = routerService;
        this.ocsBaseService = routerService;
    }

    @Autowired
    private RouterService routerService;

    @GetMapping("/{id}")
    public ResponseEntity<RouterDTO> getRouter(@PathVariable(name = "id") @NotNull @Positive Long id) {
        RouterDTO routerDTO = routerService.getRouterDetail(id);
        return ResponseEntity.ok().body(routerDTO);
    }

    @PostMapping("")
    public ResponseEntity<RouterDTO> createRouter(@Validated(RouterDTO.Create.class) @RequestBody RouterDTO routerDTO) {
        routerDTO.setId(null);
        RouterDTO routerDTORes = routerService.createRouter(routerDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(routerDTORes);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RouterDTO> updateRouter(
        @Validated(RouterDTO.Update.class) @RequestBody RouterDTO routerDTO,
        @PathVariable Long id
    ) {
        routerDTO.setId(id);
        RouterDTO routerDTORes = routerService.updateRouter(routerDTO);
        return ResponseEntity.ok().body(routerDTORes);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RouterDTO> deleteRouter(@PathVariable(name = "id") @NotNull @Positive Long id) {
        routerService.deleteRouter(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-child-routers/{categoryId}")
    public ResponseEntity<Page<RouterDTO>> getRouters(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<RouterDTO> dtos = routerService.getRouters(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<RouterDTO>> getRouters(@PathVariable Long categoryId) {
        Pageable uppaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Router.FieldNames.POS_INDEX));
        List<RouterDTO> dtos = routerService.getRouters(categoryId, null, null, uppaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/search-routers")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getRouterTree(@RequestParam(required = false, defaultValue = "") String name) {
        List<Tree> list = routerService.searchInTree(name.trim(), CategoryType.ROUTER);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/router-types")
    public ResponseEntity<List<DropdownDTO>> getRouterType() {
        return ResponseEntity.ok().body(routerService.getDropdownRouterType());
    }

    @Override
    public String getSortFieldName() {
        return Router.FieldNames.POS_INDEX;
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getRouterSearchParam(order.getProperty());
    }
}
