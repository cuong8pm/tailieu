package com.mycompany.myapp.web.rest.characteristic;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mycompany.myapp.dto.characteristic.ValueTypeDTO;
import com.mycompany.myapp.service.characteristic.ValueTypeService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.viettel.ocs.slice.domain.ValueType}.
 */
@RestController
@RequestMapping("/api")
public class ValueTypeResource {

    private final Logger log = LoggerFactory.getLogger(ValueTypeResource.class);

    private static final String ENTITY_NAME = "ocsSliceValueType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ValueTypeService valueTypeService;

    public ValueTypeResource(ValueTypeService valueTypeService) {
        this.valueTypeService = valueTypeService;
    }

    /**
     * {@code POST  /value-types} : Create a new valueType.
     *
     * @param valueTypeDTO
     *            the valueTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new valueTypeDTO, or with status {@code 400 (Bad Request)}
     *         if the valueType has already an ID.
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect.
     */
    @PostMapping("/value-types")
    public ResponseEntity<ValueTypeDTO> createValueType(@Valid @RequestBody ValueTypeDTO valueTypeDTO)
            throws URISyntaxException {
        log.debug("REST request to save ValueType : {}", valueTypeDTO);
        if (valueTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new valueType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ValueTypeDTO result = valueTypeService.save(valueTypeDTO);
        return ResponseEntity
                .created(new URI("/api/value-types/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /value-types} : Updates an existing valueType.
     *
     * @param valueTypeDTO
     *            the valueTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated valueTypeDTO, or with status {@code 400 (Bad Request)} if
     *         the valueTypeDTO is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the valueTypeDTO couldn't be
     *         updated.
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect.
     */
    @PutMapping("/value-types")
    public ResponseEntity<ValueTypeDTO> updateValueType(@Valid @RequestBody ValueTypeDTO valueTypeDTO)
            throws URISyntaxException {
        log.debug("REST request to update ValueType : {}", valueTypeDTO);
        if (valueTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ValueTypeDTO result = valueTypeService.save(valueTypeDTO);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, valueTypeDTO.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /value-types} : get all the valueTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of valueTypes in body.
     */
    @GetMapping("/value-types")
    public List<ValueTypeDTO> getAllValueTypes() {
        log.debug("REST request to get all ValueTypes");
        return valueTypeService.findAll();
    }

    /**
     * {@code GET  /value-types/:id} : get the "id" valueType.
     *
     * @param id
     *            the id of the valueTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the valueTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/value-types/{id}")
    public ResponseEntity<ValueTypeDTO> getValueType(@PathVariable Long id) {
        log.debug("REST request to get ValueType : {}", id);
        Optional<ValueTypeDTO> valueTypeDTO = valueTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(valueTypeDTO);
    }

    /**
     * {@code DELETE  /value-types/:id} : delete the "id" valueType.
     *
     * @param id
     *            the id of the valueTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/value-types/{id}")
    public ResponseEntity<Void> deleteValueType(@PathVariable Long id) {
        log.debug("REST request to delete ValueType : {}", id);
        valueTypeService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
