package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.UnitType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.UnitTypeDTO;
import com.mycompany.myapp.service.UnitTypeService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api/unit-types")
@Validated
@IgnoreWildCard
public class UnitTypeResource {
    private final Logger log = LoggerFactory.getLogger(UnitTypeResource.class);

    @Autowired
    private UnitTypeService unitTypeService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<UnitTypeDTO>> getUnitTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, UnitType.FieldNames.POS_INDEX));

        List<UnitTypeDTO> list = unitTypeService.getUnitTypes(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }
    // Get
    @GetMapping("/list-child-unit-types/{categoryId}")
    public ResponseEntity<Page<UnitTypeDTO>> getUnitTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        pageable = validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description, categoryId);
        Page<UnitTypeDTO> page = unitTypeService.getUnitTypes(searchCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<UnitTypeDTO> getUnitTypeDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        UnitTypeDTO unitTypeDTO = unitTypeService.getUnitTypeDetail(id);
        return ResponseEntity.ok().body(unitTypeDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addUnitType(
        @Validated(UnitTypeDTO.Create.class) @RequestBody UnitTypeDTO unitType) {
        unitType.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(unitTypeService.createUnitType(unitType));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveUnitTypes(
        @Validated(UnitTypeDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<UnitTypeDTO> unitTypes) {
        if (unitTypes.size() != 2) {
            throw new BadRequestAlertException("Bad request", "UnitType's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(unitTypeService.move(unitTypes));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteUnitType(@PathVariable(name = "id", required = true) Long id) {
        unitTypeService.deleteUnitType(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateUnitType(
        @Validated(UnitTypeDTO.Update.class) @RequestBody UnitTypeDTO unitType,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        unitType.setId(id);
        return ResponseEntity.ok().body(unitTypeService.updateUnitType(unitType));
    }


    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCategorySearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.ASC, UnitType.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/search-unit-types")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getUnitTypes (
        @RequestParam(value = "name") String name) {
        List<Tree> list = unitTypeService.searchInTree(name, CategoryType.UNIT_TYPES);
        return ResponseEntity.ok().body(list);
    }

    @JsonView(UnitTypeDTO.BalTypeView.class)
    @GetMapping("/dd-unit-type")
    public ResponseEntity<List<UnitTypeDTO>> getForDropdown() {
        return ResponseEntity.ok(unitTypeService.getUnitTypeForDropdown());
    }
}
