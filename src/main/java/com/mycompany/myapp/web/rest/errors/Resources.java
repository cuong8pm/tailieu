package com.mycompany.myapp.web.rest.errors;

public final class Resources {

    private Resources() {}

    public static final String CATEGORY = "categories";
    public static final String UNIT_TYPE = "unit-types";
    public static final String PARAMETER = "parameters";
    public static final String RESERVE_INFO = "reserve-infos";
    public static final String ZONE_MAP = "zone-maps";
    public static final String ZONE = "zones";
    public static final String ZONE_DATA = "zone-datas";
    public static final String GEO_HOME_ZONE = "geo-home-zones";
    public static final String GEO_NET_ZONE = "geo-net-zones";
    public static final String STATISTIC = "statistics";
    public static final String BILLING_CYCLE_TYPE = "billing-cycle-types";
    public static final String BAL_TYPE = "balances";
    public static final String ACM_BAL_TYPE = "accumulate-balances";
    public static final String THRESHOLD = "threshold";
    public static final String THRESHOLD_BALTYPE_MAP = "threshold-baltype-map";
    public static final String BILLING_CYCLE = "billing-cycles";
    public static final String SERVICE = "services";
    public static final String CLONEABLE = "cloneable";
    public static final String ENTITY = "entity";
    public static final String ACCOUNT_BALANCE_MAPPINGS = "account-balance-mappings";
    public static final String PCC_RULE = "pcc-rules";
    public static final String STATEGROUPS = "state-groups";
    public static final String STATETYPE = "state-types";
    public static final String MONITOR_KEY = "monitor-key";
    public static final String CHARACTERISTIC = "characteristic";
    public static final String BALANCE_ACM_MAPPINGS = "balance-acm-mappings";
    public static final String PEP_PROFILE = "pep-profiles";
    public static final String QOS = "qos";
    public static final String CALC_UNIT = "calc-unit";
    public static final String BLOCK = "blocks";
    public static final String BLOCK_BASIC = "block-basic";
    public static final String BLOCK_DISCOUNT = "block-discount";
    public static final String BLOCK_ACCMULATE = "block-accumulate";
    public static final String BLOCK_FILTER = "block-filters";
    public static final String NORMALIZER = "normalizers";
    public static final String NESTED_OBJECT = "nested-objects";
    public static final String NORMALIZER_PARAMS = "normalizer-params";
    public static final String RATING_FILTER = "rating-filters";
    public static final String RATING_FILTER_RATE_TABLE_MAP = "rating-filter-rate-table-map";
    public static final String RATE_TABLE = "rate-tables";
    public static final String FORMULA = "formula";
    public static final String FORMULA_ERROR_CODE = "formula-error-code";
    public static final String PRICE_COMPONENT = "price-components";
    public static final String ACTION = "actions";
    public static final String ACTION_PRICE_COMPONENT_MAP = "action-price-component-map";
    public static final String OFFER = "offers";
    public static final String OFFER_VERSION = "offer-versions";
    public static final String OFFER_TEMPLATE_VERSION = "offer-template-versions";
    public static final String OFFER_TEMPLATE = "offer-templates";
    public static final String ACTION_TYPE = "action-types";
    public static final String EVENT = "events";
    public static final String ACTION_DYNAMIC_REVERVE = "action_dynamic_reverve";
    public static final String ACTION_SORTING_PC_FILTER = "action_sorting_pc_filter";
    public static final String ACTION_PRIORITY_FILTER = "action_priority_filter";
    public static final String ACTION_PC_LIST = "action_pc_list";
    public static final String OFFER_VERSION_REDIRECTION = "offer-version-redirections";
    public static final String OFFER_TEMPLATE_VERSION_REDIRECTION = "offer-template-version-redirections";
    public static final String OFFER_VERSION_BUNDLE = "offer-version-bundles";
    public static final String OFFER_TEMPLATE_VERSION_BUNDLE = "offer_template_version_bundle";
    public static final String OFFER_VERSION_ACTION = "offer-version-actions";
    public static final String OFFER_TEMPLATE_VERSION_ACTION = "offer-template-version-actions";
    public static final String REFER_TABLE = "refer-tables";
    public static final String BUILDER = "builders";
    public static final String NOTIFY = "notify";
    public static final String BUILDER_TYPE = "builder-types";
    public static final String BUILDER_TYPE_ROUTER_MAP = "builder-type-router-map";
    public static final String BUILDER_ITEM = "builder-item";
    public static final String CONDITION = "conditions";
    public static final String CONDITION_BUILDER_MAP = "condition-builder-map";
    public static final String CONDITION_VERIFICATION_MAP = "condition-verification-map";
    public static final String VERIFICATION = "verifications";
    public static final String VERIFICATION_ITEM = "verifications-items";
    public static final String FUNCTION = "functions";
    public static final String FILTER = "filters";
    public static final String EVENT_POLICY = "event-policies";
    public static final String EXPRESSION = "expressions";
    public static final String DATA_SOURCE = "data-sources";
    public static final String BUILDER_TYPE_ITEM = "builder-type-items";
    public static final String ROUTER = "routers";
    public static final String FUNCTION_PARAM = "function-params";
    public static final String CONDITION_BUILDER_MAPPINGS = "condition-builder-mappings";
    public static final String EVENT_OBJECT_FIELD = "event-object-field";
    public static final String FIELD = "fields";

    public static final Long DEFAULT_RELATIONSHIP = 1L;

    /**
     * Constants for front end to display icon
     * @author HaTH
     *
     */
    public final class TreeType {

        private TreeType() {}

        public static final String CATEGORY = "category";
        public static final String TEMPLTE = "template";
        public static final String SUB_TEMPLATE = "sub-template";
        public static final String SERVICE = "service";
    }
}
