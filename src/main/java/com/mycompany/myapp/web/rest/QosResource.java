package com.mycompany.myapp.web.rest;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mycompany.myapp.domain.Qos;
import com.mycompany.myapp.domain.QosClass;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.QosDTO;
import com.mycompany.myapp.service.QosSerivce;
import com.mycompany.myapp.utils.IgnoreWildCard;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/qos")
public class QosResource {
    
    private final Logger log = LoggerFactory.getLogger(QosResource.class);
    
    @Autowired
    private QosSerivce qosService;
    
    @DeleteMapping("/{qosId}")
    public ResponseEntity<QosDTO> deleteQos(@PathVariable(name = "qosId", required = true) @NotNull @Positive Long qosId) {
        qosService.deleteQos(qosId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
    
    @GetMapping("/")
    public ResponseEntity<Page<QosDTO>> getQosS (@RequestParam(value = "name", required = false) String name,
                                                 @RequestParam(value = "qci", required = false) String qci,
                                                 @RequestParam(value = "mbrul", required = false) String mbrul,
                                                 @RequestParam(value = "mbrdl", required = false) String mbrdl,
                                                 @RequestParam(value = "gbrul", required = false) String gbrul,
                                                 @RequestParam(value = "gbrdl", required = false) String gbrdl,
                                                 @RequestParam(value = "description", required = false) String description,
                                                 Pageable pageable) {
        pageable = qosService.validatePageable(pageable);
        Page<QosDTO> page = qosService.getAllQos(name, qci, mbrul, mbrdl, gbrul, gbrdl, description, pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }
    
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addQos(@Validated(QosDTO.Create.class) @RequestBody QosDTO qosDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(qosService.createQos(qosDTO));
    }
    
    @PutMapping("/{qosId}")
    public ResponseEntity<BaseResponseDTO> updateQos(
            @Validated(QosDTO.Update.class) @RequestBody QosDTO qosDTO,
            @PathVariable(name = "qosId") @NotNull @Positive Long qosId) {
        qosDTO.setId(qosId);
        return ResponseEntity.ok().body(qosService.updateQos(qosDTO));
    }
    
    @GetMapping("/list-qci")
    public ResponseEntity<List<QosClass>> getListQCI() {
        return ResponseEntity.ok().body(qosService.getListQCI());
    }

    @GetMapping("/list-all-qos")
    public ResponseEntity<List<Qos>> getListQos() {
        return ResponseEntity.ok().body(qosService.getListAllQos());
    }
}
