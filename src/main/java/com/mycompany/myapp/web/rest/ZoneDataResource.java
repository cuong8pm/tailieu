package com.mycompany.myapp.web.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.javatuples.Quartet;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mycompany.myapp.config.ApplicationProperties;
import com.mycompany.myapp.domain.Zone;
import com.mycompany.myapp.domain.ZoneData;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ZoneDataDTO;
import com.mycompany.myapp.repository.ZoneRepository;
import com.mycompany.myapp.service.ZoneDataService;
import com.mycompany.myapp.utils.ExcelUtils;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/zone-datas")
public class ZoneDataResource {
    private final Logger log = LoggerFactory.getLogger(ZoneDataResource.class);
    @Autowired
    private ZoneDataService zoneDataService;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ZoneRepository zoneRepository;

    private static final String EXTENSION = ".xls";
    private static final String FILE_EXPORT = "ZoneName-";

    @GetMapping("/list-child/{zoneId}")
    public ResponseEntity<List<ZoneDataDTO>> getZoneData(
        @PathVariable(value = "zoneId") @NotNull @Positive Long zoneId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, ZoneData.FieldNames.ID));

        List<ZoneDataDTO> list = zoneDataService.getZoneData(zoneId,null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-zone-datas/{zoneId}")
    public ResponseEntity<Page<ZoneDataDTO>> getZoneData(
        @PathVariable(value = "zoneId") @NotNull @Positive Long zoneId,
        @RequestParam(value = "zoneDataValue", required = false) String zoneDataValue,
        @RequestParam(value = "updateDate", required = false) String updateDate,
        @RequestParam(value = "id", required = false) String id,
        Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<ZoneDataDTO> page = zoneDataService.getZoneData(zoneId, zoneDataValue, updateDate, id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<ZoneDataDTO> getZoneDataDetail(@PathVariable(value = "id") @NotNull @Positive Long id) {
        ZoneDataDTO ZoneDataDTO = zoneDataService.getZoneDataDetail(id);
        return ResponseEntity.ok().body(ZoneDataDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addZoneData(
        @Validated(ZoneDataDTO.Create.class) @RequestBody ZoneDataDTO zoneData) {
        zoneData.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(zoneDataService.createZoneData(zoneData));
    }

    // Xoa
    @DeleteMapping("/{ids}")
    public ResponseEntity<BaseResponseDTO> deleteZoneData(@PathVariable(name = "ids") List<Long> ids) {
        zoneDataService.deleteZoneData(ids);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateZone(
        @Validated(ZoneDataDTO.Update.class) @RequestBody ZoneDataDTO ZoneDataDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        ZoneDataDTO.setId(id);
        return ResponseEntity.ok().body(zoneDataService.updateZoneData(ZoneDataDTO));
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getZoneDataSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, ZoneData.FieldNames.ID) : sort);
    }

    @GetMapping(path = "/download")
    public ResponseEntity<Resource> downloadTemplate() throws Exception {

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + applicationProperties.getZoneDataTemplateFileName());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
        ByteArrayResource resource = null;
        File file = null;
        try {
            String filePath = applicationProperties.getExcelTemplateRootDirectoryPath() + applicationProperties.getZoneDataTemplateFileName();
            Path path = Paths.get(filePath);
            file = new File(filePath);
            if(file.length() == 0) {
                throw new Exception();
            }
            resource = new ByteArrayResource(Files.readAllBytes(path));
        } catch (Exception e) {
            log.error("Request download template : {}", e.getMessage());
            throw new BadRequestAlertException("excel_file_does_not_exist", Resources.ZONE_DATA, "file");
        }


        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(resource);
    }

    @Transactional
    @PostMapping("/{zoneId}/import")
    public ResponseEntity<?> importDataFromFile(@RequestParam("file") MultipartFile readExcelDataFile, @PathVariable Long zoneId) throws IOException {
        Quartet<Boolean, InputStream, Integer, Integer> quartet = zoneDataService.importDataFromFile(readExcelDataFile,
                zoneId);
        String[] fileName = readExcelDataFile.getOriginalFilename().split("\\.");
        HttpHeaders header = new HttpHeaders();

        LocalDateTime now = LocalDateTime.now().plusHours(7);
        LocalDate nowDay = LocalDate.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMuuuu_HHmmss");

        String nameOfFile = fileName[0] + "_" + dtf.format(now) + "." + fileName[fileName.length - 1];
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + nameOfFile);
        header.add("Number-Of-Records", "" + quartet.getValue2());
        header.add("Number-Of-Valid-Records", "" + quartet.getValue3());
        header.add("Is-Success", "" + quartet.getValue0());
        InputStreamResource file = new InputStreamResource(quartet.getValue1());

        if("txt".equals(fileName[fileName.length - 1])) {
            File fileTemp = new File(
                    applicationProperties.getExcelTemplateRootDirectoryPath() + readExcelDataFile.getName() + dtf.format(now) + ".txt");
            if(fileTemp.delete()) {
                System.out.println("Deleted the file: " + fileTemp.getName());
            }
            return ResponseEntity.ok()
                    .headers(header)
                    .contentType(MediaType.TEXT_XML)
                    .body(file);
        }
        return ResponseEntity.ok()
                .headers(header)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/{zoneId}/export")
    public ResponseEntity<Resource> exportZoneData(@PathVariable Long zoneId) throws IOException {
        LocalDateTime now = LocalDateTime.now().plusHours(7);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(ExcelUtils.FORMAT_DATE_TIME);
        Zone currentZone = zoneRepository.findByIdAndDomainId(zoneId, OCSUtils.getDomain());
        String filename = currentZone.getName() + "-" + dtf.format(now) + EXTENSION;
        InputStreamResource file = new InputStreamResource(zoneDataService.exportZoneData(zoneId));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
}
