package com.mycompany.myapp.web.rest.offer.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.service.offer.common.OfferCommonService;
import com.mycompany.myapp.utils.OCSUtils;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.common.OfferVersionCommon;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.OfferVersionCloneDTO;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.service.exception.DataConstrainException;
import com.mycompany.myapp.service.exception.ErrorKey;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.service.offer.common.OfferVersionCommonService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.AbstractResource;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Validated
@RestController
@IgnoreWildCard
@RequestMapping("/api")
public abstract class OfferVersionCommonResource<D extends OfferVersionCommonDTO, E extends OfferVersionCommon> extends AbstractResource<OfferVersionCloneDTO>{

    protected OfferVersionCommonService<E, D> offerVersionCommonService;

    @Autowired
    private ReferTableRepository referTableRepository;

    protected OfferCommonService offerCommonService;

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> createVersion(@NotNull @RequestBody @Validated(OfferVersionCommonDTO.Create.class) D offerVersionCommonDTO) {
        if(offerVersionCommonDTO.getId() != null) {
            throw new DataConstrainException(ErrorMessage.OfferVersion.MUST_BE_NULL, offerVersionCommonService.getResourceName(), ErrorKey.ID);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(offerVersionCommonService.create(offerVersionCommonDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateVersion(@NotNull @RequestBody @Validated(OfferVersionCommonDTO.Update.class) D offerVersionCommonDTO,
            @Validated(OfferVersionCommonDTO.Update.class) @PathVariable("id") @Positive Long id) {
        if(id == null) {
            throw new DataConstrainException(ErrorMessage.OfferVersion.MUST_NOT_BE_NULL, offerVersionCommonService.getResourceName(), ErrorKey.ID);
        }
        offerVersionCommonDTO.setId(id);
        return ResponseEntity.ok().body(offerVersionCommonService.update(offerVersionCommonDTO));
    }

    @GetMapping("/list-states")
    public ResponseEntity<List<ReferTable>> getListDropDownState(){
        return ResponseEntity.ok().body(referTableRepository.findByReferTypeOrderByValueAsc(ReferType.State));
    }

    @GetMapping("/list-special-methods")
    public ResponseEntity<List<ReferTable>> getListDropDownSpecialMethod(){
        return ResponseEntity.ok().body(referTableRepository.findByReferTypeOrderByValueAsc(ReferType.SpecialMethod));
    }

    @GetMapping("/{id}")
    public ResponseEntity<D> getVersionDetail(@NotNull @PathVariable Long id) {
        return ResponseEntity.ok().body(offerVersionCommonService.getVersionDetail(id));
    }

    @PostMapping("/{id}/clone")
    public ResponseEntity<OCSCloneableEntity> cloneRatingFilter(@PathVariable("id") @Positive Long id,
                                                                @RequestBody @Valid OfferVersionCloneDTO cloneDTO) {
        cloneDTO.setId(id);
        List<CloneDTO> cloneDTOs = new ArrayList<>();
        cloneDTOs.add(cloneDTO);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<OCSCloneableEntity> results = ocsCloneService.cloneEntities(cloneDTOs, oldNewChildMap);
        ocsCloneService.clearCache();
        return ResponseEntity.status(HttpStatus.CREATED).body(results.get(0));
    }

    @GetMapping("/list-child-actions/{offerVersionId}")
    public ResponseEntity<List<ActionDTO>> getTreeOffer(
        @PathVariable(value = "offerVersionId") @NotNull @Positive Long offerId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
            Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.ID));
        List<ActionDTO> list = offerVersionCommonService.getListAction(offerId, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("{parentId}/check-has-active-version")
    public ResponseEntity<Boolean> checkHasActiveVersion(@PathVariable(value = "parentId") Long parentId) {
        return ResponseEntity.ok().body(offerVersionCommonService.checkHasVersionActive(parentId));
    }

    @GetMapping("/list-child/{offerId}")
    public ResponseEntity<List<OfferVersionCommonDTO>> getTreeOfferVersions(
        @PathVariable(value = "offerId") @NotNull @Positive Long offerId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
            Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.ID));
        List<OfferVersionCommonDTO> list = offerCommonService.getListOfferVersion(offerId, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-versions/{offerId}")
    public ResponseEntity<Page<OfferVersionCommonDTO>> getVersions(
        @PathVariable(value = "offerId") @NotNull @Positive Long offerId,
        @RequestParam(value = "id", required = false) String id, Pageable pageable) {
        pageable = validatePageable(pageable);

        Page<OfferVersionCommonDTO> page = offerCommonService.getListOfferVersion(offerId, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferVersionSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.ID) : sort);
    }
}
