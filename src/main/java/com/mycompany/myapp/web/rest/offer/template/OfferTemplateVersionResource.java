package com.mycompany.myapp.web.rest.offer.template;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.TreeClone;
import com.mycompany.myapp.dto.action.ActionDTO;
import com.mycompany.myapp.dto.offer.common.MaxNumberDTO;
import com.mycompany.myapp.dto.offer.common.OfferRelationshipCustomDTO;
import com.mycompany.myapp.dto.offer.common.OfferVersionCommonDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionRedirectionDTO;
import com.mycompany.myapp.repository.ReferTableRepository;
import com.mycompany.myapp.repository.offer.common.OfferVersionCommonRepository;
import com.mycompany.myapp.repository.offer.offer.OfferRepository;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.service.offer.offer.OfferVersionService;
import com.mycompany.myapp.service.offer.template.OfferTemplateService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.Resources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersion;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.offer.OfferDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionDTO;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.offer.common.OfferVersionCommonResource;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/offer-template-versions")
public class OfferTemplateVersionResource extends OfferVersionCommonResource<OfferTemplateVersionDTO, OfferTemplateVersion> {

    @Autowired
    private OfferTemplateVersionService offerTemplateVersionService;

    @Autowired
    private OfferTemplateService offerTemplateService;

    @Autowired
    private ReferTableRepository referTableRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferVersionService offerVersionService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.offerVersionCommonService = offerTemplateVersionService;
        this.ocsCloneService = offerTemplateVersionService;
        this.childMappingService = null;
        this.offerCommonService = offerTemplateService;
    }

    @Override
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> createVersion(@NotNull @RequestBody @Validated(OfferVersionCommonDTO.Create.class) OfferTemplateVersionDTO offerTemplateVersionDTO) {
        offerTemplateVersionDTO.setNumber(offerTemplateVersionService.getMaxNumberOfVersions(offerTemplateVersionDTO.getNumber(),offerTemplateVersionDTO.getOfferTemplateId()));
        return super.createVersion(offerTemplateVersionDTO);
    }

    @GetMapping("/{offerTemplateId}/max-number")
    public ResponseEntity<MaxNumberDTO> getMaxNumber(@NotNull @PathVariable Long offerTemplateId) {
        return ResponseEntity.ok().body(offerTemplateVersionService.getMaxNumberOfVersion(offerTemplateId));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteOfferTemplateVersion(@NotNull @PathVariable Long id) {
        offerTemplateVersionService.deleteOfferTemplateVersion(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/{versionId}/create-offer")
    public ResponseEntity<BaseResponseDTO> create(@NotNull @RequestBody OfferDTO dto, @PathVariable Long versionId) {
        return ResponseEntity.ok().body(offerTemplateVersionService.createOffer(dto, versionId));
    }

    @PostMapping("/validate-offer")
    public ResponseEntity validateOffer(@Validated(OfferDTO.Validate.class) @NotNull @RequestBody OfferDTO offerDTO) {
        Integer domainId = OCSUtils.getDomain();
        if (offerDTO.getEffDate().after(offerDTO.getExpDate())) {
            throw new DataInvalidException(ErrorMessage.Offer.EFFECT_DATE_LESS_THAN_EXPIRE_DATE, Resources.OFFER, ErrorKey.Offer.EFF_DATE);
        }
        if (offerDTO.getId() == null || offerDTO.getId() == 0) {
            Offer offer = offerRepository.findByDomainIdAndExternalId(domainId, offerDTO.getExternalId());
            if (offer != null) {
                throw new DataConstrainException(ErrorMessage.Offer.DUPLICATED_EXTERNAL_ID, Resources.OFFER, ErrorKey.Offer.EXTERNAL_ID);
            }
        }
        Offer offers = offerRepository.findByNameAndDomainIdCheck(offerDTO.getName(), domainId);
        if (offers != null) {
            throw new DataConstrainException(ErrorMessage.Offer.DUPLICATED_NAME, Resources.OFFER,
                ErrorKey.Offer.NAME);
        }
        Set<OfferRelationshipCustomDTO> setChilds = new HashSet<>();
        setChilds.addAll(offerDTO.getListChilds());
        if (setChilds.size() != offerDTO.getListChilds().size()) {
            throw new DataInvalidException(ErrorMessage.OfferVersion.DUPLICATED_CHILD_ROW_ID, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.Offer.ID);
        }
        Set<OfferRelationshipCustomDTO> setParents = new HashSet<>();
        setParents.addAll(offerDTO.getListParents());
        if (setParents.size() != offerDTO.getListParents().size()) {
            throw new DataInvalidException(ErrorMessage.OfferVersion.DUPLICATED_PARENT_ROW_ID, Resources.OFFER_TEMPLATE_VERSION, ErrorKey.Offer.ID);
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/validate-url")
    public ResponseEntity validateUrl(@Valid @NotNull @RequestBody OfferVersionRedirectionDTO offerVersionRedirectionDTO) {
        ReferTable referTable = referTableRepository.findByReferTypeAndValue(ReferType.RedirectionType, offerVersionRedirectionDTO.getRedirectionType());
        if (referTable == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReferTable.NOT_FOUND, Resources.REFER_TABLE, ErrorKey.ReferTable.VALUE);
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/validate-offer-version")
    public ResponseEntity validateOfferVersion(@Validated(OfferVersionDTO.Validate.class) @NotNull @RequestBody OfferVersionDTO offerVersionDTO) {
        ReferTable referTable = referTableRepository.findByReferTypeAndValue(ReferType.SpecialMethod, offerVersionDTO.getSpecialMethod());
        if (referTable == null) {
            throw new ResourceNotFoundException(ErrorMessage.ReferTable.NOT_FOUND, Resources.REFER_TABLE, ErrorKey.ReferTable.VALUE);
        }
        if (offerVersionDTO.getEffDate().after(offerVersionDTO.getExpDate()) || offerVersionDTO.getEffDate().equals(offerVersionDTO.getExpDate())) {
            throw new DataInvalidException(ErrorMessage.Offer.EFFECT_DATE_LESS_THAN_EXPIRE_DATE, Resources.OFFER_VERSION, ErrorKey.Offer.EFF_DATE);
        }
        return ResponseEntity.ok().build();
    }
}
