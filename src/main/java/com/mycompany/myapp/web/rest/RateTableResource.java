package com.mycompany.myapp.web.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.ratetable.RateTable;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.formula.FormulaDTO;
import com.mycompany.myapp.dto.normailizer.NormalizerDTO;
import com.mycompany.myapp.dto.ratetable.DropDownCustomDTO;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.repository.UnitTypeRepository;
import com.mycompany.myapp.service.ratetable.RateTableRowService;
import com.mycompany.myapp.service.ratetable.RateTableService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

@RestController
@IgnoreWildCard
@RequestMapping("/api/rate-tables")
public class RateTableResource extends AbstractResource {

    @Autowired
    private RateTableService rateTableService;

    @Autowired
    private UnitTypeRepository unitTypeRepository;

    @Autowired
    private RateTableRowService rateTableRowService;

    @GetMapping("/get-list-unit-types")
    public List<DropDownCustomDTO> getLstUnitType() {
        return unitTypeRepository.getLstUnitType();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteRateTable(
            @PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        rateTableService.deleteRateTable(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> addRateTable(
            @Validated(RateTableDTO.Create.class) @RequestBody RateTableDTO rateTableDTO) {
        rateTableDTO.setId(null);
        BaseResponseDTO base = rateTableService.createRateTable(rateTableDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(base);
    }

    @GetMapping("{rateTableId}/default-formula/{formulaId}")
    public ResponseEntity<String> getDefaultFormula(@PathVariable Long formulaId, @PathVariable Long rateTableId) {
       return ResponseEntity.ok().body(rateTableService.getDefaultFormula(formulaId, rateTableId));
    }

    @PutMapping("/convert-formula")
    public ResponseEntity<String> convertFormula(@Validated(FormulaDTO.Create.class) @RequestBody FormulaDTO formulaDTO) {
        return ResponseEntity.ok().body(rateTableService.convertDefaultFormula(formulaDTO, 1));
    }

    @GetMapping("/{id}")
    public ResponseEntity<RateTableDTO> getRateTable(@PathVariable Long id) {
        RateTableDTO dto = rateTableService.getRateTableDetail(id);
        return ResponseEntity.ok().body(dto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RateTableDTO> updateRateTable(
            @Validated(RateTableDTO.Update.class) @RequestBody RateTableDTO rateTableDTO,
            @PathVariable Long id) {
        rateTableDTO.setId(id);
        RateTableDTO dto = rateTableService.updateRateTable(rateTableDTO);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/search-rate-tables")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getRateTableTree(@RequestParam(required = false) String name,
            @RequestParam(required = false) Long ratingFilterId, @RequestParam(required = false) Long blockId,
            @RequestParam(required = false) Integer componentType, @RequestParam(required = false) Boolean isPopup) {
    	List<Long> removeIds = new ArrayList<>();
        if (ratingFilterId != null) {
            removeIds = rateTableService.getRemoveIds(ratingFilterId, blockId, componentType);
        }
        if (blockId != null && componentType != null) {
            removeIds = rateTableService.getRemoveIds(ratingFilterId, blockId, componentType);
        }
        List<Tree> list = rateTableService.searchInTree(name.trim(), CategoryType.RATE_TABLE, removeIds, isPopup);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-rate-tables/{categoryId}")
    public ResponseEntity<Page<RateTableDTO>> getRateTables(@PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "description", required = false) String description,
            Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<RateTableDTO> dtos = rateTableService.getRateTables(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getRateTableSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, RateTable.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<RateTableDTO>> getRateTables(@PathVariable Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, RateTable.FieldNames.POS_INDEX));
        List<RateTableDTO> dtos = rateTableService.getRateTables(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child-normalizers/{rateTableId}")
    public ResponseEntity<List<NormalizerDTO>> getNormalizers(@PathVariable Long rateTableId) {
        List<NormalizerDTO> dtos = rateTableService.getNormalizers(rateTableId);
        return ResponseEntity.ok().body(dtos);
    }

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = rateTableService;
        this.childMappingService = rateTableRowService;
    }
}
