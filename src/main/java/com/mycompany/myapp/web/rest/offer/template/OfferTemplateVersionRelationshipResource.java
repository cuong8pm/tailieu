package com.mycompany.myapp.web.rest.offer.template;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionRelationship;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionRelationshipDTO;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionRelationshipService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.offer.common.OfferRelationshipCommonResource;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-template-version-relationships")
public class OfferTemplateVersionRelationshipResource extends OfferRelationshipCommonResource<OfferTemplateVersionRelationshipDTO, OfferTemplateVersionRelationship> {

    @Autowired
    private OfferTemplateVersionRelationshipService offerTemplateVersionRelationshipService;

    @PostConstruct
    @Override
    public void setCommonService() {
        this.commonService = offerTemplateVersionRelationshipService;
    }

}
