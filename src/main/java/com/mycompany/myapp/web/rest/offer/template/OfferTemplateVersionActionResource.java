package com.mycompany.myapp.web.rest.offer.template;

import com.mycompany.myapp.domain.action.Action;
import com.mycompany.myapp.domain.offer.template.OfferTemplateVersionAction;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.dto.offer.template.OfferTemplateVersionActionDTO;
import com.mycompany.myapp.service.offer.template.OfferTemplateVersionActionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.offer.common.VersionActionCommonResource;
import liquibase.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Iterator;
import java.util.List;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-template-version-actions")
public class OfferTemplateVersionActionResource extends VersionActionCommonResource<OfferTemplateVersionActionDTO, OfferTemplateVersionAction> {
    @Autowired
    private OfferTemplateVersionActionService offerTemplateVersionActionService;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerTemplateVersionActionService;
    }

    @PutMapping("/move")
    public ResponseEntity<Void> move(@RequestBody @Size(min = 2, max = 2) List<OfferTemplateVersionActionDTO> offerTemplateVersionActionDTOS,@NotNull @RequestParam Long parentId) {
        if (offerTemplateVersionActionDTOS.size() != 2) {
            throw new BadRequestAlertException("Bad request", " quantity must be 2.", "");
        }
        offerTemplateVersionActionService.moveVersionAction(offerTemplateVersionActionDTOS, parentId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/add-child/{offerVersionId}")
    public ResponseEntity<BaseResponseDTO> add(@NotNull @Positive @PathVariable Long offerVersionId,
                                               @Validated(OfferTemplateVersionActionDTO.Create.class) @RequestBody OfferTemplateVersionActionDTO offerTemplateVersionActionDTO,
                                               @RequestParam Integer posIndex) {
        return ResponseEntity.status(HttpStatus.CREATED).body(offerTemplateVersionActionService.save(offerTemplateVersionActionDTO, offerVersionId, posIndex));
    }

    @PostMapping("/{offerVersionId}")
    public ResponseEntity createOfferVersionAction(@NotNull @Positive @PathVariable Long offerVersionId,
                                                   @Validated(OfferTemplateVersionActionDTO.Create.class) @RequestBody List<OfferTemplateVersionActionDTO> offerTemplateVersionActionDTOS) {
        offerTemplateVersionActionService.create(offerTemplateVersionActionDTOS, offerVersionId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity updateOfferVersionAction(@NotNull @Positive @PathVariable Long id,
                                                   @Validated(OfferTemplateVersionActionDTO.Update.class) @RequestBody OfferTemplateVersionActionDTO offerTemplateVersionActionDTO) {
        offerTemplateVersionActionDTO.setId(id);
        offerTemplateVersionActionService.update(offerTemplateVersionActionDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/{offerTemplateVersionId}")
    public ResponseEntity<Page<OfferActionDTO>> getListOfferVersionAction(@NotNull @Positive @PathVariable Long offerTemplateVersionId, Pageable pageable) {
        pageable = validatePageable(pageable);
        return ResponseEntity.ok().body(offerTemplateVersionActionService.getListOfferTemplateVersionAction(offerTemplateVersionId, pageable));
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = OCSUtils.getOfferTemplateVersionActionSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, "posIndex") : sort);
    }
}
