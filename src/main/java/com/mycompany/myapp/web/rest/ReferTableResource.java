package com.mycompany.myapp.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.service.ReferTableService;
import com.mycompany.myapp.utils.IgnoreWildCard;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/refer-table")
public class ReferTableResource {
    
    @Autowired
    private ReferTableService referTableService;
    
    @GetMapping("/list-refer-table/{type}")
    public ResponseEntity<List<ReferTable>>getReferTables(@PathVariable(value = "type") Integer type) {
        return ResponseEntity.ok().body(referTableService.getReferTables(type));
    }
    
    @GetMapping("/list-refer-table")
    public ResponseEntity<List<ReferTable>>getReferTables() {
        return ResponseEntity.ok().body(referTableService.getReferTables(ReferType.CharSpecType));
    }
    
    @GetMapping("/list-refer-table-value")
    public ResponseEntity<List<ReferTable>>findReferTables() {
        return ResponseEntity.ok().body(referTableService.getReferTables(ReferType.ValueType));
    }

}
