package com.mycompany.myapp.web.rest.eventProcessing;

import com.mycompany.myapp.dto.eventProcessing.ExpressionConditionDTO;
import com.mycompany.myapp.dto.eventProcessing.ExpressionDTO;
import com.mycompany.myapp.dto.eventProcessing.FunctionDTO;
import com.mycompany.myapp.service.eventProcessing.ExpressionService;
import com.mycompany.myapp.service.eventProcessing.FunctionService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("api/expressions")
public class ExpressionResource extends AbstractResource {
    @Autowired
    private ExpressionService expressionService;

    @Autowired
    private FunctionService functionService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = expressionService;
        this.ocsCloneService = expressionService;
    }

    public ResponseEntity<Page<ExpressionConditionDTO>> getListExpressionByFilterId(Long filterId, Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<ExpressionConditionDTO> page = expressionService.getListExpressionByFilterId(filterId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    public ResponseEntity<ExpressionDTO> createExpression(Long filterId, ExpressionDTO expressionDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(expressionService.createExpression(filterId, expressionDTO));
    }

    public ResponseEntity<ExpressionDTO> getDetailExpression(Long filterId, Long expressionId) {
        return ResponseEntity.ok().body(expressionService.getDetailExpression(filterId, expressionId));
    }

    public ResponseEntity<?> deleteExpression(Long filterId, Long expressionId) {
        expressionService.deleteExpression(filterId, expressionId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    public ResponseEntity<ExpressionDTO> updateExpression(Long filterId, Long expressionId, ExpressionDTO expressionDTO) {
        expressionDTO.setId(expressionId);
        return ResponseEntity.status(HttpStatus.CREATED).body(expressionService.updateExpression(filterId, expressionDTO));
    }

    public ResponseEntity<FunctionDTO> getDetailFunction(Long expressionId, Long functionId) {
        expressionService.findExpressionByExpressionIdAndLinkedId(expressionId, functionId);
        return ResponseEntity.ok().body(functionService.getFunctionByExpression(expressionId, functionId));
    }

    public ResponseEntity<List<ExpressionConditionDTO>> getListExpressionByFilterIdNoPaging(Long filterId) {
        Pageable unpage = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<ExpressionConditionDTO> page = expressionService.getListExpressionByFilterId(filterId, unpage).getContent();
        return ResponseEntity.ok().body(page);
    }

    public ResponseEntity<ExpressionDTO> getExpressionFunctionDetail(Long id,Integer type) {
        return ResponseEntity.ok(expressionService.getExpressionFunctionDetail(id, type));
    }
}
