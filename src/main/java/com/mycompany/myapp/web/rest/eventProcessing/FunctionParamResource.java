package com.mycompany.myapp.web.rest.eventProcessing;

import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.domain.eventProcessing.FunctionParam;
import com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO;
import com.mycompany.myapp.service.eventProcessing.FunctionParamService;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@IgnoreWildCard
@RequestMapping("api/function-params")
public class FunctionParamResource extends AbstractResource {

    @Autowired
    private FunctionParamService functionParamService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = functionParamService;
    }

    public ResponseEntity<FunctionParamDTO> createFunctionParam(FunctionParamDTO dto, Long functionId) {
        dto.setFunctionId(functionId);
        return ResponseEntity.status(HttpStatus.CREATED).body(functionParamService.createFunctionParam(dto));
    }

    public ResponseEntity<FunctionParamDTO> updateFunctionParam(FunctionParamDTO dto, Long functionParamId, Long functionId) {
        dto.setFunctionId(functionId);
        dto.setId(functionParamId);
        return ResponseEntity.status(HttpStatus.CREATED).body(functionParamService.updateFunctionParam(dto));
    }

    public ResponseEntity<?> changeOrderIndex(Long functionId, List<Long> ids) {
        if (ids.size() != 2) {
            throw new BadRequestAlertException(ErrorMessage.QUANTITY_INVALID, Resources.FUNCTION_PARAM, "");
        }
        functionParamService.changeOrderIndex(ids, functionId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public ResponseEntity<?> deleteFunctionParam(Long functionParamId, Long functionId) {
        functionParamService.deleteFunctionParam(functionId, functionParamId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public ResponseEntity<Page<FunctionParamDTO>> getFunctions(Long functionId, String name, Pageable pageable) {
        pageable = pageableValidation(pageable);
        if(name == null){
            name = "";
        }
        Page<FunctionParamDTO> page = functionParamService.getFunctionParamsWithPaging(functionId, name, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getFunctionParamSearchParam(order.getProperty());
    }

    @Override
    public String getSortFieldName(){
        return FunctionParam.FieldNames.ORDER_INDEX;
    }

    public ResponseEntity<FunctionParamDTO> getDetailFunctionParam(Long functionId, Long functionIdParam) {
        return ResponseEntity.ok().body(functionParamService.getDetailFunctionParam(functionId,functionIdParam));
    }

}
