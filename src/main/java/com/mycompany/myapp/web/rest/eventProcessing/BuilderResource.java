package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Builder;
import com.mycompany.myapp.domain.eventProcessing.BuilderType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.*;
import com.mycompany.myapp.mapper.eventProcessing.BuilderTypeMapper;
import com.mycompany.myapp.repository.eventProcessing.BuilderTypeRepository;
import com.mycompany.myapp.service.eventProcessing.*;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.util.SetUtils;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@IgnoreWildCard
@RequestMapping("/api/builders")
public class BuilderResource extends AbstractResource {

    @Override
    @PostConstruct
    protected void setBaseService() {
        this.ocsBaseService = builderService;
        this.ocsCloneService = builderService;
    }

    @Autowired
    private BuilderService builderService;

    @Autowired
    private NotifyService notifyService;

    @Autowired
    private BuilderItemService builderItemService;

    @Autowired
    private BuilderTypeMapper builderTypeMapper;

    @Autowired
    private BuilderTypeRepository builderTypeRepository;

    @Autowired
    private BuilderTypeItemService builderTypeItemService;

    @Autowired
    private LanguageService languageService;

    @GetMapping("/{id}")
    public ResponseEntity<BuilderDTO> getBuilder(@PathVariable(name = "id") @NotNull @Positive Long id) {
        BuilderDTO dto = builderService.getBuilderDetail(id);
        return ResponseEntity.ok().body(dto);
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> addBuilder(@Validated(BuilderDTO.Create.class) @RequestBody BuilderDTO builderDTO) {
        builderDTO.setId(null);
        BaseResponseDTO base = builderService.createBuilder(builderDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(base);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BuilderDTO> updateBuilder(
        @Validated(BuilderDTO.Update.class) @RequestBody BuilderDTO builderDTO,
        @PathVariable Long id
    ) {
        builderDTO.setId(id);
        BuilderDTO dto = builderService.updateBuilder(builderDTO);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteBuilder(@PathVariable(name = "id") @NotNull @Positive Long id) {
        builderService.deleteBuilder(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-child-builders/{categoryId}")
    public ResponseEntity<Page<BuilderDTO>> getBuilders(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<BuilderDTO> dtos = builderService.getBuilders(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<BuilderDTO>> getBuilders(@PathVariable Long categoryId) {
        Pageable uppaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Builder.FieldNames.POS_INDEX));
        List<BuilderDTO> dtos = builderService.getBuilders(categoryId, null, null, uppaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/search-builders")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBuilderTree(@RequestParam(required = false, defaultValue = "") String name, @RequestParam(required = false) Long type) {
        List<Tree> list = builderService.searchInTree(name.trim(), CategoryType.BUILDER, type);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-builder-types")
    public ResponseEntity<List<BuilderTypeDTO>> getBuilderType() {
        List<BuilderType> builderTypes = builderTypeRepository.findAll();
        List<BuilderTypeDTO> builderTypeDTOS = new ArrayList<>();
        for (BuilderType builderType : builderTypes) {
            BuilderTypeDTO builderTypeDTO = builderTypeMapper.toDto(builderType);
            builderTypeDTOS.add(builderTypeDTO);
        }
        return ResponseEntity.ok().body(builderTypeDTOS);
    }

    @PostMapping("/condition-clone")
    public ResponseEntity<List<OCSCloneableEntity>> cloneListBuilder(@RequestBody @Valid List<Long> ids) {
        List<OCSCloneableEntity> results = builderService.cloneableEntities(ids);
        return ResponseEntity.status(HttpStatus.CREATED).body(results);
    }

    @Override
    public String getSortFieldName() {
        return Builder.FieldNames.POS_INDEX;
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getBuilderSearchParam(order.getProperty());
    }

    //notify
    @GetMapping("/{builderId}/notifies")
    public ResponseEntity<Page<NotifyDTO>> getNotifies(
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId,
        Pageable pageable
    ) {
        Page<NotifyDTO> notifyDTOPage = notifyService.getNotifies(builderId, pageable);
        return ResponseEntity.ok().body(notifyDTOPage);
    }

    @PostMapping("/{builderId}/notifies")
    public ResponseEntity<NotifyDTO> createNotify(
        @Validated(NotifyDTO.Create.class) @RequestBody NotifyDTO notifyDTO,
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId
    ) {
        notifyDTO.setId(null);
        NotifyDTO dto = notifyService.createNotify(notifyDTO, builderId);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @PutMapping("/{builderId}/notifies/{notifyId}")
    public ResponseEntity<NotifyDTO> updateNotify(
        @Validated(NotifyDTO.Update.class) @RequestBody NotifyDTO notifyDTO,
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId,
        @PathVariable(name = "notifyId") @NotNull @Positive Long notifyId
    ) {
        notifyDTO.setId(notifyId);
        NotifyDTO dto = notifyService.updateNotify(notifyDTO, builderId);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping("/{builderId}/notifies/{notifyId}")
    public ResponseEntity<BaseResponseDTO> deleteNotify(@PathVariable(name = "notifyId") @NotNull @Positive Long id) {
        notifyService.deleteNotify(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/notify/language")
    public ResponseEntity<List<LanguageDTO>> getLanguage() {
        List<LanguageDTO> languages = languageService.getLanguages();
        return ResponseEntity.ok().body(languages);
    }

    //builder-item
    @GetMapping("/{builderId}/builder-item/{builderItemId}")
    public ResponseEntity<BuilderItemDTO> getBuilderItemDetail(
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId,
        @PathVariable(name = "builderItemId") @NotNull @Positive Long builderItemId
    ) {
        BuilderItemDTO builderItemDTO = builderItemService.getBuilderItemDetail(builderId, builderItemId);
        return ResponseEntity.ok().body(builderItemDTO);
    }

    @GetMapping("/{builderId}/builder-items")
    public ResponseEntity<Page<BuilderItemDTO>> getBuilderItems(
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<BuilderItemDTO> builderItemDTOPage = builderItemService.getBuilderItemsByBuilderId(builderId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), builderItemDTOPage);
        return ResponseEntity.ok().headers(headers).body(builderItemDTOPage);
    }

    @PostMapping("/{builderId}/builder-items")
    public ResponseEntity<BuilderItemDTO> createBuilderItems(
        @Validated(BuilderItemDTO.Create.class) @RequestBody BuilderItemDTO builderItemDTO,
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId
    ) {
        builderItemDTO.setId(null);
        BuilderItemDTO dto = builderItemService.createBuilderItem(builderItemDTO, builderId);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @PutMapping("/{builderId}/builder-items/{builderItemId}")
    public ResponseEntity<BuilderItemDTO> updateBuilderItems(
        @Validated(BuilderItemDTO.Update.class) @RequestBody BuilderItemDTO builderItemDTO,
        @PathVariable(name = "builderId") @NotNull @Positive Long builderId,
        @PathVariable(name = "builderItemId") @NotNull @Positive Long notifyId
    ) {
        builderItemDTO.setId(notifyId);
        BuilderItemDTO dto = builderItemService.updateBuilderItem(builderItemDTO, builderId);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping("/{builderId}/builder-items/{builderItemId}")
    public ResponseEntity<BaseResponseDTO> deleteBuilderItems(@PathVariable(name = "builderItemId") @NotNull @Positive Long id) {
        builderItemService.deleteBuilderItem(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/builder-items-value-types")
    public ResponseEntity<List<ReferTable>> getBuilderItemValueType() {
        return ResponseEntity.ok().body(builderItemService.getBuilderItemValueTypes());
    }

    @GetMapping("/builder-items-data-types")
    public ResponseEntity<List<ReferTable>> getBuilderItemDataType() {
        return ResponseEntity.ok().body(builderItemService.getBuilderItemDataTypes());
    }

    @GetMapping("/{builderId}/builder-items/{builderTypeId}")
    public ResponseEntity<List<BuilderItemDTO>> getListBuilderItemByBuilderTypeId(@PathVariable("builderTypeId") Long builderTypeId) {
        List<BuilderItemDTO> builderItemDTOList = builderItemService.getListBuilderItemByBuilderTypeItemId(builderTypeId);
        return ResponseEntity.ok().body(builderItemDTOList);
    }

    @GetMapping("/{builderId}/builder-types/{builderTypeId}")
    public ResponseEntity<List<BuilderTypeItemDTO>> getListBuilderTypeItemByBuilderTypeId(
        @PathVariable("builderId") @Positive @NotNull Long builderId,
        @PathVariable("builderTypeId") @Positive @NotNull Long builderTypeId
    ) {
        List<BuilderTypeItemDTO> builderTypeItemDTOS = builderTypeItemService.getListBuilderTypeItemDTOByBuilderTypeId(builderId, builderTypeId);
        return ResponseEntity.ok().body(builderTypeItemDTOS);
    }

    public ResponseEntity<Page<BuilderDTO>> getListBuilderByEventPolicyIdPaging(Long eventPolicyId,Pageable pageable) {
        pageable = pageableValidation(pageable);
        Page<BuilderDTO> page = builderService.getBuilderByEventPolicyId(eventPolicyId,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @DeleteMapping("")
    public ResponseEntity<BaseResponseDTO> deleteBuilderByIds(@RequestParam("ids") List<Long> ids) {
        builderService.deleteBuilderByIds(ids);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
    @JsonView(Tree.class)
    @GetMapping("/{id}/dependencies")
    @Override
    public ResponseEntity<Map<String, Set<CategoryDTO>>> checkDependencies(@PathVariable Long id) {
        ResponseEntity<Map<String, Set<CategoryDTO>>> result = super.checkDependencies(id);
        Set<CategoryDTO> depend = result.getBody().get("depend");
        if(!SetUtils.isEmpty(depend)) {
            for (CategoryDTO categoryDTO : depend) {
                if (categoryDTO.getName().equals("Conditions")) {
                    categoryDTO.setName("Condition");
                }
                if (categoryDTO.getName().equals("Filters")) {
                    categoryDTO.setName("Filter");
                }
            }
        }
        return result;
    }
}
