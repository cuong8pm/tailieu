package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.domain.constant.AffectedObjectType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.DropdownType;
import com.mycompany.myapp.domain.constant.ReferType;
import com.mycompany.myapp.dto.*;
import com.mycompany.myapp.mapper.BalTypeMapper;
import com.mycompany.myapp.service.BalancesService;
import com.mycompany.myapp.service.BalanceAcmMappingService;
import com.mycompany.myapp.service.AccountBalanceMappingService;
import com.mycompany.myapp.service.ThresholdService;
import com.mycompany.myapp.service.exception.*;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/balances")
public class BalancesResource {
    private final Logger log = LoggerFactory.getLogger(BalancesResource.class);
    @Autowired
    protected BalancesService balancesService;

    @Autowired
    protected ThresholdService thresholdService;

    @Autowired
    protected AccountBalanceMappingService accountBalanceMappingService;

    @Autowired
    protected BalanceAcmMappingService balanceAcmMappingService;

    @Autowired
    protected BalTypeMapper balTypeMapper;

    @GetMapping("/list-child/{categoryId}")
    @JsonView(Tree.class)
    public ResponseEntity<List<BalancesDTO>> getBalTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, BalType.FieldNames.POS_INDEX));

        List<BalancesDTO> list = balancesService.getBalTypes(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-balances/{categoryId}")
    public ResponseEntity<Page<BalancesDTO>> getBalTypes(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);

        Page<BalancesDTO> page = balancesService.getBalTypes(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<BalancesDTO> getBalTypeDetail(@PathVariable(value = "id") @NotNull @Positive Long id){
        BalancesDTO balancesDTO = balancesService.getBalTypeDetail(id);
        return ResponseEntity.ok().body(balancesDTO);
    }

    // Detail Threshold
    @GetMapping("/{balanceId}/threshold/{thresholdId}")
    public ResponseEntity<ThresholdsDTO> getThresholdDetail(@PathVariable(value = "balanceId") @NotNull @Positive Long balanceId,
                                                          @PathVariable(value = "thresholdId") @NotNull @Positive Long thresholdId){
        ThresholdsDTO thresholdsDTO = thresholdService.getThresholdDetail(balanceId,thresholdId);
        return ResponseEntity.ok().body(thresholdsDTO);
    }

    // Them balance
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addBalType(
        @Validated(BalancesDTO.Create.class) @RequestBody BalancesDTO balType) {
        if (balType.getId() != null) {
            throw new DataInvalidException(ErrorMessage.BalType.INVALID, Resources.BAL_TYPE, BalancesDTO.FieldNames.ID);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(balancesService.saveBalType(balType));
    }

    //Them threshold
    @PostMapping("/{balanceId}/threshold")
    public ResponseEntity<BaseResponseDTO> addThreshold(
        @PathVariable("balanceId") @NotNull @Positive Long balanceId,
        @Validated(ThresholdsDTO.Create.class) @RequestBody ThresholdsDTO thresholds) throws IllegalArgumentException {
        if (thresholds.getId() != null) {
            throw new DataInvalidException(ErrorMessage.Threshold.MUST_BE_NULL, Resources.THRESHOLD, BalancesDTO.FieldNames.ID);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(thresholdService.createThreshold(thresholds, balanceId));
    }

    // Update balance
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateBalType(
        @Validated(BalancesDTO.Update.class) @RequestBody BalancesDTO balancesDTO,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        balancesDTO.setId(id);
        return ResponseEntity.ok().body(balancesService.updateBalType(balancesDTO));
    }

    // Update threshold
    @PutMapping("/{balanceId}/threshold/{thresholdId}")
    public ResponseEntity<BaseResponseDTO> updateThreshold(
        @PathVariable("balanceId") @NotNull @Positive Long balanceId,
        @PathVariable("thresholdId") @NotNull @Positive Long thresholdId,
        @Validated(ThresholdsDTO.Update.class) @RequestBody ThresholdsDTO thresholdsDTO) {
        thresholdsDTO.setId(thresholdId);
        return ResponseEntity.ok().body(thresholdService.updateThreshold(thresholdsDTO, balanceId));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveBalType(
        @Validated(BalancesDTO.Move.class) @RequestBody @Size(min = 2, max = 2) List<BalancesDTO> balType) {
        if (balType.size() != 2) {
            throw new BadRequestAlertException("Bad request", "BalanceType's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(balancesService.move(balType));
    }

    // Delete balance
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteBalType(@PathVariable(name = "id") Long id) {
        balancesService.deleteBalType(id, AffectedObjectType.BALANCE.getValue());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Xoa threshold
    @DeleteMapping("/{balanceId}/threshold/{thresholdId}")
    public ResponseEntity<BaseResponseDTO> deleteThreshold(@PathVariable(name = "balanceId") Long balanceId,
                                                           @PathVariable(name = "thresholdId") Long thresholdId) {
        try{
            thresholdService.deleteThreshold(thresholdId, balanceId);
        }catch (Exception e){
            throw new ResourceNotFoundException(ErrorMessage.CANNOT_DELETE_HAS_CHILD,Resources.THRESHOLD, ErrorKey.Threshold.ID);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping(value = "/balance-and-meters/search-balance-and-meters")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBalTypes(
        @RequestParam(value = "name") String name) {
        name = StringUtils.isEmpty(name) ? null : name.trim();
        List<Tree> balances = balancesService.searchInTree(name, CategoryType.BALANCES);
        balances.addAll(balancesService.searchInTree(name, CategoryType.ACCUMULATE_BALANCES));
        balances.addAll(accountBalanceMappingService.searchInTree(name, CategoryType.ACCOUNT_BALANCE_MAPPING));
        balances.addAll(balanceAcmMappingService.searchInTree(name, CategoryType.ACM_BALANCE_MAPPING));
        return ResponseEntity.ok().body(balances);
    }

    protected Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = OCSUtils.getBalTypeSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, BalType.FieldNames.POS_INDEX) : sort);
    }

    protected Pageable validateThresholdPageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String property = OCSUtils.getThresholdSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(property)) {
                sort = sort == null ? Sort.by(order.getDirection(), property)
                    : sort.and(Sort.by(order.getDirection(), property));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, Threshold.FieldNames.ID) : sort);
    }

    //for load tree
    @GetMapping("/{balanceId}/list-threshold")
    @JsonView(Tree.class)
    public ResponseEntity<List<ThresholdsDTO>> getThresholds(
        @PathVariable(value = "balanceId") @NotNull @Positive Long balanceId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Threshold.FieldNames.ID));
        List<ThresholdsDTO> list = thresholdService.getThresholds(balanceId, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // new api
    @GetMapping("/{balanceId}/threshold")
    @JsonView(Tree.class)
    public ResponseEntity<List<ThresholdsDTO>> getThresholds2(
        @PathVariable(value = "balanceId") @NotNull @Positive Long balanceId) {
        return getThresholds(balanceId);
    }

    //for load table
    @GetMapping("/{balanceId}/table-threshold")
    public ResponseEntity<Page<ThresholdsDTO>> getBalTypes(
        @PathVariable(value = "balanceId") @NotNull @Positive Long balanceId,
        @RequestParam(value = "name", required = false) String name,
        Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        pageable = validateThresholdPageable(pageable);
        Page<ThresholdsDTO> page = thresholdService.getThresholds(balanceId, name, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    //clone
    @PostMapping("/{balanceId}/clone")
    public ResponseEntity<BalancesDTO> cloneBalType(@PathVariable(value = "balanceId") Long balanceId,
        @RequestParam("deep") int deep, @RequestBody @Valid CloneDTO cloneDTO) {
        OCSCloneableEntity balanceDTO = balancesService.cloneBalType(balanceId, deep, cloneDTO.getName());
        BalancesDTO result = balTypeMapper.toDto((BalType) balanceDTO);
        if(BooleanUtils.isTrue(result.isAcm())){
            result.setType(Resources.ACM_BAL_TYPE);
        }
        result.setHasChild(balancesService.checkHasChild(result.getId(), OCSUtils.getDomain()));
        return ResponseEntity.ok().body(result);
    }

    // drop down list
    @GetMapping("/effect-date-type")
    public ResponseEntity<List<ReferTable>> getDDEffectDateType() {
        List<ReferTable> referTables = balancesService.getDDByReferType(ReferType.EffectDataType);
        referTables.add(0,new ReferTable(null,"NONE"));
        return ResponseEntity.ok().body(referTables);
    }
    @GetMapping("/expire-date-type")
    public ResponseEntity<List<ReferTable>> getDDExpireDateType() {
        List<ReferTable> referTables = balancesService.getDDByReferType(ReferType.ExpireDataType);
        referTables.add(0,new ReferTable(null,"NONE"));
        return ResponseEntity.ok().body(referTables);
    }
    @GetMapping("/multi-bal-type")
    public ResponseEntity<List<ReferTable>> getDDMultiBalType() {
        return ResponseEntity.ok().body(balancesService.getDDByReferType(ReferType.MultiBalType));
    }
    @GetMapping("/payment-type")
    public ResponseEntity<List<ReferTable>> getDDPaymentType() {
        return ResponseEntity.ok().body(balancesService.getDDByReferType(ReferType.PaymentType));
    }
    @GetMapping("/owner-level")
    public ResponseEntity<List<ReferTable>> getDDOwnerLevel(
        @RequestParam(name = "category-type") Integer categoryTypeId) {
        CategoryType categoryType = CategoryType.of(categoryTypeId);
        switch (categoryType){
            case BALANCES:
                List<ReferTable> referTables= balancesService.getDDByReferType(ReferType.BalTypeOwnerLevel);
                Optional<ReferTable> referTable = referTables.stream().findFirst().filter(x -> x.getValue().equals(DropdownType.OwnerLevel.MEMBERSHIP));
                referTable.ifPresent(referTables::remove);
                return ResponseEntity.ok().body(referTables);
            case ACCUMULATE_BALANCES:
                return ResponseEntity.ok().body(balancesService.getDDByReferType(ReferType.BalTypeOwnerLevel));
            default:
                throw new DataInvalidException(ErrorMessage.BalType.INVALID, Resources.BAL_TYPE, "type");
        }
    }
    @GetMapping("/periodic-type")
    public ResponseEntity<List<ReferTable>> getDDPeriodicType() {
        return ResponseEntity.ok().body(balancesService.getDDByReferType(ReferType.PeriodicType));
    }

    @GetMapping("/{balanceId}/threshold/threshold-types")
    public ResponseEntity<List<ReferTable>> getDDThresholdType() {
        return ResponseEntity.ok().body(balancesService.getDDByReferType(ReferType.ThresholdType));
    }

    @GetMapping("/balance-acm-tree")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getBalanceTree(@RequestParam("name") String name) {
        name = StringUtils.isEmpty(name) ? null : name;
        List<Tree> list = balancesService.searchInTree(name, CategoryType.ACCUMULATE_BALANCES);
        return ResponseEntity.ok().body(list);
    }
}
