package com.mycompany.myapp.web.rest.offer.common;

import com.mycompany.myapp.domain.offer.common.VersionActionCommon;
import com.mycompany.myapp.dto.common.MoveableDTO;
import com.mycompany.myapp.dto.offer.common.VersionActionCommonDTO;
import com.mycompany.myapp.dto.offer.offer.OfferActionDTO;
import com.mycompany.myapp.dto.offer.offer.OfferVersionActionDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.service.offer.common.VersionActionCommonService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Validated
@RestController
@IgnoreWildCard
@RequestMapping("/api")
public abstract class VersionActionCommonResource<D extends VersionActionCommonDTO, E extends VersionActionCommon> {
    protected VersionActionCommonService<D, E> commonService;

    protected abstract void setCommonService();

    @DeleteMapping("/{parentId}/delete/{id}")
    public ResponseEntity delete(@NotNull @Positive @PathVariable Long parentId, @NotNull @Positive @PathVariable Long id, @NotNull @RequestParam Integer posIndex) {
        commonService.delete(id, posIndex, parentId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
