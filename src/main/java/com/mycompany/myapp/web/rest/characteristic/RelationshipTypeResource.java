package com.mycompany.myapp.web.rest.characteristic;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.dto.characteristic.RelationshipTypeDTO;
import com.mycompany.myapp.service.characteristic.RelationshipTypeService;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.viettel.ocs.slice.domain.ValueType}.
 */
@RestController
@RequestMapping("/api")
public class RelationshipTypeResource {

    private final Logger log = LoggerFactory.getLogger(RelationshipTypeResource.class);

    private static final String ENTITY_NAME = "RelationshipType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RelationshipTypeService relationshipTypeService;

    public RelationshipTypeResource(RelationshipTypeService relationshipTypeService) {
        this.relationshipTypeService = relationshipTypeService;
    }

    /**
     * {@code GET  /value-types} : get all the valueTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of valueTypes in body.
     */
    @GetMapping("/relationship-types")
    public List<RelationshipTypeDTO> getRelationshipTypes() {
        log.debug("REST request to get all RelationshipTypes");
        return relationshipTypeService.findAll();
    }

    /**
     * {@code GET  /value-types/:id} : get the "id" valueType.
     *
     * @param id
     *            the id of the valueTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the valueTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/relationship-types/{id}")
    public ResponseEntity<RelationshipTypeDTO> getRelationshipType(@PathVariable Long id) {
        log.debug("REST request to get RelationshipType : {}", id);
        Optional<RelationshipTypeDTO> relationshipTypeDTO = relationshipTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(relationshipTypeDTO);
    }
}
