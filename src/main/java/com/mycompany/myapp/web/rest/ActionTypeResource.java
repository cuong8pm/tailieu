package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.actiontype.ActionType;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.actiontype.ActionTypeCustomDTO;
import com.mycompany.myapp.dto.actiontype.ActionTypeDTO;
import com.mycompany.myapp.dto.actiontype.EventOfActionTypeDTO;
import com.mycompany.myapp.service.actiontype.ActionTypeService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/action-types")
public class ActionTypeResource extends AbstractResource {

    @Autowired
    private ActionTypeService actionTypeService;

    @GetMapping("/list-all-action-type")
    public ResponseEntity<List<ActionType>> getAllActionType() {
        return ResponseEntity.ok().body(actionTypeService.getAllActionType());
    }

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = actionTypeService;
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> createPriceComponent(@Validated(ActionTypeDTO.Create.class) @RequestBody ActionTypeCustomDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(actionTypeService.createActiontype(dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteActionType(@Valid @PathVariable Long id ) {
        actionTypeService.deleteActionType(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ActionTypeDTO> getActionTypeDetail(@Valid @PathVariable Long id) {
        ActionTypeDTO dto = actionTypeService.getActionTypeDetail(id);
        return ResponseEntity.ok().body(dto);
    }


    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<ActionTypeDTO>> listChild(@Valid @PathVariable(value = "categoryId") Long categoryId , Pageable pageable) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, ActionType.FieldNames.POS_INDEX));
        List<ActionTypeDTO> dtos = actionTypeService.ActionTypes(categoryId, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("/list-child-action-types/{categoryId}")
    public ResponseEntity<Page<ActionTypeDTO>> listChildActionType(@Valid @PathVariable Long categoryId,
            @RequestParam(required = false) String name, @RequestParam(required = false) String description, Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<ActionTypeDTO> dtos = actionTypeService.ActionTypes(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getActionTypeParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, ActionType.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/list-child-event/{actionTypeId}")
    public ResponseEntity<Page<EventOfActionTypeDTO>> listChildEvent(@PathVariable Long actionTypeId, Pageable pageable) {
        Page<EventOfActionTypeDTO> dtos = actionTypeService.listChildEvent(actionTypeId, pageable);
        return ResponseEntity.ok().body(dtos);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateActionType(@PathVariable Long id, @RequestBody @Validated(ActionTypeDTO.Create.class) ActionTypeDTO actionTypeDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(actionTypeService.updateActiontype(actionTypeDTO, id));
    }

    @GetMapping("/search-action-types")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getActionTypeTree(@RequestParam(required = false) String name) {
        List<Tree> list = actionTypeService.searchInTree(name.trim(), CategoryType.ACTION_TYPE);
        return ResponseEntity.ok().body(list);
    }
}
