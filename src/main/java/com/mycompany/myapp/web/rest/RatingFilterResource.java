package com.mycompany.myapp.web.rest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.RatingFilter;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.ratetable.RateTableDTO;
import com.mycompany.myapp.dto.ratingfilter.RatingFilterDTO;
import com.mycompany.myapp.service.OCSBaseService.Keys;
import com.mycompany.myapp.service.ratingfilter.RatingFilterRateTableMapService;
import com.mycompany.myapp.service.ratingfilter.RatingFilterService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

import io.github.jhipster.web.util.PaginationUtil;
import liquibase.util.StringUtils;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/rating-filters")
public class RatingFilterResource extends AbstractResource {
    @Autowired
    private RatingFilterService ratingFilterService;

    @Autowired
    private RatingFilterRateTableMapService filterRateTableMapService;

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteRatingFilter(@PathVariable @NotNull @Positive Long id) {
        ratingFilterService.deleteRatingFilter(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/search-rating-filters")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getRatingFilter(@RequestParam(value = "name", defaultValue = "") String name,
            @RequestParam(required = false) Integer type, @RequestParam(required = false) Boolean isPopup) {
    	Map<String, Object> params = new HashMap<>();
    	params.put(Keys.TYPE, type);
        List<Tree> list = ratingFilterService.searchInTree(name.trim(), CategoryType.RATING_FILTER, null, params, isPopup);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity<BaseResponseDTO> createRatingFilter(@RequestBody @Validated(RatingFilterDTO.Create.class) RatingFilterDTO ratingFilterDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(ratingFilterService.createRatingFilter(ratingFilterDTO));
    }

    @GetMapping("/rating-filter-type")
    public ResponseEntity<List<ReferTable>> getRatingFilterType() {
        return ResponseEntity.ok().body(ratingFilterService.getRatingFilterType());
    }

    @GetMapping("/{id}")
    public ResponseEntity<RatingFilterDTO> getRatingFilterDetail(@PathVariable Long id) {
        return ResponseEntity.ok().body(ratingFilterService.getRatingFilterDetail(id));
    }

    @GetMapping
    public ResponseEntity<List<RatingFilterDTO>> getRatingFilter() {
        return ResponseEntity.ok().body(ratingFilterService.getAllRatingFilter());
    }

    @GetMapping("/list-child-rate-tables/{id}")
    public ResponseEntity<List<RateTableDTO>> getRateTableByRatingFilterId(@PathVariable Long id) {
        return ResponseEntity.ok().body(ratingFilterService.getRateTableByRatingFilterId(id));
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<RatingFilterDTO>> getByCategoryId(@PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, RatingFilter.FieldNames.POS_INDEX));
        List<RatingFilterDTO> list = ratingFilterService.getRatingFilters(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> editRatingFilter(@RequestBody @Validated(RatingFilterDTO.Update.class) RatingFilterDTO ratingFilterDTO, @PathVariable Long id) {
        ratingFilterDTO.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(ratingFilterService.editRatingFilter(ratingFilterDTO));
    }

    @GetMapping("/list-child-rating-filters/{categoryId}")
    public ResponseEntity<Page<RatingFilterDTO>> getRatingFilter(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        pageable = pageableValidation(pageable);

        Page<RatingFilterDTO> page = ratingFilterService.getRatingFilterWithPage(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @DeleteMapping("/{ratingFilterId}/rate-table/{rateTableId}")
    public ResponseEntity<BaseResponseDTO> deleteRateTable(@PathVariable @Positive Long ratingFilterId, @Positive @PathVariable Long rateTableId) {
        filterRateTableMapService.deleteMapping(ratingFilterId, rateTableId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = ratingFilterService;
        this.childMappingService = filterRateTableMapService;
    }

    @Override
    public String getSortFieldName() {
        return RatingFilter.FieldNames.POS_INDEX;
    }
}
