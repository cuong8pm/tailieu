package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.Verification;
import com.mycompany.myapp.domain.eventProcessing.VerificationItem;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.eventProcessing.VerificationDTO;
import com.mycompany.myapp.dto.eventProcessing.VerificationItemDTO;
import com.mycompany.myapp.service.eventProcessing.VerificationItemService;
import com.mycompany.myapp.service.eventProcessing.VerificationService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import io.github.jhipster.web.util.PaginationUtil;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@IgnoreWildCard
@RequestMapping("/api/verifications")
public class VerificationResource extends AbstractResource {
    @Autowired
    private VerificationService verificationService;

    @Autowired
    private VerificationItemService verificationItemService;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsBaseService = this.verificationService;
        this.ocsCloneService = this.verificationService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteVerification(@PathVariable(name = "id") @NotNull @Positive Long id) {
        verificationService.deleteVerification(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("")
    public ResponseEntity<BaseResponseDTO> createVerification(
        @Validated(VerificationDTO.Create.class) @RequestBody VerificationDTO verificationDTO
    ) {
        verificationDTO.setId(null);
        BaseResponseDTO base = verificationService.createVerification(verificationDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(base);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VerificationDTO> getVerification(@PathVariable Long id) {
        VerificationDTO verificationDTO = verificationService.getVerificationDetail(id);
        return ResponseEntity.ok().body(verificationDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<VerificationDTO> updateVerification(
        @Validated(VerificationDTO.Update.class) @RequestBody VerificationDTO verificationDTO,
        @PathVariable Long id
    ) {
        verificationDTO.setId(id);
        VerificationDTO dto = verificationService.updateVerification(verificationDTO);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<VerificationDTO>> getListVerifications(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description
    ) {
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        Page<VerificationDTO> verificationDTOPage = verificationService.getVerifications(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(verificationDTOPage.getContent());
    }

    @GetMapping("/list-child-verifications/{categoryId}")
    public ResponseEntity<Page<VerificationDTO>> getPageVerifications(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = validatePageable(pageable);
        Page<VerificationDTO> verificationDTOPage = verificationService.getVerifications(categoryId, name, description, pageable);
        return ResponseEntity.ok().body(verificationDTOPage);
    }

    @GetMapping("/search-verifications")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getVerificationTree(@RequestParam(required = false) String name) {
        List<Tree> list = verificationService.searchInTree(name.trim(), CategoryType.VERIFICATION);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/{id}/search-verification-items")
    public ResponseEntity<List<VerificationItemDTO>> getVerificationItemByVerificationId(
        @PathVariable(value = "id") Long verificationId,
        @RequestParam(required = false) String item
    ) {
        List<VerificationItemDTO> list = verificationItemService.getVerificationItemByVerificationIdAndItem(verificationId, item);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/{id}/verification-items")
    public ResponseEntity<Page<VerificationItemDTO>> getVerificationItemsByVerificationId(
        @PathVariable(value = "id") Long verificationId,
        @RequestParam(value = "name", required = false) String itemId,
        Pageable pageable
    ) {
        pageable = validatePageableItem(pageable);
        Page<VerificationItemDTO> page = verificationItemService.getVerificationItemByVerificationId(itemId, verificationId, pageable);
        HttpHeaders httpHeaders = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(httpHeaders).body(page);
    }

    @PostMapping("/{id}/verification-items")
    public ResponseEntity<VerificationItemDTO> createVerificationItem(
        @PathVariable(value = "id") @Positive @NotNull Long verificationId,
        @Validated(VerificationItemDTO.Create.class) @RequestBody @NotNull VerificationItemDTO verificationItemDTO
    ) {
        verificationItemDTO.setVerificationId(verificationId);
        VerificationItemDTO verificationItem = verificationItemService.createVerificationItem(verificationItemDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(verificationItem);
    }

    @PutMapping("/{id}/verification-items/{itemId}")
    public ResponseEntity<VerificationItemDTO> updateVerificationItem(
        @Validated(VerificationItemDTO.Update.class) @RequestBody VerificationItemDTO verificationItemDTO,
        @PathVariable(value = "id") @Positive @NotNull Long verificationId,
        @PathVariable(value = "itemId") @Positive @NotNull Long verificationItemId
    ) {
        verificationItemDTO.setId(verificationItemId);
        VerificationItemDTO dto = verificationItemService.updateVerificationItem(verificationItemDTO, verificationId);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping("/{id}/verification-items/{itemId}")
    public ResponseEntity<BaseResponseDTO> deleteVerificationItem(@PathVariable(value = "itemId") Long verificationItemId) {
        verificationItemService.deleteVerificationItem(verificationItemId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/verification-types")
    public ResponseEntity<List<ReferTable>> getVerificationType() {
        return ResponseEntity.ok().body(verificationService.getVerificationTypes());
    }

    @GetMapping("/list-types")
    public ResponseEntity<List<ReferTable>> getListType() {
        return ResponseEntity.ok().body(verificationService.getListTypes());
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getVerificationSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery) : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(
            pageable.getPageNumber(),
            pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, Verification.FieldNames.POS_INDEX) : sort
        );
    }

    private Pageable validatePageableItem(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getVerificationItemKeys(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery) : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(
            pageable.getPageNumber(),
            pageable.getPageSize(),
            sort == null ? Sort.by(Sort.Direction.ASC, VerificationItem.FieldNames.ID) : sort
        );
    }
}
