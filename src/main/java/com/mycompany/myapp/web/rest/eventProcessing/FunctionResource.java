package com.mycompany.myapp.web.rest.eventProcessing;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.OCSCloneableEntity;
import com.mycompany.myapp.domain.ReferTable;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.eventProcessing.BuilderTypeItem;
import com.mycompany.myapp.domain.eventProcessing.Function;
import com.mycompany.myapp.dto.CloneDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.common.MoveableDTO;
import com.mycompany.myapp.dto.eventProcessing.FunctionCloneDTO;
import com.mycompany.myapp.dto.eventProcessing.FunctionDTO;
import com.mycompany.myapp.dto.eventProcessing.FunctionParamDTO;
import com.mycompany.myapp.service.eventProcessing.FunctionParamService;
import com.mycompany.myapp.service.eventProcessing.FunctionService;
import com.mycompany.myapp.service.exception.ErrorMessage;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.AbstractResource;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.errors.Resources;
import io.github.jhipster.web.util.PaginationUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@IgnoreWildCard
@RequestMapping("/api/functions")
public class FunctionResource extends AbstractResource<FunctionCloneDTO> {
    @Autowired
    private FunctionService functionService;

    @Autowired
    private FunctionParamResource functionParamResource;

    @PostConstruct
    @Override
    protected void setBaseService() {
        this.ocsCloneService = functionService;
        this.ocsBaseService = functionService;
    }

    @PostMapping("")
    public ResponseEntity<FunctionDTO> createFunction(@Validated(FunctionDTO.Create.class) @NotNull @RequestBody FunctionDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(functionService.createFunction(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<FunctionDTO> updateFunction(
        @PathVariable(value = "id") @Positive @NotNull Long functionId,
        @Validated(FunctionDTO.Update.class) @NotNull @RequestBody FunctionDTO dto
    ) {
        dto.setId(functionId);
        return ResponseEntity.status(HttpStatus.CREATED).body(functionService.updateFunction(dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFunction(@PathVariable(value = "id") @Positive @NotNull Long functionId) {
        functionService.deleteFunction(functionId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<FunctionDTO> getDetailFunction(@PathVariable(value = "id") @Positive @NotNull Long functionId) {
        return ResponseEntity.ok().body(functionService.getDetailFunction(functionId));
    }

    @GetMapping("/search-functions")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchFunctions(@RequestParam(value = "name", defaultValue = "") String name, @RequestParam(required = false) Long type) {
        return ResponseEntity.ok().body(functionService.searchInTree(name.trim(), CategoryType.FUNCTION, type));
    }

    @GetMapping("list-child/{categoryId}")
    public ResponseEntity<List<FunctionDTO>> getListObjectByCategory(
        @PathVariable(value = "categoryId") @Positive @NotNull Long categoryId
    ) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, getSortProperties()));
        List<FunctionDTO> dtos = functionService.getListChildByCategoryId(categoryId, unpaged);
        return ResponseEntity.ok().body(dtos);
    }

    @GetMapping("list-child-functions/{categoryId}")
    public ResponseEntity<Page<FunctionDTO>> getListFunctionPaging(
        @PathVariable(value = "categoryId") @Positive @NotNull Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable
    ) {
        pageable = pageableValidation(pageable);
        Page<FunctionDTO> page = functionService.getListFunctionsWithPaging(categoryId,name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @Override
    public String getSortFieldName() {
        return Function.FieldNames.POS_INDEX;
    }

    @Override
    public String getSearchParam(Sort.Order order) {
        return OCSUtils.getFunctionParamSearchParam(order.getProperty());
    }

    @PostMapping("/{id}/function-params")
    public ResponseEntity<FunctionParamDTO> createFunctionParam(
        @Validated(FunctionParamDTO.Create.class) @NotNull @RequestBody FunctionParamDTO dto,
        @PathVariable(value = "id") @Positive @NotNull Long functionId
    ) {
        return functionParamResource.createFunctionParam(dto, functionId);
    }

    @PutMapping("/{id}/function-params/{functionParamId}")
    public ResponseEntity<FunctionParamDTO> updateFunctionParam(
        @Validated(FunctionParamDTO.Update.class) @NotNull @RequestBody FunctionParamDTO dto,
        @PathVariable(value = "functionParamId") @Positive @NotNull Long functionParamId,
        @PathVariable(value = "id") @Positive @NotNull Long functionId
    ) {
        return functionParamResource.updateFunctionParam(dto, functionParamId, functionId);
    }

    @PutMapping("/{id}/function-params/move")
    public ResponseEntity<?> changeOrderIndex(
        @PathVariable(value = "id") @Positive @NotNull Long functionId,
        @RequestBody @Size(min = 2, max = 2) List<Long> ids
    ) {
        return functionParamResource.changeOrderIndex(functionId, ids);
    }

    @DeleteMapping("/{id}/function-params/{functionParamId}")
    public ResponseEntity<?> deleteFunctionParam(
        @PathVariable(value = "functionParamId") @Positive @NotNull Long functionParamId,
        @PathVariable(value = "id") @Positive @NotNull Long functionId
    ) {
        return functionParamResource.deleteFunctionParam(functionParamId, functionId);
    }

    @GetMapping("/{id}/function-params")
    public ResponseEntity<Page<FunctionParamDTO>> getFunctions(
        @PathVariable(value = "id") @Positive @NotNull Long functionId,
        @RequestParam(value = "name", required = false) String name,
        Pageable pageable
    ) {
        return functionParamResource.getFunctions(functionId, name, pageable);
    }

    @PostMapping("/{id}/clone")
    public ResponseEntity<OCSCloneableEntity> cloneRatingFilter(
        @PathVariable("id") @Positive Long id,
        @RequestBody @Valid FunctionCloneDTO cloneDTO) {
        cloneDTO.setId(id);
        List<CloneDTO> cloneDTOs = new ArrayList<>();
        cloneDTOs.add(cloneDTO);
        Map<Long, CloneDTO> oldNewChildMap = new HashMap<>();
        List<OCSCloneableEntity> results = ocsCloneService.cloneEntities(cloneDTOs, oldNewChildMap);
        return ResponseEntity.status(HttpStatus.CREATED).body(results.get(0));
    }

    @GetMapping("/drop-down/levels")
    public ResponseEntity<List<ReferTable>> getDropdownLevel() {
        return ResponseEntity.ok(functionService.getDropdownLevel());
    }

    @GetMapping("/drop-down/data-types")
    public ResponseEntity<List<ReferTable>> getDropdownDataType() {
        return ResponseEntity.ok(functionService.getDropdownDataType());
    }

    @GetMapping("/{id}/function-params/{functionParamId}")
    public ResponseEntity<FunctionParamDTO> getDetailFunctionParam(
        @PathVariable(value = "functionParamId") @Positive @NotNull Long functionParamId,
        @PathVariable(value = "id") @Positive @NotNull Long functionId
    ) {
        return functionParamResource.getDetailFunctionParam(functionId, functionParamId);
    }
}
