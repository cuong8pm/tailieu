package com.mycompany.myapp.web.rest.offer.common;

import java.util.List;

import com.mycompany.myapp.utils.IgnoreWildCard;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.common.VersionCharspecUseCommon;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.common.CharValueUseMapDTO;
import com.mycompany.myapp.dto.offer.common.VersionCharspecUseCommonDTO;
import com.mycompany.myapp.service.offer.common.VersionCharspecUseCommonService;

import javax.validation.Valid;

@RestController
@Validated
@IgnoreWildCard
public abstract class VersionCharspecUseCommonResource<D extends VersionCharspecUseCommonDTO, E extends VersionCharspecUseCommon> {

    protected VersionCharspecUseCommonService<D, E> commonService;

    protected abstract void setCommonService();

//    @PostMapping("/{id}")
//    public ResponseEntity<BaseResponseDTO> update(@PathVariable Long id, @RequestBody List<@Valid CharValueUseMapDTO> dtos) {
//        commonService.addCharacteristic(id, dtos);
//        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
//    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> delete(@PathVariable Long id) {
        commonService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
