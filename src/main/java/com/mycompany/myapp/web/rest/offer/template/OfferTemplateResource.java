package com.mycompany.myapp.web.rest.offer.template;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.template.OfferTemplate;
import com.mycompany.myapp.dto.CategoryDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.template.OfferTemplateDTO;
import com.mycompany.myapp.mapper.offer.template.DropDownOfferTemplateVersionDTO;
import com.mycompany.myapp.repository.offer.offertemplate.OfferTemplateRepository;
import com.mycompany.myapp.service.offer.template.OfferTemplateService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.offer.common.OfferCommonResource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Validated
@IgnoreWildCard
@RestController
@RequestMapping("/api/offer-templates")
public class OfferTemplateResource extends OfferCommonResource<OfferTemplateDTO, OfferTemplate> {
    @Autowired
    private OfferTemplateService offerTemplateService;

    @Autowired
    private OfferTemplateRepository offerTemplateRepository;


    @PostConstruct
    @Override
    protected void setBaseService() {
        this.offerCommonService = offerTemplateService;
        this.ocsCloneService = offerTemplateService;
        this.childMappingService = null;
    }

    @Override
    protected CategoryType getCategoryType() {
        return CategoryType.OFFER_TEMPlATE;
    }

    @JsonView(Tree.class)
    @GetMapping("/{id}/check-dependencies")
    public ResponseEntity<List<CategoryDTO>> checkDependenciesOfOfferTemplate(@PathVariable Long id, @RequestParam Long offerTemplateVersionId) {
        return ResponseEntity.ok(offerTemplateService.checkDependenciesOfOfferTemplate(offerTemplateVersionId));
    }

    @GetMapping("/{id}/drop-down-version")
    public ResponseEntity<List<DropDownOfferTemplateVersionDTO>> getListDropDownOfferTemplateVersion(@PathVariable Long id) {
        return ResponseEntity.ok(offerTemplateRepository.getListDropDownVersionOfOfferTemplate(id));
    }

    @GetMapping("/list-child-offer-templates/{categoryId}")
    @Override
    public ResponseEntity<Page<OfferTemplateDTO>> getOffers(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "id", required = false) String id,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description, Pageable pageable) {
        return super.getOffers(categoryId, id, name, description, pageable);
    }

    @GetMapping("/search-offer-templates")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> searchInTree(@RequestParam(value = "name", defaultValue = "") String name, @RequestParam(required = false) Boolean isPopup) {
        List<Tree> list = offerTemplateService.searchInTree(name, getCategoryType());
        return ResponseEntity.ok().body(list);
    }
}
