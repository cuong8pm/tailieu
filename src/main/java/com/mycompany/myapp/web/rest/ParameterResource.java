package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.mycompany.myapp.domain.ReferTable;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.Parameter;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ParameterDTO;
import com.mycompany.myapp.dto.SearchCriteria;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.ParameterService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@IgnoreWildCard
@RequestMapping("/api/parameters")
public class ParameterResource {

    private final Logger log = LoggerFactory.getLogger(ParameterResource.class);

    @Autowired
    private ParameterService parameterService;

    @PostMapping("/clone/{id}")
    public ResponseEntity<BaseResponseDTO> cloneParameter(@PathVariable(value = "id") Long id) {
        BaseResponseDTO parameterDTO = parameterService.cloneParameter(id);
        return ResponseEntity.ok().body(parameterDTO);
    }

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<ParameterDTO>> getParameters(
        @PathVariable(value = "categoryId") Long categoryId) {
        SearchCriteria searchCriteria = new SearchCriteria(null, null, categoryId);

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, Parameter.FieldNames.POS_INDEX));

        List<ParameterDTO> list = parameterService.getParameters(searchCriteria, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    // Get
    @GetMapping("/list-child-parameters/{categoryId}")
    public ResponseEntity<Page<ParameterDTO>> getParameters(
        @PathVariable(value = "categoryId") Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        pageable = validatePageable(pageable);
        SearchCriteria searchCriteria = new SearchCriteria(name, description, categoryId);
        Page<ParameterDTO> page = parameterService.getParameters(searchCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    // Detail
    @GetMapping("/{id}")
    public ResponseEntity<ParameterDTO> getParameterDetail(@PathVariable(value = "id") Long id) {
        ParameterDTO parameterDTO = parameterService.getParameterDetail(id);
        return ResponseEntity.ok().body(parameterDTO);
    }

    // Them
    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addParameter(
        @Validated(ParameterDTO.Create.class) @RequestBody ParameterDTO parameter) {
        parameter.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(parameterService.createParameter(parameter));
    }

    // Move
    @PutMapping("/move")
    public ResponseEntity<Object> moveParameters(
        @Validated(ParameterDTO.Move.class) @RequestBody List<ParameterDTO> parameters) {
        if (parameters.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Parameter's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(parameterService.move(parameters));
    }

    // Xoa
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteParameter(@PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        parameterService.deleteParameter(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateParameter(
        @Validated(ParameterDTO.Update.class) @RequestBody ParameterDTO parameter,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        parameter.setId(id);
        return ResponseEntity.ok().body(parameterService.updateParameter(parameter));
    }

    @GetMapping("/owner-level")
    public ResponseEntity<List<ReferTable>>getOwnerLevel() {
        return ResponseEntity.ok().body(parameterService.getOwnerLevels());
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCategorySearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.ASC, Parameter.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/search-parameters")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getUnitTypes (
        @RequestParam(value = "name") String name) {
        List<Tree> list = parameterService.searchInTree(name, CategoryType.PARAMETER);
        return ResponseEntity.ok().body(list);
    }
}
