package com.mycompany.myapp.web.rest.characteristic;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.BillingCycleType;
import com.mycompany.myapp.domain.characteristic.Characteristic;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.constant.CharacteristicType;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.characteristic.CharacteristicDTO;
import com.mycompany.myapp.dto.characteristic.MappingValueDTO;
import com.mycompany.myapp.dto.characteristic.RelationshipDTO;
import com.mycompany.myapp.dto.characteristic.ValuesDTO;
import com.mycompany.myapp.service.characteristic.CharacteristicService;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;

/**
 * REST controller for Characteristic Screens.
 */
@RestController
@RequestMapping("/api")
public class CharacteristicResource {
    private final Logger log = LoggerFactory.getLogger(CharacteristicResource.class);

    private static final String SCREEN_NAME = "characteristic";
    private static final String APPLICATION_NAME = "ocs ver 3";

    @Autowired
    private CharacteristicService characteristicService;

    @GetMapping("characteristic/list-child/{categoryId}")
    public ResponseEntity<List<CharacteristicDTO>> getBillingCycleTypes(
            @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {

        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, BillingCycleType.FieldNames.POS_INDEX));

        List<CharacteristicDTO> list = characteristicService.searchCharacteristics(categoryId, null, null, unpaged)
                .getContent();
        return ResponseEntity.ok().body(list);
    }
    // Get
    @GetMapping("characteristic/list-child-characteristic/{categoryId}")
    public ResponseEntity<Page<CharacteristicDTO>> getCharacteristics(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {

        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;

        pageable = validatePageable(pageable);
        Page<CharacteristicDTO> page = characteristicService.searchCharacteristics(categoryId, name, description, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }


    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getCharacteristicSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Direction.ASC, BillingCycleType.FieldNames.POS_INDEX) : sort);
    }

    /**
     * {@code GET  /characteristic/:id} : get the Characteristic by "id".
     *
     * @param id
     * @return CharacteristicDTO
     */
    @GetMapping("/characteristic/{id}")
    public CharacteristicDTO getCharSpecFormById(@PathVariable Long id) {
        log.debug("REST request to get Characteristic by Id: {}", id);
        CharacteristicDTO characteric = characteristicService.getCharacteristicDetail(id);
        return characteric;
    }

    @GetMapping("/characteristic/search-characteristic")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getCharacteristic(@RequestParam(value = "id", required = false) Long id,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "offerVersionId", required = false) Long offerVersionId,
            @RequestParam(value = "offerTemplateVersionId", required = false) Long offerTemplateVersionId,
            @RequestParam(value = "type" ,required = false) Integer type,
            @RequestParam(value = "charSpecId", required = false) Long charSpecId,
            @RequestParam(required = false) Boolean isPopup) {
        List<Long> removeIds = new ArrayList<>();
        List<Long> removeId = new ArrayList<>();
        List<Long> removeID = new ArrayList<>();
        if (id != null) {
            removeIds = characteristicService.getRemoveIds(id);
        }
        else if( offerVersionId != null) {
            removeId = characteristicService.getIds(offerVersionId);
            removeID = characteristicService.getRemoveId(CharacteristicType.OFFER);
            removeIds.addAll(removeId);
            removeIds.addAll(removeID);
        }
        else if (offerTemplateVersionId != null) {
            removeId = characteristicService.getIdsInOfferTemplateVersion(offerTemplateVersionId);
            removeID = characteristicService.getRemoveId(CharacteristicType.OFFER);
            removeIds.addAll(removeId);
            removeIds.addAll(removeID);
        }

        else if(charSpecId != null) {
            removeIds.add(charSpecId);
            removeIds.addAll(characteristicService.RemoveIdByCharSpecId(charSpecId));
        }
        else if(type != null && type == -1) {
            removeIds = characteristicService.getRemoveId(CharacteristicType.OFFER);
        }


        List<Tree> list = characteristicService.searchInTree(name, CategoryType.CHARACTERISTIC, removeIds, isPopup);
        return ResponseEntity.ok().body(list);
    }

    /**
     * {@code GET  /characteristic} : get the Characteristic by "id".
     *
     * @param characteristicDTO
     * @return ResponseEntity<CharacteristicDTO>
     */
    @PostMapping("/characteristic")
    public ResponseEntity<CharacteristicDTO> addCharacteristic(@Valid @RequestBody CharacteristicDTO characteristicDTO)
            throws URISyntaxException {
        log.debug("REST request to add new Characteristic: {}", characteristicDTO);
        if (characteristicDTO.getCharacteristicId() != null) {
            throw new BadRequestAlertException("characteristic_id_already_exist", SCREEN_NAME, "characteristicId");
        }
        CharacteristicDTO result = characteristicService.save(characteristicDTO);
        return ResponseEntity.created(new URI("/api/characteristic/" + result.getCharacteristicId()))
                .headers(HeaderUtil.createEntityCreationAlert(APPLICATION_NAME, true, SCREEN_NAME,
                        result.getCharacteristicId().toString()))
                .body(result);
    }

    @PutMapping("/characteristic/{id}")
    public ResponseEntity<CharacteristicDTO> updateCharacteristic(@PathVariable Long id,
            @Valid @RequestBody CharacteristicDTO characteristicDTO) throws URISyntaxException {
        log.debug("REST request to update Characteristic: {} with characteristicId: {}", characteristicDTO, id);
        characteristicDTO.setCharacteristicId(id);
        if (characteristicDTO.getCharacteristicId() == null) {
            throw new BadRequestAlertException("characteristic_id_is_null_or_invalid", SCREEN_NAME, "characteristicId");
        }
        CharacteristicDTO result = characteristicService.save(characteristicDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(APPLICATION_NAME, true, SCREEN_NAME,
                result.getCharacteristicId().toString())).body(result);
    }

    @DeleteMapping("/characteristic/{id}")
    public ResponseEntity<Void> deleteCharSpec(@PathVariable Long id) {
        log.debug("REST Delete Characterisrtic with id: {}", id);
        characteristicService.deleteCharacteristic(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(APPLICATION_NAME, true, SCREEN_NAME, id.toString()))
                .build();
    }

    @PostMapping("/characteristic/{id}/values")
    public ResponseEntity<ValuesDTO> addValues(@PathVariable Long id, @Valid @RequestBody ValuesDTO valuesDTO)
            throws URISyntaxException {
        log.debug("REST request to add new Characterisrtic Values: {} with characteristicId: {}", valuesDTO, id);
        valuesDTO.setCharacteristicId(id);
        if (valuesDTO.getValueId() != null) {
            throw new BadRequestAlertException("value_id_already_already_exist", SCREEN_NAME, "valueId");
        }
        ValuesDTO result = characteristicService.save(valuesDTO);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(APPLICATION_NAME, true, SCREEN_NAME, result.getValueId().toString()))
                .body(result);
    }

    @PutMapping("/characteristic/{id}/values/{valuesId}")
    public ResponseEntity<ValuesDTO> updateValues(@PathVariable Long id, @PathVariable Long valuesId,
            @Valid @RequestBody ValuesDTO valuesDTO) throws URISyntaxException {
        log.debug("REST request to update Characterisrtic Values: {} with characteristicId: {}", valuesDTO, id);
        valuesDTO.setCharacteristicId(id);
        valuesDTO.setValueId(valuesId);
        if (valuesDTO.getValueId() == null) {
            throw new BadRequestAlertException("value_id_is_null_or_invalid", SCREEN_NAME, "valueId");
        }
        ValuesDTO result = characteristicService.save(valuesDTO);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(APPLICATION_NAME, true, SCREEN_NAME, result.getValueId().toString()))
                .body(result);
    }

    @GetMapping("/characteristic/{id}/values/{valuesId}")
    public ValuesDTO getValuesDTOById(@PathVariable Long id, @PathVariable Long valuesId) {
        log.debug("REST request to get Characterisrtic Values with valueId: {}", valuesId);
        return characteristicService.getValuesDTO(valuesId);
    }

    @GetMapping("/characteristic/{id}/values/all")
    public List<ValuesDTO> getAllValuesDTO(@PathVariable Long id) {
        log.debug("REST request to get All Characterisrtic Values with characteristicId: {}", id);
        return characteristicService.getAllValuesDTO(id);
    }

    @GetMapping("/characteristic/{id}/values")
    public Page<ValuesDTO> getValuesDTOs(@PathVariable Long id, Pageable pageable,
            @RequestParam(defaultValue = "", required = false) String textSearch) {
        log.debug("REST request to get Characterisrtic Values in Paging with characteristicId: {}", id);
        Page<ValuesDTO> page = characteristicService.getValuesDTOs(id, pageable, textSearch);

        return page;
    }

    @DeleteMapping("/characteristic/{id}/values/delete/{valueId}")
    public ResponseEntity<Void> deleteValues(@PathVariable long id, @PathVariable long valueId) {
        log.debug("REST Delete Characterisrtic Values with valueId: {}", valueId);
        characteristicService.deleteValues(id, valueId, true);

        return ResponseEntity.noContent().headers(
                HeaderUtil.createEntityDeletionAlert(APPLICATION_NAME, true, SCREEN_NAME, String.valueOf(valueId)))
                .build();
    }

    @DeleteMapping("/characteristic/{id}/values/delete-all")
    public ResponseEntity<Void> deleteAllValues(@PathVariable Long id) {
        log.debug("REST Delete ALL Characterisrtic Values with Characterisrtic Id: {}", id);
        characteristicService.deleteAllValues(id);

        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(APPLICATION_NAME, true, SCREEN_NAME, id.toString()))
                .build();
    }

    @PostMapping("/characteristic/{id}/relationship")
    public ResponseEntity<List<RelationshipDTO>> addRelationship(@PathVariable Long id,
            @Valid @RequestBody List<RelationshipDTO> relationshipDTOs) throws URISyntaxException {
        characteristicService.saveRelationship(SCREEN_NAME,id,relationshipDTOs);
        return ResponseEntity.status(HttpStatus.CREATED).body(relationshipDTOs);
    }

    @PutMapping("/characteristic/{id}/relationship/{relationshipId}")
    public ResponseEntity<RelationshipDTO> updateRelationship(@PathVariable Long id, @PathVariable Long relationshipId,
            @Valid @RequestBody RelationshipDTO relationshipDTO) throws URISyntaxException {
        log.debug("REST request to update Characteristic Relationship: {} with relationshipId: {}", relationshipDTO,
                relationshipId);
        relationshipDTO.setParentCharacteristicId(id);
        relationshipDTO.setId(relationshipId);
        if (relationshipDTO.getId() == null) {
            throw new BadRequestAlertException("relationship_id_is_null_or_invalid", SCREEN_NAME, "id");
        }
        RelationshipDTO result = characteristicService.save(id, relationshipDTO);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(APPLICATION_NAME, true, SCREEN_NAME, result.getId().toString()))
                .body(result);
    }

    @GetMapping("/characteristic/{id}/relationship/{relationshipId}")
    public RelationshipDTO getRelationshipById(@PathVariable Long id, @PathVariable Long relationshipId) {
        log.debug("REST request to get Characteristic Relationship with relationshipId: {}", relationshipId);
        return new RelationshipDTO();
    }

    @GetMapping("/characteristic/{id}/relationship")
    public Page<RelationshipDTO> getAllRelationshipById(@PathVariable Long id, Pageable pageable,
            @RequestParam(defaultValue = "", required = false) String textSearch) {
        log.debug("REST request to get Characteristic Relationship in paging with characteriscId: {}", id);
        return characteristicService.findRelationshipByParentId(id, pageable, textSearch);
    }


    @GetMapping("/characteristic/{id}/relationship/{childId}/mapping-values")
    public List<MappingValueDTO> getAllMappingValueDTOs(@PathVariable Long id, @PathVariable Long childId) {
        log.debug(
                "REST request to get All Characteristic Relationship Maping Values with parentCharacteristicId: {}, chilCharacteristicId: {}",
                id, childId);
        return characteristicService.getAllMappingValueDTOs(id, childId);
    }

    @DeleteMapping("/characteristic/{id}/relationship/delete/{relationshipId}")
    public ResponseEntity<Void> deleteRelationship(@PathVariable Long id, @PathVariable Long relationshipId) {
        log.debug("REST Delete Characterisrtic Relationship with characterisrticId: {} and relationshipId: {}", id,
                relationshipId);
        characteristicService.deleteRelationship(id, relationshipId);

        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(APPLICATION_NAME, true, SCREEN_NAME, id.toString()))
                .build();
    }

    @DeleteMapping("/characteristic/{id}/relationship/delete-all")
    public ResponseEntity<Void> deleteCharacteristic(@PathVariable Long id) {
        log.debug("REST Delete All Characterisrtic Relationship with characterisrticId: {}", id);
        characteristicService.deleteAllRelationships(id);

        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(APPLICATION_NAME, true, SCREEN_NAME, id.toString()))
                .build();
    }

    // Move
    @PutMapping("/characteristic/move")
    public ResponseEntity<Object> moveCharacteristic(@Validated(Characteristic.Move.class) @RequestBody @Size(min = 2, max = 2) List<CharacteristicDTO> characteristics) {
        if (characteristics.size() != 2) {
            throw new BadRequestAlertException("Bad request", "Characteristic's quantity must be 2.", "");
        }
        return ResponseEntity.ok().body(characteristicService.move(characteristics));
    }

    @DeleteMapping("/category/{categoryId}/list-child-characteristic/delete-all")
    public ResponseEntity<Void> deleteAllCharSpec(@PathVariable Long categoryId) {
        log.debug("REST Delete Characterisrtic by Cateogry id: {}", categoryId);
        characteristicService.deleteByCategoryId(categoryId);
        return ResponseEntity.ok().build();
    }
}
