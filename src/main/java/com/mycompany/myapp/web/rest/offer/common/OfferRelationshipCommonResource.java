package com.mycompany.myapp.web.rest.offer.common;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.common.OfferCommon;
import com.mycompany.myapp.domain.offer.common.OfferRelationshipCommon;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ReferTableDTO;
import com.mycompany.myapp.dto.offer.common.OfferCustomDTO;
import com.mycompany.myapp.dto.offer.common.OfferRelationshipCommonDTO;
import com.mycompany.myapp.service.offer.common.OfferRelationshipCommonService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

@RestController
@Validated
@IgnoreWildCard
public abstract class OfferRelationshipCommonResource<D extends OfferRelationshipCommonDTO, E extends OfferRelationshipCommon> {

    protected OfferRelationshipCommonService<D, E> commonService;

    public abstract void setCommonService();

    @GetMapping("/list-relationships/{id}")
    public ResponseEntity<Page<OfferCustomDTO>> getChilds(@PathVariable @Positive Long id,
            @RequestParam(required = false) Integer type, @RequestParam(required = false) Boolean isParent,
            @RequestParam(required = false) String name, Pageable pageable) {
        pageable = validatePageable(pageable);
        Page<OfferCustomDTO> page = commonService.getChild(id, type, isParent, name, pageable);
        return ResponseEntity.ok().body(page);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        Sort defaultSort = Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.ID);

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? defaultSort : sort);
    }

    @DeleteMapping("")
    public ResponseEntity<BaseResponseDTO> delete(@RequestParam @Positive Long parentId,
            @RequestParam @Positive Long childId, @RequestParam("relationshipTypes") List<Integer> relationshipTypes,
            @RequestParam(required = false) Boolean isParent) {
        commonService.delete(parentId, childId, relationshipTypes, isParent);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/relation-types")
    public ResponseEntity<List<ReferTableDTO>> getListRelationShip(@RequestParam(required = false) Long parentId, @RequestParam(required = false) Long childId, @RequestParam(required = false) Boolean isParent) {
        List<ReferTableDTO> list = commonService.getListRelationShip(parentId, childId, isParent);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping("/{parentId}/{childId}")
    public ResponseEntity<BaseResponseDTO> createMapping(@PathVariable @Positive Long parentId,
            @PathVariable @Positive Long childId, @RequestBody List<ReferTableDTO> relationshipTypes,
            @RequestParam(required = false) Boolean isParent) {
        commonService.createMapping(parentId, childId, relationshipTypes, isParent);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-all-relationships/{id}")
    public ResponseEntity<List<OfferCustomDTO>> getListRelationships(@PathVariable @Positive Long id,
            @RequestParam(required = false) Integer type, @RequestParam(required = false) Boolean isParent) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE,
                Sort.by(Sort.Direction.ASC, OfferCommon.FieldNames.NAME));
        List<OfferCustomDTO> list = commonService.getChild(id, type, isParent, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }
}
