package com.mycompany.myapp.web.rest;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.ReserveInfo;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.ReserveInfoDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.service.ReserveInfoService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@Validated
@IgnoreWildCard
@RequestMapping("/api/reserve-infos")
public class ReserveInfoResource {

    @Autowired
    private ReserveInfoService reserveInfoService;

    @GetMapping("/list-child/{categoryId}")
    public ResponseEntity<List<ReserveInfoDTO>> getReserveInfos(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId) {
        Pageable unpaged = PageRequest.of(0, Integer.MAX_VALUE, Sort.by(Sort.Direction.ASC, ReserveInfo.FieldNames.POS_INDEX));
        List<ReserveInfoDTO> list = reserveInfoService.getReserveInfo(categoryId, null, null, null, unpaged).getContent();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/list-child-reserve-infos/{categoryId}")
    public ResponseEntity<Page<ReserveInfoDTO>> getReserveInfo(
        @PathVariable(value = "categoryId") @NotNull @Positive Long categoryId,
        @RequestParam(value = "id", required = false) String id,
        @RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "description", required = false) String description,
        Pageable pageable) {
        name = StringUtils.isEmpty(name) ? null : name;
        description = StringUtils.isEmpty(description) ? null : description;
        pageable = validatePageable(pageable);
        Page<ReserveInfoDTO> page = reserveInfoService.getReserveInfo(categoryId, name, description, id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReserveInfoDTO> getReserveInfoDetail(@PathVariable(value = "id") @NotNull @Positive Long id) {
        ReserveInfoDTO reverseInfo = reserveInfoService.getReserveInfoDetails(id);
        return ResponseEntity.ok().body(reverseInfo);
    }

    @PostMapping("/")
    public ResponseEntity<BaseResponseDTO> addReserveInfo(
        @Validated(ReserveInfoDTO.Create.class) @RequestBody ReserveInfoDTO reserveInfo) {
        reserveInfo.setId(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(reserveInfoService.createReserveInfo(reserveInfo));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> deleteReserveInfo(@PathVariable(name = "id", required = true) @NotNull @Positive Long id) {
        reserveInfoService.deleteReserveInfo(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    // Update
    @PutMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> updateReserveInfo(
        @Validated(ReserveInfoDTO.Update.class) @RequestBody ReserveInfoDTO reserveInfo,
        @PathVariable(name = "id") @NotNull @Positive Long id) {
        reserveInfo.setId(id);
        return ResponseEntity.ok().body(reserveInfoService.updateReserveInfo(reserveInfo));
    }

    @PutMapping("/move")
    public ResponseEntity<Object> moveReserveInfos(
        @Validated(ReserveInfoDTO.Move.class) @RequestBody @NotNull @Size(min = 2, max = 2) List<ReserveInfoDTO> reserveInfos) {
        return ResponseEntity.ok().body(reserveInfoService.move(reserveInfos));
    }
    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getReserveInfoSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
            sort == null ? Sort.by(Direction.ASC, ReserveInfo.FieldNames.POS_INDEX) : sort);
    }

    @GetMapping("/search-reserve-infos")
    @JsonView(Tree.class)
    public ResponseEntity<List<Tree>> getUnitTypes(
        @RequestParam(value = "name") String name) {
        List<Tree> list = reserveInfoService.searchInTree(name, CategoryType.RESERVE_INFO);
        return ResponseEntity.ok().body(list);
    }
}
