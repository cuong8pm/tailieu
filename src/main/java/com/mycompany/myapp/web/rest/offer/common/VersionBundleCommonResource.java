package com.mycompany.myapp.web.rest.offer.common;

import java.util.List;

import javax.validation.Valid;

import com.mycompany.myapp.utils.IgnoreWildCard;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.common.VersionBundleCommon;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.common.VersionBundleCommonDTO;
import com.mycompany.myapp.service.offer.common.VersionBundleCommonService;
import com.mycompany.myapp.web.rest.AbstractResource;

@RestController
@Validated
@IgnoreWildCard
public abstract class VersionBundleCommonResource<D extends VersionBundleCommonDTO, E extends VersionBundleCommon>{

    protected VersionBundleCommonService<D, E> commonService;

    protected abstract void setCommonService();

    @PostMapping("/{versionId}")
    public ResponseEntity<BaseResponseDTO> addBundles(@RequestBody List<Long> offerIds, @PathVariable("versionId") Long versionId) {
        commonService.createOfferVersionBundle(offerIds , versionId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Valid @PathVariable Long id ) {
        commonService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{id}/{versionId}")
    public ResponseEntity<BaseResponseDTO> updateBundle(@PathVariable("id") Long id ,@PathVariable("versionId") Long versionId ,@RequestParam(value = "offerId") Long childId) {
        commonService.updateBundle(id,versionId,childId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
