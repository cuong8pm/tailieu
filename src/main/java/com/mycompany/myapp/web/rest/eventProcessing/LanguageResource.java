package com.mycompany.myapp.web.rest.eventProcessing;

import com.mycompany.myapp.dto.eventProcessing.LanguageDTO;
import com.mycompany.myapp.service.eventProcessing.LanguageService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@IgnoreWildCard
@RequestMapping("/api/language")
public class LanguageResource  {


    @Autowired
    private LanguageService languageService;

    @GetMapping("")
    public ResponseEntity<List<LanguageDTO>> getLanguages(){
        List<LanguageDTO> languageDTOS = languageService.getLanguages();
        return ResponseEntity.ok().body(languageDTOS);
    }

}
