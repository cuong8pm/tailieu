package com.mycompany.myapp.web.rest.offer.common;

import java.util.Iterator;

import javax.validation.Valid;

import com.mycompany.myapp.utils.IgnoreWildCard;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.offer.common.VersionRedirectionCommon;
import com.mycompany.myapp.domain.offer.offer.OfferVersionRedirection;
import com.mycompany.myapp.dto.BaseResponseDTO;
import com.mycompany.myapp.dto.offer.common.OfferOfVersionRedirectionDTO;
import com.mycompany.myapp.dto.offer.common.VersionRedirectionCommonDTO;
import com.mycompany.myapp.service.offer.common.VersionRedirectionCommonService;
import com.mycompany.myapp.utils.OCSUtils;

@RestController
@Validated
@IgnoreWildCard
public abstract class VersionRedirectionCommonResource<D extends VersionRedirectionCommonDTO, E extends VersionRedirectionCommon> {

    protected VersionRedirectionCommonService<E, D> commonService;

    protected abstract void setCommonService();

    @PostMapping("")
    public ResponseEntity<D> create(@Valid @RequestBody D dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(commonService.create(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<D> update(@PathVariable Long id, @Valid @RequestBody D dto) {
        dto.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(commonService.update(dto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<D> getDetail(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(commonService.getDetail(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponseDTO> delete(@PathVariable Long id) {
        commonService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/list-child-redirections/{versionId}")
    public ResponseEntity<Page<OfferOfVersionRedirectionDTO>> getOfferVersionRedirections(@PathVariable Long versionId, Pageable pageable){
        pageable = validatePageable(pageable);
        Page<OfferOfVersionRedirectionDTO> dtos = commonService.findVersionRedirections(versionId, pageable);
                return ResponseEntity.ok().body(dtos);
    }

    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferRedirectionParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                    : sort.and(Sort.by(order.getDirection(), propery));
            }
        }

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, OfferVersionRedirection.FieldNames.ID) : sort);
    }
}
