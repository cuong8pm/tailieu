package com.mycompany.myapp.web.rest.characteristic;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mycompany.myapp.dto.characteristic.TypeDTO;
import com.mycompany.myapp.service.characteristic.TypeService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing
 * {@link com.Type.ocs.slice.domain.CharSpecType}.
 */
@RestController
@RequestMapping("/api")
public class TypeResource {

    private final Logger log = LoggerFactory.getLogger(TypeResource.class);

    private static final String ENTITY_NAME = "ocsSliceCharSpecType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeService charSpecTypeService;

    public TypeResource(TypeService charSpecTypeService) {
        this.charSpecTypeService = charSpecTypeService;
    }

    /**
     * {@code POST  /char-spec-types} : Create a new charSpecType.
     *
     * @param charSpecTypeDTO
     *            the charSpecTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new charSpecTypeDTO, or with status
     *         {@code 400 (Bad Request)} if the charSpecType has already an ID.
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect.
     */
    @PostMapping("/char-spec-types")
    public ResponseEntity<TypeDTO> createCharSpecType(@Valid @RequestBody TypeDTO charSpecTypeDTO)
            throws URISyntaxException {
        log.debug("REST request to save CharSpecType : {}", charSpecTypeDTO);
        if (charSpecTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new charSpecType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeDTO result = charSpecTypeService.save(charSpecTypeDTO);
        return ResponseEntity
                .created(new URI("/api/char-spec-types/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /char-spec-types} : Updates an existing charSpecType.
     *
     * @param charSpecTypeDTO
     *            the charSpecTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated charSpecTypeDTO, or with status {@code 400 (Bad Request)}
     *         if the charSpecTypeDTO is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the charSpecTypeDTO couldn't
     *         be updated.
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect.
     */
    @PutMapping("/char-spec-types")
    public ResponseEntity<TypeDTO> updateCharSpecType(@Valid @RequestBody TypeDTO charSpecTypeDTO)
            throws URISyntaxException {
        log.debug("REST request to update CharSpecType : {}", charSpecTypeDTO);
        if (charSpecTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypeDTO result = charSpecTypeService.save(charSpecTypeDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
                charSpecTypeDTO.getId().toString())).body(result);
    }

    /**
     * {@code GET  /char-spec-types} : get all the charSpecTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of charSpecTypes in body.
     */
    @GetMapping("/char-spec-types")
    public List<TypeDTO> getAllCharSpecTypes() {
        log.debug("REST request to get all CharSpecTypes");
        return charSpecTypeService.findAll();
    }

    /**
     * {@code GET  /char-spec-types/:id} : get the "id" charSpecType.
     *
     * @param id
     *            the id of the charSpecTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the charSpecTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/char-spec-types/{id}")
    public ResponseEntity<TypeDTO> getCharSpecType(@PathVariable Long id) {
        log.debug("REST request to get CharSpecType : {}", id);
        Optional<TypeDTO> charSpecTypeDTO = charSpecTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(charSpecTypeDTO);
    }

    /**
     * {@code DELETE  /char-spec-types/:id} : delete the "id" charSpecType.
     *
     * @param id
     *            the id of the charSpecTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/char-spec-types/{id}")
    public ResponseEntity<Void> deleteCharSpecType(@PathVariable Long id) {
        log.debug("REST request to delete CharSpecType : {}", id);
        charSpecTypeService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
