package com.mycompany.myapp.web.rest.errors;

import java.net.URI;

public final class ErrorConstants {
    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String PROBLEM_BASE_URL = "https://www.jhipster.tech/problem";
    public static final URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/problem-with-message");
    public static final URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/constraint-violation");

    public static final String NOT_FOUND = "not found";
    public static final String CONFLICT = "conflict";
    public static final String PERMISSION_DENIED = "permission denied";
    public static final String DATA_CONSTRAIN = "data constrain";
    private ErrorConstants() {}
}
