package com.mycompany.myapp.web.rest.offer.offer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Positive;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.mycompany.myapp.domain.constant.CategoryType;
import com.mycompany.myapp.domain.offer.offer.Offer;
import com.mycompany.myapp.domain.offer.offer.OfferVersionBundle;
import com.mycompany.myapp.dto.Tree;
import com.mycompany.myapp.dto.offer.offer.OfferVersionBundleDTO;
import com.mycompany.myapp.dto.offer.offer.VersionBunndleDTO;
import com.mycompany.myapp.service.offer.offer.OfferVersionBundleService;
import com.mycompany.myapp.utils.IgnoreWildCard;
import com.mycompany.myapp.utils.OCSUtils;
import com.mycompany.myapp.web.rest.offer.common.VersionBundleCommonResource;

@RestController
@IgnoreWildCard
@Validated
@RequestMapping("/api/offer-version-bundles")
public class OfferVersionBundleResource extends VersionBundleCommonResource<OfferVersionBundleDTO, OfferVersionBundle> {

    @Autowired
    private OfferVersionBundleService offerVersionBundleService;

    @PostConstruct
    @Override
    protected void setCommonService() {
        this.commonService = offerVersionBundleService;
    }

    @GetMapping("/list-bundles/{versionId}")
    public ResponseEntity<List<VersionBunndleDTO>> getBundles( @PathVariable @Positive Long versionId) {
      Pageable  unpageable = PageRequest.of(0,Integer.MAX_VALUE,Sort.by(Direction.ASC, Offer.FieldNames.ID));
        List<VersionBunndleDTO> list = offerVersionBundleService.findVersionBunndleDTOs(versionId,unpageable).getContent();
        return ResponseEntity.ok().body(list);
    }
    private Pageable validatePageable(Pageable pageable) {
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort sort = null;
        while (iterator.hasNext()) {
            Sort.Order order = iterator.next();
            String propery = OCSUtils.getOfferVersionBundleSearchParam(order.getProperty());
            if (StringUtils.isNotEmpty(propery)) {
                sort = sort == null ? Sort.by(order.getDirection(), propery)
                        : sort.and(Sort.by(order.getDirection(), propery));
            }
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                sort == null ? Sort.by(Sort.Direction.ASC, Offer.FieldNames.ID) : sort);
    }
    @GetMapping("/{versionId}")
    public ResponseEntity<Page<VersionBunndleDTO>> getAllBundle( @PathVariable @Positive Long versionId,Pageable pageable) {
       pageable = validatePageable(pageable);
       Page<VersionBunndleDTO> list = offerVersionBundleService.findVersionBunndleDTOs(versionId, pageable);
        return ResponseEntity.ok().body(list);
    }
}
