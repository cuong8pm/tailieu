package com.mycompany.myapp.config;

import com.mycompany.myapp.aop.logging.LoggingAspect;
import com.mycompany.myapp.aop.logging.OcsAspect;
import io.github.jhipster.config.JHipsterConstants;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectConfiguration {

    @Bean
    @Profile(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
    public LoggingAspect loggingAspect(Environment env) {
        return new LoggingAspect(env);
    }

    @Bean
    @Profile({JHipsterConstants.SPRING_PROFILE_PRODUCTION, JHipsterConstants.SPRING_PROFILE_DEVELOPMENT, ConfigEnvironment.STAGING})
    public OcsAspect prodAspect(Environment env) {
        return new OcsAspect(env);
    }
}
