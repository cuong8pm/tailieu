package com.mycompany.myapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Ocs Product.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private String excelTemplateRootDirectoryPath;
    private String zoneDataTemplateFileName;
    private String geoNetZoneTemplateFileName;
    
    public String getExcelTemplateRootDirectoryPath() {
        return excelTemplateRootDirectoryPath;
    }

    public void setExcelTemplateRootDirectoryPath(String excelTemplateRootDirectoryPath) {
        this.excelTemplateRootDirectoryPath = excelTemplateRootDirectoryPath;
    }

    public String getZoneDataTemplateFileName() {
        return zoneDataTemplateFileName;
    }

    public void setZoneDataTemplateFileName(String zoneDataTemplateFileName) {
        this.zoneDataTemplateFileName = zoneDataTemplateFileName;
    }

    public String getGeoNetZoneTemplateFileName() {
        return geoNetZoneTemplateFileName;
    }

    public void setGeoNetZoneTemplateFileName(String geoNetZoneTemplateFileName) {
        this.geoNetZoneTemplateFileName = geoNetZoneTemplateFileName;
    }
    
}
