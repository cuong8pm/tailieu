package com.mycompany.myapp.config.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.mycompany.myapp.domain.constant.BillingCycleStatus;
import com.mycompany.myapp.domain.constant.CategoryType;

@Converter(autoApply = true)
public class BillingCycleStatusConverter implements AttributeConverter<BillingCycleStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(BillingCycleStatus billingCycleStatus) {
        if (billingCycleStatus == null) {
            return null;
        }
        return billingCycleStatus.getValue();
    }

    @Override
    public BillingCycleStatus convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return Stream.of(BillingCycleStatus.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}