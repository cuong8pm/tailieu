package com.mycompany.myapp.config.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.mycompany.myapp.domain.constant.State;


@Converter(autoApply = true)
public class StateTypeConverter implements AttributeConverter<State, Integer>{

    @Override
    public Integer convertToDatabaseColumn(State state) {
        if(state == null) {
            return null;
        }
        return state.getValue();
    }
    
    @Override
    public State convertToEntityAttribute(Integer value) {
        if(value == null) {
            return null;
        }
        return Stream.of(State.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
