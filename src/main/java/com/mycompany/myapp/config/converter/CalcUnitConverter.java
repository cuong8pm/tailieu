package com.mycompany.myapp.config.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.mycompany.myapp.domain.constant.CalcUnitType;
import com.mycompany.myapp.domain.constant.CategoryType;

@Converter(autoApply = true)
public class CalcUnitConverter implements AttributeConverter<CalcUnitType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CalcUnitType calcUnitType) {
        if (calcUnitType == null) {
            return null;
        }
        return calcUnitType.getValue();
    }

    @Override
    public CalcUnitType convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return Stream.of(CalcUnitType.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}