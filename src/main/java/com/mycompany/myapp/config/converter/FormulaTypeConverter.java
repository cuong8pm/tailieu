package com.mycompany.myapp.config.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.mycompany.myapp.domain.constant.FormulaType;

@Converter(autoApply = true)
public class FormulaTypeConverter implements AttributeConverter<FormulaType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(FormulaType formula) {
        if(formula == null) {
            return null;
        }
        return formula.getValue();
    }

    @Override
    public FormulaType convertToEntityAttribute(Integer value) {
        if(value == null) {
            return null;
        }
        return Stream.of(FormulaType.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
    

}
