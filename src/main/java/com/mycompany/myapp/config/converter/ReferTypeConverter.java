package com.mycompany.myapp.config.converter;

import com.mycompany.myapp.domain.constant.ReferType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class ReferTypeConverter implements AttributeConverter<ReferType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(ReferType referType) {
        if (referType == null) {
            return null;
        }
        return referType.getValue();
    }

    @Override
    public ReferType convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return Stream.of(ReferType.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
