package com.mycompany.myapp.config.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.mycompany.myapp.domain.constant.CategoryType;

@Converter(autoApply = true)
public class CategoryTypeConverter implements AttributeConverter<CategoryType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CategoryType category) {
        if (category == null) {
            return null;
        }
        return category.getValue();
    }

    @Override
    public CategoryType convertToEntityAttribute(Integer value) {
        if (value == null) {
            return null;
        }
        return Stream.of(CategoryType.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}