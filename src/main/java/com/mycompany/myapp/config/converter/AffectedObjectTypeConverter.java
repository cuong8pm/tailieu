package com.mycompany.myapp.config.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

import com.mycompany.myapp.domain.constant.AffectedObjectType;

@Converter(autoApply = true)
public class AffectedObjectTypeConverter implements AttributeConverter <AffectedObjectType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(AffectedObjectType affected) {
        if(affected == null) {
            return null;
        }
        return affected.getValue();
    }

    @Override
    public AffectedObjectType convertToEntityAttribute(Integer value) {
        if(value == null) {
            return null;
        }
        return Stream.of(AffectedObjectType.values()).filter(c -> c.getValue() == value).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
    
    
}
