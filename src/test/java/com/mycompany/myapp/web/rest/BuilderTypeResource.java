package com.mycompany.myapp.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.dto.eventProcessing.BuilderTypeDTO;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeItemService;
import com.mycompany.myapp.service.eventProcessing.BuilderTypeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BuilderTypeResource {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BuilderTypeService builderTypeService;

    @MockBean
    private BuilderTypeItemService builderTypeItemService;

    @Before
    public void init(){

    }

    @Test
    @ParameterizedTest(name = "{index}: test builer type case name {0}")
    @CsvFileSource(resources = "/csvfiles/buildertypes/createBuilderTypeResource.csv", numLinesToSkip = 1)
    public void testCreateBuilderType(ArgumentsAccessor data) throws Exception{
        BuilderTypeDTO dto = new BuilderTypeDTO();
        dto.setName(data.getString(1));
        dto.setCategoryId(data.getLong(2));
        dto.setDescription(data.getString(3));

        Mockito.when(builderTypeService.createBuilderType(dto)).thenReturn(dto);

        mockMvc.perform(post("/api/builders")
                .contentType("json/application")
                .content(objectMapper.writeValueAsString(dto)))
            .andExpect(status().is(data.getInteger(4)));
    }


}
