UPDATE `category` SET `treeType` = 'unit-types' WHERE (`CATEGORY_TYPE` = 1);
UPDATE `category` SET `treeType` = 'parameters' WHERE (`CATEGORY_TYPE` = 2);
UPDATE `category` SET `treeType` = 'reserve-infos' WHERE (`CATEGORY_TYPE` = 3);
UPDATE `category` SET `treeType` = 'zone-maps' WHERE (`CATEGORY_TYPE` = 4);
UPDATE `category` SET `treeType` = 'geo-home-zones' WHERE (`CATEGORY_TYPE` = 5);
UPDATE `category` SET `treeType` = 'billing-cycles' WHERE (`CATEGORY_TYPE` = 6);
UPDATE `category` SET `treeType` = 'state-types' WHERE (`CATEGORY_TYPE` = 7);
UPDATE `category` SET `treeType` = 'state-groups' WHERE (`CATEGORY_TYPE` = 8);
UPDATE `category` SET `treeType` = 'statistics' WHERE (`CATEGORY_TYPE` = 9);
UPDATE `category` SET `treeType` = 'services' WHERE (`CATEGORY_TYPE` = 10);
UPDATE `category` SET `treeType` = 'balances' WHERE (`CATEGORY_TYPE` = 11);
UPDATE `category` SET `treeType` = 'account-balance-mappings' WHERE (`CATEGORY_TYPE` = 13);
UPDATE `category` SET `treeType` = 'pep-profiles' WHERE (`CATEGORY_TYPE` = 16);
UPDATE `category` SET `treeType` = 'characteristic' WHERE (`CATEGORY_TYPE` = 18);
UPDATE `category` SET `treeType` = 'normalizers' WHERE (`CATEGORY_TYPE` = 19);
UPDATE `category` SET `treeType` = 'rate-tables' WHERE (`CATEGORY_TYPE` = 20);
UPDATE `category` SET `treeType` = 'rating-filters' WHERE (`CATEGORY_TYPE` = 21);
UPDATE `category` SET `treeType` = 'blocks' WHERE (`CATEGORY_TYPE` = 22);
UPDATE `category` SET `treeType` = 'price-components' WHERE (`CATEGORY_TYPE` = 23);
UPDATE `category` SET `treeType` = 'action-types' WHERE (`CATEGORY_TYPE` = 24);
UPDATE `category` SET `treeType` = 'events' WHERE (`CATEGORY_TYPE` = 25);
UPDATE `category` SET `treeType` = 'actions' WHERE (`CATEGORY_TYPE` = 26);
UPDATE `category` SET `treeType` = 'offer-templates' WHERE (`CATEGORY_TYPE` = 27);
UPDATE `category` SET `treeType` = 'offers' WHERE (`CATEGORY_TYPE` = 28);
UPDATE `category` SET `treeType` = 'accumulate-balances' WHERE (`CATEGORY_TYPE` = 12);
UPDATE `category` SET `treeType` = 'acm-balance-mappings' WHERE (`CATEGORY_TYPE` = 14);
UPDATE `category` SET `treeType` = 'pcc-rules' WHERE (`CATEGORY_TYPE` = 15);

UPDATE `category` SET `treeType` = 'balance-acm-mappings' WHERE (`CATEGORY_TYPE` = '14');
UPDATE `category` SET `treeType` = 'account-balance-mappings' WHERE (`CATEGORY_TYPE` = '13');
UPDATE `category` SET `treeType` = 'balances' WHERE (`CATEGORY_TYPE` = '11');
UPDATE `category` SET `treeType` = 'accumulate-balances' WHERE (`CATEGORY_TYPE` = '12');


UPDATE `category` SET `treeType` = 'state-types' WHERE (`CATEGORY_TYPE` = '7');
UPDATE `category` SET `treeType` = 'billing-cycle-types' WHERE (`CATEGORY_TYPE` = '6');